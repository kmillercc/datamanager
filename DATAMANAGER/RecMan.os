package DATAMANAGER

public object RecMan inherits DATAMANAGER::ColumnValidators

	override	String	fApiKey1 = 'metadata'
	public		String	fCreateApiKey2 = 'rmCreateInfo'
	public		String	fCreateApiKey3
	public		String	fUpdateApiKey2 = 'rmUpdateInfo'
	public		String	fUpdateApiKey3


	
	override function Void __Init()
						
							if .fEnabled
								// **** set up a map to this object in the NodeValidateWrapper
								Object wrapper = $DataManager.ValidateWrapper
								if IsUndefined(wrapper._fColValidatorMap)
									wrapper._fColValidatorMap = Assoc.CreateAssoc()
								end
								wrapper._fColValidatorMap.(.fKey) = this	
						
						
								Object apiDataObj = $DataManager.ApiDataObj
								if IsUndefined(apiDataObj._fNodeApiKeysMap)
									apiDataObj._fNodeApiKeysMap = Assoc.CreateAssoc()
								end
								apiDataObj._fNodeApiKeysMap.(.fKey) = Assoc.CreateAssoc()
								apiDataObj._fNodeApiKeysMap.(.fKey).("CREATE") = 	{.fApiKey1, .fCreateApiKey2, .fCreateApiKey3}
								apiDataObj._fNodeApiKeysMap.(.fKey).("UPDATE") = 	{.fApiKey1, .fUpdateApiKey2, .fUpdateApiKey3}		
						
						
							end
						
						end

	
	public function __SetKeys()
						
							/* map column names from csv to the additional data fields (for updating recman metadata) */
							Assoc rmUpdateInfoMap = Assoc.CreateAssoc()
							rmUpdateInfoMap.('$RDSubject') = 'fSubject'
							rmUpdateInfoMap.('$RDOfficial') = 'fOfficial'
							rmUpdateInfoMap.('$RDRecordDate') = 'fDocDate'
							rmUpdateInfoMap.('$RDStatus') = 'fStatus'
							rmUpdateInfoMap.('$RDStatusDate') = 'fStatusDate'
							rmUpdateInfoMap.('$RDEssential') = 'fEssential'
							rmUpdateInfoMap.('$RDReceivedDate') = 'fReceivedDate'
							rmUpdateInfoMap.('$RDStorageMedium') = 'fStorage'
							rmUpdateInfoMap.('$RDAccession') = 'fAccession'
							rmUpdateInfoMap.('$RDAddressee') = 'fAddressee'
							rmUpdateInfoMap.('$RDUpdateCyclePeriod') = 'fCyclePeriod'
							rmUpdateInfoMap.('$RDNextReviewDate') = 'fNextReviewDate'
							rmUpdateInfoMap.('$RDLastReviewDate') = 'fLastReviewDate'
							rmUpdateInfoMap.('$RDRSI') = 'fRSI'
							rmUpdateInfoMap.('$RDOriginator') = 'fOriginator'
							rmUpdateInfoMap.('$RDAuthor') = 'fOriginator'
							rmUpdateInfoMap.('$RDOtherAddressee') = 'fSentTo'
							rmUpdateInfoMap.('$RDOrganization') = 'fEstablishment'	
							
							
							/* map column names from csv to the createInfo fields (for initial creating of rec managed object) */
							Assoc rmCreateInfoMap = Assoc.CreateAssoc()
							rmCreateInfoMap.('$RDSubject') = 'RMCreateInfo.rimssubject'
							rmCreateInfoMap.('$RDOfficial') = 'RMCreateInfo.official'
							rmCreateInfoMap.('$RDRecordDate') = 'RMCreateInfo.docDate'
							rmCreateInfoMap.('$RDStatus') = 'RMCreateInfo.fileStatus'
							rmCreateInfoMap.('$RDStatusDate') = 'RMCreateInfo.statusDate'
							rmCreateInfoMap.('$RDEssential') = 'RMCreateInfo.essential'
							rmCreateInfoMap.('$RDReceivedDate') = 'RMCreateInfo.rimsReceivedDate'
							rmCreateInfoMap.('$RDStorageMedium') = 'RMCreateInfo.storage'
							rmCreateInfoMap.('$RDAccession') = 'RMCreateInfo.rimsAccession'
							rmCreateInfoMap.('$RDAddressee') = 'RMCreateInfo.rimsAddressee'
							rmCreateInfoMap.('$RDUpdateCyclePeriod') = 'RMCreateInfo.vitalRecordCycle'
							rmCreateInfoMap.('$RDNextReviewDate') = 'RMCreateInfo.nextReviewDate'
							rmCreateInfoMap.('$RDLastReviewDate') = 'RMCreateInfo.rimsLastReviewDate'
							rmCreateInfoMap.('$RDRSI') = 'RMCreateInfo.rsi'
							rmCreateInfoMap.('$RDOriginator') = 'RMCreateInfo.rimsOriginator'
							rmCreateInfoMap.('$RDAuthor') = 'RMCreateInfo.rimsOriginator'
							rmCreateInfoMap.('$RDOtherAddressee') = 'RMCreateInfo.rimsSentTo'
							rmCreateInfoMap.('$RDOrganization') = 'RMCreateInfo.rimsEstablishment'
						
						
							Object child
							
							for child in OS.Children(this)
								String key = "$" + child.OSName
							
								// Update info
								if IsFeature(rmUpdateInfoMap, key)
									child.fApiKey4 = rmUpdateInfoMap.(key)
								end	
								
								// Create info
								if IsFeature(rmCreateInfoMap, key)
									child.fCreateApiKey3 = rmCreateInfoMap.(key)
								end			
							end
								
							
						
						
						end

end

package DATAMANAGER

public object ModuleDBObject inherits DBWIZAPI::ModuleDBObject

	override	List	fDBVersion = { 1, 0, 4 }
	override	Boolean	fEnabled = TRUE
	override	Object	fExecPkg = DATAMANAGER::ExecPkg
	override	List	fInstallScripts = { 'datamanager_sql', 'datamanager_sql_db' }
	override	String	fModuleName = 'DataManager'
	override	String	fName = 'datamanager'
	override	Object	fScripts = DATAMANAGER::DBScripts
	override	List	fUninstallScripts = { 'datamanager_drop', 'datamanager_drop_db' }

end

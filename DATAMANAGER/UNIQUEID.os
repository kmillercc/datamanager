package DATAMANAGER

public object UNIQUEID inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'pouniqueID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$UNIQUEID'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
							
							// *** this is only used at creation time.  For update, we allow anything to pass through because it's ignored
							if action == "CREATE"					
								Dynamic result = .ExecSql(prgCtx, "SELECT UniqueID FROM PhysItemCO WHERE UniqueID=:A1", {val})
								if IsNotError(result) && Length(result)
									return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("%1 is already in use", val))
								end
							end
						
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

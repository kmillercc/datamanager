package DATAMANAGER

public object EMAILRECEIVEDDATE inherits DATAMANAGER::Email

	override	String	fApiKey4 = 'OTEmailReceivedDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$EMAILRECEIVEDDATE'

end

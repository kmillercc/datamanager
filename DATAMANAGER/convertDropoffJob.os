package DATAMANAGER

public object convertDropoffJob inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'id', 2, 'ID', FALSE }, { 'jobType', -1, 'jobType', FALSE }, { 'action', -1, 'action', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "convertDropoffJob.DoExec", this )
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
						 	 *  id	=> Integer	=> jobId
						 	 *  jobType	=> String	=> jobType
						 	 *  action	=> String	=> action
						     **/		
						     
							// *** hand off to the API
							Assoc status = $DataManager.DropoffConverter.ConvertDropoffJob(prgCtx, r.id, r.jobType, r.action)
							if status.ok
						
								.fContent = synCore.SuccessResult(undefined)	
								
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\ 
									{ "id", IntegerType, "ID", FALSE }, \
									{ "jobType", StringType, "jobType", FALSE },\			
									{ "action", StringType, "action", FALSE } \
								}
						end

end

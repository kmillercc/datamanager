package DATAMANAGER

public object deleteFromWorkerQueue inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'data', -18, 'Data', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "deleteFromWorkerQueue.DoExec", this )
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
							 * 	data 		=> Assoc 	=> the data object for row from WORKERQUEUECURRENT
						     **/		
								
							// *** hand off to the API
							Assoc status = $DataManager.DistribAgentsAPI.DeleteItem(prgCtx, r.data)
							if status.ok
								.fContent = synCore.SuccessResult(status.result)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "data", Assoc.AssocType, "Data", FALSE } \
								}
						end

end

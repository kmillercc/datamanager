package DATAMANAGER

public object Versions inherits DATAMANAGER::ColumnValidators

	public		List	_fApiKeys = { 'pNumber', 'pComment', 'pVerMajor', 'pVerMinor', 'pType', 'pFileName', 'pFileDataSize', 'pCreateDate', 'pModifyDate', 'pMIMEType', 'pOwner' }
	public		List	_fLegacyColNames = { '$VERSION', '$VERSIONDESCRIPTION', '$MAJOR', '$MINOR', '$RENDITIONTYPE', '$VERSIONFILENAME', '$VERSIONFILESIZE', '$VERSIONCREATEDATE', '$VERSIONMODIFIEDDATE', '$VERSIONMIMETYPE', '$VERSIONCREATOR' }
	override	String	fApiKey1 = 'metadata'
	override	String	fApiKey2 = 'versionData'

end

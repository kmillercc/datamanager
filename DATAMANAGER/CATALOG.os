package DATAMANAGER

public object CATALOG inherits DATAMANAGER::Dapi

	override	String	fApiKey3 = 'pCatalog'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$CATALOG'


	
	override function Assoc ValidateVal(Object prgCtx, \
																		Dynamic val, \
																		String action, \
																		Frame apiDataObj)
																	
												if (val in {0,1,2} == 0)
													return .SetErrObj(.InitErrObj("ValidateSetVal"), "Catalog value must be either 0,1, or 2")
												end
												
												Assoc rtn
												rtn.result = val
												rtn.ok = true
												return rtn
																	
											end

end

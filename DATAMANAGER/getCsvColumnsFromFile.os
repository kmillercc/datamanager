package DATAMANAGER

public object getCsvColumnsFromFile inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'filepath', -1, 'FilePath', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
								
								Object		synCore = $DataManager.SynCoreInterface
								Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							
								/**
								 * R may contain: 
								 * 	filepath 		=> String 	=> the filepath we are validating
							     **/
							
								// *** hand off to the API
								Assoc status = $DataManager.FileUtils.CheckPathExists(r.filepath, 'file')
								if status.ok
									
									// *** load the csv
									status = $DataManager.CsvParser.GetHeader(r.filePath)	
									if status.ok
										List colNames = status.result.colNames
										
										// *** KM 5-29-19 why is this code here? makes no sense
										/*
										if Length(colNames) > 0 && Str.ByteLength(colNames[1]) > Length(colNames) 
											String s, newStr
											for s in colNames[1]
												if Str.ByteLength(s) == Length(s)
													newStr += s
												end
											end
											colNames[1] = Str.Upper(newStr)
										end	
										*/
										
										colNames = List.SetUnion({"Line", "Status"}, colNames) 
										Assoc a
										a.colNames = colNames
										a.dataFlags = status.result.dataFlags			
										.fContent = synCore.SuccessResult(a)
									else
										// for debugging .
										synCore.SetErrObj( rtn, status )
									
										.fError = rtn
									end		
								
								else
									// for debugging .
									synCore.SetErrObj( rtn, status )
								
									.fError = rtn
								end
							
								return undefined	
							End

	
	override function void SetPrototype()
								
								.fPrototype =\
									{\
										{ "filepath", StringType, "FilePath", FALSE } \
									}
							end

end

package DATAMANAGER

public object RootNodes inherits DATAMANAGER::CachedLookupObjects

	public		Dynamic	_fCache
	override	Boolean	fEnabled = TRUE


	
	public function Assoc GetRootNode(Object prgCtx, String path, String rootName = undefined)	// *** rootName allows us to pass in a relative path to a category
			
			DAPINODE 	node
			Integer 	rootIndex
			
		Assoc		cache = ._fCache	
			
			// *** KM 3-4-16 remove the "System Root:" from paths
			if Str.Upper(path[1:12]) == "SYSTEM ROOT:"
				path = path[13:]
			end
			
			List		pathParts = Str.Elements(path, ":")
		
			// *** determine the rootIndex and name of root
			if IsDefined(rootName)
				rootName = Str.Upper(rootName)
				
				// *** maybe we passed in a rootName, but the path was actually absolute -- 
				// *** make the adjustment here so that the calling function will skip to the second item in path		
				if Str.Upper(pathParts[1]) == rootName
					rootIndex = 1
				else
					rootIndex = 0
				end	
		
			elseif Length(pathParts)
				rootIndex = 1
				rootName = Str.Upper(pathParts[1])	
			else
				// *** KM 6-13-16 add distinctive error message to show where it's failing
				return .SetErrObj(.InitErrObj( "GetRootNode" ), Str.Format("Invalid path '%1.' Expecting elements to be separated by colons.", path))			
			end	
			
			// KM 11-14-18 this cache needs to be by dataid to avoid problems with session across request
			// *** build our cache (if we have not already)
			if IsUndefined(cache)
				cache = ._fCache = ._GetRootNodes(prgCtx)
			end	
		
			// KM 11-14-18 this cache needs to be by dataid to avoid problems with session across request
			// *** see if we can get the node now
			Integer dataID = cache.(rootName)
			if IsUndefined(dataID)
				// *** KM 6-13-16 add distinctive error message to show where it's failing
				return .SetErrObj(.InitErrObj( "_GetRootNode" ), Str.Format("Invalid path '%1.' Can not find the root node '%2' for user '%3.'", path, rootName, prgCtx.USession().fUserName))
			
			else
				
				node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, dataID)
				
				if IsError(node)
					return .SetErrObj(.InitErrObj( "_GetRootNode" ), Str.Format("Invalid path '%1.' Can not retrieve node '%2' for user '%3.' Permissions?", path, rootName, prgCtx.USession().fUserName))				
				end	
				
			end		
			
			Assoc result
			result.rootIndex = rootIndex		// *** location in the path for the root
			result.node = node
		
			Assoc rtn
			rtn.ok = true
			rtn.result = result
			return rtn
		
		end

	
	private function Assoc _GetRootNodes(Object prgCtx)
			
			// ***  Get Enterprise (141), Category (133), Classification (198), Doc Templates (20541) volumes
			
			// *** KM 5/6/16 make this query compatible with case sensitive SQL Server "Subtype" becomes "SubType"
			Assoc 	status = $LLIAPI.NodeUtil.ListNodes(prgCtx, "ParentID=:A1 and SubType in (:A2,:A3,:A4,:A5)", {-1, 141,133,198,20541})
			Assoc	a
			
			if status.ok
				Record	rec
				
				for rec in status.Contents
					DAPINODE node = DAPI.GetNodeById(prgCtx.DapiSess(), rec.OWNERID, rec.DATAID)
					if IsNotError(node)	
					
						// KM 11-14-18 this cache needs to be by dataid to avoid problems with session across request
						a.(Str.Upper(rec.NAME)) = node.pID
					end	 		
				end
			end		
		
			return a
		
		end

end

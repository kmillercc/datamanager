package DATAMANAGER

public object GENERATELABEL inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'GenerateLabel'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$GENERATELABEL'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckBoolean(val)
							
							if status.ok
								val = status.result
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

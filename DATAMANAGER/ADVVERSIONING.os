package DATAMANAGER

public object ADVVERSIONING inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'advVersioning'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$ADVVERSIONING'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckBoolean(val)
							
							if status.ok
								val = status.result
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

package DATAMANAGER

public object ExportProfilesAPI inherits DATAMANAGER::ProfilesAPI

	override	String	fIniSection = 'SyntBulkExporter.ExportProfiles'


	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg	
							
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_EXPORT_MNG_PROF)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end		
						
							return rtn
						
						end

end

package DATAMANAGER

public object DBScripts inherits DATAMANAGER::DatamanagerRoot

	public		Object	Mssql = DATAMANAGER::Mssql
	public		Object	Mssql70 = DATAMANAGER::Mssql70
	public		Object	Oracle = DATAMANAGER::Oracle


	script datamanager_drop
	
														
					#ifdef COMPILEME
					
					drop table %prefix%DATAMANAGER_DATACACHE
					/
					
					drop table %prefix%DATAMANAGER_JOBS
					/
					
					drop table %prefix%DATAMANAGER_JOB_SCHEDULES
					/
					
					delete from %prefix%KIni where IniSection = '%IniSection%' and IniKeyword = '%IniKeyword%'
					/
					
					%commit_command%
					/
					#endif
		
		
	
	endscript

	script datamanager_drop_db
	
			
		
	
	endscript

	script datamanager_sql
	
														
				#ifdef COMPILEME
				
				CREATE TABLE %prefix%DATAMANAGER_DATACACHE (
				  JOB_ID 			%type_int% 			%not_null%,
				  JOB_TYPE 			%type_char_32% 		%not_null%,
				  KEY1	 			%type_char_32% 		%not_null%, 
				  KEY2	 			%type_char_32% 		%null%, 
				  VAL_INT 			%type_int%			%null%,
				  VAL_DATE			%type_date%			%null%,
				  VAL_STRING_255	%type_char_255% 	%null%,
				  VAL_STRING_MAX	%type_char_MAX% 	%null%,
				  VAL_LONG			%type_long_string%	%null%, 
				  VAL_REAL			%type_real%			%null%,
				  EXPIRATION_DATE	%type_date%			%not_null%
				)
				/
				
				create index DATAMANAGER_DATACACHE_IDX1 on %prefix%DATAMANAGER_DATACACHE( JOB_ID, JOB_TYPE, KEY1, KEY2 )
				/
				
				create index DATAMANAGER_DATACACHE_IDX2 on %prefix%DATAMANAGER_DATACACHE( EXPIRATION_DATE )
				/
				
				create index DATAMANAGER_DATACACHE_IDX3 on %prefix%DATAMANAGER_DATACACHE( JOB_ID, JOB_TYPE, KEY1, KEY2, VAL_INT )
				/
				
				create index DATAMANAGER_DATACACHE_IDX4 on %prefix%DATAMANAGER_DATACACHE( JOB_ID, JOB_TYPE, KEY1, KEY2, VAL_DATE )
				/
				
				create index DATAMANAGER_DATACACHE_IDX5 on %prefix%DATAMANAGER_DATACACHE( JOB_ID, JOB_TYPE, KEY1, KEY2, VAL_STRING_255 )
				/
				
				create index DATAMANAGER_DATACACHE_IDX7 on %prefix%DATAMANAGER_DATACACHE( JOB_ID, JOB_TYPE, KEY1, KEY2, VAL_REAL )
				/
				
				create index DATAMANAGER_DATACACHE_IDX8 on %prefix%DATAMANAGER_DATACACHE( JOB_ID, JOB_TYPE, KEY1 )
				/
				
				CREATE TABLE %prefix%DATAMANAGER_JOB_SCHEDULES (
					SCHEDULEID			%type_identity%,	
					JOBNAME				%type_char_32% 		%not_null%,
					JOBTYPE				%type_char_32% 		%not_null%,
					JOBSUBTYPE			%type_char_32% 		%not_null%,
					JOBSTATUS			%type_char_32% 		%not_null%,
					ENABLED				%type_int%			%not_null%,
					OWNERID				%type_int%			%not_null%,
					JOBDESC				%type_char_255%		%not_null%,
					SCHEDULE_TYPE		%type_int%			%not_null%,
					OPTIONS				%type_long_string%	%not_null%,
					DATE_CREATED		%type_date%			%not_null%,
					DATE_MODIFIED		%type_date%			%not_null%,
					LAST_RUN_DATETIME	%type_date%			%null%,
					NEXT_RUN_DATETIME	%type_date%			%null%,
					NOTIFY_EMAILS		%type_char_MAX%		%null%,
					ONE_TIME_DATETIME	%type_date%			%null%,
					DAY_PATTERN 		%type_int%			%null%,
					DAY_RECURRENCE		%type_int%			%null%,
					DAYS_OF_WEEK		%type_char_32% 		%null%,
					DAILY_FREQUENCY		%type_int%			%null%,
					DAILY_AT_TIMES		%type_char_MAX%			%null%,
					DAILY_EVERY			%type_int%			%null%,
					DAILY_EVERY_UNIT	%type_int%			%null%,
					START_TIME			%type_int%			%null%,
					END_TIME			%type_int%			%null%,
					START_DATE			%type_date%			%null%,
					END_DATE			%type_date%			%null%
				)
				/
				
				create unique index DM_JS_SCHEDULEID on %prefix%DATAMANAGER_JOB_SCHEDULES( SCHEDULEID )
				/
				
				create index DM_JS_OWNERID on %prefix%DATAMANAGER_JOB_SCHEDULES( OWNERID )
				/
				
				create index DM_JS_JOBTYPE on %prefix%DATAMANAGER_JOB_SCHEDULES( JOBTYPE )
				/
				
				create index DM_JS_JOBSUBTYPE on %prefix%DATAMANAGER_JOB_SCHEDULES( JOBSUBTYPE )
				/
				
				insert into %Prefix%KIni values( '%IniSection%', '%IniKeyword%', '{1,0,4}' )
				/
				
				%commit_command%
				/
				#endif
					
			
		
	
	endscript

	script datamanager_sql_db
	
			
		
	
	endscript

	
	override function void scratch()
						
			// Get the SQL replacement values for the current database.
			//
			Boolean isOracle = false
			Assoc mapping
		
			if ( isOracle )
				mapping = $DBWizAPI.OracleAPI._DatabaseMapping()
			else
				mapping = $DBWizAPI.MsSQLAPI._DatabaseMapping()
			end
		
			String sqlScript = "CREATE TABLE SYN_JOB (" +\
		  "JOB_ID 			%type_identity%," +\
		  "JOB_TYPE 			%type_char_32% 	%not_null%," +\
		  "EXT_ID			%integer%		%not_null%," +\
		  "JOB_STATUS 		%type_char_32%," +\
		  "START_DATE		%type_date% 			%not_null%," +\
		  "COMPLETE_DATE 	%type_date%," +\
		  "LAST_UPDATED 		%type_date%			%not_null%," +\
		  "EXPIRATION_DATE	%type_date%," +\
		  "TIMEOUT 			%type_int% 		%not_null%," +\
		  "PAYLOAD 			%type_char_MAX%)"
		
			
			//
			// Make any neccessary substitutions.
			//
			
			String sqlString = Str.Substitute( sqlScript, Assoc.ToRecord( mapping ) )
			
			echo(sqlString)
		
		
		end

end

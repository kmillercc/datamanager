package DATAMANAGER

public object NodeIterator inherits DATAMANAGER::ExportIterators

	override	Dynamic	_fChunks
	public		String	_fDefaultFileExt
	public		Date	_fDocFilterDate = 2013-12-13T17:17:46
	public		String	_fErrorCsvNdcName = 'DataManagerErrorCsvLog'
	public		Dynamic	_fFileNameCache
	public		Date	_fFlatStructure = 2013-12-13T17:17:46
	public		Date	_fHierarchyStructure = 2013-12-13T17:17:46
	public		Boolean	_fIncludeRoot = TRUE
	public		Integer	_fMaxNameLength = 9999
	public		Dynamic	_fMediaTypeNames
	public		Boolean	_fMetadataOnly = FALSE
	public		Assoc	_fNameTruncCache
	public		List	_fNodeTypesFilter
	override	Dynamic	_fParentPaths
	public		Boolean	_fUseObjectIdForFileName = FALSE
	public		Boolean	_fUseObjectIdForFolderName = FALSE
	public		Boolean	_fWantAllVersions = FALSE
	public		Boolean	_fWantCsv = TRUE
	public		Boolean	_fWantFiles = FALSE
	public		Boolean	_fWantFolders = FALSE
	public		Boolean	_fWantRenditions = FALSE
	override	Boolean	fEnabled = TRUE
	override	String	fSubKey = 'nodes'


	
	override function Assoc Callback(Integer id, String path, Dynamic node, Boolean isRoot, String parentPath, Boolean isFolderNode = undefined, Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType))
													
				Assoc		rtn				
				Assoc		status
				Assoc		options = ._fOptions
				Object		prgCtx = ._fPrgCtx
				String 		newFolderPath = parentPath
				
				// *** see if we can skip out of this based on subtype and our options
				if ._CanSkip(node, ._fOptions)
					rtn.ok = true
					return rtn
				end
			
				// *** increment the counter
				.fIterateTotal+=1
				
				// ***  track the min and max ids
				if id < .fMinId || .fMinId==0
					.fMinId = id
				end
				if id > .fMaxId
					.fMaxId = id
				end			
				
				if IsUndefined(isFolderNode)
					isFolderNode = ._IsFolderNode(node, llNode)
				end
				
				// *** make sure we don't have the volume here
				if node.pId < 0
					// *** get the actual node
					node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
					
					if IsError(node)
						return .SetErrObj(.InitErrObj("Callback"), Str.Format("Cannot get project for volume nodeid=%1", node.pId))
					end
				end
			
				// *** get the path to export this node to
				String exportToThisPath
				if options.hierarchyStructure
					exportToThisPath = parentPath	
			
				elseif options.flatStructure
					exportToThisPath = ._fRootTargetPath
				end	
			
				// *** get a path to the csv file we will be writing to
				String jsonPath
				if options.numMetadataFiles == "multiple"
					jsonPath = exportToThisPath + options.metadataFileName + ".json"
				elseif options.numMetadataFiles == "single"
					jsonPath = ._fRootTargetPath + options.metadataFileName + ".json"
				end	
			
				// *** make sure we have the metadata gatherer before we call the _Handle methods below
				._fMetadataGatherer = ._GetMetadataGatherer(jsonPath, ._fColNames)
				
				// *** Determine if there's a version here	
				if llNode.fVersioned
					// *** get version file info and metadata
					status = ._HandleFiles(prgCtx, id, node, path, jsonPath, exportToThisPath)
					if !status.ok
						return .SetErrObj(.InitErrObj("Callback"), status)
					end	
										
				elseif !llNode.fVersioned
					// *** get node metadata
					status = ._HandleNonFile(prgCtx, id, node, path, jsonPath, exportToThisPath, isFolderNode)
					if !status.ok
						return .SetErrObj(.InitErrObj("Callback"), status)
					else	
						newFolderPath = status.newFolderPath
					end					
				
				end	
				
				// *** update the job comleted count
				$DataManager.JobInterface.IncrementCompletedCount(prgCtx, ._fJob)
				
				// *** audit the success of this export
				Assoc data
				data.nodePath = path
				data.action = "Exported"
				data.jsonPath = jsonPath
				.GetLogger().Debug("Successfully exported: " + Str.String(data))
				
				rtn.ok = true
				rtn.newFolderPath = newFolderPath
				return rtn	
			
			end

	
	override function void InitSubclass(Assoc options, Assoc sourceSettings)
												
				Object prgCtx = ._fPrgCtx
			
				// *** needed for metadata gathering
				._fGatherers = Assoc.CreateAssoc()
			
				// *** name truncate cache
				._fNameTruncCache = Assoc.CreateAssoc()
		
				// *** params we get from options
				._fDocFilterDate = options.docFilterDate
				._fHierarchyStructure = options.hierarchyStructure
				._fFlatStructure = options.flatStructure
				._fMetadataOnly = options.metadataOnly
				._fWantAllVersions = options.wantAllVersions
				._fDefaultFileExt = IsDefined(options.defaultFileExt) ? options.defaultFileExt : 'unknown'
				._fIncludeRoot = options.includeRoot	
				._fWantRenditions = options.wantRenditions
				
				// *** want csv file?
				._fWantCsv = !options.noCsvFile
			
				// *** are these needed?
				._fUseObjectIdForFolderName = options.useObjectIdForFolderName
				._fUseObjectIdForFileName = options.useObjectIdForFileName	
				._fMaxNameLength =	options.maxNameLength
				
				// *** initialize the filename cache
				._fFileNameCache = Assoc.CreateAssoc()
				
				// *** initialize the current path and separator
				._fSeparator = File.Separator()
				
				// *** how about a list of category ids?
				
				if IsDefined(options.catIds) && options.catIds != 0 && options.catIds != "" 
					options.catIds = Type(options.catIds) == ListType ? options.catIds : {options.catIds}
				else
					options.catIds = {}
				end
				
				// *** object types? default is undefined to allow for all
				String 	objectTypes = IsDefined(options.objectTypes) ? options.objectTypes : "all"
				List	nodeTypeFilter
				if objectTypes == "specified" && IsDefined(options.specNodeTypes) && options.specNodeTypes != "" 
					nodeTypeFilter = Type(options.specNodeTypes) == ListType ? options.specNodeTypes : {options.specNodeTypes}
				
				elseif objectTypes == "files"
					nodeTypeFilter = $DataManager.ObjectTypes.GetFileTypes(prgCtx).listOfSubtypes
					
				elseif objectTypes == "folders"
					nodeTypeFilter = $DataManager.ObjectTypes.GetContainerTypes(prgCtx).listOfSubtypes
					
				end		
				
				// *** colNames ?
				._fColNames = options.colNames
				
				// *** initialize specific node types
				._fNodeTypesFilter = nodeTypeFilter
				
			end

	
	override function Assoc Iterate(Object prgCtx, Dynamic node, String nodePath, Boolean isRoot = true, String folderPath = ._fRootTargetPath, Assoc result = Assoc.CreateAssoc())
																		
				/*
					Iterates Content Server nodes and calls a callback to process them
				*/								
									
				Assoc 	rtn
				Assoc	status
				Assoc 	options = ._fOptions		
				Integer limit = ._fLimit
				Boolean	recurse = options.recurse
			
				if !IsFeature(result, 'numErrors')	
					result.numErrors = 0
				end	
				
				// *** get llNode	
				Object		llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)
				
				// *** is this a folder node?
				Boolean		isFolderNode = ._IsFolderNode(node, llNode)		
		
				// *** include the root node?
				if !isRoot || ._fIncludeRoot
					// *** call the callback
					status = this.Callback(node.pId, nodePath, node, false, folderPath, isFolderNode, llNode)
					if !status.ok
						result.numErrors += 1
						.GetLogger().Error(status.errMsg)
						
						if !status.ok
							// *** KM 12-20-18 Log Failure to csv
							._LogFailure(node, nodePath, folderPath, status)	
						end
						
					else
						folderPath = status.newFolderPath
					end
				end		
			
				if isRoot || (recurse && isFolderNode)
					// *** get a list of files, passing in callback
					Object childGetter = $DataManager.NodeChildGetter.New(node.pSubType)
					status = childGetter.GetSubNodes(prgCtx, node)	
					if !status.ok
						// KM 12-18-19 just report the error don't interrupt the export
						// return .SetErrObj(.InitErrObj("Iterate"), status)
						result.numErrors += 1
						.GetLogger().Error(status.errMsg)
						
					else
						DAPINODE childNode
						
						for childNode in status.result
							String 	childNodePath = nodePath + ":" + childNode.pName
							
							if .fIterateTotal > limit
								return .SetErrObj(.InitErrObj("Iterate"), "Limit of " + Str.String(limit) + " reached.")
							end
							
							// *** recurse
							status = Iterate(prgCtx, childNode, childNodePath, false, folderPath, result)
							if !status.ok
								// KM 12-18-19 just report the error don't interrupt the export
								result.numErrors += 1
								.GetLogger().Error(status.errMsg)
								//return .SetErrObj(.InitErrObj("Iterate"), status)
							end				
							
						end
					end	
				end					
				
				rtn.result = result
				rtn.ok = true
				return rtn	
				
			end

	
	private function Boolean _CanSkip(DAPINODE node, Assoc options)
													
				// *** check node types filter
				if IsDefined(._fNodeTypesFilter) && Length(._fNodeTypesFilter) && (node.pSubtype in ._fNodeTypesFilter == 0)
					return true	
				end
												
				// *** make sure we don't have a restrictions on this document by create date
				if node.pSubtype == $TypeDocument && node.pCreateDate < ._fDocFilterDate
					return true
				end	
				
				return false
				
			end

	
	private function Assoc _CheckPathLength(DAPINODE node, String childNodePath, String filePath)
			
		if Length(filePath) > 254
			Object logger = .GetLogger()
			logger.ERROR("")
			logger.ERROR("Cannot fetch document. The following path exceeds the Windows 260-character limit:")
			logger.ERROR(Str.Tab() + filePath)
			
			Assoc rtn =  .SetErrObj(.InitErrObj("CheckPathLength"), "File path exceeds Windows 260-character limit.")	
						
			return rtn
		end			

		return Assoc{'ok': true}
	end

	// Patched by Pat201907040
	private function Assoc _ExportFile(	Object prgCtx, DAPIVERSION version, String filePath)
		
		Object logger = .GetLogger()
		if Length(filePath) > 255
			logger.ERROR("")
			logger.ERROR("Cannot fetch document. The following path exceeds the Windows 255 character limit:")
			logger.ERROR(Str.Tab() + filePath)
			return .SetErrObj(.InitErrObj("ExportFile"), "File export failed.")
		end
	
		// log the version
		logger.INFO("About to fetch the version to the following path: " + filePath)

		Dynamic result = DAPI.FetchVersion( version, filePath )				
		if IsError(result)	
			Object logger = .GetLogger()
			logger.ERROR("")
			logger.ERROR(Str.Format(" ERROR: Cannot Fetch file to path '%1' for nodeid %2 versionNum %3", filePath, version.pNodeId, version.pNumber))
	
			return .SetErrObj(.InitErrObj("ExportFile"), "Cannot Fetch file to path: " + Str.String(result))
	
		end
	
		Assoc rtn
		rtn.ok = true
		return rtn
	
	end

	
	private function String	_GetFileNameForDoc(String exportDir, DAPINODE node, DAPIVERSION version, Boolean appendVerNum, String appendStr = "")									
												
				// *** start with the node's Name
				// *** KM 11/5/15 pass in false to avoid stripping illegal chars -- we'll do that below
				String fileName = ._GetFileNameFromNode(exportDir, node, false, false)
			
				// *** version file extension
				String fileExt = ._fDefaultFileExt
				
				// *** if we have a file extension in DVERSDATA, grab that instead
				if IsDefined(version) && IsDefined(version.pFileType)
					fileExt = version.pFileType
					if fileExt == 'text/html'
						fileExt = "txt"
					elseif IsDefined(Str.Locate(fileExt, '/'))
						fileExt = Str.ReplaceAll(fileExt, '/', '')	
					end	
				end	
				
				// *** now parse the name
				// **** Grab the base name and any implicit file extension from the dtree name
				// *** example = name.pdf	period is in pos 5  Length of filename is 8	
				String baseName
			
				Integer		periodLoc = Str.RChr(fileName, '.')
				if IsDefined(periodLoc) && Length(fileName) - periodLoc in {3,4}
					if fileExt == ._fDefaultFileExt
						fileExt = fileName[periodLoc+1:Length(fileName)]
					end	
					baseName = fileName[1:periodLoc-1]	
				else
					baseName = fileName
				end	
				
				// *** Add the version number if it's requested
				baseName = appendVerNum ? Str.Format("%1_v%2", baseName, version.pNumber) : baseName
				
				// *** is there an append string to add?
				baseName += appendStr
					
				
				// *** truncate if we need to
				if Length(baseName) > ._fMaxNameLength-3
					baseName = ._TruncateName(exportDir, baseName, 'file', ._fMaxNameLength-3)
				end		
				
				// *** finally, set the name
				fileName = baseName + "." + fileExt
				
				// *** strip out illegal file characters -- KM 11/5/15 moved this down here to strip out illegal chars in file ext too
				fileName = $DataManager.FileUtils.StripIllegalFileChars(fileName)	
				
				// *** make sure we won't have any name collisions here
				String key = ._HashKey(exportDir)
				if IsFeature(._fFileNameCache, key) && fileName in ._fFileNameCache.(key) 
					fileName = baseName + "_" + Str.String(version.pId) + "." + fileExt	
				end	
				
				// *** finally cache the name
				List fileNames = IsFeature(._fFileNameCache, key) ? ._fFileNameCache.(key) : {}
				fileNames = List.SetAdd(fileNames, fileName)
				._fFileNameCache.(key) = fileNames
				
				return fileName
			
			end

	
	private function String _GetFileNameFromNode(String exportDir, DAPINODE node, Boolean isFolderNode, Boolean stripIllegalChars=true)
																				
				String fileName
			
				if isFolderNode
					// *** do they want us to use DATAID instead of node.pName?
					if ._fUseObjectIdForFolderName
						fileName = Str.String(node.pId)
					else
						fileName = node.pName				
					end
				else
					// *** do they want us to use DATAID instead of node.pName?
					if ._fUseObjectIdForFileName
						fileName = Str.String(node.pId)
					else
						fileName = node.pName
					end	
				end
				
				if stripIllegalChars
					// *** and tidy up by removing illegal chars
					fileName = $DataManager.FileUtils.StripIllegalFileChars(fileName)	
				end
				
				if Length(fileName) > ._fMaxNameLength
					// *** now truncate if needed
					fileName = ._TruncateName(exportDir, fileName, isFolderNode ? "folder" : "file", ._fMaxNameLength)
				end	
			
				if isFolderNode
					fileName += File.Separator()
				end
			
				return fileName
			
			end

	
	private function Assoc _HandleFiles(Object prgCtx, \
										Integer itemNo, \
										DAPINODE node, \
										String nodePath, \
										String csvPath, \
										String exportDir)
											
				Assoc 		rtn
				Assoc		status
				DAPIVERSION version
				Boolean		allVersions = ._fWantAllVersions
				Boolean 	appendVerNum = allVersions	
				Boolean		wantRenditions = ._fWantRenditions	
				List 		versions
			
				if allVersions
					versions = DAPI.ListVersions(node)
					if IsError(versions)
						rtn = .InitErrObj("_HandleFiles")
						.SetErrObj(rtn, versions)
						return rtn
					end	
					
				else
					version = DAPI.GetVersion( node, node.pVersionNum )
					if IsError(version)
						rtn = .InitErrObj("_HandleFiles")
						.SetErrObj(rtn, Str.Format("Could not get current version for node dataid [%1] versionNum [%2]", node.pId, node.pVersionNum))
						return rtn
					else		
						versions = {version}
					end
				end	
			
				Integer versionCount = 0
				
				for version in versions	
					String		sourcePath = ""	
					String      fileName = ""
			
					if !(._fMetadataOnly)
						// *** increment the versionCount	
						versionCount += 1
								
						// *** increment our counters if we have more than 1 version 
						// *** the latest version was already counted in the main Callback function
						if versionCount > 1
							.fIterateTotal+=1
						end		
						
						// *** call _HandleVersion to take care of the version
						status = ._HandleVersionFile(versionCount, exportDir, nodePath, csvPath, node, version, appendVerNum, allVersions)
						if !status.ok
							rtn = .InitErrObj("_HandleFiles")
							.SetErrObj(rtn, status)
							return rtn
						else
							sourcePath = status.result.sourcePath
							fileName = status.result.fileName
						end	
					end	
						
					// *** handle version metadata now
					status = ._HandleVersionMetadata(prgCtx, node, version, csvPath, sourcePath, fileName)
					if !status.ok
						rtn = .InitErrObj("_HandleFiles")		
						.SetErrObj(rtn, status)
						return rtn
					end				
					
					// *** handle renditions here
					if wantRenditions && !._fMetadataOnly
						DAPIVERSION rVersion
						List renditions = DAPI.ListRenditions(version)
						if IsError(renditions)
							rtn = .InitErrObj("_HandleFiles")			
							.SetErrObj(rtn, renditions)
							rtn.errMsg = Str.Format("Error getting renditions for version %1: ", version.pNumber) + rtn.errMsg
						end
						
						for rVersion in renditions
							// *** increment the versionCount	
							versionCount += 1
									
							// *** increment our counters if we have more than 1 version
							// *** the latest version was already counted in the main Callback function
							if versionCount > 1
								.fIterateTotal+=1
							end
							
							status = ._HandleVersionFile(versionCount, exportDir, nodePath, csvPath, node, rVersion, true, true, "_Rendition")	
							if !status.ok
								rtn = .InitErrObj("_HandleFiles")
								.SetErrObj(rtn, status)
								return rtn
							else
								sourcePath = status.result.sourcePath
								fileName = status.result.fileName
							
								// *** handle rendition metadata now
								status = ._HandleVersionMetadata(prgCtx, node, rVersion, csvPath, sourcePath, fileName)
								if !status.ok
									rtn = .InitErrObj("_HandleFiles")
									.SetErrObj(rtn, status)
									return rtn
								end							
							end		
						end
					end	
				end	
				
				rtn. ok = true	
				return rtn
			
			end

	
	private function Assoc _HandleNonFile(	Object prgCtx, \
													Integer itemNo, \	
													DAPINODE node, \
													String nodePath, \
													String csvPath, \
													String exportDir, \
													Boolean isFolderNode)
												
				Assoc 		rtn = .InitErrObj("_HandleFolder")
				Assoc		status
				String 		sourcePath
				String 		newFolderPath = exportDir
		
				// *** all the stuff that we will gather and cache at the end	
			
				// *** file output section
				// *** this is a non-file type node. Could be represented as a folder in the export dir when we export
				if ._fMetadataOnly
					
					// *** nothing needed here	
						
				
				elseif ._fHierarchyStructure
					
					// *** ok, determine what the path would be
					newFolderPath = exportDir + ._GetFileNameFromNode(exportDir, node, isFolderNode)
		
					// *** the sourcepath needs to be relative to csvPath
					sourcePath = IsDefined(newFolderPath) ? Str.Replace(newFolderPath, $DataManager.FileUtils.GetParentPath(csvPath), "") : ""
					
					status = ._ProcessFolder(._fPrgCtx, newFolderPath, node, nodePath)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end	
		
				end	
				
				if ._fWantCsv
					// *** gather the metadata for this node
					status = ._fMetadataGatherer.WriteMetadata(prgCtx, node, undefined, sourcePath)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end
				end		
				
				rtn.newFolderPath = newFolderPath
				return rtn
				
			end

	
	private function Assoc _HandleVersionFile(Integer versionCount, \
										String exportDir, \
										String nodePath, \
										String csvPath, \
										DAPINODE node, \
										DAPIVERSION version, \
										Boolean appendVerNum, \
										Boolean useFileName, \
										String appendStr = "")
									
				Assoc 		rtn = .InitErrObj("_HandleVersionFile")	
				Object 		logger = .GetLogger()
			
				// ***  Get the file name
				String fileName = ._GetFileNameForDoc(exportDir, node, version, appendVerNum, appendStr)
				if IsUndefined(fileName) || Length(fileName)==0
					.SetErrObj(rtn, Str.Format("Could not get file path for nodeid %1.", node.pId))
					return rtn
				end

				logger.INFO("_GetFileNameForDoc returns " + fileName)
				
				// *** append the filename onto the existing exportDir
				String 	fullFilePath = exportDir + fileName
				
				logger.INFO("Full sourcePath is " + fullFilePath)
				
				// *** the sourcepath needs to be relative to csvPath
				String sourcePath = Str.Replace(fullFilePath, $DataManager.FileUtils.GetParentPath(csvPath), "")
				
				// *** write out the file
				Assoc	itemData
				itemData.versionId = version.pId
				itemData.filePath = fullFilePath
				itemData.parentPath = $DataManager.FileUtils.GetParentPath(fullFilePath)
		
				Assoc status = ._ProcessFile(._fPrgCtx, itemData, node, nodePath)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				end	
				
				Assoc result
				result.sourcePath = sourcePath
				result.fileName = fileName
				rtn.result = result	
					
				return rtn
			
			end

	
	private function Assoc _HandleVersionMetadata(	Object prgCtx, DAPINODE node, DAPIVERSION version, String csvPath, String sourcePath, String fileName = "")
				
				Assoc rtn
				Assoc status	
			
				if ._fWantCsv
					// *** mGatherer get metadata
					status = ._fMetadataGatherer.WriteMetadata(prgCtx, node, version, sourcePath, fileName)
					if !status.ok
						return .SetErrObj(.InitErrObj("_HandleVersionMetadata"), status)
					end	
				end
				
				rtn.ok = true	
				return rtn
			
			end

	
	private function void _LogFailure(	DAPINODE childNode, String childNodePath, String filePath, Assoc status)
												
				Assoc options = ._fOptions
				String errorCsvPath = options.auditFolderPath + "errors.csv"
					
				Assoc csvData
				
				csvData.nodeid = childNode.pID
				csvData.nodePath = childNodePath
				csvData.filePath = filePath
				csvData.error = status.errMsg
				
				// *** log to error.csv file		
				$DataManager.AuditAPI.New().LogCsv(._fErrorCsvNdcName, errorCsvPath, csvData)	
				
				// resume the normal logging
				$DataManager.SynCoreInterface.PushNDC('DataManagerProcessLog')
			
			end

	
	private function Assoc _ProcessFile(Object prgCtx, Assoc itemData, DAPINODE node, String childNodePath)
																									
				Assoc 	status
				
				// check path length
				status = ._CheckPathLength(node, childNodePath, itemData.filePath)
				if !status.ok
					return .SetErrObj(.InitErrObj("_CreateFileFolder") , status)
				end		
		
				// *** ensure parent path exists
				status = $DataManager.FileUtils.EnsurePathExists(itemData.parentPath)
				if !status.ok
					return .SetErrObj(.InitErrObj("_ProcessFile") , status)
				end		
			
				DAPIVERSION version = DAPI.GetVersionByID(itemData.versionId, prgCtx.DapiSess())
				if IsError(version)
					return .SetErrObj(.InitErrObj("_ProcessFile"), Str.Format("Could not get version from versionId %1", itemData.versionId))
				end		
			
				status = ._ExportFile(prgCtx, version, itemData.filePath)
				if !status.ok
					return .SetErrObj(.InitErrObj("_ProcessFile") , status)
				end				
					
				Assoc rtn
				rtn.ok = true
				return rtn
			end

	
	private function Assoc _ProcessFolder(Object prgCtx, String folderPath, DAPINODE node, String childNodePath)
																									
				Assoc 	status
				
				// check path length
				status = ._CheckPathLength(node, childNodePath, folderPath)
				if !status.ok
					return .SetErrObj(.InitErrObj("_CreateFileFolder") , status)
				end		
					
				// *** call ensure path exists
				status = $DataManager.FileUtils.EnsurePathExists(folderPath)
				if !status.ok
					return .SetErrObj(.InitErrObj("_ProcessFile") , status)
				end			
		
				Assoc rtn
				rtn.ok = true
				return rtn
			end

	
	private function String _TruncateName(	String exportDir, String name, String objectType, Integer maxLength)
												
				String	ext = ''										
				
				// *** we might need to mess with the file extension
				if objectType == "file"
					Integer	pointLoc = Str.RChr(name, '.')
					Boolean	containsExtension = IsDefined(pointLoc) && pointLoc > Length(name) - 5
					if containsExtension
						ext = name[pointLoc:]
						name = name[1:pointLoc-1]
					end	
				end	
			
				// *** truncate if we need to	
				if Length(name) > maxLength
		
					String key = ._HashKey(exportDir)
		
					if IsFeature(._fNameTruncCache, key)
						._fNameTruncCache.(key) += 1
					else
						._fNameTruncCache.(key) = 1
					end	
		
					Integer nameTruncNumber = ._fNameTruncCache.(key)
					String newName = Str.Format('%1_%2%3', name[1:maxLength], nameTruncNumber, ext)
					String s = Str.Format(Str.Tab() + "Truncating name '%1' to '%2' because it exceeds the maxLength of %3", name, newName, maxLength)
					.GetLogger().Info(s)
					
					return newName
				else
					return name	
				end	
			
			end

end

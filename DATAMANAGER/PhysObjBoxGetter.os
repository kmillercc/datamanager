package DATAMANAGER

public object PhysObjBoxGetter inherits DATAMANAGER::NodeChildGetter

	override	Integer	fSubKey = 494


	
	override function Assoc GetSortedContents(Object prgCtx, \
															Dynamic node, \
															String sortBy = "NAME")
								
								Assoc		rtn = .InitErrObj("GetSortedContents")
								
								
								String 		stmt = "SELECT DTree.* from DTree, PhysItemCO WHERE DTree.DataID = PhysItemCO.NodeID AND PhysItemCO.BoxID=:A1 order by %1"
								RecArray 	results
								
								// *** query to get results
								results = .ExecSql(prgCtx, stmt, {node.pId})
								
								// *** loop through and get DAPINODE handles for each of these
								if IsError(results)
									.SetErrObj(rtn, results)
									return rtn	
								end
								
								return .SuccessResult(results)
								
							end

	
	override function Assoc GetSubNodes(Object prgCtx, \
														DAPINODE node)
								
								Assoc		rtn = .InitErrObj("GetSubNodes")
								
								
								String 		stmt = "SELECT DTree.DATAID, DTree.NAME from DTree, PhysItemCO WHERE DTree.DataID = PhysItemCO.NodeID AND PhysItemCO.BoxID=:A1"
								RecArray 	results
								Record		rec
								DAPINODE	subNode
								List		subnodes
								
								// *** query to get results
								results = .ExecSql(prgCtx, stmt, {node.pId})
								
								// *** loop through and get DAPINODE handles for each of these
								if IsNotError(results)
									for rec in results
										subNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.DATAID)
										if IsNotError(node)
											subNodes = {@subNodes, subNode}
										end			
									end
								else
									.SetErrObj(rtn, results)
									return rtn	
								end
								
								return .SuccessResult(subNodes)
								
							end

end

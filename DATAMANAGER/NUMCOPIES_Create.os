package DATAMANAGER

public object NUMCOPIES_Create inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'NumberOfCopies'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$NUMCOPIES'

end

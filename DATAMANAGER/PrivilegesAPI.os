package DATAMANAGER

public object PrivilegesAPI inherits DATAMANAGER::CrudKiniAPI

	override	String	_fIdFieldName = 'userName'


	
	override function Assoc _DbToUI(Record rec, Integer tzOffset = 0)
											
												String  s = rec.INIVALUE
											
											   	Assoc vals
											    vals.("canrunasync") = s[1] == "1" ? "true" : "false"
											    vals.("canmanagejobs") = s[2] == "1" ? "true" : "false"
											    vals.("canmanagepaths") = s[3] == "1" ? "true" : "false"
											   	vals.("canmanageprivileges") = s[4] == "1" ? "true" : "false"
											    vals.("canmanageprofiles") = s[5] == "1" ? "true" : "false"
												vals.("canrunsync") = s[6] == "1" ? "true" : "false"
												vals.("candousers") = s[7] == "1" ? "true" : "false"
												
												vals.("userName") = rec.INIKEYWORD
												
												return vals
											 
											end

	
	override function Assoc _Put(Object prgCtx, \
																Dynamic key, \
																Dynamic value, \
																Boolean update)
																		
												Assoc 	rtn = .InitErrObj( "_Put" )
												
												
												// *** do the ui to db
												value = ._UIToDb(prgCtx, value).result
											
												Dynamic result = CAPI.IniPut( prgCtx.fDBConnect.fLogin, .fIniSection, key, value ) 
												
												if IsError(result)
													.SetErrObj(rtn, result)
												else
													rtn = .SuccessResult(result)	
												end	
											
												return rtn							
											 
											end

	
	override function Assoc _SubclassValidate(Object prgCtx, \
																			 Assoc data)
																			 
												Assoc 	rtn = .InitErrObj( "_SubclassValidate" )
												String	idName = ._fIdFieldName
												
												// *** allow for a new profile here
												if IsDefined(data.(idName)) && Length(data.(idName))
												
													// do nothing	
												else
													.SetErrObj(rtn, "Plese enter/select a user name")
													return rtn			
												end
												
											
												
												return rtn
											
											end

	
	override function Assoc _UIToDb(Object prgCtx, \
																	Dynamic vals, \
																	Integer tzOffset=0)
											
											  	String	s
											   	
											   	
											    s += vals.("canrunasync") == "true" ? "1" : "0"
											    s += vals.("canmanagejobs") == "true" ? "1" : "0"
											    s += vals.("canmanagepaths") == "true" ? "1" : "0"
											    s += vals.("canmanageprivileges") == "true" ? "1" : "0"
											    s += vals.("canmanageprofiles") == "true" ? "1" : "0"
											    s += vals.("canrunsync") == "true" ? "1" : "0"
											    s += vals.("candousers") == "true" ? "1" : "0"
												
										
												Assoc rtn
												rtn.result = s
												
												return rtn
											 
											end

end

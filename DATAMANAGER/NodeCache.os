package DATAMANAGER

public object NodeCache inherits DATAMANAGER::InstanceObjects

	public		Dynamic	_fCache
	override	Boolean	fEnabled = TRUE


	script GetNodeFromPath
	
			
				// Patched by Pat201812180
				function Assoc GetNodeFromPath(Object prgCtx, \
												String fullPath, \
												Dynamic rootNames = undefined, \
												Boolean getVolume=TRUE)
												
					// *** KM allow callers to pass in a possible list of roots
					if IsDefined(rootNames) 
						if Type(rootNames) != ListType
							rootNames = {Str.String(rootNames)}		
						end
					else
						rootNames = {undefined}		
					end
					
					Assoc status
					String rootName
					for rootName in rootNames
						status = _GetNodeFromPath(prgCtx, fullPath, rootName, getVolume)
						if status.ok
							return status
						end
					end
					
					// *** if we got here, we didn't find it
					return status
				
				end	
												
				function Assoc _GetNodeFromPath(Object prgCtx, \
												String fullPath, \
												String rootName = undefined, \
												Boolean getVolume=TRUE)
					Assoc 		status
					DAPINODE	node, volNode
					String		path
					
					// *** grab from cache if we can
					if IsDefined(._fCache)
						// 11/12/18 fixed to cache only dataID instead of DAPINODE. Must retrieve DAPINODE fresh instead of returning DAPINODE
						Integer dataID = ._fCache.(Str.Upper(fullPath))
				
						if IsDefined(dataID)
							node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, dataID)
							if IsNotError(node)
								return Assoc{'ok': true, 'result': node}
							end	
						end	
					end
					
					// *** start from a uniform format  - format the path to remove leading and trailing colons
					fullPath = ._FormatNodePath(fullPath)
					
					// *** try to get the root node
					status = $DataManager.RootNodes.GetRootNode(prgCtx, fullPath, rootName)
					if !status.ok
						return.SetErrObj(.InitErrObj( "GetNodeFromPath" ), status)
					else
						Integer 	rootIndex = status.result.rootIndex	
						node = status.result.node
						List 		pathParts = Str.Elements(fullPath, ":")
						Boolean		containsDash = "-" in pathParts	> 0	// if we have a dash, that means there is a volume in there
						
						if Length(pathParts)
							pathParts = Str.Elements(fullPath, ":")[rootIndex+1:]
							for path in pathParts
								
								// *** dash indicates volume
								if path == "-"
									// *** get the volume node
									volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
									if IsError(volNode)		
										return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get volume node for node %1 [dataid %2]", node.pId, node.pName))
									else
										node = volNode			
									end				
								
								else
							
									// *** now get the node with that path inside the root node
									node = .DAPIGetNode(prgCtx, node, path)
									if IsError(node)
										return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get node from path '%1'", path))
				
									// *** get the volume automatically, but only if there's no dash to indicate so (with dash, we get volume in the code block above)
									// *** KM 11/29/16 only get the volume if the calling function indicates so
									
									elseif !containsDash && getVolume
										Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)	
										if llNode.fAssociatedVolume != 0
											// *** get the volume node
											volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
											if IsNotError(volNode)	
												node = volNode
											elseif !llNode.fAssociatedVolumeOptional	
												return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get volume node for node %1 [dataid %2]", node.pId, node.pName))
											end
										end				
													
									end
								end	
									
							end
						end
					end
					
					Assoc rtn
					rtn.ok = true
					rtn.result = node
					
					// 11/12/18 fixed to cache only dataID instead of DAPINODE
					if IsDefined(._fCache)
						._fCache.(Str.Upper(fullPath))  = node.pID
					end
				
					return rtn	
					
				end
			
		
	
	endscript

	
	public function Object New( )
									
			Frame f = ._NewFrame()
			
			f._fCache = Assoc.CreateAssoc()
			
			return f
			
		end

end

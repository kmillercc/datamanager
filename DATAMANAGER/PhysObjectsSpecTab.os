package DATAMANAGER

public object PhysObjectsSpecTab inherits DATAMANAGER::PhysObj

	override	String	fApiKey3 = 'request'


	
	public function __SetKeys()
						
							Assoc poInfoMap = Assoc.CreateAssoc()
							poInfoMap.('$ClientID')	= 'Client_ID'		// a KUAF ID
							poInfoMap.('$ClientName')	= 'Client_Name'	// is a client name from the physobjclient table
							poInfoMap.('$NumCopies')		=	'poNumCopies'
							poInfoMap.('$FromDate')	=	'poFromDate'
							poInfoMap.('$Keywords')	=	'poKeywords'
							poInfoMap.('$HomeLocation')	=	'poLocation'
							poInfoMap.('$Location')	=	'poLocation'	
							poInfoMap.('$LocatorType')	=	'LocatorType'
							poInfoMap.('$OffsiteStorID')	= 	'OffsiteStorID'
							poInfoMap.('$SelectedMedia') = 'selectedMedia'
							poInfoMap.('$PhysItemType') = 'selectedMedia'
							poInfoMap.('$ReferenceRate') =	'RefRate'
							poInfoMap.('$ToDate')		=	'poToDate'
							poInfoMap.('$UniqueID')	=	'pouniqueID'
							poInfoMap.('$TemporaryID') = 'TemporaryID'
							poInfoMap.('$NumCopies') = 'poNumCopies'
							poInfoMap.('$BoxId')	= 'boxID'
							poInfoMap.('$Locator') = 'Locator'
							poInfoMap.('$Area') = 'Area'
							poInfoMap.('$Facility') = 'Facility'
							poInfoMap.('$TransferID') = 'TransferID'
						
						
							Object child
							
							for child in OS.Children(this)
								String key = "$" + child.OSName
							
								if IsFeature(poInfoMap, key)
									child.fApiKey2 = poInfoMap.(key)
								end	
							end
								
							
						
						
						end

end

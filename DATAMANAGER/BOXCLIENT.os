package DATAMANAGER

public object BOXCLIENT inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'pseudoColumn'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$BOXCLIENT'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							// *** which column name is this supposed to be?
							String colName
							
							if Type(val) == StringType
							
								if (._CheckUser(prgCtx, val)).ok
									colName = "$CLIENTID"
								else
									colName = "$CLIENTNAME"
								end	
							
							elseif Type(val) == IntegerType
							
								colName = "$CLIENTID"
							
							else
								return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("'%1' needs to be either an integer or string representing a Content Server User or Phys Obj Client", val))
							end
							
							
							Assoc rtn
							rtn.useColName = colName 
							rtn.ok = true
							return rtn
												
						end

end

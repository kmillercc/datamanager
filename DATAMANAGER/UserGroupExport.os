package DATAMANAGER

public object UserGroupExport inherits DATAMANAGER::ExportAPIProcesses

	override	Boolean	fEnabled = TRUE
	override	String	fMetadataFileName = 'userdata'
	override	String	fName = 'UserGroupExport'
	override	String	fSubKey = 'UserGroup.Export'


	override script GetSourceRoot
	
			
					
							
									
											
													
												
														
															function Assoc _GetSourceRoot(Object prgCtx, Assoc options)
															
																Assoc 	rtn = .InitErrObj("_GetSourceRoot")
																Dynamic root
																String 	path
																Integer id
															
																	
																if IsUndefined(options.rootSourceId) || IsUndefined(options.rootSourcePath)
																	.SetErrObj(rtn, "Could not determine root group for export")
																	return rtn	
																else
																	id = options.rootSourceId
																	path = options.rootSourcePath		
																end
																
																Assoc result
																result.root = root
																result.rootSourcePath = path
																result.rootFilePath = path
																result.id = id	
																
																return .SuccessResult(result)
															end	
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Assoc NewJobPayload(Object prgCtx, Assoc options, String action)
						
							// *** Initialize payload Assoc
							Assoc payload
							
							// *** constant that's hardcoded for now
							options.metadataFileName = 	.fMetadataFileName
							
							// *** the counter we use to set unique names when we truncate name length
							payload.nameTruncNumber = 0
							
							// *** if we have name collision with selected subfolder, do we delete?
							options.deleteSub = IsFeature(options, "deleteSub") && options.deleteSub
							
							// *** Gather options from the form
							options.delimiter = IsDefined(options.delimiter) && Length(options.delimiter) ? options.delimiter : ","
							options.numMetadataFiles = IsFeature(options, 'numMetadataFiles') ? options.numMetadataFiles : 1
							options.includeRoot = IsFeature(options, "includeRoot") && options.includeRoot	
							
							// *** ug options
							options.includeUsers = IsFeature(options, "includeUsers") && options.includeUsers  
							options.includeGroups = IsFeature(options, "includeGroups") && options.includeGroups 	
							
							// *** recurse and get all nodes?
							options.recurse = options.numSubLevels == "All"
						
							// *** do we have colNames?
							options.colNames = IsDefined(options.colNames) ? options.colNames : {}	
							
							// *** put the options in 
							payload.options = options	
							
							return payload
						
						end

end

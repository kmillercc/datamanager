package DATAMANAGER

public object VERMODIFIEDDATE inherits DATAMANAGER::Versions

	override	String	fApiKey3 = 'pModifyDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERMODIFIEDDATE'

end

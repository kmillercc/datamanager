package DATAMANAGER

public object MIDDLENAME inherits DATAMANAGER::UserGroup

	override	String	fApiKey1 = 'MIDDLENAME'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$MIDDLENAME'

end

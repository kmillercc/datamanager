package DATAMANAGER

public object NICKNAME inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'metadata'
	override	String	fApiKey2 = 'NickName'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$NICKNAME'

end

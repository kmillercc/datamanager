package DATAMANAGER

public object Configure inherits WEBADMIN::AdminRequestHandler::Configure

	override	Boolean	fEnabled = TRUE
	override	String	fFuncPrefix = 'datamanager'
	override	Boolean	fHasDbSchema = TRUE


	
	override function Assoc DoCustomStep( Record r )
							
							Assoc	retVal
						
							
							// *** try to get admin ctx
							Assoc status = $Syntergycore.Utils.GetAdminCtx()
							if status.ok
								Object prgCtx = status.prgCtx
								
								// *** Upgrade bulk tools jobs from SYNTBT_JOB_SCHEDULES table
								$DataManager.DropoffConverter.UpgradeBulkToolsJobs(prgCtx)
								
							end	
							
							retVal.OK = TRUE
							
							return retVal
						end

end

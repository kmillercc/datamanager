package DATAMANAGER

public object ACTION inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'action'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$ACTION'


	
	override function Assoc ValidateVal(Object prgCtx, \
												Dynamic val, \
												String action, \
												Frame apiDataObj)
						
				List validActions = {"CREATE",\
									"ADDVERSION",\
									"MOVE",\
									"COPY",\
									"PURGE",\
									"DELETE",\
									"UPDATE"}
		
				if (Type(val) !=  StringType)
					return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("Expected String type for $Action value. Instead %1 specified.", val))
				end
				
				if !(val in validActions)
					return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("Invalid action %1 specified.", val))
				end
		
				Assoc rtn
				rtn.result = Str.Upper(val)
				rtn.ok = true
				return rtn
									
			end

end

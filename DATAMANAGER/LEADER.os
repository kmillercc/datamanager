package DATAMANAGER

public object LEADER inherits DATAMANAGER::LEADERID

	override	String	fApiKey1 = 'LEADERID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$LEADER'

end

package DATAMANAGER

public object #'DataManager LLRequestHandlers'# inherits SYNTERGYCORE::#'Syntergy LLRequestHandler'#

	override	Boolean	fEnabled = FALSE
	override	String	fFuncPrefix = 'datamanager'


	
	override function Dynamic Execute( Dynamic ctxIn, Dynamic ctxOut, Record r )
											
												Object uSession = .fPrgCtx.USession()
											
												Dynamic userInfo = UAPI.GetByID( uSession.fSession, uSession.fUserID )
												
												if Length( userInfo ) > 0
													$SyntergyCore.LogPkg.NDC.Push( userInfo[1].NAME + " (" + Str.String( userInfo[1].ID ) + ")" )
												end
												
												String profileName = Str.Format( "%1.%2", .fBaseProfileName, .fName )
												
												$SyntergyCore.Stats.Start( "datamanager:request", profileName )
												
												Dynamic rtn = .DoExec( ctxIn, ctxOut, r )
												
												$SyntergyCore.LogPkg.NDC.Clear()	
												
												$SyntergyCore.Stats.Stop( "datamanager:request", profileName )
													
												return rtn	
											end

	
	private function Assoc _ParseOptions(String s, Boolean doConversion = true, Integer tzOffset = 0)
											
												// *** return val
												Assoc a
											
												// *** parse into elements
												List	elList = Str.Elements(s, "&")
											
												String  el
												List	nameValList
												Dynamic	val
												String	name
												List	listKeys 	= {"catIds", "specNodeTypes"}
												List 	stringParams = {"subTargetPath"}
												
												for el in elList
													// *** unescape
													el = Web.Unescape(el)
													
													nameValList = Str.Elements(el, "=")
													name = nameValList[1]
														
													if Length(nameValList) == 2
														val	= nameValList[2] == "" ? undefined : nameValList[2] 
											
														// *** look for dates, true false values and numbers
														Boolean isLikelyDate = IsDefined(Str.LocateI(nameValList[1], 'date'))
														
														
														// *** clear out any escaped ampersands
														if IsDefined(Str.LocateI(name, "path"))
															val = Str.ReplaceAll(val, ";amp;", "&")
														end	
											
														// *** convert values			
														if IsDefined(val) && doConversion && (name in stringParams == 0)
															Dynamic newVal = $DataManager.FormatPkg.ConvertStringToVal(val, undefined, isLikelyDate, true, tzOffset)
															
															if IsDefined(newVal)
																val = newVal
															end
														end	
														
														if Assoc.IsKey(a, name) && (name in listKeys)
															// *** must mean we have a list of values
															List vals = Type(a.(name)) == ListType ? a.(name) : {a.(name)} 
															vals = {@vals, val} 
															a.(name) = vals
														else
															a.(name) = val
														end	
														
													elseif Length(nameValList) == 1
														a.(nameValList[1])	= undefined
													end	
												end
											
												return a
											
											end

end

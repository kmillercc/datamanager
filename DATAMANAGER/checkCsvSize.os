package DATAMANAGER

public object checkCsvSize inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'filepath', -1, 'FilePath', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
							 * 	filepath 		=> String 	=> the filepath we are validating
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.FileUtils.CheckPathExists(r.filepath, 'file')
							if status.ok
								
								Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)	
								
								// *** hand off to the API
								status = $DataManager.CsvParser.CheckCsvSize(r.filepath, limit)
								if status.ok
									.fContent = synCore.SuccessResult(status.result)
								else
									
									// *** tweak the status errMsg
									._FormatStatusErrMsg(status)
								
									// for debugging .
									synCore.SetErrObj( rtn, status )
								
									.fError = rtn
								end		
							
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "filepath", StringType, "FilePath", FALSE } \
								}
						end

	
	private function void _FormatStatusErrMsg(Assoc status)
						
							/*
								The number of columns to be processed (3) must match the number of CellProcessors (4): check that the number of CellProcessors you have defined matches the expected number of columns being read/written
							*/
							
							if IsDefined(Str.Locate(status.errMsg, "The number of columns to be processed"))
								status.errMsg = Str.ReplaceAll(status.errMsg, "CellProcessors", "Header Columns")
								status.errMsg = Str.Replace(status.errMsg, "read/written", "read.")
								
							end	
							
						end

end

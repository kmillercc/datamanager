package DATAMANAGER

public object getGroupTree inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'groupId', 2, 'groupId', FALSE }, { 'path', -1, 'path', FALSE }, { 'showUsers', 5, 'showUsers', TRUE, TRUE }, { 'doRecursive', 5, 'DoRecursive', TRUE, FALSE }, { 'returnDirect', 5, 'ReturnDirect', TRUE, FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
							 * 	groupId 		=> Integer 	=> the id of the group we are trying to retrieve
						 	 * 	path 		=> String 	=> (optional) the path of the group	 
						 	 *  showUsers 		=> Boolean	=> showUsers
						  	 *  doRecursive 	=> Boolean	=> recurse into node and get all sub nodes
						 	 *  returnDirect		=> Boolean	=> return the data directly with no standard OK assoc (i.e. for lazy loading)
						     **/
						
							Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)	
						
							// *** hand off to the API
							Assoc status = $DataManager.UiSupportAPI.GetGroupTree(.fPrgCtx, r.groupId, limit, r.path, r.showUsers, r.doRecursive)
							if status.ok
								if r.returnDirect
									.fContent = status.result.children
								else
									.fContent = synCore.SuccessResult(status.result)	
								end	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "groupId", IntegerType, "groupId", FALSE}, \
									{ "path", StringType, "path", FALSE }, \			
									{ "showUsers", BooleanType, "showUsers", TRUE, TRUE }, \			
									{ "doRecursive", BooleanType, "DoRecursive", TRUE, FALSE }, \	
									{ "returnDirect", BooleanType, "ReturnDirect", TRUE, FALSE } \			
								}
						end

end

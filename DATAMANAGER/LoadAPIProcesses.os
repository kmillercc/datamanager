package DATAMANAGER

public object LoadAPIProcesses inherits DATAMANAGER::ApiProcesses

	public		String	_fValidateResultsKey = 'validateResults'


	override script GetSourceRoot
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											function Assoc _GetSourceRoot(Object prgCtx, Assoc options)
																											
																												
																												Assoc result
																												result.root = undefined
																												result.rootSourcePath = options.rootSourcePath
																												result.rootFilePath = options.rootFilePath
																												result.id = undefined	
																												
																												return .SuccessResult(result)
																											end	
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function GetTargetRoot(Object prgCtx, Assoc options)
																				
													
													Assoc result
													result.path = options.rootTargetPath
													result.id = undefined
													
													return .SuccessResult(result)	
													
												end

	override script ValidateJob
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																								function Assoc ValidateJob(Object prgCtx, \
																															List colNames, \
																															String action, \
																															Assoc options, \
																															String rootSourcePath, \
																															String rootTargetPath, \
																															Integer limit, \
																															Assoc sourceSettings)
																									
																									// *** check to make sure the root path exists
																									Assoc status = $DataManager.FileUtils.CheckPathExists(rootSourcePath)
																									if !status.ok
																										return .SetErrObj(.InitErrObj("LoadAPIProcesses.ValidateJob"), status)
																									end			
																									
																									if Length(colNames)
																									
																										List hiddenCols = IsDefined(sourceSettings) ? sourceSettings.hiddenCols : {}
																									
																										// *** validate the csv columns if we have them
																										status = _ValidateCsvColumns(action, colNames, hiddenCols)
																										if !status.ok
																											return .SetErrObj(.InitErrObj("LoadAPIProcesses.ValidateJob"), status)
																										end	
																									end
																									
																									Assoc rtn
																									rtn.ok = true					
																									return rtn
																								
																								end
																								
																								function Assoc _ValidateCsvColumns(String action,\
																																	List inColNames, \
																																	List hiddenCols)
																								
																									List colNames
																									String col
																								
																									// *** capitalize the column names
																									Integer i
																									for i = 1 to Length(inColNames)
																										col = Str.Upper(Str.Trim(inColNames[i]))	
																										if (col in hiddenCols == 0)
																											colNames = {@colNames, col}
																										end	
																									end
																								
																									
																									Assoc	rtn = .InitErrObj("ValidateCsvColumns")
																									
																									SWITCH Str.Upper(action)
																									
																										case "CREATE"
																											if ("$TARGETPATH" in colNames == 0) && ("$PARENTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $ParentId column in your CSV for the Create action.')
																												return rtn			
																											end
																										end
																										
																										case "ADDVERSION"
																								
																											if ("$TARGETPATH" in colNames == 0) && ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $ObjectId column in your CSV for the Add Version action.')
																												return rtn			
																											end
																										end
																										
																										case "UPDATE"
																								
																											if ("$TARGETPATH" in colNames == 0) && ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $ObjectId column in your CSV for the Update action.')
																												return rtn			
																											end		
																										end
																									
																										case "MOVE"
																											if ("$TARGETPATH" in colNames == 0) && ("$NEWPARENTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $NewParentId column in your CSV to specifiy the new location for the Move action.')
																												return rtn			
																											end	
																											if ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need an $ObjectId column in your CSV for the Move action.')
																												return rtn			
																											end					
																										end
																										
																										case "COPY"
																											if ("$TARGETPATH" in colNames == 0) && ("$NEWPARENTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $NewParentId column in your CSV to specifiy the new location for the Copy action.')
																												return rtn			
																											end	
																											if ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need an $ObjectId column in your CSV for the Copy action.')
																												return rtn			
																											end					
																										end		
																										
																										case "DELETE"
																											if ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need an $ObjectId column in your CSV for the Delete action.')
																												return rtn			
																											end					
																										end		
																										
																										
																										case "PURGE"
																											if ("$TARGETPATH" in colNames == 0) && ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $ObjectId column in your CSV for the Purge Versions action.')
																												return rtn			
																											end	
																										end				
																										
																										case "UPDATEPERMS"
																											if ("$TARGETPATH" in colNames == 0) && ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $ObjectId column in your CSV for the UpdatePerms action.')
																												return rtn			
																											end	
																											if ("$PERMISSIONS" in colNames == 0)
																												.SetErrObj(rtn, 'You need a $Permissions column in your CSV for the Update Permissions action.')
																												return rtn			
																											end				
																										end		
																										
																										case "RENAME"
																											if ("$TARGETPATH" in colNames == 0) && ("$OBJECTID" in colNames == 0)
																												.SetErrObj(rtn, 'You need either a $TargetPath column or $ObjectId column in your CSV for the UpdatePerms action.')
																												return rtn			
																											end	
																											if ("$NEWNAME" in colNames == 0)
																												.SetErrObj(rtn, 'You need a either a $NewName column in your CSV to for the rename action.')
																												return rtn			
																											end				
																										end		
																										
																										case "CREATEUSERGROUP", "UPDATEUSERGROUP"
																											if ("$NAME" in colNames == 0)
																												.SetErrObj(rtn, 'You need at least a "$NAME" column in your CSV to create a user/group')
																												return rtn			
																											end			
																											
																										end
																									
																									end
																								
																									
																									return rtn
																																		
																								end										
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function Assoc _GetCsvArchivePath(Assoc options)
				String	path
				Object 	fileUtils = $Datamanager.FileUtils
				if IsDefined(options.csvArchivePath) && Length(options.csvArchivePath)
					String rootPath = fileUtils.EnsureDirSeparator(options.csvArchivePath)
					Assoc status = fileUtils.EnsurePathExists(rootPath)
					if status.ok 
						status = fileUtils.CreateTimeSpecificDir(rootPath)
						if status.ok
							path = status.result
						else
							return .SetErrObj(.InitErrObj("_GetCsvArchivePath"), status)
						end	
					end	
				else
					path = options.auditFolderPath
				end
				Assoc rtn
				rtn.ok = true
				rtn.result = path
				return rtn
			end

	
	override function String _GetDefaultAuditPath(Assoc options)
												
													String path
												
													if IsDefined(options.loadDirPath)
													
														path = $DataManager.FileUtils.EnsureDirSeparator(options.loadDirPath) + "_audit" + File.Separator()
												
													else
														// **** need to do something here.  For now, log to the opentext logs folder
														path = File.StartupDir() + "logs" + File.Separator() + "datamanager" + File.Separator() 
													end
														
													return path			
												end

end

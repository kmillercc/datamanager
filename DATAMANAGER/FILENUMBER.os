package DATAMANAGER

public object FILENUMBER inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'fileNumber'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$FILENUMBER'
	override	String	fUpdateApiKey3 = 'fileNumber'

end

package DATAMANAGER

public object AbstractIterator inherits DATAMANAGER::FactoryObjects

	public		String	_fAction
	public		Dynamic	_fApiProcess
	public		Dynamic	_fAuditAPI
	public		Integer	_fChunkCount
	public		Dynamic	_fChunkCounter
	public		Integer	_fChunkSize
	public		Dynamic	_fChunks
	public		List	_fColNames
	public		Integer	_fCurrentIndex
	public		Dynamic	_fDataCache
	public		Integer	_fEstimatedTotal
	public		Date	_fExpireDate = 2014-03-03T10:13:40
	public		Dynamic	_fFileMetadataObj
	public		Boolean	_fForUI = TRUE
	public		Integer	_fIntOrder = 1
	public		Dynamic	_fJob
	public		String	_fLastParentPath
	public		Integer	_fLimit
	public		Integer	_fMaxAsyncChunkSize = 5
	public		Dynamic	_fMetadataGatherer
	public		Dynamic	_fNodeIDs
	public		Dynamic	_fObjectPaths
	public		Dynamic	_fOptions
	public		Dynamic	_fParentPaths
	public		Dynamic	_fPrgCtx
	public		Integer	_fRootSourceID
	public		String	_fRootSourcePath
	public		Integer	_fRootTargetID
	public		String	_fRootTargetPath
	public		String	_fSeparator
	public		Boolean	_fSortChunk = FALSE
	public		Integer	fCollectionCount
	public		List	fDataKeys
	public		Integer	fIterateTotal
	override	String	fKey = 'Iterator'
	public		Integer	fMaxID
	public		Integer	fMinID
	public		List	fNodeIdArray
	public		Dynamic	fNodeObjects
	public		Boolean	fRunSingleThreaded = FALSE


	
	public function Assoc Callback(Integer id, \
																				String path, \
																				Dynamic data, \
																				Boolean isRoot = false)
									return undefined
																
									// abstract.  All Callback methods must implement this signature.  Optional arguments can be added at the end if need be but must be defaulted.
								end

	
	public function Assoc Finalize()
														
															if ._fSortChunk
																.fNodeIdArray = ._SortArray(.fNodeIdArray)
															else
																List chunk
																Assoc chunks = ._fChunks
																
																for chunk in Assoc.Items(chunks)
																	.fNodeIdArray = {@.fNodeIdArray, chunk}	
																end 
																
															end		
															
															Assoc rtn
															rtn.ok = true
															return rtn
														
														end

	
	public function Assoc GetCachedData(Object prgCtx, Object dataCache, Dynamic key2, String  key1 = .fItemDataKey, Integer dataType = Assoc.AssocType, Integer listItemType = StringType)
																					
															Assoc 	rtn = .InitErrObj("GetCachedData")
															Assoc	csvData, apiVals
															
															// *** implemented in subclasses
														
														
															Assoc result
															result.apiVals = apiVals
															result.csvData = csvData
														
															rtn.result = result
															return rtn						
														end

	script InitSubclass
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																											
																													
																												
																														
																															function void Init(Assoc options, Assoc sourceSettings)
																																
																																// *** implemented in subclasses
																															
																															end
																														
																													
																												
																														
																													
																												
																											
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function Assoc Iterate(	Object prgCtx, Dynamic root, String path, Boolean isRoot = true, String filePath = "" )
															
									// all iterate methods must implement this signature 
									// abstract
									
									return undefined
								end

	
	public function Assoc IterateWrapper(	Object prgCtx, \
										Dynamic root = undefined, \
										String path = undefined, \
										Boolean doLogging = TRUE)
																
			Assoc  	rtn = .InitErrObj("IterateWrapper")
			Object 	logger = .GetLogger()
			Assoc	dataFlags
			Assoc	lineErrorMap
			Integer numErrors = 0
			List	filesProcessed
			String  timerName = Str.Format("IterateWrapper_%1", Date.Tick() )
		
			$SyntergyCore.Timer.Start( timerName )
		
			// *** call the real iteration func
			Assoc status = .Iterate(prgCtx, root, path)
			if !status.ok
				._Cleanup()
				.SetErrObj(rtn, status)
				return rtn
			else
				dataFlags = IsFeature(status.result, "dataFlags") ? status.result.dataFlags : dataFlags
				lineErrorMap = IsFeature(status.result, "lineErrorMap") ? status.result.lineErrorMap : lineErrorMap
				numErrors = IsFeature(status.result, "numErrors") ? status.result.numErrors : numErrors
				filesProcessed = IsFeature(status.result, "filesProcessed") ? status.result.filesProcessed : filesProcessed
			end	
		
			// *** finalize the node iterator in case we have any chunks that have not been added
			status = .Finalize()
			if !status.ok
				._Cleanup()
				.SetErrObj(rtn, status)
				return rtn
			end
			
			// *** finally save the job that we have
			._fJob.Save(prgCtx)
			
			// *** a note here: the .fIterateTotal is the total number of items processed by the iterator (for computing average time)
			// *** the .fCollectionCount is the total number of items that the UI or the Async Processor actually needs to to process
			// *** in other words, some items get filtered out and won't appear in the .fCollectionCount
		
			Real 	elapsed = $SyntergyCore.Timer.Stop( timerName )
			Real	average = .fIterateTotal > 0 ? elapsed/.fIterateTotal : elapsed
			
			if .fIterateTotal == 0
				logger.Warn("No items found for this job.")
			elseif doLogging 
				logger.Info("")
				logger.Info(Str.Format("Total Count = %1", .fIterateTotal))
				logger.Info(Str.Format("Number of Errors = %1", numErrors))		
				logger.Info(Str.Format("Total Time = %1", elapsed))
				logger.Info(Str.Format("Average Per Item %1", average))
			end	
			
			Assoc result
			result.nodeIdArray = .fNodeIdArray
			result.nodeObjects = .fNodeObjects
			result.itemsToProcess = .fCollectionCount
			result.itemsInitialized = .fIterateTotal
			result.average = average
			result.elapsed = elapsed
			result.minId = .fMinId
			result.maxId = .fMaxId
			result.lineErrorMap = lineErrorMap
			result.numErrors = numErrors
			result.filesProcessed = filesProcessed
			
			// *** set a flag for the browser to indicate whether it's allowed to divide up chunks
			result.allowSmallerChunks = !.fRunSingleThreaded
		
			// *** save the number of csv files (only really needed for LoadFromFileStructure)
			dataFlags.numCsvFiles = ._GetNumCsvFiles()
			result.dataFlags = dataFlags
			
			._Cleanup()
			
			return .SuccessResult(result)
			
										
		end

	script New
	
			
				
				// Patched by Pat201707190
				function Frame New( Object prgCtx, \
									Object job, \
									String action, \
									Integer limit, \
									Object dataCache, \
									Assoc options, \
									Integer rootSourceId, \
									String rootSourcePath, \
									Integer rootTargetId, \
									String rootTargetPath, \
									Integer numItems, \
									Assoc sourceSettings, \
									Object apiProcess)
																										
					Frame f = ._NewFrame()
																										
					f._fJob = job	
					f._fPrgCtx = prgCtx
					f._fAction = action
					f._fLimit = limit
					f._fDataCache = dataCache
					f._fOptions = options
					f._fApiProcess = apiProcess
																											
					f._fRootSourcePath = rootSourcePath
					f._fRootSourceId = rootSourceId
																											
					f._fRootTargetPath = rootTargetPath
					f._fRootTargetId = rootTargetId
																											
					f.fNodeIdArray = {}
					f.fNodeObjects = Assoc.CreateAssoc()
																											
					Object logger = .GetLogger()
																											
					// *** KM 6-20-16
					// *** do we have a flag in the options that says not to allow chunks (i.e. single-threaded)?
					f.fRunSingleThreaded = IsFeature(options, "runSingleThreaded") && options.runSingleThreaded
					logger.Debug(Str.Format("Initializing % Iterator.", .fSubKey))
										
					// helpful to know the total number of items
					f._fEstimatedTotal = numItems
					logger.Debug(Str.Format("Estimated Total is %1.", numItems))
																											
					// *** how to parse out our individual chunks?
					f._fChunkSize = _GetChunkSize(numItems, options)
					logger.Debug(Str.Format("Chunk Size is %1.", f._fChunkSize))
																											
					// *** set a max async chunk size
					f._fMaxAsyncChunkSize = $DataManager.ModulePrefs.GetPref("MAX_ASYNC_CHUNK_SIZE", 5)
																											
					// *** set up an assoc for our chunks
					f._fChunks = Assoc.CreateAssoc()
					f._fParentPaths = Assoc.CreateAssoc()
					f._fObjectPaths = Assoc.CreateAssoc()
					f._fChunkCounter = Assoc.CreateAssoc()
					f._fNodeIDs = Assoc.CreateAssoc()
																										
					// *** set min and max
					f.fMinId = 0
					f.fMaxId = 0
																											
					// *** give this object a chance to take in some other options
					f.InitSubclass(options, sourceSettings)
																													
					return f
																											
				end
																										
				function Integer _GetChunkSize(Integer numItems, Assoc options)
																										
					// *** minimum and maximum chunk size
					Integer minChunkSize
					Integer maxChunkSize
																											
					if options.whenOption == "async"
						minChunkSize = $DataManager.ModulePrefs.GetPref("MIN_ASYNC_CHUNK_SIZE", 2)
						maxChunkSize = ._fMaxAsyncChunkSize
					else
						minChunkSize = $DataManager.ModulePrefs.GetPref("MIN_SYNC_CHUNK_SIZE", 10)
						maxChunkSize = $DataManager.ModulePrefs.GetPref("MAX_SYNC_CHUNK_SIZE", 50)
					end	
																												
					Integer	chunkSize
																											
					if numItems > 0
						// *** by default we will try to create 10 chunkss
						chunkSize = Math.Max(1, numItems/10)
																												
						if chunkSize <= minChunkSize	
							chunkSize = minChunkSize		
																										
						elseif chunkSize >= maxChunkSize	
							chunkSize = maxChunkSize	
						end
																												
					else
																												
						// this just means that it's an async job and we don't have the count of the number of objects
																											
						chunkSize = maxChunkSize
					end
																											
																										
					return chunkSize
																										
				end
																									
						
					
				
			
		
	
	endscript

	
	private function void _0SetSubkey()
														
														end

	
	private function void _AddToCollection(Assoc nodeObj, Integer index, Dynamic data = undefined)
									// Patched by Pat201803140								
									
									// increment the collection count
									.fCollectionCount += 1
								
									// KM 3/14/18 avoid stack trace
									if IsUndefined(._fChunks.(index))
										._fChunks.(index) = {}
									end
							
									// Put the object into this chunk
									._fChunks.(index) = {@._fChunks.(index), nodeObj.id}
							
									// **** also add node Objects for the UI
									.fNodeObjects.(nodeObj.id) = nodeObj	
								end

	
	private function Assoc _CacheData(	List keys, \
																					Dynamic data, \
																					Integer groupIndex, \
																					Boolean update = false,\
																					Boolean isLong = false)
														
															Assoc rtn
															
															// *** call the data cache to insert/update data
															Assoc status = ._fDataCache.Put(._fPrgCtx, keys, data, update, isLong, groupIndex)
															if !status.ok
																rtn = .InitErrObj("_CacheData")
																.SetErrObj(rtn, status)
																return rtn	
															end
															
															rtn.ok = true
															return rtn
														
														end

	
	override function void _Cleanup()
															
															// *** cleanup any objects in the frame to avoid memory leaks
														
															String f
															
															for f in Frame.SlotNames(this)
																if Type(this.(f)) in {ObjectType, Frame.FrameType}
																	this.(f) = undefined
																end	
															end
															
														
															
														end

	
	private function Integer _GetChunkIndex(Assoc csvData, Object apiDataObj)
								
			Integer index = ._fCurrentIndex
			//Assoc 	parentPaths = ._fParentPaths
			Assoc 	objectPaths = ._fObjectPaths
			Assoc	nodeIDs = ._fNodeIDs		
			Integer chunkSize = ._fChunkSize
			Integer counter = .fIterateTotal
									
			// *** only 1 chunk for single-threaded loads
			if .fRunSingleThreaded
										
				index = ._fCurrentIndex = 1
											
			// *** we need to ensure that we group chunks by the parent folder path for create action 	
			elseif ._fAction == "CREATE" && IsDefined(apiDataObj)
								
				//String parentPath = apiDataObj.GetValue("parentPath")
				//String hashKey = ._HashKey(parentPath)	
				
				// KM fixed this for sync loads
				String hashKey	= apiDataObj.GetValue("hashedObjectPath")
									
				// *** have we encountered this path before? If so, use the index associated with the path
				if IsFeature(objectPaths, hashKey)
											
					index = objectPaths.(hashKey)
											
				// *** OK. We need to create a new chunk for this path
				else
					index = ._fCurrentIndex  = ._fCurrentIndex + 1	
					objectPaths.(hashKey) = index
														
				end	
			// *** we need to ensure that we group chunks by the nodeID for AddVersion 	
			elseif ._fAction == "ADDVERSION" && IsDefined(apiDataObj)
				Integer nodeID = apiDataObj.GetValue("nodeID")
				// *** have we encountered this path before? If so, use the index associated with the path
				if IsFeature(nodeIDs, nodeID)
					index = nodeIDs.(nodeID)
								
				// *** OK. We need to create a new chunk for this path
				else
					index = ._fCurrentIndex  = ._fCurrentIndex + 1	
					nodeIDs.(nodeID) = index
				end				
			elseif counter%chunkSize == 0 || ._fCurrentIndex == 0
									
				// new chunk
				index = ._fCurrentIndex  = ._fCurrentIndex + 1	
									
			end	
								
			return index
									
		end

	
	private function Integer _GetChunkIndexAsync(Assoc csvData, Object apiDataObj)
							
			Integer index = ._fCurrentIndex
			//Assoc 	parentPaths = ._fParentPaths
			Assoc 	objectPaths = ._fObjectPaths
			Assoc	nodeIDs = ._fNodeIDs
			Integer chunkSize = ._fChunkSize
			Integer counter = .fIterateTotal
			Integer maxChunkSize = ._fMaxAsyncChunkSize
							
			// *** if single threaded
			if .fRunSingleThreaded
			
				// *** increment the count
				._fChunkCount += 1
														
				// *** KM 7-19-17 what if the chunk is getting too large to be run by Distributed Agent Map Process, which will wrap the entire
				// *** chunk in a DB Transaction which may fill up the transaction logs?			
				if ._fChunkCount > maxChunkSize
	
					._fChunkCount = 1
					index = ._fCurrentIndex  = ._fCurrentIndex + 1	
							
				else
					index = ._fCurrentIndex
							
				end	
										
			// *** we need to ensure that we group chunks by the parent folder path for create action 		
			elseif ._fAction == "CREATE" && IsDefined(apiDataObj)
								
				//String parentPath = apiDataObj.GetValue("parentPath")
				//String hashKey = ._HashKey(parentPath)
				
				String hashKey	= apiDataObj.GetValue("hashedObjectPath")
									
				// *** have we encountered this path before? If so, use the index associated with the path
				if IsFeature(objectPaths, hashKey)
											
					index = objectPaths.(hashKey)
					
					Integer chunkCount = ._fChunkCounter.(index)
											
					// *** KM 7-19-17 what if the chunk is getting too large to be run by Distributed Agent Map Process, which will wrap the entire
					// *** chunk in a DB Transaction which may fill up the transaction logs?
					// *** if the chunk has gotten too big, we need to break it apart
					if chunkCount == maxChunkSize 
						index = ._fCurrentIndex  = ._fCurrentIndex + 1	
						._fChunkCounter.(index) = 1
						objectPaths.(hashKey) = index
					else
						._fChunkCounter.(index) += 1
					end
												
				// *** OK. We need to create a new chunk for this path
				else
					index = ._fCurrentIndex  = ._fCurrentIndex + 1	
					._fChunkCounter.(index) = 1
					objectPaths.(hashKey) = index
														
				end	
			// *** we need to ensure that we group chunks by the nodeID for AddVersion 		
			elseif ._fAction == "ADDVERSION" && IsDefined(apiDataObj)
				Integer nodeID = apiDataObj.GetValue("nodeID")
				// *** have we encountered this path before? If so, use the index associated with the path
				if IsFeature(nodeIDs, nodeID)
					index = nodeIDs.(nodeID)
					
					Integer chunkCount = ._fChunkCounter.(index)
					
					// *** KM 7-19-17 what if the chunk is getting too large to be run by Distributed Agent Map Process, which will wrap the entire
					// *** chunk in a DB Transaction which may fill up the transaction logs?
					// *** if the chunk has gotten too big, we need to break it apart
					if chunkCount == maxChunkSize 
						index = ._fCurrentIndex  = ._fCurrentIndex + 1
						._fChunkCounter.(index) = 1	
						nodeIDs.(nodeID) = index
					else
						._fChunkCounter.(index) += 1
					end
								
				// *** OK. We need to create a new chunk for this path
				else
					index = ._fCurrentIndex  = ._fCurrentIndex + 1	
					._fChunkCounter.(index) = 1
					nodeIDs.(nodeID) = index
				end
								
			elseif counter%chunkSize == 0 || ._fCurrentIndex == 0
									
				index = ._fCurrentIndex  = ._fCurrentIndex + 1	
										
			end	
								
			return index
							
			end

	
	private function Integer _GetNumCsvFiles()
															// *** default is 1
															return 1
														end

	
	private function Assoc _NewNodeObj(Integer id, \
																					String path = undefined)
														
															Assoc nodeObj
															nodeObj.id = id
															nodeObj.path = path
															nodeObj.skip =false
															nodeObj.ok = true
															nodeObj.errMsg = undefined
														
															
															return nodeObj
														
														end

	
	private function List _SortArray(List nodeIdArray)
														
															List	newArray
															List 	newChunk
															
															for newChunk in ._fChunks
															
																Boolean inserted = false	
																
																// *** Sort the chunk
																newChunk = List.Sort(newChunk)
																					
																// *** get the max ID from the last position
																Integer minNewVal = newChunk[1]
																Integer maxNewVal = newChunk[-1]
															
														
																List eachChunk
																Integer i
																	
																if Length(nodeIdArray)
																	for i = 1 to Length(nodeIdArray)
																		eachChunk = nodeIdArray[i]
																		
																		// *** if our min val is greater than the max val, then keep going 
																		if minNewVal > eachChunk[-1]
																		
																			// keep going
																			newArray = {@newArray, eachChunk}
																	
																		// *** if our max val is less than the min val of this chunk, then we need to insert	
																		elseif !inserted && maxNewVal  < eachChunk[1]
																		
																			// insert
																			inserted = true
																			newArray = {@newArray, newChunk}
																			newArray = {@newArray, eachChunk}
																		
																		else
																		
																			// *** just put the chunk in	
																			newArray = {@newArray, eachChunk}
																		
																		end		
																	
																	end		
																else
																
																	// *** just put the chunk in	
																	newArray = {@newArray, newChunk}
																		
																end
															end
														
															
															return newArray
																					
								end

	
	override function void _SubclassDestructor()
															
															._fPrgCtx = undefined
															._fJob = undefined
															._fDataCache = undefined
															._fApiProcess = undefined
															
														end

end

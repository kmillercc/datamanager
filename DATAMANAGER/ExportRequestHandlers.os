package DATAMANAGER

public object ExportRequestHandlers inherits DATAMANAGER::JSONEnabled

	public		String	_fJobType = 'BulkExporter.Export'
	override	Boolean	fEnabled = FALSE
	override	String	fFuncPrefix = 'datamanager'

end

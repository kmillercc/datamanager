package DATAMANAGER

public object getJobStatusHtml inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'scheduleId', 2, 'scheduleId', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 *  scheduleId			=> Integer	=> scheduleId
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.UISupportAPI.GetJobStatusHtml(prgCtx, r.scheduleId)
							if status.ok
								
								.fContent = synCore.SuccessResult(status.result)
							
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "scheduleId", IntegerType, "scheduleId", FALSE } \
								}
						end

end

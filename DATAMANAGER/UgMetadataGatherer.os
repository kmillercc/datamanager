package DATAMANAGER

public object UgMetadataGatherer inherits DATAMANAGER::ExportObjects

	
	public function Assoc GatherMetadata(	Object prgCtx, \
																			Record ugRec, \
																			Boolean includeMembership)
																			
												Assoc status
												String col
											
												// *** colNames
												List colNames = ._fColNames
												
												// *** initialize the csvVals assoc
												Assoc csvVals = Assoc.Copy(._fEmptyVals)
											
												// *** first convert the record to an assoc that we will use as the base for our metadata
												Assoc ugData = Assoc.FromRecord(ugRec)	
												
												for col in colNames
													String colNoDollar = col[2:]
													if IsDefined(ugData.(colNoDollar))
														csvVals.(col) = ugData.(colNoDollar)	
													end
												end	
											
											
												// *** variables that we need to obtain
											
												if ugRec.TYPE == UAPI.GROUP && includeMembership
													
													status = $DataManager.UserUtils.GetGroupMembers(prgCtx, ugRec.ID)
													if !status.ok
														return .SetErrObj(.InitErrObj("GatherMetadata"), status)
													else
														// *** build a list of the user/group membership
														Record rec
														String s
														for rec in status.result
															if rec.TYPE == UAPI.USER 
																s += "U" + rec.NAME + ","
															elseif rec.TYPE == UAPI.GROUP
																s += "G" + rec.NAME + ","
															end
														end
														csvVals.("$MEMBERS") = Str.Quote(s[1:-2], '"')
													end
												end
												
												return .SuccessResult(csvVals)	
											
											end

	
	public function Dynamic New(	Object prgCtx, \
																	Assoc options, \
																	String filePath, \
																	List colNames = {}, \
																	Boolean convertToString = true, \
																	String outputType = 'csv')	// csv or xml (only csv supported right now)
											
												Frame 	f = ._NewFrame()
											
												f._fOutputType = outputType
												f._fFilePath = filePath
												f._fConvertToString = convertToString
																	
												// *** stuff we get from options
												f._fConstantVals = options.constantVals	
												f._fColNames = colNames
												
												// *** initialize a dummy assoc from ._fColNames
												f._InitEmptyVals()
												
												return f
												
											end

	
	public function Assoc WriteMetadata(	Object prgCtx, \
																			Record ugRec, \
																			Boolean includeMembership)
																			
												Assoc status
												Assoc	csvVals
												Boolean isStart = false
											
												if IsUndefined(._fFileWriter)
													._fFileWriter = $DataManager.FileWriter.New("json", ._fFilePath)
													
													// *** start the file with an opening
													status = ._fFileWriter.StartFile()
													if !status.ok
														return .SetErrObj(.InitErrObj("WriteMetadata"), status)
													end
													isStart	 = true	
												end	
											
												// *** get the metadata
												status = .GatherMetadata(prgCtx, ugRec, includeMembership)
												if !status.ok
													return .SetErrObj(.InitErrObj("WriteMetadata"), status)
												else
													csvVals = status.result		
												end	
												
												// *** a note of explanation: the Java library we are using to transform the json to csv gathers the column
												// *** names from the first row of json data. So, we need to have all column names in the first row 
												if isStart
													
													._fFirstRowData = csvVals
												
												else	
													String jsonStr = ._fFirstRowWritten ? "," : "" 	
													jsonStr += $DataManager.SynCoreInterface.toJSON(csvVals)
													._fFirstRowWritten = true
													._fFileWriter.Write(jsonStr)			
												end
											
												Assoc 	rtn
												rtn.ok = true
												return rtn	
											end

end

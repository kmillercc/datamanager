package DATAMANAGER

public object UserGroup inherits DATAMANAGER::ColumnValidators

	
	override function Void __Init()
						
							if .fEnabled
								// **** set up a map to this object in the NodeValidateWrapper
								Object wrapper = $DataManager.ValidateWrapper
								if IsUndefined(wrapper._fUGColValidatorMap)
									wrapper._fUGColValidatorMap = Assoc.CreateAssoc()
								end
								wrapper._fUGColValidatorMap.(.fKey) = this	
						
							
								// *** set up the keys for the Api Data Obj
								Object apidataObj = $DataManager.ApiDataObj
								if IsUndefined(apidataObj._fUGApiKeysMap)
									apidataObj._fUGApiKeysMap = Assoc.CreateAssoc()
								end
								apidataObj._fUGApiKeysMap.(.fKey) = {.fApiKey1, .fApiKey2, .fApiKey3}
						
						
							end
						
						end

	
	public function __SetKeys()
						
							Object child
							
							for child in OS.Children(this)
								child.fApiKey1 = child.fKey[2:]
							end
								
							
						
						
						end

end

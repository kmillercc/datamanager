package DATAMANAGER

public object FileAsync inherits DATAMANAGER::CORE::FileIterator

	override	Boolean	fEnabled = TRUE
	override	String	fSubKey = 'File_Async'


	
	override function Void _SubclassCallback(Integer id, \
														String path, \
														Dynamic data, \
														Integer chunkIndex = 1)							
						
							// *** the only difference between this and the Sync Iterator is that we don't create an object to send back to the UI in chunks (Distributed Agent will handle this)
							// *** but we still cache the data	
						
							// *** increment the collection count
							.fCollectionCount += 1
							 
						end

end

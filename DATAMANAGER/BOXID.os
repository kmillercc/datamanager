package DATAMANAGER

public object BOXID inherits DATAMANAGER::PhysObjBox

	override	String	fApiKey4 = 'boxID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$BOXID'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckNodeReference(prgCtx, val)
							
							if !status.ok
								
								return .SetErrObj(.InitErrObj("ValidateSetVal"), status)
								
							elseif status.result.pSubtype != $TypePhysicalItemBox
								
								return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format(Str.Format("'%1' does not point to a valid box in this system", val)))
						
							else
						
								val = status.result.pId
						
							end	
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

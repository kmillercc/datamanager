package DATAMANAGER

public object ExportPathsAPI inherits DATAMANAGER::PathsAPI

	override	String	fIniSection = 'SyntBulkExporter.ExportPaths'


	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg	
							
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_EXPORT_MNG_PATHS)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end		
						
							return rtn
						
						end

end

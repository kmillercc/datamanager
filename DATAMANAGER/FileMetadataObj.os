package DATAMANAGER

public object FileMetadataObj inherits DATAMANAGER::LoadObjects

	public		Dynamic	_fCsvCache
	public		Integer	_fNumCsvFiles
	override	Boolean	fEnabled = TRUE


	script GetMetadataForFile
	
			
					
							
									
																								
												function Assoc GetMetadataForFile(	String path, \
																					Integer limit, \
																					String rootPath)
																					
												
													Assoc 	rtn = .InitErrObj( "GetMetadataForFile" )
													Assoc	status
													String	parentPath = $DataManager.FileUtils.GetParentPath(path)
													Assoc	result
													
													// *** if it's a CSV, open the CSV
													if Str.Upper(path[-4:-1]) == ".CSV"
														status = ._OpenCSV(path, limit)
														if !status.ok
															.SetErrObj(rtn, status)
															return rtn
														else
															result = status.result
														end
													else
														Assoc 	csvVals
														
														// *** get the fileName
														String 	fileName = File.GetName(path)
														fileName = fileName[-1] == File.Separator() ? fileName[:-2] : fileName
														
														// *** get the sourcePath
														
														// *** get the csv from this dir
														status = _GetCSVFromDir(rootPath, limit)
														if !status.ok
															// *** not in this folder.  Check root folder.
															status = _GetCSVFromDir(parentPath, limit)
															if !status.ok
																.SetErrObj(rtn, status)
																return rtn
															else
																csvVals = status.result
															end			
														else
															csvVals = status.result
														end
														
														// *** try to find the metadata for this specific file
														status = ._GetCSVLine(csvVals, {"$FileName","$SourcePath"}, fileName, rootPath, path)
														if !status.ok
															.SetErrObj(rtn, status)
															return rtn
														else
															result = status.result
														end	
													end	
												
													return .SuccessResult(result)	
												end
												
												function _GetCSVFromDir(String parentPath, \
																		Integer limit)
												
													Assoc 	rtn = .InitErrObj( "_GetCSVFromDir" )
													Assoc	status
													Assoc	csvVals = undefined
													String  filePath
													String	key = ._HashKey(parentPath)
													Assoc 	csvCache = ._fCsvCache
													
													// *** first get the csv vals (from cache if possible)
													if IsFeature(csvCache, key)
														return csvCache.(key)
													
													else
														// *** have to loop through and look for csv her
														for filePath in File.FileList( parentPath )
															if Str.Upper(File.GetName(filePath)[-3:-1]) == "CSV"
																status = ._OpenCSV(filePath, limit)
																if status.ok
																	csvVals = status.result
																else
																	// *** cache even the error so we don't have to check again
																	.SetErrObj(rtn, status)
																	csvCache.(key) = rtn
																	return rtn
																end	
															end		
														end
														
														
													end
													
													// *** set error if we did not find it
													if IsUndefined(csvVals)
														// set the error	
														.SetErrObj(rtn, Str.Format("Could not find csv file in path %1", parentPath))
													else
														rtn = .SuccessResult(csvVals)
												
													end	
													
													// *** cache the result for this parentPath (good or bad)
													csvCache.(key) = rtn
													return rtn	
												
												end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function Integer GetNumCsvFiles()
												return ._fNumCsvFiles
											end

	
	public function Object New()
											
										Frame f = ._NewFrame()
									
										f._fCsvCache = Assoc.CreateAssoc()
									
										return f
										
									end

	
	private function Assoc _GetCSVLine(	Assoc  csvVals, \
																		List indexColNames, \
																		String fileName, \
																		String rootPath, \
																		String filePath)
											
												Assoc 	rtn = .InitErrObj( "_GetCSVLine" )
												Assoc	result
												String	indexColName
												
												// *** @Returns
												//Assoc result
												//result.colNames = List of columnNames
												//result.data = a list of data assoces--actually it should only be 1 assoc in the list (Not sure why--has something to do with front-end)	
												
												// *** figure out the path -- replace the rootPath in the filePath
												String path = Str.Replace(filePath, rootPath, "")
												String pathNoSep = File.IsDirSpec(path) ? path[1:-2] : path
												
												// *** initialize result
												result.colNames = csvVals.colNames
												
												// *** do we have an index telling us where to get the vals based on sourcePath?
												Assoc index	= csvVals.sourcePathIndex
												List 	paths = {._HashKey(path), ._HashKey(pathNoSep)}
												String 	hashKey
												Integer pos
												for hashKey in paths
													if IsDefined(index) && IsFeature(index, hashKey)
														pos = index.(hashKey)
														result.data = {csvVals.data[pos]}
														result.dataFlags = csvVals.dataFlags
														return .SuccessResult(result)
													end	
												end
											
												// *** ok, what about fileName?
												hashKey = ._HashKey(fileName)
												index	= csvVals.fileNameIndex
												if IsDefined(index) && IsFeature(index, hashKey)
													pos = index.(hashKey)
													result.data = {csvVals.data[pos]}
													return .SuccessResult(result)
												end
											
												// *** uggh.  Somethings wrong, we have to loop through the vals.  return only metadata for this file
												Assoc vals
												for vals in csvVals.data 
													for indexColName in indexColNames
														if IsFeature(vals, indexColName) && (vals.(indexColName) == fileName || vals.(indexColName) == path || vals.(indexColName) == pathNoSep)
															result.data = {vals}
															result.dataFlags = csvVals.dataFlags
															return .SuccessResult(result)
														end
													end	
												end
												
												.SetErrObj(rtn, Str.Format("Could not find metadata in index col names %1 with filename %2 or path %3", indexColNames, fileName, filePath))
												return rtn
											
											end

	
	private function Assoc _OpenCSV(String csvPath, \
																	Integer limit)
											
												Assoc 	rtn = .InitErrObj( "_OpenCSV" )
												Assoc	status
												Assoc	csvResult
												Assoc 	csvCache = ._fCsvCache	
											
												// *** return from the cache if we can
												if IsFeature(csvCache, csvPath)
													csvResult = csvCache.(csvPath)
												else
													String parentPath = $DataManager.FileUtils.GetParentPath(csvPath)
												
													// *** load the csv
													status = $DataManager.CsvParser.ParseFile(csvPath, 1, undefined, limit)
													if status.ok
														._fNumCsvFiles += 1
														csvResult = status.result
														csvCache.(csvPath) = csvResult
														csvCache.(parentPath) = csvResult
													else
														.SetErrObj(rtn, status)
														return rtn
													end
												end
											
												return .SuccessResult(csvResult)
											
											end

end

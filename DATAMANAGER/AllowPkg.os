package DATAMANAGER

public object AllowPkg inherits DATAMANAGER::SynCoreMain

	
	public function Assoc CheckAllowed(Object prgCtx, \
													Integer event)
						
							Assoc rtn = .InitErrObj("IsAllowed")
							Assoc status
							String permS
							Object licenseObj = $DataManager.License
							
							// *** make sure we have the licenseObj
							licenseObj.GetLicenseInfo(prgCtx)
						
							// *** first check licensing
							// *** licenseObj expired?
							if licenseObj.IsExpired()
								.SetErrObj(rtn, "Your license has expired")		
								return rtn
							end	
							
							// ***  events in the range 1-7 are export events
							if event >= 1 &&  event <= 7 && !$DataManager.License.AllowBulkExporter()
								.SetErrObj(rtn, "License does not allow use of BulkExporter")		
								return rtn
								
							// ***  events in the range 8-14 are load events and 14
							elseif event >= 8 &&  event <= 14 && !$DataManager.License.AllowBulkLoader()
								.SetErrObj(rtn, "License does not allow use of BulkLoader")		
								return rtn	
								
							elseif event < 1 || event > 14
								.SetErrObj(rtn, "License error.  Unknown event.")
								return rtn	
							end
						
						
							if prgCtx.fDbConnect.HasSysAdminPerm()
								
								// we are good
								
							// ***  events in the range 1-7 are export events
							elseif event >= 1 &&  event <= 7
								// *** next check the user's privileges
								status = $DataManager.ExportPrivilegesAPI.GetItem(prgCtx, prgCtx.USession().fUserName)
								if !status.ok
									.SetErrObj(rtn, "The user does not have privileges to use the BulkExporter")
									return rtn
								
								else
									permS = status.result
									if permS[event] == "0"
										.SetErrObj(rtn, "The user does not have adequate privileges to perform this operation.")
										return rtn			
									end
								end	
								
							// ***  events in the range 8-14 are load events
							elseif event >= 8 &&  event <= 14
						
								// *** next check the user's privileges
								status = $DataManager.LoadPrivilegesAPI.GetItem(prgCtx, prgCtx.USession().fUserName)
								if !status.ok
									.SetErrObj(rtn, "The user does not have privileges to use the BulkLoader")
									return rtn
								
								else
									permS = status.result
									if permS[event-7] == "0"
										.SetErrObj(rtn, "The user does not have adequate privileges to perform this operation.")
										return rtn			
									end
								end	
						
							else
								.SetErrObj(rtn, "License error.  Unknown event.")
								return rtn	
							end
							
							
							
							return rtn
							
						end

	
	public function Boolean IsAllowed(Object prgCtx, \
													Integer event)
						
							Assoc status = .CheckAllowed(prgCtx, event)
							if !status.ok
								return false
							else
								return true
							end		
							
						end

	
	public function Assoc IsEvalCountExceeded(	Object prgCtx, Integer i )
						
							Assoc	rtn = .InitErrObj("IsEvalCountExceeded")
							Object licenseObj = $DataManager.License
						
							// *** make sure we have the licenseObj
							licenseObj.GetLicenseInfo(prgCtx)	
						
							// *** first check licensing
							Boolean isEval = licenseObj.IsEval()
							Integer limit = licenseObj.GetEvalLimit()
							if isEval && i > limit
								.SetErrObj(rtn, "You have reached the EVAL limit for this job. To load more items, please purchase a license.")
								return rtn	
							end
							
							return rtn
							
						end

end

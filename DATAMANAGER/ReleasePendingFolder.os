package DATAMANAGER

public object ReleasePendingFolder inherits DATAMANAGER::#'DataManager LLRequestHandlers'#

	override	Boolean	fEnabled = TRUE
	public		Dynamic	fPending


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
				
				Object prgCtx = .fPrgCtx
				
				// make sure we start out with no error
				.fError = undefined
				
				// path from request
				String	dirPath = r.dirpath
				
				if File.Exists(dirPath)
					
					// now release the load by renaming it
					if ._WriteUserIdFile(prgCtx, dirPath)
						if ._IsPending(dirPath)
							if !($DataManager.Utils.RemovePending(dirPath))
								// did not work, delete the userID file, then report error
								._DeleteUserFile(dirPath)
								.fError = "Unable to delete '.PENDING' from '" + dirPath + "'"							
							end
						end
					end
					
				else
					.fError = Str.Format("Path %1 no longer exists.", dirPath)
				end		
							
				.fLocation = .URL() + '?func=datamanager.ReleasePendingLoads'
				return undefined		
				
			end

	
	private function Void _DeleteUserFile(String dirPath)
				String	userFilePath  = dirPath + File.Separator() + "USERID.DATA"
				File.Delete(userFilePath)
			end

	
	private function Boolean _IsPending(String path)
				
				return $DataManager.Utils.IsPending(path)
				
			end

	
	private function Boolean _WriteUserIdFile(Object prgCtx, String dirPath)
			
				// handle the user file
				Integer UserID 		= prgCtx.fUSession.fUserId			
				String	UserName 	= prgCtx.fUSession.fUserName
				
				// open a new file path
				String	userFilePath  = dirPath + File.Separator() + "USERID.DATA"
				File UserIDFile = File.Open(userFilePath, File.WriteMode)
				if isError(UserIDFile)
					.fError = "Unable to open USERID.DATA " + Str.Quote(userFilePath)
					return false
				end
				
				// write to the file
				Dynamic userData 		= File.Write(UserIDFile, str.format("%1|%2", UserID, UserName))
				File.Close(UserIDFile)
				
				return IsNotError(userData)
				
			end

end

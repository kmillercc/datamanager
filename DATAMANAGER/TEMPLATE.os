package DATAMANAGER

public object TEMPLATE inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'templateId'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$TEMPLATE'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
													
							Integer templateId
							
							Assoc status = ._CheckNodeReference(prgCtx, val)
									
							if !status.ok
										
								return .SetErrObj(.InitErrObj("ValidateSetVal"), status)
								
							else
								
								templateId = status.result.pId
								
							end	
						
							Assoc rtn
							rtn.result = templateId
							rtn.ok = true
							return rtn
												
						end

end

package DATAMANAGER

public object CSVWriter inherits DATAMANAGER::FileWriter

	override	String	fSubKey = 'csv'


	
	public function Assoc StartFile(List colNames)
												
									
				Assoc status = .OpenFile()
				if !status.ok
					return .SetErrObj(.InitErrObj("StartFile"), status)
				end
		
				// *** first pull out the heading string to write into the first row of the csv
				.WriteList(colNames, ._fDelim)
			
				Assoc rtn
				rtn.ok = true		
				return rtn
			
			end

	
	public function String WriteList(List valList, String delim = ._fDelim)
											
				String lineStr = '"' + Str.Catenate(valList, '"' + delim + '"')
				
				// *** end will have the extra comma and quote - remove these
				File.Write(._fFile, lineStr[1:-3])
				
				return lineStr
						
			end

end

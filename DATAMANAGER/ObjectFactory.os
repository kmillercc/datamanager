package DATAMANAGER

public object ObjectFactory inherits KERNEL::Subsystem

	public		Boolean	fInitialized = FALSE


	
	public function Object GetObject(Dynamic key, Dynamic subkey = undefined)
												
			if IsFeature(.fRegistry.(key), subkey)
				return .fRegistry.( key ).( subkey )
			else
				return .fRegistry.( key ).( undefined )
			end
				
		end

	
	override function Object New( Object	defaultItem = .fRegistryDefaultItem )
												
			Object	subsystem
			
			echo("ObjectFactory.fInitialized=" + Str.String(this.fInitialized))
			
			if .fInitialized
				subsystem = this
			else	
				subsystem = OS.NewTemp( this )
			
				//	Create the registry with the specified default item.
				subsystem.fRegistry = Assoc.CreateAssoc( defaultItem )
				
				subsystem.fInitialized = true
			end
			
			return subsystem
		end

	
	override function void RegisterItem(Object item, Dynamic key, Dynamic subkey = undefined)
												
			if !IsFeature(.fRegistry, key)
				.fRegistry.(key) = Assoc.CreateAssoc()
			end	
			.fRegistry.(key).(subkey) = item
			
		end

end

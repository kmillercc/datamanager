package DATAMANAGER

public object openCSV inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'filepath', -1, 'FilePath', FALSE }, { 'colNames', -2, 'colNames', FALSE }, { 'forDataTable', 5, 'For Data Table', TRUE, FALSE }, { 'startNo', 2, 'startNo', TRUE, 0 }, { 'displayLength', 2, 'displayLength', TRUE, 10 }, { 'total', 2, 'total', FALSE }, { 'nodeObjects', -18, 'nodeObjects', TRUE, Undefined }, { 'sourceSettings', -18, 'sourceSettings', TRUE, Undefined } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
						 	 * 	colNames 		=> colNames 	=> List of colNames (already parsed earlier)
							 * 	filepath 		=> String 	=> the filepath we are validating
						 	 *  forDataTable	=> Boolean	=> for the data table?   
						 	 *	startNo			=> Integer	=> start point
						 	 *	displayLength	=> Integer	=> number of records to return	 
						 	 *	total			=> Integer	=> total (computed earlier)
						 	 *  nodeObjects		=> Assoc	=> Assoc of nodeObjects (for setting errMsgs and such)
						  	 *  sourceSettings		=> Assoc	=> Assoc of values (for setting find and replace and push down values)
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.FileUtils.CheckPathExists(r.filepath, 'file')
							if status.ok
								Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 300000)
							
								// *** display length might be set to 1, in which case the UI is asking for all rows
								Integer displayLen = r.displayLength == 1 ? limit : r.displayLength
							
								// *** the javascript is 0 based, plus we have the header column. take the start and end and add 2
								Integer startNo = r.startNo+1
								Integer endNo = startNo + displayLen - 1
								
								// *** Instantiate the NodeIterator
								Object csvCallback = $DataManager.CsvDisplayCallback.New(r.nodeObjects, r.sourceSettings)	
						
								// *** load the csv
								status = $DataManager.CsvParser.ParseFile(r.filepath, startNo, endNo, limit, csvCallback, "CsvLineCallback")
								if status.ok
									if r.forDataTable		
										// Plop in some things here for the data tables
										Assoc a
										a.aaData = status.result.data
										a.iTotalRecords = r.total
										a.iTotalDisplayRecords = r.total
										.fContent = synCore.SuccessResult(a)
									else
										.fContent = synCore.SuccessResult(status.result)
									end
								else
									// for debugging .
									synCore.SetErrObj( rtn, status )
								
									.fError = rtn
								end		
							
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "filepath", StringType, "FilePath", FALSE }, \			
									{ "colNames", ListType, "colNames", FALSE }, \		
									{ "forDataTable", BooleanType, "For Data Table", TRUE, FALSE }, \			
									{ "startNo", IntegerType, "startNo", TRUE, 0 }, \
									{ "displayLength", IntegerType, "displayLength", TRUE, 10 }, \
									{ "total", IntegerType, "total", FALSE }, \			
									{ "nodeObjects", Assoc.AssocType, "nodeObjects", TRUE, undefined}, \
									{ "sourceSettings", Assoc.AssocType, "sourceSettings", TRUE, undefined } \			
								}
						end

end

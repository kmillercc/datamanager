package DATAMANAGER

public object getCsvColumnHtml inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'formVals', -18, 'formVals', TRUE, Undefined } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 * 	formVals 	=> Assoc 	=> Options to control what happens	 
							*/	 
						     
							// *** hand off to the API
							String html = $DataManager.CsvColumnPicker.GetCsvColumnHtml(prgCtx, r.formVals)
							.fContent = synCore.SuccessResult(html)
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "formVals", Assoc.AssocType, "formVals", TRUE, undefined} \
								}
						end

end

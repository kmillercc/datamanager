package DATAMANAGER

public object MEDIATYPEFIELDS inherits DATAMANAGER::PhysObjectsSpecTab

	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$MEDIATYPEFIELDS'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							List 	pairs = Str.Elements(val, "|")
							String	pair
							
							// *** get the media field record which tells us what field type this is
							Integer mediaTypeId = apiDataObj.GetValueFromCol("$SELECTEDMEDIA")
							Integer nodeId = apiDataObj.GetValue("nodeid")
							
							if IsUndefined(mediaTypeId)
								if IsDefined(nodeId)
									String stmt = "select MediaTypeID from PhysItemData where NodeID=:A1"
									RecArray result = .ExecSql(prgCtx, stmt, {nodeid})
									if IsError(result)
										return .SetErrObj(.InitErrObj("ValidateVal"), result)
									elseif Length(result)
										mediaTypeId = result[1].MediaTypeID
									else
										//return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("Could not parse $MediaTypeFields.Could not get media type id for nodeid %1", nodeid))
									end	
								else
									//return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("Could not parse $MediaTypeFields. Could not determine the media type for this item. You may need to include a $SELECTEDMEDIA or $PHYSITEMTYPE column", nodeid))
								end		
											
							end	
							
							for pair in pairs
								Integer equalsLoc = Str.Locate(pair, "=")
								if IsDefined(equalsLoc)
									String name = pair[1:equalsLoc-1]
									String valStr = pair[equalsLoc+1:]
									
									// *** support clear
									if Str.Lower(valStr) in {"<clear>","[clear]"}
										valStr = undefined
									end
									
									String fName = "mtField_" + name
									
									// *** register this field in the apiDataObj
									apiDataObj.RegisterMediaTypeField(.fApiKey1, .fApiKey2, .fApiKey3, fName)
									
									Record fieldRec
									Assoc status = $DataManager.PoMediaTypes.GetMediaFieldRec(mediaTypeId, name)
									if !status.ok
										return .SetErrObj(.InitErrObj("ValidateVal"), status)
									else
										fieldRec = status.result
									end	
									
									// *** convert the value to appropriate type
									Dynamic fieldVal = Length(Str.Trim(valStr)) ? ._ConvertVal(fName, valStr, fieldRec) : undefined
									
									// *** now save the value
									apiDataObj.SetValueFromCol(fName, fieldVal, false, action, false)
									
								end
							end 
							
							Assoc 	rtn
							rtn.ok = true
							return rtn
												
						end

	
	private function Dynamic _ConvertVal(String fieldName, \
													Dynamic	val, \
													Record mediaRec)
																	
							if IsDefined(val)										
							
								switch mediaRec.FieldType
								
									case 1, 2, 3  // string, integer, popup
										val = Type(val) != StringType ? Str.String(val) : val
									end
							
									case 4 //date
										if Type(val) == StringType 
											Dynamic d = $DataManager.FormatPkg.ConvertStringToVal(val, DateType)
											if IsUndefined(d)
												.GetLogger().ERROR(Str.Format("Could not convert %1 to date value for field %2", val, fieldName))
											else
												val = Str.ValueToString(d)
											end	
										elseif Type(val) == DateType
											val = Str.ValueToString(val)						
										end	
									end
									
									case 5  //boolean
										if Str.Upper(val) in {'1', 'ON', 'TRUE'}
											val = 'on'
										end	
									end
									
									default 
										val = Str.String(val)
									end	
								end				
							
							end	
							
							return val
							
						end

end

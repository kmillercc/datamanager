package DATAMANAGER

public object EMAILPARTICIPANTS inherits DATAMANAGER::Email

	override	String	fApiKey3 = 'Participants'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$EMAILPARTICIPANTS'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc rtn
							rtn.ok = true
							rtn.result = {}
							
							// *** val is expected as an Oscript List
							if Type(val) == StringType
								val = Str.StringToValue(val)
								if IsDefined(val) && Type(val) == ListType	
									rtn.result = val
								end
							end		
						
							return rtn
												
						end

end

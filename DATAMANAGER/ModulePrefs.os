package DATAMANAGER

public object ModulePrefs inherits DATAMANAGER::Utils

	public		Dynamic	fPrefs


	
	public function Dynamic GetPref(String key, \
												Dynamic defaultVal = undefined, \
												Object prgCtx = undefined, \
												String moduleName = 'DataManager', \
												Boolean convertToValue = true)
																					
							Assoc a = .GetPrefs(prgCtx, key, moduleName, convertToValue)
							
							if IsFeature(a, key)
								return a.(key)
							else
								return defaultVal
							end			
						
						end

	
	public function Dynamic GetPrefString(String key, \
														Dynamic defaultVal = undefined, \
														Object prgCtx = undefined, \
														Boolean loadFresh = false, \
														String moduleName = 'DataManager', \
														Boolean convertToValue = true)
																					
							Assoc a = .GetPrefs(prgCtx, key, moduleName, convertToValue, loadFresh)
							
							if IsFeature(a, key)
								return a.(key)
							else
								return defaultVal
							end			
						
						end

	
	public function Assoc GetPrefs(Object prgCtx = undefined, \
												String keyword = 'Prefs', \
												String moduleName = 'DataManager', \
												Boolean convertToValue = true, \
												Boolean loadKiniFresh = true)
						
							Dynamic kiniPrefs
						
							// *** did we cache it?	
							Assoc prefs = IsUndefined(.fPrefs) ? Assoc.CreateAssoc() : .fPrefs
							
							if IsUndefined(prefs.(keyword))
								Assoc newPrefs
								
								newPrefs = ._GetFilePrefs(moduleName, "Prefs", convertToValue)	
								
								if IsDefined(prgCtx)
									kiniPrefs = ._GetKiniPrefs(prgCtx, moduleName, keyword, Assoc.CreateAssoc())
									
									if Type(kiniPrefs) == Assoc.AssocType
										newPrefs = Assoc.Merge(newPrefs, kiniPrefs)
									else
										newPrefs.(keyword) = kiniPrefs
									end		
									
								end	
										
								prefs = .fPrefs = Assoc.Merge(prefs, newPrefs)
						
							elseif loadKiniFresh && IsDefined(prgCtx)
								Assoc newPrefs
								
								kiniPrefs = ._GetKiniPrefs(prgCtx, moduleName, keyword, Assoc.CreateAssoc())
								
								if Type(kiniPrefs) == Assoc.AssocType
									newPrefs = Assoc.Merge(newPrefs, kiniPrefs)
								else
									newPrefs.(keyword) = kiniPrefs
								end			
								
								prefs = .fPrefs = Assoc.Merge(prefs, newPrefs)
							end
						
							return prefs	
						
						end

	
	public function Dynamic PutPref(Object prgCtx, \
												String key, \
												Dynamic val, \
												String keyword = 'Prefs', \
												String moduleName = 'DataManager')
														
							Assoc existingVals = ._GetKiniPrefs(prgCtx, moduleName, keyword, Assoc.CreateAssoc())
								
							// *** put the new val into the assoc							
							existingVals.(key) = val
														
							return CAPI.IniPut(prgCtx.fDbConnect.fLogin, moduleName, keyword, existingVals)
						
							
						end

	script PutPrefString
	
			
					
							
									
											
													
												
														
															function Dynamic PutPref(Object prgCtx, \
																					String keyword, \
																					Dynamic val, \
																					String moduleName = 'DataManager')
																							
																				
																return CAPI.IniPut(prgCtx.fDbConnect.fLogin, moduleName, keyword, val)
															
																
															end								 
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	script _GetFilePrefs
	
			
					
							
									
											
													
												
														
															//
															//
															// Syntergy
															// Author: 			Kit Miller  
															// Date: 			May 5, 2008
															// Description:		Retrieves all preferences in the form of an Assoc from the module ini file.
															// Modifications:
															// Date           Name         Description
															//
															//////////////////////////////////////////////////////
															
															function Assoc _GetFilePrefs( 	String moduleName, \
																							String section, \
																					    	Boolean convertToValue)
															
																FilePrefs	fprefs = _LoadFilePrefs(moduleName)
																
																Assoc		prefs
																Dynamic		eachKey
																Dynamic		val
																
																if ( IsDefined( fprefs ) && !IsError( fprefs ) )
															
																	prefs = Fileprefs.ListValues( fprefs, section )
																	Fileprefs.Close( fprefs )		
																	
																	if IsDefined(prefs) && IsNotError(prefs)
																		if convertToValue
																			for eachKey in Assoc.Keys(prefs)
																				if Type(prefs.(eachKey)) == StringType && !_IsAlpha(prefs.(eachKey))
																					val = Str.StringToValue(prefs.(eachKey))
																					if IsDefined(val)
																						prefs.(eachKey) = val
																					end		
																				end	
																			end	
																		end	
																	else
																		prefs = Assoc.CreateAssoc()	
																	end
															
																end
															
																return prefs
															
															end
															
															
															function Boolean _IsAlpha(String s)
															
																List match = Pattern.Find(s, '[A-Za-z]+', TRUE)
																
																if IsNotError(match) && IsDefined(match)
																	if match[3] == s
																		return true
																	end
																end	
																	
																return false
															
															end
															
															//
															//
															// Syntergy
															//
															// Name: __LoadFilePrefs
															//
															// Author: Kit Miller  Date: Wed May 05 11:15:17 2004
															//
															// Description: Retrieves the preferences in the module's ini file.
															//
															//
															// Modifications:
															//
															//Date           Name         Description
															//
															//
															//////////////////////////////////////////////////////
															function Dynamic _LoadFilePrefs(String moduleName)
															
																String 		globalName = moduleName + 'IniPrefs'
																FilePrefs	fPrefs = Global.Application(globalName) 
																
																if IsDefined(fPrefs)
																	return fPrefs
																else	
																	String		iniFile = $Kernel.ModuleUtils.ModuleIniPath(moduleName)
																	fPrefs = Fileprefs.Open( iniFile )
																	Fileprefs.Close( fprefs )
																	
																	Global.Application(globalName, fPrefs)
																end	
															
																return fPrefs
															
															end
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function Dynamic _GetKiniPrefs(	Object prgCtx, \
														String moduleName, \
														String keyword, \
														Dynamic defaultVal)
						
							return CAPI.IniGet(prgCtx.fDBConnect.fLogin, moduleName, keyword, defaultVal)
							
						end

end

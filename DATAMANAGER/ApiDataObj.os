package DATAMANAGER

public object ApiDataObj inherits DATAMANAGER::LoadObjects

	public		String	_fAction
	public		Dynamic	_fApiKeysMap
	public		Dynamic	_fApiVals
	public		Dynamic	_fCsvData
	public		Boolean	_fIgnoreMetadata = FALSE
	public		Dynamic	_fNodeApiKeysMap
	public		Dynamic	_fUGApiKeysMap
	public		List	fColNames


	
	public function void AddAttrKeys(Assoc attrKeyMap)
									
									._fApiKeysMap = Assoc.Merge(._fApiKeysMap, attrKeyMap)
									
								end

	
	public function Void AddCatDef(Integer catID, Integer catVersionNum, Assoc catDef, Boolean override = FALSE)
			
				Assoc 	apiVals = ._fApiVals
				String 	key = Str.Format("%1.%2", catID, catVersionNum)
				
				if override || !IsFeature(apiVals.metadata.catDefs, key)
					apiVals.metadata.catDefs.(key) = catDef
				end	
			
			end

	
	public function Void AddCatDefByKey(String key, Assoc catDef, Boolean override = FALSE)
			
				Assoc 	apiVals = ._fApiVals
		
				if override || !IsFeature(apiVals.metadata.catDefs, key)
					apiVals.metadata.catDefs.(key) = catDef
				end	
			
			end

	
	public function Assoc GetAssocVals()
									return ._fApiVals
								end

	
	public function Assoc GetCatDefs()
				Assoc apiVals = ._fApiVals
				
				return apiVals.metadata.catDefs
			end

	
	public function Assoc GetCsvData()
									return ._fCsvData
								end

	
	public function Dynamic GetValue(String key)
								
									return ._fApiVals.(key)
								
								end

	
	public function Dynamic GetValueFromCol(String col)
								
									Assoc	apiVals = ._fApiVals
									Dynamic val
									
									Dynamic keys = ._fApiKeysMap.(col)
									if Type(keys) == Assoc.AssocType
										String action = ._fAction == "CREATE" ? "CREATE" : "UPDATE"
										keys =  keys.(action)
									end	
								
									if ._fIgnoreMetadata && keys[1]=="metadata"
										return undefined
									end		
								
									if IsDefined(keys)
										if IsDefined(keys[4])
											val = apiVals.(keys[1]).(keys[2]).(keys[3]).(keys[4])
										elseif IsDefined(keys[3])
											val = apiVals.(keys[1]).(keys[2]).(keys[3])
										elseif IsDefined(keys[2])
											val = apiVals.(keys[1]).(keys[2])
										else
											val = apiVals.(keys[1])
										end	
									end	
								
									return val
								end

	
	public function Object New( String action, \
													 Assoc apiVals = ._NewApiValsAssoc(action), \
													 Assoc csvData = undefined, \
													 Assoc attrKeysMap = undefined)
								
								
									Frame f = ._NewFrame()
								
									if IsUndefined(apiVals)
										apiVals = ._NewApiValsAssoc(action)
									end
									
									// *** no metadata needed for purge and delete	
									if action in {"PURGE","DELETE"}
										f._fIgnoreMetadata = true
									end
								
									
									f._fAction = action
									f._fApiVals = apiVals	
									f._fCsvData = csvData
									
									// *** KM 11/9/16 distinguish between users and groups and regular node actions
									if action in {"UPDATEUSERGROUP","CREATEUSERGROUP","DELETEUSERGROUP"}
										f._fApiKeysMap = Assoc.Copy(._fUGApiKeysMap)
									else
										if IsDefined(attrKeysMap)
											._fNodeApiKeysMap = Assoc.Merge(._fNodeApiKeysMap, attrKeysMap)				
										end	
									
										f._fApiKeysMap = Assoc.Copy(._fNodeApiKeysMap)
									end	
											
									return f	
								
									
								end

	
	public function Assoc PopulateFromNode(Object prgCtx, \
																Dynamic node, \
																Assoc dataFlags, \
																List colNames, \
																Boolean doValidate = true, \
																Object llNode = undefined)
									
									if Type(node) == IntegerType
										Integer nodeId = node
										node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, nodeId)
										if IsError(node)
											return .SetErrObj(.InitErrObj("PopulateFromNode"), Str.Format("Could not get node from ID %1", nodeId))		
										end
									end	
									
									if IsUndefined(llNode)
										llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubtype)
									end
									
									/* the dataFlags
									
								        hasRecManData
								        hasPhysObjData
										hasPhysObjCirc
										hasPhysObjBox
										hasPhysObjLocator
										hasPhysObjXfer
										hasPhysObjLabel
								        hasEmailData
								        hasPermData
								        hasClassification
								        hasDapiNodeData
								        hasOwnerData
								        hasNickName
								        hasNewName
								        hasFiles
								        hasAttrData
								        hasSystemAttrs
								        hasMetadata
								        hasVersionData
									
									*/
									
									Assoc 	options
									options.metadataOnly = true
									
									options.wantNodeId = true
									options.wantBasicData = dataFlags.hasDapiNodeData || dataFlags.hasNickName
									options.wantClassifications = dataFlags.hasClassification
									options.wantRMData = dataFlags.hasRecManData
									options.wantCategoryData = dataFlags.hasAttrData
									options.wantSystemAtts = dataFlags.hasSystemAttrs
									options.wantEmails = dataFlags.hasEmailData
									options.wantPhysObj = dataFlags.hasPhysObjData
									options.wantAllVersions = dataFlags.hasVersionData
									options.wantPerms = dataFlags.hasPermData
									options.wantOwner = dataFlags.hasOwnerData	
									options.wantPhysObjCirc = dataFlags.hasPhysObjCirc
									options.wantContractData = dataFlags.hasContractData	
									
									// *** get the version (if it's versioned)
									DAPIVERSION	 version = llNode.fVersioned ? DAPI.GetVersion(	node, DAPI.VERSION_GET_CURRENT) : undefined		
									
									Boolean convertToString = false							
									Object gatherer = $DataManager.NodeMetadataGatherer.New(prgCtx, options, undefined, colNames, convertToString)
									
									Assoc status = gatherer.GatherMetadata(prgCtx, node, version, "")
									
									if !status.ok
										return .SetErrObj(.InitErrObj("PopulateFromNode"), status)
										
									else
									
										Assoc csvData = status.result
										._fCsvData = csvData
										
										if doValidate
											Object validateWrapper = $DataManager.ValidateWrapper.New(prgCtx, "UPDATE", undefined, "")
											Frame dataObj = validateWrapper.GetFromCsvData(csvData)
											._fApiVals = dataObj.GetAssocVals()
										end	
										
									end
								
									Assoc rtn
									rtn.ok = true
									return rtn
								
								end

	
	public function void RegisterMediaTypeField( String key1, \
																	  String key2, \
																	  String key3, \
																	  String fieldName)
																
									
									if !(IsFeature(._fApiKeysMap, fieldName))
										._fApiKeysMap.(fieldName) = {key1, key2, key3, fieldName}
									end
										
																
								end

	
	public function void SetAddValue(String key, Dynamic newVal)
								
									List vals = .GetValue(key)
									
									if IsDefined(vals)
										vals = List.SetAdd(vals, newVal)
									else	
										vals = {newVal}
									end
									
									.SetValue(key, vals)
								
								end

	
	public function void SetAddValueFromCol(String key, Dynamic newVal, Boolean addToCols = true)
								
									List vals = .GetValueFromCol(key)
									
									if IsDefined(vals)
										vals = List.SetAdd(vals, newVal)
									else	
										vals = {newVal}
									end
									
									.SetValueFromCol(key, vals, false, ._fAction, addToCols)
								
								end

	
	public function Void SetAddValueFromKeys(	String key1,\
													String key2,\
													String key3,\
													Dynamic newVal)
			
			Assoc apiVals = ._fApiVals
			List  vals
		
			if IsDefined(key3)
				vals = apiVals.(key1).(key2).(key3)
			elseif IsDefined(key2)
				vals = apiVals.(key1).(key2)
			else
				vals = apiVals.(key1)
			end	
			
			vals = List.SetAdd(vals, newVal)
	
			.SetValueFromKeys(key1, key2, key3, vals)
	
		end

	
	public function Void SetValue(String key, Dynamic val)
									._fApiVals.(key) = val
								end

	
	public function Void SetValueFromCol(	String col, \
																Dynamic val, \
																Boolean isCatAttr = false, \
																String action=._fAction, \
																Boolean addToColNames = true)
																
															
									if addToColNames
										// *** add this col to the .fColNames
										.fColNames = {@.fColNames, col}
									end	
								
									Assoc	apiVals = ._fApiVals
									
									Dynamic keys = ._fApiKeysMap.(col)
									
									if Type(keys) == Assoc.AssocType
										// *** also set vals in update in case we find later that the node already exists
										if action == "CREATE"
											SetValueFromCol(col, val, isCatAttr, "UPDATE")
										else
											action = "UPDATE"
										end	
											
										keys =  keys.(action)		
										
									end	
								
									if keys[1] == "metadata"
										
										if ._fIgnoreMetadata
											return
										else		
											apiVals.hasMetadata = true
										end
									end	
									
									if isCatAttr
								
										// *** should we dynamically add this to the list of catIds -- not sure			
										List attrKeys = Str.elements(keys[ 3 ], ".")
										if Length(attrKeys)
											Integer catId = Str.StringToInteger(attrKeys[1])
											List catIds = ._fApiVals.metadata.catIds
																if (catId in catIds == 0)
												._fApiVals.metadata.catIds = {@catIds, catId}
											end
										end	
									
										._StoreCatAttrVal(apiVals.(keys[1]).(keys[2]), keys[3], val)
									
									else
								
										if IsDefined(keys[4])
											apiVals.(keys[1]).(keys[2]).(keys[3]).(keys[4]) = val	
										elseif IsDefined(keys[3])
											apiVals.(keys[1]).(keys[2]).(keys[3]) = val
										elseif IsDefined(keys[2])
											apiVals.(keys[1]).(keys[2]) = val
										else
											apiVals.(keys[1]) = val	
										end	
									end		
								
								end

	
	public function Void SetValueFromKeys(	String key1,\
													String key2,\
													String key3,\
													Dynamic val)
			
				Assoc apiVals = ._fApiVals
			
				if IsDefined(key3)
					apiVals.(key1).(key2).(key3) = val
				elseif IsDefined(key2)
					apiVals.(key1).(key2) = val
				else
					apiVals.(key1) = val	
				end	
				
			end

	
	private function Assoc _NewApiValsAssoc(String action)
			
				// *** these are the actions
				/* 	ADDVERSION
					COPY
					CREATE
					DELETE
					MOVE
					UPDATE
					PURGE
					
					UPDATEUSERGROUP
					DELETEUSERGROUP		
					CREATEUSERGROUP
					
					// perms is part of update PERMS
					// rename is part of update RENAME
			
				*/	
			
				Assoc a
				
				if action in {"UPDATEUSERGROUP","CREATEUSERGROUP","DELETEUSERGROUP"}
				
					// we need simple user group values
					// just return a blank assoc
				
				else
					
					// *** basic important information
					a.objectNameFromCsv = undefined
					a.subType = undefined
					a.targetPath = undefined
					a.nodeid = undefined
					a.filePath = undefined
					a.sourcePath = undefined
					a.fileName = undefined
					a.newName = undefined
					a.name = undefined
					a.volumeId = undefined
					a.parentId = undefined
					
					
					if action == "PURGE"
					
						// *** for purging versions
						a.versionsToKeep = 1
						
					elseif action == "DELETE"
						
						// nothing needed		
						
					else
		
						// *** metadata
						a.metadata = Assoc.CreateAssoc()
					
						// *** physical objects
						if IsDefined($PhysicalObjects)
							// *** KM add these fields to avoid Null Pointer exception
							Assoc poData
							poData.request = Assoc.CreateAssoc()
							poData.labelData = Assoc.CreateAssoc()
							poData.boxData = Assoc.CreateAssoc()
							poData.circData = Assoc.CreateAssoc()
							poData.locatorData = Assoc.CreateAssoc()	
							poData.transferData = Assoc.CreateAssoc()
							poData.unused = Assoc.CreateAssoc()		
							
							// *** po data
							a.metadata.poData = poData
						end
					
						// *** flag that tells us if there is metadata
						a.hasMetadata = false
						
						// *** basic node data
						a.metadata.dapiNodeData = Assoc.CreateAssoc()
						
						// *** system attributes
						a.metadata.systemAtts = Assoc.CreateAssoc()
						
						// *** owner data
						a.metadata.ownerData = Assoc.CreateAssoc()
						
						// *** cats and atts 
						a.metadata.catDefs = Assoc.CreateAssoc()
						a.metadata.attrVals = Assoc.CreateAssoc()
						a.metadata.catIds = {}
						a.metadata.catIdsToRemove = {}
					
						// *** classification	
						a.metadata.classIds = {}
						a.metadata.doClearClassifications = false
						
						// *** recman
						a.metadata.rmClassId = undefined
						a.metadata.rmCreateInfo = Assoc.CreateAssoc() 
						a.metadata.rmUpdateInfo = Assoc.CreateAssoc()	
						
						// *** versions and renditions
						a.metadata.versionData = Assoc.CreateAssoc()
						a.metadata.renditionData = Assoc.CreateAssoc()
						
						if IsDefined($OTEMAIL)
							// *** email data
							a.metadata.emailData = Assoc.CreateAssoc()
							a.metadata.emailData.Properties = Assoc.CreateAssoc()
						end	
					
						// *** Nickname
						a.metadata.NickName = undefined
						
						// *** permissions
						a.permData = Assoc.CreateAssoc()			
						
						// *** contractFile
						a.metadata.contractFile = Assoc.CreateAssoc()
						
						// *** case management
						a.metadata.caseManagement = Assoc.CreateAssoc()
					end
				end	
			
				
				return a
			
			end

	
	private function void _StoreCatAttrVal(Assoc attrVals, \
																String attrKey, \
																List vals)
									
									List attrKeys = Str.Elements(attrKey, ".")
									String key
									
									switch length(attrKeys)
									
										case 5
											// *** means it's an attribute inside a set
											// attrKeys = {catId, catVersionNum, attrSetId, cardinality, attrDef.ID}	
											
											key = Str.Format("%1.%2.%3", attrKeys[1], attrKeys[2], attrKeys[3])
											Integer cardinality = Str.StringToInteger(attrKeys[4])
											Integer attrId = Str.StringToInteger(attrKeys[5])
											List setVals = IsFeature(attrVals, key) ? attrVals.(key) : {}
											
											while Length(setVals) < cardinality	-1
												setVals = {@setVals, Assoc.CreateAssoc()}
											end
											
											// *** not put vals in there
											Assoc newVals
											if Length(setVals) >= cardinality
												newVals = setVals[cardinality]
												newVals.(attrId) = vals
											else
												newVals = Assoc.CreateAssoc()
												newVals.(attrId) = vals
												setVals = {@setVals, newVals}
											end	
											
											attrVals.(key) = setVals
										end
										
										case 4
											// *** means it's an attribute set (maybe with <clear> directive to remove all rows)
											if vals=={undefined}	// *** {undefined} is what clear gets converted to in ValidateSetAttrValue
												key = Str.Format("%1.%2.%3.%4", attrKeys[1], attrKeys[2], attrKeys[3], attrKeys[4])
												attrVals.(key) = {}	// *** set it to empty list to signal to the AttrGroupPrep to clear all rows
											end				
										end
										
										case 3
											// *** regular attribute
											key = Str.Format("%1.%2.%3", attrKeys[1], attrKeys[2], attrKeys[3])
											attrVals.(key) = vals
										end
									
									
									end	
													
													
								end

end

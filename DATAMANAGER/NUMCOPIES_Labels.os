package DATAMANAGER

public object NUMCOPIES_Labels inherits DATAMANAGER::PhysObjLabels

	override	String	fApiKey4 = 'NumberOfCopies'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$NUMCOPIES'

end

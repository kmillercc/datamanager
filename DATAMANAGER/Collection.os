package DATAMANAGER

public object Collection inherits DATAMANAGER::NodeChildGetter

	override	Integer	fSubKey = 298


	override script GetSortedContents
	
			
					
							
									
											
													
												
														
															function Assoc GetSubNodes(	Object prgCtx, \
																						DAPINODE node, \
																						String sortBy = "NAME")
																
																Assoc		rtn = .InitErrObj("GetSubNodes")
																Assoc		result
																Dynamic		fakeRequest = Assoc.CreateAssoc()
																fakeRequest.sort = 'name'
																fakeRequest = Assoc.ToRecord(fakeRequest)
																RecArray	contents
																
																Object	llNode = $LLIAPI.LLNodeSubsystem.GetItem($TypeCollection)
																
																// *** determine how many params and what type of params for NodeListContents
																// *** OpenText has changed this in different update versions
																List	args = ._GetArgs(node, fakeRequest, "WebNodes", '', 0)
															
																Switch Length(args)
																	case 1
																		result = llNode.NodeListContents(args[1])		
																	end
																	
																	case 2
																		result = llNode.NodeListContents(args[1], args[2])				
																	end
																	
																	case 3
																		result = llNode.NodeListContents(args[1], args[2], args[3])				
																	end
																	
																	case 4
																		result = llNode.NodeListContents(args[1], args[2], args[3], args[4])				
																	end
																	
																	case 5
																		result = llNode.NodeListContents(args[1], args[2], args[3], args[4], args[5])				
																	end
																	
																	case 6
																		result = llNode.NodeListContents(args[1], args[2], args[3], args[4], args[5], args[6])
																	end
																	
																	case 7
																		result = llNode.NodeListContents(args[1], args[2], args[3], args[4], args[5], args[6], args[7])				
																	end
																
																
																end
																
																if !result.ok
																	.SetErrObj(rtn, result)
																	return rtn
																else
																	contents = result.Contents	
																end
																
																contents = RecArray.Sort(contents, sortBy)
															
																return .SuccessResult(contents)
																
															end
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Assoc GetSubNodes(	Object prgCtx, \
													DAPINODE node)
							
							Assoc		rtn = .InitErrObj("GetSubNodes")
							List		subNodes = {}
							DAPINODE	subNode
							Assoc		result
							Dynamic		fakeRequest = Assoc.CreateAssoc()
							fakeRequest.sort = 'name'
							fakeRequest = Assoc.ToRecord(fakeRequest)
							
							Object	llNode = $LLIAPI.LLNodeSubsystem.GetItem($TypeCollection)
							
							// *** determine how many params and what type of params for NodeListContents
							// *** OpenText has changed this in different update versions
							List	args = ._GetArgs(node, fakeRequest, "WebNodes", '', 0)
						
							Switch Length(args)
								case 1
									result = llNode.NodeListContents(args[1])		
								end
								
								case 2
									result = llNode.NodeListContents(args[1], args[2])				
								end
								
								case 3
									result = llNode.NodeListContents(args[1], args[2], args[3])				
								end
								
								case 4
									result = llNode.NodeListContents(args[1], args[2], args[3], args[4])				
								end
								
								case 5
									result = llNode.NodeListContents(args[1], args[2], args[3], args[4], args[5])				
								end
								
								case 6
									result = llNode.NodeListContents(args[1], args[2], args[3], args[4], args[5], args[6])
								end
								
								case 7
									result = llNode.NodeListContents(args[1], args[2], args[3], args[4], args[5], args[6], args[7])				
								end
							
							
							end
							
							if !result.ok
								.SetErrObj(rtn, result)
								return rtn
							else
								Record rec
								for rec in result.Contents
									subNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.Dataid)
									
									if IsNotError(subNode)
										subNodes = {@subNodes, subNode}
									end
														
								end
							
							end
						
							return .SuccessResult(subNodes)
							
						end

	override script Scratch
	
			
					
							
									
											
													
												
														
															function void TestLLNodeListContents()
															
																Assoc		checkVal
																Object		prgCtx = $WebProspector.Utils.GetPrgCtx()
																
																// ***  plug in the valid dataid of a collection here
																DAPINode	colNode = DAPI.GetNodeByID( prgCtx.DSession().fSession, DAPI.BY_DATAID, 23619, FALSE )
																
																Object		colLLNode =  $LLIApi.LLNodeSubsystem.GetItem( colNode.pSubType )
															
																checkVal = colLLNode.NodeListContents( colNode, 'WebNodes', '', 0 )
															
																if ( checkVal.ok )
																	echo( 'success. The size of the collection is ', Length( checkVal.contents ) )
																else
																	echo( 'failed! ', checkVal.errMsg )
																end
															
															end
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	script _GetArgs
	
			
					
							
									
											
													
												
														
															// Patched by Pat201411132
															function List _GetArgs(	DAPINODE node = undefined, \
																					Record request = undefined, \
																					String	viewName = "WebNodes",\
																					String	whereClause = '',\
																					Integer	permMask = 0)
															
																Object	llNode = $LLIAPI.LLNodeSubsystem.GetItem($TypeCollection)
																List	p = _GetScriptPrototype(llNode.NodeListContents)
																List	pArgs = p[2]
																List	myArgs = {}
															
																Integer i 
																
																// *** example protoype from LL971
																// {{-18,'NodeListContents'},{{-72,'node'},{-1,'viewName'},{-1,'whereClause'},{2,'permMask'},{-127,'request'}}}
																
															
																for i = 1 to Length(pArgs)
																	List arg = pArgs[i]
																	
																	
																	if arg[1] == DAPI.DapiNodeType && Str.Lower(arg[2]) == 'node'
																		myArgs = {@myArgs, node}
																	elseif arg[1] == StringType && Str.Lower(arg[2]) == 'viewname'
																		myArgs = {@myArgs, viewName}			
																	elseif arg[1] == StringType && Str.Lower(arg[2]) == 'whereclause'
																		myArgs = {@myArgs, whereClause}
																	elseif arg[1] == IntegerType && Str.Lower(arg[2]) == 'permmask'
																		myArgs = {@myArgs, permMask}						
																	elseif arg[1] in {RecArray.RecordType, DynamicType} && Str.Lower(arg[2]) == 'request'
																	
																		// *** KM 11/13/14 for collections 2.5.0, we will pass in undefined
																		//myArgs = {@myArgs, request}	
																		myArgs = {@myArgs, undefined}				
																	
																	else
																		// hope we don't get here.  just have to put in undefined
																		myArgs = {@myArgs, undefined}		
																					
																	end
																	
																end
																
																return myArgs
																
															end	
															
															function List _GetScriptPrototype(Script s)
																Dynamic p = Compiler.Prototype(s)
																
																return p
															
															end
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

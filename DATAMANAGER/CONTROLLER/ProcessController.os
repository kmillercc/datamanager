package DATAMANAGER::CONTROLLER

public object ProcessController inherits DATAMANAGER::InstanceObjects

	public		String	_fAction
	public		Dynamic	_fApiProcess
	public		Dynamic	_fApiProcessMap
	public		Dynamic	_fAuditAPI
	public		Boolean	_fDoDeltaLogs = FALSE
	public		String	_fErrorCsvNdcName = 'DataManagerErrorCsvLog'
	public		Integer	_fEvent
	public		Dynamic	_fEventMap
	public		Dynamic	_fIterator
	public		Dynamic	_fIteratorMap
	public		String	_fJobType
	public		String	_fLogPath = 'DataManagerProcessLog'
	public		String	_fOptionsKey = 'options'
	public		String	_fSuccessCsvNdcName = 'DataManagerSuccessCsvLog'
	public		String	_fWhenStr


	
	public function Assoc CheckJobStatus(Object prgCtx, Integer jobId)
														
				Assoc	rtn = .InitErrObj("ProcessController.CheckJobStatus")
				Object	job
				Assoc	result 
														
				// *** get the job
				Assoc status  = $DataManager.JobInterface.GetJob(prgCtx, jobID)	
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				else
					job = status.result	
				end
														
				// *** what is the status?
				String jobStatus = job.GetStatus()
														
				// *** what is the payload?
				Assoc payload = job.getPayload()
																
				// *** set up our return values
				if jobStatus == "error"
					.SetErrObj(rtn, payload.errMsg)
					return rtn
				else	
					if jobStatus == "completed"
						if IsDefined(payload.result) && Type(payload.result) == Assoc.AssocType
							result = payload.result
						end
						result.completed = true
																
					else
						result.completed = false
						result.completedCount = payLoad.completedCount
																
					end	 
				end
														
				rtn.result = result
				return rtn
																				
			end

	
	public function void Close(Boolean closeLog = true)
														
				if closeLog
					String appenderNdcName = ._fNdcProcessLog
				
					Object logger = .GetLogger()
					if IsDefined(logger) && IsFeature(logger.fLogAppenders, appenderNdcName)
						logger.fLogAppenders.( appenderNdcName ).Close()
					end	
				end
				
				$DataManager.SynCoreInterface.ClearNDC()
				
			end

	
	public function Assoc FinalizeAsync(Object prgCtx, \
										Assoc taskData, \
										String action, \
										Integer jobId, \
										Object dataCache, \
										Object job, \
										Assoc options, \
										List filesProcessed, \
										Integer nonFatalErrors = 0, \
										Integer validationErrors = 0)
			
				Dynamic params = Parameters()
				Assoc	rtn = .InitErrObj("ProcessController.FinalizeAsync", this, params)
				Assoc 	status
				Object	log = .GetLogger()
				
				if log.IsDebugEnabled()
					log.Debug("*** Function: FinalizeAsync")
					log.Debug(Str.Tab + "Parameters: " + $SyntergyCore.Utils.join( params, "," + Str.EOL() + Str.Tab() + Str.Tab() + Str.Tab() + Str.Tab()))		
				end	
				
				// *** get schedule data
				Assoc	schedData = taskData.schedData
				
				// *** initialize to 0 errors
				Boolean	hasFatalError = false
				
				// *** finalize process
				status  = .FinalizeProcess(prgCtx, action, jobId, dataCache, job, options, nonFatalErrors, validationErrors)
				if !status.ok
					hasFatalError = true
					log.Error(Str.Tab() + "Error trying to finalize the job")
					
					// *** send email if there are email addresses
					._SendNotification(prgCtx, schedData, options, hasFatalError, status.errMsg, nonFatalErrors, validationErrors, filesProcessed)			
					.SetErrObj(rtn, status)			
					return rtn
				end	
				
				// *** are we supposed to mark this folder after loading to prevent repeats?
				// KM 9/9/15 do this only for recurring jobs
				if options.markAfterLoading && Str.Lower(options.jobType) == "load" && schedData.schedule_type == 2 
				
					._MarkAfterLoading(filesProcessed)
					
				end
				
				// *** need to update status and next run datetime
				status = $DataManager.SchedJobsAPI.UpdateFinishJob(prgCtx, schedData, taskData.SCHEDULEID, hasFatalError, nonFatalErrors, validationErrors)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				end
				
				log.Info(Str.Tab() + Str.Format("Job Completed with %1 errors.", nonFatalErrors))
				
				// *** send email if there are email addresses
				._SendNotification(prgCtx, schedData, options, hasFatalError, status.errMsg, nonFatalErrors, validationErrors, filesProcessed)	
				
				
				return rtn		
			end

	
	public function Assoc FinalizeProcess(Object prgCtx, \
											String action, \
											Integer jobId, \
											Object dataCache, \
											Object job, \
											Assoc options, \
											Integer	nonFatalErrors, \
											Integer validationErrors)
			
				Assoc	rtn = .InitErrObj("ProcessController.FinalizeProcess")
				
				String	returnMsg
				Integer	logLevel = $SyntergyCore.LogPkg.LEVEL_DEBUG
				
				.LogEnter("FinalizeProcess", Parameters(), logLevel, true)	
										
				// *** complete job
				Assoc status = $DataManager.JobInterface.CompleteJob(prgCtx, job)
				if !status.ok
					.SetErrObj( rtn, status )	
					return rtn	
				end
			
				// do load specific stuff, like archive csv files?
				status = ._fApiProcess.Finalize(options)
					
				if nonFatalErrors > 0 || validationErrors > 0
					String  impExpStr = Str.Lower(options.jobType) == "load" ? "Import" : "Export"
			
					// *** send back an error message
					returnMsg = Str.Format("The process completed, but there were errors (%1 %2 errors and %3 validation errors).  The errors have been logged to the folder path '%4.'", nonFatalErrors, impExpStr, validationErrors, options.auditFolderPath)
			
				else
					returnMsg = Str.Format("The process completed successfully with no errors. You can see the results in the audit folder path '%1.'", options.auditFolderPath)
				end
				
				Assoc result
				result.msg = returnMsg
				result.nonFatalErrors = nonFatalErrors
				result.validationErrors = validationErrors
				
				.LogExit("FinalizeProcess", result, $SyntergyCore.LogPkg.LEVEL_INFO)
				
				// *** KM 4-22-16 Close out the success csv and error csv logs
				._fAuditAPI.Close({._fSuccessCsvNdcName, ._fErrorCsvNdcName})
			
				return .SuccessResult(result)
											
			end

	
	public function Assoc Get(Object prgCtx, \
								Integer jobId, \
								String action, \
								String whenStr, \
								Boolean needJob = false, \
								String logName = "process", \
								String logPath = undefined)
			
				Assoc 	rtn = .InitErrObj("Get")
				Assoc  	result	
				String	jobType = "DataManager" + "_" + Str.Capitalize(action)
				Assoc 	status
				Object 	job
			
				// *** get job if needed
			 	if needJob
			 		status  = $DataManager.JobInterface.GetJob(prgCtx, jobId)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					else
						job = status.result	
					end
				end 	
			 
			 	// *** get dataCache
			 	Object dataCache = $DataManager.DataCache.New(prgCtx, jobId, jobType)
					
				// *** get the options
				Assoc options = dataCache.Get(prgCtx, {._fOptionsKey, jobId}, Assoc.AssocType)
				if IsUndefined(options)
					.SetErrObj(rtn, Str.Format("Could not retrieve options for job id %1", jobId))	
					return rtn		
				end
						
				// *** if log name is undefined, maybe datacache has it
				if IsUndefined(logPath)
					logPath = dataCache.Get(prgCtx, {"logPath"}, StringType)
				end
				
				// *** now get a new controller with the params we have
				Object controller = .New(options, options.jobType, options.jobSubType, action, whenStr) 
				if IsUndefined(controller)
					.SetErrObj(rtn, Str.Format("Could not retrieve process for job id %1", jobId))	
					return rtn
				else
					controller.SetLogger(options, logName, logPath) 			
				end
					
				result.job = job
				result.options = options
				result.controller = controller
				result.dataCache = dataCache
				
				return .SuccessResult(result)
			end

	
	public function void Init() 
					
				// Patched by Pat201410150
				if Os.IsTemp(this)
		
					// *** one time setup
					
					Object	factory = $DataManager.ObjectFactory
					Object	allowPkg = $DataManager.AllowPkg
					Assoc 	map
					
					// *** here's how we map these things
					//._fIteratorMap.(jobSubType)
					//._fEventMap.(jobSubType + "_" + whenStr)
					//._fApiProcessMap.(action)	
					//._fAuditApiMap.(jobType)	
					 
					// ******* map iterators
					map = Assoc.CreateAssoc()
					map.("dataids") = factory.GetObject("Iterator", "dataids")
					map.("exportobjects_sync") = factory.GetObject("Iterator", "nodes")
					map.("exportobjects_async") = factory.GetObject("Iterator", "nodes")
					map.("exportmetadata_sync") = factory.GetObject("Iterator", "nodes")
					map.("exportmetadata_async") = factory.GetObject("Iterator", "nodes")
					map.("exportusers_sync") = factory.GetObject("Iterator", "users")	
					map.("exportusers_async") = factory.GetObject("Iterator", "users")		
					map.("loadcsv_sync")	= factory.GetObject("Iterator", "Csv_Sync")
					map.("loadcsv_async")	= factory.GetObject("Iterator", "Csv_ASync")	
					map.("loadfilestructure_sync")	= factory.GetObject("Iterator", "File_Sync")
					map.("loadfilestructure_async")	= factory.GetObject("Iterator", "File_Async")
					map.("loadusers_sync")	= factory.GetObject("Iterator", "Csv_Sync")
					map.("loadusers_async")	= factory.GetObject("Iterator", "Csv_Async")				
					._fIteratorMap = Assoc.Copy(map)
					
					// *** map api process
					map = Assoc.CreateAssoc()
					map.("exportusers") = factory.GetObject("APIProcess", "UserGroup.Export")	
					map.("createusergroup") = factory.GetObject("APIProcess", "UserGroup.Create")	
					map.("updateusergroup") = factory.GetObject("APIProcess", "UserGroup.Update")	
					map.("deleteusergroup") = factory.GetObject("APIProcess", "UserGroup.Delete")			
					map.("exportnodes") = factory.GetObject("APIProcess", "Node.Export")			
					map.("dynamic") = factory.GetObject("APIProcess", "Node.Dynamic")	
					map.("create") = factory.GetObject("APIProcess", "Node.Create")	
					map.("addversion") = factory.GetObject("APIProcess", "Node.AddVersion")	
					map.("move") = factory.GetObject("APIProcess", "Node.Move")	
					map.("copy") = factory.GetObject("APIProcess", "Node.Copy")			
					map.("purge") = factory.GetObject("APIProcess", "Node.PurgeVersions")
					map.("delete") = factory.GetObject("APIProcess", "Node.Delete")		
					map.("update") = factory.GetObject("APIProcess", "Node.Update")	
					map.("perms") = factory.GetObject("APIProcess", "Node.UpdatePerms")					
					map.("rename") = factory.GetObject("APIProcess", "Node.Rename")					
					._fApiProcessMap = Assoc.Copy(map)			
					
					// *******map Integer events for allow pkg
					map = Assoc.CreateAssoc()
					map.("exportfromdataids_sync") = allowPkg.fEVENT_EXPORT_SYNC
					map.("exportfromdataids_async") = allowPkg.fEVENT_EXPORT_SYNC
					map.("exportobjects_sync") = allowPkg.fEVENT_EXPORT_SYNC
					map.("exportobjects_async") = allowPkg.fEVENT_EXPORT_ASYNC
					map.("exportmetadata_sync") = allowPkg.fEVENT_EXPORT_SYNC
					map.("exportmetadata_async") = allowPkg.fEVENT_EXPORT_ASYNC
					map.("exportusers_sync") = allowPkg.fEVENT_EXPORT_USERS
					map.("exportusers_async") = allowPkg.fEVENT_EXPORT_ASYNC	
					map.("loadcsv_sync")	= allowPkg.fEVENT_LOAD_SYNC
					map.("loadcsv_async")	= allowPkg.fEVENT_LOAD_ASYNC
					map.("loadfilestructure_sync")	= allowPkg.fEVENT_LOAD_SYNC
					map.("loadfilestructure_async")	= allowPkg.fEVENT_LOAD_ASYNC
					map.("loadusers_sync")	= allowPkg.fEVENT_LOAD_USERS
					map.("loadusers_async")	= allowPkg.fEVENT_LOAD_ASYNC			
					._fEventMap = Assoc.Copy(map)
					
				end		
		
			end

	
	public function Assoc InitializeJobData(Object prgCtx, \
													Assoc options, \
													String action, \
													Integer estimatedTotal, \
													Assoc result, \
													Integer extId)
													
				Assoc 	status
				Object	dataCache, job
											 	
				// *** first get the job payload
				Assoc jobPayload = ._fApiProcess.NewJobPayload(prgCtx, options, action)
			
				// *** some standard data
				jobPayload.totalCount = estimatedTotal
				jobPayload.completedCount = 0
				jobPayload.startTime = Date.now()	
				jobPayload.endTime = undefined
				
				// *** Create a job for this export that we will use throughout the process
				status = $DataManager.JobInterface.CreateJob(prgCtx, jobPayload, ._fJobType, extId)
				if !status.ok
					return .SetErrObj(.InitErrObj("InitializeJobData"), status)
				else
					result.job = job = status.result
					result.jobId = job.fJobId
				end		
				
				// *** cleanup inactive jobs older than 1 day -- as we create new ones
				$DataManager.DataCache.CleanupType( prgCtx, ._fJobType, 60*60*24 )	
				
				// *** set up a data cache obj for ease of putting and getting data
				result.dataCache = dataCache = $DataManager.DataCache.New(prgCtx, job.fJobId, ._fJobType)	
				
				// *** cache the options
				status = dataCache.Put(prgCtx, {._fOptionsKey, result.jobId}, options)
				if !status.ok
					return .SetErrObj(.InitErrObj("InitializeJobData"), "Could not save options in dataCache " + status.errMsg)
				end			
				
				// *** cache 0 errors so far for this job
				status = dataCache.Put(prgCtx, {'numberOfErrors'}, 0)
				if !status.ok
					return .SetErrObj(.InitErrObj("InitializeJobData"), "Could not save 0 numberOfErrors in dataCache " + status.errMsg)
				end		
		
				// *** cache 0 errors so far for this job
				status = dataCache.Put(prgCtx, {'validationErrors'}, 0)
				if !status.ok
					return .SetErrObj(.InitErrObj("InitializeJobData"), "Could not save 0 validationErrors in dataCache " + status.errMsg)
				end												
				
				// *** now determine the root target
				status = ._fApiProcess.GetTargetRoot(prgCtx, options)
				if !status.ok
					return .SetErrObj(.InitErrObj("InitializeJobData"), status)
				else
					result.rootTargetPath = status.result.path
					result.rootTargetId = status.result.id
				end	
				
				// *** get the root source path
				status = ._fApiProcess.GetSourceRoot(prgCtx, options)
				if !status.ok
					return .SetErrObj(.InitErrObj("InitializeJobData"), status)
				else
					result.rootSource = status.result.root
					result.rootSourceId = status.result.id
					result.rootSourcePath = status.result.rootSourcePath
					result.rootFilePath = status.result.rootFilePath
				end	
			
				return .SuccessResult(result)
			end

	
	public function Assoc InitializeProcess1(Object prgCtx, \
											 List colNames, \
											 String action, \
											 Assoc options, \
											 Integer limit, \
											 Assoc sourceSettings, \
											 Integer extId = 0, \
											 Boolean async = false)
				Assoc 	result
				Assoc 	status
				Object	dataCache
				Integer	logLevel = $SyntergyCore.LogPkg.LEVEL_DEBUG
				Object	log = .GetLogger()
				Boolean	runSingleThreaded = FALSE
		
				if IsFeature(options, "ensureCsvOrder") && options.ensureCsvOrder
					runSingleThreaded = TRUE
				elseif options.jobSubtype == 'loadfilestructure'
					runSingleThreaded = TRUE
				end
		
				// put singleThreadedFlag back into the return result and options
				result.runSingleThreaded = runSingleThreaded
				options.runSingleThreaded = runSingleThreaded
				
				// *** put colNames in options
				options.colNames = colNames
				
				// *** variables we need to get and put in the result at end
				Integer estimatedTotal = IsDefined(options.estimatedTotal) ? options.estimatedTotal : 0
				
				// *** adjust estimated Total if need be
				if options.includeRoot
					estimatedTotal +=1
				end	
				
				// *** KM 6/28/17 added support for single threaded processing for certain types of jobs
				result.loadJobID = 0
				
				if runSingleThreaded
					
					// *** for synchronous single-threaded jobs, we will need a job to monitor job in the UI
					if !async
						Assoc jobPayload
						jobPayload.totalCount = estimatedTotal
						jobPayload.completedCount = 0
						jobPayload.startTime = Date.now()	
						jobPayload.endTime = undefined
					
						// *** Create a job for this export that we will use throughout the process
						status = $DataManager.JobInterface.CreateJob(prgCtx, jobPayload, ._fJobType, extId)
						if !status.ok
							return .SetErrObj(.InitErrObj("InitializeJobData"), status)
						else
							result.loadJobID = status.result.fJobID
						end	
					end
				end
				
				// **** check if we are ok to do this
				status = $DataManager.AllowPkg.CheckAllowed(prgCtx, ._fEvent)
				if !status.ok
					return .SetErrObj(.InitErrObj("ProcessController.InitializeProcess1"), status)
				end		
			
				// *** Initialize our job data
				status = .InitializeJobData(prgCtx, options, action, estimatedTotal, result, extId)
				if !status.ok
					return .SetErrObj(.InitErrObj("ProcessController.InitializeProcess1"), status)
				else
					dataCache = status.result.dataCache	
				end			
			
				// *** now that we have the payload, set the auditAPI with the options.auditFolderPath	
				String previousAuditPath = options.auditFolderPath	
				
				// Set the logger (it will put into a new date-timestamp folder)		
				.SetLogger(options, "validation")
			
				// if audit path changed, save to dataCache
				if previousAuditPath != options.auditFolderPath
			
					// what if we were supposed to archive files in the audit path? change that too then
					if IsFeature(options, "csvArchivePath") && options.csvArchivePath == previousAuditPath
						options.csvArchivePath = options.auditFolderPath
					end
			
					dataCache.Put(prgCtx, {._fOptionsKey}, options, TRUE, TRUE)
				end	
			
				.LogSystemInfo(prgCtx)
				.LogEnter("InitializeProcess1", Parameters(), logLevel, true)
				
				if runSingleThreaded
					log.Debug("Running single threaded")
				else	
					log.Debug("Running multi-threaded")
				end
				
				// *** what's the limit?
				// **** check if we're in an eval state first -- that always trumps whatever limit is passed in
				if $DataManager.License.IsEval()
					result.limit = $DataManager.License.GetEvalLimit()
					log.INFO("Operating with evaluation license. Limit is " + Str.String(result.limit))			
				else
					result.limit = limit
				end		
			
				// *** before we start iterating through the whole process, give the apiProcess a chance to validate the sourcepath, targetpath, csv (as a whole), and other things
				status = ._fApiProcess.ValidateJob(prgCtx, colNames, action, options, result.rootSourcePath, result.rootTargetPath, limit, sourceSettings)
				if !status.ok
					return .SetErrObj(.InitErrObj("ProcessController.InitializeProcess1"), status)
				else
					log.Debug(Str.Tab() + "Result: ValidateJob is successful.")
				end		
				
				.LogExit("InitializeProcess1", result, logLevel)
				
				return .SuccessResult(result)	
										
			end

	
	public function Assoc InitializeProcess2(	Object prgCtx, \
												String action, \
												Assoc options, \
												Integer jobId, \
												Object job, \
												Object dataCache, \
												Dynamic	rootSource, \
												Integer rootSourceId, \
												String 	rootSourcePath, \
												String	rootFilePath, \
												String 	rootTargetPath, \
												Integer rootTargetId, \							 
												Integer limit, \
												Assoc sourceSettings = Assoc.CreateAssoc())
			
			
				Dynamic params = Parameters()
				Assoc 	rtn = .InitErrObj("ProcessController.InitializeProcess2", this, params)
				Assoc 	status
				Object	log = .GetLogger()
				Assoc	dataFlags
				Assoc	lineErrorMap
				Integer	numErrors
				Integer	logLevel = $SyntergyCore.LogPkg.LEVEL_DEBUG
				Boolean	allowSmallerChunks
				
				.LogEnter("InitializeProcess2", params, logLevel, true)			
			
					// *** variables we need to get and put in the result at end
					Integer estimatedTotal = IsDefined(options.estimatedTotal) ? options.estimatedTotal : 0
					List	nodeIdArray
					Assoc	nodeObjects
					Real	average, elapsed
					Integer	minId
					Integer	maxId
					Integer itemsToProcess, itemsInitialized
					List	filesProcessed
					String	actionStr = Str.Upper(options.jobType) == "LOAD" ? "Validation" : "Export"
				
					// *** Instantiate the Iterator as a frame now and call it
					Object iterator = ._fIterator.New(prgCtx, \
														job, \
														action, \
														limit, \
														dataCache, \
														options, \
														rootSourceId, \
														rootFilePath, \
														rootTargetId, \
														rootTargetPath, \
														estimatedTotal, \
														sourceSettings, \
														._fApiProcess)	
					
					// *** now iterate through the items and process them
					status = iterator.IterateWrapper(prgCtx, rootSource, rootSourcePath)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
						
					else
						if status.result.numErrors > 0
							log.Error(Str.Format("Result: %1 had %2 errors.", actionStr, status.result.numErrors))
						elseif status.result.itemsInitialized > 0
							log.Info(Str.Format("Result: %1 is successful.", actionStr))
						end	
					
						// KM 4-22-19 if we're archiving csv, then we need to re-cache the options, which will contain a list of CSV files
						// we iterated through
						if IsFeature(options, "archiveCsvFiles") && options.archiveCsvFiles
							dataCache.Put(prgCtx, {._fOptionsKey}, options, TRUE, TRUE)
						end	
			
						nodeIdArray = status.result.nodeIdArray
						nodeObjects = status.result.nodeObjects
						itemsToProcess = status.result.itemsToProcess
						itemsInitialized = status.result.itemsInitialized
						average = status.result.average
						elapsed = status.result.elapsed
						minId = status.result.minId
						maxId = status.result.maxId
						dataFlags = status.result.dataFlags
						lineErrorMap = status.result.lineErrorMap
						numErrors = status.result.numErrors
						filesProcessed = IsDefined(status.result.filesProcessed) ? status.result.filesProcessed : {}
						
						// *** set a flag for the browser to indicate whether it's allowed to divide up chunks
						allowSmallerChunks = status.result.allowSmallerChunks		
						
					end		
					
					// *** save the totalCount back into the payload
					Assoc jobPayload = job.getPayload()
					jobPayload.totalCount = itemsToProcess
					job.setPayload(jobPayload)		
				
					// *** hand off to the API
					status = ._fApiProcess.Initialize(prgCtx, dataCache, job, options, iterator, limit, sourceSettings)
					if !status.ok
						.SetErrObj( rtn, status )
						return rtn
					end	
				
					// *** put the payload back into job and save
					status = $DataManager.JobInterface.SaveJob(prgCtx, job)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end		
					
					// *** marshall the results to return
					Assoc result
					result.nodeIdArray = nodeIdArray
					result.rootTargetPath = rootTargetPath
					result.limit = limit
					result.jobId = jobId		
					result.itemsToProcess = itemsToProcess
					result.itemsInitialized = itemsInitialized
					result.average = average
					result.elapsed = elapsed
					result.minId = minId
					result.maxId = maxId
					result.nodeObjects = nodeObjects
					result.lineErrorMap = lineErrorMap
					result.numErrors = numErrors
					result.filesProcessed = filesProcessed
					result.allowSmallerChunks = allowSmallerChunks	
				
					// *** save the log name so we can re-use it for the processItem calls
					status = dataCache.Put(prgCtx, {"logPath"}, ._fLogPath)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end	
					
					// *** save the dataFlags so we can re-use them for the processItem calls
					status = dataCache.Put(prgCtx, {"dataFlags"}, dataFlags)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end		
			
				.LogExit("InitializeProcess2", result, logLevel)		
			
				return .SuccessResult(result)	
										
			end

	
	public function void LogEnter(	String funcName, \
											List params, \
											Integer logLevel, \
											Boolean logTimings = true, \
											Integer tabs = 0)
				
				Object	synCore = $DataManager.SynCoreInterface
				Object	statsPkg = $SyntergyCore.Stats
				Object	log = .GetLogger()
				Object	logPkg = $SyntergyCore.LogPkg
				String tabStr = tabs>0 ? Str.Set(tabs, Str.Tab()) : ""
				
				synCore.ClearNDC()
				synCore.PushNDC( ._fNdcProcessLog )
			
				if logLevel <= logPkg.LEVEL_DEBUG
					if logTimings
						statsPkg.Start( ._fPkgName, funcName )	
					end
					log.Log(logLevel, "")
					log.Log(logLevel, tabStr + "Function: " + funcName)
					log.Log(logLevel, Str.Tab() + tabStr + "Parameters: " + Str.String(params) )
				else
					log.Log(logLevel, tabStr + "Function: " + funcName + "(" + Str.String(params) + ")")
				end
			
			end

	
	public function void LogExit(	String funcName, \
												Dynamic result, \
												Integer logLevel, \
												Boolean logTimings = false, \
												Integer	tabs = 0, \
												Boolean extraLine = false)
				
				Object	synCore = $DataManager.SynCoreInterface
				Object	statsPkg = $SyntergyCore.Stats
				Object	log = .GetLogger()	
				
				synCore.ClearNDC()
				synCore.PushNDC( ._fNdcProcessLog )	
				
				String tabStr = tabs>0 ? Str.Set(tabs, Str.Tab()) : ""
				
				if extraLine
					log.Log(logLevel, "")
				end	
				
				log.Log(logLevel, tabStr + Str.Tab() + "Result: " + Str.String(result))
				
				if logTimings
					Dynamic elapsed = statsPkg.Stop( ._fPkgName, funcName )
					log.Log(logLevel, tabStr + Str.Tab() + "Time: " + Str.String(elapsed))	
				end
					
				synCore.ClearNDC()
				
			end

	
	public function LogSystemInfo(Object prgCtx)
																				
				Object 	log = .GetLogger()
				Integer	appServicePackNumber
				Integer	appUpdateNumber
				String  version = "?"
				String  appVersionSubString = "?"
				
				if IsFeature($WebLL,"UpdateFilePrefs") 
					appServicePackNumber = $WebLL.UpdateFilePrefs.GetPrefInteger( "Update", "ServicePackNumber", 0 )
					appUpdateNumber = $WebLL.UpdateFilePrefs.GetPrefInteger( "Update", "UpdateNumber", 0 )
					version = Str.Format( "%1.%2", System.AppVersion(), System.AppBuildNo() )
					appVersionSubString = version[ 1 : Str.Chr( Str.String( System.AppVersion() ) , '.' ) - 1 ]
				end		
				
				log.INFO("")
				log.INFO("*************************************************************************")	
				log.INFO("*** Content Server Info")
				if ( appUpdateNumber == 0 )
					log.INFO(Str.Format(Str.Tab() + "Version: Livelink %1 (version %2)", appVersionSubString, version))
				else
					log.INFO(Str.Format(Str.Tab() + "Version: Content Server %1 Update %2 (version %3)", appVersionSubString, appUpdateNumber, version))
				end
				
				log.INFO(Str.Format(Str.Tab() + "Service Pack: %1", appServicePackNumber ))
				
				Assoc dbInfo = CAPI.ConnectionAttr(prgCtx.fDbConnect.fConnection)
				
				// *** remove sensitive or unnecessary info
				String key
				for key in Assoc.Keys(dbInfo)
					if Length(key) >= 8 && key[1:8]=="Database"
						Assoc.Delete(dbInfo, key)
					end	
				end
				dbInfo.ConnectionType = dbInfo.ConnectionType == CAPI.CT_ORACLE ? "Oracle" : (dbInfo.ConnectionType == CAPI.CT_DBLIB ? "MSSQL" : "Unkown")
				
				log.INFO(Str.Format(Str.Tab() + "Database Info: %1", dbInfo))
				log.INFO("*****")
				log.INFO("")
			
				Object	info = $DataManager.AboutDataManager
				log.INFO("*** DataManager Info")
					log.INFO(Str.Tab() + "Version: " + info.getReleaseString())
					log.INFO(Str.Tab() + "Build Number: " + info.GetLastCommit())
					log.INFO(Str.Tab() + "Schema: " + info.GetSchemaVersion())		
				log.INFO("*****")	
				log.INFO("")	
				
									
			end

	
	public function Object New( Assoc options, \
									String jobType, \
									String jobSubType, \
									String action, \
									String whenStr, \
									String iteratorType = jobSubType + "_" + whenStr)
					
				Frame f = ._NewFrame()
				
				f._fWhenStr = Str.Capitalize(whenStr)
				f._fAction = Str.Capitalize(action)	
				
				// *** logging?	
				f._fDoDeltaLogs = .GetLogger().IsTraceEnabled()
				
				// *** determine event
				f._fEvent = ._fEventMap.(jobSubType + "_" + whenStr)
			
				// *** get the audit api
				f._fAuditAPI = $DataManager.AuditApi.New()
				
				// *** determine the iterator
				f._fIterator = ._fIteratorMap.(iteratorType)
				
				// *** what's the jobType for datacache?
				f._fJobType = "DataManager" + "_" + Str.Capitalize(action)
			
				// *** get the ApiProcess
				f._fApiProcess = ._fApiProcessMap.(action).New(options)			
				
				if IsUndefined(f._fEvent)  || IsUndefined(f._fIterator) || IsUndefined(f._fApiProcess) || IsUndefined(f._fJobType) || IsUndefined(f._fAuditAPI)
					return undefined
				end
				
				return f
				
			end

	
	public function Assoc ProcessChunk(Object prgCtx, \
												String action, \
												List items, \
												Object dataCache, \
												Assoc options, \
												Integer loadJobID)
				
				Assoc 	status	
				Assoc	item
				Object 	synCore = $DataManager.SynCoreInterface
			    Integer	counter		= 0
			   	Integer	N = Length(items)
			   	Integer	updateIncrement = Math.Min(Math.Max(1,N/10), 20)
			   	Object	log = $DataManager.SynCoreInterface.GetLogger()
				Object	jobInterface = $DataManager.JobInterface
				Object 	job
				Integer lastCounter, increment
				
				Boolean runSingleThreaded = options.runSingleThreaded
				
				if (runSingleThreaded)
						
					// *** put the payload back into job and save
					status = $DataManager.JobInterface.GetJob(prgCtx, loadJobID)	
					if !status.ok
						return .SetErrObj(.InitErrObj("ProcessChunk"), status)
					else
						job = status.result	
					end
				
				end
				
				
				// *** Loop through the items and call .ProcessItem for each one
				for item in items
					status = .ProcessItem(prgCtx, \
										action, \
										item.id, \
										dataCache, \
										options)
					if status.ok
						item.ok = true
						item.successMsg = status.result
					else
						item.ok = false
						item.errMsg = status.errMsg
					end
			
					counter += 1
					
					// *** need to provide updates for single-threaded jobs every X items
					if runSingleThreaded &&  ((counter % updateIncrement == 0) || counter==N)
					
						synCore.ClearNDC()
						synCore.PushNDC( "DataManagerProcessLog" )
						log.Debug(Str.Format("updating completed count to %1", counter))
						synCore.ClearNDC()
						
						increment = counter - lastCounter
						
						// *** update now
						jobInterface.IncrementCompletedCount(prgCtx, job, increment, false)						
						
						lastCounter = counter			
					end
					
				end	
				
				if runSingleThreaded
					status = jobInterface.CompleteJob(prgCtx, job)
					if !status.ok
					    .SetErrObj(.InitErrObj("ProcessChunk"), status)
					end	
				end
						
				Assoc rtn = .SuccessResult(items)
			
				return rtn
										
			end

	
	public function Assoc ProcessItem(Object prgCtx, \
											String action, \
											Integer id, \
											Object dataCache, \
											Assoc options)
										
				Assoc 		status	
				Assoc 		csvData, actionFlags, dataFlags, oldCsvData
				String 		actionPerformed
				Frame		apiDataObj, oldDataObj, newDataObj
				Object		logger = .GetLogger()
				String		successCsvPath = ._fLogPath + "successItems.csv"
				String		errorCsvPath = ._fLogPath + "errorItems.csv"
				DAPINODE 	node = undefined
				Assoc		ugData = undefined
				Integer		ugId, nodeId
				Object		logPkg = $SyntergyCore.LogPkg
				Integer		logLevel = logPkg.LEVEL_DEBUG
				Assoc 		rtn
				
				// *** get the csvData
				status = ._fIterator.GetCachedData(prgCtx, dataCache, id)
				if !status.ok
					return .SetErrObj(.InitErrObj("ProcessController.ProcessItem"), status)
				else
					dataFlags = status.result.dataFlags
					csvData = status.result.csvData
					
					// do we need to set a dynamic action?
					if Str.Upper(action) == "DYNAMIC"
						if IsDefined(csvData.("$Action"))
							action = Str.Upper(csvData.("$Action"))
							._fApiProcess = ._fApiProcessMap.(action).New(options)	
						end							
					end
					
					apiDataObj = $DataManager.ApiDataObj.New(action, status.result.apiVals, csvData)
					actionFlags = status.result.actionFlags
					oldCsvData = status.result.oldCsvData
					oldDataObj = IsDefined(status.result.oldVals) ? $DataManager.ApiDataObj.New(action, status.result.oldVals, oldCsvData) : undefined
					ugId = status.result.ugId
					nodeId = status.result.nodeId
				end	
				
				// *** Initial logging
				if logger.IsDebugEnabled()
					.LogEnter("ProcessItem", {id, action, status.result.apiVals}, logPkg.LEVEL_DEBUG, true, 1)	
				end	
				
				// *** if update and we're logging delta values, try to get the previous vals (it may be create action, in which case we won't have any)
				if ._fDoDeltaLogs && IsUndefined(oldDataObj)
					if IsDefined(ugId)
						oldDataObj = $DataManager.ApiDataObj.New(action)
						oldDataObj.PopulateFromUgID(prgCtx, ugId, {})
					elseif IsDefined(nodeId)
						oldDataObj = $DataManager.ApiDataObj.New(action)
						oldDataObj.PopulateFromNode(prgCtx, nodeId, dataFlags, {}, false)
					end	
				end		
				
				// stash the dataFlags the he options in case we need it
				options.dataFlags = dataFlags	
		
				// *** hand off to ProcessItem to handle doing the work
				status = ._fApiProcess.ProcessItem(	prgCtx, \
													action, \
													apiDataObj, \
													actionFlags, \
													options)
													
				actionPerformed = IsDefined(status.result) && IsDefined(status.result.actionPerformed) ? status.result.actionPerformed : "No action performed."
				
				if status.ok
					// *** Log to the success.csv file
					csvData.("#Result") = "Success"
					csvData.("#ActionSpecified") = action
					csvData.("#ActionPerformed") = actionPerformed
					csvData.("#EventDetails") = "See Audit log for more details."
					csvData.("#itemNo") = id		
					
					if IsFeature(status.result, "node")
						node = status.result.node
						csvData.("#nodeId") = node.pId
						
						status = ._GetLastAuditEntries(prgCtx, status.result.nodeId, status.result.targetItem)
						if status.ok
							String	auditStr
							Record rec
							for rec in status.AuditRecs
								auditStr += rec.EventDetail	+ "|"
							end
							if IsDefined(auditStr) && Length(auditStr)
								csvData.("#EventDetails") = auditStr[:-2]
							end	
						end	
					elseif IsFeature(status.result, "ugData")
						ugData = status.result.ugData
						csvData.("#userGroupId") = ugData.ID
					end			
				
					// *** log to success.csv file
					._fAuditAPI.LogCsv(._fSuccessCsvNdcName, successCsvPath, csvData)
			
					
					// *** do delta logging?
					if ._fDoDeltaLogs
						if IsDefined(ugData)
							newDataObj = $DataManager.ApiDataObj.New(action)
							//newDataObj.PopulateFromUgData(prgCtx, ugData, {})
										
						elseif IsDefined(node)
							newDataObj = $DataManager.ApiDataObj.New(action)
							newDataObj.PopulateFromNode(prgCtx, node, dataFlags, {}, false)
			
						end	
						._fAuditAPI.LogDeltas(prgCtx, options, ._fLogPath, oldDataObj, apiDataObj, newDataObj, id)
					end
					
				else
				
					// *** Log to the errorItems.csv file
					rtn = .InitErrObj("ProcessController.ProcessItem")	
					.SetErrObj(rtn, status )	
					if !logger.IsDebugEnabled()	
						.LogEnter("ProcessItem", Parameters(), $SyntergyCore.LogPkg.LEVEL_ERROR, false, 1)	
					end			
					logger.Error(Str.Tab() + Str.Tab() + rtn.errMsg)
					
					csvData.("#ErrMsg") = rtn.errMsg
					csvData.("#ActionSpecified") = action
					csvData.("#ActionPerformed") = actionPerformed
					csvData.("#itemNo") = id
					if IsFeature(status, "result") && IsFeature(status.result, "nodeId")
						csvData.("#nodeId") =  status.result.nodeId
					end	
					
					// *** log the error in the datacache
					dataCache.IncrementInt(prgCtx, {'numberOfErrors'}, 1)
					
					// *** log to error.csv file		
					._fAuditAPI.LogCsv(._fErrorCsvNdcName, errorCsvPath, csvData)
					
					return rtn	
				end	
				
				
				rtn.ok = true
				rtn.result = actionPerformed
				
				// *** Final Logging
				if logger.IsDebugEnabled()
					.LogExit("ProcessItem", rtn.result, logLevel, true, 1)
				end	
					
				return rtn
										
			end

	
	public function Assoc RunAsync( Object adminCtx, \
									 String action, \
									 Assoc options, \
									 Assoc schedData)
																 
				Dynamic params = Parameters()
				Assoc 	rtn = .InitErrObj("ProcessController.RunAsync", this, params)
				Assoc 	status
				Assoc	jobData
				Object 	job	
				Object	jobInterface = $DataManager.JobInterface
				Object 	prgCtx
				Object	schedAPI = $DataManager.SchedJobsAPI
				Integer	numberOfErrors = 0
											
				// *** upsdate job to INITIALIZING status for load RUNNING status for Export
				String jobStatus = (options.action == 'exportnodes' || options.action == 'exportusers') ? schedAPI.fSTATUS_RUNNING : schedAPI.fSTATUS_INITIALIZING
				status = schedAPI.UpdateJobStatus(adminCtx, jobStatus, schedData.SCHEDULEID)
				if !status.ok
					.SetErrObj(rtn, status)
					.GetLogger().Error(Str.Format("%1Error trying to update job status to INITIALIZING", Str.Tab()))
					return rtn
				end	
											
				// *** get the program context for this job
				status = .GetAdminCtx(options.runAsUserName)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn	
				else
					prgCtx = status.prgCtx
				end
										
				// *** double-scheck the limit
				Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_ASYNC_LIMIT", 1000000)
										
				// *** initialize
				status = ._InitializeProcessAsync(prgCtx, action, options, schedData.SCHEDULEID, limit)
				if !status.ok
					$DataManager.SchedJobsAPI.UpdateJobStatus(prgCtx, schedAPI.fSTATUS_ERROR, schedData.SCHEDULEID)
												
					// *** KM 11-13-17 We should not send a notification here for recurring job because recurring jobs will continually send these emails
					if options.markAfterLoading && Str.Lower(options.jobType) == "load" && schedData.schedule_type == 2 && IsDefined(Str.LocateI(status.errMsg, "does not exist"))
						// do nothing
						.GetLogger().Info("Skipping Notification for 0 items")
					else
						// *** send email if there are email addresses
						._SendNotification(prgCtx, schedData, options, true, status.errMsg)					
					end
								
					.SetErrObj(rtn, status)		
					return rtn				
										
				else
					numberOfErrors = status.numberOfErrors
					jobData = status.result	
												
				end
											
				// *** check to see if anything was initialized
				if jobData.itemsInitialized == 0 
					.GetLogger().Warn("No items found for this job.")
					$DataManager.SchedJobsAPI.UpdateFinishJob(prgCtx, schedData, schedData.SCHEDULEID, false)
												
					if schedData.schedule_type == 2
						// do nothing
						.GetLogger().Info("Skipping Notification for 0 items")					
					else
						// *** send email if there are email addresses
						._SendNotification(prgCtx, schedData, options, false, "No items found for job", 0, 0, {}, "Note: Nothing was loaded/exported because no items were found for this job.")						
					end							
					return rtn		
				end
											
				// *** create our task data
				Assoc taskData
				taskData.jobId = jobData.jobId
				taskData.maxID	= jobData.maxId
				taskData.minID	= jobData.minId
				taskData.total	= jobData.itemsToProcess
			  	taskData.options = options
			  	taskData.action = action
			  	taskData.extId 	= schedData.SCHEDULEID
			  	taskData.scheduleId = schedData.SCHEDULEID
			 	taskData.dataCacheKey = ._fIterator.fItemDataKey
			 	taskData.jobType = ._fJobType
			 	taskData.logPath = ._fLogPath
				taskData.schedData = schedData
				taskData.split = false
				taskData.filesProcessed = jobData.filesProcessed
											
				// ** data cache
				Object dataCache = jobData.dataCache
											
				// *** Job
				job = jobData.job	
											
				// *** Exports do not need map/reduce. Just finalize
				if options.action == 'exportnodes' || options.action == 'exportusers'
					// *** call finalize
					status = .FinalizeAsync(prgCtx, taskData, taskData.action, taskData.jobId, dataCache, job, options, taskData.filesProcessed, numberOfErrors)
					if !status.ok
						// *** send email if there are email addresses
						._SendNotification(prgCtx, schedData, options, true, status.errMsg)	
						.SetErrObj(rtn, status)
						return rtn			
					end	
										
				// *** pass this task off to the Distributed Agent Map function		
				else
										
					// *** reset the completed count to 0
					$DataManager.JobInterface.UpdateCompletedCount(prgCtx, job, 0)
											
					// *** if we this far, we can set the job status to running
					// *** update status to RUNNING
					status = $DataManager.SchedJobsAPI.UpdateStartJob(prgCtx, schedData, schedData.SCHEDULEID)
					if !status.ok
						// *** send email if there are email addresses
						._SendNotification(prgCtx, schedData, options, true, status.errMsg, 0, 0, {}, "Error trying to update job status to RUNNING.")
						.SetErrObj(rtn, status)
						.GetLogger().Error(Str.Format("%1Error trying to update job status to RUNNING", Str.Tab()))
						return rtn
					end
										
					.GetLogger().Info("Handing off to Distrubuted Agents.Total number of items to process is " + Str.String(taskData.total) + ".")
					status = ._MapDistribAgentTask( prgCtx, "DataManagerJob", taskData )
					if !status.ok
						// *** need to update status with fatal error flag
						jobInterface.SetErrorOnJob(prgCtx, job, rtn)			
						$DataManager.SchedJobsAPI.UpdateFinishJob(prgCtx, schedData, schedData.SCHEDULEID, true, 0)
						// *** send email if there are email addresses
						._SendNotification(prgCtx, schedData, options, true, status.errMsg)			
						.SetErrObj(rtn, status)				
						return rtn
					end					
				end					
											
				return rtn		
																	
			end

	
	override function void Scratch()
													
				Assoc options
				options.loadDirPath = "C:\ImportExport\ScheduledCsvLoad\"
			
				File	theFile = File.StringToFile(options.loadDirPath)
				
				if IsNotError(theFile)
					
					String	parentPath = $DataManager.FileUtils.GetParentPath(options.loadDirPath)
					String newName = parentPath + File.GetName(theFile)
					newName = newName[-1] == File.Separator() ? newName[1:-2] : newName
					newName += "_PROCESSED"
				
					Dynamic result = File.Rename(theFile, newName)
					
					if !result
						echo(Str.Format("Could not rename folder '%1' to '%2.' Error is '%3.'", options.loadDirPath, newName, result))
					end
				else
					echo(Str.Format("Could not get folder at path '%1' for renaming'", options.loadDirPath))
				end
					
										
			
			end

	
	public function void SetLogger(Assoc options, \
											String logName, \	
											String logPath = undefined, \
											String appenderNdcName = ._fNdcProcessLog)
					
				Object synCore = $DataManager.SynCoreInterface
		
				if IsDefined(logPath)
					._fLogPath = logPath
				else
		
					if ._fAction[1:6] == "Export"
						._fLogPath = logPath = options.auditFolderPath
						logName = "export.log"	
					else
						// *** set up the log	
						String	dateTimeStamp = Date.DateToString(Date.Now(), "%Y-%m-%dT%H-%M-%S")
						._fLogPath = logPath = Str.Format("%1%2_%3_%4%5", options.auditFolderPath, ._fAction, ._fWhenStr, dateTimeStamp, File.Separator())
						options.auditFolderPath = logPath
					end	
				
				end					
		
				String fullPath = Str.Format(logPath + logName + "_%1.log", System.ThreadIndex())
		
				synCore.ClearNDC()
				synCore.PushNDC(appenderNdcName)
				Object logger = .GetLogger()
				if IsDefined(logger) && IsFeature(logger.fLogAppenders, appenderNdcName)
					
					Object appender = logger.fLogAppenders.( appenderNdcName )
					
					appender.SetFilename( fullPath )
				end	
				
			end

	
	private function String _FormatList(List l, \
												String indent = '')
			
				String	s	
				Dynamic item
				
				for item in l
					s += Str.Format( "%1%2%3", indent, item, Str.EOL() )	
				end
				
				return s
					
			end

	
	private function String _GetEmailBody(Assoc schedData, \
											Assoc options, \
											Boolean hasFatalError, \
											String errMsg, \
											Integer nonFatalErrors, \
											Integer validationErrors, \
											List filesProcessed, \
											String note, \
											Boolean hasSuccessFile, \
											Boolean hasErrorFile, \
											Boolean hasValidationFile)
			
				String line1, line2, line3
				
				if hasFatalError
				
					line1 = Str.Format('Job "%1" failed with fatal error: %2.', schedData.JOBNAME, errMsg)
					if hasErrorFile
						line3 = "Check the attached errorItems.csv audit files for more details."
					else
						line3 = Str.Format("Check the logs (%1) for more details.", options.auditFolderPath)
					end	
				
				elseif nonFatalErrors > 0 || validationErrors > 0
			
					String  impExpStr = Str.Lower(options.jobType) == "load" ? "Import" : "Export"
			
					line1 = Str.Format('Job "%1" completed with %2 %3 error(s)', schedData.JOBNAME, nonFatalErrors, impExpStr)
			
					if validationErrors > 0
						line1 += Str.Format(' and %1 validation Errors.', validationErrors)
					else
						line1 += '.'	
					end
				else
					line1 = Str.Format('Job "%1" completed SUCCESSFULLY with NO ERRORS.', schedData.JOBNAME)
				end
			
				if hasSuccessFile || hasErrorFile || hasValidationFile
					line3 = Str.Format("Check the attached csv file(s) or the logs folder (%1) for more details.", options.auditFolderPath)
				else
					line3 = Str.Format("Check the logs folder (%1) for more details.", options.auditFolderPath)	
				end
			
				Integer numFiles = Length(filesProcessed)
				if numFiles>0
					if numFiles < 10
						if numFiles>1
							line2 = Str.Format("%1 files were processed:", numFiles)
						else
							line2 = Str.Format("%1 file was processed:", numFiles)
						end	
						line2 += Str.EOL() + ._FormatList(filesProcessed, Str.Tab() + Str.Tab())
					else
						line2 = Str.Format("%1 files were processed.", numFiles)
					end
				end	
				
				
				if Length(line2) > 0
					return line1 + Str.EOL() + Str.EOL() + line2 + Str.EOL() + Str.EOL() + line3
				else
					return line1 + Str.EOL() + Str.EOL() + line3
				end	
			
			end

	
	private function Assoc _GetLastAuditEntries(Object prgCtx, \
													Integer nodeId, \
													Integer targetItem)
												
				Assoc fetchInfo
				
				fetchInfo.NumRows = 1
				fetchInfo.TotalNoRows = 1
				fetchInfo.PageNo = 1
				
				Date	now = CAPI.Now( prgCtx.fDBConnect.fConnection )
				
			
			//	filterInfo		Assoc of filterInfo on events to be reported
			//	fromDate	only show events after this date
			//	toDate		only show events before this date
			//	event		only show events of this type
			//	performer	only show events performed by this user
			//	domain		only show events performed by this users from this domain
			//	subType		only show events on nodes of this type
			//	userOnly	only show events on users and groups
			//	nodeID		only show events on this node
			//	targetItem	target type, 0=all, 1=objType, 2=nodeID, 3=userID
			//	performer	performer type, 0=all, 1=userID, 2=domainID
			//	performerID	only show events performed by this user	
			
				Assoc filterInfo
				filterInfo.fromDate = now - 5
				filterInfo.toDate = now
				filterInfo.performerID = prgCtx.USession().fUserID
				filterInfo.nodeID = nodeId
				filterInfo.targetItem = targetItem
			
					
				Assoc result = $LLIAPI.AuditUtil.LoadPagedAdminEvents( prgCtx, fetchInfo, filterInfo )	
				
				return result
				
			end

	
	private function Assoc _InitializeProcessAsync(Object prgCtx, \
												 String action, \
												 Assoc options, \
												 Integer jobId, \
												 Integer limit)
											 
				Assoc 	rtn = .InitErrObj("ProcessController._InitializeProcessAsync")
				Assoc 	status
			
				
				// *** initialize some extra vals in the rtn
				rtn.hasFatalError = false
				rtn.exceedsThreshold = false
				rtn.validationErrorCount = 0
				
				
				// *** do initialize1, which is fast
				status = .InitializeProcess1(prgCtx, {}, action, options, limit, undefined, jobId, true)
				if !status.ok
					.SetErrObj(rtn, status)
					// *** KM 2-8-16 Set any error here as a fatal error.
					rtn.hasFatalError = true
					return rtn	
				else
			
					// *** marshall the results			
					Object 	job = status.result.job
					Dynamic	rootSource = status.result.rootSource
					Integer rootSourceId = status.result.rootSourceId
					String 	rootSourcePath = status.result.rootSourcePath
					String	rootFilePath = status.result.rootFilePath
					String 	rootTargetPath = status.result.rootTargetPath
					Integer rootTargetId = status.result.rootTargetId
					jobId = status.result.jobId
					Object	dataCache = status.result.dataCache
					
					// *** hand off to ProcessController for the more lengthy iteration part
					status = .InitializeProcess2(prgCtx, \
												action, \
												options, \
												jobId, \
												job, \
												dataCache, \
												rootSource, \
												rootSourceId, \
												rootSourcePath, \
												rootFilePath, \
												rootTargetPath, \
												rootTargetId, \							 
												limit)
											
					if !status.ok
						.SetErrObj(rtn, status)
						$DataManager.JobInterface.SetErrorOnJob(prgCtx, job, rtn)
						return rtn
					
					elseif status.result.itemsInitialized == 0 && Str.Lower(options.jobType) == "load"
					
						.GetLogger().Warn("No items found for this job.")
						$DataManager.JobInterface.CompleteJob(prgCtx, job)
						return status	
								
					else
						Integer numberOfErrors = status.result.numErrors
			
						// *** cache the number of errors so far for this job
						dataCache.Put(prgCtx, {'validationErrors'}, numberOfErrors, true)					
			
						Integer errorThreshold = $DataManager.ModulePrefs.GetPref("VALIDATION_ERROR_THRESHOLD", 25)	
			
						// *** determine if number of errors exceed threshhold
						// *** KM 9-10-15 previously this balked at any errors -- insure it only balks at 25% threshold
						if numberOfErrors >= (status.result.itemsInitialized * (errorThreshold * 0.01))
			
							Object	synCore = $DataManager.SynCoreInterface
							synCore.ClearNDC()
							synCore.PushNDC( ._fNdcProcessLog )
			
							.SetErrObj(rtn, Str.Format("%1 items in the load had validation errors. This meets or exceeds the %2 percent threshold for validation errors. Therefore, the job will not run.", numberOfErrors, errorThreshold))
							
							Object	log = .GetLogger()
							log.ERROR(rtn.errMsg)
							
							$DataManager.JobInterface.SetErrorOnJob(prgCtx, job, rtn)
							
							synCore.ClearNDC()
						else
							rtn.result = status.result
							rtn.result.dataCache = dataCache
							rtn.result.job = job
						end	
			
						// regardless set the error count in case it's needed
						rtn.numberOfErrors = 	numberOfErrors		
			
					end	
								
				end										
				
				return rtn
				
			end
			
	private function Assoc _MapDistribAgentTask(Object prgCtx, String handlerID, Dynamic taskData)
		
		Integer		priority			= Undefined
		Date 		activationDate		= Date.Now()
		
		Assoc checkVal = $DISTRIBUTEDAGENT.Utils.GetNewTaskID( prgCtx )
				
		if checkVal.ok
		
			Integer taskID = checkVal.taskID		
		
			
			Assoc	task = Assoc{'userID': prgCtx.GetUserID(),
									  'spaceID': prgCtx.fDBConnect.UserInfo().spaceID,
									   'handlerID': handlerID,
									   'taskData': taskData,
									   'priority': priority,
									   'activationDate': activationDate,
									   'taskID': taskID }
									   

			checkVal = $DISTRIBUTEDAGENT.TaskTypeSubsystem.GetValidItem( $Datamanager.fTaskTypeDMMap )
	
			if ( checkVal.ok == TRUE )
	
				Object mapper = checkVal.item
				
				checkVal = mapper.Schedule(prgCtx,
										  task.handlerID,
										  $DISTRIBUTEDAGENT.fUNDEFINED_WORKER_TASKID,
										  task.taskData,
										  task.priority,
										  task.activationDate,
										  Undefined,
										  task.taskID,
										  task.userID,
										  task.spaceID )
				
				if ( checkVal.ok != TRUE )
	
					EchoError(
						Str.Format(
							"DataManagerTask: Scheduling failed for task '%1' (Error: %2) (Message: %3).",
							task,
							checkVal.apiError,
							checkVal.errMsg ) )
							
					return .SetErrObj(.InitErrObj("_MapDistribAgentTask"), checkVal)		
	
				end									  
										  
	
			else
	
				EchoError(
					Str.Format(
						"DataManagerTask: Could not get mapper for task type '%1' (Error: %2) (Message: %3).",
						task.handlerID,
						checkVal.apiError,
						checkVal.errMsg ) )
						
				return .SetErrObj(.InitErrObj("_MapDistribAgentTask"), checkVal)			
	
			end
		else
			
			return .SetErrObj(.InitErrObj("_MapDistribAgentTask"), checkVal)

		end
									   		
		return Assoc{'ok': true}
	end			

	
	private function void _MarkAfterLoading(List filesProcessed)
														
				Object log = .GetLogger()
				String eachFile
			
				for eachFile in filesProcessed
					
					File	theFile = File.StringToFile(eachFile)
					
					if IsNotError(theFile)
						
						String	parentPath = $DataManager.FileUtils.GetParentPath(eachFile)
						String newName = parentPath + File.GetName(theFile)
						newName = newName[-1] == File.Separator() ? newName[1:-2] : newName
						newName += "_PROCESSED"
					
						Dynamic result = File.Rename(theFile, newName)
						
						if !result
							log.Error(Str.Format("Could not rename file/folder '%1' to '%2.' Error is '%3.'", eachFile, newName, result))
						end
						
					else
						log.Error(Str.Format("Could not get folder at path '%1' for renaming'", eachFile))
					end
					
					
				end
			
			end

	
	private function void _SendNotification(Object prgCtx, \
											Assoc schedData, \
											Assoc options, \
											Boolean hasFatalError = false, \
											String errMsg = "", \
											Integer nonFatalErrors = 0, \
											Integer validationErrors = 0, \
											List filesProcessed = {}, \
											String note = "")	
					
					if Length(schedData.NOTIFY_EMAILS)
									
						List 	recipients = Str.Elements(schedData.NOTIFY_EMAILS, ",")
						String 	subject
						String  impExpStr = Str.Lower(options.jobType) == "load" ? "Import" : "Export"
						
						if hasFatalError
							subject = Str.Format('Data Manager Job "%1" Failed', schedData.JOBNAME)
						
						elseif nonFatalErrors > 0 || validationErrors > 0
			
							subject = Str.Format('Data Manager Job "%1" Completed with %2 %3 Error(s)', schedData.JOBNAME, nonFatalErrors, impExpStr)
						
							if validationErrors > 0	
								subject += Str.Format(" and %1 Validation Error(s)", validationErrors)
							end
			
						else 
							subject = Str.Format('Data Manager Job "%1" Completed Successfully with No Errors', schedData.JOBNAME)
						end
						
						// Get the attachments
						List attachments
						Boolean hasSuccessFile, hasErrorFile, hasValidationFile
						
						String successPath = options.auditFolderPath + "successItems.csv"
						if (File.Exists(successPath))
							Assoc a1
							a1.fileName = successPath	
							a1.name = "successItems.csv"
							a1.mimeType = "text/csv"
							attachments={@attachments, a1}
							hasSuccessFile = true			
						end
				
						String errorPath = options.auditFolderPath + "errorItems.csv" 
						if (File.Exists(errorPath))
							Assoc a2
							a2.fileName = errorPath	
							a2.name = "errorItems.csv"
							a2.mimeType = "text/csv"
							attachments={@attachments, a2}	
							hasErrorFile = true		
						end		
			
						String validationPath = options.auditFolderPath + "validationResults.csv" 
						if (File.Exists(validationPath))
							Assoc a3
							a3.fileName = validationPath	
							a3.name = "validationResults.csv"
							a3.mimeType = "text/csv"
							attachments={@attachments, a3}	
							hasValidationFile = true
						end	
						
						String	body 	= ._GetEmailBody(schedData, options, hasFatalError, errMsg, nonFatalErrors, validationErrors, filesProcessed, note, hasSuccessFile, hasErrorFile, hasValidationFile) 
						
						.GetLogger().INFO(Str.Format("Sending Email. Recipents=%1%2, Subject=%3%2", recipients, Str.EOL(), subject))					
													
						$DataManager.SchedJobsAPI.SendMessageWithAttachments(prgCtx, \
																		 	recipients, \
																			subject, \
																			body, \
																			attachments)
					
					end
				end

end

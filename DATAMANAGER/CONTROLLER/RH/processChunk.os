package DATAMANAGER::CONTROLLER::RH

public object processChunk inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'action', -1, 'action', FALSE }, { 'jobId', 2, 'JOBID', FALSE }, { 'items', -2, 'items', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
								
				Object		synCore = $DataManager.SynCoreInterface
				Assoc		rtn = synCore.InitErrObj( "ProcessItem.DoExec", this )
				Object		prgCtx = .fPrgCtx
				Assoc		status
			
				/**
				 * R may contain: 
				 *  action				=> String	=> AddVersion, Create, Update, Move, Rename, UpdatePerms
			   	 *  jobId				=> Integer 	=> JobId to retrieve data for this job
			   	 *	items				=> List		=> List of assoces with references to the objects data in the dataCache 
			   	 *  loadJobID			=> Integer 	=> JobId to retrieve for monitoring performance 
			     **/	
				
			    // *** get the process controller
			    status = $DataManager.ProcessController.Get(prgCtx, r.jobId, r.action, "sync", true, "process")
			    if !status.ok
					synCore.SetErrObj(rtn, status)
					.fError = rtn    	
			    else
			    	Object controller = status.result.controller 
			    	    
					// *** hand off to ProcessController
					status = controller.ProcessChunk(prgCtx, Str.Upper(r.action), r.items, status.result.dataCache, status.result.options, r.jobID)
					if !status.ok
						synCore.SetErrObj(rtn, status)
						.fError = rtn
					else
						.fContent = synCore.SuccessResult(status.result)
					end	 	
					
					// *** close out any logs, etc.	
					// *** KM 1-4-15 don't close out of the log because we may need it later for this thread
					controller.Close(false)
				
				end
				
				return undefined	
			End

	
	override function void SetPrototype()
								
								.fPrototype =\
									{\
										{ "action", StringType, "action", FALSE }, \
										{ "jobId", IntegerType, "JOBID", FALSE }, \			
										{ "items", ListType, "items", FALSE }, \
										{ "runSingleThreaded", BooleanType, "runSingleThreaded", FALSE} \
									}
							end

end

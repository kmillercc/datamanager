package DATAMANAGER::CONTROLLER::RH

public object checkJobStatus inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'jobId', 2, 'JOBID', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "CheckJobStatus.DoExec", this )
							Object		prgCtx = .fPrgCtx
							Assoc		status
							
							/**
							 * R may contain: 
						   	 *  jobId			=> Integer 	=> JobId to retrieve data for this job
						     **/
						     
						    // *** get the process controller
						    status = $DataManager.ProcessController.CheckJobStatus(prgCtx, r.jobId)
						    if !status.ok
								synCore.SetErrObj(rtn, status)
								.fError = rtn    	
						    else	     
								.fContent = synCore.SuccessResult(status.result)
							end
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "jobId", IntegerType, "JOBID", FALSE } \
								}
						end

end

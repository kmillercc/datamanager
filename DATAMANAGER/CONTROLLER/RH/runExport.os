package DATAMANAGER::CONTROLLER::RH

public object runExport inherits DATAMANAGER::JSONEnabled
	
	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'action', -1, 'action', FALSE }, { 'optionStr', -1, 'optionStr', FALSE }, { 'sourceSettings', -18, 'sourceSettings', FALSE }, { 'colNames', -2, 'colNames', TRUE, {  } } }
	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r)
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "InitializeProcess.DoExec", this )
							Object		prgCtx = .fPrgCtx
							
							/**
							 * R may contain: 
							 * 	action 	=> String 	=> the action we are performing	 
							 * 	optionStr 	=> String 	=> Options to control what happens	 
							 *  sourceSettings => Assoc => settings for the load (hidden cols, pushDownVals, etc)
							 *	colNames	=> List => List of column names either to export or to load (colNames get validated before each item is validated -- avoids repetetive checks for missing columns) 
							*/	 
						     
							Assoc 	options = ._ParseOptions(r.optionStr, true)
							
							String iteratorType
							
							if IsFeature(options, "dataIdFilePath") && IsDefined(options.dataIdFilePath) && Length(Str.Trim(options.dataIdFilePath)) > 0 
								
								iteratorType = "dataids"							
								
							else
								
								iteratorType = 	options.jobSubType + "_" + "sync"	
								
							end
							
						    
						    // *** get the process controller
						    Object controller = $DataManager.ProcessController.New(options, options.jobType, options.jobSubType, r.action, "sync", iteratorType) 
						    
						    if IsUndefined(controller)
						    	synCore.SetErrObj(rtn, Str.Format("Cannot map process for jobType:%1 jobSubType:%2 action:%3 when:%4.'", options.jobType, options.jobSubType, r.action, "sync"))	
						    	.fError = rtn
							else
							
								Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)
							
								// *** do initialize1, which is fast
								Assoc status = controller.InitializeProcess1(prgCtx, r.colNames, Str.Upper(r.action), options, limit, r.sourceSettings)
								if !status.ok
									synCore.SetErrObj(rtn, status)
									// ** any error we got here is fatal
									rtn.errMsg = "FATAL: " + rtn.errMsg			
									.fError = rtn		
								else
						
									Assoc result = status.result
									
									// *** return to browser to call status update using jobId
									.fContent = synCore.SuccessResult(result)
									.BrowserBegone( ctxIn, ctxOut, r)
						
									// *** marshall the results			
									Object 	job = result.job
									Dynamic	rootSource = result.rootSource
									Integer rootSourceId = result.rootSourceId
									String 	rootSourcePath = result.rootSourcePath
									String	rootFilePath = result.rootFilePath
									String 	rootTargetPath = result.rootTargetPath
									Integer rootTargetId = result.rootTargetId
									Integer jobId = result.jobId
									Object	dataCache = result.dataCache
									
									// reset the limit
									limit = result.limit
						
									// *** hand off to ProcessController for the more lengthy iteration part
									status = controller.InitializeProcess2(prgCtx, \
																			Str.Upper(r.action), \
																			options, \
																			jobId, \
																			job, \
																			dataCache, \
																			rootSource, \
																			rootSourceId, \
																			rootSourcePath, \
																			rootFilePath, \
																			rootTargetPath, \
																			rootTargetId, \							 
																			limit, \
																			r.sourceSettings)													
									if !status.ok
										synCore.SetErrObj(rtn, status)
										status = $DataManager.JobInterface.SetErrorOnJob(prgCtx, job, rtn)
										if !status.ok
											synCore.SetErrObj(rtn, status)
										end
										
									// *** KM 10-15-15 Handle lack of items here	
									elseif status.result.itemsInitialized==0
										synCore.SetErrObj(rtn, "No items found for this job.")
										status = $DataManager.JobInterface.SetErrorOnJob(prgCtx, job, rtn)
										if !status.ok
											synCore.SetErrObj(rtn, status)
										end				
									else
										status = $DataManager.JobInterface.CompleteJob(prgCtx, job, status.result)
										if !status.ok
											synCore.SetErrObj(rtn, status)
										end													
									end				
								end		
						
							end
							
							if IsDefined(controller)
								controller.Close()
							end	
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "action", StringType, "action", FALSE }, \
									{ "optionStr", StringType, "optionStr", FALSE }, \
									{ "sourceSettings", Assoc.AssocType, "sourceSettings", FALSE }, \
									{ "colNames", ListType, "colNames", TRUE, {} }	\					
								}
						end	

end

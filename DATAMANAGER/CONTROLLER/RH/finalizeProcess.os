package DATAMANAGER::CONTROLLER::RH

public object finalizeProcess inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'action', -1, 'action', FALSE }, { 'jobId', 2, 'JOBID', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
								
								Object		synCore = $DataManager.SynCoreInterface
								Assoc		rtn = synCore.InitErrObj( "FinalizeProcess.DoExec", this )
								Object		prgCtx = .fPrgCtx
								Assoc		status
								
								/**
								 * R may contain: 
								 *  action			=> String	=> AddVersion, Create, Sync, Update, Move
							   	 *  jobId			=> Integer 	=> JobId to retrieve data for this job
							     **/
							     
							    // *** get the process controller
							    status = $DataManager.ProcessController.Get(prgCtx, r.jobId, r.action, "sync", true, "finalize")
							    if !status.ok
									synCore.SetErrObj(rtn, status)
									.fError = rtn    	
							    else	     
							       	Object controller = status.result.controller	     
							    
							    	Object dataCache = status.result.dataCache
							    
			    					// *** how many errors did we have?
									Integer nonFatalErrors = dataCache.Get(prgCtx, {'numberOfErrors'}, IntegerType, IntegerType, 0)
									Integer validationErrors = dataCache.Get(prgCtx, {'validationErrors'}, IntegerType, IntegerType, 0)
							    
							    
									// *** hand off to ProcessController
									status = controller.FinalizeProcess(prgCtx, Str.Upper(r.action), r.jobId, status.result.dataCache, status.result.job, status.result.options, nonFatalErrors, validationErrors)
									if !status.ok
										synCore.SetErrObj(rtn, status)
										.fError = rtn
									else
										.fContent = synCore.SuccessResult(status.result)
									end	     
							
									// *** close out any logs, etc.
									controller.Close()
							
								end
								
								return undefined	
							End

	
	override function void SetPrototype()
								
								.fPrototype =\
									{\
										{ "action", StringType, "action", FALSE }, \
										{ "jobId", IntegerType, "JOBID", FALSE } \
									}
							end

end

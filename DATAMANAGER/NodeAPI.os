package DATAMANAGER

/**
 * 
 * Contains some useful DAPINODE methods.
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 */
public object NodeAPI inherits DATAMANAGER::Utils

	
	public function AddUpdateIdToACL(	Object prgCtx, \
													DAPINODE node, \
													Integer rightId, \
													Assoc permAssoc)
						
							Assoc	rtn = .InitErrObj("AddUpdateIdToACL")
							Dynamic	result
							Assoc	status
							Object	logger = .GetLogger(._fLogClassName)
							
							// *** does this node already have this user/group in the ACL
							String	stmt = 'select * from DTreeACL where DataID=:A1 and RightID=:A2 and ACLType=:A3'
							
							result = .ExecSQL(prgCtx, stmt, {node.pId, rightId, DAPI.PERMTYPE_ACL})
							
							if IsNotError(result)
							
								if Length(result)
									logger.Debug(Str.Format("RightID %1 already exists on node %2.  Will now attempt to update.", rightId, node.pId))
									
									status = .NodeRightUpdate(prgCtx, node, rightId, permAssoc)
									
									if status.ok
										logger.Debug(Str.Format("Update SUCCEEDED for RightID %1 on node %2.", rightId, node.pId))					
									else
										logger.Error(Str.Format("Update FAILED for RightID %1 on node %2.", rightId, node.pId))	
										.SetErrObj(rtn, status)
										return rtn
									end			
								else
									logger.Debug(Str.Format("RightID %1 does not exist on node %2.  Will now attempt to add.", rightId, node.pId))		
								
									status = .NodeRightAdd(prgCtx, node, rightId, permAssoc)
									
									if status.ok
										logger.Debug(Str.Format("Add SUCCEEDED for RightID %1 on node %2.", rightId, node.pId))								
									else
										logger.Error(Str.Format("Add FAILED for RightID %1 on node %2.", rightId, node.pId))				
										.SetErrObj(rtn, status)
										return rtn
									end
								end
							
							
							else
								.SetErrObj(rtn, result)
								return rtn
							end
								
							
							return .SuccessResult(node)						
						end

	
	public function Assoc ApplyNodeData(Object prgCtx, \
													DAPINODE node, \
													Assoc nodeData)
						
							Assoc	rtn = .InitErrObj("_ApplyNodeData")
							String	field
							Boolean	needDAPIUpdate = false
							
							/*  the fields
							'pCreateDate'
							'pModifyDate'
							'pCreatedBy'
							'pComment'
							'pCatalog'
							'pUserId'
							'pGroupId'	
							*/
						
							// *** loop through the fields and set them
							for field in Assoc.Keys(nodeData)
								Dynamic val = nodeData.(field)
							
								if val != node.(field)
									needDAPIUpdate = true
									
									// *** undefined comment needs to be set to a string
									if Str.Upper(field) == "PCOMMENT" && IsUndefined(val)
										val = ""
									end
									
									node.(field) = val
								end
						
						
							end
							
							
							if needDAPIUpdate
								// *** try DAPI.Update now
								Dynamic result = DAPI.UpdateNode(node)
								if IsError(result)
									.SetErrObj(rtn, result)
									return rtn
								end
							end	
							
							return rtn		
							
						end

	
	public function Assoc ApplySystemAtts(Object prgCtx, \
													DAPINODE node, \
													Assoc systemAtts, \
													Boolean doDapiUpdate)
						
							Assoc	rtn = .InitErrObj("ApplySystemAtts")
							String	field
							Boolean	needDAPIUpdate = false
							Dynamic result
								
							// *** loop through the fields and set them
							for field in Assoc.Keys(systemAtts)
								Dynamic val = systemAtts.(field)
							
								if IsDefined(val)
									needDAPIUpdate = true
									result = DAPI.SetNodeUserAttr(node, field, val)
									if IsError(result)
										.SetErrObj(rtn, Str.Format("Error updating system attribute %1. Result is '%2'", field, result))
										return rtn
									end
								end
						
							end
						
							if needDAPIUpdate && doDapiUpdate
								// *** try DAPI.Update now
							 	result = DAPI.UpdateNode(node)
								if IsError(result)
									.SetErrObj(rtn, result)
									return rtn
								end
							end	
						
							
							return rtn		
							
						end

	
	public function Assoc ApplyVersionData(Object prgCtx, \
														DAPIVERSION version, \
														Assoc versionData)
						
							Assoc	rtn = .InitErrObj("ApplyVersionData")
							String	field
							Boolean	needDAPIUpdate = false
							
							/*  the fields
							'pCreateDate'
							'pModifyDate'
							'pCreator'
							*/
						
							// *** loop through the fields and set them
							for field in Assoc.Keys(versionData)
								Dynamic val = versionData.(field)
							
								if IsDefined(val) && val != version.(field)
									needDAPIUpdate = true			
									version.(field) = val
								end
							end
							
							
							if needDAPIUpdate
								// *** try DAPI.Update now
								Dynamic result = DAPI.UpdateVersion(version)
								if IsError(result)
									.SetErrObj(rtn, result)
									return rtn
								end
							end	
							
							return rtn		
							
						end

	
	public function Assoc CopyNode(Object prgCtx, \
												DAPINODE src, \
												DAPINODE parent, \
												Integer options = DAPI.COPY_UNRESERVE | DAPI.COPY_UNLOCKV, \
												String nodeName = src.pName, \
												Boolean makeNameUnique = true)
													
							Assoc		rtn = .InitErrObj("_CopyNode")
							Assoc		status
							DAPINODE	newNode
							
							// *** Check access to this node
							status = ._CheckAccess(prgCtx, src)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end
							
							// *** Now check the name for bad characters and uniqueness
							status = ._CheckName(prgCtx, parent, nodeName, makeNameUnique)
								
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							else
								nodeName = status.result	
							end
						
							Assoc copyInfo = Assoc.CreateAssoc()
							copyInfo.AttrHandling = true
							copyInfo.AttrSourceType = 1
						
							Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(src.pSubtype)
							status = llNode.NodeCopy(src, parent, options, nodeName, copyInfo)
							if status.ok
								newNode = status.NewNode
								
								// *** Update Create Date
								newNode.pCreateDate = CAPI.Now(prgCtx.fDbConnect.fConnection)
								newNode.pModifyDate = CAPI.Now(prgCtx.fDbConnect.fConnection)		
								DAPI.UpdateNode(newNode)		
								
							else
								.SetErrObj(rtn, status)
								return rtn	
							end	
							
							return .SuccessResult(newNode)
						
						end

	
	public function Assoc CreateNode(	Object prgCtx, \
													DAPINODE parent, \
													Integer subtype, \
													String name, \
													Assoc 	createInfo = Assoc.CreateAssoc(), \
													Assoc	nodeData = Assoc.CreateAssoc(), \
													Boolean inheritParent = true, \
													Boolean makeNameUnique = false, \
													Boolean checkName = false, \
													Assoc systemAtts = Assoc.CreateAssoc())
						
							//._TimerStart("CreateNode")	
						
							Assoc		rtn = .InitErrObj("CreateNode")
							Assoc		status
							DAPINODE	node
							Object		llNode = $LLIAPI.LLNodeSubsystem.GetItem(subtype)
							
							// *** Allocate Node
							status = ._AllocateNode(prgCtx, parent, name, subtype, createInfo, inheritParent, makeNameUnique, checkName, llNode)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							else
								node = status.result
							end
							
							// *** Apply System Atts
							if IsDefined(systemAtts) && Length(Assoc.Keys(systemAtts))
								status = .ApplySystemAtts(prgCtx, node, systemAtts, false)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn		
								end		
							end		
						
							// *** Now Create
							createInfo.name = name
							createInfo.subtype = subtype	
							status = llNode.NodeCreate(node, parent, createInfo)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end
						
							// *** Apply any nodeData we have like pCreateDate, pModifyDate, etc.
							if IsDefined(nodeData) && Length(Assoc.Keys(nodeData))
								status = .ApplyNodeData(prgCtx, node, nodeData)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn		
								end		
							end
							
							// *** also apply any version data updates
							if IsFeature(createInfo, "versionInfo")
								DAPIVERSION version = DAPI.GetVersion(node, DAPI.VERSION_GET_CURRENT)
								if IsNotError(version)
									status = .ApplyVersionData(prgCtx, version, createInfo.versionInfo)
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn		
									end	
								end
							end	
							
							//._TimerStop("CreateNode")		
							
							return .SuccessResult(node)
							
						end


	
	public function Assoc	GetCopyNode(	Object prgCtx, \
														DAPINODE src, \
														DAPINODE parent, \
														Integer options = DAPI.COPY_UNRESERVE | DAPI.COPY_UNLOCKV, \
														String name = src.pName)
														
							Assoc		rtn = .InitErrObj("GetCopyNode")
							DAPINODE 	node = UNDEFINED
							Assoc		status
							Boolean 	makeNameUnique = false
							Assoc		result
							
							// *** initialize the boolean flag for creation to false
							rtn.created = false
							
							node = .DAPIGetNode(prgCtx, parent, name)
							
							if isError(node) || isUnDefined(node)
								
								status = .CopyNode(prgCtx, src, parent, options, name, makeNameUnique)	
								
								if status.ok
									result.created = true
									result.node = status.result
								else
									.SetErrObj(rtn, status)
									return rtn
								end
							end
						
							return .SuccessResult(result)
							
						end

	
	public function Assoc	GetCreateNode(	Object prgCtx, \
														DAPINODE parent, \
														Integer subtype, \
														String name, \
														Integer numRetries = 0,\
														Assoc createInfo = Assoc.CreateAssoc(), \
														Assoc	nodeData = Assoc.CreateAssoc(), \
														Boolean inheritParent = true, \
														Boolean makeNameUnique = false, \
														Boolean checkName = false, \
														Assoc systemAtts = Assoc.CreateAssoc())
						
							Assoc		rtn = .InitErrObj("GetCreateNode")
							DAPINODE 	node = UNDEFINED
							Assoc		status
							Assoc		result
							
							// *** initialize the boolean flag for creation to false
							rtn.created = false
							
							// *** call to .GetNode which can handle regex
							status = .GetNode(prgCtx, parent, name)	
							if status.ok
								node = status.result
							end	
							
							if isError(node) || isUnDefined(node)
								status = .CreateNode(prgCtx, parent, subtype, name, createInfo, nodeData, inheritParent, makeNameUnique, checkName, systemAtts)
								
								if status.ok
									
									result.created = true
									result.node = status.result
								
								elseif numRetries>0
									
									// *** maybe it got created in another thread and we have a unique constraint violation
									Integer i
									for i = 1 to numRetries
										status = .GetNode(prgCtx, parent, name)
										// *** KM 2-4-16 Patched this -- need to check Assoc return
										if status.ok
											node = status.result
											
											// *** Apply any nodeData we have like pCreateDate, pModifyDate, etc.
											if IsDefined(nodeData) && Length(Assoc.Keys(nodeData))
												status = .ApplyNodeData(prgCtx, node, nodeData)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn		
												end		
											end					
											
											// *** Apply System Atts
											if IsDefined(systemAtts) && Length(Assoc.Keys(systemAtts))
												status = .ApplySystemAtts(prgCtx, node, systemAtts, true)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn		
												end		
											end						
											
											result.node = node
											result.created = false
											return .SuccessResult(result)
										end
									end
									
									.SetErrObj(rtn, Str.Format("Could not get or create node named '%1' in folder named '%2.' Could be a permissions error.", name, parent.pName))
									return rtn					
								else
									.SetErrObj(rtn, status)
									return rtn
								end
							
							else
								result.node = node
								result.created = false	
							end
							
							return .SuccessResult(result)
							
						end

	
	public function String GetUniqueName(	Object prgCtx, \
														DAPINODE parent, \
														String proposedName)
														
						
							Integer 	i
							String		lookFor
							String		fExt
							String 		newName	= proposedName
							String		baseName = proposedName
						
							// **** Grab any file extension from the name, lop it off, and save it
							Integer		periodLoc = Str.RChr(proposedName, '.')
							
							// *** KM 4/24/17 ensure that periodLoc is defined to avoid stack trace
							if IsDefined(periodLoc) && periodLoc  <> 0
								fExt = proposedName[periodLoc:]
								baseName = proposedName[1:periodLoc-1]	
							end
							
							// ***** Lop off any existing "_1" or "-2"
							for i = 1 to 100
								lookFor = Str.Format('_%1', i)
								
								if Str.RChr(baseName, lookFor) == Length(baseName) - 1
									baseName = baseName[1:Length(baseName) - 2]
									break
								end	
							end
						
							// ***** Now get the unique Name with File Extension if any
							i = 1
							newName = Str.Format('%1%2', baseName, fExt)
						
							while !(.IsUniqueName(prgCtx, parent, newName)) 
								newName = Str.Format('%1_%2%3', baseName, i, fExt)
								i += 1
							end
						
							return newName
						
						end

	
	public function Boolean IsUniqueName(	Object prgCtx, \
														DAPINODE parent, \
														String name)
													
							String stmt = "select DataID from DTree where ParentID=:A1 and Name=:A2"
							Dynamic result = .ExecSQL(prgCtx, stmt, {parent.pId, name})
							
							if Length(result) > 0
								return false
							else
								return true
							end	
																
						end

	
	public function NodeRightAdd(	Object prgCtx, \
												DAPINODE node, \
												Integer rightId, \
												Assoc permAssoc)
							
							//logger.Entry(.OSName + ".NodeRightAdd")	
							Assoc	rtn = .InitErrObj("NodeRightAdd")
							Object		llNode
							Assoc		status
						
							// *** Add the right	
							llNode = $LLIAPI.LLNodeSubsystem.GetItem( node.pSubType )
							status = llNode.NodeRightAdd( node, rightId, permAssoc )
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end
								
							return .SuccessResult(node)						
						end

	
	public function NodeRightUpdate(	Object prgCtx, \
													DAPINODE node, \
													Integer rightId, \
													Assoc permAssoc, \
													Integer permType = DAPI.PERMTYPE_ACL)							
							
							Assoc	rtn = .InitErrObj("NodeRightUpdate")
							Assoc	status
							
							// *** Update the right	
							Object llNode = $LLIAPI.LLNodeSubsystem.GetItem( node.pSubType )
							status = llNode.NodeRightUpdate( node, permType, rightId, permAssoc )
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end	
							
							return .SuccessResult(node)	
						end

	
	private function Assoc _AllocateNode(	Object prgCtx, \
														DAPINODE parent, \
														String name, \
														Integer subtype, \ 
														Assoc createInfo = Assoc.CreateAssoc(), \
														Boolean inheritParent = true, \
														Boolean makeNameUnique = true, \
														Boolean checkName = true, \
														Object	llNode = $LLIAPI.LLNodeSubsystem.GetItem(subtype))
						
							Assoc 		rtn = .InitErrObj('Error in _AllocateNode.')
							Dynamic		newNode
							Assoc		status		
							
							if checkName || makeNameUnique
								// *** Check the Name for any bad characters and uniqueness
								status = ._CheckName(prgCtx, parent, name, makeNameUnique)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else
									name = status.result	
								end
							end
							
							// *** allocate Node
							Assoc options
							if inheritParent
								options.InheritNode = parent
								createInfo.InheritNode = parent
							end	
							status = llNode.NodeAlloc(parent, name, options)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							else
								newNode	= status.node
								if IsError(newNode)
									.SetErrObj(rtn, newNode)
									return rtn
								end			
							end
						
							return .SuccessResult(newNode)
						
						end

	
	private function Assoc _AuditEvent(	Object prgCtx, \
													DAPINODE node, \
													Integer eventId, \
													String value1 = undefined, \
													String value2 = undefined, \
													String valueKey = undefined, \							
													Assoc  extraAuditInfo = undefined)
													
						
							/* 	***** $LLIAPI.AuditSubsystem.Audit method specifies the following arguments:
								//		AuditID			The ID of the auditable event. Must be specified.
								//		prgCtx			Either the program context or a DAPI context. Must be specified.
								//		DataID			One of four things
								//							The ID of the node, if this is a node event.
								//							The DAPINode, if this is a node event.
								//							An Assoc with the following fields, if this is a node event
								//								DapiCtx		-	The DapiSession Object.
								//								SubType		-	The subtype of the node
								//								DataID		-	The node id.
								//								Name		-	The name of the node.
								//							Undefined, if this is not a node event
								//		SubType			The subType of the node, if this is a node event.
								//		userID			The ID of the user or group, if this is a user or group event.
								//		value1			A String, specific to this event.
								//		value2			A String, specific to this event.
								//		valueKey		A String, specific to this event.		
								//		when			The timestamp of when the auditable event happened.
								//		extraAuditInfo	An Assoc containing even more data specific to this event.	
							*/						
						
							Assoc	rtn = .InitErrObj("_AuditEvent")
							Assoc	status
							
							status = $LLIAPI.AuditSubsystem.Audit(	eventId, \
																	prgCtx, \
																	node, \
																	node.pSubtype, \
																	Undefined, \
																	value1, \
																	value2, \
																	valueKey, \
																	CAPI.Now(prgCtx.fDbConnect.fConnection), \
																	extraAuditInfo )							
													
						
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end
							
							return .SuccessResult(node)
						
						end

	
	private function Assoc _CheckAccess(Object prgCtx, \
													DAPINODE node)
														
							Assoc		rtn = .InitErrObj("_CheckAccess")
							
							// *** make sure that the node is not reserved or reserved by the user
							if ( DAPI.NODE_NOT_RESERVED == node.pReserved || $LLIAPI.NodeUtil.UnreservableByID( prgCtx.DSession(), node.pReservedBy ) )	
								// ok			
							else
								rtn.ok = false
								rtn.errMsg = node.pName + ' has been reserved.  You cannot perform this operation.'
								return rtn
							end
						
							return rtn
							
						end

	
	private function Assoc _CheckName(	Object prgCtx, \
													DAPINODE parent, \
													String name, \
													Boolean makeNameUnique)
													
							Assoc  rtn = .InitErrObj("_CheckName")
						
							// **** Strip out any ":" and any double quotes
							name = Str.ReplaceAll(name, ':', '')
							name = Str.ReplaceAll(name, '"', '')
							
							// *** Make the name unique Automatically?
							if makeNameUnique	
								name = .GetUniqueName(prgCtx, parent, name)
							
							// *** or report an error if the name conflicts?	
							elseif !(.IsUniqueName(prgCtx, parent, name))
								rtn.ok = false
								rtn.errMsg = Str.Format('There is already an object with the name %1 in this folder.  Please select a unique name.', name)
								return rtn
								
							end
							
							return .SuccessResult(name)
							
						end

	
	private function Assoc _CreateNode(	Object prgCtx, \
													DAPINODE parent, \
													DAPINODE newNode, \
													Assoc createInfo = undefined)
													
							Assoc 		rtn = .InitErrObj('Error in _CreateNode.')							
							Object		llNode = $LLIAPI.LLNodeSubsystem.GetItem(newNode.pSubtype)
							Assoc		status
						
							status = llNode.NodeCreate(newNode, parent, createInfo)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end
							
							return .SuccessResult(newNode)
													
						end

end

package DATAMANAGER

public object RDADDRESSEE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsAddressee'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDADDRESSEE'
	override	String	fUpdateApiKey3 = 'fAddressee'

end

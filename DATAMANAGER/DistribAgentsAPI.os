package DATAMANAGER

public object DistribAgentsAPI inherits DATAMANAGER::CrudAPI

	override	String	_fIdFieldName = 'ID'


	
	override function Assoc _DBToUI(Record rec, Integer tzOffset = 0)
											
												Assoc a = Assoc.FromRecord(rec)
												Assoc newAssoc
												String	key
												
												// *** convert blob field to assoc and get some pertinent info about the job to put into the UI
												Assoc data = Str.StringToValue(rec.blob)
												
												if IsFeature(data, 'taskData') && IsFeature(data.taskData, 'schedData')
													newAssoc.jobname = data.taskData.schedData.JOBNAME
												elseif 	IsFeature(data, 'taskData') && IsFeature(data.taskData, 'data') && IsFeature(data.taskData.data, 'schedData')
													newAssoc.jobname = data.taskData.data.schedData.JOBNAME
												else
													newAssoc.jobname = "?"	
												end	
												
												// *** convert everything to lowercase
												for key in Assoc.Keys(a)
													newAssoc.(Str.Lower(key)) = a.(key)		
												end
											
												return newAssoc
													
											end

	
	override function Assoc _Delete(	Object prgCtx, \
																		Dynamic id)
												
																				
													Assoc 	rtn = .InitErrObj( "_Delete" )
													String	stmt
													Dynamic result
												
													stmt = 'delete from WorkerPackages where NID=(select PackageID from WorkerQueueCurrent where ID=:A1)'
													
													result = .ExecSql(prgCtx, stmt, {id})
													if IsError(result)
														
														stmt = 'delete from WorkerPackages where TaskID=:A1'
														result = .ExecSql(prgCtx, stmt, {id})
												
														if IsError(result)
															.SetErrObj(rtn, result)
															return rtn
														end	
													end		
												
													stmt = 'delete from WorkerQueueChildren where TaskID=:A1 or ChildTaskID=:A2'
													
													result = .ExecSql(prgCtx, stmt, {id, id})
													if IsError(result)
														.SetErrObj(rtn, result)
														return rtn
													end		
													
													stmt = 'delete from WorkerQueueCurrent where ID=:A1'
													
													result = .ExecSql(prgCtx, stmt, {id})
													if IsError(result)
														.SetErrObj(rtn, result)
														return rtn
													end	
													
													stmt = 'delete from WorkerQueue where NID=:A1'
													
													result = .ExecSql(prgCtx, stmt, {id})
													if IsError(result)
														
														stmt = 'delete from WorkerQueue where ID=:A1'
														result = .ExecSql(prgCtx, stmt, {id})
												
														if IsError(result)
															.SetErrObj(rtn, result)
															return rtn
														end	
													end		
													
													
													return .SuccessResult(rtn)
												 
												end

	
	override function Assoc _Get(Object prgCtx, \
																Dynamic id, \
																Dynamic dftValue = undefined, \
																Integer tzOffset = 0)
											
												Assoc 	rtn = .InitErrObj( "_Get" )
											
												if Type(id) == StringType
													id = Str.StringToInteger(id)
												end
											
												// *** KM 6-24-16 -- fix for case sensitive SQL server - Upper(Blob)-->Blob
												String	stmt = 'select wqc.*, wp.Blob  from WorkerQueueCurrent wqc, WorkerPackages wp where wqc.PACKAGEID=wp.NID and wqc.ID=:A1'
												
												RecArray result = .ExecSql(prgCtx, stmt, {id})
												if IsError(result)
													.SetErrObj(rtn, result)
													return rtn
												elseif Length(result)==0
													.SetErrObj(rtn, Str.Format("Could not get job with id %1", id))
													return rtn			
												end	
												
												// *** now we need to massage the record into a data assoc
												Assoc data = ._DbToUi(result[1], tzOffset)
												
												return .SuccessResult(data)
											 
											end

	
	override function Assoc _List(	Object prgCtx, \
																	Assoc filter, \
																	String whereStr = "where wqc.PackageID=wp.NID", \
																	List subs = {})
																	
												/* ****
												
													filter is an assoc. it can be a simple key value pair where the key is a column name and value is the value that that column must have
													e.g. 	filter.ID = 32  (operator defaults to equals)
													
													or it can be a nested Assoc like this with an "operator" and "value" key
													e.g.	filter.NEXT_RUN_DATETIME = Assoc.CreateAssoc()
															filter.NEXT_RUN_DATETIME.operator = "<="
															filter.NEXT_RUN_DATETIME.value = Date.Now()
												*/					
																	
											
												Assoc 	rtn = .InitErrObj( "_List" )
												Integer	i = Length(subs)+1
												String	key
												
												// *** turn Upper(Blob)-->Blob for case sensitive SQL server
												String	stmt = "select wqc.*, wp.Blob from WorkerQueueCurrent wqc, WorkerPackages wp "
												
												if IsDefined(filter)
													whereStr += Length(whereStr) ? " and " : " where "
													// *** filter provides our where clause
													for key in Assoc.Keys(filter)
														Dynamic val = Type(filter.(key)) == Assoc.AssocType ? filter.(key).value : filter.(key)
														String 	operator  = Type(filter.(key)) == Assoc.AssocType ? filter.(key).operator : "="
													
														whereStr += Str.Format("%1 %2 %3 :A%4", i==1 ? '' : ' and ', key, operator, i)
														subs = {@subs, val}
														i+=1
													end
												end	
												
												// *** another Case sensitive SQL Server fix
												whereStr = Str.Replace(whereStr, 'handlerID', 'HandlerID')
												
												stmt += whereStr
												
												// *** run the query	
												RecArray result = .ExecSql(prgCtx, stmt, subs)
												if IsError(result)
												
													// *** OT changed the name of a column in WorkerPackages. Try again
													stmt = Str.Replace(stmt, "where wqc.PackageID=wp.NID", "where wqc.ID=wp.TaskID")
											
													result = .ExecSql(prgCtx, stmt, subs)
													if IsError(result)	
														.SetErrObj(rtn, result)
														return rtn
													end	
												end	
											
												return .SuccessResult(result)	
											
											 
											end

end

package DATAMANAGER

public object Tester inherits DATAMANAGER::SynCoreMain

	public		List	_fChangeableFields = { '$CATALOG', '$CLASSIFICATION', '$CLIENT', '$CLIENTID', '$CLIENTNAME', '$CREATEDATE', '$CREATOR', '$DESCRIPTION', '$FILENUMBER', '$FROMDATE', '$GROUP', '$KEYWORDS', '$HOMELOCATION', '$LOCATION', '$LOCATORTYPE', '$MEDIATYPEFIELDS', '$MODIFYDATE', '$NICKNAME', '$OFFSITESTORID', '$OWNER', '$PERMISSIONS', '$RDACCESSION', '$RDADDRESSEE', '$RDAUTHOR', '$RDCLASSIFICATION', '$RDESSENTIAL', '$RDLASTREVIEWDATE', '$RDNEXTREVIEWDATE', '$RDOFFICIAL', '$RDORGANIZATION', '$RDORIGINATOR', '$RDOTHERADDRESSEE', '$RDRECEIVEDDATE', '$RDRECORDDATE', '$RDRSI', '$RDSTATUS', '$RDSTATUSDATE', '$RDSTORAGEMEDIUM', '$RDSUBJECT', '$RDUPDATECYCLEPERIOD', '$REFERENCERATE', '$TEMPORARYID', '$TODATE' }


	
	public function void GenerateChangeClearCsvFiles(Object prgCtx = .GetAdminCtx().prgCtx)
											
												String 	key
												String	path = "C:\ImportExport\tests\csv\"
												String	csvPath, jsonStr	
												Dynamic result
												List	keys
												Assoc	newVals
												String	folderName
												
												// *** get current Vals
												Integer nodeId = 796334	
												Assoc 	currentVals = ._GetCurrentVals(prgCtx, nodeId)	
												keys = Assoc.Keys(currentVals)	
												
												// *** vals we gather for phys obj transaction testing
												Assoc circVals
												circVals.("$ObjectID") = currentVals.("$OBJECTID") 
												
												Assoc transferVals
												transferVals.("$ObjectID") = currentVals.("$OBJECTID") 	
												
												Assoc labelVals
												labelVals.("$ObjectID") = currentVals.("$OBJECTID")
												
												Assoc locatorVals	
												locatorVals.("$ObjectID") = currentVals.("$ObjectID") 	
												
												for key in keys
													
													folderName =  key[1] == "$" ? key[2:] : Str.ReplaceAll(key, ":", "-")	
											
													// *** for testing clearing the value	
													if (key[1] != "$" || key in ._fClearableFields > 0) && IsUndefined(Str.LocateI(key, "IsAttachedCat"))
												 
														newVals = Assoc.CreateAssoc()
														newVals.("$OBJECTID") = currentVals.("$OBJECTID") 
														
														// *** test <clear>
														newVals.(key) = "[clear]"
											
														csvPath = path + "test_clear\" + folderName + "\metadata.csv"
															
														jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(newVals) + "]"
														
														result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
														
														if IsError(result)
															echo("Error: " + Str.String(result))
														end	
													end
												
													// *** for testing changing the value
													if (key[1] != "$" || key in ._fChangeableFields > 0) && IsUndefined(Str.LocateI(key, "IsAttachedCat"))
											
														// *** test change
														newVals = Assoc.CreateAssoc()
														newVals.("$OBJECTID") = currentVals.("$OBJECTID")
														
														if key == "$MEDIATYPEFIELDS"
															newVals.("$PHYSITEMTYPE") = currentVals.("$PHYSITEMTYPE")
														end 
														
														newVals.(key) = ._ChangeVal(key, currentVals.(key))
														csvPath = path + "test_change\" + folderName + "\metadata.csv"
															
														jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(newVals) + "]"
														
														result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
														
														if IsError(result)
															echo("Error: " + Str.String(result))
														end			
													
													end
													
													// *** gather groups of columns for check in / check out
													if (key in ._fPOCircColNames > 0)
														
														circVals.(key) = ._ChangeVal(key, currentVals.(key))
														
													end
													
													// *** gather groups of columns for transfer
													if (key in ._fPOXferColNames > 0)
														
														transferVals.(key) = ._ChangeVal(key, currentVals.(key))
														
													end		
													
													// *** gather groups of columns for label
													if (key in ._fPOLabelColNames > 0)
														
														labelVals.(key) = ._ChangeVal(key, currentVals.(key))
														
													end			
													
													// *** gather groups of columns for locator change
													if (key in ._fPOLocatorColNames > 0)
														
														locatorVals.(key) = ._ChangeVal(key, currentVals.(key))
														
													end			
												
												end
											
												// *** generate circulation csv
												csvPath = path + "test_po_actions\circulation\metadata.csv"
												jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(circVals) + "]"
												result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
												if IsError(result)
													echo("Error: " + Str.String(result))
												end		
												
												// *** generate Transfer csv
												csvPath = path + "test_po_actions\transfer\metadata.csv"
												jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(transferVals) + "]"
												result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
												if IsError(result)
													echo("Error: " + Str.String(result))
												end	
												
												// *** generate label csv
												csvPath = path + "test_po_actions\label\metadata.csv"
												jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(labelVals) + "]"
												result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
												if IsError(result)
													echo("Error: " + Str.String(result))
												end		
												
												// ** generate locator csv
												csvPath = path + "test_po_actions\locator\metadata.csv"
												jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(locatorVals) + "]"
												result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
												if IsError(result)
													echo("Error: " + Str.String(result))
												end				
											
											
												// **** assign box
												nodeId = 930315		// War & Peace book	
											
												// *** get current vals (only need a few)
												Assoc options	
												options.wantNodeId = true
												options.metadataOnly = true
												options.wantPhysObjOther = true	
												currentVals = ._GetCurrentVals(prgCtx, nodeId, options)	
											
												// *** box vals
												Assoc boxVals
												boxVals.("$ObjectID") = currentVals.("$OBJECTID")	
											
												
												for key in Assoc.Keys(currentVals)	
												
													// *** gather groups of columns for box change
													if (key in ._fPOBoxColNames > 0)
														
														boxVals.(key) = ._ChangeVal(key, currentVals.(key))
														
													end		
											
												end	
												
												
												
												// *** generate boxing csv
												csvPath = path + "test_po_actions\box\metadata.csv"
												jsonStr = "[" + $DataManager.SynCoreInterface.toJSON(boxVals) + "]"
												result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonStrToCsv", {jsonStr, csvPath} )
												if IsError(result)
													echo("Error: " + Str.String(result))
												end		
												
											
											end

	script GetTestCsvExports
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function List GetTestCsvExports(Object prgCtx = .GetAdminCtx().prgCtx)
																									
																										List configs
																									
																										List configs1 = _GetMetadataExports(prgCtx)
																										configs = {@configs, @configs1}
																									
																										List configs2 = _GetFileCsvExports(prgCtx)
																										configs = {@configs, @configs2}
																									
																										return configs
																										
																									
																									
																									end
																									
																									function _GetFileCsvExports(Object prgCtx)
																									
																										Integer nodeId = 452998	// East Coast Folder
																										
																										
																										String	rootSourcePath = $DataManager.NodeUtils.GetNodePath(nodeId, prgCtx)
																										
																										String	rootExportPath = "C:\ImportExport\tests\csv\test_load_files\"
																									
																										Assoc	a
																										List 	groups, configs
																									
																										// *** construct the base optionStr
																										String 	optionStr = Str.Format("jobType=export&jobSubtype=exportobjects&deleteSub=true&numSubLevels=All&exportType=hierarchy&numMetadataFiles=single&metadataOnly=false&action=exportnodes&sourceType=browse&dataIdFilePath=&rootSourceId=%1&rootSourcePath=%2&rootExportPath=%3", \
																																		nodeID, \
																																		rootSourcePath, \
																																		Web.Escape(rootExportPath))
																										
																									
																										
																										// *** build an assoc template
																										Assoc config
																										config.action = "exportnodes"
																										config.optionStr = optionStr
																										config.colNames = {}
																										config.rootSourcePath = rootSourcePath
																										config.sourceSettings = ._GetSourceSettingsAssoc()
																										
																										// *** List to iterate through to build the exports
																										List	configCols = {\
																												{{"wantNodeId", "wantBasicData"}, "wantBasicData"}, \
																												{{"wantNodeId", "wantCategoryData"}, "wantCategoryData"}, \
																												{{"wantNodeId", "wantSystemAtts"}, "wantSystemAtts"}, \
																												{{"wantNodeId", "wantRMData"}, "wantRMData"}, \
																												{{"wantNodeId", "wantClassifications"}, "wantClassifications"}, \
																												{{"wantNodeId", "wantOwner"}, "wantOwner"}, \
																												{{"wantNodeId", "wantPerms"}, "wantPerms"} \
																												}																									
																									
																										List c
																										
																										for c in configCols
																										
																											groups = c[1]
																											a = Assoc.Copy(config)
																											a.optionStr += Str.Format("&subTargetPath=%1&" + Str.Join(groups, "=true&"), c[2]) + "=true"
																											a.colNames = ._GetColumns(groups)
																											a.testName = "test_load_files\" + c[2] 
																											configs = {@configs, a}
																										
																										end
																										
																										return configs 
																									
																									
																									
																									end
																									
																									
																									function List _GetMetadataExports(Object prgCtx)
																									
																										Integer nodeId = 327414	// Physical Object Folder
																										
																										
																										String	rootSourcePath = $DataManager.NodeUtils.GetNodePath(nodeId, prgCtx)
																										
																										String	rootExportPath = "C:\ImportExport\tests\csv\test_update_same_vals\"
																									
																										Assoc	a
																										List 	groups, configs
																									
																										// *** construct the base optionStr
																										String 	optionStr = Str.Format("jobType=export&jobSubtype=exportmetadata&deleteSub=true&exportType=flat&numMetadataFiles=single&metadataOnly=true&action=exportnodes&sourceType=browse&dataIdFilePath=&rootSourceId=%1&rootSourcePath=%2&rootExportPath=%3", \
																																		nodeID, \
																																		rootSourcePath, \
																																		Web.Escape(rootExportPath))
																										
																									
																										
																										// *** build an assoc template
																										Assoc config
																										config.action = "exportnodes"
																										config.optionStr = optionStr
																										config.colNames = {"$OBJECTID"}
																										config.rootSourcePath = rootSourcePath
																										config.sourceSettings = ._GetSourceSettingsAssoc()
																										
																										// *** List to iterate through to build the exports
																										List	configCols = {\
																												{{"wantNodeId", "wantBasicData"}, "wantBasicData"}, \
																												{{"wantNodeId", "wantCategoryData"}, "wantCategoryData"}, \
																												{{"wantNodeId", "wantSystemAtts"}, "wantSystemAtts"}, \
																												{{"wantNodeId", "wantRMData"}, "wantRMData"}, \
																												{{"wantNodeId", "wantClassifications"}, "wantClassifications"}, \
																												{{"wantNodeId", "wantOwner"}, "wantOwner"}, \
																												{{"wantNodeId", "wantPerms"}, "wantPerms"}, \
																												{{"wantNodeId", "wantPhysObj"}, "wantPhysObj"}, \
																												{{"wantNodeId", "wantPhysObjCirc"}, "wantPhysObjCirc"}, \		
																												{{"wantNodeId", "wantPhysObjOther"}, "wantPhysObjOther"}, \
																												{{"wantNodeId", "wantOther"}, "wantOther"} \
																												}																									
																									
																										List c
																										
																										for c in configCols
																										
																											groups = c[1]
																											a = Assoc.Copy(config)
																											a.optionStr += Str.Format("&subTargetPath=%1&" + Str.Join(groups, "=true&"), c[2]) + "=true"
																											a.colNames = ._GetColumns(groups)
																											a.testName = "test_update_same_vals\"+ c[2]
																									
																											configs = {@configs, a}
																										
																										end
																										
																										return configs 
																										
																									end	
																									
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	script GetTestCsvLoads
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function List GetTestCsvLoads()
																									
																									
																										String optionStr = "jobType=load&jobSubtype=loadcsv&sourceOption=server&rootSourcePath=%1&rootFileLoc=csvpath&rootTargetPath=Enterprise&action=%2" +\
																															"&actionRename=true" +\
																															"&actionMetadata=true" +\
																															"&actionPerms=true" +\
																															"&actionPoCirc=true" +\
																															"&actionPoBox=true" +\
																															"&actionPoXFer=true" +\
																															"&actionPoLabel=true" +\	
																															"&mdDapi=true" +\
																															"&mdOwner=true" +\
																															"&mdCats=true" +\
																															"&mdClass=true" +\
																															"&mdRm=true" +\
																															"&mdPo=true" +\
																															"&mdName=true" +\
																															"&mdSysAttrs=true" +\						
																															"&mdDapi=true" +\
																															"&docOption=update&docOptionAddVersion=true&docOptionUpdateMd=true&containerOption=update&containerOptionUpdateMd=true&whenOption=now&defaultFileExtension=pdf" +\
																															"&auditFolderPath=&inheritParent=true&autoCreateFolders=true&targetPathType=object&attrSourceType=1&jobId=0&estimatedTotal=1"
																									
																									
																										// *** build an assoc template
																										Assoc config
																										config.optionStr = optionStr
																										config.action = "update"
																										config.colNames = {"$OBJECTID"}
																										config.sourceSettings = ._GetSourceSettingsAssoc()
																										config.waitTime = 500
																										
																										return _GetLoadConfigs(config)
																									
																									
																									end
																									
																									function _GetLoadConfigs(Assoc config)
																									
																										List	configs
																										List	updatePaths = {"C:\ImportExport\tests\csv\test_update_same_vals\", "C:\ImportExport\tests\csv\test_change\", "C:\ImportExport\tests\csv\test_clear\", "C:\ImportExport\tests\csv\test_po_actions\"}
																										List	createPaths = {"C:\ImportExport\tests\csv\test_load_files\"}
																										String 	csvPath 
																										Assoc 	findReplace
																									
																										String path
																										
																										for path in updatePaths
																											// *** get list of csv files in the tests folder
																											Assoc status = $DataManager.FileUtils.FileListRecursive( path, \
																																									  1000, \
																																									  true, \
																																									  true)
																																									  
																											if status.ok
																												for csvPath in status.result.files
																												
																													Assoc a = Assoc.Copy(config)
																													a.loadPath = csvPath		
																									
																													a.optionStr = Str.Format(a.optionStr, csvPath, "update")
																													a.optionStr += ._GetAuditPathStr(csvPath)
																													a.action = "update"
																													a.colNames = $DataManager.UISupportAPI.GetCsvColumns(csvPath).result
																												
																													configs = {@configs, a}
																													
																												end	
																											end	
																										end
																										
																										for path in createPaths
																											// *** get list of csv files in the tests folder
																											
																											Assoc status = $DataManager.FileUtils.FileListRecursive( path, \
																																									  1000, \
																									 																  true, \
																																									  true)
																																									  
																											if status.ok
																												for csvPath in status.result.files
																												
																													Assoc a = Assoc.Copy(config)
																													a.loadPath = csvPath		
																													a.optionStr = Str.Format(a.optionStr, csvPath, "create", ._GetAuditPathStr(csvPath))
																													a.action = "create"
																													a.colNames = $DataManager.UISupportAPI.GetCsvColumns(csvPath).result
																													a.waitTime = 3000				
																													
																													// *** put find and replace vals in there
																													findReplace = Assoc.CreateAssoc()
																													findReplace.("$TARGETPATH") = Assoc.CreateAssoc()
																													findReplace.("$TARGETPATH").find = "East Coast"
																													findReplace.("$TARGETPATH").replace = "Tests:" + Str.Elements(csvPath, "\")[5]
																													a.sourceSettings.findReplaceVals = findReplace
																													
																													// *** remove the objectid col
																													a.colNames = List.SetRemove(a.colNames, "$OBJECTID")				
																													
																													
																													configs = {@configs, a}
																													
																												end	
																											end	
																										end	
																									
																										return configs 
																									end
																									
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function List GetTestFileExports(Object prgCtx)
											
												return {}
											
											end

	
	public function List GetTestFileLoads()
											
												return {}
											
											end

	script TestUTF8FileNames
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function String TestUTF8FileNames()
																									
																										;String s
																									
																										;String f
																										;Integer i=0
																										
																										;for f in File.FileList("C:\ImportExport\French Files\")
																											i+=1
																											s+= Str.Format("File %1: ", i) + File.GetName(f) + "<BR>"
																										;end
																										
																										;return s
																									end
																									
																									Function List parse_unicode( String s )
																										Integer i = 1, uc = 0, uc2 = 0, len = 0
																										List firstByteMark = { 0, 0, 192, 224, 240, 248, 252 }
																										
																										uc = parse_hex4( s[i+1:i+5] )
																										i += 4
																										
																										if ( uc >= 56320 && uc <= 57343 ) || uc == 0
																											// invalid unicode character.
																											return { "", i }
																										end	
																										
																										if ( uc >= 55296 && uc <= 56319 )	/* UTF16 surrogate pairs.	*/
																											
																											if ( s[i+1] != '\' || s[i+2] != 'u' )	
																												
																												/* missing second-half of surrogate.	*/
																												return { "", i }
																											
																											end
																											
																											uc2 = parse_hex4( s[i+3:i+7] )
																											i += 6
																											
																											if ( uc2 < 56320 || uc2 > 57343)
																												/* invalid second-half of surrogate.	*/
																												return { "", i }
																											end
																											
																											uc=65536 + ((( uc & 1023 )<<10) | (uc2 & 1023))
																										end
																										
																										len=4;
																									
																										if uc < 128
																											len=1
																										elseif uc < 2048
																											len=2
																										elseif uc < 65536
																											len=3
																										end
																										
																										String buffer = ""
																										
																										if len == 4
																											buffer = Str.Ascii((uc | 128) & 191) 
																											uc = uc >> 6
																										end
																										
																										if len >= 3
																											buffer = Str.Ascii((uc | 128) & 191) + buffer
																											uc = uc >> 6
																										end
																										
																										if len >= 2
																											buffer = Str.ascii((uc | 128) & 191) + buffer
																											uc = uc >> 6
																										end
																										
																										if len >= 1
																											buffer = Str.Ascii( uc | firstByteMark[len + 1] ) + buffer
																										end
																										
																										return { buffer, i }
																									end
																									
																									Function Integer parse_hex4( String s ) 
																									
																										Integer h = 0
																										Integer i = 1, j = 1
																										
																										
																										for j = 1 to 4
																										
																											Integer ascCh = Str.Ascii( s[i] )
																										
																											if ascCh >= 48 && ascCh <= 57 							/* '0' <= x <= '9' */
																												h += ascCh - 48
																											elseif s[i] >= 65 && s[i] <= 70							/* 'A' <= x <= 'F' */
																												h += 10 + ascCh - 65
																											elseif ascCh >= 97 && ascCh <= 102						/* 'a' <= x <= 'f' */
																												h += 10 + ascCh - 97
																											else 
																												return 0
																									
																											end
																												
																											if i < 4 
																												h = h << 4
																											end
																											i += 1
																										end
																										
																										return h
																									End
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	script _0Setup
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Setup()
																									
																										._fChangeableFields = {'$CATALOG',\
																															'$CLASSIFICATION',\
																															'$CLIENT',\
																															'$CLIENTID',\
																															'$CLIENTNAME',\
																															'$CREATEDATE',\
																															'$CREATOR',\
																															'$DESCRIPTION',\
																															'$FILENUMBER',\
																															'$FROMDATE',\
																															'$GROUP',\
																															'$KEYWORDS',\
																															'$HOMELOCATION',\
																															'$LOCATION',\
																															'$LOCATORTYPE',\
																															'$MEDIATYPEFIELDS',\
																															'$MODIFYDATE',\
																															'$NICKNAME',\
																															'$OFFSITESTORID',\
																															'$OWNER',\
																															'$PERMISSIONS',\
																															'$RDACCESSION',\
																															'$RDADDRESSEE',\
																															'$RDAUTHOR',\
																															'$RDCLASSIFICATION',\
																															'$RDESSENTIAL',\
																															'$RDLASTREVIEWDATE',\
																															'$RDNEXTREVIEWDATE',\
																															'$RDOFFICIAL',\
																															'$RDORGANIZATION',\
																															'$RDORIGINATOR',\
																															'$RDOTHERADDRESSEE',\
																															'$RDRECEIVEDDATE',\
																															'$RDRECORDDATE',\
																															'$RDRSI',\
																															'$RDSTATUS',\
																															'$RDSTATUSDATE',\
																															'$RDSTORAGEMEDIUM',\
																															'$RDSUBJECT',\
																															'$RDUPDATECYCLEPERIOD',\
																															'$REFERENCERATE',\
																															'$TEMPORARYID',\
																															'$TODATE',\
																															'$TRANSFERID'}
																															
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function String _ChangeVal(String col, String valStr)
											
												if valStr == ""
													valStr = undefined
												end	
											
												switch col
												
													case "$CATALOG"
														if valStr=="0"
															valStr = "1"
														else
															valStr="0"
														end		
													end
													
													case "$CLASSIFICATION"
														if IsUndefined(Str.LocateI(valStr, "Sports"))
															valStr = "Classifications:Regular Classifications:Sports"
														else
															valStr = "Classifications:Regular Classifications:Science"
														end
													end		
													
													case "$CLIENTID", "$CLIENT"
														if valStr != "msmith"
															valStr = "msmith"
														else
															valStr = "gworden"
														end	
													end
													
													case "$CLIENTNAME"
														if valStr != "George Brett"
															valStr = "George Brett"
														else
															valStr = "Bill Buckner"
														end	
													end
													
													case "$CREATEDATE"
														valStr = ._DateToString(Date.Now()-10000000)
													end
													
													case "$CREATOR"
														if valStr != "msmith"
															valStr = "msmith"
														else
															valStr = "gworden"
														end	
													end
													
													case "$DESCRIPTION"
														if valStr != "windy day"
															valStr = "windy day"
														else
															valStr = "sunny day"
														end	
													end
													
													case "$FILENUMBER"
														if valStr != "0003"
															valStr = "0003"
														else
															valStr = "0004"
														end	
													end
													
													case "$FROMDATE"
														valStr = ._DateToString(Date.Now() - Math.Random(10000000))
													end
													
													case "$GROUP"
														if valStr != "Development"
															valStr = "Development"
														else
															valStr = "HR"
														end			
													end
													
													case "$KEYWORDS"
														if valStr != "Boston Book Festival"
															valStr = "Boston Book Festival"
														else
															valStr = "Cambridge Science Fair"
														end
													end
													
													case "$HOMELOCATION"
														if valStr != "BOS"
															valStr = "BOS"
														else
															valStr = "CAM"
														end
													end
													
													case "$LOCATORTYPE"
														if valStr != "BOS"
															valStr = "BOS"
														else
															valStr = "CAM"
														end
													end
													
													case "$MEDIATYPEFIELDS"
														List 	pairs = Str.Elements(valStr, "|")
														String	pair
														String	newValStr
														
														for pair in pairs
															Integer equalsLoc = Str.Locate(pair, "=")
															if IsDefined(equalsLoc)
																String name = pair[1:equalsLoc-1]
																String val = pair[equalsLoc+1:]		
																
																switch Str.Upper(name)
																
																	case "FORMAT"
																		if val != "Hardback"
																			newValStr += name + "=Hardback|"
																		else
																			newValStr += name + "=Paperback|"
																		end						
																	end
																	
																	case "PUBDATE"
																		if val != "D/2015/4/14:0:0:0"
																			newValStr += name + "=D/2015/4/14:0:0:0|"
																		else
																			newValStr += name + "=D/2015/5/14:0:0:0|"
																		end						
																	end
																	
																	case "LOCID"
																		if val != "1234"
																			newValStr += name + "=1234|"
																		else
																			newValStr += name + "=4321|"
																		end							
																	end
																	
																end
															end
														end	
														
														valStr = Length(newValStr) ? newValStr[1:-2] : newValStr
														
													end
													
													case "$MODIFYDATE"
														valStr = ._DateToString(Date.Now()-Math.Random(10000000))
													end
													
													case "$NICKNAME"
														if valStr != "930315"
															valStr = "930315"
														else
															valStr = "513039"
														end
													end
													
													case "$NUMCOPIES"
														if valStr != "1"
															valStr = "1"
														else
															valStr = "2"
														end
													end
											
													case "$OBJECTNAME"
														if valStr != "War & Peace"
															valStr = "War & Peace"
														else
															valStr = "Peace & War"
														end
													end
													
													case "$OFFSITESTORID"
														if valStr != "9003"
															valStr = "9003"
														else
															valStr = "10004"
														end
													end
													
													case "$OWNER"
														if valStr != "msmith"
															valStr = "msmith"
														else
															valStr = "gworden"
														end
													end
													
													case "$PERMISSIONS"
														if valStr != "SCMAEVDRP:$Owner|SC:$Group|SC:$Public|SC:Umsmith"
															valStr = "[clear]:SCMAEVDRP:$Owner|SC:$Group|SC:$Public|SC:Umsmith"
														else
															valStr = "[clear]:SC:$Owner|SC:$Group|SC:$Public|SC:Ugworden"		
														end	
													end
													
													case "$RDACCESSION"
														if valStr != "ACC"
															valStr = "ACC"
														else
															valStr = "ACC2"
														end
													end
													
													case "$RDADDRESSEE"
														if valStr != "Sam Smith"
															valStr = "Sam Smith"
														else
															valStr = "John Jones"
														end
													end
													
													case "$RDAUTHOR"
														if valStr != "Kit Miller"
															valStr = "Kit Miller"
														else
															valStr = "Matt Smith"
														end
													end
													
													case "$RDCLASSIFICATION"
														
														if IsUndefined(Str.LocateI(valStr, "Audit"))
															valStr = "Classifications:Audit"
														else
															valStr = "Classifications:Account Payment"
														end
													end
													
													case "$RDESSENTIAL"
														if valStr != "ESS"
															valStr = "ESS"
														else
															valStr = "NON-ESS"
														end
													end
													
													case "$RDLASTREVIEWDATE"
														valStr = ._DateToString(Date.Now()-Math.Random(10000000))
													end
													
													case "$RDNEXTREVIEWDATE"
														valStr = ._DateToString(Date.Now()+Math.Random(10000000))
													end
													
													case "$RDOFFICIAL"
														if valStr == "TRUE" || valStr=="1"
															valStr = "FALSE"
														else
															valStr = "TRUE"
														end
													end
													
													case "$RDORGANIZATION"
														if valStr != "Syntergy"
															valStr = "Syntergy"
														else
															valStr = "Apple Computers"
														end
													end
													
													case "$RDORIGINATOR"
														if valStr != "Kit Miller"
															valStr = "Kit Miller"
														else
															valStr = "Matt Smith"
														end
													end
													
													case "$RDOTHERADDRESSEE"
														if valStr != "Greg Worden"
															valStr = "Greg Worden"
														else
															valStr = "Martin Black"
														end
													end
													
													case "$RDRECEIVEDDATE"
														valStr = ._DateToString(Date.Now()-Math.Random(10000000))
													end
													
													case "$RDRECORDDATE"
														valStr = ._DateToString(Date.Now()-Math.Random(10000000))
													end
													
													case "$RDRSI"
														if valStr != "1YEAR"
															valStr = "1YEAR"
														else
															valStr = "5 YEAR"
														end
													end
													
													case "$RDSTATUS"
														if valStr != "ACT"
															valStr = "ACT"
														else
															valStr = "INACTIVE"
														end
													end
													
													case "$RDSTATUSDATE"
														valStr = ._DateToString(Date.Now()-Math.Random(10000000))
													end
													
													case "$RDSTORAGEMEDIUM"
														if valStr != "PAPER"
															valStr = "PAPER"
														else
															valStr = "CS"
														end
													end
													
													case "$RDSUBJECT"
														if valStr != "What time is it?"
															valStr = "What time is it?"
														else
															valStr = "Why is the sky blue?"
														end
													end
													
													case "$RDUPDATECYCLEPERIOD"
														if valStr != "Monthly"
															valStr = "Monthly"
														else
															valStr = "Semi-Annual"
														end
													end
													
													case "$REFERENCERATE"
														if valStr != "REFRATE1"
															valStr = "REFRATE1"
														else
															valStr = "REFRATE2"
														end		
													end
													
													case "$TEMPORARYID"
														if valStr != "9003"
															valStr = "9003"
														else
															valStr = "10004"
														end
													end
													
													case "$TODATE"
														valStr = ._DateToString(Date.Now() + Math.Random(10000000))
													end
													
													case "$UNIQUEID"
														valStr = Str.String(Math.Random(100000))
													end
											
													/* categories */
													case "Content Server Categories:Demo Category:Author"
														if valStr != "Kit Miller"
															valStr = "Kit Miller"
														else
															valStr = "Erin Miller"
														end		
													end
											
													case "Content Server Categories:Demo Category:Date Submitted"
														valStr = ._DateToString(Date.Now()-Math.Random(10000000))
													end
													
													case "Content Server Categories:Demo Category:High Priority"
														if valStr != "true"
															valStr = "true"
														else
															valStr = "false"
														end
													end
													
													case "Content Server Categories:Demo Category:Reference Number"
														if valStr != "9003"
															valStr = "9003"
														else
															valStr = "4500"
														end		
													end
													
													case "Content Server Categories:Demo Category:Summary"
														if valStr != "Now is the time for all good men to come to the aid..."
															valStr = "Now is the time for all good men to come to the aid..."
														else
															valStr = "Four score and seven years ago..."
														end		
													end
													
													case "Content Server Categories:Demo Category:Publication Info[1]:Publication Type"
														if valStr != "Book"
															valStr = "Book"
														else
															valStr = "Pamphlet"
														end	
													end
													
													case "Content Server Categories:Demo Category:Year"
														if valStr != "2015"
															valStr = "2015"
														else
															valStr = "2014"
														end			
													end
													
													// *** phys obj transaction fields
													case "$AREA"
														
														if valStr!="ARCH"
															valStr = "ARCH"
														else
															valStr = "CIRC"
														end
													end
													
													case "$BOX"
														
														if IsUndefined(Str.LocateI(valStr, "Boston"))
															valStr = ":Enterprise:Physical Objects:Boston Box"
														else
															valStr = ":Enterprise:Physical Objects:Miller Box"
														end
													end		
													
													case "$FACILITY"
														if valStr != "BOS"
															valStr = "BOS"
														else
															valStr = "CAM"
														end	
													end		
													
													case "$LOCATOR"
														if valStr != "SHELVES"
															valStr = "SHELVES"
														else
															valStr = "CAB"
														end
													end		
													
													case "$BORROWCOMMENT"
														if valStr != "Borrowed by msmith"
															valStr = "Borrowed by msmith"
														else
															valStr = "Borrowed by gworden"
														end		
													end
													
													case "$BORROWEDBY"
														if valStr != "msmith"
															valStr = "msmith"
														else
															valStr = "gworden"
														end		
													end
													
													case "$BORROWEDDATE"
														valStr = ._DateToString(Date.Now() - Math.Random(10000000))
													end
													
													case "$CURRENTLOCATION"
														if valStr != "BOS"
															valStr = "BOS"
														else
															valStr = "CAM"
														end
													end
													
													case "$LABELCOPIES"
														if valStr != "5"
															valStr = "5"
														else
															valStr = "10"
														end		
													end
													
													case "$LABELTYPE"
														if valStr != "Label A"
															valStr = "Label A"
														else
															valStr = "Label B"
														end		
													end
													
													case "$OBTAINEDBY"
														if valStr != "msmith"
															valStr = "msmith"
														else
															valStr = "gworden"
														end		
													end
													
													case "$RETURNDATE"
														valStr = ._DateToString(Date.Now() + Math.Random(10000000))		
													end
													
													case "$TRANSFERID"
														if valStr != "Transfer 101"
															valStr = "Transfer 101"
														else
															valStr = "Transfer 102"
														end			
													end
													
													default
														echo("ERROR: No code in _Change Val for " + col)
													end
													
												end
											
											
												return valStr
											
											end

	
	private function String _GetAuditPathStr(String rootPath)
											
												String s
												
												List fullPathElements = Str.Elements(rootPath, "\");
												String keyName = fullPathElements[6];
												List testPathElements = {fullPathElements[1], fullPathElements[2], fullPathElements[3], fullPathElements[4], fullPathElements[5]}
												String testPath = Str.Join(testPathElements, "\");
												
												// *** old vals
												$Kernel.FileUtils.DeleteRecursive(testPath + "\_audit\oldVals\")
												s += "&auditDeltaOldPath=" + testPath + "\_audit\oldVals\" + keyName + ".txt"
												
												// *** input vals
												$Kernel.FileUtils.DeleteRecursive(testPath + "\_audit\inputVals\") 
												s += "&auditDeltaInputPath=" + testPath + "\_audit\inputVals\" + keyName + ".txt";
												
												// *** new vals
												$Kernel.FileUtils.DeleteRecursive(testPath + "\_audit\newVals\") 
												s += "&auditDeltaNewPath=" + testPath + "\_audit\newVals\" + keyName + ".txt";
												
												return s
												
											end

	
	private function List _GetColumns(List groups)
											
												List	cols
												String 	group
												
												for group in groups
											
													switch group
													
														case "wantNodeId"
															cols = {@cols, @{'$OBJECTNAME','$TARGETPATH','$OBJECTID','$OBJECTTYPE'}}
														end
													
														case "wantBasicData"
															cols = {@cols, @._fBasicColNames}
														end
														
														case "wantCategoryData"
														
														end
														
														case "wantSystemAtts"
														
														end
														
														case "wantRMData"
															cols = {@cols, @._fRecManColNames}
														end
														
														case "wantClassifications"
															cols = {@cols, @._fClassificationColNames}
														end
														
														case "wantOwner"
															cols = {@cols, @._fOwnerColNames}
														end
														
														case "wantPerms"
															cols = {@cols, @._fPermColNames}
														end
														
														case "wantPhysObj"
															cols = {@cols, @._fExportedPOSpecTabColNames}
														end
														
														case "wantPhysObjCirc"
															cols = {@cols, @._fExportedPOCircColNames}
														end
													
														case "wantPhysObjOther"
															cols = {@cols, @._fExportedPOOtherColNames}		
														end
													
														case "wantEmails"
															cols = {@cols, @._fEmailColNames}
														end
														
														case "wantOther"
															cols = {@cols, @{"$NEWNAME", "$NEWPARENTID"}}
														end
													
													
													end
													
												end	
												
											
												return cols
											
											
											end

	
	private function _GetCurrentVals(Object prgCtx = .GetAdminCtx().prgCtx, Integer nodeId = 930315, Assoc options = undefined)
												
												Assoc		vals
												DAPINODE 	node = Dapi.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, nodeId)
												Object 		llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubtype)
												
												if IsUndefined(options)
													options = Assoc.CreateAssoc()
													options.wantNodeId = true
													options.metadataOnly = true
													options.wantBasicData = true
													options.wantClassifications = true
													options.wantRMData = true
													options.wantCategoryData = true
													options.wantSystemAtts = true
													options.wantEmails = false // TODO
													options.wantPhysObj = true
													options.wantAllVersions = false
													options.wantPerms = true
													options.wantOwner = true
													options.wantPhysObjCirc = true
													options.wantPhysObjOther = true	
													options.wantOther = true		
												end
												
												// *** get the version (if it's versioned)
												DAPIVERSION	 version = llNode.fVersioned ? DAPI.GetVersion(	node, DAPI.VERSION_GET_CURRENT) : undefined		
																			
																			
												Object gatherer = $DataManager.NodeMetadataGatherer.New(prgCtx, options, undefined, {})
												
												Assoc status = gatherer.GatherMetadata(prgCtx, node, version, "")
												
												if !status.ok
													echo("ERROR:" + Str.String(status))
													
												else
												
													vals = status.result	
													
													if Length(vals.("$MEDIATYPEFIELDS")) == 0
														Dynamic mediaType = vals.("$PHYSITEMTYPE")
														$DataManager.PoMediaTypes.SetEmptyMediaTypeFields(prgCtx, mediaType, vals)
													end	
													
													
												end	
												
												return vals
											end

	
	private function _GetSourceSettingsAssoc()
											
												Assoc a
												
											    a.hiddenCols = {}
											    a.findReplaceVals = Assoc.CreateAssoc()
											    a.pushDownVals = Assoc.CreateAssoc()
											    a.statusColVals = Assoc.CreateAssoc()    
											
												return Assoc.Copy(a)
											
											end

end

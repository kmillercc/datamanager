package DATAMANAGER

public object ProfilesAPI inherits DATAMANAGER::CrudKiniAPI

	override	String	_fIdFieldName = 'profileId'
	override	Boolean	fCheckPermsOnAddEdit = FALSE


	
	override function Assoc _SubclassValidate(Object prgCtx, \
														 Assoc data)
						
							Assoc 	rtn = .InitErrObj( "_SubclassValidate" )
							
							// *** allow for a new profile here
							if IsDefined(data.newProfileId) && Length(data.newProfileId)
								data.profileId = data.newProfileId
								
								// *** check to see if another profile exists with this name
								Assoc status = ._Get(prgCtx, data.profileId)
								if status.ok && IsDefined(status.result)
									.SetErrObj(rtn, Str.Format("There is already another profile with that name for the %1 operation. Please enter another name.", data.jobSubtype))
									return rtn
								end		
								
							end
							
							// *** remove the newProfileId
							Assoc.Delete(data, "newProfileId")
							
							return rtn
						
						end

end

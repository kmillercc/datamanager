package DATAMANAGER

public object RDORIGINATOR inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsOriginator'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDORIGINATOR'
	override	String	fUpdateApiKey3 = 'fOriginator'

end

package DATAMANAGER

public object ExportIterators inherits DATAMANAGER::AbstractIterator

	public		Dynamic	_fGatherers
	public		String	fCsvPathsKey = 'csvPaths'
	public		String	fCsvRowsKey = 'fileCsvVals'


	
	override function Assoc Finalize()
						
							Assoc 	rtn = .InitErrObj("Finalize")
							Object	gatherer
							String	firstRowStr = undefined
						
						
							// *** loop through gatherers and write out the data
							for gatherer in ._fGatherers
							
								// *** close out the file
								gatherer.Finalize()
						
								// *** get json file
								String jsonPath = gatherer.GetFilePath()
						
								if File.Exists(jsonPath)
									// *** the csvPath will be the same as json file with diff extension
									String csvPath = Str.ReplaceAll(jsonPath, ".json", ".csv")
								
									Assoc firstRowData = gatherer.getFirstRowData()
									if IsDefined(firstRowData)
										firstRowStr = $DataManager.SynCoreInterface.toJSON(firstRowData)
									end	
								
									// *** send a parameter that tells CsvWriter a threshhold (in MB) for doing a line by line reading and writing of the JSON and CSV
									Integer limit = $DataManager.ModulePrefs.GetPref("JSON_SIZE_LIMIT", 50)
								
									// *** call the JsonFileToCsv Method
									Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvWriter", "JsonFileToCsv", {jsonPath, csvPath, firstRowStr, limit} )
									
									if IsError(result)
										return ._ReturnJavaError(rtn, result)
									end		
									
							       	// *** delete the json file
							       	File.Delete(jsonPath)		
								end
							end
						
							._fGatherers = undefined
						 	
							rtn.ok = true
							return rtn
						
						end

	
	override function void Scratch()
						
							Integer rowCount = 4781
							$SyntergyCore.Timer.Start( "Export" )
						
							String jsonPath = "C:\ImportExport\test\small\metadata.json"
							
							// *** the csvPath will be the same as json file with diff extension
							String csvPath = Str.ReplaceAll(jsonPath, ".json", ".csv")
						
							// *** call the JsonFileToCsv Method
							Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvUtil", "JsonFileToCsv", {jsonPath, csvPath} )
							
							if IsError(result)
								echo( JavaObject.GetError() )
						        List errStack = JavaObject.GetErrorStack()
						        String s
						        for s in errStack
						            echo( s )
						        end		
								
							else
								echo("success")	
							end
						
							Real elapsed = $SyntergyCore.Timer.Stop( "Export" )
						
							Echo( "CSV: ", elapsed * 1000.0, "ms, ", Math.Round( rowCount / elapsed ), " rows per second" )		
													
						
						end

	
	private function Object _GetMetadataGatherer(	String filePath, \
																List colNames = {})
						
							Object gatherObject = $DataManager.NodeMetadataGatherer
							String key = ._HashKey(filePath)
						
							if IsUndefined(._fGatherers.(key))
								._fGatherers.(key) = gatherObject.New(._fPrgCtx, ._fOptions, filePath, colNames, true, ._fOptions.catIds)
							end
						
							return ._fGatherers.(key)
						
						
						end

	
	override function void _SubclassDestructor()
							
							._fPrgCtx = undefined
							._fJob = undefined
							._fDataCache = undefined
							._fGatherers = undefined
						end

end

package DATAMANAGER

public object RDESSENTIAL inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.essential'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDESSENTIAL'
	override	String	fUpdateApiKey3 = 'fEssential'

end

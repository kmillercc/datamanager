package DATAMANAGER

public object RENDITIONTYPE inherits DATAMANAGER::Renditions

	override	String	fApiKey3 = 'pType'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RENDITIONTYPE'

end

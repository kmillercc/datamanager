package DATAMANAGER

public object ExportObjects inherits DATAMANAGER::InstanceObjects

	public		Dynamic	_fColNameMap
	public		List	_fColNames
	public		Dynamic	_fConstantVals
	public		Boolean	_fConvertToString = FALSE
	public		Dynamic	_fEmptyVals
	public		String	_fFilePath
	public		Dynamic	_fFileWriter
	public		Dynamic	_fFirstRowData
	public		Boolean	_fFirstRowWritten = FALSE
	public		String	_fOutputType


	
	public function void Finalize()
											
												if IsDefined(._fFileWriter)
													// *** Write out final end bracket and close file
													Assoc status = ._fFileWriter.EndFile()
													if !status.ok
														.GetLogger().Error(status.errMsg)
													end
												else
													.GetLogger().Error("Could not get FileWriter from MetadataGatherer. Could not write out file")
												end	
											
												._fFileWriter = undefined	
											
											end

	
	public function String GetFilePath()
												return ._fFilePath
											end

	
	public function Assoc GetFirstRowData()
											
												return ._fFirstRowData
												
											end

	
	public function Assoc GetLineStrFromVals(List colNames, Assoc vals, String delimiter)
											
												Dynamic	val
												String 	colName
												String	lineStr
											
												// *** loop through and build a line str
												for colName in colNames
													if IsDefined( vals.(colName))
														val = vals.(colName)
													// *** check if the $ is messing it up		
													elseif colName[1] == '$' && IsDefined( vals.(colName[2:]))
														val = vals.(colName[2:])	
													else
														val = ''
													end		
													
													// *** Build the line string by concatenating vals to delimiter 
													lineStr += Length(lineStr) ? delimiter : ''
													lineStr += ._FormatForCsv(val)
												end
												
												return .SuccessResult(lineStr)
													
											end

	
	private function _InitEmptyVals()
												
												String 	colName
												Assoc	vals
												
												for colName in ._fColNames
													vals.(colName) = ._fConvertToString ? "" : undefined
												end 
											
												._fEmptyVals = vals
											end

	
	override function void _SubclassDestructor()
												._fFileWriter = undefined
											end

end

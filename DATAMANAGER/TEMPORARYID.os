package DATAMANAGER

public object TEMPORARYID inherits DATAMANAGER::PhysObjectsSpecTab

	override	String	fApiKey4 = 'TemporaryID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$TEMPORARYID'

end

package DATAMANAGER

public object VERFILESIZE inherits DATAMANAGER::Versions

	override	String	fApiKey3 = 'pFileDataSize'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERFILESIZE'

end

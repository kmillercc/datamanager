package DATAMANAGER

public object getLoadPageConfig inherits DATAMANAGER::LoadRequestHandlers

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'profileModel', -18, 'profileModel', FALSE }, { 'formVals', -18, 'formVals', TRUE, Undefined }, { 'profileFilter', -18, 'profileFilter', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "getLoadPageConfig.DoExec", this )
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
						 	 *  profileModel 			=> Assoc   =>  profileModel
						 	 *  formVals 				=> Assoc   =>  formVals 	 
						 	 *  profileFilter 			=> List    =>  params for filtering the profiles 	 
						     **/	     	
							
							Assoc result, status
								
							// *** get profiles
							status = $DataManager.LoadProfilesAPI.GetFilteredItems(prgCtx, r.profileModel, r.profileFilter, false)
							if status.ok
								result.profiles = status.result
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
								return undefined
							end		
							
							// *** package the result into a successful return assoc
							.fContent = synCore.SuccessResult(result)
							
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{ \
									{ "profileModel", Assoc.AssocType, "profileModel", FALSE }, \		
									{ "formVals", Assoc.AssocType, "formVals", TRUE, undefined }, \
									{ "profileFilter", Assoc.AssocType, "profileFilter", FALSE } \										
								}
						end

end

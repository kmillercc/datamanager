package DATAMANAGER

public object FileWriter inherits DATAMANAGER::FactoryObjects

	public		String	_fDelim
	public		Dynamic	_fFile
	public		Boolean	_fFileIsOpen = FALSE
	public		String	_fFilePath
	public		Boolean	_fIsNew = FALSE
	override	Boolean	fEnabled = TRUE
	override	String	fKey = 'FileWriter'


	
	public function Assoc CloseFile(String s="", String encoding="UTF-8")
											
												Assoc rtn
												rtn.ok = true
											
												// *** Try to close the file
												Dynamic result = File.Close(._fFile)
											
												if IsError(result)
													rtn = .SetErrObj(.InitErrObj("CloseFile"), result)
													rtn.errMsg += " Error closing file."
													return rtn
												end
											
												return rtn
											
											end

	
	public function Assoc DeleteFile()
											
												if File.Exists(._fFilePath)
													// *** Try to delete the file
													Dynamic result = File.Delete(._fFilePath)
											
													if IsError(result)
														return .SetErrObj(.InitErrObj("DeleteFile"), result, undefined)
													end
												end	
											
												Assoc rtn
												rtn.ok = true
												return rtn
											
											end

	
	public function Assoc EndFile(String s="")
											
												Assoc rtn
												rtn.ok = true
											
												// *** Try to close the file
												Dynamic result = File.Close(._fFile)
											
												if IsError(result)
													rtn = .SetErrObj(.InitErrObj("EndFile"), result)
													rtn.errMsg = "Error closing file." + rtn.errMsg
													return rtn
												end
											
												return rtn
											
											end

	
	public function Dynamic New(	Dynamic subtype, \
																	String filePath, \
																	String delim = ",")
											
										Frame f = ._NewFrame(subtype)
									
										f._fFilePath = filePath
										f._fDelim = delim
										
										return f
										
									end

	
	public function Assoc OpenFile()
												
											
												if !._fFileIsOpen
													String filePath = ._fFilePath
													if !File.Exists(filePath)
														if !File.Create(filePath)
															return .SetErrObj(.InitErrObj("OpenFile"), Str.Format("Error creating file at filePath '%1'", filePath))
														end
													end	
													Dynamic f = ._fFile = File.Open(filePath, File.WriteMode)
													if IsError(f)
														return .SetErrObj(.InitErrObj("OpenFile"), Str.Format("Error opening file at filePath '%1'. Error is %2.", filePath, Error.ErrorToString(f)))
													else
														._fFileIsOpen = true
													end
												end	
											
												Assoc rtn
												rtn.ok = true	
												return rtn
												
											end

	
	public function void Write(String s)
											
												Assoc status = .OpenFile()
												if !status.ok
													.GetLogger().Error(status.errMsg)
												end
											
												File.Write(._fFile, s)
												
											end

end

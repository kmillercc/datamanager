package DATAMANAGER

public object searchUsers inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'searchTerm', -1, 'searchTerm', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 *  searchTerm			=> String	=> searchTerm
						     **/
						     
						    RecArray userRecs
						
							RecArray userRecs1 = UAPI.PowerList(prgCtx.USession().fSession,\
																UAPI.SYSTEM, \
																UAPI.USER, \
																r.searchTerm, \
																UAPI.NEXT, \
																10, \
																UAPI.NAME, \
																UAPI.STARTSWITH,\
																0, \
																'')
																
							RecArray userRecs2 = UAPI.PowerList(prgCtx.USession().fSession,\
																UAPI.SYSTEM, \
																UAPI.USER, \
																r.searchTerm, \
																UAPI.NEXT, \
																10, \
																UAPI.LASTNAME, \
																UAPI.STARTSWITH,\
																0, \
																'')
																
							RecArray userRecs3 = UAPI.PowerList(prgCtx.USession().fSession,\
																UAPI.SYSTEM, \
																UAPI.USER, \
																r.searchTerm, \
																UAPI.NEXT, \
																10, \
																UAPI.FIRSTNAME, \
																UAPI.STARTSWITH,\
																0, \
																'')																				
						
							for userRecs in {userRecs1, userRecs2, userRecs3}
								if IsNotError(userRecs)
								
									List userList
										
									Record rec
									
									for rec in userRecs
										Assoc a
										a.label = rec.NAME
										a.value = rec.NAME
										a.id = rec.ID
										
										userList = {@userList, Assoc.Copy(a)}
									end
									
									.fContent = synCore.SuccessResult(userList)
								else
									// for debugging .
									synCore.SetErrObj( rtn, userRecs )
								
									.fError = rtn
									return undefined
								end	
							
							end
								
						
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "searchTerm", StringType, "searchTerm", FALSE } \
								}
						end

end

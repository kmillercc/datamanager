package DATAMANAGER

public object VERSIONSTOKEEP inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'versionsToKeep'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERSIONSTOKEEP'

end

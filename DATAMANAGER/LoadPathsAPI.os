package DATAMANAGER

public object LoadPathsAPI inherits DATAMANAGER::PathsAPI

	override	String	fIniSection = 'SyntBulkLoader.LoadPaths'


	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg	
						
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_LOAD_MNG_PATHS)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end	
							
							return rtn	
						
						end

end

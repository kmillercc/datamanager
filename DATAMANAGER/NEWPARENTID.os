package DATAMANAGER

public object NEWPARENTID inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'parentId'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$NEWPARENTID'

end

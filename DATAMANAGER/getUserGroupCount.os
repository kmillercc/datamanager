package DATAMANAGER

public object getUserGroupCount inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'groupId', 2, 'groupId', TRUE, Undefined } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
							 * 	groupId 		=> Integer 	=> the dataid we are trying to retrieve
							 *  limit			=> Integer	=> limit that tells the counter when to stop counting (prevents trying to count very large node trees)
							 *  includeRoot		=> Boolean	=>  Include the root in the count	 
						     **/
						
							Assoc result
							result.count = 0
						
							if r.includeRoot
								result.count=1
							end
						
							// *** hand off to the API
							Assoc status = $DataManager.UserUtils.GetUserGroupCount(.fPrgCtx, r.groupId, r.lmit, true, result)
							
							if status.ok
								.fContent = synCore.SuccessResult(status.result)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "groupId", IntegerType, "groupId", TRUE, undefined } \
								}
						end

end

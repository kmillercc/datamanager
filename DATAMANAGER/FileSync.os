package DATAMANAGER

public object FileSync inherits DATAMANAGER::CORE::FileIterator

	override	Dynamic	_fChunks
	override	Dynamic	_fParentPaths
	override	Boolean	fEnabled = TRUE
	override	String	fSubKey = 'File_Sync'


	
	override function Void _SubclassCallback(Integer id, \
														String path, \
														Dynamic data, \
														Integer chunkIndex = 1)			
						
							// *** create a new node object to send back to the ui in an array (nodeAray)
							Assoc 	nodeObj = ._NewNodeObj(id, path)	
						
							// *** put into node array
							._AddToCollection(nodeObj, chunkIndex, data)	
							
							 
						end

end

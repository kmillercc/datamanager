package DATAMANAGER

public object TARGETURL inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'targetURL'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$TARGETURL'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc rtn
							
							// *** override in subclasses to provide functionality
							
							rtn.result = val
							rtn.skipNextTime	= true	// *** means that there is no validation at all, so we can skip making this call next time
							rtn.ok = true
							return rtn 
												
						end

end

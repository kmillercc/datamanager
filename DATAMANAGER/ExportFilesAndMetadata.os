package DATAMANAGER

public object ExportFilesAndMetadata inherits DATAMANAGER::Export

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'Export Files and Metadata'
	override	String	fQueryString = 'func=datamanager.index&sourceId=%1#be/exportobjects'

end

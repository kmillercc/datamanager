package DATAMANAGER

public object LoadPrivilegesAPI inherits DATAMANAGER::PrivilegesAPI

	override	String	fIniSection = 'SyntBulkLoader.LoadPrivileges'


	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg	
						
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_LOAD_MNG_PRIVS)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end	
							
							return rtn	
						
						end

end

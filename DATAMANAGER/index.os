package DATAMANAGER

public object index inherits DATAMANAGER::#'DataManager LLRequestHandlers'#

	override	Boolean	fEnabled = TRUE
	override	String	fHTMLFile = 'index.html'
	override	String	fHTMLMimeType = 'text/html'
	public		Integer	fLoadExportSyncLimit
	override	List	fPrototype


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
												
							Object		prgCtx = .fPrgCtx
							Object		allowPkg = $DataManager.AllowPkg
							
							// *** make a call to get the license info with our prgCtx (It gets cached, so it's no big deal)
							Object licenseObj = $DataManager.License
							licenseObj.GetLicenseInfo(prgCtx)
						
							// *** license expired?
							if licenseObj.IsExpired()
								.fError = "Your license has expired."
								return undefined
							end	
						
							// *** are the allowed to use the tool?	
							if !allowPkg.IsAllowed(prgCtx, allowPkg.fEVENT_EXPORT_SYNC) && !allowPkg.IsAllowed(prgCtx, allowPkg.fEVENT_LOAD_SYNC)
								.fError = "You do not have privileges to use this tool"
								return undefined
							end	
								
							.fLoadExportSyncLimit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)		
							
							return undefined	
						End

end

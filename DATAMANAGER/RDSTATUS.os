package DATAMANAGER

public object RDSTATUS inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.fileStatus'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDSTATUS'
	override	String	fUpdateApiKey3 = 'fStatus'

end

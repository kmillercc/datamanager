package DATAMANAGER

public object #'DataManager AdminIndexCallback'# inherits WEBADMIN::AdminIndexCallback

	override	Boolean	fEnabled = TRUE


	
	override function Assoc Execute( \
								Object	prgCtx, \
								Record	request )
								
								Dynamic	apiError
								String	errMsg
								Assoc	retVal
								Assoc	sections
								
								Boolean	ok = TRUE
								String	scriptName = request.SCRIPT_NAME
								Assoc	section_1 = ._NewSection()
								Assoc	section_1_1 = ._NewSection()
								Assoc	section_1_2 = ._NewSection()
								Assoc	section_1_3 = ._NewSection()
								Assoc	section_1_4 = ._NewSection()
								
								//	Section 1.
								section_1.anchorName = "datamanager"
								
								List levels = $DataManager.WebModule.fVersion 
								section_1.heading = Str.Format("Syntergy Data Manager %1.%2.%3", levels[1], levels[2], levels[4])
								sections.( section_1.heading ) = section_1
								
								section_1_1.anchorHREF = scriptName + "?func=datamanager.index"
								section_1_1.heading = "Syntergy Data Manager Console"
								section_1_1.text = "Open the Syntergy Data Manager Console"
								section_1.sections.( section_1_1.heading ) = section_1_1
								
								section_1_2.anchorHREF = scriptName + "?func=datamanager.license"
								section_1_2.heading = "License Key"
								section_1_2.text = "Upload or View License Key Information"
								section_1.sections.( section_1_2.heading ) = section_1_2
								
								section_1_3.anchorHREF = scriptName + "?func=datamanager.AboutDataManagerRH"
								section_1_3.heading = "<order 001 />About Data Manager"
								section_1_3.text = "View Information about this version of Data Manager"
								section_1.sections.( section_1_3.heading ) = section_1_3	
							
								section_1_4.anchorHREF = scriptName + "?func=datamanager.ReleasePendingLoads"
								section_1_4.heading = "Release Pending Loads"
								section_1_4.text = "Release recurring scheduled load jobs that are marked Pending."
								section_1.sections.( section_1_4.heading ) = section_1_4	
							
								if ( IsUndefined( retVal.ok ) )
									
									retVal.ok = ok
									retVal.apiError = apiError
									retVal.errMsg = errMsg
									retVal.sections = sections
								end
								
								return retVal
							end

end

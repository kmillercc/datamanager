package DATAMANAGER

public object RDACCESSION inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsAccession'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDACCESSION'
	override	String	fUpdateApiKey3 = 'fAccession'

end

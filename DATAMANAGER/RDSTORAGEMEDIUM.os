package DATAMANAGER

public object RDSTORAGEMEDIUM inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.storage'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDSTORAGEMEDIUM'
	override	String	fUpdateApiKey3 = 'fStorage'

end

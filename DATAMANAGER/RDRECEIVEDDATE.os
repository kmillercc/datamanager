package DATAMANAGER

public object RDRECEIVEDDATE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsReceivedDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDRECEIVEDDATE'
	override	String	fUpdateApiKey3 = 'fReceivedDate'

end

package DATAMANAGER

public object addEditExportProfile inherits DATAMANAGER::manageprofiles_1

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'data', -18, 'Data', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
												
												Object		synCore = $DataManager.SynCoreInterface
												Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
												Object		prgCtx = .fPrgCtx
													
												/**
												 * R may contain: 
												 * 	data 		=> Assoc 	=> the data object for a filepath config
											     **/		
													
												// *** hand off to the API
												Assoc status = $DataManager.ExportProfilesAPI.AddEditItem(prgCtx, r.data)
												if status.ok
													.fContent = synCore.SuccessResult(status.result)	
												else
													// for debugging .
													synCore.SetErrObj( rtn, status )
												
													.fError = rtn
												end		
												
											
												return undefined	
											End

	
	override function void SetPrototype()
												
												.fPrototype =\
													{\
														{ "data", Assoc.AssocType, "Data", FALSE } \
													}
											end

end

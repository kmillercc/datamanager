package DATAMANAGER

public object RDRECORDDATE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.docDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDRECORDDATE'
	override	String	fUpdateApiKey3 = 'fDocDate'

end

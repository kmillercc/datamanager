package DATAMANAGER

public object EMAILHASATTACHMENTS inherits DATAMANAGER::Email

	override	String	fApiKey4 = 'HasAttachments'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$EMAILHASATTACHMENTS'

end

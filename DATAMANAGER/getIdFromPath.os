package DATAMANAGER

public object getIdFromPath inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'path', -1, 'path', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 *  path			=> String	=> Node Path
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.NodeUtils.GetNodeFromPath(prgCtx, r.path)
							if status.ok
								
								.fContent = synCore.SuccessResult(status.result.pId)
							
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "path", StringType, "path", FALSE } \
								}
						end

end

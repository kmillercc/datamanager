package DATAMANAGER

public object FormatPkg inherits DATAMANAGER::Utils

	
	public function void ConvertAssocVals(	Assoc a, \
																			String toType ="val", \
																			Boolean emptyStrToNull = true)
												
												Dynamic key
												Dynamic val
											
												for key in Assoc.Keys(a)
													val = a.(key)	
												
													if toType == "val"
														if Type(val) == StringType
															
															Integer dataType = (Str.Upper(key) in ._fStringColNames) ? StringType : undefined
															
															Boolean isLikelyDate = IsDefined(Str.LocateI(key, "Date"))
											 				Dynamic newVal = .ConvertStringToVal(val, dataType, isLikelyDate, emptyStrToNull)
											 				
											 				if IsDefined(newVal)
												 				a.(key) = newVal
												 			else
													 			a.(key) = val
												 			end	
											 				
														end
													elseif toType == "string"
														a.(key) = .ConvertValToString(a.(key))
													end		
												end
											
											end

	script ConvertStringToVal
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Dynamic ConvertStringToVal(String valStr, \
																																		Integer dataType = undefined, \
																																		Boolean isLikelyDate = false, \					
																																		Boolean emptyStrToNull = true, \
																																		Integer tzOffset = 0, \
																																		Boolean convertLikelyNumbers = true)
																									
																										Object formatPkg = $LLIAPI.FormatPkg
																										
																										// *** convert empty string to undefined
																										if emptyStrToNull && Str.Trim(valStr) == ''
																										
																											return undefined
																										
																										elseif IsUndefined(valStr)
																										
																											return valStr		
																									
																										elseif dataType==StringType
																										
																											return valStr
																											
																										elseif dataType == BooleanType || Str.Lower(valStr) in {'false','true'}
																										
																											// *** do boolean val only if val is true or false
																											if Str.Lower(valStr) in {'false','true', "1", "0"}
																												Boolean boolVal = formatPkg.StringToVal(valStr, BooleanType)
																												if IsDefined(boolVal)
																													return boolVal
																												end	
																											elseif Str.Lower(valStr) == "on"
																												return true
																											elseif Str.Lower(valStr) == "off"
																												return false	
																											end		
																										
																										elseif dataType == IntegerType || (convertLikelyNumbers && _IsInteger(valStr))
																									
																											// *** try integer
																											Integer intVal = formatPkg.StringToVal(valStr, IntegerType)
																											if IsDefined(intVal) && Type(intVal) == IntegerType
																												return intVal
																											end	
																										
																										elseif dataType == RealType || (convertLikelyNumbers && _IsReal(valStr))
																										
																											// *** try real
																											Real realVal = formatPkg.StringToVal(valStr, RealType)
																											if IsDefined(realVal) && Type(realVal) == RealType
																												return realVal
																											end
																										
																										elseif dataType== DateType || isLikelyDate
																										
																											Date dateVal
																										
																											// *** try to crack with different formats
																											String	fmt
																											List	formats = {	.fISO8601DateFormat, \
																																"%Y-%m-%d", \
																																"%m-%d-%Y", \
																																"%Y/%m/%d", \
																																"%m/%d/%Y", \
																																"%Y-%m-%dT%H:%M:%S", \
																																"%a %b %d %Y", \
																																"%a %b %d %H:%M:%S %Y", \
																																"D/%Y/%m/%d"}
																																
																																
																											for fmt	in formats
																												dateVal = Date.StringToDate(valStr, fmt)
																												if IsDefined(dateVal)
																													return dateVal + tzOffset
																												end
																											end				
																											
																											// *** try the Livelink formatter.  It will look for the date to be the InputDateFormat as defined in opentext.ini
																											dateVal = formatPkg.StringToVal(valStr, DateType)
																											if IsDefined(dateVal) && Type(dateVal) == DateType
																												return dateVal
																											end			
																											
																										end	
																										
																										// *** return undefined if we fail
																										return undefined
																										
																									end	
																									
																									
																									function Boolean _IsLikelyDate(String s)
																										
																										// *** this doesn't work.  Might be worth trying to figure out at some point
																									
																										List match = Pattern.Find(s, '[A-Za-z]+', TRUE)
																										List match2 = Pattern.Find(s, '[0-9]+', TRUE)
																										
																										if IsNotError(match) && IsDefined(match) && IsNotError(match2) && IsDefined(match2)
																											return true
																										end	
																											
																										return false
																									
																									
																									end
																									
																									
																									function Boolean _IsInteger(String s)
																									
																										List match = Pattern.Find(s, '[0-9]+', TRUE)
																										
																										if IsNotError(match) && IsDefined(match)
																											if match[3] == s
																												return true
																											end
																										end	
																											
																										return false
																									
																									end
																									
																									
																									
																									
																									function Boolean _IsReal(String s)
																									
																										List match = Pattern.Find(s, '[0-9,.]+', TRUE)
																										
																										if IsNotError(match) && IsDefined(match)
																											if match[3] == s
																												return true
																											end
																										end	
																											
																										return false
																									
																									end
																									
																									
																									function Boolean _IsAlpha(String s)
																									
																										List match = Pattern.Find(s, '[A-Za-z]+', TRUE)
																										
																										if IsNotError(match) && IsDefined(match)
																											if match[3] == s
																												return true
																											end
																										end	
																											
																										return false
																									
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function ConvertValToString(Dynamic val)
											
												Object formatPkg = $LLIAPI.FormatPkg
												
												// *** default a value
												String valStr = Str.String(val)
											
												if Type(val) == StringType
												
													return val
													
												else
												
													return formatPkg.ValToString(val)
											
												end
												
												return valStr
												
											end

	
	override function Assoc _UserRecFromID(	Object prgCtx, Integer id, Integer typeObj = UAPI.USER)
											
							// *** KM 5/06/16 fixed for case sensitive SQL Server
						
							// *** run the query
							RecArray recs = .ExecSql(prgCtx, "select * from KUAF where Type=:A1 and Deleted=0 and ID=:A2", {typeObj, id})	
						
							// *** evaluate results				
							if IsError(recs)
								return .SetErrObj(.InitErrObj("_UserIDFromName"), recs)
						
							elseif Length(recs)==0		
								return .SetErrObj(.InitErrObj("_UserIDFromName"), Str.Format("Cannot find a valid %1 record for id %2", (typeObj == UAPI.GROUP) ? "group" : "user", id))
								
							else
								Assoc rtn
								rtn.ok  = true
								rtn.result = recs[1]
								return rtn
							end
						
						end

end

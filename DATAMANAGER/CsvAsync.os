package DATAMANAGER

public object CsvAsync inherits DATAMANAGER::CORE::CsvIterator

	override	Boolean	fEnabled = TRUE
	override	String	fSubKey = 'Csv_Async'


	override script _0SetSubkey
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function void _0SetSubtype()
																										.fSubkey = 'Csv_Async'
																									end 
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Assoc _SubclassCallback(Integer id, \
																			Dynamic data, \
																			Integer chunkIndex = 1)								
											
											
												// *** one difference between this and the Sync Iterator is that we don't create an object to send back to the UI in chunks (Distributed Agent will handle this)
												// *** but we still cache the data
												
												// *** increment the collection count
												.fCollectionCount += 1
												
												return undefined
												
											end

end

package DATAMANAGER

public object PoMediaTypes inherits DATAMANAGER::CachedLookupObjects

	public		Dynamic	_fCache
	public		Boolean	_fDoneSetup = FALSE
	public		Dynamic	_fMediaTypeFieldDispNames
	public		List	_fMediaTypeFieldNames
	public		Dynamic	_fMediaTypesByID
	public		Dynamic	_fMediaTypesByName
	override	Boolean	fEnabled = TRUE


	
	public function Assoc ConvertMediaTypeId(Object prgCtx, \
																			Dynamic mediaTypeId)	
											
												Assoc 	rtn = .InitErrObj("GetMediaTypeIdFromCsvData")
												
												// *** run setup (1 time only)
												Assoc status = ._Setup(prgCtx)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												end	
											
												// *** if it's a string and we can't convert to integer, then look for this in the ._fMediaTypesByName
												Dynamic convertedVal = $DataManager.FormatPkg.ConvertStringToVal(mediaTypeId, IntegerType)
													
												if IsUndefined(convertedVal)
												
													// *** means it wasn't converted --it's just a regular string
													// *** it's likely a name we need to map to ID
													
													if IsFeature(._fMediaTypesByName, mediaTypeID)
														mediaTypeId = ._fMediaTypesByName.(mediaTypeId).ID
													else
														.SetErrObj(rtn, Str.Format("Invalid media type id '%1' specified", mediaTypeId))
														return rtn				
													end
											
												elseif Type(convertedVal) == IntegerType
												
													// *** means that it was converted
													
													mediaTypeId = convertedVal
												
												end	
											
											
												rtn.result = mediaTypeId
												
												return rtn
											end

	
	public function Assoc GetMediaFieldRec(	Integer mediaTypeId, \
																				String fieldName, \
																				Object prgCtx = undefined)		
																				
												// *** run setup (1 time only)
												Assoc status = ._Setup(prgCtx)
												if !status.ok
													return .SetErrObj(.InitErrObj("GetMediaTypeFields"), status)
												end
												
												// *** loop through the media types for this id and return the record
												if IsDefined(mediaTypeId)
													if IsDefined(._fMediaTypesByID.(mediaTypeId))
														Record rec
													
														for rec in ._fMediaTypesByID.(mediaTypeId).FIELDS
															if rec.FIELDNAME == fieldName || rec.FIELDDISPLAY == fieldName
																return .SuccessResult(rec)
															end
														end  
													end
												
												else
													
													Assoc a
													
													for a in ._fMediaTypesByID
														Record rec
														for rec in a.FIELDS
															if rec.FIELDNAME == fieldName || rec.FIELDDISPLAY == fieldName
																return .SuccessResult(rec)
															end			
														end	
													end
												
												end
												
												return .SetErrObj(.InitErrObj("GetMediaTypeFields"), Str.Format("Could not find media types for media type id '%1'", mediaTypeId))
																				
											end

	
	public function Assoc GetMediaTypeFields(Object prgCtx, Integer mediaTypeId)
											
												RecArray fields
											
												// *** run setup (1 time only)
												Assoc status = ._Setup(prgCtx)
												if !status.ok
													return .SetErrObj(.InitErrObj("GetMediaTypeFields"), status)
												end
											
												Assoc	mediaTypes = ._fMediaTypesById
											
												if IsFeature(mediaTypes, mediaTypeId)
													fields = mediaTypes.(mediaTypeId).FIELDS
												end	
											
												Assoc rtn
												rtn.ok =  true
												rtn.result = fields
												return rtn
											
											end

	
	public function Assoc GetMediaTypeIdFromName(Object prgCtx, \
																					String name)
																					
												Integer id
												
												// *** run setup (1 time only)
												Assoc status = ._Setup(prgCtx)
												if !status.ok
													return .SetErrObj(.InitErrObj("GetMediaTypeIdFromName"), status)
												end	
												
												if IsFeature(._fMediaTypesByName, Str.Upper(name))
													id = ._fMediaTypesByName.(name).ID
												else
													return .SetErrObj(.InitErrObj("GetMediaTypeIdFromName"), Str.Format("Invalid media type name '%1'", name))
												end
											
												Assoc rtn	
												rtn.result = id
												rtn.ok = true
												return rtn
												
											end

	
	public function Assoc GetMediaTypeNameFromId(Object prgCtx, \
																				  Integer id)
																					
												String name = undefined
												
												// *** run setup (1 time only)
												Assoc status = ._Setup(prgCtx)
												if !status.ok
													return .SetErrObj(.InitErrObj("GetMediaTypeNameFromId"), status)
												end	
												
												if IsDefined(id)
													if IsFeature(._fMediaTypesById, id)
														name = ._fMediaTypesById.(id).NAME
													else
														return .SetErrObj(.InitErrObj("GetMediaTypeNameFromId"), Str.Format("Invalid media type id '%1'", id))
													end
												end
											
												Assoc rtn
												rtn.ok = true	
												rtn.result = name
												
												return rtn
												
											end

	
	public function Boolean IsMediaTypeField(	String fieldName, \
																				Object prgCtx = undefined)
											
												Assoc	rtn = .InitErrObj("IsMediaTypeField")
											
												
												if Str.Upper(fieldName[1:8]) == 'MTFIELD_'
													return true
												
												elseif IsFeature(._fCache, fieldName)
													return ._fCache.(fieldName)
													
												else
													Boolean isIt = false
														
													// *** run setup (1 time only)
													Assoc status = ._Setup(prgCtx)
													if !status.ok
														.SetErrObj(rtn, status)
														return false
													end	
												
													// *** get from the list of media type fields	
													isIt = Str.Upper(fieldName) in ._fMediaTypeFieldNames > 0 || Str.Upper(fieldName) in Assoc.Keys(._fMediaTypeFieldDispNames) > 0
													
													._fCache.(fieldName) = isIt
													
													return isIt		
												end	
												
											end

	
	public function Assoc SetEmptyMediaTypeFields(	Object prgCtx, \
																					Dynamic mediaType, \
																					Assoc csvVals)
											
												// *** run setup (1 time only)
												._Setup(undefined)	
											
												// *** determine media type fields
												RecArray	mediaFields
												Record 		mRec	
												Assoc		mediaTypes = ._fMediaTypesById
												String		valStr
												
												// *** get the mediaTypeId
												Integer mediaTypeId
												
												if Type(mediaType) == IntegerType 
													mediaTypeId = mediaType
												else	
													Assoc status = .ConvertMediaTypeId(prgCtx, mediaType)
													if !status.ok
														return .SetErrObj(.InitErrObj("SetMediaTypeFields"), status)
													else
														mediaTypeId = status.result	
													end
												end	
												
												if IsFeature(mediaTypes, mediaTypeId)
													mediaFields = mediaTypes.(mediaTypeId).FIELDS
													
													// *** add these fields if they don't already exist in the csvVals
													for mRec in mediaFields
														valStr += mRec.FIELDNAME + "=" + "|"		
													end		
														
												else
													return .SetErrObj(.InitErrObj("SetMediaTypeFields"), Str.Format("Cannot find media type fields for media type id %1", mediaTypeId))
												end	
												
												csvVals.('$MediaTypeFields') = IsDefined(valStr) && Length(valStr) ? valStr[1:-2] : valStr					
											
												Assoc rtn
												rtn.ok = true	
												return rtn
											
											end

	script _Setup
	
			
					
							
									
											
													
															
																	
																				
																		function Assoc _Setup(Object prgCtx)
																		
																			Assoc 	rtn = .InitErrObj("_Setup")
																			Dynamic	result
																			String	stmt
																			Assoc 	status	
																		
																			if !._fDoneSetup	
																				._fMediaTypeFieldNames = {}
																				._fMediaTypeFieldDispNames = Assoc.CreateAssoc()	
																				._fMediaTypesByName = Assoc.CreateAssoc()
																				._fMediaTypesByID = Assoc.CreateAssoc()	
																				._fCache = Assoc.CreateAssoc()	
																				
																				// *** set the flag
																				._fDoneSetup = true			
																			
																				if ($PhysicalObjects)
																					if IsUnDefined(prgCtx)
																						status = .GetAdminCtx()
																						if !status.ok
																							.SetErrObj(rtn, "Could not get prgCtx")
																							return rtn
																						else
																							prgCtx = status.prgCtx
																						end
																					end	
																			
																					// *** get media type fields
																					stmt = "select UPPER(FieldName) FieldName, UPPER(FieldDisplay) FieldDisplay from MediaTypes"
																					result = .ExecSql(prgCtx, stmt)
																					if IsNotError(result)
																						Record rec
																						for rec in result
																							._fMediaTypeFieldNames = List.SetAdd(._fMediaTypeFieldNames, rec.fieldname)
																							._fMediaTypeFieldDispNames.(rec.fielddisplay) = rec.fieldname
																						end
																					end		
																			
																					// **** get media types		
																					if IsFeature($PhysicalObjects.Utils, 'GetMediaTypesByUser')
																						status = $PhysicalObjects.Utils.GetMediaTypesByUser(prgCtx)	
																					else
																						status = _GetMediaTypesByUser(prgCtx)
																					end
																					if !status.ok
																						// for debugging
																						.SetErrObj(rtn, status)
																						rtn.errMsg += " Possible permissions error.  Does the user have rights to view the Physical Objects Media Types volume?"
																						return rtn
																					else
																						// *** set the flag
																						._fDoneSetup = true
																					
																						Record rec
																			
																						for rec in status.contents
																							
																							Assoc extData = _DefaultExtData()
																							
																							if Type(rec.EXTENDEDDATA) == StringType 
																								extData = Str.StringToValue(rec.EXTENDEDDATA)
																							elseif IsDefined(rec.EXTENDEDDATA) && Type(rec.EXTENDEDDATA) == Assoc.AssocType 
																								extData = rec.EXTENDEDDATA
																							end	
																						
																							Assoc a
																							._fMediaTypesByName.(rec.NAME) = a = Assoc.CreateAssoc()
																							a.ID = rec.DATAID
																							a.FIELDS = CAPI.Exec(prgCtx.fDbConnect.fConnection, "select * from MediaTypes where MediaTypeID=:A1", rec.DATAID)
																							a.SUBTYPE = extData.Container ? $TypePhysicalItemContainer : (extData.Box ? $TypePhysicalItemBox : $TypePhysicalItem_non_Container)
																							
																							Assoc b
																							._fMediaTypesByID.(rec.DATAID) = b = Assoc.CreateAssoc()
																							b.NAME = rec.NAME				
																							b.FIELDS = CAPI.Exec(prgCtx.fDbConnect.fConnection, "select * from MediaTypes where MediaTypeID=:A1", rec.DATAID)
																							a.SUBTYPE = extData.Container ? $TypePhysicalItemContainer : (extData.Box ? $TypePhysicalItemBox : $TypePhysicalItem_non_Container)		
																						end	
																							
																						
																					end
																				
																				end
																			end
																			
																			return rtn
																		end	
																		
																		function Assoc _DefaultExtData()
																			Assoc a
																			a.Container = false
																			a.Box = false
																			return a
																		end
																		
																		function Assoc _GetMediaTypesByUser( Object prgCtx )
																		
																		
																			Assoc 		retVal
																			Boolean		ok = TRUE
																			String		errMsg 
																			
																			Assoc		result
																			DAPINODE	mediaTypesContainer
																			RecArray	contents
																			Object		adminPrgCtx = Undefined
																			Dynamic		checkVal
																			String		viewName
																			Object		dbConnect = prgCtx.fDbConnect
																			CAPICONNECT	LLConnect = dbConnect.fConnection
																			Dynamic		dbResults
																			Dynamic		item
																			
																			result = $PhysicalObjects.Utils.GetNodeBySubtype( prgCtx, $TypeMediaTypesContainer )
																			
																			if ( result.OK )
																			
																				mediaTypesContainer = result.node
																				
																			else
																			
																				// Lets try with an adminPrgCtx
																				
																				adminPrgCtx = $PhysicalObjects.Utils._GetPrgCtx()
																				
																				if ( IsDefined( adminPrgCtx ) )
																				
																					result = $PhysicalObjects.Utils.GetNodeBySubtype( adminPrgCtx, $TypeMediaTypesContainer )
																						
																					if ( result.OK )
																					
																						mediaTypesContainer = result.node
																						
																					else
																					
																						ok = false
																						errMsg = [PhysObj_Error.CouldNotLocateMediaTypes]
																						
																					end
																					
																				else
																				
																					ok = false
																					errMsg = [PhysObj_Error.CouldNotLocateMediaTypes]
																					
																				end
																				
																			end
																			
																			if ( ok )
																				
																				if IsFeature($LLIApi, 'LanguagePkg') && IsFeature(	$LLIApi.LanguagePkg, "GetMultilingualViewName")
																					checkVal = $LLIApi.LanguagePkg.GetMultilingualViewName( prgCtx, 'WebNodes' )
																					
																					if ( checkVal.ok == TRUE )
																					
																						viewName = checkVal.viewName
																					
																						contents = DAPI.ListContents( prgCtx.DapiSess(), DAPI.BY_DATAID, mediaTypesContainer.pID, viewName, '', $PSee )
																							
																						// that function doesnot return extended data and we need to know the subtype of chosen physical item
																							
																						RecArray.AddField( contents,'ExtendedData')	
																						
																						for item in contents
																						
																							dbResults = CAPI.Exec( LLConnect, "Select ExtendedData from DTree WHERE DataID=:A1",item.DataID )
																							
																							if ( !IsError( dbResults ) )
																							
																								item.ExtendedData = dbResults[1].ExtendedData
																								
																							end
																						
																						end
																					
																					else
																					
																						contents = DAPI.ListContents(prgCtx.DapiSess(), mediaTypesContainer.pVolumeID, mediaTypesContainer.pID, "DTree")  // node to webnode
																						
																					end
																				
																				else
																				
																					contents = DAPI.ListContents(prgCtx.DapiSess(), mediaTypesContainer.pVolumeID, mediaTypesContainer.pID, "DTree")  // node to webnode
																				
																				end	
																				
																				if ( Length(contents) > 0 )
																				
																					RecArray.Sort(contents, "Name")
																					
																				end
																				
																			end
																			
																			
																			
																			retVal.ok = ok
																			retVal.errMsg = errMsg
																			retVal.contents = contents
																		
																			return retVal
																		
																		end
																
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Void __Init()
											
												if .fEnabled
													Object tempObj = OS.NewTemp(this)
													
													$Datamanager.(.OSNAME) = tempObj		
													
													tempObj._fCache = Assoc.CreateAssoc()
													
												end
											
											end

end

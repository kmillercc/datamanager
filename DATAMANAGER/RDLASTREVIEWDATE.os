package DATAMANAGER

public object RDLASTREVIEWDATE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsLastReviewDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDLASTREVIEWDATE'
	override	String	fUpdateApiKey3 = 'fLastReviewDate'

end

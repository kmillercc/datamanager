package DATAMANAGER

public object RDSTATUSDATE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.statusDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDSTATUSDATE'
	override	String	fUpdateApiKey3 = 'fStatusDate'

end

package DATAMANAGER

public object MEMBERS inherits DATAMANAGER::UserGroup

	override	String	fApiKey1 = 'MEMBERS'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$MEMBERS'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc 	rtn
							
							// *** value should come in as string -- parse into list
							List memberList = Str.Elements(val, ",")	
							
							// *** Loop through and convert names to IDs	
							String	name
							List	idList
							for name in memberList
								Integer typeObj = name[1] == "G" ? UAPI.GROUP : UAPI.USER
								name = name[2:]		
						
								RecArray recs = .ExecSql(prgCtx, "select ID from KUAF where Type=:A1 and Deleted=0 and Name=:A2", {typeObj, name})		
								
								if IsError(recs) 
									.SetErrObj(rtn, Str.Format("Error in _GetUserIdList. Result=%1", recs))
									return rtn
								elseif Length(recs)
									idList = List.SetAdd(idList, recs[1].ID)
								else
									.SetErrObj(rtn, Str.Format("Could not find an existing user or group from name %1 in $MEMBERS", name))
									return rtn				
								end
							end 
							
							rtn.result = idList
							rtn.skipNextTime	= false
							rtn.ok = true
							return rtn
												
						end

end

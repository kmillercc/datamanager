package DATAMANAGER

public object JSONWriter inherits DATAMANAGER::FileWriter

	override	String	fSubKey = 'json'


	
	override function Assoc EndFile(String closingElementStr = "]")
											
												Assoc rtn
												rtn.ok = true
											
												// *** Write out the end of the livelink element
												File.Write(._fFile, closingElementStr)
											
												// *** Try to close the file
												Dynamic result = File.Close(._fFile)
											
												if IsError(result)
													rtn = .SetErrObj(.InitErrObj("EndFile"), result)
													rtn.errMsg += " Error closing file."
													return rtn
												end
											
												return rtn
											
											end

	
	public function Assoc StartFile(String startStr = undefined)
												
												
												Assoc status = .OpenFile()
												if !status.ok
													return .SetErrObj(.InitErrObj("StartFile"), status)
												end
												
												if IsDefined(startStr)	
													// *** we're exporting an array of objects, so start with left bracket
													File.Write(._fFile, startStr)		
												end	
											
												Assoc rtn
												rtn.ok = true		
												return rtn
											
											end

end

package DATAMANAGER

public object DropoffJobsAPI inherits DATAMANAGER::CrudAPI

	override	String	_fIdFieldName = 'ID'


	
	override function Assoc _DBToUI(Record rec, Integer tzOffset = 0)
											
				Assoc newAssoc
			
				/* here is the model
		        {"mData": "checkbox", "sName": "checkbox", "aTargets": [0], "sType": "html", "sTitle": '<input type="checkbox" id="selectAll" name="selectAll">'},
		        {"mData": "id", "sName": "id", "aTargets": [1], "sType": "number", "bVisible": false },
		        {"mData": "jobName", "sName": "jobName", "aTargets": [2], "sTitle": "Job Name", "sClass": "center", "sType": "string"},
		        {"mData": "processServer", "sName": "processServer", "aTargets": [3], "sTitle": "Process Server", "sClass": "center", "sType": "string"},
		        {"mData": "loadPath", "sName": "loadPath", "aTargets": [4], "sTitle": "Load Path", "sClass": "center", "sType": "string"},
		        {"mData": "loadId", "sName": "loadId", "aTargets": [5], "sTitle": "Load ID", "sClass": "center", "sType": "number"},
		        {"mData": "options", "sName": "options", "aTargets": [6], "sTitle": "Options", "sClass": "center", "sType": "string", "sWidth": "40%" },
		        {"mData": "Action", "sName": "Action", "aTargets": [7], "sTitle": "Row Actions", "sClass": "center", "sType": "html", "sDefaultContent": '&nbsp;<a href="" class="editor_remove">Delete</a>'}
				*/
		
				newAssoc.("jobtype") = Str.Format('<select class="input-medium jobTypeSelect" name="jobType_%1" id="jobType_%1" data="%1"><option value="loadcsv">Load From CSV</option><option value="loadfilestructure">Load File Structure</option></select>', rec.ID)
				newAssoc.("action") = Str.Format('<select class="input-small" name="jobAction_%1" id="jobAction_%1"><option value="create">Create</option><option value="update">Update</option></select>', rec.ID)
				newAssoc.("checkbox") = Str.Format('<span id="checkboxSpan_%1"><input type="checkbox" value="%1" name="convertCheckbox_%1" class="convertCheckbox"></span>', rec.ID)
				newAssoc.("id") = rec.ID	
				newAssoc.("jobName") = rec.FRIENDLY_NAME
				newAssoc.("processServer") = rec.PROCESS_SERVER
				newAssoc.("loadPath") = rec.LOAD_PATH
				newAssoc.("loadId") = rec.LOAD_ID
				newAssoc.("options") = IsDefined(rec.OPTIONS) ? Str.ReplaceAll(Str.String(rec.OPTIONS), ",", ", ") : ""
							
				
				return newAssoc
								
			end

	
	override function Assoc _Delete(Object prgCtx, \
																	Dynamic id)
											
																			
												Assoc 	rtn = .InitErrObj( "_Delete" )
											
												String stmt = "delete from BULKLOADER_WATCHED_FOLDERS where ID=:A1"
												
												Dynamic result = .ExecSql(prgCtx, stmt, {id})
												if IsError(result)
													.SetErrObj(rtn, result)
													return rtn
												end		
												
											
												
												return .SuccessResult(undefined)
											 
											end

	
	override function Assoc _Get(Object prgCtx, \
																Dynamic id, \
																Dynamic dftValue = undefined, \
																Integer tzOffset = 0)
											
												Assoc 	rtn = .InitErrObj( "_Get" )
											
												if Type(id) == StringType
													id = Str.StringToInteger(id)
												end
											
												String	stmt = "select * from BULKLOADER_WATCHED_FOLDERS where ID=:A1"
											
												RecArray result = .ExecSql(prgCtx, stmt, {id})
												if IsError(result)
													.SetErrObj(rtn, result)
													return rtn
												elseif Length(result)==0
													.SetErrObj(rtn, Str.Format("Could not get job with id %1", id))
													return rtn			
												end	
												
												// *** now we need to massage the record into a data assoc
												Assoc data = ._DbToUi(result[1], tzOffset)
												
												return .SuccessResult(data)
											 
											end

	
	override function Assoc _List(	Object prgCtx, \
																	Assoc filter, \
																	String whereStr = "", \
																	List subs = {})
																	
												Assoc 	rtn = .InitErrObj( "_List" )
												Integer	i = Length(subs)+1
												String	key
												String	stmt = "select * from BULKLOADER_WATCHED_FOLDERS"
												
												if IsDefined(filter)
													whereStr += Length(whereStr) ? " and " : " where "
													// *** filter provides our where clause
													for key in Assoc.Keys(filter)
														Dynamic val = Type(filter.(key)) == Assoc.AssocType ? filter.(key).value : filter.(key)
														String 	operator  = Type(filter.(key)) == Assoc.AssocType ? filter.(key).operator : "="
													
														whereStr += Str.Format("%1 %2 %3 :A%4", i==1 ? '' : ' and ', key, operator, i)
														subs = {@subs, val}
														i+=1
													end
												end	
												
												stmt += whereStr
												
												// *** run the query	
												RecArray result = .ExecSql(prgCtx, stmt, subs)
												if IsError(result)
													.SetErrObj(rtn, result)
													return rtn
												end	
											
												return .SuccessResult(result)	
											
											 
											end

end

package DATAMANAGER

public object CLIENTID inherits DATAMANAGER::PhysObjectsSpecTab

	override	String	fApiKey4 = 'Client_ID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$CLIENTID'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckUser(prgCtx, val)
							
							if status.ok
								Record userRec = status.result
								
								// *** the clientid will need to be a string when we actually perform the update
								val = Str.String(userRec.ID)
								
								// *** also set the Client_Name from this
								apiDataObj.SetValueFromCol("$CLIENTNAME", userRec.NAME, false, action)
								
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

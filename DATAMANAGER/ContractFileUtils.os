package DATAMANAGER

public object ContractFileUtils inherits DATAMANAGER::Utils

	/* 
						* Get the current duration
						*
						* @param {Object}			prgCtx				The program context
						* @param {Record}			request				The input request
						*
						* @return {Assoc} retVal
						* @returnFeature {Boolean} 	ok				A boolean representing the success of the method
						* @returnFeature {String} 	errMsg			An error message that will be set if there was a problem finding the section
						* @returnFeature {Dynamic}	apiError		This will represent any API errors we encounter that are sent back from the C level
						* @returnFeature {Assoc} 	duration		The current duration
						*
						*/
	public function Assoc GetDuration( Object		prgCtx, \
													DAPINODE	node )
								
							Assoc		retVal
							String		errMsg
							Dynamic		apiError
							Dynamic		checkVal
							Assoc		duration
								
							Boolean		ok = TRUE
								
								
							checkVal = $ContractManagement.DurationUtils.GetDurationByDataID( prgCtx, node.pID )
											
							if ( checkVal.ok == TRUE )
											
								duration = checkVal.duration
							else 
									
								ok = FALSE
								errMsg = checkVal.errMsg
								apiError = checkVal.apiError
							end
								
							if ( ok )
								
								if ( IsUndefined( duration.data ) )
										
									duration.data = Assoc.CreateAssoc()
									duration.data.effective = ._GetEffectiveDateFromBaseCategory( prgCtx, node )
									duration.data.currentend = Undefined
									duration.data.earlytermination = Undefined
									duration.data.periods = { ._GetEmptyPeriod( 1, 1, duration.data.effective ) }
									duration.data.options = {}
									duration.data.followup = Assoc.CreateAssoc()
									duration.data.followup.active = FALSE
									duration.data.followup.recipients = { prgCtx.DSession().fUserID }
									duration.data.followup.fuactivation = Assoc.CreateAssoc()
								end
							end
								
							retVal.ok = ok
							retVal.errMsg = errMsg
							retVal.apiError = apiError
							retVal.duration = duration
								
							return retVal
						end

	/* 
						* If there is a base category in the configuration see if it has effective date defined and use it if it does
						*
						* @param {Object}			prgCtx				The program context
						* @param {DAPINODE}			node				The node the duration is on
						*
						* @return {Date}			effectiveDate		The effective date from the base category if we found it
						*
						*/
	private function Date _GetEffectiveDateFromBaseCategory( \
							Object		prgCtx, \
							DAPINODE	node )
								
							Assoc		categories
							Integer		baseCat
							String		location
							Dynamic		checkVal
								
							Date		effectiveDate = Undefined
							Assoc		result = $CMBase.Utils.GetCaseConfig( node.pID, node )
							Assoc		caseConfig = result.content
								
								
							if ( IsDefined( caseConfig ) )
										
								categories = $CMBase.NodeUtils.GetCategoriesForNode( prgCtx, node )
									
								if ( categories.ok == TRUE )
											
									baseCat = caseConfig.category
									location = $CMBase.Utils.GetPref( "EffectiveDate", "Effective Date" )
									checkVal = $CMBase.NodeUtils.GetCategoryValue( prgCtx, categories, baseCat, location )
										
									if ( Type( checkVal ) == ListType && Length( checkVal ) > 0 )
										
										effectiveDate = Date.StringToDate( checkVal[1], '%m/%d/%Y' )
									end
								end	
							end
								
							return effectiveDate
						end

	/* 
						* A function to return an empty period
						*
						* @param {Integer}			periodNumber		The period number
						* @param {Integer}			periodStatus		The period status
						* @param {Date}				periodDate			The date of the period. This is an optional parameter
						*
						* @return {Assoc}			period				The empty period
						*
						*/
	private function Assoc _GetEmptyPeriod( \
							Integer		periodNumber, \
							Integer		periodStatus, \
							Date		periodDate = Undefined )
								
							Assoc		period
							Assoc		status
							Assoc		deadline
							Assoc		termination
							Assoc		renewal
							Assoc		tempDate
								
							String 		dateFormat = $LLIAPI.FormatPkg.GetHTMLDateFormatString()
									
							period = Assoc.CreateAssoc()
							period.nr = periodNumber
							period.name = ""
							period.comments = ""
									
							status = Assoc.CreateAssoc()
							status.status = periodStatus
							status.date = Undefined
							status.user = Undefined
							period.status = {status}
								
							deadline = Assoc.CreateAssoc()
							deadline.date = Undefined
							deadline.count = Undefined
							deadline.unit = Undefined
							period.deadline = deadline
									
							termination = Assoc.CreateAssoc()
							termination.active = false
							termination.date = Undefined
							termination.count = Undefined
							termination.unit = Undefined
							termination.name = ""
							period.termination = termination
								
							renewal = Assoc.CreateAssoc()
							renewal.option = 1
							renewal.date = Undefined
							renewal.count = Undefined
							renewal.unit = Undefined
							renewal.name = ""
							renewal.frequency = 0
							period.renewal = renewal 
								
							tempDate.date = periodDate
								
							if ( periodNumber > 1 )
								
								tempDate.display = [CONTRACTMANAGEMENT_HTML.EndOfPriorPeriod]
							else
								tempDate.display = IsDefined( periodDate ) ? Date.DateToString( periodDate, dateFormat ) : ''
							end
								
							period.date = tempDate
								
							return period
						end

end

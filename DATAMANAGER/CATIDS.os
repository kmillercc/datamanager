package DATAMANAGER

public object CATIDS inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'metadata'
	override	String	fApiKey2 = 'catIds'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$CATIDS'

end

package DATAMANAGER

public object #'Datamanager WebModule'# inherits WEBDSP::WebModule

	override	List	fDependencies
	override	Boolean	fEnabled = TRUE
	override	String	fModuleName = 'datamanager'
	override	String	fName = 'Syntergy Data Manager'
	override	List	fOSpaces = { 'datamanager' }
	override	String	fSetUpQueryString = 'func=datamanager.configure&module=datamanager&nextUrl=%1'
	public		Boolean	fSettingsInheritFromLL = TRUE
	public		String	fSettingsPrefix
	override	List	fVersion = { '16', '0', 'r', '6' }

end

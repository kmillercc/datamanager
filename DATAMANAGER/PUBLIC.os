package DATAMANAGER

public object #'PUBLIC'# inherits DATAMANAGER::Perms

	override	String	fApiKey1 = 'permData'
	override	String	fApiKey2 = 'public'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$PUBLIC'

end

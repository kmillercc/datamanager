package DATAMANAGER

public object ExportAPIProcesses inherits DATAMANAGER::ApiProcesses

	public		String	fMetadataFileName


	
	override function Assoc GetSourceRoot(Object prgCtx, Assoc options)
						
							Dynamic root
							String 	path
							Integer id
						
							// *** implement in subclasses
						
							Assoc result
							result.root = root
							result.rootSourcePath = path
							result.rootFilePath = path	
							result.id = id	
							
							return .SuccessResult(result)
						end

	
	override function GetTargetRoot(Object prgCtx, Assoc options)
														
							Assoc 		rtn = .InitErrObj( "_GetTargetRoot" )
							Integer		id
						
							// *** get the root dir
							String		path = options.rootExportPath
							
							// ** make sure export path has a trailing separator
							path = $DataManager.FileUtils.EnsureDirSeparator(path)
							
							// *** put into a user specific folder
							// path += prgCtx.fUSession.fUserName + File.Separator()
							
							if IsDefined(options.subTargetPath)
								path = path + $DataManager.FileUtils.TrimLeadingSeparator(Str.ReplaceAll(options.subTargetPath, ":", "/"))
								path = $DataManager.FileUtils.EnsureDirSeparator(path)
								
								// *** ensure that this path exists
								Assoc status = $DataManager.FileUtils.EnsurePathExists(path)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end		
								
								// *** does it have children?
								if Length(File.FileList(path))
								
									if options.deleteSub 
									
										$Kernel.FileUtils.DeleteRecursive( path )
										
									elseif options.overwriteFiles
									
										// *** do nothing. Files will get automatically overwritten	
								
									else
										// create a unique directory with the same root name
										path = path[1:-2] + Date.DateToString(Date.Now(), "_%Y-%m-%d@%H.%M.%S") + File.Separator()
										
										// *** ensure that this path exists
										Dynamic result = File.Create(path)
										if IsError(result)
											.SetErrObj(rtn, result)
											return rtn
										end			
									end	
								end
								
							else
								if Length(File.FileList(path))
							
									// *** create a unique folder to contain all the files
									Assoc status = $DataManager.FileUtils.CreateTimeSpecificDir(path, "%Y-%m-%d@%H.%M.%S")
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn
									else
										path = status.result	
									end	
								end		
							end
							
							// *** ensure a separator at end
							path = $DataManager.FileUtils.EnsureDirSeparator(path)
							
							options.rootExportPath = path
							
							Assoc result
							result.path = path
							result.id = id	
							
							// *** now that we have the export location, get the audit log location
							options.auditFolderPath = .GetAuditFolderPath(options)
							
							return .SuccessResult(result)	
							
						end

	
	override function Assoc NewJobPayload(Object prgCtx, Assoc options, String action)
						
							// *** Initialize payload Assoc
							Assoc payload
							
							// *** put the options in 
							payload.options = options	
							
							return payload
						
						end

	
	override function String _GetDefaultAuditPath(Assoc options)
						
							String path
						
							if IsDefined(options.rootExportPath)
								path = options.rootExportPath + "_audit" + File.Separator()
							else
								// **** need to do something here.  For now, log to the opentext logs folder
								path = File.StartupDir() + "logs" + File.Separator() + "datamanager" + File.Separator() 	
							end	
							
							return path			
						end

end

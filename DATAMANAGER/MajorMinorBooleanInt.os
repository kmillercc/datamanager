package DATAMANAGER

public object MajorMinorBooleanInt inherits DATAMANAGER::Versions

	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc rtn
							
							Assoc status = ._CheckBooleanOrInt(val)
							
							if status.ok
								val = status.result
							else
								return status
							end	
							
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

	
	private function Assoc _CheckBooleanOrInt(Dynamic val)
							
							Boolean finalVal = false
							
							if Type(val) == StringType
						
								Boolean bVal
								Integer	iVal
						
								iVal = $DataManager.FormatPkg.ConvertStringToVal(val, IntegerType)
								bVal = $DataManager.FormatPkg.ConvertStringToVal(val, BooleanType)
								
								if IsDefined(iVal) && iVal>=1
									finalVal = true
								elseif IsDefined(bVal)
									finalVal = bVal
								else			
									return .SetErrObj(.InitErrObj("_CheckBooleanOrInt"), Str.Format("'%1' is expected to be a boolean or integer value", val))
								end
								
							elseif Type(val) == IntegerType && val >= 1
							
								finalVal = true
								
							else
								return .SetErrObj(.InitErrObj("_CheckBooleanOrInt"), Str.Format("'%1' is expected to be a boolean or integer value", val))	
							end		
						
							Assoc rtn	
							rtn.result = finalVal
							rtn.ok = true
							return rtn
							
						end

end

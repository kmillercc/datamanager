package DATAMANAGER

public object CrudAPI inherits DATAMANAGER::SynCoreMain

	public		String	_fIdFieldName = 'id'
	public		List	_fRequiredFields
	public		Boolean	fCheckPermsOnAddEdit = TRUE
	public		String	fIniSection = 'DataManager'


	
	public function Assoc AddEditItem(Object prgCtx, \
																		Assoc data, \
																		Boolean updateExisting = true)
											
												Assoc 	rtn = .InitErrObj( "AddEditItem" )
												Assoc	status
												Boolean	update = false
												Dynamic id
												
												// *** allow subclasses to validate
												status = .ValidateItem(prgCtx, data)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												end	
											
												// *** if it's a new object, then we'll get a new id from db
												status = ._GetId(data, prgCtx)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												else
													id = status.result
													data.(._fIdFieldName) = id
												end	
												
												// *** check to see if it's already there and we don't want to update existing
												status = ._Get(prgCtx, id)
												if status.ok && IsDefined(status.result)
													if !updateExisting
														echo("not updating existing pref because updateExisting flag is set to true")
														return .SuccessResult(data) 
													end
													update = true
												end
												
												prgCtx.fDbConnect.StartTrans()
												
													// *** add ours in now
													status = ._Put(prgCtx, id, data, update)
													if !status.ok
														.SetErrObj(rtn, status)
														prgCtx.fDbConnect.EndTrans(false)
														return rtn
													end	
												
												prgCtx.fDbConnect.EndTrans(true)
												
												return .SuccessResult(data)
											
											end

	
	public function Assoc DeleteItem(	Object prgCtx, \
																		Assoc data)
											
												Assoc 	rtn = .InitErrObj( "DeleteItem" )
												Assoc	status
												Dynamic	id
											
												// *** check if this is allowed
												status = ._IsAllowed(prgCtx)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												end
											
												// *** get the id
												status = ._GetId(data)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												else
													id = status.result
												end				
											
												prgCtx.fDbConnect.StartTrans()
												
													// *** delete 
													status = ._Delete(prgCtx, id)
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end	
													
												prgCtx.fDbConnect.EndTrans(true)	
												
												
												return .SuccessResult(data)
											
											end

	
	public function Assoc GetAllItems(Object prgCtx, \
																		Assoc model, \
																		Boolean checkPerms = true, \
																		Integer tzOffset = 0)	
											
												Assoc 	rtn = .InitErrObj( "GetAllItems" )
												Assoc	status
												List	dataList
												String	colName
												
												if checkPerms
													// *** check if this is allowed
													status = ._IsAllowed(prgCtx)
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end	
												end
											
												// *** get a list of all file paths	and pop it into the model assoc
												// *** that way we will return everything plus dataList to the caller in case they need it
												status = ._List(prgCtx, undefined)
												if status.ok
													// ***  _List returns recarray
													Record rec
													for rec in status.result
														Assoc eachData = ._DBToUI(rec, tzOffset)
														dataList = {@dataList, eachData}
													end			
												
													
													// *** if we have a model, put in any missing vals for the UI
													if IsDefined(model)
														Assoc vals
														for vals in dataList
															for colName in model.colNames
																if !IsFeature(vals, colName) && Str.Lower(colName)!='action'
																	vals.(colName) = ''
																end
															end
														end	
													end	
													
												else
													.SetErrObj(rtn, status)
													return rtn
												end	
												
												return .SuccessResult(dataList)
											
											end

	
	public function Assoc GetFilteredItems(	Object prgCtx, \
																				Assoc model, \
																				Assoc filter, \
																				Boolean checkPerms = true, \
																				Integer tzOffset = 0)
											
												Assoc 	rtn = .InitErrObj( "GetFilteredItems" )
												Assoc	status
												List	dataList
												String	colName, key
												List	filterKeys = Assoc.Keys(filter)
												Assoc	vals
												
												if checkPerms
													// *** check if this is allowed
													status = ._IsAllowed(prgCtx)
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end	
												end
												
												// *** get a list of all file paths	and pop it into the model assoc
												// *** that way we will return everything plus dataList to the caller in case they need it
												status = ._List(prgCtx, filter)
												if status.ok
											
													// *** loop through the recarray and return a list of objects
													Record rec
													for rec in status.result
														Boolean add = true
														// ***  _List returns recarray
														vals = ._DBToUI(rec, tzOffset)
											
														for key in filterKeys
															if vals.(key) != filter.(key)
																add = false	
															end
														end
											
														if add	
															if IsDefined(model)
																// *** default in some values for the UI if they are not there
																for colName in model.colNames
																	if !IsFeature(vals, colName) && Str.Lower(colName)!='action'
																		vals.(colName) = ''
																	end
																end		
															end
															
															dataList = {@dataList, Assoc.Copy(vals)}
														end	
													end			
													
												else
													.SetErrObj(rtn, status)
													return rtn
												end	
												
												return .SuccessResult(dataList)
											
											end

	
	public function Assoc GetItem(	Object prgCtx, \
																	Dynamic id)
											
												Assoc 	rtn = .InitErrObj( "GetItem" )
												
												// **** try to get pref
												Assoc status = ._Get(prgCtx, id)
												
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												elseif IsUndefined(status.result)
													.SetErrObj(rtn, Str.Format("Could not get item with id value %1.", id))
													return rtn
												end
												
												return .SuccessResult(status.result)
											
											end

	
	public function Assoc ValidateItem(Object prgCtx, \
																		Assoc data)
											
												Assoc 	rtn = .InitErrObj( "ValidateItem" )
												Assoc	status
												Dynamic	key
												List	dataFields = Assoc.Keys(data)
												List	requiredFields = ._fRequiredFields
												
												if .fCheckPermsOnAddEdit
													// *** check if this is allowed
													status = ._IsAllowed(prgCtx)
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end
												end
											
												for key in requiredFields
													if !key in dataFields || IsUndefined(data.(key)) || data.(key) == ''
														.SetErrObj(rtn, Str.Format('%1  is required.', key))
														return rtn
													end
												end
												
												// *** parse out the boolean values
												Dynamic val
												for key in Assoc.Keys(data)
													val = data.(key)
													if Type(val) == ListType && Length(val) && val[1] in {"true", "false"}
														data.(key) = val[1]
													elseif 	Type(val) == ListType && Length(val)==0
														data.(key) = "false"
													end
												end
												
											
												return ._SubclassValidate(prgCtx, data)
											
											end

	
	private function Assoc _DBToUI(Record rec, Integer tzOffset = 0)
											
												return Assoc.FromRecord(rec)
											
											end

	
	private function Assoc _Delete(	Object prgCtx, \
																	Dynamic id)
											
																			
												Assoc 	rtn = .InitErrObj( "_Delete" )
											
												return rtn
											 
											end

	/*
											* Generates a random GUID from the database
											* 
											*
											* @param {Object} prgCtx				Program session object
											*
											* @return {Dynamic}					Either the GIUD or an Error					
											*
											*/
	private function Assoc _GenerateID( Object prgCtx )
												
												Assoc		rtn = .InitErrObj("_GenerateID")
												Dynamic		rtnVal
												String		stmt
												Dynamic		dbResult
											
												Object		dbConnect = prgCtx.fDbConnect
												
												
												if ( dbConnect.IsOracle() )
												
													stmt = "select sys_guid() from dual"
											
													dbResult = CAPI.Exec( dbConnect.fConnection, stmt )	
													
													if ( IsNotError( dbResult ))
														rtnVal = Str.String(dbResult[ 1 ][ 1 ])
													else
														.SetErrObj(rtn, dbResult)
														return rtn
													end						 
												
													return .SuccessResult(rtnVal)		
												
												else
													Assoc status = ._NewUniqueID(prgCtx)
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end
												
													return .SuccessResult(Str.String(status.result))
												end
											
												
											end

	
	private function Assoc _Get(Object prgCtx, \
																Dynamic id, \
																Dynamic dftValue = undefined)
											
												Dynamic result
											
												return .SuccessResult(result)
											 
											end

	
	private function Assoc _GetId(	Assoc data, \
																	Object prgCtx = undefined)
											
												Assoc 	rtn = .InitErrObj("_GetId")
												Dynamic	id	
												String	idName = ._fIdFieldName
											
												if IsFeature(data, idName) && IsDefined(data.(idName)) && data.(idName) != ""
													return .SuccessResult(data.(idName))
												elseif IsDefined(prgCtx)
													Assoc status = ._GenerateID(prgCtx)
													if !status.ok
														.SetErrObj(rtn, id)
														return rtn	
													end
													
													return .SuccessResult(status.result)		
												
												else
													.SetErrObj(rtn, "Could not get id.")	
													return rtn
												end
											
											
											end

	
	private function Assoc _IsAllowed(Object prgCtx)
											
												Assoc 	rtn = .InitErrObj( "_IsAllowed" )
											
												return rtn
											
											end

	
	private function Assoc _List(Object prgCtx, Assoc filter)
											
												Dynamic result
											
												return .SuccessResult(result)	
											 
											end

	
	private function Assoc _Put(Object prgCtx, \
																Dynamic id, \
																Dynamic value, \
																Boolean update)
																		
												Assoc 	rtn = .InitErrObj( "_Put" )
													
											
												return rtn	
											 
											end

	
	private function Assoc _SubclassValidate(Object prgCtx, \
																		Assoc data)
											
												Assoc 	rtn = .InitErrObj( "_SubclassValidate" )
											
													return rtn
											
											end

	
	private function Assoc _UIToDb(Object prgCtx, Dynamic rVals, Integer tzOffset=0)
							return undefined
						end

end

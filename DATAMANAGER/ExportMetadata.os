package DATAMANAGER

public object ExportMetadata inherits DATAMANAGER::Export

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'Export Metadata'
	override	String	fQueryString = 'func=datamanager.index&sourceId=%1#be/exportmetadata'

end

package DATAMANAGER

public object SynCoreMain inherits SYNTERGYCORE::Main

	public		List	_fBasicColNames = { '$CREATEDATE', '$MODIFYDATE', '$CREATOR', '$DESCRIPTION', '$NICKNAME', '$CATALOG', '$ADVVERSIONING' }
	public		List	_fClassificationColNames = { '$CLASSIFICATION' }
	public		List	_fClearableFields = { '$BOXID', '$BORROWER', '$CLASSIFICATION', '$CLIENTID', '$CLIENTNAME', '$DESCRIPTION', '$FROMDATE', '$KEYWORDS', '$LOCATORTYPE', '$OFFSITESTORID', '$REFERENCERATE', '$TEMPORARYID', '$TODATE' }
	public		List	_fContractColNames = { '$CFCURRENTEND', '$CFEARLYTERMINATION', '$CFEFFECTIVE', '$CFMASTERAGREEMENT', '$CFTERMINATED', '$CMCONTRACTTYPE', '$CMREFNR' }
	public		List	_fEmailColNames = { '$EMAILSUBJECT', '$EMAILTO', '$EMAILFROM', '$EMAILONBEHALFOF', '$EMAILCC', '$EMAILBCC', '$EMAILRECEIVEDDATE', '$EMAILSENTDATE', '$EMAILHASATTACHMENTS', '$EMAILBODYFORMAT', '$EMAILVERSION', '$EMAILPARTICIPANTS' }
	public		List	_fExportedPOCircColNames = { '$BORROWEDDATE', '$BORROWEDBY', '$OBTAINEDBY', '$BORROWCOMMENT', '$CURRENTLOCATION', '$RETURNDATE' }
	public		List	_fExportedPOOtherColNames = { '$LABELTYPE', '$LABELCOPIES', '$AREA', '$FACILITY', '$LOCATOR', '$TRANSFERID', '$NUMCOPIES', '$BOXCLIENT' }
	public		List	_fExportedPOSpecTabColNames = { '$CLIENT', '$FROMDATE', '$KEYWORDS', '$HOMELOCATION', '$LOCATORTYPE', '$OFFSITESTORID', '$REFERENCERATE', '$TODATE', '$TEMPORARYID', '$MEDIATYPEFIELDS', '$UNIQUEID', '$BOXID', '$BOX' }
	public		List	_fGroupColNames = { '$ID', '$TYPE', '$SPACEID', '$NAME', '$USERDATA', '$LEADERID', '$MEMBERS' }
	public		List	_fGroupIdColNames = { '$GROUP' }
	public		List	_fIgnoredColumns = { 'Action', 'Line', 'Status', 'filePath', 'valid', 'parentId', 'nodeId', 'volumeId', 'subType', 'llNode', 'parentNode', 'node', 'name', 'newName', 'itemNo', 'errMsg', '#ActionSpecified', '#ActionPerformed', '#EventDetails', '#Result', '#ErrMsg', '#itemNo' }
	override	String	_fLogClassName = 'datamanager'
	public		String	_fMultiValDelim = '|'
	public		String	_fNdcProcessLog = 'DataManagerProcessLog'
	public		String	_fOspaceName = 'DataManager'
	public		List	_fOtherColNames = { '$NEWNAME', '$NEWPARENTID' }
	public		List	_fOwnerColNames = { '$OWNER', '$GROUP' }
	public		List	_fPOBoxColNames = { '$BOXID', '$BOX', '$BOXPATH' }
	public		List	_fPOCircColNames = { '$BORROWEDDATE', '$BORROWEDBY', '$BORROWEDBYID', '$OBTAINEDBY', '$OBTAINEDBYID', '$BORROWCOMMENT', '$CURRENTLOCATION', '$RETURNDATE' }
	public		List	_fPOCreateColNames = { '$PHYSITEMTYPE', '$HOMELOCATION', '$UNIQUEID', '$KEYWORDS', '$NUMCOPIES', '$LOCATORTYPE', '$REFERENCERATE', '$OFFSITESTORID', '$BOXCLIENT', '$TEMPORARYID', '$LABELTYPE', '$NUMLABELS', '$FROMDATE', '$TODATE' }
	public		List	_fPOLabelColNames = { '$LABELTYPE', '$LABELCOPIES' }
	public		List	_fPOLocatorColNames = { '$AREA', '$FACILITY', '$LOCATOR' }
	public		List	_fPOSpecTabColNames = { '$CLIENT', '$CLIENTID', '$CLIENTNAME', '$FROMDATE', '$KEYWORDS', '$HOMELOCATION', '$LOCATORTYPE', '$OFFSITESTORID', '$REFERENCERATE', '$TODATE', '$TEMPORARYID', '$MEDIATYPEFIELDS' }
	public		List	_fPOXferColNames = { '$TRANSFERID' }
	public		List	_fPermColNames = { '$PERMISSIONS' }
	public		String	_fPkgName = 'datamanager'
	public		List	_fRMClassificationColNames = { '$RDCLASSIFICATION', '$RMCLASSIFICATION' }
	public		List	_fRecManColNames = { '$RDCLASSIFICATION', '$RDSUBJECT', '$RDOFFICIAL', '$RDRECORDDATE', '$RDSTATUS', '$RDSTATUSDATE', '$RDESSENTIAL', '$RDRECEIVEDDATE', '$RDSTORAGEMEDIUM', '$RDACCESSION', '$RDAUTHOR', '$RDORIGINATOR', '$RDADDRESSEE', '$RDOTHERADDRESSEE', '$RDORGANIZATION', '$RDUPDATECYCLEPERIOD', '$RDNEXTREVIEWDATE', '$RDLASTREVIEWDATE', '$RDRSI', '$FILENUMBER' }
	public		List	_fRenditionColNames = { '$RENDITIONTYPE' }
	public		List	_fStringColNames = { '$OBJECTNAME', '$TARGETPATH', '$DESCRIPTION', '$CREATOR', '$NICKNAME', '$NAME', '$LASTNAME', '$MIDDLENAME', '$FIRSTNAME', '$MAILADDRESS', '$CONTACT', '$TITLE' }
	public		List	_fUserColNames = { '$ID', '$OWNERID', '$TYPE', '$SPACEID', '$NAME', '$USERDATA', '$LEADERID', '$GROUPID', '$USERPRIVILEGES', '$LASTNAME', '$MIDDLENAME', '$FIRSTNAME', '$MAILADDRESS', '$CONTACT', '$TITLE' }
	public		List	_fVersionColNames = { '$VERSIONID', '$VERSIONNUM', '$VERDESCRIPTION', '$VERMAJOR', '$VERMINOR', '$VERFILENAME', '$VERFILESIZE', '$VERCREATEDATE', '$VERMODIFIEDDATE', '$VERMIMETYPE', '$VERCREATOR' }
	public		Integer	fEVENT_EXPORT_ASYNC = 1
	public		Integer	fEVENT_EXPORT_MNG_JOBS = 2
	public		Integer	fEVENT_EXPORT_MNG_PATHS = 3
	public		Integer	fEVENT_EXPORT_MNG_PRIVS = 4
	public		Integer	fEVENT_EXPORT_MNG_PROF = 5
	public		Integer	fEVENT_EXPORT_SYNC = 6
	public		Integer	fEVENT_EXPORT_USERS = 7
	public		Integer	fEVENT_LOAD_ASYNC = 8
	public		Integer	fEVENT_LOAD_MNG_JOBS = 9
	public		Integer	fEVENT_LOAD_MNG_PATHS = 10
	public		Integer	fEVENT_LOAD_MNG_PRIVS = 11
	public		Integer	fEVENT_LOAD_MNG_PROF = 12
	public		Integer	fEVENT_LOAD_SYNC = 13
	public		Integer	fEVENT_LOAD_USERS = 14
	public		Integer	fHashAlgorithm = 4
	public		String	fISO8601DateFormat = '%Y-%m-%dT%H:%M:%S'
	public		String	fItemDataKey = 'nodeData'


	
	public function void BreakpointHere()
													// use this to set a temporary breakpoint for debugging the validation process
													echo("here")
													
												end

	
	public function DAPINODE DAPIGetNode(Object prgCtx, DAPINODE parentNode, String path)
	
			// KM 11-26-18 CS16 caches these nodes based on path and sometimes returns the wrong node.
	
	    	// the name equals the last element in path
	    	List  pathElements =  Str.Elements(path, ":")
	    	String name = pathElements[-1]
	
	    	DAPINODE node = DAPI.GetNode(path, parentNode)
	
	    	// logic here is that if the node isn't in the parent or the name doesn't match,
	    	// then it warrants further investigation. Note: the caller could have passed a path instead of just a name,
	    	// in which case the parent could actually be a grandparent, which would trigger this check. That's probably a good thing
	    	// name mismatch should always trigger the check
			if IsNotError(node) && (node.pParentID != parentNode.pID || node.pName != name)
	
				for name in pathElements
					if Str.Trim(name) != ""
						RecArray recs = CAPI.Exec(prgCtx.fDBConnect.fConnection, "select DataID from Dtree where ParentID=:A1 and Name=:A2", parentNode.pID, name)
						if IsNotError(recs) && Length(recs) > 0
	
							node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, recs[1].DATAID)
	
							if IsNotError(node)
								// now, if we have more elements to check, the parentNode will be this object
								// if not, we exit out of the loop and return the last item
								parentNode = node
							else
								// return the error so that the node might get created (if autoCreate option is on)
								return node	
							end	
							
						else
								
							// return an error so that the node might get created (if autoCreate option is on)
							return DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, 99)
				
						end	
					end		
				end	
			end	
	
			return node
	
	    end

	
	override function Assoc GetAdminCtx(String username = "Admin")
													Assoc rtn
													rtn.ok = true
													
													Object cachedSession = $SynPrgCtx
													
													Boolean needNew = false
													
													if username=="Admin" && IsDefined( cachedSession ) 
														if IsFeature( cachedSession, "fDBConnect" ) && \
															CAPI.IsValid( cachedSession.fDBConnect.fConnection )
															rtn.prgCtx = cachedSession
														else
															needNew	= true		
														end
													else
														needNew = true
													end
													
													if needNew
														rtn.prgCtx = ._GetAdminCtx(username).prgCtx
														if username=="Admin"
															$SynPrgCtx = rtn.prgCtx
														end	
													end
													
													rtn.ok = IsDefined( rtn.prgCtx )
													
													if IsUndefined( rtn.prgCtx )
														if IsFeature( this, "getLogger" )
															.getLogger().Error( "failed to create program context" )
														end
													end
																
													return rtn		
												end

	/*final - wrap if necessary, do not override*/
	override function Object GetLogger( String loggerName = "datamanager" )
												
			if ( IsUndefined( loggerName ) )
		
				if ( OS.IsFeature( this, "_fLogClassName" ) )
		
					loggerName = ._fLogClassName
		
				end
		
			end
		
			if ( IsDefined( loggerName ) )
		
				return $SynLog.GetLogger( loggerName )
		
			end
		
			return $SynLog.GetRootLogger()
		
		end

	
	public function Assoc GetNode(Object prgCtx, DAPINODE node, String path)
												
			Assoc status
		
			// *** check for regex by looking for 2 backslashes
			if ._IsRegexExpression(path)
							
				status = ._GetNodeFromRegexPath(prgCtx, node, path)
				if !status.ok	
					return .SetErrObj(.InitErrObj( "GetNode" ), status)
				else
					node = status.result
				end			
							
			else
							
				// *** now get the node with that path inside the root node
				node = .DAPIGetNode(prgCtx, node, path)
				if IsError(node)
					return .SetErrObj(.InitErrObj( "GetNode" ), Str.Format("Could not get node from path '%1'", path))
				end
								
			end
		
			Assoc rtn
			rtn.ok = true
			rtn.result = node
			
			return rtn
			
		end

	
	public function void Scratch()
												
			echoError("Error")
			echoWarn("Warning")
			echoInfo("Info")
			echo("just echo")
		
		end

	
	public function Void SendMessage(	List	recipients, \
																			List	toAddress, \
																			String	subject, \
																			String	body, \
																			Object	prgCtx = .GetAdminCtx().prgCtx)
												
													Frame		mailFrame	// new mail frame
													
													
													if IsDefined(prgCtx)
														CAPICONNECT	capiConn	= prgCtx.fDBConnect.fConnection	// capi connection variable
														Assoc 		mailConfig	= $LLAgent.Utils.ConfigLoad( capiConn, 100 )
														String		sender		= mailConfig.DFTMSGFROMADDRESS
														Dynamic		result
													
													
														if( IsNotError( mailConfig ) )
															// constructor
															mailFrame = $Kernel.SMTPClient.New( mailConfig.SMTPSERVER, mailConfig.SMTPPORT, mailConfig.SMTPHELOHOSTNAME )
													
															if( IsNotError( mailFrame ) && IsDefined( mailFrame ) )
																result = mailFrame.Login()	// login, connect to the "mail" server			
													
																if( result.ok )
																	mailFrame.SendMessage( \
																		sender,\				// Sender
																		recipients,\			// Recipient(s)					
																		toAddress,\            	// To: field
																		sender, \				// From: field
																		sender, \				// Reply to: field
																		subject, \				// Subject: field
																		body ) 					// Message body
																		
																	mailFrame.Logout()
																	
																end //if
																
															end //if
															
														end //if
													end
													
													return
													
												end

	
	public function Assoc SendMessageWithAttachments(	Object 	prgCtx, \
																							List	recipients, \
																							String	subject, \
																							String	body, \
																							List	attachments, \
																							Integer priority = undefined)
														
														String 		sender
														Assoc		status
														Assoc		rtn = .InitErrObj( "SendMessageWithAttachments" )
														
														// get mail config	
														Assoc 		mailConfig	= $LLAgent.Utils.ConfigLoad( prgCtx.fDBConnect.fConnection, 100 )
														if IsError(mailConfig) || IsUndefined(mailConfig)
															return .SetErrObj(rtn, "Notification Configuration for sending email.")
														else
															sender = mailConfig.DFTMSGFROMADDRESS			
														end
														
														// get smtp client
														Frame smtpClient = $Kernel.SMTPClient.New( mailConfig.SMTPSERVER, mailConfig.SMTPPORT, mailConfig.SMTPHELOHOSTNAME )
														if( IsError( smtpClient ) || IsUnDefined( smtpClient ) )
															return .SetErrObj(rtn, "Unable to get SMTP Client")
														end
														
														// Login
														status = smtpClient.Login()	// login, connect to the "mail" server
														if !status.ok		
															return .SetErrObj(rtn, status)
														end
															
														// Set Sender
														status = smtpClient.SetSender( sender )
														if !status.ok
															return .SetErrObj(rtn, status)
														end
														
														// Set the message priority
														smtpClient.SetMessagePriority( priority )
														
														// set recipients
														String addr
														for addr in recipients
															status = smtpClient.AddRecipient( addr )
															if !status.ok
																if IsFeature( this, "GetLogger" )
																	.GetLogger().Error( Str.Format("Failed to add recipient %1 to email. %2", addr, status.errMsg ))
																end	
															end	
														end	
														
														// Begin sending data		
														status = smtpClient.BeginSendingData()
														if !status.ok
															smtpClient.EndSendingData()
															return .SetErrObj(rtn, status)	
														end
																
														// Send header		
														status = smtpClient.SendMessageHeader( recipients, sender, sender, subject, sender)
														if !status.ok
															smtpClient.EndSendingData()
															return .SetErrObj(rtn, status)	
														end
															
														// Send Message Body				
														status = smtpClient.SendMessageBody( body )
														if !status.ok
															smtpClient.EndSendingData()
															return .SetErrObj(rtn, status)	
														end		
												
														// attachments
														Assoc attachment
														Integer i
														for attachment in attachments
															i+=1
															status = smtpClient.SendAttachment( attachment.fileName, \
																								attachment.MIMEType, \
																								i==Length(attachments),	\
																								attachment.name )
															if !status.ok
																if IsFeature( this, "GetLogger" )
																	.GetLogger().Error( Str.Format("Failed to send attachment %1 to email. %2", attachment.fileName, status.errMsg ))
																end	
															end
														end
														
														// End the message
														status = smtpClient.EndSendingData()
														if !status.ok
															return .SetErrObj(rtn, status)	
														end			
												
														return rtn
													
												end

	/*protected final*/
	override function Dynamic SetErrObj( Assoc errObj, \
																			Dynamic result, \
																			Object prgCtx = undefined )
													
													Error apiErrorToDecode = undefined, dbErrorToDecode = undefined
													
													if IsUndefined( errObj )
														// allow anonymous calling.
														errObj = .InitErrObj( "Anonymous" )
													end
												
													String logClassName = "SyntergyCore", OSName = "", contextStr = errObj.methodName
													
													if errObj.context
													
														Assoc contextNames = .GetNameFromContext( errObj.context )	
														logClassName = contextNames.logClassName
														OSName = contextNames.OSName
												
														contextStr = Str.Format( "%1.%2", OSName, errObj.methodName )		
													end
													
												 	if !IsFeature( errObj, "ErrStack" )
												
													 	// *** initialize the error list.
													 	errObj.errStack = { contextStr }
													end
													
													if !IsFeature( errObj, "ErrMsg" )
														errObj.errMsg = ""
													end
													
													// *** Always set OK = FALSE.  If OK = true, you should not be calling this function.
													
													errObj.OK = FALSE 
													
													// *** What is the result?
														
													switch type( result )
														case Assoc.AssocType
													
															// *** This may be another error assoc that was passed in as an error, or a standard livelink error assoc that uses errDetail & errMsg.
															
																	
															// *** if result is an error assoc like errObj, then we just need to copy its contents into errObj and adjust errObj's call stack.
													
															if IsFeature( result, "methodName" )
																
																// *** copy and extend the error stack.			
																if type( result.errStack ) == ListType && Length( result.errStack ) > 0
																	errObj.errStack = { @result.errStack, @errObj.errStack }
																end
																
																// *** copy error message
																errObj.errMsg = result.errMsg
																	
															else
																// *** copy error message...
																errObj.errMsg = result.errMsg
																
																// *** mark the API error for decoding.
																if IsDefined( result.apiError ) && Type(result.apiError) == ErrorType
																	apiErrorToDecode = result.apiError
																end
															end
												
															// *** copy the API Error	
													
															if IsFeature( result, "apiError" )
																errObj.apiError = result.apiError
															end
															
															// fatal
															if IsFeature(result, "fatal")
																errObj.fatal = result.fatal
															end
																
														end
														case ErrorType
															// *** error type passed as result.
															errObj.apiError = result
												
															// *** get the error text.
															errObj.errMsg = Error.ErrorToString( result )
															
															// *** mark for decoding if it's a DB error and we have a prgCtx.
															
															if IsDefined( prgCtx )
																Integer errInt = Error.ID( result )
																
																if errInt >= 675086337 && errInt <= 675086367
																	dbErrorToDecode = result
																end
															end
														end
														case StringType	
															// *** string passed as error description ... just use it as the errMsg.
															errObj.errMsg = result
														end
														default
															// *** string passed as unknown variable type ... just use it cast to a string.
															errObj.errMsg = Str.String( result )
														end
													end
													
													String errDetail	
													
													// *** append error message with any API or DB error that occurred.
													
													if IsDefined(apiErrorToDecode) 
														errDetail = Error.ErrorToString( apiErrorToDecode )
													elseif IsDefined( dbErrorToDecode )
													
														Object dbConnect = undefined
														
														// Convert if DBConnect was passed instead of PrgCtx...
																
														if IsFeature( prgCtx, "fDbConnect" )
															dbConnect = prgCtx.fDBConnect		
														elseif IsFeature( prgCtx, "fConnection" )
															dbConnect = prgCtx
														end
													
														if dbConnect
															errDetail = $SyntergyCore.SQLUtils.GetSQLErrorMessage( dbConnect, dbErrorToDecode )
														end
													end
												
													if errDetail 
														errObj.errMsg = Str.Format( "%1: %2", errObj.errMsg, errDetail )
													end
													
													// *** Log the error if this is the origination point of the error and debug logs are on.
													
													if Length( errObj.errStack ) == 1
														
														// *** log the error message if debug is enabled for the logger in context.
														
														Object log = $SyntergyCore.LogPkg.GetLogger( logClassName )
														
														if log.IsDebugEnabled()
															log.Warn( "{}", { errObj.errMsg } )
														end
													end
														
													return errObj
												end

	script _DateToString
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											function String DateToString(Dynamic d, \
																																		String dateFormat = .fISO8601DateFormat)
																											
																												String 	dateStr = ''
																											
																												if IsDefined(d)	
																													if Type(d) == DateType
																														dateStr = Date.DateToString(d, dateFormat)
																													
																													elseif Type(d) == IntegerType
																													
																														Date aDate = Date.StringToDate(Str.String(d), '%Y%m%d')
																														return DateToString(aDate)
																														
																													end	
																												end	
																												
																												return dateStr
																											
																											end
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function String _FormatForCsv(Dynamic val)
												
													// *** first ensure that it's a string
													val = Type(val) != StringType ? Str.String(val) : val
													
													// *** next, remove any double quotes
													val = Str.ReplaceAll(val, '"', '')
													
													// *** finally, wrap in double quotes
													val = Str.Quote(val, '"')
												
													return val
												end

	
	private function String _FormatNodePath(String path)
												
													if IsUndefined(path)
														return ""
													end
													
													// *** KM 8-18-16 remove the dash for Eurogiciel
													List elements = Str.Elements(path, ":")
													if "-" in elements
														elements = List.SetRemove(elements, "-")
														path = Str.Catenate(elements, ":")	
													end
													
													// *** remove any double colons
													path = Str.ReplaceAll(path, "::", ":")
												
													// *** remove leading and trailing colons
													if path[1]==":"
														path=path[2:]
													end
													if path[-1]	==":"
														path = path[1:-2]
													end
													
													// *** KM 3-4-16 remove the "System Root:" from paths
													
													if Str.Upper(path[1:12]) == "SYSTEM ROOT:"
												
														path = path[13:]
												
													end
												
													return path
													
												end

	script _GetAdminCtx
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											Function Assoc GetAdminCtx(String username = "Admin")
																											
																												String		cnctName = $Kernel.SystemPreferences.GetPrefGeneral( 'DftConnection' )
																											
																												Assoc 	rtn
																												Assoc	prgAssoc
																												Object	prgCtx
																												
																												String 	password = undefined
																												String 	domain = undefined
																												
																												rtn.ok = true
																												
																												if IsDefined( cnctName )
																													
																													prgAssoc = $LLIApi.PrgSession.CreateNewNamed( cnctName, { username, password, domain } )
																													
																													if prgAssoc.OK
																														prgCtx = prgAssoc.pSession
																													 				
																														if IsError( prgAssoc.resetStatus ) && CAPI.IsValid( prgCtx.fDbConnect.fConnection )
																															rtn.errMsg = Error.ErrorToString( prgAssoc.resetStatus )
																															rtn.ok = false
																														end
																														
																														if !CAPI.IsValid( prgCtx.fDbConnect.fConnection )
																															echo( "ERROR: Invalid connection was found, attempting to recover" )
																															$LLIApi.DbConnect.DumpConnectInfo()
																															$LLIApi.DbConnect.DumpLoginInfo()
																													
																															echo( "Deleting all existing program sessions" )
																															
																															for prgCtx in $prgSessions 							
																																prgCtx.Delete()
																															end
																															
																															$LLIApi.DbConnect.DumpConnectInfo()
																															$LLIApi.DbConnect.DumpLoginInfo()
																															
																															prgAssoc = $LLIApi.PrgSession.CreateNewNamed( cnctName, { username, password, domain } )
																															
																															if prgAssoc.OK
																																prgCtx = prgAssoc.pSession
																															else
																																echo( "Error recovering from lost connection" )
																																
																																rtn.ok = false
																																rtn.errMsg = [Web_ErrMsg.DBCnctLost]
																																$CnctLost = TRUE
																																prgCtx = undefined
																															end
																														end
																													end
																												end
																												
																												rtn.prgCtx = prgCtx
																											
																												return rtn
																											end
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function Assoc _GetDataFlagTemplate()
												
													Assoc a
												
													a.hasRecManData = false
													
													a.hasPhysObjData = false
													a.hasPhysObjCirc = false
													a.hasPhysObjBox = false
													a.hasPhysObjLocator = false
													a.hasPhysObjXfer = false
													a.hasPhysObjLabel = false
													
													
													a.hasEmailData = false
													a.hasPermData = false
													a.hasClassification = false
													a.hasAttrData = false	
													a.hasDapiNodeData = false
													a.hasOwnerData = false
													a.hasNickName = false
													a.hasNewName = false
													a.hasMetadata = false
													a.hasFiles = false
													a.hasSystemAttrs = false
													a.hasContractFileData = false
												
													return a
												
												end

	script _GetNodeFromRegexPath
	
			
					
							
									
											
													
														function Assoc _GetNodeFromRegex(Object prgCtx, \
																						DAPINODE parentNode, \
																						String regexStr)
														
															DAPINODE node	
															String stmt = "select dataid, name from dtree where parentid=:A1 and name like :A2"
															String name = regexStr
															Integer counter = 0 
															
															while IsDefined(Str.Locate(name, "\")) && counter < 10
																// *** check for regex by looking for 2 backslashes
																Integer loc1 = Str.Locate(name, "\")
																Integer loc2 = Str.Locate(name[loc1+1:], "\")
																	
																if IsDefined(loc2)	
																	loc2 = loc2+loc1
																							
																	// *** name is everything outside of the regex
																	if loc1>1
																		name = name[1:loc1-1] + "%" + name[loc2+1:]
																	else
																		name = "%" + name[loc2+1:] 	
																	end
																else
																	break	
																end
																counter += 1	
															end		
															
															// *** try to execute query
															//stmt = Str.Format(stmt, name)
															RecArray results = .ExecSQL(prgCtx, stmt, {parentNode.pId, name})	
															if IsError(results)
																return .SetErrObj(.InitErrObj("_GetNodeFromRegex"), Str.Format("Could not find node named %1 in parentid %2", regex, parentNode.pId))
															end
																
															// *** now remove the backslashes because we won't need them below
															regexStr = Str.ReplaceAll(regexStr, "\", "")
																
															Record rec
															for rec in results
																if IsDefined(Pattern.Find( rec.NAME, regexStr ))
																	
																	// *** this is our node
																	node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.DATAID)
																	if IsError(node)
																		return .SetErrObj(.InitErrObj("_GetNodeFromRegex"), Str.Format("Could not get node named %1 in parentid %2. Possible permissions issue.", regex, parentNode.pId))
																	else
																		Assoc rtn
																		rtn.ok = true
																		rtn.result = node
																		return rtn		
																	end
																end
															end
															
															
															return .SetErrObj(.InitErrObj("_GetNodeFromRegex"), Str.Format("Regular expression error. Could not find node named %1 in parentid %2", regex, parentNode.pId))							
														
														end
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function String _HashKey( String s )
												
		
		//Integer algorithm = Security.SHA512
		
		//echo(algorithm)
		
		return Security.Hash( s, 4  )
											
	end

	
	private function Boolean _IsFolderNode(DAPINODE node, \
												   Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType))
				
				return $DataManager.NodeUtils.IsFolderNode(node, llNode)
												
			end

	
	private function _IsRegexExpression(String path)
												
													Integer loc1 = Str.Locate(path, "\")
													if IsDefined(loc1) && Str.Locate(path[loc1+1:], "\")
														return true
													end
												
													return false	
												end

	
	private function Assoc _NewUniqueID(prgCtx)
												
													Assoc	rtn = .InitErrObj("_NewUniqueID")
													Integer id = CAPI.UniqueId(prgCtx.fDBConnect.fLogin)
													
													if IsNotError(id)
														return .SuccessResult(id)
													else
														.SetErrObj(rtn, "Error getting new unique id")
														return rtn
													end	
													
												end

	script _NodeIDToPath
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											function String NodeIDToPath(Object prgCtx, \
																																		Integer dataID)
																												
																												if IsDefined(dataId)	
																													Assoc status = $LLIAPI.LLNodeSubsystem.GetItem(0).NodeFullPathById(prgCtx.DSession(), dataId)
																												
																													if !status.ok
																														// *** for debugging purposes
																														.SetErrObj(.InitErrObj("NodeIDToPath"), status)		
																														return ''
																													else
																														return status.Path
																													end						
																																		
																												else
																													return ''
																												end						
																																			
																											end											
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	script _NodeToPath
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											function String NodeToPath(Object prgCtx, \
																																		DAPINODE node)
																												
																												// *** call the LLNode function
																												Assoc status = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubtype).NodeFullPath(node)
																											
																												if !status.ok
																													// *** for debugging purposes
																													.SetErrObj(.InitErrObj("NodeIDToPath"), status)		
																													return ''
																												else
																													return status.Path
																												end						
																																		
																																			
																											end											
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	script _ReturnJavaError
	
			
					
							
									
											
													
															
																	
																			
																					
																						// Patched by Pat201706010
																						function Assoc _ReturnJavaError(Assoc rtn, Dynamic result, String prefix = "")
																						
																							String errMsg = JavaObject.GetError()
																						
																							if IsUnDefined(errMsg) || Length(errMsg)==0
																								errMsg = Str.String(result)
																							else
																								errMsg = _FormatJavaError(errMsg, prefix)
																							end
																							.SetErrObj(rtn, errMsg)
																							String s
																							Object log = .GetLogger()
																						        for s in JavaObject.GetErrorStack()
																						            log.Error(s + Str.EOL())
																						        end					
																							return rtn  
																						
																						end
																						
																						function _FormatJavaError(String errMsg, String prefix)
																							
																							/*
																							Integer colonLoc = Str.Locate(errMsg, ":")
																							
																							if IsDefined(colonLoc)
																								errMsg = errMsg[colonLoc+2:]
																							end	
																							*/
																							
																							errMsg = Str.ReplaceAll(errMsg, Str.LF, " ")
																							errMsg = Str.ReplaceAll(errMsg, Str.CR, " ")
																							
																							// *** KM 6/1/17 also remove the names of the exceptions
																							errMsg = Str.ReplaceAll(errMsg, "com.syntergy.utils.ReturnedException:", "")
																							errMsg = Str.ReplaceAll(errMsg, "com.syntergy.utils.OscriptException:", "")
																							errMsg = Str.ReplaceAll(errMsg, "  ", "")
																							 
																							
																							return prefix + errMsg
																							
																						end
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function Assoc _UserIDFromName(	Object prgCtx, String name, Integer typeObj = UAPI.USER)
												
												
													// *** run the query
													RecArray recs
													
													if IsDefined(typeObj)
												
														recs = .ExecSql(prgCtx, "select ID from KUAF where Type=:A1 and Deleted=0 and Name=:A2", {typeObj, name})
													else
														
														recs = .ExecSql(prgCtx, "select ID from KUAF where Deleted=0 and Name=:A1", {name})
													end
													
												
													// *** evaluate results				
													if IsError(recs)
														return .SetErrObj(.InitErrObj("_UserIDFromName"), recs)
												
													elseif Length(recs)==0		
														return .SetErrObj(.InitErrObj("_UserIDFromName"), Str.Format("Cannot find a valid %1 record for name %2", (typeObj == UAPI.GROUP) ? "group" : "user", name))
														
													else
														Assoc rtn
														rtn.ok  = true
														rtn.result = recs[1].ID
														return rtn
													end
													
												end

	
	private function String _UserNameFromID(Object prgCtx, Integer ID, boolean includeUGPrefaceChar=false)
												
													String name=undefined, domain
													RecArray users, groups
													
													if IsDefined(ID)
													
														// account for empty owner or groups IDs of -3
														if ID in {-3}
														
															name = '[clear]'
															
														else
														
															UAPISession Usession = prgCtx.USession().fSession
													
															users = UAPI.GetByID( Usession, ID)
														
															if !IsError(users) && length(users) > 0
															
																name = users[1].Name
																
																if includeUGPrefaceChar
																	if users[1].Type == UAPI.USER
																		name = "U" + name
																	else
																		name = "G" + name
																	end
																end			
																
																// see if there is a LL domain involved
																if Isdefined(users[1].SpaceID) && users[1].SpaceID > 0
																	groups = UAPI.GetByID( Usession, users[1].SpaceID) 
																	if !IsError(groups) && length(groups)
																		domain = groups[1].Name
																	end	
																end
															end
															
															if length(domain)
																name = domain + "/" + name
															end			
															
														end
															
													end
													
													return name
												end

	
	private function Assoc _UserRecFromID(	Object prgCtx, Integer id, Integer typeObj = UAPI.USER)
												
													// *** KM 5/06/16 fixed for case sensitive SQL Server
												
													// *** run the query
													RecArray recs = .ExecSql(prgCtx, "select * from KUAF where Type=:A1 and Deleted=0 and ID=:A2", {typeObj, id})	
												
													// *** evaluate results				
													if IsError(recs)
														return .SetErrObj(.InitErrObj("_UserIDFromName"), recs)
												
													elseif Length(recs)==0		
														return .SetErrObj(.InitErrObj("_UserIDFromName"), Str.Format("Cannot find a valid %1 record for id %2", (typeObj == UAPI.GROUP) ? "group" : "user", id))
														
													else
														Assoc rtn
														rtn.ok  = true
														rtn.result = recs[1]
														return rtn
													end
												
												end

	
	private function Assoc _UserRecFromName(	Object prgCtx, \
																					String name, \
																					Integer typeObj = UAPI.USER)
												
													// *** KM 5/06/16 fixed for case sensitive SQL Server
												
													// *** run the query
													RecArray recs = .ExecSql(prgCtx, "select * from KUAF where Type=:A1 and Deleted=0 and Name=:A2", {typeObj, name})	
												
													// *** evaluate results				
													if IsError(recs)
														return .SetErrObj(.InitErrObj("_UserIDFromName"), recs)
												
													elseif Length(recs) == 0	
														return .SetErrObj(.InitErrObj("_UserIDFromName"), Str.Format("Cannot find a valid %1 record for name %2", (typeObj == UAPI.GROUP) ? "group" : "user", name))
														
													else
														Assoc rtn
														rtn.ok  = true
														rtn.result = recs[1]
														return rtn
													end
												
												end

	
	public function void __SetFColNames()
												
													._fUserColNames = {"$ID",\
																	"$OWNERID",\
																	"$TYPE",\
																	"$SPACEID",\
																	"$NAME",\
																	"$USERDATA",\
																	"$LEADERID",\
																	"$GROUPID",\
																	"$USERPRIVILEGES",\
																	"$LASTNAME",\
																	"$MIDDLENAME",\
																	"$FIRSTNAME",\
																	"$MAILADDRESS",\
																	"$CONTACT",\
																	"$TITLE"}					
																	 			
												
													._fGroupColNames = {"$ID",\
																	"$TYPE",\
																	"$SPACEID",\
																	"$NAME",\
																	"$USERDATA",\
																	"$LEADERID",\
																	"$MEMBERS"}
																		
														
													
												
												end

	script __SetPOolNames
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											function void __SetFColNames()
																											
																												._fPOBoxColNames = {'$BOXID','$BOX','$BOXPATH'}
																												._fPOCircColNames = {'$BORROWEDDATE','$BORROWEDBY','$BORROWEDBYID','$OBTAINEDBY','$OBTAINEDBYID','$BORROWCOMMENT','$CURRENTLOCATION','$RETURNDATE'}
																												._fPOLabelColNames = {'$LABELTYPE','$LABELCOPIES'}
																												._fPOLocatorColNames = {'$AREA','$FACILITY','$LOCATOR'}
																												._fPOSpecTabColNames = {'$CLIENT','$CLIENTID','$CLIENTNAME','$FROMDATE','$KEYWORDS','$HOMELOCATION','$LOCATORTYPE','$OFFSITESTORID','$REFERENCERATE','$TODATE','$TEMPORARYID','$MEDIATYPEFIELDS'}	
																												._fPOXferColNames = {'$TRANSFERID'}
																												
																												._fPOCreateColNames = {'$PHYSITEMTYPE','$HOMELOCATION','$UNIQUEID','$KEYWORDS','$NUMCOPIES','$LOCATORTYPE','$REFERENCERATE','$OFFSITESTORID','$BOXCLIENT','$TEMPORARYID','$LABELTYPE','$NUMLABELS','$FROMDATE','$TODATE'}
																													
																												
																											
																											end
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

package DATAMANAGER

public object NodeExport inherits DATAMANAGER::ExportAPIProcesses

	override	Boolean	fEnabled = TRUE
	override	String	fMetadataFileName = 'metadata'
	override	String	fName = 'NodeExport'
	override	String	fSubKey = 'Node.Export'


	
	override function Assoc GetSourceRoot(Object prgCtx, Assoc options)
						
							Assoc 	rtn = .InitErrObj("_GetSourceRoot")
							Dynamic root
							Assoc 	status
							String 	path
							Integer id
						
							if options.sourceType == "file"
							
								path = options.dataIdFilePath
							
							else
								if IsUndefined(options.rootSourceId) && IsUndefined(options.rootSourcePath)
									.SetErrObj(rtn, "Could not determine root source node for export")
									return rtn			
							
								elseif IsDefined(options.rootSourceId)
								
									if options.rootSourceId == $TypeFavoritesVolsVirtual
										.SetErrObj(rtn, Str.Format("You can not export all favorites from the root 'My Favorites' volume. It is possible, though, to export from folders within 'My Favorites.'", options.rootSourceId))
										return rtn		
									end
								
									root = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, options.rootSourceId)
									if IsError(root)
										.SetErrObj(rtn, Str.Format("Could not get root node from rootSourceId %1", options.rootSourceId))
										return rtn								
									end
									path = ._NodeIDToPath(prgCtx, root.pId)
									id = root.pId
								
								elseif IsDefined(options.rootSourcePath)
									// *** replace escaped ampersands
									options.rootSourcePath = Str.ReplaceAll(options.rootSourcePath, ";amp;", "&")
								
									status = $DataManager.NodeUtils.GetNodeFromPath(prgCtx, options.rootSourcePath)
									if !status.ok
										.SetErrObj(rtn, Str.Format("Could not get root node from path %1", options.rootSourcePath))
										return rtn
									end
									id = status.result.pId
									root = status.result
									path = ._NodeIDToPath(prgCtx, root.pId)
							
								end
							end
						
							Assoc result
							result.root = root
							result.rootSourcePath = path
							result.rootFilePath = path	
							result.id = id
								
							
							return .SuccessResult(result)
						end

	
	override function Assoc NewJobPayload(Object prgCtx, Assoc options, String action)
						
							// *** Initialize payload Assoc
							Assoc payload
							
							// *** constant that's hardcoded for now
							options.metadataFileName = 	.fMetadataFileName
							
							// *** the counter we use to set unique names when we truncate name length
							payload.nameTruncNumber = 0
							
							// *** if we have name collision with selected subfolder, do we delete?
							options.deleteSub = IsFeature(options, "deleteSub") && options.deleteSub
							
							// *** Gather options from the form
							options.delimiter = IsDefined(options.delimiter) && Length(options.delimiter) ? options.delimiter : ","
							options.documentFilterDate = IsDefined(options.documentFilterDate) ? options.documentFilterDate : Date.StringToDate('01/01/1900','%m/%d/%Y')	
							options.maxNameLength = IsFeature(options, 'maxNameLength') ? options.maxNameLength : 9999
							options.hierarchyStructure = Str.Lower(options.exportType)=='hierarchy'
							options.flatStructure = Str.Lower(options.exportType)=='flat'	
							options.constantVals = IsFeature(options ,'constantVals') ? options.constantVals : Assoc.CreateAssoc()	
							options.useObjectIdForFileName = IsFeature(options, 'useObjectIdForFileName')
							options.useObjectIdForFolderName = IsFeature(options, 'useObjectIdForFolderName')
							options.numMetadataFiles = IsFeature(options, 'numMetadataFiles') ? options.numMetadataFiles : 1
							options.includeRoot = IsFeature(options, "includeRoot") && options.includeRoot	
							options.wantRenditions = IsFeature(options, "wantRenditions") && options.wantRenditions	
							
							// *** 3/29/16 advanced options put in for Mannai
							options.noCsvFile = IsFeature(options, "noCsvFile") && options.noCsvFile	
							options.overwriteFiles = IsFeature(options, "overwriteFiles") && options.overwriteFiles	
							
							// *** ug options
							options.includeUsers = IsFeature(options, "includeUsers") && options.includeUsers  
							options.includeGroups = IsFeature(options, "includeGroups") && options.includeGroups 	
							
							// *** recurse and get all nodes?
							options.recurse = options.numSubLevels == "All"
						
							// *** metadata only?
							options.metadataOnly = IsFeature(options, 'metadataOnly') && options.metadataOnly	
							options.hierarchyStructure = options.metadataOnly ? false : options.hierarchyStructure
							options.flatStructure = options.metadataOnly ? true : options.flatStructure
							
							// *** options that determine which columns we export
							options.wantContractData = IsFeature(options, 'wantContract')
							options.wantCategoryData = IsFeature(options, 'wantCategoryData')
							options.wantRMData = IsFeature(options, 'wantRMData')
							options.wantClassifications = IsFeature(options, 'wantClassifications')
							options.wantPhysObj = IsFeature(options, 'wantPhysObj')
							options.wantEmails = IsFeature(options, 'wantEmails')
							options.wantProjects = IsFeature(options, 'wantProjects')
							options.wantPerms = IsFeature(options, 'wantPerms')
							options.wantAllVersions = IsFeature(options, 'wantAllVersions')
							options.wantProjects = IsFeature(options, 'wantProjects')
							options.wantBasicData = IsFeature(options, 'wantBasicData')
							options.wantNickname = IsFeature(options, 'wantNickname')	
							options.wantVersionData = IsFeature(options, 'wantVersionData')
							
							// *** do we have colNames?
							options.colNames = IsDefined(options.colNames) ? options.colNames : {}
						
							// *** put the options in 
							payload.options = options	
							
							return payload
						
						end

end

package DATAMANAGER

public object license inherits DATAMANAGER::#'DataManager LLRequestHandlers'#

	public		Dynamic	fContent
	override	Boolean	fEnabled = TRUE
	override	String	fHTMLFile = 'license.html'
	override	String	fHTMLMimeType = 'text/html'
	override	List	fPrototype = { { 'update', 5, 'update', TRUE, FALSE }, { 'licenseSaved', 5, 'licensSaved', TRUE, FALSE }, { 'licenseKey', -1, 'licenseKey', TRUE, Undefined } }
	public		Boolean	fShowLicenseSaved = FALSE


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		prgCtx = .fPrgCtx
							
							if r.update && IsDefined(r.licenseKey)
								$DataManager.ModulePrefs.PutPrefString(prgCtx, "LicenseKey", r.licenseKey )
								$DataManager.License.fLicenseLoaded = false
								
								String nextURL = Str.Format( "%1?func=datamanager.license&licenseSaved=true", r.SCRIPT_NAME)
								
								.fLocation = Str.Format( "%1?func=admin.reset&nextURL=%2", .URL(), Web.EncodeForURL( nextURL ) )
						
							elseif r.licenseSaved
								
								.fShowLicenseSaved = true
							
							end
							
							// *** make a call to get the license info with our prgCtx
							$DataManager.License.GetLicenseInfo(prgCtx)
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "update", BooleanType, "update", TRUE, false }, \
									{ "licenseSaved", BooleanType, "licenseSaved", TRUE, false }, \			
									{ "licenseKey", StringType, "licenseKey", TRUE, undefined } \							
								}
						end

end

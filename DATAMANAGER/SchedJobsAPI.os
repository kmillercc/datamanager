package DATAMANAGER

public object SchedJobsAPI inherits DATAMANAGER::CrudAPI

	public		Dynamic	_fDbToUiMap
	override	String	_fIdFieldName = 'scheduleId'
	public		Dynamic	_fUIToDbMap
	public		String	fSTATUS_COMPLETED = 'COMPLETED'
	public		String	fSTATUS_COMPLETED_SCHEDULED = 'COMPLETED/SCHEDULED'
	public		String	fSTATUS_DISABLED = 'DISABLED'
	public		String	fSTATUS_ERROR = 'ERROR'
	public		String	fSTATUS_ERROR_TRACE = 'ERROR TRACE FILE'
	public		String	fSTATUS_EXPIRED = 'EXPIRED'
	public		String	fSTATUS_INITIALIZING = 'INITIALIZING / VALIDATING'
	public		String	fSTATUS_NONFATAL = 'COMPLETED w/ERRORS'
	public		String	fSTATUS_QUEUED = 'QUEUED'
	public		String	fSTATUS_RUNNING = 'RUNNING'
	public		String	fSTATUS_SCHEDULED = 'SCHEDULED'
	public		String	fSTATUS_SCHEDULE_ERROR = 'SCHEDULE ERROR'
	public		String	fSTATUS_UNKNOWN = 'UNKNOWN (No recent updates)'
	public		String	fSTATUS_VALIDATION_ERRORS = 'COMPLETED w/ERRORS'


	
	override function Assoc AddEditItem(Object prgCtx, \
											Assoc data, \
											Boolean updateExisting = true, \
											Integer tzOffset = 0)
		
			Assoc 	rtn = .InitErrObj( "AddEditItem" )
			Assoc	status
			Boolean	update = false
			Dynamic id
			
			// *** allow subclasses to validate
			status = .ValidateItem(prgCtx, data)
			if !status.ok
				.SetErrObj(rtn, status)
				return rtn
			end	
		
			// *** if it's a new object, then we'll get a new id from db
			status = ._GetId(data, prgCtx)
			if !status.ok
				.SetErrObj(rtn, status)
				return rtn
			else
				id = status.result
				data.(._fIdFieldName) = id
			end	
			
			// *** check to see if it's already there and we don't want to update existing
			status = ._Get(prgCtx, id)
			if status.ok && IsDefined(status.result)
				if !updateExisting
					echo("not updating existing pref because updateExisting flag is set to true")
					return .SuccessResult(data) 
				end
				update = true
			end
			
			prgCtx.fDbConnect.StartTrans()
			
				// *** add ours in now
				status = ._Put(prgCtx, id, data, update, tzOffset)
				if !status.ok
					.SetErrObj(rtn, status)
					prgCtx.fDbConnect.EndTrans(false)
					return rtn
				end	
			
			prgCtx.fDbConnect.EndTrans(true)
			
			return .SuccessResult(data)
		
		end

	
	public function Assoc GetScheduledJobs(Object prgCtx)
													 
			Assoc 	 rtn = .InitErrObj("GetScheduledJobs")
			Date	 now =  CAPI.Now( prgCtx.fDBConnect.fConnection )	
			Integer	 timeSeconds = ._DateToSeconds(now)
			
			// *** build our filter
			List	subs
			String whereStr = " where NEXT_RUN_DATETIME IS NOT NULL"
			
			// *** KM 7/13/18 do not rerun jobs with an error
			whereStr += " and (JOBSTATUS = :A1 or JOBSTATUS = :A2 or JOBSTATUS=:A3 or JOBSTATUS=:A4)"
			subs = {@subs, .fSTATUS_SCHEDULED, .fSTATUS_QUEUED, .fSTATUS_COMPLETED_SCHEDULED, .fSTATUS_NONFATAL}
		
			// *** the current date time has to be greater than or equal to the next run datetime 
			whereStr += " and NEXT_RUN_DATETIME <= :A5"
			subs = {@subs, now}
		
			// *** we also can't run if the job ran less than 5 minutes ago.
			whereStr += " and (LAST_RUN_DATETIME <= :A6 or LAST_RUN_DATETIME IS NULL or JOBSTATUS = :A7)"
			subs = {@subs, now-300, .fSTATUS_QUEUED}
		
			// *** make sure that the start date is OK
			whereStr += " and (START_DATE <= :A8  or START_DATE IS NULL)"
			subs = {@subs, now}
			
			// *** make sure that the end date is OK
			whereStr += " and (END_DATE >= :A9 or END_DATE IS NULL)"
			subs = {@subs, now}
			
			// *** make sure that the start time is ok
			whereStr += " and (START_TIME <= :A10 or START_TIME IS NULL)"
			subs = {@subs, timeSeconds}
			
			// *** make sure that the end time is ok. Don't start anything within 3 minutes of it's end_time
			whereStr += " and (END_TIME >= :A11 or END_TIME IS NULL)"
			subs = {@subs, timeSeconds - 180}
			
			// *** call the function that runs the query
			Assoc status = ._List(prgCtx, undefined, whereStr, subs)
			if !status.ok
				.SetErrObj(rtn, status)
				return rtn
			else
				return .SuccessResult(status.result)
			end	
		end

	
	public function void Init()
												
			._fDbToUiMap = ._GetDbToUIMap()
			._fUIToDbMap = ._GetUIToDbMap()
			
		end

	
	public function Assoc PutDbVals(Object prgCtx, \
									Assoc dbVals, \
									Integer tzOffset = 0)
											
		Assoc 	rtn = .InitErrObj( "PutDbVals" )
		String	key
		String	stmt
		List 	subs
		Integer	i = 1
		Integer scheduleId
		
		// *** get the ui vals by calling DBToUI
		Assoc rVals = ._DbToUi(dbVals)
		
		// *** if it's a new object, then we'll get a new id from db
		Assoc status = ._GetId(dbVals, prgCtx)
		if !status.ok
			.SetErrObj(rtn, status)
			return rtn
		else
			scheduleId = Type(status.result) == StringType ? Str.StringToInteger(status.result) : status.result
			dbVals.(._fIdFieldName) = scheduleId
		end	
			
		// *** get the jobStatus
		String jobStatus = ._GetJobStatus(prgCtx, dbVals)	
	
		// *** get the next date time
		Date next
		if jobStatus != .fSTATUS_SCHEDULED
			next = undefined
		else
			next = $DataManager.Scheduler.GetNextDateTime(dbVals, Date.Now())
			if IsUndefined(next)
				jobStatus = .fSTATUS_SCHEDULE_ERROR
			end 
		end
		
		List	cols = Assoc.Keys(dbVals)
	
	
		// *** default in an undefined last_run_datetime
		Date last = undefined
	
		String stmt1
		String stmt2
		if $DataManager.SynCoreInterface.IsOracle()
			stmt1 = "insert into datamanager_job_schedules (scheduleid, options, jobtype, jobsubtype, jobstatus, last_run_datetime, next_run_datetime, ownerid, date_created, date_modified,"
			stmt2 = " values (:A1,:A2,:A3,:A4,:A5,:A6,:A7,:A8,:A9,:A10,"
			i = 11
			subs = {scheduleId, Str.ValueToString(rVals), rVals.jobtype, rVals.jobsubtype, .fSTATUS_SCHEDULED, last, next, prgCtx.USession().fUserId, CAPI.Now( prgCtx.fDBConnect.fConnection ), CAPI.Now( prgCtx.fDBConnect.fConnection )}			
		else
			stmt1 = "insert into DATAMANAGER_JOB_SCHEDULLES (OPTIONS, JOBTYPE, JOBSUBTYPE, JOBSTATUS, LAST_RUN_DATETIME, NEXT_RUN_DATETIME, OWNERID, DATE_CREATED, DATE_MODIFIED,"
			stmt2 = " values (:A1,:A2,:A3,:A4,:A5,:A6,:A7,:A8,:A9,"			
			i = 10
			subs = {Str.ValueToString(rVals), rVals.jobtype, rVals.jobsubtype, .fSTATUS_SCHEDULED, last, next, prgCtx.USession().fUserId, CAPI.Now( prgCtx.fDBConnect.fConnection ), CAPI.Now( prgCtx.fDBConnect.fConnection )}			
		end	
		
		for key in cols
			key = Str.Lower(key)
		
			if (key in {"scheduleid", "options", "jobtype", "jobsubtype", "jobstatus", "last_run_datetime", "next_run_datetime", "ownerid", "date_created", "date_modified"} == 0)
				stmt1 += Str.Format(" %1,", key)
				stmt2 += Str.Format(":A%1,", i)			
				subs = {@subs, dbVals.(key)}			
				i+=1					
			end
		
		end					
		
		stmt = stmt1[:-2] + ")" + stmt2[:-2] + ")"
	
		// *** try the SQL
		Dynamic result = .ExecSql(prgCtx, stmt, subs)
		if IsError(result)
			.SetErrObj(rtn, result)
			return rtn
		end
	
		return .SuccessResult(rVals)	
	 
	end

	
	public function Assoc RunNow(Object prgCtx, \
		 	   			      Integer scheduleId)
										
			Assoc rtn = .InitErrObj("RunNow")
			
			String stmt = "update DATAMANAGER_JOB_SCHEDULES SET NEXT_RUN_DATETIME=:A1, JOBSTATUS=:A2 WHERE SCHEDULEID=:A3"
			List subs = {CAPI.Now( prgCtx.fDBConnect.fConnection ), .fSTATUS_QUEUED, scheduleId}
		
			prgCtx.fDbConnect.StartTrans()
		
				Dynamic result = .ExecSql(prgCtx, stmt, subs)
				
				if IsError(result)
					prgCtx.fDbConnect.EndTrans(false)	
					.SetErrObj(rtn, result)
					return rtn
				end	
		
			prgCtx.fDbConnect.EndTrans(true)	
			
			return rtn
			
		end

	
	public function Assoc UpdateErrorOnJob(Object prgCtx, \
								   			   Assoc dbVals, \
								   			   Integer scheduleId, \
								   			   String jobStatus = .fSTATUS_ERROR)
										
			Assoc rtn = .InitErrObj("UpdateErrorOnJob")
			
			String stmt = "update DATAMANAGER_JOB_SCHEDULES SET LAST_RUN_DATETIME=:A1, JOBSTATUS=:A2 WHERE SCHEDULEID=:A3"
			List subs = {CAPI.Now( prgCtx.fDBConnect.fConnection ), jobStatus, scheduleId}
		
			prgCtx.fDbConnect.StartTrans()
		
				Dynamic result = .ExecSql(prgCtx, stmt, subs)
				
				if IsError(result)
					prgCtx.fDbConnect.EndTrans(false)	
					.SetErrObj(rtn, result)
					return rtn
				end	
		
			prgCtx.fDbConnect.EndTrans(true)	
			
			return rtn
			
		end

	script UpdateFinishJob
	
			
				// Patched by Pat201510151
				function Assoc UpdateFinishJob(Object prgCtx, \
												   Assoc dbVals, \
												   Integer scheduleId, \
												   Boolean hasFatalError = false, \		// *** KM 10-15-15 default to false
												   Integer nonFatalErrors = 0, \
												   Integer validationErrors = 0)
												   
													
					Assoc 	rtn = .InitErrObj("UpdateFinishJob")
					String	jobStatus
					Boolean	isRecurring = _IsRecurring(prgCtx, dbVals)
					
					if hasFatalError
					
						jobStatus = .fSTATUS_ERROR
					
					elseif nonFatalErrors > 0 || validationErrors > 0
						
						jobStatus = .fSTATUS_NONFATAL
					
				
					elseif isRecurring
						
						jobStatus = .fSTATUS_COMPLETED_SCHEDULED
					
					else
						// must have been one time only
						jobStatus	 = .fSTATUS_COMPLETED
					
					end
				
					Date next = undefined
						
					// *** get the lastRunDateTime as now
					Date lastRunDateTime = CAPI.Now( prgCtx.fDBConnect.fConnection )
				
					// *** get the next date time
					if isRecurring
						next = $DataManager.Scheduler.GetNextDateTime(dbVals, lastRunDateTime)
						if IsUndefined(next)
							jobStatus = .fSTATUS_SCHEDULE_ERROR
						end 
					end
					
					String stmt = "update DATAMANAGER_JOB_SCHEDULES SET LAST_RUN_DATETIME=:A1, NEXT_RUN_DATETIME=:A2, JOBSTATUS=:A3 where SCHEDULEID=:A4"
					List subs = {lastRunDateTime, next, jobStatus, scheduleId}
				
					prgCtx.fDbConnect.StartTrans()
				
						Dynamic result = .ExecSql(prgCtx, stmt, subs)
						
						if IsError(result)
							prgCtx.fDbConnect.EndTrans(false)	
							.SetErrObj(rtn, result)
							return rtn
						end	
				
					prgCtx.fDbConnect.EndTrans(true)	
					
					return rtn
					
				end
				
				function Boolean _IsRecurring(Object prgCtx, \
											 Assoc dbVals)
				
					Boolean isRecurring = dbVals.schedule_type == 2
					
					return isRecurring
				end
			
		
	
	endscript

	
	public function Assoc UpdateJobStatus(	Object prgCtx, \
												String jobStatus, \
												Integer scheduleId)
																				
			Assoc rtn = .InitErrObj("UpdateJobStatus")
			
		
			String stmt = "update DATAMANAGER_JOB_SCHEDULES SET JOBSTATUS=:A1 where SCHEDULEID=:A2"
			List subs = {jobStatus, scheduleId}
		
			prgCtx.fDbConnect.StartTrans()
		
				Dynamic result = .ExecSql(prgCtx, stmt, subs)
				
				if IsError(result)
					prgCtx.fDbConnect.EndTrans(false)	
					.SetErrObj(rtn, result)
					return rtn
				end	
		
			prgCtx.fDbConnect.EndTrans(true)	
			
			return rtn
			
		end

	
	public function Assoc UpdateStartJob(Object prgCtx, \
								   			   Assoc dbVals, \
								   			   Integer scheduleId)
																				
			Assoc rtn = .InitErrObj("UpdateStartJob")
			
			// *** get the jobStatus
			String jobStatus = .fSTATUS_RUNNING
		
			String stmt = "update DATAMANAGER_JOB_SCHEDULES SET JOBSTATUS=:A1 where SCHEDULEID=:A2"
			List subs = {jobStatus, scheduleId}
		
			prgCtx.fDbConnect.StartTrans()
		
				Dynamic result = .ExecSql(prgCtx, stmt, subs)
				
				if IsError(result)
					prgCtx.fDbConnect.EndTrans(false)	
					.SetErrObj(rtn, result)
					return rtn
				end	
		
			prgCtx.fDbConnect.EndTrans(true)	
			
			return rtn
			
		end

	
	private function void _ConvertDailyAtTimesDbToUI(	Assoc vals, Integer tzOffset)
																							
			String valStr = vals.daily_at_times
			
			if IsDefined(valStr)
				
				List 	secondStrings = Str.Elements(valStr, "|")
				String  secondStr
				Integer i
				String	field
				
				for secondStr in secondStrings
					i+=1
					field = "daily_at_time_" + Str.String(i)	 	
					vals.(field) = ._SecondsToTime(Str.StringToInteger(secondStr), tzOffset)
				end
			
				vals.daily_at_times_count = i
				
			end
		
		
		end

	
	private function void _ConvertDailyAtTimesUIToDb(Assoc rVals, \
														Assoc dbVals, \
														Integer tzOffset)
												
			Integer count = IsDefined(rVals.daily_at_times_count) ? rVals.daily_at_times_count : 1
			Integer actualCount
			Integer i
			List	times
			String	field
			String	valStr
			
			// *** figure out what times we got from the UI
			for i = 1 to count
				field = "daily_at_time_" + Str.String(i)	 
				if IsDefined(rVals.(field)) && (Length(Str.Trim(rVals.(field))))
					times = {@times, rVals.(field)}							
					actualCount += 1
				end							
				Assoc.Delete(rVals, field)
			end	
			
			rVals.daily_at_times_count = actualCount					
			
			// *** now put the times back into the dbVals
			for i = 1 to actualCount
				field = "daily_at_time_" + Str.String(i)
				valStr += Str.String(._TimeToSeconds(times[i], tzOffset)) + "|"
			end
		
			// *** finally put the string in for saving to db
			dbVals.daily_at_times = valStr[1:-2]
			
		
		end

	
	override function Assoc _DbToUi(Dynamic dbRec, Integer tzOffset=0)
												
			Assoc  	map = ._fDBToUIMap
			String	key	
			Assoc 	vals
		
			// *** enabled?
			vals.enabled = dbRec.enabled == 1
			
			// *** get latest data from the row in db
			for key in Assoc.Keys(map)
				vals.(map.(key)) = dbRec.(key)		
			end
		
			// *** put in the options that we store in the large test field, but don't overwrite what came from the regular columns
			Assoc options = Str.StringToValue(dbRec.options)
			for key in Assoc.Keys(options)
				if !Assoc.IsKey(vals, key)
					vals.(key) = options.(key)
				end	
			end
			
			// *** handle time conversions
			// TODO: the start and end times and dates need to be adjusted for time zone
		    vals.end_time = IsDefined(vals.end_time) ? ._SecondsToTime(vals.end_time-1, tzOffset) : undefined
		    vals.start_time = IsDefined(vals.start_time) ? ._SecondsToTime(vals.start_time, tzOffset) : undefined
		    
		    // *** handle the daily at times conversions
		    ._ConvertDailyAtTimesDbToUI(vals, tzOffset)
		
			// *** one time date
			if IsDefined(vals.one_time_time)
				Integer secs = ._TimeToSeconds(vals.one_time_time) - tzOffset
				vals.one_time_time = ._SecondsToTime(secs)
			end	
			
			// *** put in a status and last run and next run times
			vals.status = Str.Format('<div id="jobstatus_%1" class="job_status">%2</div><button class="btn btn-mini jobstatus_refresh" id="refreshButton_%1">Refresh Status</button>', dbRec.scheduleId, dbRec.jobstatus)
			vals.last_run_datetime = Str.Format('<div id="lastrun_%1">%2</div>', dbRec.scheduleId, vals.last_run_datetime)
			vals.next_run_datetime = Str.Format('<div id="nextrun_%1">%2</div>', dbRec.scheduleId, vals.next_run_datetime)
			
			// *** put in a loadpath (abstraction of serverCsvPath and folder structure load path)
			vals.loadpath = options.rootSourcePath
			
			return vals
		
		end

	
	private function Integer _DateToSeconds(Date d)
												
			Integer	h = Date.Hour(d)
			Integer	m = Date.Minute(d)
			Integer	s = Date.Second(d)
			
			// *** calculate seconds
			s += (m*60) + (h*3600)
		
			return s
		end

	
	private function void _DefaultStartEndDateTimes(Object prgCtx, Assoc dbVals)
												
			dbVals.start_date 	= IsDefined(dbVals.start_date) ? dbVals.start_date : CAPI.Now( prgCtx.fDBConnect.fConnection )
			dbVals.end_date		= IsDefined(dbVals.end_date) ? dbVals.end_date : Date.StringToDate("12/31/3000", "%m/%d/%Y")
			dbVals.start_time 	= IsDefined(dbVals.start_time) ? dbVals.start_time : 0
			dbVals.end_time 	= IsDefined(dbVals.end_time) ? dbVals.end_time : 86399
		
		
		end

	
	override function Assoc _Delete(Object prgCtx, Dynamic scheduleId)
												
										
			Assoc 	rtn = .InitErrObj( "_Delete" )
	
			String	stmt = 'delete from DATAMANAGER_JOB_SCHEDULES where SCHEDULEID=:A1'
			
			Dynamic result = .ExecSql(prgCtx, stmt, {scheduleId})
			if IsError(result)
				.SetErrObj(rtn, result)
				return rtn
			end	
	
			return .SuccessResult(rtn)
		 
		end

	/*
		* Generates a random GUID from the database
		* 
		*
		* @param {Object} prgCtx				Program session object
		*
		* @return {Dynamic}					Either the GIUD or an Error					
		*
		*/
	override function Assoc _GenerateID( Object prgCtx )
													
			Assoc		rtn = .InitErrObj("_GenerateID")
			Dynamic		rtnVal
			String		stmt
			Dynamic		dbResult
		
			Object		dbConnect = prgCtx.fDbConnect
			
			
			if ( dbConnect.IsOracle() )
			
				stmt = "select DM_JS_SEQ.nextval from dual"
		
				dbResult = CAPI.Exec( dbConnect.fConnection, stmt )	
				
				if ( IsNotError( dbResult ))
					rtnVal = Str.String(dbResult[ 1 ][ 1 ])
				else
					.SetErrObj(rtn, dbResult)
					return rtn
				end						 
			
				return .SuccessResult(rtnVal)		
			
			else
				// ** just need a random dummy number for SQL Server - it doesn't get used anyway
				return .SuccessResult(32999)
			
			end
		
			
		end

	
	override function Assoc _Get(Object prgCtx, Dynamic scheduleId, Dynamic dftValue = undefined, Integer tzOffset = 0)
												
			Assoc 	rtn = .InitErrObj( "_Get" )
		
			if Type(scheduleId) == StringType
				scheduleId = Str.StringToInteger(scheduleId)
			end
		
			String	stmt = 'select * from DATAMANAGER_JOB_SCHEDULES where SCHEDULEID=:A1'
			
			RecArray result = .ExecSql(prgCtx, stmt, {scheduleId})
			if IsError(result)
				.SetErrObj(rtn, result)
				return rtn
			elseif Length(result) == 0
				.SetErrObj(rtn, Str.Format("Could not get job with scheduleId %1", scheduleId))
				return rtn			
			end	
			
			// *** now we need to massage the record into a data assoc
			Assoc data = ._DbToUi(result[1], tzOffset)
			
			return .SuccessResult(data)
		 
		end

	
	private function Assoc _GetDbToUIMap()
												
			Assoc map
		
			map.("scheduleid")  		= "scheduleId"
			map.("jobname")				= "jobname"
			map.("jobdesc")				= "jobdesc"
			map.("jobstatus")				= "jobstatus"
			map.("notify_emails")		= "notify_emails"
			map.("schedule_type")		= "schedule_type"			//1 (1 time only) or 2 (recurring)
			map.("one_time_datetime")	= "one_time_datetime"
			map.("day_pattern")			= "day_pattern"				//1 (Every X Days) or 2 (On certain days of week)
			map.("day_recurrence")		= "day_recurrence"			//number
			map.("daily_frequency")		= "daily_frequency"			//1 (Once during the day) or 2 (At regular intervals during the day)
			map.("daily_every")			= "daily_every"				//number
			map.("daily_every_unit")	= "daily_every_unit"			//1 (Hours) 2 (Minutes)
			map.("start_date")			= "start_date"
			map.("end_date")			= "end_date"
			map.("start_time")			= "start_time"
			map.("end_time")			= "end_time"
			map.("daily_at_times") 		= "daily_at_times"
		
			map.("last_run_datetime") 	= "last_run_datetime"
			map.("next_run_datetime") 	= "next_run_datetime"
		
			return map
		
		end

	
	private function String _GetJobStatus(Object prgCtx, Assoc dbVals)
												
			// *** default the "SCHEDULED" status
			String statusStr =.fSTATUS_SCHEDULED
		
					   			 
			if dbVals.enabled == 0
				// *** disabled
				
				statusStr = .fSTATUS_DISABLED
			
			
			else
		
		
				Date	nextDateTime
				
				if IsDefined(dbVals.NEXT_RUN_DATETIME)
					nextDateTime = dbVals.NEXT_RUN_DATETIME
				else
				
					// *** set next date time based on start date and time
					Date 	startDate 	= IsDefined(dbVals.start_date) ? dbVals.start_date : CAPI.Now( prgCtx.fDBConnect.fConnection )
					Integer startTime 	= IsDefined(dbVals.start_time) ? dbVals.start_time : 0
					nextDateTime = startDate + startTime
		
					Date 	endDate		= IsDefined(dbVals.end_date) ? dbVals.end_date : Date.StringToDate("12/31/3000", "%m/%d/%Y")
					if nextDateTime > endDate
						statusStr = .fSTATUS_EXPIRED
					end
				end	
			
			end	
				
			return statusStr
		
		end

	
	private function Assoc _GetUiToDbMap()
												
			Assoc map
		
			map.("jobname")	= "jobname"
			map.("jobdesc")			= "jobdesc"
			map.("notify_emails")		= "notify_emails"
			map.("schedule_type")		= "schedule_type"			//1 (1 time only) or 2 (recurring)
			map.("one_time_datetime")	= "one_time_datetime"
			map.("day_pattern")		= "day_pattern"				//1 (Every X Days) or 2 (On certain days of week)
			map.("day_recurrence")		= "day_recurrence"			//number
			map.("daily_frequency")		= "daily_frequency"			//1 (Once during the day) or 2 (At regular intervals during the day)
			map.("daily_every")		= "daily_every"				//number
			map.("daily_every_unit")	= "daily_every_unit"			//1 (Hours) 2 (Minutes)
			map.("start_date")		= "start_date"
			map.("end_date")		= "end_date"
			map.("start_time")		= "start_time"
			map.("end_time")		= "end_time"
			
			// *** these are values that we massage in the _UIToDb routine.  we need them for the database
			map.("days_of_week")	= "days_of_week"
			map.("one_time_datetime") = "one_time_datetime"
		
			return map
		
		end

	
	override function Assoc _List(	Object prgCtx, Assoc filter, String whereStr = "", List subs = {})
																		
			/* ****
			
				filter is an assoc. it can be a simple key value pair where the key is a column name and value is the value that that column must have
				e.g. 	filter.ID = 32  (operator defaults to equals)
				
				or it can be a nested Assoc like this with an "operator" and "value" key
				e.g.	filter.NEXT_RUN_DATETIME = Assoc.CreateAssoc()
						filter.NEXT_RUN_DATETIME.operator = "<="
						filter.NEXT_RUN_DATETIME.value = Date.Now()
			*/					
								
		
			Assoc 	rtn = .InitErrObj( "_List" )
			Integer	i = Length(subs)+1
			String	key
			String	stmt = "select * from DATAMANAGER_JOB_SCHEDULES "
			
			if IsDefined(filter)
				whereStr += Length(whereStr) ? " and " : " where "
				// *** filter provides our where clause
				for key in Assoc.Keys(filter)
					Dynamic val = Type(filter.(key)) == Assoc.AssocType ? filter.(key).value : filter.(key)
					String 	operator  = Type(filter.(key)) == Assoc.AssocType ? filter.(key).operator : "="
				
					whereStr += Str.Format("%1 %2 %3 :A%4", i==1 ? '' : ' and ', Str.Upper(key), operator, i)
					subs = {@subs, val}
					i+=1
				end
			end	
			
			stmt += whereStr
			
			// *** run the query	
			RecArray result = .ExecSql(prgCtx, stmt, subs)
			if IsError(result)
				.SetErrObj(rtn, result)
				return rtn
			end	
		
			return .SuccessResult(result)	
		
		 
		end

	
	override function Assoc _Put(Object prgCtx, \
								Dynamic scheduleId, \
								Dynamic rVals, \
								Boolean update, \
								Integer tzOffset = 0)
										
			Assoc 	rtn = .InitErrObj( "_Put" )
			String	key
			String	stmt
			List 	subs
			Integer	i = 1
			Date 	now = CAPI.Now( prgCtx.fDBConnect.fConnection ) 
			
			// *** scheduleId needs to be integer
			if Type(scheduleId) == StringType
				scheduleId = Str.StringToInteger(scheduleId)
			end	
		
			// *** convert ui values to appropriate data types
			Assoc 	dbVals = ._UiToDb(prgCtx, rVals, tzOffset)	
			
			// *** get the jobStatus
			String jobStatus = ._GetJobStatus(prgCtx, dbVals)	
		
			// *** get the next date time
			Date next
			if jobStatus != .fSTATUS_SCHEDULED
				next = undefined
			else
				next = $Datamanager.Scheduler.GetNextDateTime(dbVals, now)
				if IsUndefined(next)
					jobStatus = .fSTATUS_SCHEDULE_ERROR
				end 
			end
			
			List	cols = Assoc.Keys(dbVals)
		
			if update
				stmt = "update DATAMANAGER_JOB_SCHEDULES set OPTIONS=:A1, NEXT_RUN_DATETIME=:A2, DATE_MODIFIED=:A3, JOBSTATUS=:A4, "
				subs = {Str.ValueToString(rVals), next, now, jobStatus}
				
				i = 5
				for key in cols
					// *** KM 6-1-16 make column upper case for case-sensitive SQL
					stmt += Str.Format("%1=:A%2, ", Str.Upper(key), i)
					subs = {@subs, dbVals.(key)}
					i+=1			
				end			
		
				stmt = stmt[:-3]
				stmt += Str.Format(" where SCHEDULEID=:A%1", i)		
				subs = {@subs, scheduleId}
		
				
			else
				// *** default in an undefined last_run_datetime
				Date last = undefined
			
				String stmt1
				String stmt2
				if $Datamanager.SynCoreInterface.IsOracle()
					stmt1 = "insert into DATAMANAGER_JOB_SCHEDULES (SCHEDULEID, OPTIONS, JOBTYPE, JOBSUBTYPE, JOBSTATUS, LAST_RUN_DATETIME, NEXT_RUN_DATETIME, OWNERID, DATE_CREATED, DATE_MODIFIED,"
					stmt2 = " values (:A1,:A2,:A3,:A4,:A5,:A6,:A7,:A8,:A9,:A10,"
					i = 11
					subs = {scheduleId, Str.ValueToString(rVals), rVals.jobtype, rVals.jobsubtype, .fSTATUS_SCHEDULED, last, next, prgCtx.USession().fUserId, CAPI.Now( prgCtx.fDBConnect.fConnection ), CAPI.Now( prgCtx.fDBConnect.fConnection )}			
				else
					stmt1 = "insert into DATAMANAGER_JOB_SCHEDULES (OPTIONS, JOBTYPE, JOBSUBTYPE, JOBSTATUS, LAST_RUN_DATETIME, NEXT_RUN_DATETIME, OWNERID, DATE_CREATED, DATE_MODIFIED,"
					stmt2 = " values (:A1,:A2,:A3,:A4,:A5,:A6,:A7,:A8,:A9,"		
					i = 10
					subs = {Str.ValueToString(rVals), rVals.jobtype, rVals.jobsubtype, .fSTATUS_SCHEDULED, last, next, prgCtx.USession().fUserId, CAPI.Now( prgCtx.fDBConnect.fConnection ), CAPI.Now( prgCtx.fDBConnect.fConnection )}			
				end			
				
				for key in cols
					stmt1 += Str.Format(" %1,", Str.Upper(key))
					stmt2 += Str.Format(":A%1,", i)			
					subs = {@subs, dbVals.(key)}			
					i+=1			
				end					
				
				stmt = stmt1[:-2] + ")" + stmt2[:-2] + ")"
			end
			
		
			// *** try the SQL
			Dynamic result = .ExecSql(prgCtx, stmt, subs)
			if IsError(result)
				.SetErrObj(rtn, result)
				return rtn
			end
		
			return .SuccessResult(rVals)	
		 
		end

	
	private function String _SecondsToTime(	Integer total = 86399, Integer tzOffset = 0)
												
			// *** adjust for the user's time zone
			total = total - tzOffset
		
			// *** get the remainder after dividing by 60
			Integer seconds	= total%60 	
		
			Integer hours = total/3600
			
			Integer minutes = (total - (hours*3600))/60
		
			String s = Str.Format('%1:%2:%3', hours<10 ? Str.Format('0%1', hours) : hours,\
										  minutes<10 ? Str.Format('0%1', minutes) : minutes,\
										  seconds<10 ? Str.Format('0%1', seconds) : seconds)
										  
										  
			return s								  
		end

	
	override function Assoc _SubclassValidate(Object prgCtx, Assoc data)
																			
			Assoc 	rtn = .InitErrObj( "_SubclassValidate" ) 
			
			// *** KM 6-23-16
			// *** make sure the runAsUser actually exists	
			if IsDefined(data.runAsUserName) && Length(data.runAsUserName)
				Assoc status = .GetAdminCtx(data.runAsUserName)
				if !status.ok
					.SetErrObj(rtn, Str.Format('The user "%1" cannot be found in the system. Please specify a valid "Run As User."', data.runAsUserName))
					return rtn	
				end	
			end
				
			// *** validate the schedule values
			switch data.schedule_type
			
				case 1	// one time only
					// *** need to conver the one_time_date and one_time_time to a full date
					if IsUndefined(data.one_time_date) || IsUndefined(data.one_time_time)
						.SetErrObj(rtn, "You must have a One time date and time defined for this schedule type.")
						return rtn
					end
				end
				
				case 2	// recurring
				
					switch data.day_pattern
					
						case 1		// every x days
							
							if IsUndefined(data.day_recurrence)
								.SetErrObj(rtn, "You need to put an integer value in the text box below 'Recurs every X days'")
								return rtn
							end	
						
						end
						
						case 2		// on certain days of week
							// *** determine days of week
							Boolean has1Day = false
							List	days = {"sunday","monday","tuesday","wednesday","thursday","friday","saturday"}
							String  day
							for day in days
								if IsFeature(data, day) && data.(day)
									has1day  = true
									break
								end
							end	
							
							if !has1Day
								.SetErrObj(rtn, "You need to select at least 1 day of the week for this schedule type.")
								return rtn					
							end	
						end
					
					end
					
					switch data.daily_frequency
						
						case 1 		// at specific times during the day
							Integer count = IsDefined(data.daily_at_times_count) ? data.daily_at_times_count : 1
							Integer actualCount
							Integer i
							String	field
							for i = 1 to count
								field = "daily_at_time_" + Str.String(i)	 
								if IsDefined(data.(field)) && (Length(Str.Trim(data.(field))))
									actualCount += 1
								end							
							end	
							
							if actualCount < 1
								.SetErrObj(rtn, "You need to set at least one time in the 'At these Times' field.")
								return rtn
							end						
						end
						
						case 2 		// regular intervals during the day
							if IsUndefined(data.daily_every) || Type(data.daily_every) == StringType
								.SetErrObj(rtn, "You need to set a time for the Every hours or minutes field.")
								return rtn
							elseif data.daily_every_unit==2 && data.daily_every in {1,2,3,4,5,6,7,8,9}
								.SetErrObj(rtn, "Schedule frequency must be no greater than every 10 minutes")
								return rtn						
							end		
						end									
			
					end
				end
			end		
		
		
			return rtn
		
		end

	
	private function Integer _TimeToSeconds(String valStr, Integer tzOffset = 0)
												
			List	els = Str.Elements(valStr, ":")
			Integer	h = Str.StringToInteger(els[1])
			Integer	m = Length(els) > 1 ? Str.StringToInteger(els[2]) : 0
			Integer	s = Length(els) > 2 ? Str.StringToInteger(els[3]) : 0
			
			// *** calculate seconds
			s += (m*60) + (h*3600)
			
			// *** include tzOffset
			s+=tzOffset
			
			return s
		end

	
	override function Assoc _UIToDb(Object prgCtx, Dynamic rVals, Integer tzOffset=0)
												
			Assoc 	dbVals
		
			// *** enabled?
			dbVals.enabled = IsFeature(rVals, "enabled") && rVals.enabled ? 1 : 0
			
			// *** mark after loading
			rVals.markAfterLoading = IsFeature(rVals, "markAfterLoading") && rVals.markAfterLoading	
			
			// *** get the one time date time for the database
			if rVals.schedule_type == 1
				Date 	d = rVals.one_time_date
				Integer	s = ._TimeToSeconds(rVals.one_time_time, tzOffset)
				d = d + s
				dbVals.one_time_datetime = d
			end
			
			// *** get the string we'll put into the days_of_week column	
			if rVals.schedule_type == 2 && rVals.day_pattern == 2
				// *** recurring schedule on certain days of week
		
				String	dayStr = ''
				List	days = {"sunday","monday","tuesday","wednesday","thursday","friday","saturday"}
				String  day
				for day in days
					if IsFeature(rVals, day) && rVals.(day)
						dayStr += "1"
					else
						dayStr += "0"	
					end
				end	
				dbVals.days_of_week = dayStr
			end		
			
			// *** convert daily at times
			if rVals.daily_frequency == 1
				._ConvertDailyAtTimesUIToDb(rVals, dbVals, tzOffset)
			end	
			
			// *** build up the options
			String	key	
			Assoc map = ._fUIToDbMap	
			for key in Assoc.Keys(map)
				if IsDefined(rVals.(key))
					dbVals.(map.(key)) = rVals.(key)		
						
					// *** we can pop these off the rVals that will go into options
					Assoc.Delete(rVals, key)
				end	
			end
			
			// *** convert start date and time
			Date start_date 	= IsDefined(dbVals.start_date) ? dbVals.start_date : undefined
			Integer start_time 	= IsDefined(dbVals.start_time) ? ._TimeToSeconds(dbVals.start_time) : undefined
			
			// *** adjust using time zone
			if IsDefined(dbVals.start_date) && IsDefined(dbVals.start_time)
				// TODO time zone adjustment
			end
			dbVals.start_date = start_date
			dbVals.start_time = start_time		
			
		
			// *** convert end date and time
			Date end_date		= IsDefined(dbVals.end_date) ? dbVals.end_date : undefined
			Integer end_time 	= IsDefined(dbVals.end_time) ? ._TimeToSeconds(dbVals.end_time) : undefined
			
			// *** adjust using time zone
			if IsDefined(dbVals.end_date) && IsDefined(dbVals.end_time)
				// TODO time zone adjustment	
			end	
			dbVals.end_date = end_date
			dbVals.end_time = end_time
			
			// *** set the description if it's not set
			dbVals.jobdesc = IsDefined(dbVals.jobdesc) ? dbVals.jobdesc : dbVals.jobname
		
			return dbVals 
		 
		end

end

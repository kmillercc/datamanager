package DATAMANAGER

public object AuditAPI inherits DATAMANAGER::InstanceObjects

	public		String	_fFiles
	public		Dynamic	_fFilesOpen
	public		Boolean	_fFirstRowWritten = FALSE
	public		Boolean	_fHeaderDone = FALSE
	public		String	_fLogPath
	public		String	_fNdcDeltaLogName = 'DMDeltaLog'
	override	Boolean	fEnabled = TRUE
	public		Boolean	fHasCsvLock = FALSE


	
	public function Assoc CheckResults(Object prgCtx, \
											Object dataCache)
											
			
			Assoc result = dataCache.Get(prgCtx, {"AuditResults"}, Assoc.AssocType)
			
			if IsUndefined(result)
				result = Assoc.CreateAssoc()
				result.nonFatalErrors = 0
			end
		
			return .SuccessResult(result)
									
		end

	
	public function void ClearLogger()
			$DataManager.SynCoreInterface.PopNDC()
		end

	
	public function void Close(List ndcNames = {})
												
			// *** KM 4-22-16 close out of log files
			if Length(ndcNames)
				Object logger = .GetLogger()
				String ndcName
				for ndcName in ndcNames
					if IsDefined(logger) && IsFeature(logger.fLogAppenders, ndcName)
						logger.fLogAppenders.( ndcName ).Close()
					end
				end		
			end
		end

	
	public function void LockCsvFiles(String logFolderPath)
											
			// *** ensure a lock on the csv file
			String lockFilePath = logFolderPath + ".csvlock"
			Integer	i
			while File.Exists(lockFilePath)	
				i+=1
				breakif i>20000
			end
			
			if File.Exists(lockFilePath)
				File.Delete(lockFilePath)
			end
			File.Create(lockFilePath)
			.fHasCsvLock = true
		
		end

	script LogCsv
	
			
										
				// Patched by Pat201711090										
				// Patched by Pat201604220
				function LogCsv(String ndcName, \ 
								String filePath, \
								Assoc csvData)
				
					.ClearLogger()	
					
					// *** KM 4/22/16 csv logs need mutex -- pass in last parameter as true
					.SetLogger(filePath, ndcName, true)	
				
					// *** note: we have to pass in LEVEL_NONE here so that we will always get the CSV files no matter what the logging level
					Object logger = .GetLogger("datamanager")
					
					if !File.Exists(._fLogPath)
						String headerStr = _ListToString(Assoc.Keys(csvData))
						logger.Log(logger.LEVEL_NONE, headerStr)
					end
					
					// *** format the csv string to be logged
					String csvStr = _ListToString(Assoc.Items(csvData))
					
					// *** write out the csv to the log file	
					logger.Log(logger.LEVEL_NONE, csvStr)	
					.ClearLogger()
							
				
				end
				
				function String _ListToString(List valList)
					Integer i
					for i = 1 to Length(valList)
						if IsUndefined(valList[i])
							valList[i] = ""
						elseif Type(valList[i]) == DateType
							valList[i] = ._DateToString(valList[i])
						end
					end
				
					// *** KM 11-9-17 have to remove the double quotes in here
					String lineStr = Str.ValueToString(valList)
					lineStr = Str.ReplaceAll(lineStr, '"', '')
					valList = Str.StringToValue(lineStr)
				
					lineStr = '"' + Str.Catenate(valList, '","')
					
					// *** end will have the extra comma and quote - remove these
					return lineStr[1:-3]
				
				end
												
			
		
	
	endscript

	script LogDeltas
	
			
				function Void LogDeltas(Object prgCtx, \
										Assoc options, \
										String logPath, \
										Object oldDataObj, \
										Object apiDataObj, \
										Object newDataObj, \
										Integer id)
					
					Integer logLevel = $SyntergyCore.LogPkg.LEVEL_TRACE
					
					String oldPath = IsDefined(options.auditDeltaOldPath) ? options.auditDeltaOldPath : logPath + "oldVals" + File.Separator() + Str.String(id) + ".txt"
					// *** TODO uncomment these lines
					//oldPath += Str.String(id) + ".txt"
					String inputPath = IsDefined(options.auditDeltaInputPath) ? options.auditDeltaInputPath : logPath + "inputFromCsv" + File.Separator() + Str.String(id) + ".txt"
					// *** TODO uncomment these lines
					//inputPath += Str.String(id) + ".txt"
					String newPath = IsDefined(options.auditDeltaNewPath) ? options.auditDeltaNewPath : logPath + "newVals" + File.Separator() + Str.String(id) + ".txt"
					// *** TODO uncomment these lines
					//newPath += Str.String(id) + ".txt"
					Object log = .GetLogger()
				
					// *** log the old value Assoc (if defined)	
					if IsDefined(oldDataObj)
						.ClearLogger()	
						.SetLogger(oldPath, ._fNdcDeltaLogName)	
						_LogAssoc(oldDataObj.GetCsvData(), log, logLevel)
					end
				
					// *** log the input csv vals
					.ClearLogger()
					.SetLogger(inputPath, ._fNdcDeltaLogName)	
					_LogAssoc(apiDataObj.GetCsvData(), log, logLevel, 0, true, true)
				
					// *** log the result csv vals	
					.ClearLogger()
					.SetLogger(newPath, ._fNdcDeltaLogName)	
					_LogAssoc(newDataObj.GetCsvData(), log, logLevel, 0)
					.ClearLogger()
					
				end				
				
				
				function void _LogAssoc(Assoc a, \
										Object log, \
										Integer	logLevel, \
										Integer indent = 0, \
										Boolean capitalizeKeys = true, \
										Boolean convertToStr = false)
					Dynamic key
						
					for key in Assoc.Keys(a)
						// *** ignore the # keys
						if key[1] != "#"
							if capitalizeKeys
								key = Str.Upper(key)
							end
							
							Dynamic val = a.(key)
						
							if Type(val) == Assoc.AssocType
								log.Log(logLevel, Str.Set(indent, Str.Tab()) + key + ": ")
								_LogAssoc(val, log, logLevel, indent+1, capitalizeKeys)
							else
								if convertToStr
									
									if IsUndefined(val)
										val = ""
									elseif Type(val) == DateType
										val = ._DateToString(val)
									end	
								
								
									log.Log(logLevel, Str.Set(indent, Str.Tab()) + key + ": " + Str.String(val))
								else
									log.Log(logLevel, Str.Set(indent, Str.Tab()) + key + ": " + Str.String(val))					
								end	
							end	
						end			
					end
				
				
				
				end
		
			
		
	
	endscript

	
	public function LogText(String s)
												
			Object logger = .GetLogger("datamanager")
		
			logger.Info(s)	
						
		
		end

	
	public function Object New( )
					
			Frame f = ._NewFrame()
			
			f._fFilesOpen = Assoc.CreateAssoc()
			
			return f
			
		end

	override script Scratch
	
			
					
				function void Scratch(String folderPath = "C:\ImportExport\Wholesale\_audit\Update_Sync_2016-04-22T16-32-41\")
				
					_CountToWhatever(1000)
					_CountToWhatever(10000)
					_CountToWhatever(20000)
				
				
				/*
					$SyntergyCore.Stats.Start( "AuditAPI", "scratch" )	
					
				
					Object logger = .GetLogger()
				
				
					// *** ensure a lock on the csv file
					String lockFilePath =  folderPath + ".csvlock"
					Integer	i
					while File.Exists(lockFilePath)	
						// do nothing but count
						i+=1
						breakif i>10000
					end
					
					if !File.Exists(lockFilePath)
						File.Create(lockFilePath)
					end
					logger.Log(logger.LEVEL_NONE, "Hello")	
					.ClearLogger()
					File.Delete(lockFilePath)
					
					echo("Timing: ", $SyntergyCore.Stats.Stop( "AuditAPI", "scratch" ))
				*/	
				
				end
				
				function void _CountToWhatever(Integer stopAt)
				
					$SyntergyCore.Stats.Start( "AuditAPI", "_CountToWhatever" )	
				
					Integer i
					
					while File.Exists("C:\temp\")
						i+=1
						breakif i>stopAt
					end
				
				
					echo("Timing for ", stopAt, ": ", $SyntergyCore.Stats.Stop( "AuditAPI", "_CountToWhatever" ))
				end
			
		
	
	endscript

	
	public function void SetLogger(String filePath, \
										String appenderNdcName, \
										Boolean needsMutex = false)
																	
			._fLogPath = filePath					
			
			$DataManager.SynCoreInterface.PushNDC(appenderNdcName)
			Object logger = .GetLogger()
			if IsDefined(logger) && IsFeature(logger.fLogAppenders, appenderNdcName)
				
				Object appender = logger.fLogAppenders.( appenderNdcName )
		
				appender.SetFilename( filePath )
		
				appender.fNeedsMutex = needsMutex
			end	
			
		end

	
	public function void UnlockCsvFiles(String logFolderPath)
											
			// *** unlock the csv files
			String lockFilePath = logFolderPath + ".csvlock"
		
			File.Delete(lockFilePath)
		
		
		end

end

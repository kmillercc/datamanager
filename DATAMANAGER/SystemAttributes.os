package DATAMANAGER

public object SystemAttributes inherits DATAMANAGER::CachedLookupObjects

	public		List	_fAttrNames = { 'DOCTYPE', 'FORMNUMB', 'ODAID', 'PID', 'PubDate', 'Title' }
	public		Dynamic	_fCache
	public		Boolean	_fDoneSetup = FALSE
	override	Boolean	fEnabled = TRUE


	
	public function List GetAllAttrNames(Object prgCtx)
						
							Assoc	rtn = .InitErrObj("GetAllAttrNames")
						
							if !._fDoneSetup
								// *** run setup (1 time only)
								Assoc status = ._Setup(prgCtx)
								if !status.ok
									.SetErrObj(rtn, status)
									return {}
								end	
							end		
						
							return ._fAttrNames
							
						end

	
	public function List GetAllAttrNamesUpper(Object prgCtx)
						
							Assoc	rtn = .InitErrObj("GetAllAttrNamesUpper")
						
							if !._fDoneSetup
								// *** run setup (1 time only)
								Assoc status = ._Setup(prgCtx)
								if !status.ok
									.SetErrObj(rtn, status)
									return {}
								end	
							end		
						
							return Assoc.Keys(._fCache)
							
						end

	
	public function String GetSystemAttrName(Object prgCtx, \
														String fieldName)
						
							Assoc	rtn = .InitErrObj("GetSystemAttrName")
						
							// *** field name
							fieldName = Str.Upper(fieldName)
							fieldName = fieldName[1] == "$" ? fieldName[2:] : fieldName
						
							if !._fDoneSetup
								// *** run setup (1 time only)
								Assoc status = ._Setup(prgCtx)
								if !status.ok
									.SetErrObj(rtn, status)
									return undefined
								end	
							end		
						
							String name = IsFeature(._fCache, fieldName) ? ._fCache.(fieldName).ATTRNAME : undefined
						
							return name
							
						end

	
	public function Dynamic GetSystemAttrRec(Object prgCtx, \
														String fieldName)
						
							Assoc	rtn = .InitErrObj("GetSystemAttrRec")
						
							// *** field name
							fieldName = Str.Upper(fieldName)
							fieldName = fieldName[1] == "$" ? fieldName[2:] : fieldName
						
							if !._fDoneSetup
								// *** run setup (1 time only)
								Assoc status = ._Setup(prgCtx)
								if !status.ok
									.SetErrObj(rtn, status)
									return undefined
								end	
							end		
						
							Dynamic rec = IsFeature(._fCache, fieldName) ? ._fCache.(fieldName) : undefined
						
							return rec
							
						end

	
	public function Boolean IsSystemAttr(String fieldName, \
														Object prgCtx = undefined)
						
							Assoc	rtn = .InitErrObj("IsSystemAttr")
						
							// *** field name
							fieldName = Str.Upper(fieldName)
							fieldName = fieldName[1] == "$" ? fieldName[2:] : fieldName
						
							if !._fDoneSetup
								// *** run setup (1 time only)
								Assoc status = ._Setup(prgCtx)
								if !status.ok
									.SetErrObj(rtn, status)
									return false
								end		
							end
							
							return IsFeature(._fCache, fieldName)	
						end

	
	private function Assoc _Setup(Object prgCtx = .GetAdminCtx().prgCtx)
						
							Assoc 	rtn = .InitErrObj("_Setup")
							Assoc 	status	
						
							if !._fDoneSetup	
								._fCache = Assoc.CreateAssoc()
								._fAttrNames = {}
								
								// *** set the flag
								._fDoneSetup = true			
								
								if IsUnDefined(prgCtx)
									status = .GetAdminCtx()
									if !status.ok
										.SetErrObj(rtn, "Could not get prgCtx")
										return rtn
									else
										prgCtx = status.prgCtx
									end
								end			
									
								Object		dapiCtx = prgCtx.DSession()
								status = $LLIApi.CatPkg.ListAttributes( dapiCtx, "<System>" ) // Do not XLate "<System>"
								if !status.OK
									.SetErrObj(rtn, status)
									return rtn
								else
									
									// *** loop through the results and build the cache
									Record rec
									for rec in status.Attributes
										String attrName = Str.Upper(rec.ATTRNAME)
										String dispName = Str.Upper(rec.DISPNAME)
										
										// *** add to cached list
										._fAttrNames = {@._fAttrNames, rec.ATTRNAME}
										
										// *** add to cache
										._fCache.(attrName) = Assoc.FromRecord(rec)
										
										if Str.Cmp(dispName, attrName) <> 0
											._fCache.(dispName) = Assoc.FromRecord(rec)	
										end
										
									end
									
									
								end	
							
							end
							
							return rtn
						end

	
	override function Void __Init()
						
							if .fEnabled
								Object tempObj = OS.NewTemp(this)
								
								$Datamanager.(.OSNAME) = tempObj		
								
								tempObj._fCache = Assoc.CreateAssoc()		
								
							end
						
						end

end

package DATAMANAGER

public object RDCLASSIFICATION inherits DATAMANAGER::Classifications

	override	String	fApiKey2 = 'rmClassId'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDCLASSIFICATION'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
							
							Assoc status = ._CheckNodeReference(prgCtx, val)
							
							if !status.ok
								
								return .SetErrObj(.InitErrObj("ValidateSetVal"), status)
								
							elseif status.result.pSubtype != $TypeRmClassification
								
								return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format(Str.Format("'%1' does not point to a valid RM Classification in this system", val)))
						
							else
						
								val = status.result.pId
						
							end	
						
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

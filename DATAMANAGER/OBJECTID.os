package DATAMANAGER

public object OBJECTID inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'nodeId'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$OBJECTID'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc rtn
							
							rtn.result = Type(val) == StringType ? Str.StringToInteger(val) : val
							rtn.ok = true
							return rtn
												
						end

end

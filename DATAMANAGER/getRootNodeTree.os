package DATAMANAGER

public object getRootNodeTree inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'showFiles', 5, 'ShowFiles', TRUE, TRUE }, { 'returnDirect', 5, 'ReturnDirect', TRUE, FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
			Object		synCore = $DataManager.SynCoreInterface
			Assoc		rtn = synCore.InitErrObj( "getRootNodeTree.DoExec", this )
		
			/**
			 * R may contain: 
			 * 	dataid 		=> Integer 	=> the dataid we are trying to retrieve
		 	 * 	path 		=> String 	=> (optional) the path of the node
		 	 *  showFiles 		=> Boolean	=> showFiles
		  	 *  doRecursive 	=> Boolean	=> recurse into node and get all sub nodes
		 	 *  returnDirect		=> Boolean	=> return the data directly with no standard OK assoc (i.e. for lazy loading)
		     **/
		
			Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)	
		
			// *** hand off to the API
			Assoc status = $DataManager.UiSupportAPI.GetRootNodeTree(.fPrgCtx, limit, r.showFiles)
			if status.ok
				if r.returnDirect
					.fContent = status.result.children
				else
					.fContent = synCore.SuccessResult(status.result)	
				end	
			else
				// for debugging .
				synCore.SetErrObj( rtn, status )
			
				.fError = rtn
			end		
			
		
			return undefined	
		End

end

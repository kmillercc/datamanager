package DATAMANAGER

public object getExportPageConfig inherits DATAMANAGER::ExportRequestHandlers

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'pathModel', -18, 'pathModel', TRUE, Undefined }, { 'profileModel', -18, 'profileModel', FALSE }, { 'objectTypeModel', -18, 'objectTypeModel', TRUE, Undefined }, { 'formVals', -18, 'formVals', TRUE, Undefined }, { 'profileFilter', -18, 'profileFilter', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "getCategories.DoExec", this )
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
						 	 *  pathModel				=> Assoc	=> pathModel 
						 	 *  profileModel 			=> Assoc   =>  profileModel
						 	 *  objectTypeModel			=> Assoc	=> objectTypeModel 
						 	 *  formVals 				=> Assoc   =>  formVals 	 
						 	 *  profileFilter 			=> List    =>  params for filtering the profiles 	 
						     **/	     	
							
							Assoc result, status
								
							// *** get object types
							result.objectTypes = $DataManager.ObjectTypes.GetAllObjectTypes(prgCtx).listOfAssoces
							
							// *** get export paths
							// *** KM 2-23-16 Make sure we pass in false so it does not check permissions for this
							status = $DataManager.ExportPathsAPI.GetAllItems(prgCtx, r.pathModel, false)
							if status.ok
								result.exportPaths = status.result
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
								return undefined
							end		
							
							// *** get profiles
							status = $DataManager.ExportProfilesAPI.GetFilteredItems(prgCtx, r.profileModel, r.profileFilter, false)
							if status.ok
								result.profiles = status.result
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
								return undefined
							end			
							
							// *** get column html
							result.columnHtml = $DataManager.CsvColumnPicker.GetCsvColumnHtml(prgCtx, r.formVals)
						
							// *** package the result into a successful return assoc
							.fContent = synCore.SuccessResult(result)
							
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{ \
									{ "pathModel", Assoc.AssocType, "pathModel", TRUE, undefined }, \
									{ "profileModel", Assoc.AssocType, "profileModel", FALSE }, \		
									{ "objectTypeModel", Assoc.AssocType, "objectTypeModel", TRUE, undefined }, \
									{ "formVals", Assoc.AssocType, "formVals", TRUE, undefined }, \
									{ "profileFilter", Assoc.AssocType, "profileFilter", FALSE } \										
								}
						end

end

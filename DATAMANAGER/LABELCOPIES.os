package DATAMANAGER

public object LABELCOPIES inherits DATAMANAGER::PhysObjLabels

	override	String	fApiKey4 = 'NumberOfCopies'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$LABELCOPIES'

end

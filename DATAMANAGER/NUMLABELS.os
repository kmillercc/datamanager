package DATAMANAGER

public object NUMLABELS inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'NumberOfLabels'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$NUMLABELS'

end

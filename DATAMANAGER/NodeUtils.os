package DATAMANAGER

public object NodeUtils inherits DATAMANAGER::Utils

	
	public function String EnsurePathSeparator(String path)
			
				// ** make sure path has a trailing separator
				path = Str.Trim(path)
				if path[-1] != ":"
					path += ":"
				end	
				
				return path
				
			end

	
	public function Assoc GetCreateNodeFromPath(Object prgCtx, String path, Integer numRetries = 10, Integer containerType = 0)
													
			
				Assoc 		rtn = .InitErrObj( "GetCreateNodeFromPath" )
				DAPINODE	node
				Assoc		status
				
				
				// *** start from a uniform format  - format the path to remove leading and trailing colons
				path = ._FormatNodePath(path)
			
				// *** try to get the root node
				status = $DataManager.RootNodes.GetRootNode(prgCtx, path)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				else
					node = status.result.node
					Integer 	rootIndex = status.result.rootIndex
					List 		pathParts = Str.Elements(path, ":")
					
					// *** KM 7-25-16 - Added logic here to handle volume
					Boolean		containsDash = "-" in pathParts > 0		// if we have a dash, that means there is a volume in there
					
					if Length(pathParts) > 1
						pathParts = Str.Elements(path, ":")[rootIndex+1:]
						for path in pathParts
							// *** now get the node with that path inside the root node
							if Length(Str.Trim(path)) 
								// *** dash indicates volume
								if path == "-"
									// *** get the volume node
									DAPINODE volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
									if IsError(volNode)	
										return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get volume node for node %1 [dataid %2]", node.pId, node.pName))
									else
										node = volNode			
									end		
									continue			
								end
			
								// *** Added logic to create specific container type 9/29/16
								containerType = IsDefined(containerType) ? containerType : $TypeFolder
					
								// *** Call get create Node
								status = $DataManager.NodeAPI.GetCreateNode(prgCtx, node, containerType, path, numRetries)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else
									node = status.result.node
									
									// *** get the volume automatically, but only if there's no dash to indicate so (with dash, we get volume in the code block above)
									if !containsDash
										Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)	
										if llNode.fAssociatedVolume != 0
											// *** get the actual node
											DAPINODE volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
											if IsNotError(volNode)
												node = volNode			
											elseif !llNode.fAssociatedVolumeOptional	
												.SetErrObj(rtn, Str.Format("Could not get volume node for node %1 [dataid %2]", node.pId, node.pName))
												return rtn
											end							
										end						
									end
								end
							end
						end
					end	
				end			
				
				return .SuccessResult(node)
			end

	
	public function Assoc GetNodeById(Object prgCtx, Integer dataid)
			
				DAPINODE node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, dataid)
				
				if IsNotError(node)
					return .SuccessResult(node)
				else
					return .SetErrObj(.InitErrObj("GetNodeById"), Str.Format("Could not get node from dataid %1", dataid))
				end	
			
			end

	
	public function Assoc GetNodeFromPath(Object prgCtx, \
								String fullPath, \
								String rootName = undefined)
													
				DAPINODE	node, volNode
				Assoc		status
				String		path
			
				// *** start from a uniform format  - format the path to remove leading and trailing colons
				fullPath = ._FormatNodePath(fullPath)
				
				// *** try to get the root node
				status = $DataManager.RootNodes.GetRootNode(prgCtx, fullPath, rootName)
				if !status.ok
					return.SetErrObj(.InitErrObj( "GetNodeFromPath" ), status)
				else
					Integer 	rootIndex = status.result.rootIndex	
					node = status.result.node
					List 		pathParts = Str.Elements(fullPath, ":")
					Boolean		containsDash = "-" in pathParts	> 0	// if we have a dash, that means there is a volume in there
					
					if Length(pathParts)
						pathParts = Str.Elements(fullPath, ":")[rootIndex+1:]
						for path in pathParts
							
							// *** dash indicates volume
							if path == "-"
								// *** get the volume node
								volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
								if IsError(volNode)	
									return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get volume node for node %1 [dataid %2]", node.pId, node.pName))
								else
									node = volNode			
								end				
							
							else
						
								// *** now get the node with that path inside the root node
								node = .DAPIGetNode(prgCtx, node, path)
								if IsError(node)
									return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get node from path '%1'", path))
			
								// *** get the volume automatically, but only if there's no dash to indicate so (with dash, we get volume in the code block above)
								elseif !containsDash
									Object llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)	
									if llNode.fAssociatedVolume != 0
										// *** get the volume node
										volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
										if IsNotError(volNode)	
											node = volNode			
										elseif !llNode.fAssociatedVolumeOptional
											return .SetErrObj(.InitErrObj( "GetNodeFromPath" ), Str.Format("Could not get volume node for node %1 [dataid %2]", node.pId, node.pName))
										end
									end				
												
								end
							end	
								
						end
					end
				end
				
				Assoc rtn
				rtn.ok = true
				rtn.result = node
			
				return rtn
				
			end

	
	public function String GetNodePath(Dynamic node, Object prgCtx = undefined)
												
				// *** might be an id that gets passed in
				if Type(node) == IntegerType && IsDefined(prgCtx)
					node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, node)
					if IsError(node)
						return ""
					end
				end
			
				Object	llnode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)
				Assoc pathInfo = llnode.NodePath(node)
				if pathInfo.ok
					return pathInfo.Path
				else
					return ""
				end		
			
			end

	
	public function Assoc GetRecManData(Object prgCtx, DAPINODE node, Dynamic defaultVal = undefined)
												
				String	sqlStr = "SELECT a.nodeID, a.rimsDocDate, a.rimsStatus, a.rimsStatusDate, a.rimsEssential, a.rimsOfficial, a.rimsStorage, "
						sqlStr += "a.rimsAddressee, a.rimsSentTo, a.rimsOriginator, a.rimsEstablishment, a.rimsAccession, a.rimsRSI, "
						sqlStr += "a.rimsCyclePeriod, a.rimsNextReviewDate, a.rimsLastReviewDate, a.rimsReceivedDate, "
						sqlStr += "a.rimsSubject, b.CID, b.Status FROM rimsNodeClassification a, LLClassify b WHERE "
						sqlStr += "b.ID=a.nodeID "
						sqlStr += "AND a.nodeID=:A1 "
						sqlStr += "AND a.ClassID=b.CID"	
			
				RecArray results = .ExecSql(prgCtx, sqlStr, {node.pId})
				if IsError(results)
					return .SetErrObj(.InitErrObj("GetRecManData"), results)
				elseif Length(results)
					return .SuccessResult(Assoc.FromRecord(results[1]))			
				else
					return .SuccessResult(defaultVal)
				end	
				
			end

	
	public function Boolean IsFolderNode(Dynamic node, Object llNode = undefined)
														
				// *** seems like the best way to determine if a node needs to have a folder created for it
				// *** is to check and see if it has any children or if it has an associated volume (in which case the volume may have chilren)
				// *** or maybe its a container
														
				// *** KM 12/6/11  eliminate non-container phys object subtype 411, and documents 144
													
				// *** allow passing of DAPINODE or WebNodes View Row
				Boolean hasChildren
				Integer subtype
														
				if Type(node) == DAPI.DapiNodeType
					hasChildren = node.pSubtype != $TypeCollection ? node.pChildcount > 0 : true
					subtype = node.pSubtype
				elseif Type(node) == RecArray.RecordType && IsFeature(node, 'Childcount')  && IsFeature(node, 'Subtype')
					if node.Subtype == $TypeCollection
						hasChildren = true
					else	
						hasChildren = node.Childcount > 0
					end	
					subtype = node.Subtype
				end
														
				if IsUndefined(llNode)
					llNode = $LLIAPI.LLNodeSubsystem.GetItem(subType)	
				end
														
														
				Boolean isContainer = (hasChildren|| llNode.fAssociatedVolume <> 0 || llNode.fContainer) && (subtype in {411, 144} == 0)
														
				return isContainer
																						
			end

	
	public function Boolean IsRecManagedSubtype(Object prgCtx, Integer subtype)
													
				if IsUndefined($DataManager.fCachedRMSubtypes)  
				       Assoc status = $RecMan.RIMSUtils.GetManagedSubTypes(prgCtx)
				       if status.ok
			       			$DataManager.fCachedRMSubtypes = status.managedSubTypes
				       end
				end
													
				return subtype in $DataManager.fCachedRMSubtypes > 0
			end

	
	public function Assoc NodeDescendantCount(Object prgCtx, \
								Integer dataid, \
								String path, \
								Integer limit, \
								Boolean includeRoot)
													
				Assoc 		rtn = .InitErrObj( "NodeDescendantCount" )
				DAPINODE	node
				Integer 	totalCount
													
				Assoc status = IsDefined(dataId) ? .GetNodeById(prgCtx, dataid) : .GetNodeFromPath(prgCtx, path) 
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				else
					node = status.result
					dataid = node.pId
				end
				
				if node.pSubtype == $TypeCollection
					Object childGetter = $DataManager.NodeChildGetter.New(node.pSubType)
					status = childGetter.GetSubNodes(prgCtx, node)	
					if !status.ok
						return .SetErrObj(rtn, status)
					else
						totalCount = Length(status.result)
					end
													
				else 
													
					// *** query DTREE ANCESTORS to get total count
					RecArray 	result = .ExecSql(prgCtx, "select count(DataID) TotalCount from DTreeAncestors where AncestorID=:A1", {dataid})	
															 
					if IsError(result)
						.SetErrObj(rtn, result)
						return rtn
					elseif Length(result)==0
						.SetErrObj(rtn, Str.Format("Could not get total count from DTreeAncestors table for DataID %1", dataid))		
						return rtn			
					else
						totalCount = result[1].TotalCount
					end
				end
				
				if includeRoot
					totalCount += 1
				end
				return .SuccessResult(totalCount)	
			end

	
	public function String NodeIDToPath(Object prgCtx, Integer dataID)
													
				return ._NodeIdToPath(prgCtx, dataId)
																					
			end

	
	public function Assoc NodeListRecursive( Object prgCtx, \
								DAPINODE node, \
								String nodePath, \
								Integer limit, \
								Object callbackObj = undefined, \
								Assoc result = Assoc.CreateAssoc(), \
								Boolean returnNodes = false, \
								Boolean isRoot = true)
													
				Assoc 		rtn = .InitErrObj("NodeListRecursive")
				Assoc		status
				DAPINODE	childNode
				String		childPath
													
													
				// *** default the result
				if Length(result) == 0
					result = Assoc.CreateAssoc()
					result.count=0
					result.nodes={}
				end
													 
				if isRoot
					$SyntergyCore.Timer.Start( "NodeListRecursive" )
					if !$DataManager.NodeUtils.IsFolderNode(node)	
						.SetErrObj(rtn, Str.Format("NodeListRecursive called on a non-folder: %1.", node.pId))
						return rtn		
					end	
				end
															
				// *** get children
				Object	childGetter = $DataManager.NodeChildGetter.New(node.pSubType)
														
				status = childGetter.GetSubNodes(prgCtx, node)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn		
				end		
													
				for childNode in status.result
															
					childPath = nodePath + ":" + childNode.pName
					result.count+=1
															
					if returnNodes
						result.nodes = {@result.nodes, childNode}							
					end			
															
					if result.count>limit
						return .SuccessResult(result)
					end		
															
					if IsDefined(callbackObj) && IsFeature(callbackObj, "Callback")
						status = callbackObj.Callback(childNode.pId, childPath, childNode, false)
						if !status.ok
							.SetErrObj(rtn, status)
							return rtn
						end
					end						
															
					if $DataManager.NodeUtils.IsFolderNode(childNode)
						status = NodeListRecursive( prgCtx, childNode, childPath, limit, callbackObj, result, returnNodes, false )
						if !status.ok
							.SetErrObj(rtn, status)
							return rtn
																	
						end						
					end
															
				end
													
				if IsRoot
					echo("NodeListRecursive Total Count = ", result.count, " files and folders.")
					Real elapsed = $SyntergyCore.Timer.Stop( "NodeListRecursive" )
					echo("NodeListRecursive ", elapsed, " seconds")
															
				end
													
				return .SuccessResult(result)
													
			end

end

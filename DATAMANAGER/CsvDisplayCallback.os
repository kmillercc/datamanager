package DATAMANAGER

public object CsvDisplayCallback inherits DATAMANAGER::InstanceObjects

	public		Dynamic	_fFindReplaceVals
	public		List	_fHiddenCols
	public		Dynamic	_fPushDownVals
	public		Dynamic	_fStatusColVals
	override	Boolean	fEnabled = TRUE


	
	public function Assoc CsvLineCallback(Integer lineNo, \
																		Assoc metadata)
											
												// *** adjust metadata based on source settings (hidden cols, find/replace, pushDown)
												String	key	
												
												// *** do find and replace
												for key in Assoc.Keys(._fFindReplaceVals)
													if IsFeature(metadata, key)
														Assoc a = ._fFindReplaceVals.(key)
														String	find = a.find
														String  replace = a.replace
														Dynamic val = metadata.(key)
														val = Type(val) != StringType ? Str.String(val) : val
														
														val = Str.ReplaceAll(val, find, replace)
														metadata.(key) = val
													end
													
												end
												
												// *** put line number in
												metadata.("Line") = lineNo
												
												// *** set status col vals
												if IsDefined(._fStatusColVals) && IsFeature(._fStatusColVals, Str.String(lineNo))
													metadata.("Status") = ._fStatusColVals.(Str.String(lineNo))
												else
													metadata.("Status") = Str.Format('<div style="color:%1">__________________________</div>', lineNo%2==0 ? "#FFFFFF" : "#E2E4FF")		
												end
												
												// *** push down vals
												for key in Assoc.Keys(._fPushDownVals)
													if IsFeature(metadata, key)
														String newVal = ._fPushDownVals.(key)
														metadata.(key) = newVal
													end
													
												end	
											
												Assoc rtn
												rtn.ok = true
												rtn.result = metadata
												
												return rtn
											end

	
	public function Frame New( Assoc nodeObjects, \
																Assoc tableVals)
											
											
												Frame f = ._NewFrame()
												
												tableVals = IsDefined(tableVals) ? tableVals : Assoc.CreateAssoc()
												f._fHiddenCols = IsFeature(tableVals, "hiddenCols") ? tableVals.hiddenCols : {}
												f._fPushDownVals = IsFeature(tableVals, "pushDownVals") ? tableVals.pushDownVals : Assoc.CreateAssoc()
												f._fFindReplaceVals = IsFeature(tableVals, "findReplaceVals") ? tableVals.findReplaceVals : Assoc.CreateAssoc()	
												f._fStatusColVals = IsFeature(tableVals, "statusColVals") ? tableVals.statusColVals : Assoc.CreateAssoc()	
												
														
												return f
												
											end

end

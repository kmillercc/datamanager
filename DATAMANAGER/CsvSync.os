package DATAMANAGER

public object CsvSync inherits DATAMANAGER::CORE::CsvIterator

	override	Boolean	fEnabled = TRUE
	override	String	fSubKey = 'Csv_Sync'


	
	override function Assoc _SubclassCallback(Integer id, \
														Dynamic data, \
														Integer chunkIndex = 1)								
												
						
							// *** create a new node object to send back to the ui in an array (nodeAray)
							Assoc 	nodeObj = ._NewNodeObj(id)	
							
							// *** put into node array
							._AddToCollection(nodeObj, chunkIndex, data)
							
							Assoc rtn
							rtn.ok = true	
							return rtn
							
						end

end

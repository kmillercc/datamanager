package DATAMANAGER

public object CsvColumnPicker inherits DATAMANAGER::CachedLookupObjects

	public		Dynamic	_fCachedDefaultHtml
	override	Boolean	fEnabled = TRUE


	script GetCsvColumnHtml
	
																				
				function String GetCsvColumnHtml(Object prgCtx, Assoc formVals)
				
					String html
				
				
					if IsUndefined(formVals) && IsDefined(._fCachedDefaultHtml)
					
						html = ._fCachedDefaultHtml
					
					else
								
				
						html = '<table width="100%">'
					
						// *** the basic html template
						String template = \
								'<tr>'+\
									'<td>' +\
									 '<label class="checkbox" for="%1" style="margin:2px 5px 2px 2px">%2' +\
					            	 	'<input type="checkbox" name="%1" id="%1" value="true" %3 class="%6" data="%1">' +\
					        	     '</label>' +\
									'</td>' +\
									'<td>' +\
										'<div id="%1Div" class="columnGroups">' +\ 
					   						'<h6 class="colHeading">%4</h6>' +\
								           '<div id="%1SubDiv">' +\
								            '%5' +\
					          			  '</div>' +\
					    		        '</div>' +\
						            '</td>' +\
					            '</tr>'
						            
					            
					  	// *** the list of different types
						List	groups = {\
											{"wantNodeId", "Node ID Columns", "Node ID Columns"}, \
											{"wantBasicData", "Basic Node Data", "Basic Node Columns"}, \
											{"wantCategoryData", "All Category Data", "Select Individual Categories"}, \
											{"wantVersionData", "Version Data", "Version Columns"}, \
											{"wantSystemAtts", "Extended Node Attributes", "Extended Node Attributes"}, \
											{"wantRMData", "RecMan Data", "RM Columns"}, \
											{"wantClassifications", "Classifications", "Classification Columns"}, \																								
											{"wantOwner", "Owner Data", "Owner Columns"}, \	
											{"wantPerms", "Permissions Data", "Perm Columns"}, \	
											{"wantPhysObj", "PO Specific Tab Data", "PO Columns"}, \	
											{"wantPhysObjCirc", "PO Circ Data", "PO Circ Columns"}, \	
											{"wantPhysObjOther", "PO Other Data", "PO Other Columns"}, \							
											{"wantEmails", "Email Data", "Email Columns"}, \																															
											{"wantOther", "Other", "Other Columns"}, \
											{"wantContract", "Contract Mgmt Data", "Contract Management Columns"} \								
										 }	
					  
						List group
						
						for group in groups
						
							String groupName = group[1]
							
							if _ShowGroup(prgCtx, groupName)
							
								List defaultEnabledGroups = {"wantNodeId", "wantBasicData", "wantCategoryData"}	
						
								Boolean groupEnabled
								
								if IsDefined(formVals)
									groupEnabled = formVals.(groupName)
								else
									groupEnabled = (groupName in defaultEnabledGroups > 0)
								end
									
								String  checked = groupEnabled ? "checked" : ""
								
								String groupClass = groupName=="wantCategoryData" ? "group_Select_Checkbox group_" + groupName : "group_Checkbox group_" + groupName
							
								html += Str.Format(template, groupName, group[2], checked, group[3], _GetSelections(prgCtx, groupName, groupEnabled, formVals), groupClass)
							
							end
									
						end
					
						html += "</table>"
					
						if IsUndefined(formVals)
							._fCachedDefaultHtml = html
						end
							
					end
					
					return html
				
				end
				
				function String _GetSelections(Object prgCtx, String group, Boolean groupEnabled, Assoc formVals)
				
					String template = '<label class="checkbox" id="col_%1_label" for="col_%1" style="font-size: 9px; margin-bottom: 1px;">' +\
										'<input id="col_%1" class="col_%3 col_Checkbox" data="%3" type="checkbox" name="col_%1" value="true" %2>' +\
										'%1' +\
										'</label>'
										
					String catTemplate = Str.Format('<select multiple name="catIds" id="catIds" class="span12 select_Group" data="%1" size="7">', group)
				
				
					String col, checked, s
				
					if group == "wantCategoryData"
				
						String selected = groupEnabled ? "selected" : ""
						Assoc status = $DataManager.CategoryAPI.GetAvailableCategories(prgCtx)
						
						if status.ok
							s = catTemplate
							Assoc cat		
							for cat in status.result
								s += Str.Format('<option value="%1" %2 class="option_%3" data="%3">%4</option>', cat.ID, selected, group, cat.NAME)
							end	
							s += '</select>'
						end
					
					else	
						for col in ._GetColumns(group)
							checked = ._GetChecked(groupEnabled, formVals, "col_" + col)
							s += Str.Format(template, col, checked, group)	
						end
					end	
				
					return s
				end
				
				function Boolean _ShowGroup(Object prgCtx, String group)
				
				
					switch group
					
						case "wantNodeId"
							return true
						end
					
						case "wantBasicData"
							return true
						end
						
						case "wantVersionData"
							return true
						end
						
						case "wantCategoryData"
							return true		
						end
						
						case "wantSystemAtts"
							return Length($DataManager.SystemAttributes.GetAllAttrNames(prgCtx)) > 0
						end
						
						case "wantRMData"
							return IsDefined($RecMan)
						end
						
						case "wantClassifications"
							return IsDefined($Classification)
						end
						
						case "wantOwner"
							return true
						end
						
						case "wantPerms"
							return true
						end
						
						case "wantPhysObj"
							return IsDefined($PhysicalObjects)
						end
						
						case "wantPhysObjCirc"
							return IsDefined($PhysicalObjects)
						end
					
						case "wantPhysObjOther"
							return IsDefined($PhysicalObjects)
						end	
					
						case "wantEmails"
							return IsDefined($OTEMAIL)
						end
						
						case "wantOther"
							return true
						end
						
						case "wantContract"
							return IsDefined($ContractManagement)
						end
					
					
					end
					
				
					return false
				
				
				end
																									
		
			
		
	
	endscript

	
	private function String _GetChecked(Boolean groupEnabled, Assoc formVals, String field)
											
												Boolean hasCols = false
												
												if IsDefined(formVals)
													hasCols = formVals.hasCols || IsDefined(Str.Locate(Str.String(formVals), "col_$"))
												end	
											
												if hasCols
													if IsDefined(formVals) && IsDefined(formVals.(field)) && formVals.(field)
														return "checked"
													end	
												elseif groupEnabled
													return "checked"
												end	
												
												return ""
											
											end

	
	private function List _GetColumns(String grouping)
											
											
												switch grouping
												
													case "wantNodeId"
														return {'$OBJECTNAME','$TARGETPATH','$OBJECTID','$OBJECTTYPE'}
													end
												
													case "wantBasicData"
														return ._fBasicColNames
													end
													
													case "wantCategoryData"
													
													end
													
													case "wantVersionData"
														return {@._fVersionColNames, @._fRenditionColNames}
													end		
													
													case "wantSystemAtts"
													
													end
													
													case "wantRMData"
														return ._fRecManColNames
													end
													
													case "wantClassifications"
														return ._fClassificationColNames
													end
													
													case "wantOwner"
														return ._fOwnerColNames
													end
													
													case "wantPerms"
														return ._fPermColNames
													end
													
													case "wantPhysObj"
														return ._fExportedPOSpecTabColNames
													end
													
													case "wantPhysObjCirc"
														return ._fExportedPOCircColNames
													end
												
													case "wantPhysObjOther"
														return ._fExportedPOOtherColNames		
													end
												
													case "wantEmails"
														return ._fEmailColNames
													end
													
													case "wantOther"
														return {"$NEWNAME", "$NEWPARENTID"}
													end
													
													case "wantContract"
														return ._fContractColNames
													end
												
												
												end
												
											
												return {}
											
											
											end

end

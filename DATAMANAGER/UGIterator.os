package DATAMANAGER

public object UGIterator inherits DATAMANAGER::ExportIterators

	public		Dynamic	_fCache
	public		List	_fDesiredTypes
	public		String	_fGroupCsvPath
	public		Boolean	_fIncludeGroups = FALSE
	public		Boolean	_fIncludeMembership = FALSE
	public		Boolean	_fIncludeUsers = FALSE
	public		String	_fUserCsvPath
	override	Boolean	fEnabled = TRUE
	override	String	fSubKey = 'users'


	
	override function Assoc Callback(Integer id, \
																	String path, \
																	Dynamic ugRec, \
																	Boolean isRoot = false, \
																	String parentPath = "")
												Assoc	rtn
												rtn.ok = true	
												Assoc	status
												Object 	prgCtx = ._fPrgCtx
												
												if IsUndefined(._fCache.(ugRec.ID))
													// ** exit out if the user does not want this type
													if ugRec.TYPE == UAPI.USER && !._fIncludeUsers
														return rtn	
													elseif ugRec.TYPE == UAPI.GROUP && !._fIncludeGroups
														return rtn				
													end	
												
													// *** increment the counter
													.fIterateTotal+=1
													
													// ***  track the min and max ids
													if id < .fMinId || .fMinId==0
														.fMinId = id
													end
													if id > .fMaxId
														.fMaxId = id
													end			
													
													String	csvPath = ugRec.TYPE == UAPI.USER ? ._fUserCsvPath : ._fGroupCsvPath
													
													// *** get metadata gatherer
													._fMetadataGatherer = ._GetMetadataGatherer(csvPath)
													
													// *** get the metadata	
													status = ._fMetadataGatherer.WriteMetadata(prgCtx, ugRec, ._fIncludeMembership)
													if !status.ok
														return .SetErrObj(.InitErrObj("Callback"), status)
													end	
													
													// *** update the job comleted count
													$DataManager.JobInterface.IncrementCompletedCount(prgCtx, ._fJob)
													
													// *** cache this id so we don't export again
													._fCache.(ugRec.ID) = true
												end		
											
												return rtn
											
											end

	
	override function void InitSubclass(Assoc options, Assoc sourceSettings)
											
											
												// *** needed for metadata gathering
												._fGatherers = Assoc.CreateAssoc()
												._fCache = Assoc.CreateAssoc()
											
												if options.includeGroups
													if options.includeMembership
														._fIncludeMembership = true
														._fIncludeUsers = options.includeUsers
														._fIncludeGroups = true
														._fOptions.includeRoot = true
														._fDesiredTypes = ._fIncludeUsers ? {UAPI.GROUP,UAPI.USER} : {UAPI.GROUP}
													else
														._fIncludeGroups = true		
														._fDesiredTypes = {@._fDesiredTypes, UAPI.GROUP}	
													end	
												end		
												if options.includeUsers
													._fIncludeUsers = true
													._fDesiredTypes = {@._fDesiredTypes, UAPI.USER}
												end	
												
												// *** get our csvPaths
												._fUserCsvPath = ._fRootTargetPath + "users.json"
												._fGroupCsvPath = ._fRootTargetPath + "groups.json"
												
												
												List paths
												if ._fIncludeGroups
													 paths = {._fGroupCsvPath}
												end	
												if ._fIncludeUsers
													paths = {@paths, ._fUserCsvPath}
												end
												
												// *** go ahead and set up the metadata gatherers
												String path
												for path in paths
													List colNames = path==._fUserCsvPath ? ._fUserColNames	: ._fGroupColNames
													._GetMetadataGatherer(path, colNames)
												end
											
												
											
											end

	
	override function Assoc Iterate(Object prgCtx, \
																	Dynamic root = undefined, \
																	String path = undefined)				
																
												Assoc 	rtn = .InitErrObj("IterateUsersGroups")
												Assoc	status
												Assoc 	result						
												Assoc 	options = ._fOptions
												Integer limit = ._fLimit
												Boolean	recurse = options.recurse	
												List desiredTypes = ._fDesiredTypes
												
												if Length(desiredTypes)==0
													.SetErrObj(rtn, "You have selected to include neither users nor groups nor group memberships.  You must include at least 1 type of object.")
													return rtn
												end
												
												// *** include the root node?
												if options.includeRoot && ._fRootSourceId > 0
												
													RecArray recs = UAPI.GetByID( prgCtx.USession().fSession, ._fRootSourceId )
													if IsError(recs)
														.SetErrObj(rtn, "Unable to retrieve root group.")
														return rtn
													end			
												
													result.count=1
													this.Callback(result.count, ._fRootSourcePath, recs[1], true)
												end		
												
												if recurse
													// *** get a list of users and groups, passing in callback
													status = $DataManager.UserUtils.UgListRecursive(prgCtx, \
																								._fRootSourceId, \
																								._fRootSourcePath, \
																								true, \
																								desiredTypes, \
																								limit, \
																								this, \
																								result)		
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end	
												else
													status = $DataManager.UserUtils.GetGroupMembers(prgCtx, ._fRootSourceId, true)
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													else
														Record 	ugRec
														for ugRec in status.result
															path = ._fRootSourcePath + ":" + ugRec.NAME
															result.count+=1
															this.Callback(ugRec.ID, path, ugRec, false)		
														end
													end					
												end	
												
												return rtn	
												
											end

	override script _0SetSubkey
	
			
					
							
									
											
													
															
																
																		function void _0SetSubtype()
																			.fSubKey = 'users'
																		end
																								
																
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Object _GetMetadataGatherer(	String csvPath, \
																					List colNames = {})
											
												Object gatherObject = $DataManager.UGMetadataGatherer
												String key = ._HashKey(csvPath)
											
												if IsUndefined(._fGatherers.(key))
													._fGatherers.(key) = gatherObject.New(._fPrgCtx, ._fOptions, csvPath, colNames)
												end
											
												return ._fGatherers.(key)
											
											
											end

end

package DATAMANAGER

public object NodeChildGetter inherits DATAMANAGER::FactoryObjects

	override	Boolean	fEnabled = TRUE
	override	String	fKey = 'NodeChildGetter'


	
	public function Assoc GetSortedContents(Object prgCtx, \
																				Dynamic node, \		//DAPINODE or WebNode Rec
																				String sortBy = "NAME")
												
												Assoc		rtn = .InitErrObj("_GetSubNodes")
												
												Integer 	subtype = Type(node) ==  DAPI.DapiNodeType ? node.pSubType : node.SUBTYPE
												Integer		nodeId = Type(node) ==  DAPI.DapiNodeType ? node.pId : node.DATAID
												Integer		volId = Type(node) ==  DAPI.DapiNodeType ? node.pVolumeId : node.OWNERID
																		
												Object		llNode = $LLIAPI.LLNodeSubsystem.GetItem(subtype)
												RecArray	contents = undefined
												
												// *** get contents of the volume
												if llNode.fAssociatedVolume != 0
													
													// *** KM 2-18-16 Changed this code so that it would get the contents of a volume properly. Also changed so that it will only get the volume
													// *** contents instead of trying to get contents of both the volume and the regular node. 
													
													// *** get the actual node
													DAPINODE volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(nodeId))
													if IsNotError(volNode)
															
														contents = DAPI.ListContents( 	prgCtx.DapiSess(), \
																						-(volNode.pId), \
																						volNode.pId, \
																						"WebNodes", \
																						 "", \
																						$PSee )
																						
														if IsError(contents)
															.SetErrObj(rtn, contents)
															rtn.errMsg = Str.Format("Error retrieving contents of node '%1 [%2]' ", volNode.pId, volNode.pName) + rtn.errMsg
															return rtn
														end	
														
														// *** KM 10-18-16 Also need to get the sub-projects
														RecArray contents2 = DAPI.ListContents( prgCtx.DapiSess(), \
																								volId, \
																								nodeId, \
																								"WebNodes", \
																						 		"", \
																								$PSee )
																								
														if IsError(contents2)
															.SetErrObj(rtn, contents2)
															rtn.errMsg = Str.Format("Error retrieving contents of node '%1 [%2]' ", volNode.pId, volNode.pName) + rtn.errMsg
															return rtn
														else
															contents = $LLIAPI.RecArrayPkg.MergeRecArrays(contents, contents2)
														end											
														
														
													end
												end
												
												if IsUndefined(contents)
													// *** get contents of the node	
													contents = DAPI.ListContents( prgCtx.DapiSess(), \
																						volID, \
																						nodeId, \
																						"WebNodes", \
																						 "", \
																						$PSee )
													if IsError(contents)
														.SetErrObj(rtn, contents)
														rtn.errMsg = Str.Format("Error retrieving contents of node '%1 [%2]' ", node.pId, node.pName) + rtn.errMsg
														return rtn
													end		
													
												end											
															
											
												// *** sort
												contents = RecArray.Sort(contents, sortBy)
												
												return .SuccessResult(contents)
												
											end

	
	public function Assoc GetSubNodes(Object prgCtx, \
																		DAPINODE node)
												
												List		subNodes = {}
												
												// *** KM 11-15-16 make this flexible enough to handle volume or regular node
												if node.pSubtype != $TypeFolder
													// *** check if we have a volume type node
													DAPINODE volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(node.pId))
													if IsNotError(volNode)
														subNodes = DAPI.ListSubNodes(volNode)
													end
												end	
												
												subNodes = List.SetUnion(subNodes, DAPI.ListSubNodes(node))
												
												Assoc rtn
												rtn.ok = true
												rtn.result = subNodes
												
												return rtn
												
											end

	
	public function Object New( Dynamic subtype = undefined)
											
												Frame f = ._NewFrame(subtype)
											
												return f
												
											end

end

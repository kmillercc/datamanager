package DATAMANAGER

public object RDORGANIZATION inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsEstablishment'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDORGANIZATION'
	override	String	fUpdateApiKey3 = 'fEstablishment'

end

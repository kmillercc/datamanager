package DATAMANAGER

public object LoadRequestHandlers inherits DATAMANAGER::JSONEnabled

	public		String	_fJobType = 'BulkLoader.Load'
	override	Boolean	fEnabled = FALSE
	override	String	fFuncPrefix = 'datamanager'

end

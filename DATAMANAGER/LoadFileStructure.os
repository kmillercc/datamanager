package DATAMANAGER

public object LoadFileStructure inherits DATAMANAGER::Load

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'Load File Structure to Here'
	override	String	fQueryString = 'func=datamanager.index&targetId=%1#bl/loadfilestructure'

end

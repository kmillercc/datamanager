package DATAMANAGER

public object AutoDocumentUtils inherits DATAMANAGER::Utils

	
	public function Assoc HandleADN(Object prgCtx, Object apiDataObj, Object llNode, DAPINODE parentNode, List attrGroups, Assoc createInfo)
		
			Assoc 	catDefs = apiDataObj.GetCatDefs()
			String	catKey
			Assoc 	result
			
			// do we have category definitions here that include an ADNID attribute that we need to initialize?		
			for catKey in Assoc.Keys(catDefs)
				Assoc 	rootDef
				Assoc 	child, grandChild
				
				rootDef = catDefs.(catKey)
				
				// look for any adnid type attributes to generate the number
				for child in rootDef.Children
					String 	fieldName = Str.Format("_1_1_%1_1", child.ID) 
					Integer quantity = 1
					Integer seqID = undefined
					Dynamic sheet = undefined		
					
					if child.Type == 10131		// ADNID type attribute
						
						// *** set up the rootDef.ValueTemplate with the values
						._PrepData(rootDef, attrGroups, catKey)
					
						// now actually generate the number
						result = $ADN.Utils.GenerateIDNumber( prgCtx, rootDef, child, fieldName, quantity, seqID, sheet )
						
						if !result.ok
							return .SetErrObj(.InitErrObj("HandleADN"), result)
						else
							attrGroups = ._SetADNID(child, attrGroups, catKey, result.Number)  
						end
	
					elseif IsFeature(child, 'Children')
						for grandChild in child.Children
							if grandChild.Type == 10131		// ADNID type attribute
							
								// *** set up the rootDef.ValueTemplate with the values
								._PrepData(rootDef, attrGroups, catKey)						
							
								// set field name
								fieldName = Str.Format("_1_1_%1_%2_1", child.ID, grandchild.ID)
								
								// now actually generate the number
								result = $ADN.Utils.GenerateIDNumber( prgCtx, rootDef, grandChild, fieldName, quantity, seqID, sheet )
								if !result.ok
									return .SetErrObj(.InitErrObj("HandleADN"), result)
								else
									attrGroups = ._SetADNID(child, attrGroups, catKey, result.Number)  
								end							
							end											
						end
					end
				end	
			end
			
			return Assoc{'ok': true, 'result': attrGroups}					
	
		end

	
	private function Void _PrepData(Assoc rootDef, List attrGroups, String catKey)
		
			Assoc 	valueTree = rootDef.ValueTemplate
			Frame 	attrGroup
	
			if IsDefined(valueTree.Values) && Length(valueTree.Values) > 0
				Assoc 	attrRow = valueTree.Values[1]
				Assoc	attr 		
				for attr in attrRow
					String attrKey = Str.Format("%1.%2", catKey, attr.ID)
					for attrGroup in attrGroups
						if attrGroup.fKey == catKey
							Frame attrFrame
							for attrFrame in attrGroup.fValues
								if attrFrame.fKey == attrKey
									attr.Values = attrFrame.fValues
									break
								end 
							end
							break
						end	
	 				end				
				end
			end
		end

	
	private function List _SetADNID( Assoc adnFieldDef, List attrGroups, String catKey, String value)
		
			String attrKey = Str.Format("%1.%2", catKey, adnFieldDef.ID)
			Frame 	attrGroup
		
			for attrGroup in attrGroups
				if attrGroup.fKey == catKey
					Frame attrFrame
					Integer i
					for i = 1 to Length(attrGroup.fValues)
						attrFrame = attrGroup.fValues[i]
						if attrFrame.fKey == attrKey
							attrGroup.fValues[i].fValues = {value}
							break
						end 
					end
					break
				end	
			end	
	
			return attrGroups
		
		end

end

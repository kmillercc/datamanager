package DATAMANAGER

public object getWorkerQueue inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'forDataTable', 5, 'For Data Table', TRUE, FALSE }, { 'model', -18, 'For Data Table', TRUE, Undefined } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
						 	 *  forDataTable	=> Boolean	=> for the data table?   
						 	 *  model 			=> Assoc   => the model we use
						     **/		
						     
						    Assoc filter
						    filter.handlerID = "DataManagerJob"
						     
							// *** hand off to the API
							// *** KM 7/13/16 disable the time zone offset
							// *** IsFeature(r, 'tzOffset') ? r.tzOffset : 0
							Assoc status = $DataManager.DistribAgentsAPI.GetFilteredItems(prgCtx, r.model, filter, true, 0)	
							if status.ok
									
								if r.forDataTable		
									// Plop in some things here for the data tables
									Assoc a
									a.aaData = status.result
									a.sEcho = 1
									a.iTotalRecords = Length(a.aaData)
									a.iTotalDisplayRecords = Length(a.aaData)
									.fContent = synCore.SuccessResult(a)	
									
								else
									.fContent = synCore.SuccessResult(status.result)	
								end	
								
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\ 
									{ "forDataTable", BooleanType, "For Data Table", TRUE, FALSE }, \
									{ "model", Assoc.AssocType, "For Data Table", TRUE, undefined } \
								}
						end

end

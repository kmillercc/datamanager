package DATAMANAGER

public object OWNERID inherits DATAMANAGER::UserGroup

	override	String	fApiKey1 = 'OWNERID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$OWNERID'

end

package DATAMANAGER

public object VERSION inherits DATAMANAGER::VERSIONNUM

	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERSION'

end

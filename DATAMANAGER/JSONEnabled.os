package DATAMANAGER

public object JSONEnabled inherits DATAMANAGER::#'DataManager LLRequestHandlers'#

	public		Boolean	fAllowTokenSecurity = FALSE
	public		Boolean	fAlreadyJSON = FALSE
	override	String	fBaseProfileName = 'DataManager.RequestHandler'
	public		Boolean	fCheckReferer = FALSE
	public		Dynamic	fContent
	override	Boolean	fEnabled = FALSE
	override	String	fErrorFile = 'error.txt'
	override	String	fHTMLFile = 'plain.txt'
	override	String	fHTMLMimeType = 'text/javascript'
	public		Dynamic	fWarnings


	override script CheckArgsUtil
	
			
					
							
									
											
													
												
														
															// *** KM 7/23/ 10 added handling for real values and dates
															
															//	CheckArgsUtil validates the specified request handler arguments
															//	(r, a.k.a. .fArgs) using the specified prototype (checks,
															//	a.k.a. .fPrototype).
															//
															//	.fPrototype is a List of Lists where each sub-list defines
															//	how to validate a request handler argument.  A sub-list's
															//	elements are:
															//
															//		[ 1 ] = Name (e.g. "NodeId")
															//
															//				NOTE:  All node ID arguments MUST end with "Id" for Q/A
															//					   pourposes.
															//
															//		[ 2 ] = Datatype (e.g. IntegerType)
															//
															//		[ 3 ] = Short description (e.g. "Node ID")
															//
															//		[ 4 ] = Boolean indicting if the argument is optional
															//				(TRUE = optional argument, FALSE = required argument)
															//
															//		[ 5 ] = If an argument is optional, this is its default value.
															//
															//		[ 6 ] = Boolean indicating if the argument, whose name MUST end
															//				with "_list", is expected to be a List.
															//
															//				(e.g. { "userId_list", IntegerType, "User IDs", FALSE, Undefined, TRUE })
															//
															//				If the code that translates CGI paramaters into .fArgs
															//				encounters multiple, identically-named arguments (e.g.
															//				"userId=1234&userId=4321"), .fArgs will have the fields
															//				.fArgs.userId = 4321 and .fArgs.userId_list = { 4321, 1234 }.
															//				However, if only one argument is specified (e.g. "userId=1234"),
															//				.fArgs will only have userId = 1234, and it is up to
															//				.CheckArgsUtil() to add the field .fArgs.userId_list = { 1234 }.
															//
															//		[ 7 ] = (Optional) Integer defining the maximum length of the argument.
															//				Say you set it to 5, but the Length() of the argument was
															//				6, the check would fail, 5 or less would succeed.
															//				Set to Undefined to not perform the check.
															//
															//		[ 8 ] = (Optional) This feature works in conjunction with [ 2 ] (Data Type).
															//				Here you can further refine what your request will accept.
															//				Integer Constants Define what is allowed:
															//				1 = Alpha only - [a-z] and [A-Z]
															//				2 = Numeric only - [0-9]
															//				3 = Alphanumeric Only - [a-z], [A-Z] and [0-9]
															//				4 = Special parameter for sort (whitespace not allowed) - this is needed in many request handlers and
															//					is two expensive to run a Pattern.Compile() on all these request handlers. Optional minus sign followed
															//					by upper or lower case alphabetical characters. You should also use parameter [ 7 ] and set a length as well.
															//				(Whitespace is allowed in any of the constants if the constant has a - (minus) sign preceding it.
															//				 For example, -1 is alpha plus whitespace)
															//				Set to Undefined to not perform the check.
															//
															//		[ 9 ] = (Optional) Defined regular expression using our Pattern built-in.
															//				For example, "-*[A-Za-z]+" would allow an argument to optionally start with a minus sign and
															//				be followed by any upper or lower case letters. This is a good example for a sort argument.
															//				Use this feature carefully and consult the Pattern built-in for documentation.
															//				Set to Undefined to not perform the check.
															//
															function Boolean CheckArgsUtil( \
																Record	r, \		//	RequestHandler::fArgs.
																List	checks )	//	RequestHandler::fPrototype.
																
																Dynamic c
																Integer	col
																Boolean	done
																Dynamic	val
																List	colsToAdd	// For default values.
																List	valsToAdd
																List	valList
																List	newList
																List	findResult
																PatFind	finder
																Record	nr
																  	
																
																Boolean	ok = TRUE
																Dynamic	kNextURL = $WebDsp.kNextURL
																Dynamic	kCheckPost = $WebDsp.kCheckPost
																
																
																// process all entries in the check specifications
																
																for c in checks
																
																	done = false
																	
																	if ( Type( c ) != ListType )
																	
																		if ( ( Type( c ) == StringType ) && ( 0 == Str.Cmp( c, kCheckPost ) ) )
																		
																			// check that the request method is a post
																
																			if ( IsFeature( r, 'LLENVIRON_ASSOC' ) && IsDefined( r.LLENVIRON_ASSOC ) \
																				&& IsFeature( r.LLENVIRON_ASSOC, 'REQUEST_METHOD' ) && IsDefined( r.LLENVIRON_ASSOC.REQUEST_METHOD ) \
																				&& 'POST' != Str.Upper( r.LLENVIRON_ASSOC.REQUEST_METHOD ) )
																	
																				.fError = [WebDsp_ErrMsg.YourRequestContainedInvalidContentPleaseContactYourSystemAdministrator]
																
																				break						
																			end
																
																		end
															
																		// continue with the rest of the checks
																
																		done = TRUE
																	end
															
																	if ( !done )
																	
																		// locate the column index
																
																		col = RecArray.IsColumn( r, c[ 1 ] )
																
																		// If the column was not found, and it was declared a list type, and the
																		// column has name xxx_list, then look for the column xxx.  If found,
																		// turn it into a list and continue processing.
																		
																		if ( col == 0 ) && \
																			( Length( c ) >= 6 ) && \
																			c[ 6 ] && \
																			( c[ 1 ][ -5: ] == "_list" )
																						
																			col = RecArray.IsColumn( r, c[ 1 ][ :-6 ] )
																			
																			if ( col != 0 )
																			
																				newList = {}	// Since all OScript vars are globally scoped, we
																								// *must* initialize newlist on each pass, otherwise
																								// subsequent lists contain values of previous args
																
																				// Found it... apply conversions.
																				
																				val = r[ col ]
																			
																				if ( Type( val ) == ListType )
																				
																					valList = val
																				else
																					valList = { val }
																				end
																				
																				for val in valList
																				
																					val = DoConversion( val, c[ 2 ], c[ 1 ] )
																					newList = { @newList, val }
																				end
																				
																				// Add the _list column to the recarray.
																				
																				colsToAdd = { @colsToAdd, c[ 1 ] }
																	 			valsToAdd = { @valsToAdd, newList }
																	 			
																	 			done = TRUE
																			end
																		end
																		
																		if ( !done )
																		
																			if ( col != 0 )
																			
																				// If the column was found, extract current value
																		
																				val = r[ col ]
																		
																				// should any conversions be applied
																		
																				if ( Length( c ) >= 6 && c[ 6 ] )
																				
																					// If it's a list, apply the conversion to every element of the list
																	
																					newList = {}	// Since all OScript vars are globally scoped, we
																									// *must* initialize newlist on each pass, otherwise
																									// subsequent lists contain values of previous args
																				
																					if ( Type( val ) == ListType )
																					
																						valList = val
																					else
																						valList = { val }
																					end
																					
																					for val in valList
																					
																						val = DoConversion( val, c[ 2 ], c[ 1 ] )
																						newList = { @newList, val }
																					end
																					
																					val = newList
																					
																				elseif ( Length( c ) >= 7 )
																				
																					//
																					// Request handler wants the maximum length checked
																					//
																					if ( Type( c[ 7 ] ) == IntegerType && Length( val ) > c[ 7 ] )
																					
																						.fError = Str.Format( [WEBDSP_ERRMSG.MaximumLengthOfArgument1WasExceeded], c[ 1 ] )
																						ok = FALSE
																					end
																					
																					if ( ok && Length( c ) >= 8 && Type( c[ 8 ] ) == IntegerType )
																					
																						//
																						// If it's supposed to be numeric, but is a String that can
																						// be converted into a Integer then do so
																						//
																						//value = Str.StringToValue( val )
																							
																						//if ( IsDefined( value ) && Type( value ) == IntegerType )
																							
																							//val = value
																						//end
																					
																						ok = CheckType( val, c[ 8 ] )
																					end
																					
																					if ( ok && Length( c ) >= 9 && Type( c[ 9 ] ) == StringType )
																					
																						finder = Pattern.CompileFind( c[ 9 ] )
																						
																						if ( IsNotError( finder ) )
																						
																							findResult = Pattern.Find( val, finder )
																						
																							if !( IsDefined( findResult ) && findResult[ 1 ] == 1 && findResult[ 2 ] == Length( val ) )
																							
																								.fError = [WEBDSP_ERRMSG.PatternOfArgumentWasNotRecognized]
																							end
															  							else
																							.fError = "Pattern String could not be compiled." // Do not Xlate - Friendly developer error :)
																						end
																					end						
																				else
																					val = DoConversion( val, c[ 2 ], c[ 1 ] )
																				end
																				
																				// save any converted value back to record
																		
																				r[ col ] = val
																	 			
																			elseif ( Length( c ) >= 5 )
																			
																				// If a default value is specified, use it instead
																				
																				Dynamic	value = c[ 5 ]
																				
																				
																				//	Is kNextURL specified?
																				if ( Str.CmpI( c[ 1 ], "nextURL" ) == 0 && \
																					 c[ 2 ] == StringType && \
																					 value == kNextURL )
																					
																					value = r.SCRIPT_NAME
																				end
																				
																				colsToAdd = { @colsToAdd, c[ 1 ] }
																	 			valsToAdd = { @valsToAdd, value }
																	 			
																			elseif ( c[ 2 ] == BooleanType )
																			
																				// If it's not present and a boolean parameter and default is not specified, set it to FALSE.
																				// NOTE: This needs to be here because an unchecked checkbox parameter will not appear in the request,
																				//       whether it's required or not.
																				
																				colsToAdd = { @colsToAdd, c[ 1 ] }
																	 			valsToAdd = { @valsToAdd, FALSE }
															
																			else
																				// if the column is required, require it
																	
																				if ( !c[ 4 ] )
																				
																					.fError = Str.Format( [WebDsp_ErrMsg.Argument1IsRequired],\
																										  c[ 1 ] )
																					
																					break
																				end
																			end
																		end
																	end
																end
																
																
																if ( IsUndefined( .fError ) )
																
																	nr = RecArray.CreateRecord(	\
																			{ @RecArray.FieldNames( r ), @colsToAdd }, \
																			{ @RecArray.GetRecord( r ), @valsToAdd } )
																	
																	.fArgs = nr
																end
																
																return IsUndefined( .fError )
															end
															
															
															
															//
															// Local methods follow
															//
															  
															  
															function Dynamic DoConversion( \
																Dynamic		val, \
																Integer		dataType, \
																String		fieldName )
															  
																Boolean		tst = FALSE
																
															
																switch dataType
																
																	case StringType
																	
																		val = Str.Strip( val, Str.Ascii( 13 ) )	// carriage-return
																		tst = ( Type( val ) == StringType )
																	end
																	
																	case BooleanType
																	
																		if ( Str.Upper( val ) == 'FALSE' )
																		
																			val = FALSE
																		else
																			val = TRUE
																		end
															
																		tst = ( Type( val ) == BooleanType )
																	end
															
																	case DateType
																	
																		if ( Type( val ) == StringType )
																		
																			Dynamic testVal = Str.StringToValue( val )
																			
																			if Type(testVal) != DateType
																				val = Date.StringToDate(val, '%m-%d-%Y')
																			else
																				val = testVal	
																			end	
																		end
															
																		tst = ( Type( val ) == DateType ) || IsUndefined( val )
																		
																		if ( tst && IsDefined( val ) )
																		
																			val = val + .fTZOffset
																		end
																	end
																	
																	case RealType
																		val = Str.StringToReal( val )
																		tst = ( Type( val ) == RealType )
																	end
																	
																	default		
																	
																		if ( Type( val ) == StringType )
																		
																			val = Str.StringToValue( val )
																		end
															
																		tst = ( Type( val ) == dataType )
																	end
																end
															
																// check that the converted column is of the correct type
															
																if ( !tst )
																
																	.fError = Str.Format( [WebDsp_ErrMsg.InvalidDatatypeSpecifiedForArgument1], fieldName )
																end
																
																return val
															end
															
															
															//
															// 1 = Alpha only - [a-z] and [A-Z]
															// 2 = Numeric only - [0-9]
															// 3 = Alphanumeric Only - [a-z], [A-Z] and [0-9]
															// 4 = Sort parameter only - optional '-' character then [a-z], [A-Z]
															// (Whitespace is allowed in any of the constants if the constant has a - (minus) sign preceding it.
															//  For example, -1 is alpha plus whitespace)
															//
															function Boolean CheckType( \
																String			val, \
																Integer			check )
															
																List			findResult
																PatFind			finder
																String			strType
																
																Boolean			retVal = TRUE
																Boolean			validConstant = TRUE
																Boolean			whitespaceAllowed = FALSE
																Integer			checkType = 0
															
															
																if ( IsUndefined( $WebDSP.RequestHandlerUtils.fPatternsCompiled ) )
															
																	//
																	// Cached compiled patterns for prototype checking
																	//
																	$PATTERN_ALPHA = Pattern.CompileFind( "[A-Za-z]+" )
																	$PATTERN_ALPHA_WHITESPACE = Pattern.CompileFind( "[A-Za-z @t@n]+" )
																	
																	$PATTERN_NUMERIC = Pattern.CompileFind( "-*[0-9]+" )
																	$PATTERN_NUMERIC_WHITESPACE = Pattern.CompileFind( "-*[0-9 @t@n]+" )
																	
																	$PATTERN_ALPHANUMERIC = Pattern.CompileFind( "[A-Za-z0-9]+" )
																	$PATTERN_ALPHANUMERIC_WHITESPACE = Pattern.CompileFind( "[A-Za-z0-9 @t@n]+" )
																	
																	$PATTERN_SORT_PARAMETER = Pattern.CompileFind( "-*[A-Za-z]+" )
																	
																	$WebDSP.RequestHandlerUtils.fPatternsCompiled = TRUE
																end		
															
																if ( Type( val ) == StringType )
																
																	if ( check < 0 )
																	
																		whitespaceAllowed = TRUE
																		checkType = check * -1
																	else
																		checkType = check
																	end
																	
																	switch checkType
																		
																		//
																		// Alpha
																		//
																		case 1
																		
																			if ( whitespaceAllowed )
																			
																				finder = $PATTERN_ALPHA_WHITESPACE
																			else
																				finder = $PATTERN_ALPHA
																			end
																			
																			strType = [WEBDSP_ERRMSG.Alphabetic]
																		end
																		
																		//
																		// Numeric
																		//
																		case 2
																		
																			if ( whitespaceAllowed )
																			
																				finder = $PATTERN_NUMERIC_WHITESPACE
																			else
																				finder = $PATTERN_NUMERIC
																			end
																			
																			strType = [WEBDSP_ERRMSG.Numeric]
																		end
																		
																		//
																		// Alphanumeric
																		//
																		case 3
																		
																			if ( whitespaceAllowed )
																			
																				finder = $PATTERN_ALPHANUMERIC_WHITESPACE
																			else
																				finder = $PATTERN_ALPHANUMERIC
																			end
																			
																			strType = [WEBDSP_ERRMSG.Alphanumeric]
																		end
															
																		//
																		// Alphanumeric
																		//
																		case 4
																		
																			finder = $PATTERN_SORT_PARAMETER
																			
																			strType = [WEBDSP_ERRMSG.SortType]
																		end
																		
																		default
																		
																			validConstant = FALSE
																		end
																	end
																	
																	if ( validConstant )
																	
																		findResult = Pattern.Find( val, finder )
																
																		if !( IsDefined( findResult ) && findResult[ 1 ] == 1 && findResult[ 2 ] == Length( val ) )
																		
																			.fError = Str.Format( [WEBDSP_ERRMSG.ArgumentWasNotA1Value], strType )
																			retVal = FALSE
																		end
																	else
																		.fError = "Invalid pattern constant. Look in CheckArgsUtil() for list of valid integer constants." // Do not Xlate - Friendly developer error :)
																	end
																else
																	.fError = "Argument type was not a String." // Do not Xlate - Friendly developer error :)
																end
																
																return retVal
															end
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							return undefined
						End

	
	public function String ErrorFilePrefix()
						
							return .ModHTMLPrefix()
						
						end

	
	override function Dynamic Execute( Dynamic ctxIn, Dynamic ctxOut, Record r )
						
							Object prgCtx = .PrgSession()
						
							Object uSession = prgCtx.USession()
						
							Dynamic userInfo = UAPI.GetByID( uSession.fSession, uSession.fUserID )
							
							if Length( userInfo ) > 0
								$DataManager.SynCoreInterface.PushNDC( userInfo[1].NAME + " (" + Str.String( userInfo[1].ID ) + ")" )
							end
							
							String profileName = Str.Format( "%1.%2", .fBaseProfileName, .fName )
							
							$SyntergyCore.Stats.Start( "datamanager:request", profileName )
							
							Dynamic rtn = .DoExec( ctxIn, ctxOut, r )
							
							$DataManager.SynCoreInterface.ClearNDC()	
							
							Real elapsed = $SyntergyCore.Stats.Stop( "datamanager:request", profileName )
							
							// func() will return datamanager.func_name
							
							Object log = $DataManager.SynCoreInterface.GetLogger( "datamanager.requesthandler." + .Func() )
							log.Info( "{} executed in {} seconds.", { .Func(), elapsed } ) 
											
							return rtn	
						end

	
	override function Dynamic ExecuteRequest( Dynamic ctxIn, Dynamic ctxOut, Record r )
						
							// *** EW - 03/29/2013 - Clearing NDC here so it is always fresh for DataManager requests.
							$DataManager.SynCoreInterface.ClearNDC()
						
							// *** normal processing
							Dynamic retVal = .ExecuteWithLoginAlt( ctxIn, ctxOut, r )
							
							return retVal
						end

	script ExecuteWithLoginAlt
	
			
					
							
									
											
													
												
														
															function Dynamic ExecuteWithLogin( Dynamic ctxIn, Dynamic ctxOut, record r )
															
																// We want ExecuteWithLogin behavior but without the redirections and error pages -- switch out
																// .fLocation and .fError for better behavior when using json/jsonp.
																
																Dynamic result = .ExecuteWithLogin( ctxIn, ctxOut, r )
																
																if IsDefined( .fLocation )
																	// if it redirected us to ll.getlogin, set error instead.
																	
																	if IsUndefined( .fError ) 
																		if IsDefined( Str.LocateI( .fLocation, "ll.getlogin" ) )
																			.fError = "You are not logged into this system."
																		else
																			if IsUndefined( .fContent )
																				// this re-direct is probably an error since .fContent is not set.
																				.fError = "An unknown server error occurred."
																			end
																		end
																	end
																	
																	// don't redirect please ...
																	.fLocation = undefined
																end
																
																// assume if .fError set it will be handled correctly by generate output... 
																return	result
															end
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Void GenerateOutput( Dynamic ctxIn, Dynamic ctxOut, Record r, Dynamic data )
						
							String		json
							String		contentType = "application/json"
							String		httpStatus	
								
							// first check that the output wasn't already generated and 
							// the socket already closed.
							
							if !( .fSocketClosed )
								
								boolean isJSONP =  IsFeature( r, "callback" )
						
								// convert to JSON ...
						
								if IsDefined( .fError )
									if isJSONP
									
										.fContent = Assoc.createAssoc()
										.fContent.__jsonpError = true
										.fContent.result = .fError
								
										// we need to convert errors to assoc for JSONP
										if Type( .fError ) == StringType
											.fContent.status = .fError
										elseif IsFeature( .fError, "errMsg" ) 
											.fContent.status = .fError.errMsg
										else	
											.fContent.status = "Error"
										end
									
									else
										
										// return entire .fError in data ...
										.fContent = .fError	
						
										if Type( .fError ) == StringType
											httpStatus = .fError
										elseif IsFeature( .fError, "errMsg" ) 
											httpStatus = .fError.errMsg
										else	
											httpStatus = "Error"
										end
									end
									
									// pass true to use the Web.ToJSON built in
									json = $DataManager.SynCoreInterface.toJSON( .fContent, true )
								else		
									if .fAlreadyJSON
										json = .fContent
									else
										// pass true to use the Web.ToJSON built in
										json = $DataManager.SynCoreInterface.toJSON( .fContent, true )				
									end		
								end
						
									
								// JSONP format?  Change content-type and wrap in callback.
													
								if isJSONP
									contentType = "application/javascript"
									json = Str.Format( "%1(%2)", r.callback, json )
								end
										
								// generate header
						
								String		htmlHeaders = ''
								String		charset
								
								if ( IsDefined( .fExtraHeaders ) && .fExtraHeaders != '' )
									htmlHeaders = "Expires: -1" + Web.CRLF + .fExtraHeaders
								else
									htmlHeaders = "Expires: -1" + Web.CRLF
								end
								
								charset = $Kernel.SystemPreferences.GetPrefLocale( "HTMLCharset" )
								
								if  .fUseCharSet && IsDefined( charset ) && IsNotError( charset ) && length( Str.Trim( charset ) )
									charset = ";charset=" + charset
								elseif ( System.CharEncoding() == System.UTF8 )
									charset = ";charset=UTF-8"
								else
									charset = ""
								end
								
								if httpStatus != ""
									htmlHeaders += Str.Format( "Status:512 %1%2", httpStatus, Web.CRLF )
								end
							
								String	s = \
									"Content-Type: " + contentType + charset + Web.CRLF + htmlHeaders + Web.CRLF
									
								Web.Write( ctxOut, s )
										
								Web.Write( ctxOut, json )
							end
						end

	script _FormatTestContent
	
			
					
							
									
											
													
												
														
															function String FormatTestContent(Dynamic val)
															
																String s
															
																switch type(val)
																	case ListType
																		s+='<TABLE CELLPADDING="5" CELLSPACING="0" border="0">'	
																			Dynamic v
																			Integer i = 1
																			for v in val
																				s+='<TR>'
																				s += Str.Format('<TD>val %1</TD>', i)
																				s+=Str.Format('<TD>%1</TD>', FormatTestContent(v))
																				s+='</TR>'
																				i+=1
																			end
																		s+='</TABLE>'			
																	end	
																	case Assoc.AssocType
																		s+='<TABLE CELLPADDING="5" CELLSPACING="0" border="0">'	
																			List colNames = Assoc.Keys(val)
																			String c
																			for c in colNames
																				s+='<TR>'
																				s += Str.Format('<TD>%1</TD>', c)
																				s+=Str.Format('<TD>%1</TD>', FormatTestContent(val.(c)))
																				s+='</TR>'
																			end
																		s+='</TABLE>'			
																	end
																	
																	case RecArray.RecArrayType
																		RecArray recs = val
																		s+='<TABLE CELLPADDING="5" CELLSPACING="0" border="0">'	
																			List colNames = RecArray.FieldNames(recs)
																			String c
																			s+='<TR>'
																			for c in colNames
																				s += Str.Format('<TH>%1</TH>', c)
																			end
																			s+='</TR>'
																			Record rec
																			for rec in recs
																				s+='<TR>'
																				for c in colNames
																					s+=Str.Format('<TD>%1</TD>', FormatTestContent(rec.(c)))
																				end								
																			s+='</TR>'
																			end
																		s+='</TABLE>'		
																	end
																	case RecArray.RecordType
																		Record rec = val
																		s+='<TABLE CELLPADDING="5" CELLSPACING="0" border="0">'	
																			List colNames = RecArray.FieldNames(rec)
																			String c
																			s+='<TR>'
																			for c in colNames
																				s += Str.Format('<TH>%1</TH>', c)
																			end
																			s+='</TR>'
																			s+='<TR>'
																			for c in colNames
																				s+=Str.Format('<TD>%1</TD>', FormatTestContent(rec.(c)))
																			end								
																			s+='</TR>'		
																		s+='</TABLE>'
																	end					
																	default
																		s = Str.String(val)
																	end
																end
																
																return s
																			
															end				
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

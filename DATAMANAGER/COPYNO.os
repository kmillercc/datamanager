package DATAMANAGER

public object COPYNO inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'poNumCopies'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$COPYNO'

end

package DATAMANAGER

/**
 * 
 * 
 * /*  Here is the code from BulkLOader for setting ot email properties
 * 	function Assoc SetEmailProperties(Assoc csvVals)
 * 	
 * 		Assoc		otEmailData
 * 		Assoc		props
 * 		String		body
 * 		List		participants
 * 		Assoc		customProps
 * 		Integer		version
 * 	
 * 		// *** set the body	
 * 		body = csvVals.('$EmailBody')
 * 		
 * 		// *** participants
 * 		participants = _GetParticipants(csvVals.('$EMailParticipants'))
 * 		
 * 		// *** custom properties (not really supported right now)
 * 		customProps = Assoc.CreateAssoc()	
 * 	
 * 		// *** get the version
 * 		version = Str.StringToInteger(csvVals.("$EmailVersion"))
 * 	
 * 		// *** basic email properties
 * 		props.OTEmailSubject = csvVals.('$EmailSubject')
 * 		props.OTEmailTo = csvVals.('$EmailTo')
 * 		props.OTEmailCC = csvVals.('$EmailCC')
 * 		props.OTEmailBCC = csvVals.('$EmailBCC')
 * 		props.OTEmailFrom = csvVals.('$EmailFrom')
 * 		props.OTEmailOnBehalfOf = csvVals.('$EmailOnBehalfOf')
 * 		props.OTEmailSentDate = $BulkLoader.Loader.MetadataDateToDate(csvVals.('$EmailSentDate'))
 * 		props.OTEmailReceivedDate = $BulkLoader.Loader.MetadataDateToDate(csvVals.('$EmailReceivedDate'))
 * 		props.BodyFormat = Str.StringToInteger(csvVals.('$EmailBodyFormat'))
 * 		props.HasAttachments = Str.StringToInteger(csvVals.('$EmailHasAttachments'))
 * 	
 * 		otEmailData.Properties = props
 * 		otEmailData.Body = body
 * 		otEmailData.Participants = participants
 * 		otEmailData.CustomProperties = customProps
 * 		otEmailData.Version	= version
 * 		
 * 		return otEmailData
 * 	
 * 	end
 * 	
 * 	function List _GetParticipants(Dynamic input)
 * 	
 * 		input = Str.ReplaceAll(input, '|', ',')
 * 		
 * 		input = Str.StringToValue(input)
 * 	
 * 		if IsDefined(input) && Type(input) == ListType	
 * return input
 * 		else
 * return {}
 * 		end
 * 		
 * 	end
 * 
 * * /
 * 
 * 
 * 
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 */
public object Email inherits DATAMANAGER::ColumnValidators

	override	String	fApiKey1 = 'metadata'
	override	String	fApiKey2 = 'emailData'
	override	String	fApiKey3 = 'Properties'



	
	override function __GenerateValidators()
						
							Object 	child
							
							
							for child in OS.Children(this)
								
								child.fApiKey3 = child.fApiKey2
								OS.ClearValue( child, "fApiKey2" )		
								
							end
								
							/*
							for col in cols
								
								Object o = OS.New(this)
								o.OSNAME = Str.Upper(col[2:])
								o.fKey = Str.Upper(col)
								o.fEnabled = true
								
							end	
							*/
						
						end

	
	public function __SetKeys()
						
							Object child
							
							for child in OS.Children(this)
								child.fApiKey2 = Str.Lower(child.OSNAME[6:])
							end
								
							
						
						
						end

end

package DATAMANAGER

public object ExportLoadCmds inherits DATAMANAGER::#'DataManager WebNodeCmds'#

	public		Integer	fEvent
	public		List	fNodetypes = { 0, 132, 136, 196, 199, 202, 205, 206, 208, 212, 298, 345, 372, 373, 374, 398, 411, 412, 421, 422, 423, 424, 426, 428, 441, 480, 482, 542, 551, 552, 553, 554, 556, 558, 559, 602, 739, 751, 830, 831, 832, 833, 3030334 }


	/*protected*/
	override function List _CmdTypes()
							
							return	{ \
										Undefined \
									}
						end

	
	override function List _Nodetypes()
							
							return .fNodetypes
							
						end

	
	override function String _QueryString(\
							Record	args, \	//	RequestHandler.fArgs.
							Dynamic	node )	//	WebNodes view row.
							
							
							return Str.Format( .fQueryString, \
											   node.dataId )
						end

	script _SetNodetypes
	
			
					
														
						//	Returns the List of nodetypes (a.k.a. subtypes) to which this
						//	WebNodeCmd applies.
						//
						//	NOTE:	Do not use "$Type" globals here because they may not be
						//			defined until AFTER the WebNodeCmd has been initialized!
						//
						//	A value of { Undefined } indicates that this WebNodeCmd applies
						//	to all nodetypes.  This should be used with EXTREME caution!
						
						function void _Nodetypes()
							
							List	llNodes = $LLIAPI.LLNodeSubsystem.GetItems()
							Object	llNode
							List	nodeTypes
							
							for llNode in llNodes
								echo (Str.Format('%1 %2, %3, %4, %5, %6', llNode.fSubType, llNode.fName, llNode.IsContainer(), llNode.fObjectFactory, llNode.fMoveable, llNode.fRenameable))
								if (llNode.IsContainer() || llNode.fSubtype == 298) && llNode.fObjectFactory && llNode.fMoveable && llNode.fRenameable
									//echo(llNode.fName)
									nodeTypes = List.SetAdd(nodeTypes, llNode.fSubtype)		
								end
							end
							
							echo("length of nodetypes is =", Length(nodeTypes))
							
							.fNodetypes =  nodeTypes
							
						end
													
						/*
						List	llNodes = $LLIAPI.LLNodeSubsystem.GetItems()
						Object	llNode
						List	nodeTypes
						
						for llNode in llNodes
							echo (Str.Format('%1 %2, %3, %4, %5, %6', llNode.fSubType, llNode.fName, llNode.IsContainer(), llNode.fObjectFactory, llNode.fMoveable, llNode.fRenameable))
							if (llNode.IsContainer() || llNode.fSubtype == 298) && llNode.fObjectFactory && llNode.fMoveable && llNode.fRenameable
								//echo(llNode.fName)
								nodeTypes = List.SetAdd(nodeTypes, llNode.fSubtype)		
							end
						end
						
						echo("length of nodetypes is =", Length(nodeTypes))
						
						.fNodetypes =  nodeTypes									
						*/						
								
				
			
		
	
	endscript

	
	override function Boolean _SubclassIsEnabled(\
							Object	prgCtx,\				//	Program session.
							Dynamic	node,\					//	WebNodes view row.
							Dynamic	parent = Undefined )	//	WebNodes view row.
							
							if $DataManager.AllowPkg.IsAllowed(prgCtx, .fEvent) && $DataManager.NodeUtils.IsFolderNode(node)
								return true
							else
								return false
							end	
							
						end

end

package DATAMANAGER

public object VERMAJOR inherits DATAMANAGER::MajorMinorBooleanInt

	override	String	fApiKey3 = 'pVerMajor'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERMAJOR'

end

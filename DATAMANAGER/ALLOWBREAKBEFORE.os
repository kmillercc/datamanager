package DATAMANAGER

public object ALLOWBREAKBEFORE inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'allowBreakBefore'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$ALLOWBREAKBEFORE'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckBoolean(val)
							
							if status.ok
								val = status.result
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

package DATAMANAGER

public object checkFolderPath inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'folderPath', -1, 'FolderPath', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
							 * 	folderPath 		=> String 	=> the filepath we are validating
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.FileUtils.CheckPathExists(r.folderPath, "folder")
							if status.ok
								.fContent = synCore.SuccessResult(true)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "folderPath", StringType, "FolderPath", FALSE } \
								}
						end

end

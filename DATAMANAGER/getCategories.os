package DATAMANAGER

public object getCategories inherits DATAMANAGER::ExportRequestHandlers

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "getCategories.DoExec", this )
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
						     **/		
								
							// *** hand off to the API
							Assoc status = $DataManager.CategoryAPI.GetAvailableCategories(prgCtx)
							if status.ok
								.fContent = synCore.SuccessResult(status.result)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{ \
								}
						end

end

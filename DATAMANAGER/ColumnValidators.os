package DATAMANAGER

public object ColumnValidators inherits DATAMANAGER::LoadMetadataObjects

	public		String	_fAction
	public		String	_fMappedKey
	public		String	fApiKey1 = 'dummy'
	public		Dynamic	fApiKey2
	public		Dynamic	fApiKey3
	public		Dynamic	fApiKey4
	public		Boolean	fEnabled = FALSE
	public		String	fKey


	
	public function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc rtn
							
							// *** override in subclasses to provide functionality
							
							rtn.result = val
							rtn.skipNextTime	= true	// *** means that there is no validation at all, so we can skip making this call next time
							rtn.ok = true
							return rtn
												
						end

	
	private function Assoc _CheckBoolean(Dynamic val)
						
							Boolean bVal	
							
							if Type(val) == StringType
								bVal = $DataManager.FormatPkg.ConvertStringToVal(val, BooleanType)
								
								if IsUndefined(bVal)
									return .SetErrObj(.InitErrObj("_CheckBoolean"), Str.Format("'%1' is expected to be a boolean value", val))
								end
								
							elseif Type(val) == IntegerType
							
								bVal = val
								
							else
								return .SetErrObj(.InitErrObj("_CheckBoolean"), Str.Format("'%1' is expected to be a boolean value", val))	
							end		
						
							Assoc rtn	
							rtn.result = bVal
							rtn.ok = true
							return rtn
							
						end

	
	private function Assoc _CheckGroup(Object prgCtx, Dynamic val)
						
							Assoc rtn , status
							
							if Type(val) == StringType
								status = ._UserIDFromName(prgCtx, val, UAPI.GROUP)
								if !status.ok
									return .SetErrObj(.InitErrObj("_CheckGroup"), Str.Format("'%1' is not a valid group name in this system", val))
								else
									val = status.result	
								end	
							
							elseif Type(val) == IntegerType
								if !._IsValidUserId(prgCtx, val)
									return .SetErrObj(.InitErrObj("_CheckGroup"), Str.Format("'%1' is not a valid group id", val))
								end
							else
								return .SetErrObj(.InitErrObj("_CheckGroup"), Str.Format("'%1' needs to be either an integer or string representing a group", val))		
						 		
							end	
							
							rtn.result = val
							rtn.ok = true
							return rtn
							
						end

	
	private function Assoc _CheckNodeReference(Object prgCtx, Dynamic val)
						
							Assoc rtn , status
							DAPINODE node
							
							if Type(val) == StringType
								status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, val)
								if !status.ok
									return .SetErrObj(.InitErrObj("_CheckNodePath"), Str.Format("'%1' is not a valid node path in this system", val))
								else
									node = status.result
								end	
							
							elseif Type(val) == IntegerType
								node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, val)
								if IsError(node)
									return .SetErrObj(.InitErrObj("_CheckNodePath"), Str.Format("'%1' is not a valid node id in this system", val))
								end
							else
								return .SetErrObj(.InitErrObj("_CheckNodePath"), Str.Format("'%1' needs to be either an integer id or string representing a node in the system", val))		
						 		
							end	
							
							rtn.result = node
							rtn.ok = true
							return rtn
							
						end

	
	private function Assoc _CheckUser(Object prgCtx, Dynamic val)
						
							Assoc 	rtn , status
							Record	userRec
							
							if Type(val) == StringType
								status = ._UserRecFromName(prgCtx, val, UAPI.USER)
								if !status.ok
									return .SetErrObj(.InitErrObj("_CheckUser"), Str.Format("'%1' is not a valid username in this system", val))
								else
									userRec = status.result	
								end	
							
							elseif Type(val) == IntegerType
								status = ._UserRecFromID(prgCtx, val, UAPI.USER)
								if !status.ok
									return .SetErrObj(.InitErrObj("_CheckUser"), Str.Format("'%1' is not a valid username in this system", val))
								else
									userRec = status.result	
								end	
								
							else
								return .SetErrObj(.InitErrObj("_CheckUser"), Str.Format("'%1' needs to be either an integer or string representing a user", val))		
						 		
							end	
							
							rtn.result = userRec
							rtn.ok = true
							return rtn
							
						end

	
	private function Boolean _IsValidUserId(prgCtx, ID)
						
							UAPISession Usession = prgCtx.USession().fSession
							
							RecArray users = UAPI.GetByID( Usession, ID)
								
							if !IsError(users) && length(users) > 0
								return true
							else
								return false
							end		
						
						
						end

	
	public function __GenerateValidators()
						
							List cols
							String col
							
							for col in cols
								
								Object o = OS.New(this)
								o.OSNAME = Str.Upper(col[2:])
								o.fKey = Str.Upper(col)
								o.fEnabled = true
								
							end	
						
						
						end

	
	public function Void __Init()
						
							if .fEnabled
								// **** set up a map to this object in the NodeValidateWrapper
								Object wrapper = $DataManager.ValidateWrapper
								if IsUndefined(wrapper._fColValidatorMap)
									wrapper._fColValidatorMap = Assoc.CreateAssoc()
								end
								wrapper._fColValidatorMap.(.fKey) = this	
						
							
								// *** set up the keys for the Api Data Obj
								Object apidataObj = $DataManager.ApiDataObj
								if IsUndefined(apidataObj._fNodeApiKeysMap)
									apidataObj._fNodeApiKeysMap = Assoc.CreateAssoc()
								end
								
								if IsDefined(.fApiKey4)
									apidataObj._fNodeApiKeysMap.(.fKey) = {.fApiKey1, .fApiKey2, .fApiKey3, .fApiKey4}
								else
									apidataObj._fNodeApiKeysMap.(.fKey) = {.fApiKey1, .fApiKey2, .fApiKey3}
								end		
								
						
							end
						
						end

end

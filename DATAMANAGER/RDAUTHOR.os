package DATAMANAGER

public object RDAUTHOR inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsOriginator'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDAUTHOR'
	override	String	fUpdateApiKey3 = 'fOriginator'

end

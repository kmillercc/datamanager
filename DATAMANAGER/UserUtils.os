package DATAMANAGER

public object UserUtils inherits DATAMANAGER::Utils

	
	public function Assoc GetGroupMembers(Object prgCtx, \
																		 Integer groupId, \
																		 List desiredTypes = {UAPI.GROUP, UAPI.USER},\
																		 Boolean allInfo = false)
											
												Assoc		rtn = .InitErrObj("GetGroupMembers")
												RecArray 	recs
												String		typeFilter = Str.String(desiredTypes)[2:-2]
											
												// *** means we're at root
												if groupId == 0
												
													String stmt
													
													if allInfo
														stmt = "select k.ID,k.OwnerID,k.Type,k.SpaceID,k.Name,k.UserData,k.LeaderID,k.UserPWD,k.GroupID,k.UserPrivileges," +\
														"k.LastName,k.MiddleName,k.FirstName,k.MailAddress,k.Contact,k.Title from KUAF k where k.Type in (%1) and k.Deleted=0 " +\ 
															 "and NOT EXISTS (SELECT 1 FROM KUAFChildren kg WHERE kg.ChildID = k.ID) order by k.Name"		
													else
														stmt = "select k.ID,k.Type,k.Name from KUAF k where k.Type in (%1) and k.Deleted=0 " +\ 
															 "and NOT EXISTS (SELECT 1 FROM KUAFChildren kg WHERE kg.ChildID = k.ID) order by k.Name"		
													end
													
													stmt = Str.Format(stmt, typeFilter)
												
													// *** get all groups that are not members of another group
													recs = .ExecSql(prgCtx, stmt)
												
												else
													recs = UAPI.ChildrenListByID(prgCtx.USession().fSession, groupId)
												end
												
												if IsError(recs)
													.SetErrObj(rtn, recs)
													return rtn
												end
												
												return .SuccessResult(recs)	
											end

	
	public function Assoc GetUserGroupCount(Object prgCtx, \
																			 Integer groupId, \
																			 Integer limit, \
																			 Boolean doRecursive = false, \
																			 Assoc result = undefined)
											
												Assoc 	rtn = .InitErrObj("GetUserGroupCount")
												RecArray recs
												Record 	rec
												Assoc	status
											
												if IsUndefined(result)
													result = Assoc.CreateAssoc()
													result.count = 0
												end
												
												// *** get member list
												status = .GetGroupMembers(prgCtx, groupId, {UAPI.GROUP, UAPI.USER})
												if !status.ok
													.SetErrObj(rtn, recs)
													return rtn
												else
													recs = status.result	
												end	
												
												result.count+=Length(recs)
													
												if result.count >= limit
													return .SuccessResult(result.count)
												end
											
												if doRecursive		
													for rec in recs
														if rec.TYPE == UAPI.GROUP
															status = GetUserGroupCount(prgCtx, rec.ID, limit, doRecursive, result)
															if !status.ok
																.SetErrObj(rtn, status)
																return rtn
															end
														end
													end
												end	
												
												return .SuccessResult(result.count)
																			
											end

	
	public function Assoc UgListRecursive(Object prgCtx, \
																		 	Integer groupId, \
																		 	String groupPath, \
																		 	Boolean wantAllInfo, \
																			List desiredTypes, \			// example {UAPI.GROUP, UAPI.USER, 2} exports groups, users, and group memberships
																			Integer limit, \
																			Object callbackObj = undefined, \
																			Assoc result = undefined, \
																			Boolean returnRecs = false, \
																			Boolean isRoot = true)
											
												Assoc 	rtn = .InitErrObj("UgListRecursive")
												Record 	rec
												Assoc	status
												String 	childPath
												
												if isRoot
													$SyntergyCore.Timer.Start( "UgListRecursive" )
												end	
											
												if IsUndefined(result) || Length(result) == 0
													result = Assoc.CreateAssoc()
													result.recs = RecArray.Create()
													result.count = 0
												end
												
												// *** get member list
												status = .GetGroupMembers(prgCtx, groupId, {UAPI.GROUP, UAPI.USER}, wantAllInfo)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												end
												
												RecArray ugRecs = status.result
											
												for rec in ugRecs
												
													if rec.TYPE in desiredTypes
														result.count += 1
														childPath = groupPath + ":" + rec.NAME
														
														if result.count >= limit
															// *** KM 10/2/14 change to assoc return to avoid stack trace
															return .SuccessResult(result)
														end
														
														if returnRecs
															result.recs = {@result.recs, rec}							
														end
														
														if IsDefined(callbackObj) && IsFeature(callbackObj, "Callback")
															status = callbackObj.Callback(result.count, childPath, rec, false)
															if !status.ok
																.SetErrObj(rtn, status)
																return rtn
															end
														end									
													end
													
													if rec.TYPE == UAPI.GROUP
													
														status = UgListRecursive(prgCtx, rec.ID, childPath, wantAllInfo, desiredTypes, limit, callbackObj, result, returnRecs, false)
														if !status.ok
															.SetErrObj(rtn, status)
															return rtn
														end
													end
												end
											
												if IsRoot
													echo("UgListRecursive Total Count = ", result.count, " users and groups.")
													Real elapsed = $SyntergyCore.Timer.Stop( "UgListRecursive" )
													echo("UgListRecursive ", elapsed, "seconds")
													
												end
											
												
												return .SuccessResult(result)
																			
											end

end

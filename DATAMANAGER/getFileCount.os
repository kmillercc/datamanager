package DATAMANAGER

public object getFileCount inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'filepath', -1, 'FilePath', FALSE }, { 'doRecursive', 5, 'DoRecursive', TRUE, FALSE }, { 'showFiles', 5, 'ShowFiles', TRUE, TRUE }, { 'returnDirect', 5, 'ReturnDirect', TRUE, FALSE }, { 'getMetadata', 5, 'GetMetadata', TRUE, FALSE }, { 'includeRoot', 5, 'includeRoot', TRUE, FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
												
												Object		synCore = $DataManager.SynCoreInterface
												Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
												Assoc		status
												/**
												 * R may contain: 
												 * 	filepath 		=> String 	=> the filepath we are gettting count for
												 *  includeRoot		=> Boolean	=>  Include the root in the count	 
											     **/
											
												// **** make sure file path does not come in empty! very important as this will cause LL to start looking in the root
												if Str.Trim(r.filepath) == ''
													// ** for debugging
													synCore.SetErrObj( rtn, "Filepath must not be empty")
													.fError = rtn	
													return undefined				
												end
											
												// *** check that file exists
												if !r.createPath
													status = $DataManager.FileUtils.CheckPathExists(r.filepath, 'folder')
													if !status.ok
														// ** for debugging
														synCore.SetErrObj( rtn, status )
														.fError = rtn
														return undefined	
													end
												end
												
												List	excludedFileNames = {"auditlog.log", "_audit", "_audit" + File.Separator()}
												
												Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)		
													
												// *** hand off to the API
												Assoc result
												result.count = r.includeRoot ? 1 : 0
												Integer count = $DataManager.FileUtils.FileCountRecursive(r.filepath, limit, result, excludedFileNames)
												.fContent = synCore.SuccessResult(count)	
												
												return undefined	
											End

	
	override function void SetPrototype()
												
												.fPrototype =\
													{\
														{ "filepath", StringType, "FilePath", FALSE }, \
														{ "doRecursive", BooleanType, "DoRecursive", TRUE, FALSE }, \			
														{ "showFiles", BooleanType, "ShowFiles", TRUE, TRUE }, \			
														{ "returnDirect", BooleanType, "ReturnDirect", TRUE, FALSE }, \						
														{ "getMetadata", BooleanType, "GetMetadata", TRUE, FALSE }, \
														{ "includeRoot", BooleanType, "includeRoot", TRUE, FALSE } \			 
													}
											end

end

package DATAMANAGER

public object RDNEXTREVIEWDATE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.nextReviewDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDNEXTREVIEWDATE'
	override	String	fUpdateApiKey3 = 'fNextReviewDate'

end

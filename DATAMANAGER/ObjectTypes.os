package DATAMANAGER

public object ObjectTypes inherits DATAMANAGER::CachedLookupObjects

	public		Dynamic	_fCachedAllTypes
	public		Dynamic	_fCachedCaseTypes
	public		Dynamic	_fCachedContainerTypes
	public		Dynamic	_fCachedFileTypes
	override	Boolean	fEnabled = TRUE


	
	public function Assoc GetAllObjectTypes(Object prgCtx)
											
												Assoc	result
												
												if IsDefined(._fCachedAllTypes)
													result = ._fCachedAllTypes
												else
													._fCachedAllTypes = result = ._GetObjectTypes(prgCtx, true, true)
												end
												
												return result
											
											end

	
	public function List GetCaseTypes(Object prgCtx)
											
												List	types
												
												if IsDefined(._fCachedCaseTypes)
													types = ._fCachedCaseTypes
												elseif IsDefined($CaseBasic)
													._fCachedCaseTypes = types = $CaseBasic.Utils.GetCaseSubtypes(prgCtx)
												end
												
												return types
											
											end

	
	public function Assoc GetContainerTypes(Object prgCtx)
											
												Assoc	result
												
												if IsDefined(._fCachedContainerTypes)
													result = ._fCachedContainerTypes
												else
													._fCachedContainerTypes = result = ._GetObjectTypes(prgCtx, false, true)
												end
												
												return result
											
											end

	
	public function Assoc GetFileTypes(Object prgCtx)
											
												Assoc	result
												
												if IsDefined(._fCachedFileTypes)
													result = ._fCachedFileTypes
												else
													._fCachedFileTypes = result = ._GetObjectTypes(prgCtx, true, false)
												end
												
												return result
											
											end

	script _GetObjectTypes
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Assoc GetObjectTypes(Object prgCtx, \
																																Boolean	wantFiles = true, \
																																Boolean wantContainers = true)
																									
																										List		exclude = {}
																										Object		factoryUtil = $LLIAPI.FactoryUtil	
																										RecArray	objs = RecArray.Create( { "id", "name"} )
																										List		listOfAssoces, listOfSubtypes
																										
																										// *** List of object types will be limited to what can be created in a folder
																										Object	folderWebNode = $WebNode.WebNodes.GetItem(0)
																										
																										Object 	webNode
																									
																										for webNode in folderWebNode.ChildWebNodes( TRUE, prgCtx )
																											Integer subtype = webNode.fSubtype
																											
																											if ( subtype in exclude  == 0) && factoryUtil.IsCreatable( prgCtx, subtype ) && _IsDesired(subtype, wantFiles, wantContainers)
																												RecArray.AddRecord( objs, { subtype, webNode.Name()} )
																												listOfSubtypes = {@listOfSubtypes, subtype}
																											end	
																										end
																										
																										RecArray.Sort(objs, "name")
																										
																										Object obj
																										for obj in objs
																											Assoc a = Assoc.CreateAssoc()
																											a.id = obj.id
																											a.name = obj.name
																											listOfAssoces = {@listOfAssoces, Assoc.Copy(a)}
																										end
																										
																										Assoc result
																										result.listOfAssoces = listOfAssoces
																										result.listOfSubtypes = listOfSubtypes
																										
																										return result
																									
																									end
																									
																									function _IsDesired(Integer subtype, Boolean wantFiles, Boolean wantContainers)
																										
																										if wantFiles && wantContainers
																											return true
																										elseif wantFiles
																											return $LLIAPI.LLNodeSubsystem.GetItem(subtype).fVersioned
																										elseif wantContainers
																											return !$LLIAPI.LLNodeSubsystem.GetItem(subtype).fVersioned
																										else
																											// *** something is wrong here
																											return false			
																										end	
																										
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

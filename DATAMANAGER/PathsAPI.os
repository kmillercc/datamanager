package DATAMANAGER

public object PathsAPI inherits DATAMANAGER::CrudKiniAPI

	override	List	_fRequiredFields = { 'path', 'name' }


	
	override function Assoc _SubclassValidate(Object prgCtx, \
													Assoc data)
						
							Assoc 	rtn = .InitErrObj( "_SubclassValidate" )
							Assoc	status
							
								
							
							if IsDefined(data.path)
								status = $DataManager.FileUtils.CheckPathExists(data.path, "folder")
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end			
							else
								.SetErrObj(rtn, "Server Path is required.")
								return rtn			
							end	
						
							return rtn
						
						end

end

package DATAMANAGER

public object InstanceObjects inherits DATAMANAGER::SynCoreMain

	public		Boolean	fEnabled = FALSE


	
	public function void Destructor()
											
												// *** cleanup will set all object and frame references to undefined to allow for full object deletion
												._Cleanup()
											
												// *** give subclasses a chance to clean up (i.e. close files, etc)
												// *** there is some redundancy in these subclass scripts, but that's ok
												._SubclassDestructor()
											
											end

	
	private function void _Cleanup()
												
												// *** cleanup any objects in the frame to avoid memory leaks
											
												String f
												
												for f in Frame.SlotNames(this)
													if Type(this.(f)) in {ObjectType, Frame.FrameType}
														this.(f) = undefined
													end	
												end
												
											end

	
	private function Frame _NewFrame()
											
												return Frame.New( {this} )
											
											end

	
	private function void _SubclassDestructor()
											
											end

	
	public function Void __Init()
											
												// put code to execute on startup here
											
											end

end

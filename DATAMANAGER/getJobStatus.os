package DATAMANAGER

public object getJobStatus inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'jobid', 2, 'jobid', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 *  jobid			=> Integer	=> jobid
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.SchedJobsAPI.GetItem(prgCtx, r.jobid)
							if status.ok
								
								.fContent = synCore.SuccessResult(status.result.JOBSTATUS)
							
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "jobid", IntegerType, "jobid", FALSE } \
								}
						end

end

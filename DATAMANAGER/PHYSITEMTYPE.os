package DATAMANAGER

public object PHYSITEMTYPE inherits DATAMANAGER::PhysObjCreate

	override	String	fApiKey4 = 'selectedMedia'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$PHYSITEMTYPE'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							if Type(val) == StringType						
								// *** Call to the POMediaTypes to handle this
								Assoc status = $DataManager.PoMediaTypes.ConvertMediaTypeId(prgCtx, val)
								if !status.ok	
									return .SetErrObj(.InitErrObj("ValidateVal"), status)
								else
									val = status.result
								end
							end
						
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

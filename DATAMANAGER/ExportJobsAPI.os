package DATAMANAGER

public object ExportJobsAPI inherits DATAMANAGER::SchedJobsAPI

	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg
								
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_EXPORT_MNG_JOBS)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end		
						
							return rtn
						
						end

	
	public function void __Init()
						
							Object tempObj = OS.NewTemp(this)
							tempObj._fUIToDbMap = ._GetUiToDbMap()
							tempObj._fDBToUIMap = ._GetDbToUiMap()	
							
							$DataManager.ExportJobsAPI = tempObj
						
						end

end

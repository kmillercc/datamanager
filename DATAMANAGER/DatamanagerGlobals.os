package DATAMANAGER

public object DatamanagerGlobals inherits KERNEL::Globals

	public		Object	AboutDataManager = DATAMANAGER::AboutDataManager
	public		Object	AllowPkg = DATAMANAGER::AllowPkg
	public		Object	ApiDataObj = DATAMANAGER::ApiDataObj
	public		Object	AttrGroupPrep = DATAMANAGER::CORE::AttrGroupPrep
	public		Object	AuditAPI = DATAMANAGER::AuditAPI
	public		Object	AutoDocumentUtils = DATAMANAGER::AutoDocumentUtils
	public		Object	CSVWriter = DATAMANAGER::CSVWriter
	public		Object	CatAttrValidator = DATAMANAGER::CatAttrValidator
	public		Object	CatFrameAttrPrep = DATAMANAGER::CORE::CatFrameAttrPrep
	public		Object	CategoryAPI = DATAMANAGER::CategoryAPI
	public		Object	ColumnValidators = DATAMANAGER::ColumnValidators
	public		Object	ContractFileUtils = DATAMANAGER::ContractFileUtils
	public		Object	CsvColumnPicker = DATAMANAGER::CsvColumnPicker
	public		Object	CsvDisplayCallback = DATAMANAGER::CsvDisplayCallback
	public		Object	CsvIterator = DATAMANAGER::CORE::CsvIterator
	public		Object	CsvParser = DATAMANAGER::CsvParser
	public		Object	DataCache = DATAMANAGER::DataCache
	public		Object	DistribAgentsAPI = DATAMANAGER::DistribAgentsAPI
	public		Object	DropoffConverter = DATAMANAGER::DropoffConverter
	public		Object	DropoffJobsAPI = DATAMANAGER::DropoffJobsAPI
	public		Object	ExportJobsAPI = DATAMANAGER::ExportJobsAPI
	public		Object	ExportPathsAPI = DATAMANAGER::ExportPathsAPI
	public		Object	ExportPrivilegesAPI = DATAMANAGER::ExportPrivilegesAPI
	public		Object	ExportProfilesAPI = DATAMANAGER::ExportProfilesAPI
	public		Object	FileIterator = DATAMANAGER::CORE::FileIterator
	public		Object	FileMetadataObj = DATAMANAGER::FileMetadataObj
	public		Object	FileUtils = DATAMANAGER::FileUtils
	public		Object	FileWriter = DATAMANAGER::FileWriter
	public		Object	FormatPkg = DATAMANAGER::FormatPkg
	public		Object	JobInterface = DATAMANAGER::JobInterface
	public		Object	License = DATAMANAGER::LicenseObj
	public		Object	LoadJobsAPI = DATAMANAGER::LoadJobsAPI
	public		Object	LoadPathsAPI = DATAMANAGER::LoadPathsAPI
	public		Object	LoadPrivilegesAPI = DATAMANAGER::LoadPrivilegesAPI
	public		Object	LoadProfilesAPI = DATAMANAGER::LoadProfilesAPI
	public		Object	ModulePrefs = DATAMANAGER::ModulePrefs
	public		Object	NewObjectInterface = DATAMANAGER::CORE::NewObjectInterface
	public		Object	NodeAPI = DATAMANAGER::NodeAPI
	public		Object	NodeAddVersion = DATAMANAGER::CORE::NodeAddVersion
	public		Object	NodeAssignLocator = DATAMANAGER::CORE::NodeAssignLocator
	public		Object	NodeAssignTransfer = DATAMANAGER::CORE::NodeAssignTransfer
	public		Object	NodeBox = DATAMANAGER::CORE::NodeBox
	public		Object	NodeCache = DATAMANAGER::NodeCache
	public		Object	NodeCacheRoot = DATAMANAGER::NodeCache
	public		Object	NodeChildGetter = DATAMANAGER::NodeChildGetter
	public		Object	NodeCirculate = DATAMANAGER::CORE::NodeCirculate
	public		Object	NodeGenerateLabels = DATAMANAGER::CORE::NodeGenerateLabels
	public		Object	NodeIterator = DATAMANAGER::NodeIterator
	public		Object	NodeMetadataGatherer = DATAMANAGER::NodeMetadataGatherer
	public		Object	NodeMetadataUpdate = DATAMANAGER::CORE::NodeUpdateMetadata
	public		Object	NodePurgeVersions = DATAMANAGER::CORE::NodePurgeVersions
	public		Object	NodeRename = DATAMANAGER::CORE::NodeRename
	public		Object	NodeUpdate = DATAMANAGER::CORE::NodeUpdate
	public		Object	NodeUpdateMetadata = DATAMANAGER::CORE::NodeUpdateMetadata
	public		Object	NodeUpdatePerms = DATAMANAGER::CORE::NodeUpdatePerms
	public		Object	NodeUtils = DATAMANAGER::NodeUtils
	public		Object	ObjectFactory = DATAMANAGER::ObjectFactory
	public		Object	ObjectTypes = DATAMANAGER::ObjectTypes
	public		Object	PhysObjPreProcessor = DATAMANAGER::CORE::PhysObjPreProcessor
	public		Object	PingerHandler = DATAMANAGER::ASYNCJOB::DataManager_PingerHandler
	public		Object	PoMediaTypes = DATAMANAGER::PoMediaTypes
	public		Object	ProcessController = DATAMANAGER::CONTROLLER::ProcessController
	public		Object	RMPreProcessor = DATAMANAGER::CORE::RMPreProcessor
	public		Object	RootNodes = DATAMANAGER::RootNodes
	public		Object	SchedJobsAPI = DATAMANAGER::SchedJobsAPI
	public		Object	Scheduler = DATAMANAGER::Scheduler
	public		Object	SynCoreInterface = DATAMANAGER::SynCoreInterface
	public		Object	SysAttrValidator = DATAMANAGER::SysAttrValidator
	public		Object	SystemAttributes = DATAMANAGER::SystemAttributes
	public		Object	TemplateUtils = DATAMANAGER::CORE::TemplateUtils
	public		Object	Tester = DATAMANAGER::Tester
	public		Object	UGIterator = DATAMANAGER::UGIterator
	public		Object	UgMetadataGatherer = DATAMANAGER::UgMetadataGatherer
	public		Object	UgUpdate = DATAMANAGER::CORE::UgUpdate
	public		Object	UiSupportAPI = DATAMANAGER::UiSupportAPI
	public		Object	UserUtils = DATAMANAGER::UserUtils
	public		Object	Utils = DATAMANAGER::Utils
	public		Object	ValidateWrapper = DATAMANAGER::CORE::ValidateWrapper
	public		Object	WebModule = DATAMANAGER::#'Datamanager WebModule'#
	public		Dynamic	fCachedRMSubtypes
	public		Boolean	fDateFormatsInitialized = FALSE
	public		String	fJavaDateFormat
	public		String	fJavaDateTimeFormat
	public		String	fJavaISO8601DateFormat = 'yyyy-MM-dd''T''HH:mm:ss'
	public		Integer fTaskTypeDMMap = 99

end

package DATAMANAGER

public object RDOFFICIAL inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.official'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDOFFICIAL'
	override	String	fUpdateApiKey3 = 'fOfficial'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckBoolean(val)
							
							if status.ok
								val = status.result
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

package DATAMANAGER

public object JobInterface inherits DATAMANAGER::SynCoreMain

	public		String	_fIniSection = 'BulkTools.JobLock'


	
	public function Assoc CompleteJob(	Object prgCtx, Object job, Assoc result = Assoc.CreateAssoc() )
							
							Assoc rtn = .InitErrObj("UpdateJobStatus")							
							
							// *** set status							
							job.SetStatus("completed")			
							
							// *** add result
							if IsDefined(result)
								Assoc payload = job.getPayload()
								payload.result = result
								job.SetPayload(payload)
							end
											
							// *** complete the job
							Assoc status = job.Complete(prgCtx)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end
							
							return rtn
							
						end

	/* 
							This creates an Export or Load job to store data for the export/load
							The entry will stay  until it is finished or timed out.
						*/
	public function Assoc CreateJob( 	Object prgCtx, \
													Assoc jobPayload, \
													String jobType, \
													Integer extId = 0, \
													Integer timeOut=8*60*60)		/* default a timeout of 8 hours */
						
							Assoc 	rtn = .InitErrObj('_CreateExportJob')
							Assoc 	status
							Dynamic result
							
							if prgCtx.fDbConnect.StartTrans()
						
								// create a job to store updating info...
											
								status = $SyntergyCore.JobAPI.CreateJob( prgCtx, jobType, extId, "active", jobPayload, timeOut )
								
								if status.ok
									result = status.result
								else
									.SetErrObj( rtn, status )
								end	
						
								// commit.
								prgCtx.fDbConnect.EndTrans( rtn.ok )
							
							else
								.SetErrObj( rtn, "Could not start transaction." )
							end
							
							if !rtn.ok
								return rtn
							end	
						
							// *** cleanup old jobs -- clear inactive/completed jobs older than one day --
							//		Errors here will be logged but will not be returned...
							
							Integer minAgeSeconds = 60 * 60 * 24
							
							status = $SyntergyCore.JobAPI.CleanupType( prgCtx, jobType, minAgeSeconds )
							
							if !status.ok
								.GetLogger().Error( "Failed to cleanup expired and complete '{}' jobs: {}", { jobType, status.errMsg } )
							end		
							
							return .SuccessResult(result)
						End

	
	public function Assoc GetActiveJobByExtId( Object prgCtx, String jobType, Integer extId, Date maxAge = Date.Now() - 60*60*48 )
						
							Assoc 	rtn = .InitErrObj("GetJob")
						
							Boolean	onlyActive = true
							
							// *** call syntergycore JobsAPI
							Assoc 	status = $Syntergycore.JobAPI.GetJobsByExtID( prgCtx, jobType, extId, onlyActive, maxAge)
							Object	job
							
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
								
							elseif Length(status.result) > 1
								job = status.result[1]	
								
							elseif Length(status.result) == 1
							
								job = status.result[1]
							
							else	
							
								.SetErrObj(rtn, Str.Format("Could not get job with extId of %1", extId))
								return rtn	
								
							end
								
							return .SuccessResult(job)
								
						End

	
	public function Assoc GetJob( Object prgCtx, Integer jobID )
						
							return $Syntergycore.JobAPI.GetJob(prgCtx, jobID)
								
						End

	
	public function Assoc GetJobPayload( Object prgCtx, Integer jobID )
							
							Assoc 	rtn = .InitErrObj("GetJobPayload")
							Assoc 	status
							Object 	job
							Dynamic	payload
							
							// *** retrieve the job
							status = .GetJob(prgCtx, jobID)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							else
								job = status.result
								payload = job.GetPayload()
							end	
							
							return .SuccessResult(payload)
							
						End

	
	public function void IncrementCompletedCount(Object prgCtx, \
																Object job, \
																Integer increment = 1, \
																Boolean doThrottle = true, \
																Integer throttleSeconds = 5)
															
							Assoc jobData = job.GetPayload()
							
							jobData.completedCount += increment
							
							//echo("completedCount=", jobData.completedCount)
						    
						    job.SetPayload( jobData )
						    
						    if doThrottle
						    	job.ThrottledSave( prgCtx, throttleSeconds )
						    else
						    	job.Save(prgCtx)
						    end	
						    
						end

	
	public function Assoc SaveJob(Object prgCtx, Object job)
						
							Assoc 	rtn = .InitErrObj("SaveJob")
							Assoc	status
						
							// *** set the job payload from the data cache	
							status = job.Save(prgCtx)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end	
							
							return rtn
						end

	
	public function Assoc SetErrorOnJob(Object prgCtx, \
												Object job, \
												Assoc errObj)
							
							Assoc rtn = .InitErrObj("SetErrorOnJob")
							
							// *** save errMsg into the payload
							Assoc payload = job.getPayload()
							payload.errMsg = errObj.errMsg
							job.SetPayload(payload)
							
							// *** update job status 					
							Assoc status = .UpdateJobStatus(prgCtx, job, "error")
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end	
								
							return rtn					
						end

	
	public function void UpdateCompletedCount(Object prgCtx, \
															Object job, \
															Integer count)
															
							Assoc jobData = job.GetPayload()
							
							jobData.completedCount  = count
						    
						    job.SetPayload( jobData )
						    job.ThrottledSave( prgCtx, 5 )
						    
						end

	
	public function Assoc UpdateJobStatus(	Object prgCtx, \
														Object job, \
														String status)
							
							// *** set status							
							job.SetStatus(status)							
							
							Assoc result = .SaveJob(prgCtx, job)
							if !result.ok
								return .SetErrObj(.InitErrObj("UpdateJobStatus"), result)
							end
						
							Assoc rtn
							rtn.ok = true	
							return rtn
							
						end

end

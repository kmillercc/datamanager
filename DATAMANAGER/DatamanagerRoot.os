package DATAMANAGER

/**
 * This is a good place to put documentation about your OSpace.
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 */
public object DatamanagerRoot

	public		Object	Globals = DATAMANAGER::DatamanagerGlobals



	
	public function Boolean IsRequired(Object prgCtx)
			
				Assoc	checkVal
				Boolean	isRequired = TRUE
				
				if ( IsDefined( prgCtx ) && IsNotError( prgCtx ) )
				
					checkVal = $LLConfig.ConfigPkg.Get( prgCtx, { 'Settings_SysAdmin_Item_SkipValidationForTableKeyLookup' }, FALSE, TRUE )
			
					if ( checkVal.ok == TRUE )
				
						isRequired = !checkVal.values.Settings_SysAdmin_Item_SkipValidationForTableKeyLookup
				
					end
					
				end
				
			
				return isRequired		
			end

	script Startup
	
			
					
						//
						// Initialize globals object
						
						//
						
						Object	globals = $Datamanager = .Globals.Initialize()
						
						// *** make a temp for object factory
						globals.ObjectFactory = globals.ObjectFactory.New()
						
						
						// *** make a temp for NodeValidateWrapper
						globals.ValidateWrapper = OS.NewTemp(globals.ValidateWrapper)
						
						// *** make a temp for ApiDataObj
						globals.ApiDataObj = OS.NewTemp(globals.ApiDataObj)
						
						//
						// Initialize objects with __Init methods
						//
						
						$Kernel.OSpaceUtils.InitObjects( globals.f__InitObjs )
						
						
						echo( Str.Format( 'Module: %2 (%3)%1Author: %4%1Company: %5%1Creation Date: %6%1', Str.EOL(), 'datamanager', 'Data Manager', 'Kit Miller', 'Syntergy', '2015/02/13' ) )
						
						// *** create a temp ModulePrefs object
						globals.ModulePrefs = OS.NewTemp(globals.ModulePrefs)
						
						// *** create a temp ProcessController to allow for caching objects
						globals.ProcessController = Os.NewTemp(globals.ProcessController)
						globals.ProcessController.Init()
						
						// *** create a new schedJobs API and init the maps
						globals.schedJobsAPI = Os.NewTemp(globals.schedJobsAPI)
						globals.schedJobsAPI.Init()
								
				
			
		
	
	endscript

	
	public function void scratch(Object prgCtx = $PrgSessions[1])
			
			Assoc a
			
			a.('$ADVVERSIONING')=''
			a.('$CATALOG')='0'
			a.('$CREATEDATE')='2018-10-02T14:52:35'
			a.('$CREATOR')='Admin'
			a.('$DESCRIPTION')='Clear'
			a.('$MODIFYDATE')='2019-12-19T12:06:32'
			a.('$NICKNAME')=''
			a.('$OBJECTID')='177032'
			a.('$OBJECTNAME')='0542KA0030-P-FP 4003.101 (EN)'
			a.('$OBJECTTYPE')='144'
			a.('$TARGETPATH')=':Enterprise:Docs:0542KA0030-P-FP 4003.101 (EN)'
			
			a.('Livelink Categories:Author, Title, Date:Keywords') = ''
			
			String json = $SyntergyCore.JSON.ToJSON( a, true)
			
			echo(json)
		
			json = $SyntergyCore.JSON.ToJSON( a, false)
			
			echo(json)
											 
		end

end

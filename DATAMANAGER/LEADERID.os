package DATAMANAGER

public object LEADERID inherits DATAMANAGER::UserGroup

	override	String	fApiKey1 = 'LEADERID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$LEADERID'


	
	override function Assoc ValidateVal(Object prgCtx, \
												Dynamic val, \
												String action, \
												Frame apiDataObj)										
				Assoc rtn
				
				switch Type(val)
					case IntegerType
						// TODO validate this
					end
					
					case StringType
						Assoc status =  ._UserIDFromName(prgCtx, val, UAPI.USER)
						if !status.ok
							.SetErrObj(rtn, status)
							return rtn
						else
							val = status.result
						end	
					end
				end	
				
				rtn.result = val
				rtn.skipNextTime	= false
				rtn.ok = true
				return rtn
									
			end

end

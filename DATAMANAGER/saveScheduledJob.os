package DATAMANAGER

public object saveScheduledJob inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'optionStr', -1, 'optionStr', FALSE }, { 'type', -1, 'type', FALSE }, { 'subType', -1, 'subType', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 * 	optionStr 	=> String 	=> Options to control what happens	 
							 *	type		=> String	=> load or export 
						 	 *	subType		=> String	=> loadcsv or exportobjects
						     **/
						
							// *** parse the options
							Assoc 	options = ._ParseOptions(r.optionStr, true)
							Assoc	status
							
							// *** put some things into the options
							options.jobtype = r.type
							options.jobsubtype = r.subType
						
							// *** hand off to the API	
							SWITCH Str.Upper(r.type)
							
								CASE 'LOAD'
									// *** KM 7/13/16 disable the time zone offset
									// *** IsFeature(r, 'tzOffset') ? r.tzOffset : 0		
									status = $DataManager.LoadJobsAPI.AddEditItem(prgCtx, options, true, 0)
								END
								
								CASE 'EXPORT'
									// *** KM 7/13/16 disable the time zone offset
									// *** IsFeature(r, 'tzOffset') ? r.tzOffset : 0		
									status = $DataManager.ExportJobsAPI.AddEditItem(prgCtx, options, true, 0)
								END
							
							END	
							
							// *** figure out if we were successful
							if status.ok
								.fContent = synCore.SuccessResult(status.result)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
								.fError = rtn
							end	
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{"optionStr", StringType, "optionStr", FALSE }, \	
									{"type", StringType, "type", FALSE }, \	
									{"subType", StringType, "subType", FALSE } \
								}
								
							
						end

end

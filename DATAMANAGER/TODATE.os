package DATAMANAGER

public object TODATE inherits DATAMANAGER::PhysObjectsSpecTab

	override	String	fApiKey4 = 'poToDate'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$TODATE'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							Assoc rtn
							
							// *** val should come in as date. Convert to String
							val = Str.ValueToString(val)
							
							rtn.result = val
							rtn.skipNextTime	= false
							rtn.ok = true
							return rtn
												
						end

end

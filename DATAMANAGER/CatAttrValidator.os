package DATAMANAGER

public object CatAttrValidator inherits DATAMANAGER::LoadObjects

	public		Dynamic	_fCache
	public		Assoc	_fDataAssoc
	public		String	fApiKey1 = 'attrVals'
	override	Boolean	fEnabled = TRUE


	
	public function Assoc GetCatId(Object prgCtx, String colName, String catPath)
			
				Assoc 	rtn
				Assoc 	status
				
				// *** can we get the catId from cache
				Integer catId = ._CacheGet("categoryIds", catPath)
				if IsUndefined(catId)
					status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, catPath, {"Content Server Categories", "Livelink Categories"})
					if status.ok
						DAPINODE catNode = status.result
						catId = catNode.pId	
						
						._CachePut("categoryIds", catPath, catId)
					
					else
						return .SetErrObj(.InitErrObj("GetCatId"), Str.Format("AttachedCat Category path %1 is invalid.  Category cannot be found", colName))
					end	
				end
				
				rtn.result = catId
				rtn.ok = true
			
				return rtn
			end

	
	public function Object New()
														
				Frame f = ._NewFrame()
			
				f._fCache = Assoc.CreateAssoc()
				f._fDataAssoc = Assoc.CreateAssoc()
				
				return f
				
			end

	
	public function Assoc ValidateAttrPath(Object prgCtx, String csvAttrPath)
														
				Assoc		rtn = .InitErrObj("ValidateAttrPath")
				Assoc		status
				Assoc		result
				Integer		catId, catVersionNum
				Assoc		attrDef, attrSetDef
				String		attrKey
				Assoc		catDef
				
				// *** parse the csvAttrPath to get the category node and attribute path
				status = ._ParseAttrPath(prgCtx, csvAttrPath)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				else
				
					catId = status.result.catId
					catDef = status.result.catDef
					catVersionNum = status.result.catVersionNum
					attrDef = status.result.attrDef
					attrSetDef = status.result.attrSetDef			
							
					if IsDefined(attrDef)
					
						// *** cache the result, but also put in a reference to the DefinitionID
						attrDef.fDefId = catId
						result.attrDef = attrDef			
			
						// *** is it an attribute inside a set?					
						if IsDefined(attrSetDef)
						
							// *** this attr key needs to be a list of data to signal the possibility that we need to add rows to attribute set
							Integer attrSetId = attrSetDef.ID 
							Integer cardinality = status.result.cardinality
							attrKey = Str.Format("%1.%2.%3.%4.%5", catId, catVersionNum, attrSetId, cardinality, attrDef.ID)
					
						// *** is it a straight up attribute?
						else
							attrKey = Str.Format("%1.%2.%3", catId, catVersionNum, attrDef.ID)
						end	
					
					// *** is it an attribute set (maybe with <clear> directive to remove all rows?)
					elseif IsDefined(attrSetDef)
						attrKey = Str.Format("%1.%2.%3.%4", "0_attrSet", catId, catVersionNum, attrSetDef.ID)
						
						// *** cache the result, but also put in a reference to the DefinitionID
						attrSetDef.fDefId = catId
						result.attrDef = attrSetDef
					
					else
						.SetErrObj(rtn, Str.Format("Error parsing attribute path '%1. Could not find attribute or attribute set from path.", csvAttrPath))
					end	
				end	
				
				result = Assoc.CreateAssoc()
				result.catId = catId
				result.attrDef = attrDef
				result.attrKey = attrKey
				result.catDef = catDef
				result.catVersionNum = catVersionNum
				rtn = .SuccessResult(result)
				
				// *** cache this before returning
				._CachePut('csvAttrPaths', csvAttrPath, rtn)
			
				return rtn
			end

	
	public function Assoc ValidateVal(Object prgCtx, String attrPath, Dynamic val, String action, Integer nodeID = undefined)
																					
		Assoc	status
		String	multiValDelim = ._fMultiValDelim
		Assoc	attrDef
		Integer	catId, catVersionNum
		String	attrKey
		List	vals
		Assoc	catDef
		Boolean	cacheResult
				
		// *** validate the attribute path
		status = ._CacheGet('csvAttrPaths', attrPath)
		if IsUndefined(status)
			return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("Cannot get attribute information from cache for attrPath %1", attrPath))
					
		elseif !status.ok
			return .SetErrObj(.InitErrObj("ValidateVal"), status)
					
		else
			catId = status.result.catId
			attrDef = status.result.attrDef
			catDef = status.result.catDef
	
			// *** KM -- need the parentAssoc for ADN TKL validation				
			attrDef.ParentAssoc = catDef
					
			attrKey = status.result.attrKey
			catVersionNum = status.result.catVersionNum
		end		
				
		// *** convert to a list using MV delimiter
		vals = Type(val) == StringType ? Str.Elements(val, multiValDelim) : {val}	
			
		// *** convert and check values
		Integer i		
		for i = 1 to Length(vals)
					
			// *** turn clear to undefined value
			if vals[i] in {"<clear>", "[clear]"}
				vals[i] = undefined
			end	
				
			// *** in all cases, an empty string means undefined
			if Type(vals[i]) == StringType && Length(Str.Trim(vals[i])) == 0
				vals[i] = undefined
			end		
		
			// *** KM 8-5-19 support for appending values
			if vals[i] in {"<append>", "[append]"}
				vals[i] = "<append>"
					
			// *** KM 1-2-20 support for appending unique values
			elseif vals[i] in {"<appendunique>", "[appendunique]"}
				vals[i] = "<appendunique>"
	
			// regular value that we will set
			elseif IsDefined(vals[i])
				// *** do any necessary conversions on the value
				status = ._ConvertVal(prgCtx, attrDef, vals[i])
				if status.ok
					vals[i] = status.result
				else
					return .SetErrObj(.InitErrObj("ValidateVal"), status)
				end	
			
				// *** check the value
				status = ._CheckAttrValue(prgCtx, attrDef, vals[i], nodeID, catID)
				cacheResult = status.cacheResult
				if !status.ok
					Assoc retVal = .SetErrObj(.InitErrObj("ValidateVal"), status)
					retVal.cacheResult = cacheResult
					return retVal
				end	 		
			end	
		end
				
		Assoc 	rtn
		Assoc result
		result.attrKey = attrKey
		result.val = vals
		result.catId = catId
		result.catDef = catDef
		result.catVersionNum = catVersionNum
		rtn.result = result
		rtn.cacheResult = cacheResult
		rtn.ok = true
		return rtn
									
	end

	
	private function Dynamic _CacheGet(String key1, Dynamic key2)
														
				Assoc cache = ._fCache
				
				if IsFeature(cache, key1) && IsFeature(cache.(key1), key2)
					return cache.(key1).(key2)
				else
					return undefined
				end	
				
			end

	
	private function Void _CachePut(String key1, Dynamic key2, Dynamic val)
			
				Assoc cache = ._fCache
				
				if !IsFeature(cache, key1)
					cache.(key1) = Assoc.CreateAssoc()
				end	
				
				cache.(key1).(key2) = val
			
			end

	
	private function Assoc _CheckAdnTKL(Object prgCtx, \ 
												Assoc defAssoc,
												Assoc dataAssoc,
												Dynamic value, \
												Integer index = 1)
				Assoc		catInfo					
				Assoc		rtnval			
				Assoc		tklResult		
				Dynamic		apiError		
				Integer		i				
				Object		llAttr			
				String		errMsg			
				String		fieldName		
		
				Boolean		ok				 = TRUE
				Boolean		savedValue		 = FALSE
				
				
				// **** KM 7-16-2019 all of this is OT code that they commented out
				
				// Commented below logic to fix the LPV-1030, we have an issue with the SET and MultiValued TKL validation, CS core module still not passing the latest values in Tablevalues.		
				
				//LPV-783
				//To validate TKL attribute value, when a category with ADN-TKL attribute is applied to a folder, if validate entry flag is checked
				// We get callback for each set and Root level of category , so we need to validate at set level only.
		
				//LPV-783: defAssoc has the details of tkl and defAssoc.ParentAssoc has the value for all the attributes of category at set level.
				// callback is coming on every action of category and in some cases prgCtx is null, so we have to bypass the validation if prgCtx is null
				
				// EDM-1807 Validate only if the value is defined and if it is not same as the previously saved value 
				/*
				if ( IsDefined( value ) && IsDefined( index ) && IsDefined( dataAssoc.Values ) && Length( dataAssoc.Values ) > 0 ))
					   
					   if ( IsDefined( dataAssoc.Values[ index ] ) )
					   
					   		if ( value != dataAssoc.Values[ index ] )
					   		
					   			savedValue = FALSE
					   		
					   		end
					   		
					   else
					   	
					   		savedValue = FALSE
					   
					   end
					   
				end
				*/
		
				if ( IsDefined( prgCtx ) && IsFeature( defAssoc, "ParentAssoc" ) && IsDefined( value ) && !savedValue )
				
					catInfo = defAssoc.ParentAssoc
		
					// bypass the validation if user clicks on dropdown button of tkl
		
					if ( !IsFeature( defAssoc, "calledFromGetTKLResults" ) || !IsDefined( defAssoc.calledFromGetTKLResults ) )
		
						if !( defAssoc.ManualEntry == true && defAssoc.ValidateEntry == false )
		
							if ( IsDefined( catInfo ) && IsFeature( catInfo, "Children" ) && IsDefined( catInfo.Children ) )
		
								if ( defAssoc.Type == 10130 || defAssoc.Type == 10135 )
		
									fieldName = Str.Format( '_1_1_%1_1', defAssoc.ID )
									llAttr = $LLIApi.LLAttributeSubsystem.GetItem( defAssoc.Type )
		
									if ( IsFeature( catInfo, "ValueTemplate" ) )	// callback received for root level tkl attribute
		
										tklResult = llAttr.GetTableValues( prgCtx, catInfo, defAssoc, fieldName )
		
									else	// callback received for set level tkl attribute
									
										if IsFeature( catInfo, "ParentAssoc" )
		
											fieldName = Str.Format( '_1_1_%1_1_%2_1', catInfo.ID, defAssoc.ID )
											tklResult = llAttr.GetTableValues( prgCtx, catInfo.ParentAssoc, defAssoc, fieldName )
										end
		
									end
		
									if ( tklResult.ok )
		
										ok = FALSE
		
										for i = 1 to Length( tklResult.TableValues )
		
											if ( value == Str.String( tklResult.TableValues[ i ][ 1 ] ) )
		
												ok = TRUE
		
												break
		
											end
		
										end
		
		
										if ( ok == FALSE )
		
											ok = FALSE
											errMsg = Str.Format(
														 [ADN_ERRMSG.InvalidValueSpecifiedForAttribute],
														 value,
														 catInfo.DisplayName,
														 defAssoc.DisplayName )
											value = undefined
		
										end
		
									end
		
								end	
		
							end	
		
						end	
		
					end	
		
				end	// End for LPV-1030
		
				if ( IsUndefined( rtnval.OK ) )
		
					rtnval.OK = ok
					rtnval.ErrMsg = errMsg
					rtnval.ApiError = apiError
		
				end
		
				return rtnval
				
			
			end

	
	private function Assoc _CheckAttrValue(Object prgCtx, Assoc attrDef, Dynamic val, Integer nodeID, Integer catID)
														
		// *** also set cached Flag to default true
		Assoc rtn = Assoc{'cacheResult': true, 'ok': true}
					
		// *** we need a dataAssoc.  Simulate one
		Assoc dataAssoc
		dataAssoc.ID = attrDef.ID
		dataAssoc.Values = {undefined}	
				
		// *** get the attribute object by type and try to set it
		Object attrObj = $LLIAPI.LLAttributeSubSystem.GetItem( attrDef.Type )
		Assoc status = attrObj.ValueSet(prgCtx, attrDef, dataAssoc, val)
		if !status.ok
			.SetErrObj(rtn, status)
			rtn.errMsg = Str.Format("Error on attribute value '%1' for attribute named '%2.' ", val, attrDef.DisplayName) + rtn.errMsg
			return rtn
		end	
				
		// KM 11-13-19 reset the ParentAssoc based on the updated attribute value
		Assoc catDef = attrDef.ParentAssoc
		if IsFeature(catDef, "ValueTemplate") && IsFeature(catDef.ValueTemplate, "Values") && Length(catDef.ValueTemplate.Values) > 0
					
			Integer attID
			for attID in Assoc.Keys(catDef.ValueTemplate.Values[1])
				if attID == dataAssoc.ID
					catDef.ValueTemplate.Values[1].(attID) = dataAssoc
					._CachePut("catDef", catID, catDef)
					break
				end
			end
		end				
				
		// KM 7-15/19 special handling for ADN TKL
		if attrDef.Type == 	10130
			// don't want to cache validation result of TKL
			rtn.cacheResult = false
			status = ._CheckAdnTKL(prgCtx, attrDef, dataAssoc, val)
			if !status.ok
				return .SetErrObj(rtn, status)
			end			
		end
				
		return rtn
				
	end

	
	private function Assoc _ConvertVal(Object prgCtx, Assoc attrDef, Dynamic val)
																					
				Dynamic outVal						
										
				SWITCH attrDef.Type
				
					case 10131		// *** ATTRTABLE Module Table Lookup Attribute
						// *** KM 11/3/16 added code below to ADNID attribute
						outVal = Type(val) != StringType ? Str.String(val) : val
					end
			
					case 10013		// *** ATTRTABLE Module Table Lookup Attribute
						// KM 10/2/14 added code below to handle attrtable string lookup value
						outVal = Type(val) != StringType ? Str.String(val) : val
					end
					
					case 10130		// *** Auto Document Numbering Module ADN Table Lookup String Attribute
						outVal = Type(val) != StringType ? Str.String(val) : val
					end
					
					case 10135		// *** Auto Document Numbering Module ADN Table Lookup Integer Attribute
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(val, IntegerType) : val
					end
					
					case 5 /* Boolean*/
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(Str.Lower(val), BooleanType) : val
					end
					
					case -1, 10	/*String, Multiline, StringPopup*/
						outVal = Type(val) != StringType ? Str.String(val) : val
					end
					
					case 11 /* Multiline */
						outVal = Type(val) != StringType ? Str.String(val) : val
						outVal = Str.ReplaceAll(outVal, "\r\n", Str.EOL())
					end
					
					case 109, 108	/* UniqueID and ReleaseNumber */
						outVal = Type(val) != StringType ? Str.String(val) : val
					end
					
					case 305	/* node select type */
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(val, IntegerType) : val			
					end
					
					case 2, 12		/*Integer and IntegerPopup*/
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(val, IntegerType) : val
					end
					
					case -7, 13		/*Date and DatePopup*/
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(val, DateType) : val
					end
					
					case 14, 19		/*User and UserPopup*/
					
						// *** try to convert to integer on the off chance that it came in as a userid converted to string (e.g. "982927")
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(val, IntegerType) : val
						if IsUndefined(outVal)
							outVal = val
						end
				
						// *** might have come in as a userid, but it's probably still a string username		
						if Type(outVal) == StringType
							Assoc status = ._UserIDFromName(prgCtx, outVal, undefined)
							if !status.ok
								return .SetErrObj(.InitErrObj("ConvertVal"), Str.Format("Cannot find the username '%1' in the system. Failed to convert to a valid userid.", val))
							else
								outVal = status.result
							end	
						end
						
					end
					
					case -4, 20		/*RealPopup*/
						outVal = Type(val) == StringType ? $DataManager.FormatPkg.ConvertStringToVal(val, RealType) : val
					end
				
				END
				
				if IsUndefined(outVal)
					return .SetErrObj(.InitErrObj("ConvertVal"), Str.Format("Could not convert the value '%1' to the appropriate type for attribute '%2' (type %3)", val, attrDef.DisplayName, attrDef.Type))	
				end
			
				Assoc rtn
				rtn.ok = true
				rtn.result = outVal
			
				return rtn
			end

	
	private function Assoc _GetAttrDef(	Assoc catDef, String attrPath)
																						
				Assoc 	rtn = .InitErrObj("_GetAttrDef")
				Assoc	attrDef, attrSetDef = undefined
				List	elements = Str.Elements(attrPath, ":")
				Assoc	result
				String	setName, attName
				Integer	cardinality = 1
												
				if Length(elements) == 2
					setName = elements[1]
					attName = elements[2]
					// *** attr set name can have the brackets and number indicating set occurrence
					if setName[-1] == "]"
						Integer frontBrackLoc = Str.Locate(setName, "[")
						Integer endBrackLoc = Str.Locate(setName, "]")
						if IsUndefined(frontBrackLoc) || IsUndefined(endBrackLoc)
							// *** error out
							.SetErrObj(rtn, Str.Format("Could not parse cardinality of attribute set with name '%1' in attribute path %2", setName, attrPath ))
							return rtn					
						end
						// *** KM 4/17/18 fixed this to allow for multi digits set indexes
						cardinality = Str.StringToInteger(setName[frontBrackLoc+1:endBrackLoc-1])
		
						setName = setName[1:frontBrackLoc-1]
						if IsUnDefined(cardinality)
							// *** error out
							.SetErrObj(rtn, Str.Format("Could not parse cardinality of attribute set with name '%1' in attribute path %2", setName, attrPath ))
							return rtn					
						end
					end
					for attrSetDef in catDef.Children			
						// *** look for match
						if attrSetDef.DisplayName == setName
							for attrDef in attrSetDef.Children
								if attrDef.DisplayName == attName
									result.attrSetDef = attrSetDef
									result.attrDef = attrDef
									result.cardinality = cardinality
									return .SuccessResult(result)
								end					
							end											
						end	
					end
				elseif Length(elements) == 1
					attName = elements[1]
					for attrDef in catDef.Children
						if attrDef.DisplayName == attName
							if attrDef.Type == $AttrTypeSet
								result.attrSetDef = attrDef
								result.attrDef = undefined
							else
								result.attrSetDef = undefined
								result.attrDef = attrDef				
							end
							return .SuccessResult(result)
						end
					end		
				else
					.SetErrObj(rtn, "Invalid attrPath " + attrPath)
					return rtn
				end
											
				// *** we did not find it. set the error
				.SetErrObj(rtn, Str.Format("Could not find attr definition from attribute path '%1'", attrPath))
				return rtn
											
			end

	
	private function Assoc _ParseAttrPath(	Object prgCtx, String csvAttrPath)
																						
				Assoc 		rtn = .InitErrObj("_ParseAttrPath")
				DAPINODE	catNode
				Assoc		status
				Assoc		catDef, attrDef, attrSetDef
				String		attrPath
				Integer		cardinality = 1
				String		path
				
				Assoc result = Assoc.CreateAssoc()
			
				// *** logically, all attribute paths passed in this function should point to an attribute (not category)
				// *** by that logic, lop off the last element in path
				List elements = Str.Elements(csvAttrPath, ":")
				Integer len = Length(elements)
				if Length(elements) > 1
					path = Str.Join(elements[1:len-1], ":")
					attrPath = ":" + elements[len]
				else
					// *** strange sitation
					path = csvAttrPath
				end	
				
				// *** try first to get from cache
				catNode = ._CacheGet('catNodes', path)
				
				// *** loop through and lop of parts of the path until we get just the path to category
				while IsUndefined(catNode) && Length(path)
					status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, path, {"Content Server Categories", "Livelink Categories"})
					if status.ok
						catNode = status.result
						
						// *** we got all the way to categories volume without finding it -- return errror
						if catNode.pSubtype != $TypeCategory
							.SetErrObj(rtn, Str.Format("Cannot find category from path '%1'", path))
							return rtn		
						else
							
							// *** cache it
							._CachePut('catNodes', path, catNode)
									
						end
						
					else
						Integer lastSepLoc = Str.RChr(path, ":")
						if IsDefined(lastSepLoc)
							attrPath = path[lastSepLoc:] + attrPath
							path = path[1:lastSepLoc-1]	
						else
							path=""
						end			
					end
				end
				
			
				// *** did we get it?
				if IsUndefined(catNode)
					.SetErrObj(rtn, "Could not get category from path " + "'" + csvAttrPath + "'" )
					return rtn	
				else
					// *** if we got it, the attrPath might have leading colon -- get rid of it
					if attrPath[1]==":"
						attrPath = attrPath[2:]		
					end	
				end
				
				// *** try to get cat definition from category id
				catDef = ._CacheGet('catDefs', catNode.pId)
				if IsUndefined(catDef)	
					status = $DataManager.CategoryAPI.GetCategoryDefinition(prgCtx, catNode.pId)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					else	
						catDef = status.result	
						._CachePut('catDefs', catNode.pId, catDef)
					end
				end	
				
				// *** try to get attr definition from category definition
				status = ._GetAttrDef(catDef, attrPath)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				else				
					attrDef = status.result.attrDef
					attrSetDef = status.result.attrSetDef
					cardinality = status.result.cardinality
				end		
				
				result.catId = catNode.pId
				result.catDef = catDef
				result.catVersionNum = catNode.pVersionNum
				result.attrDef = attrDef
				result.attrSetDef = attrSetDef
				result.cardinality = cardinality
				
				return .SuccessResult(result)			
				
			
			end

end

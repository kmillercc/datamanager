package DATAMANAGER

public object CLASSIFICATION inherits DATAMANAGER::Classifications

	override	String	fApiKey2 = 'classIds'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$CLASSIFICATION'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
													
							List classIds						
						
							List   classVals	
								
							if Type(val) == StringType  && Str.Locate(val, ._fMultiValDelim)
									classVals = Str.Elements(val, ._fMultiValDelim)
							else
								classVals = {val}
							end	
						
							for val in classVals
							
								if Str.Lower(val) in {"<clear>", "[clear]"}
							
									apiDataObj.SetValueFromKeys("metadata", "doClearClassifications", undefined, true)
						
								else
							
									Assoc status = ._CheckNodeReference(prgCtx, val)
									
									if !status.ok
										
										return .SetErrObj(.InitErrObj("ValidateSetVal"), status)
										
									elseif status.result.pSubtype != $TypeClassification
										
										return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format(Str.Format("'%1' does not point to a valid Classification in this system", val)))
								
									else
								
										classIds = List.SetAdd(classIds, status.result.pId)
								
									end	
								end	
							end
						
							
							Assoc rtn
							rtn.result = classIds
							rtn.ok = true
							return rtn
												
						end

end

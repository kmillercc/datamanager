package DATAMANAGER

public object OBTAINEDBYID inherits DATAMANAGER::PhysObjCirc

	override	String	fApiKey4 = 'fObtainedByID'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$OBTAINEDBYID'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckUser(prgCtx, val)
							
							if status.ok
								Record userRec = status.result
								
								// *** the obtained by id will need to be a string when we actually perform the update
								val = userRec.ID
								
								// *** also set the Borrowed By Name from this
								apiDataObj.SetValueFromCol("$OBTAINEDBYNAME", userRec.NAME, false, action)
								
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

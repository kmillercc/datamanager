package DATAMANAGER

public object VERSIONDESCRIPTION inherits DATAMANAGER::VERDESCRIPTION

	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERSIONDESCRIPTION'

end

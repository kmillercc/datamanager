package DATAMANAGER::ASYNCJOB

public object DataManagerAgent inherits LLAGENT::LLAgentObjects::Agents

	override	Integer	fAgentID = 1818
	override	Boolean	fEnabled = TRUE
	override	Boolean	fSchedulable = TRUE


	
	override function Assoc Execute()
				$DataManager.PingerHandler.Execute(.fPrgCtx)
				
				Assoc rtn
				rtn.ok = true
				return rtn		
			end

end

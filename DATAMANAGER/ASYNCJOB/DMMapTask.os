package DATAMANAGER::ASYNCJOB

public object DMMapTask inherits DISTRIBUTEDAGENT::TaskType::Map
	
	override	Integer	fID = 99

	/*
	* Find the task handler, and then hand off execution to _SubclassExecute to do task type specific operations.
	*
	* @param {Object} prgCtx
	* @param {Object} taskHandler
	* @param {Dynamic} dataPkg
	* @param {Record} taskRec
	*
	* @return {Assoc}
	* @returnFeature {Boolean} ok
	* @returnFeature {String} errMsg
	* @returnFeature {Dynamic} apiError
	* @returnFeature {Dynamic} result
	*/
	override function Assoc Execute( Object prgCtx, Object taskHandler, Dynamic dataPkg, Record taskRec )

		Assoc		rtnVal
		Boolean		done
		Dynamic		apiError
		Dynamic		checkVal
		Dynamic		result
		Dynamic		schedReturn
		String		errMsg

		Boolean		ok = TRUE
		Object		dbConnect = prgCtx.fDBConnect
		Integer		executionStatus = 0

		echoInfo("Executing Data Manager MapTask without big transaction")
		
		
		// KM 3-23-20 do not wrap in transaction
		// if ( dbConnect.StartTrans() == TRUE )

			DISTRIBUTEDAGENT::MapReducePkg.StartBulkOperation( prgCtx )
			schedReturn = Scheduler.RunThis( this, "_SubclassExecute", prgCtx, taskHandler, dataPkg, taskRec )
			DISTRIBUTEDAGENT::MapReducePkg.EndBulkOperation( prgCtx )

			if ( IsDefined( schedReturn ) && IsNotError( schedReturn ) )

				checkVal = schedReturn

				if ( checkVal.ok == TRUE )

					result = checkVal.result
					done = checkVal.done

					if ( IsDefined( result ) && IsFeature( result, 'ok' ) && IsDefined( result.ok ) && ( !result.ok ) )

						ok = FALSE
						executionStatus = $DISTRIBUTEDAGENT.STATUS_ERROR

						if ( IsFeature( result, 'errMsg' ) && IsDefined( result.errMsg ) )

							errMsg = result.errMsg

						end

						if ( IsFeature( result, 'apiError' ) && IsDefined( result.apiError ) )

							apiError = result.apiError

						end

						result.status = executionStatus
						result.ID = taskRec.id

					end

				else

					ok = FALSE
					executionStatus = $DISTRIBUTEDAGENT.STATUS_ERROR
					errMsg = checkVal.errMsg
					apiError = checkVal.apiError
					result = Assoc{ 'ok': ok, 'errMsg': errMsg, 'status': executionStatus, 'ID': taskRec.id }

				end

			else

				ok = FALSE
				executionStatus = $DISTRIBUTEDAGENT.STATUS_CRASH
				errMsg = [DISTRIBUTEDAGENT_ERRMSG.ScriptCrash]
				result = Assoc{ 'ok': ok, 'errMsg': errMsg, 'status': executionStatus, 'ID': taskRec.id }

			end

			/*
			if ( dbConnect.EndTrans( ok ) != TRUE )

				ok = FALSE
				errMsg = [LLIAPI_ERRMSG.UnableToEndTransaction]

			end

		else

			ok = FALSE
			errMsg = [LLIAPI_ERRMSG.UnableToStartTransaction_]

		end
		 */

		if ( !ok )

			$DistributedAgent.MailUtil.SendAdminMessage( prgCtx, errMsg )

		end

		if ( executionStatus == $DISTRIBUTEDAGENT.STATUS_CRASH )

			dbConnect.EndTrans( ok )	// in case there are pending transactions in _SubclassExecute()                      

			._HandleTaskError( prgCtx, taskRec, result, taskHandler )

		elseif ( executionStatus == $DISTRIBUTEDAGENT.STATUS_ERROR )

			._HandleTaskError( prgCtx, taskRec, result, taskHandler )

		else

			checkVal = ._SetTaskStatus( prgCtx, taskRec, done, taskHandler )

		end

		//
		// assign the return values now from the execution of the task
		//
		rtnVal.ok = ok
		rtnVal.errMsg = errMsg
		rtnVal.apiError = apiError
		rtnVal.result = result

		return rtnVal

	end
	
	

	/*
		* Perform task type specific operations.
		*
		* @param {Object} prgCtx			Program session object
		* @param {Object} taskHandler		Object that performs the task
		* @param {Dynamic} taskData			Data package for the task handler
		* @param {Record} taskRec			Information about the task
		*
		* @return {Assoc}
		* @returnFeature {Boolean} ok			FALSE if an error occurred, TRUE otherwise
		* @returnFeature {String} errMsg		Error message, if an error occurred
		* @returnFeature {Dynamic} apiError		Application error, if applicable
		* @returnFeature {Boolean} done			Indicates if the task is done
		* @returnFeature {Dynamic} result		Results of the task execution
		*
		* @private
		*/
	override function Assoc _SubclassExecute( \
			Object		prgCtx, \
			Object		taskHandler, \
			Dynamic		taskData, \
			Record		taskRec )

		Dynamic		apiError
		Assoc		checkVal
		//Dynamic		dbResult
		String		errMsg
		Object 		mapper
		Long		parentTaskID
		Object		reducer
		Dynamic		result
		Assoc		rtnVal
		List		setList
		//String		stmt
		Dynamic		subTaskData

		Boolean		ok = TRUE
		Boolean		done = FALSE

		// Get the object required for map operations

		if ( ok )

			// KM 3-23-20 this is the only line changed. Changed to use our custom task type
			checkVal = $DISTRIBUTEDAGENT.TaskTypeSubsystem.GetValidItem( $Datamanager.fTaskTypeDMMap )

			if ( checkVal.ok )

				mapper = checkVal.item

			else

				ok = FALSE
				errMsg = checkVal.errMsg
				apiError = checkVal.apiError

			end

		end

		if ( ok )

			//				
			// Reset the fSubtaskCounter. If this task creates children, we will use this counter
			// to make the fingerprint unique (to avoid de-duplication).
			//

			.fSubtaskCounter = 1

			// Split the job and create a task for each child.

			checkVal = taskHandler.Split()

			if ( checkVal.ok )

				if ( Length( checkVal.set ) > 0 )

					setList = checkVal.set

					taskdata = setList[1]
					setList = setList[2:Length( setList ) ]
					taskHandler.fTaskdata = taskdata

				end


			else

				ok = FALSE
				errMsg = checkVal.errMsg
				apiError = checkVal.apiError

			end

			// Iterate through each chunk and schedule a job that is a child
			// of this top-level task.

			if ( ok )

				for subTaskData in set

					checkVal = mapper.Schedule( \
										taskHandler.fPrgCtx, \
										taskHandler.Key(), \
										taskRec.id, \
										subTaskData, \
										taskRec.priority )

					if ( checkVal.ok == FALSE )

						ok = FALSE
						errMsg = checkVal.errMsg
						apiError = checkVal.apiError

					end

				end

			end

		end

		//
		// Get the task ID of the parent of this task; tasks at the root will have -1 as a parent ID.
		//	

		if ( ok && taskHandler.fSimpleJob == FALSE )

			checkVal = ._GetParentTaskID( prgCtx, taskRec.id )

			if ( checkVal.ok )

				parentTaskID = checkVal.parentTaskID

			else

				ok = FALSE
				errMsg = checkVal.errMsg
				apiError = checkVal.apiError

			end

		end

		if ( ok )

			// Perform the map operation on the data set. 
			taskHandler.fTaskID = taskRec.id

			result = taskHandler.Map()

			if ( taskHandler.fSimpleJob == TRUE )

				done = TRUE

			else

				// Get the object required to perform reductions

				checkVal = $DISTRIBUTEDAGENT.TaskTypeSubsystem.GetValidItem( $DATaskTypeReduce )

				if ( checkVal.ok )

					reducer = checkVal.item

				else

					ok = FALSE
					errMsg = checkVal.errMsg
					apiError = checkVal.apiError

				end

				if ( ok )

					// Schedule a Reduce job, which replaces this task in the execution tree.
					checkVal = reducer.ScheduleReplacement( \
										taskHandler.fPrgCtx, \
										taskHandler.Key(), \
										parentTaskID, \
										taskRec.id, \
										result, \
										taskRec.Priority - 1, \
										Date.Now(), \
										taskRec.fingerprint )

					if ( checkVal.ok )

						done = TRUE

					else

						ok = FALSE
						errMsg = checkVal.errMsg
						apiError = checkVal.apiError

					end

				end

			end

		end

		rtnVal.ok = ok
		rtnVal.errMsg = errMsg
		rtnVal.apiError = apiError
		rtnVal.done = done
		rtnVal.result = result

		return rtnVal

	end

end

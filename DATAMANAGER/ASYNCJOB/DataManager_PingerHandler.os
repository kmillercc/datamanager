package DATAMANAGER::ASYNCJOB

public object DataManager_PingerHandler inherits LLAGENT::LLAgentObjects::PingerHandlers

	public		String	_fPkgName = 'datamanager'
	override	Boolean	fEnabled = TRUE
	override	Boolean	fOnlyAtMidnight = FALSE
	override	Integer	fPingerID = 12115

	override function Void Execute(Object prgCtx = .fPrgCtx)
						
				prgCtx = IsUndefined(prgCtx) ? $DataManager.SynCoreInterface.GetAdminCtx().prgCtx : prgCtx
			
				Object	synCore = $DataManager.SynCoreInterface
				Object 	log = synCore.GetLogger("datamanager")
				Object	statsPkg = $SyntergyCore.Stats
				
				synCore.ClearNDC()
				synCore.PushNDC("PingerHandler")
				
					statsPkg.Start( ._fPkgName, "PingerHandler" )	
				
					log.Info( "" )		
					log.Info( "" )
					log.Info( "-------------------------" )
					log.Info( "Starting " + .OSName + " @ " + Date.DateToString(Date.Now(), "%d-%b-%Y %I:%M:%S %p") )
					
					Assoc status = .ProcessScheduledJobs(prgCtx)	
			
					log.Info(" ")	
					if !status.ok
						log.Error(Str.Format("Error getting scheduled jobs. Result is %1.", status))
					else
						log.Info("Completed " + .OSName + " @ " + Date.DateToString(Date.Now(), "%d-%b-%Y %I:%M:%S %p. "))
						log.Info("Result is " + Str.String(status))
					end
				
					Real elapsed = statsPkg.Stop( ._fPkgName, "PingerHandler" )	
					log.Info( "{} executed in {} seconds.", { .OSName, elapsed } )
			
				synCore.ClearNDC()
					
				log.Info( "-------------------------" )	
				
			end

	
	public function ProcessScheduledJobs(Object prgCtx)
						
							Object	synCore = $DataManager.SynCoreInterface
							Assoc	rtn = synCore.InitErrObj("ProcessScheduledJobs")
							Object 	schedAPI = $DataManager.SchedJobsAPI
							Object	log = synCore.GetLogger("datamanager")
							Assoc	status
							Integer numJobs
							Integer	numFatalErrors = 0
						
							// *** get all scheduled jobs
							status = schedAPI.GetScheduledJobs(prgCtx)
							if !status.ok
								synCore.SetErrObj(rtn, status)
								return rtn
							else
								// *** ok no error. let's see if we have any jobs
								RecArray recs = status.result
								Record rec
								numJobs = Length(recs)
							
								log.Info(Str.Format("There are %1 jobs scheduled to run.", numJobs))
								
								for rec in recs
									
									if rec.ENABLED
										Assoc 	schedData = Assoc.FromRecord(rec)
										Assoc 	options = ._GetOptions(rec)
										Boolean hasFatalError = false
										
										log.Info("")
										log.Info(Str.Format("%1Processing job %2", Str.Tab(), rec))
										log.Info(Str.Format("%1Options are %2", Str.Tab(), options))				
										
										String iteratorType
										if IsFeature(options, "dataIdFilePath") && IsDefined(options.dataIdFilePath) && Length(Str.Trim(options.dataIdFilePath)) > 0 
											iteratorType = "dataids"							
										else
											iteratorType = 	options.jobSubType + "_" + "async"	
										end
	
									    // *** get the process controller
									    Object controller = $DataManager.ProcessController.New(options, options.jobType, options.jobSubType, options.action, "async", iteratorType) 
									    if IsUndefined(controller)
											numFatalErrors += 1
										    hasFatalError = true				
									    	log.Error(Str.Format("Cannot map process for %1 %2 %3 %4.", options.jobType, options.jobSubType, options.action, "async"))	
										else
							
											// *** hand off to ProcessController
											status = controller.RunAsync(prgCtx, Str.Upper(options.action), options, schedData)
											if !status.ok
												numFatalErrors += 1
											    hasFatalError = true				
												synCore.DumpErrorAssoc(status, log)
											end	 
												
											synCore.ClearNDC()			
											synCore.PushNDC("PingerHandler")				
										
										end		
														
										if hasFatalError
											log.Error(Str.Format("%1A Fatal Error has occurred.", Str.Tab()))
										end	
									else
										log.Info("")
										log.Info(Str.Format("%Skipping DISABLED job %2.", Str.Tab(), rec))				
									end	
										
								end				
							end	
						
							Assoc result
							result.("Number of Jobs Processed") = numJobs
							result.("Number of Jobs with Fatal Errors") = numFatalErrors
							return synCore.SuccessResult(result)
						
						end

	
	private function Assoc _GetOptions(Record rec)
						
							Assoc options  = Str.StringToValue(rec.options)
						
							options.("runAsUserName") = IsDefined(options.runAsUserName) ? options.runAsUserName : "Admin"			// *** default run as admin	
							options.("allowAnonymousUsers") = IsDefined(options.allowAnonymousUsers) ? options.AllowAnonymousUsers : true
							options.("allowPOUpdates") = IsDefined(options.allowPOUpdates) ? options.allowPOUpdates : true
							options.("allowRDUpdates") = IsDefined(options.allowRDUpdates) ? options.allowRDUpdates : true
							
							return options
						
						end

end

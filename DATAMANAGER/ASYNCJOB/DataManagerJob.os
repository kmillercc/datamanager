package DATAMANAGER::ASYNCJOB

/**
 * 
 * 
 * Handles the processing of load/export tasks
 * 
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 *  
 */
public object DataManagerJob inherits DISTRIBUTEDAGENT::DistributedAgentObjects::WorkerTask::JobChains

	override	Boolean	fEnabled = TRUE
	override	String	fKey = 'DataManagerJob'
	override	String	fName = 'DataManagerJob'
	override	Integer	fPriority = 99



	/**
				* This method will perform final reduction of the task results.
				* 
				* @param {List} childResults	Results from execution of child tasks
				*
				* @return {Assoc}
				* @returnFeature {Boolean} ok			FALSE if an error occurred, TRUE otherwise
				* @returnFeature {String} errMsg		Error message, if an error occurred
				* @returnFeature {Dynamic} apiError		Error object, if an error occurred
				*/
	override function Assoc Finalize(List		childResults )
												
					Assoc 		rtnVal
					Dynamic		apiError
				
					Object		prgCtx		= .fPrgCtx
					
					Dynamic		taskData	= .fTaskData.data
					
					// *** KM 1/30/17 moved this up to avoid stack trace
					if IsDefined(taskData)
					
						Object 	synCore = $DataManager.SynCoreInterface 
						Assoc	rtn = synCore.InitErrObj("Finalize")
						Object	log = $DataManager.SynCoreInterface.GetLogger()
				
						// *** get the process controller 
					    Assoc status = $DataManager.ProcessController.Get(prgCtx, taskData.jobId, taskData.action, "async", true, "finalize", taskData.logPath)
					
						if log.IsDebugEnabled()
							log.Debug("*****")
							log.Debug("DataManagerJob.Finalize childResults: " + Str.String(childResults))		
							log.Debug("DataManagerJob.Finalize taskData: " + $SyntergyCore.Utils.join( taskData, "," + Str.EOL() + Str.Tab() + Str.Tab() + Str.Tab() + Str.Tab()))
						end	
						
					    if !status.ok
					    	synCore.SetErrObj(rtn, status)
							synCore.DumpErrorAssoc(rtn, log)		
					    else
					    	Object 	dataCache = status.result.dataCache
					    	Object 	job = status.result.job
					    	Assoc	options = status.result.options
					    
					    	Object controller = status.result.controller
				
							// *** get the program context for this job
							status = synCore.GetAdminCtx(options.runAsUserName)
							if !status.ok
								synCore.SetErrObj(rtn, status)
								return rtn	
							else
								log.Info("Running as " +  options.runAsUserName)
								prgCtx = status.prgCtx
							end   
					    	
							// *** how many errors did we have?
							Integer nonFatalErrors = dataCache.Get(prgCtx, {'numberOfErrors'}, IntegerType, IntegerType, 0)
							Integer validationErrors = dataCache.Get(prgCtx, {'validationErrors'}, IntegerType, IntegerType, 0)
							
							log.Info(Str.Format("Number of errors = %1.", nonFatalErrors))
			
							// *** hand off to ProcessController
							status = controller.FinalizeAsync(prgCtx, taskData, taskData.action, taskData.jobId, dataCache, job, options, taskData.filesProcessed, nonFatalErrors, validationErrors)
							if !status.ok
								synCore.SetErrObj(rtn, status)
								synCore.DumpErrorAssoc(rtn, log)			
							end
							
							if log.IsDebugEnabled()
								log.Debug("DataManagerJob.FinalizeAsync returns" + Str.String(rtn))			
							end			
							
							// *** close the controller
							controller.Close()
						end	
						
						rtnVal.ok = rtn.ok
						rtnVal.errMsg = rtn.errMsg
						rtnVal.apiError = apiError
				
					else		
						rtnVal.ok = false
						rtnVal.errMsg = "Could not get TaskData. TaskData value is " + Str.String(.fTaskData)
					end
						
					return rtnVal
					
				end

	/*
				* This script will create a fingerprint string that will be used to uniquely identify tasks.  
				* This is created from the data in the dataPkg.  If two tasks in the worker system have the 
				* same finger print then the first one will be executed on and subsequent ones will be deleted  
				* e.g. rtnVal.fingerprint = Str.Format( "%1 dataid:%2", .Key(), dataPkg.DataID )
				*
				* @return {Assoc}
				* @returnFeature {Boolean} ok			FALSE if an error occurred, TRUE otherwise
				* @returnFeature {String} errMsg		Error message, if an error occurred
				* @returnFeature {Dynamic} apiError		Application error, if applicable
				* @returnFeature {String} fingerprint	A unique identifier for this task
				*/
	override function Assoc GetFingerprint() 
													
					Assoc 		rtnVal
					String		errMsg
					Dynamic		apiError
					String		fingerprint
				
					Dynamic		taskData	= .fTaskData
					Integer		max			= taskData.maxID
					Integer		min			= taskData.minID
					Integer		jobId		= taskData.jobId
					Boolean		ok			= TRUE
					
					
					if ( IsUndefined( min ) || IsUndefined( max ) )
					
						ok = FALSE
						errMsg = [DISTRIBUTEDAGENT_ERRMSG.RequiredParametersAreMissing]
						
					end
					
					if ( ok )
					
						fingerprint = Str.Format( "%1 jobId:%2;min:%3;max:%4", .Key(), jobId, min, max )
				
					end
				
					rtnVal.ok = ok
					rtnVal.errMsg = errMsg
					rtnVal.apiError = apiError
					rtnVal.fingerprint = fingerprint
				
					return rtnVal
				end

	/**
				* This method will do the task.
				* 
				* @return {Assoc}
				* @returnFeature {Boolean} ok			FALSE if an error occurred, TRUE otherwise
				* @returnFeature {String} errMsg		Error message, if an error occurred
				* @returnFeature {RecArray} errDetail	Detailed information about errors
				* @returnFeature {Assoc} data			Data returned from this job
				*/
	override function Assoc Map()
													
		Assoc 		rtnVal
		Assoc		data
		
		Dynamic		taskData	= .fTaskData
		Object		prgCtx		= .fPrgCtx
		List		itemNumbers = taskData.itemNumbers
		Assoc 		payload
		
		Object	log = $DataManager.SynCoreInterface.GetLogger()
		Object 	synCore = $DataManager.SynCoreInterface 
		Assoc	rtn = synCore.InitErrObj("Map")	
		
		// *** get the process controller 
	    Assoc status = $DataManager.ProcessController.Get(prgCtx, taskData.jobId, taskData.action, "async", true, "map", taskData.logPath)
	    if !status.ok
			synCore.DumpErrorAssoc(status, log)		
	    else
	    	Object controller = status.result.controller
	
			if log.IsDebugEnabled()
				log.Debug("*****")
				log.Debug("DataManagerJob.Map " + $SyntergyCore.Utils.join( taskData, "," + Str.EOL() + Str.Tab() + Str.Tab() + Str.Tab() + Str.Tab()))
			end
			
	    	Object 	dataCache = status.result.dataCache
	    	Object 	job = status.result.job
	    	Assoc	options = status.result.options
	    	
	    	// *** KM 3-3-17 single threaded/ensure csv order?
	    	Boolean runSingleThreaded = options.runSingleThreaded

	    	// log out who we're running the job as
			log.Info("Running as " +  options.runAsUserName)
	    	
			// *** get the program context for this job
			status = synCore.GetAdminCtx(options.runAsUserName)
			if !status.ok
				synCore.SetErrObj(rtn, status)
				return rtn	
			else
				log.Debug("Running as " +  options.runAsUserName)
				prgCtx = status.prgCtx
			end    	
	    
	    	Integer		counter		= 0
	    	Integer 	itemNo  
	    	Integer		N = Length(itemNumbers)
	    	Integer		updateIncrement = Math.Min(Math.Max(1,N/10), 20)
		
	    	  
	   		for itemNo in itemNumbers
	
				// *** hand off to ProcessController
				status = controller.ProcessItem(prgCtx, taskData.action, itemNo, dataCache, options)
				if !status.ok
					synCore.DumpErrorAssoc(status, log)
				end	 	
				counter += 1
				
				
				// *** need to provide updates for single-threaded jobs every X items
				if runSingleThreaded && ((counter % updateIncrement == 0) || counter==N)
					synCore.ClearNDC()
					synCore.PushNDC( "DataManagerProcessLog" )
					log.Debug(Str.Format("Running Single-threaded. Updating completed count to %1", counter))
					synCore.ClearNDC()
	
					// *** update now
					status = job.Load(prgCtx)
					if status.ok
						payload = job.GetPayload()
						payload.completedCount = counter
						job.SetPayload(payload)
						
						// *** put the payload back into job and save
						$DataManager.JobInterface.SaveJob(prgCtx, job)						
					end		
				end
				
			end
			if !runSingleThreaded
				// *** update now
				status = job.Load(prgCtx)
				if !status.ok
					synCore.SetErrObj(rtn, status)
					controller.Close()
					return rtn
				else
					payload = job.GetPayload()
					payload.completedCount += counter
					job.SetPayload(payload)
					
					synCore.ClearNDC()
					synCore.PushNDC( "DataManagerProcessLog" )
					log.Info("")
					log.Info(Str.Format("Running Multi-threaded. Updating completed count to %1", payload.completedCount))
					log.Info("")
					synCore.ClearNDC()
		
					// *** put the payload back into job and save
					status = $DataManager.JobInterface.SaveJob(prgCtx, job)
					if !status.ok
						synCore.SetErrObj(rtn, status)
						controller.Close()				
						return rtn
					end							
				end	
			end
	
			// *** close out logs, etc.	
			// *** KM 1-4-15 don't close out of the log because we may need it later for this thread
			controller.Close(false)
			
		end
		
		// *** set up the data we'll need for the finalize step
		data = taskData
		
	
		rtnVal.ok = rtn.ok
		rtnVal.errDetail = rtn.errMsg
		rtnVal.errMsg = rtn.errMsg
		rtnVal.data = data
	
		if log.IsDebugEnabled()
			log.Debug(Str.Tab() + "Result: Success" + " Return value is " + Str.String(rtnVal))
			log.Debug("")	
		end
		
	 
		return rtnVal	
	end

	/*
			* Split the initial dataset into a list of data sets which will be mapped.
			*
			* @return {Assoc}
			* @returnFeature {Boolean} ok			FALSE if an error occurred, TRUE otherwise
			* @returnFeature {String} errMsg		Error message, if an error occurred
			* @returnFeature {Dynamic} apiError		Application error, if applicable
			* @returnFeature {List} set				List of data sets to be mapped
		*/
	override function Assoc Split()
		
			Object		synCore = $DataManager.SynCoreInterface											
			Object		log = synCore.GetLogger()
			Object		prgCtx		= .fPrgCtx
			Dynamic		taskData	= .fTaskData
			Assoc 		rtn
			Assoc		chunk			
			Record		rec	
			List		setList	
		
			// *** get controller -- in this case to handle logging
			Object controller = $DataManager.ProcessController.Get(prgCtx, taskData.jobId, taskData.action, "async", true, "split", taskData.logPath)
				
			if ( IsDefined( taskData.split ) && taskData.split == TRUE )
		
				log.Debug( "DataManagerJob: Already split the taskData.")
				setList = { taskData }
		
			else
				log.Info("*****")
				log.Info("DataManagerJob.Split " + $SyntergyCore.Utils.join( taskData, "," + Str.EOL() + Str.Tab() + Str.Tab() + Str.Tab() + Str.Tab()))	
					
				Assoc queryInfo = ._GetSplitQuery( taskData )
				
				log.Info("DataManagerJob.stmt=" + queryInfo.stmt)
				log.Info("DataManagerJob.subs=" + Str.String(queryInfo.subs))	
						
				// *** execute query								
				RecArray dbResult = synCore.ExecSql( prgCtx, queryInfo.stmt, queryInfo.subs )									
				
				if ( IsError( dbResult ) )		
					rtn.ok = FALSE
					rtn.errMsg = "Error getting rows from dataCache"
					rtn.apiError = dbResult
					rtn.set = undefined
					return rtn
				end	
				
				// build a list of assoces (chunks)
				Integer i = 1
				for rec in dbResult		
					log.Info(Str.Format("DataManagerJob.Split Row %1 = %2", i, Str.String(rec)))
		
					
					chunk = Assoc.CreateAssoc()
					chunk.jobId = taskData.jobId
					chunk.minID = rec.min
					chunk.maxID = rec.max
					chunk.split = true
					chunk.logPath = taskData.logPath
					chunk.action = taskData.action
				  	chunk.scheduleId = taskData.scheduleId
				  	chunk.total = taskData.total
					chunk.schedData = taskData.schedData
					chunk.jobType	= taskData.jobType
					chunk.extId = taskData.extId
					chunk.filesProcessed = taskData.filesProcessed
						
					chunk.itemNumbers = Str.StringToValue("{" + rec.IDs + "}")
					setList = { @setList, Assoc.Copy(chunk) }
					i+=1
				end						
		
				log.Info( Str.Format("DataManagerJob.Split: Success. Distributed Agent Job will be split into the above %1 tasks.", Length(setList)))
			end
		
			// *** clear up logs, etc.
			if IsDefined(controller)
				controller.Close()
			end
			
			// Successful return		
			rtn.ok = true
			rtn.errMsg = ""
			rtn.apiError = undefined
			rtn.set = setList
			
			return rtn
		end

	// Patched by Pat201903191
	private function Assoc _GetSplitQuery(Assoc taskData)
		
		Object		synCore = $DataManager.SynCoreInterface	
		Object		log = synCore.GetLogger()
		String		stmt
		List		subs
		
		// Otherwise, split by getting a comma-separated list of ids that fall into the buckets.  					
		if synCore.IsMSSQL()	
			// *** build Sql statement
			// KM 3-19-19 query is so much simpler now						
			stmt = "SELECT " +\
					    "MIN(CAST(KEY2 AS INT) ) min, " +\
					    "MAX(CAST(KEY2 AS INT) ) max, " +\
						"IDs = STUFF " +\
					"( " +\
					    "( " +\
					        "SELECT ',' + KEY2 FROM DATAMANAGER_DATACACHE As T2 " +\
							"WHERE " +\
					            "JOB_ID = :A1 " +\
					        "AND " +\
					            "JOB_TYPE = :A2 " +\
					        "AND " +\
					            "KEY1 = :A3 " +\
							"AND T2.VAL_INT = T1.VAL_INT " +\
					        "ORDER BY CAST(KEY2 AS INT) " +\
					        "FOR XML PATH ('')), 1, 1, ''), " +\
							"VAL_INT as batch " +\
					"FROM DATAMANAGER_DATACACHE As T1 " +\
					"WHERE JOB_ID = :A4 AND JOB_TYPE = :A5 AND KEY1 = :A6 "+\ 
					"group by T1.VAL_INT " +\
					"order by T1.VAL_INT "	
						
			subs = 	{ taskData.jobId, taskData.jobType, taskData.dataCacheKey, taskData.jobId, taskData.jobType, taskData.dataCacheKey }												
		
		elseif synCore.IsOracle()	
			
			// *** build oracle statement
			// KM 3-19-19 query is so much simpler now
			stmt = "SELECT " +\
			    	"MIN(CAST(key2 AS INT) ) min, " +\
		    		"MAX(CAST(key2 AS INT) ) max, " +\
					"rtrim(XMLAGG(XMLELEMENT(c,key2 || ',') ORDER BY CAST(key2 AS INT) ).extract('//text()').getclobval(), ',') ids, " +\
			        "val_int AS batch " +\
			    "FROM " +\
			        "DATAMANAGER_DATACACHE " +\
			    "WHERE " +\
			            "job_id = :A1 " +\
			        "AND " +\
			            "job_type = :A2 " +\
			        "AND " +\
			            "key1 = :A3 " +\
			    "GROUP BY " +\
			        "val_int"
				        
			subs = 	{ taskData.jobId, taskData.jobType, taskData.dataCacheKey }
			
		else
			log.ERROR("Unknown database type!")
			
		end	
			
		Assoc result
		result.stmt = stmt
		result.subs = subs
			
		return result
		
	end
			
	/*
	* Create a sub-task of the current task.
	*
	* @param {Dynamic} subTaskData		Data for the sub-task
	* @param {Integer} priority			Task priority (0-100, higher is more important)
	* @param {Date} activationDate		Date after which the task may be activated
	*
	* @return {Assoc}
	* @returnFeature {Boolean} ok			FALSE if an error occurred, TRUE otherwise
	* @returnFeature {String} errMsg		Error message, if an error occurred
	* @returnFeature {Dynamic} apiError		Application error, if applicable
	*
	* @private
	*/
	override function Assoc _MapSubTask(Dynamic	subTaskData = Undefined, Integer priority = .fPriority, Date activationDate = Date.Now() )

		Assoc		rtnVal
		String		errMsg
		Dynamic		apiError
		Dynamic		checkVal
		Object		mapper

		Boolean		ok = TRUE
		Object		prgCtx = .fPrgCtx

		// KM 3-23-20 this is the only line changed. Changed to use our custom task type
		checkVal = $DISTRIBUTEDAGENT.TaskTypeSubsystem.GetValidItem( $Datamanager.fTaskTypeDMMap )

		if ( checkVal.ok == TRUE )

			mapper = checkVal.item

		else

			ok = FALSE
			errMsg = checkVal.errMsg
			apiError = checkVal.apiError

		end

		if ( ok )

			checkVal = mapper.Schedule(
										prgCtx,
										.Key(),
										$DISTRIBUTEDAGENT.fWorkerTaskID,
										subTaskData,
										priority,
										activationDate )

			if ( checkVal.ok == FALSE )

				ok = FALSE
				errMsg = checkVal.errMsg
				apiError = checkVal.apiError

			end

		end

		rtnVal.ok = ok
		rtnVal.errMsg = errMsg
		rtnVal.apiError = apiError

		return rtnVal

	end
end

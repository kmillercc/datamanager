package DATAMANAGER

public object LoadFromCsv inherits DATAMANAGER::Load

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'Load from CSV to Here'
	override	String	fQueryString = 'func=datamanager.index&targetId=%1#bl/loadcsv'

end

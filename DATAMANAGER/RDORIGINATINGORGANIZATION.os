package DATAMANAGER

public object RDORIGINATINGORGANIZATION inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsEstablishment'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDORIGINATINGORGANIZATION'
	override	String	fUpdateApiKey3 = 'fEstablishment'

end

package DATAMANAGER

public object RDUPDATECYCLEPERIOD inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.vitalRecordCycle'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDUPDATECYCLEPERIOD'
	override	String	fUpdateApiKey3 = 'fCyclePeriod'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							if Type(val) == StringType
								switch val
									case "Monthly"
										val  = 1
									end	
									case "Semi-Annual"
										val = 2
									end
									case "Quarterly"
										val = 3
									end	
									case "Annual"
										val = 12
									end	
									case "Weekly"
										val = 7
									end	
									case "Daily"
										val = 365			
									end	
									case 'None'
										val = 0
									end
									default
										return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("%1 is not a valid update cycle period", val))
									end
								end
						
							elseif Type(val) == IntegerType
							
								if !(val in {1,2,3,12,7,365,0})
						
									return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("%1 is not a valid update cycle period", val))
								end	
						
							else
								return .SetErrObj(.InitErrObj("_CheckGroup"), Str.Format("'%1' needs to be either an integer or string representing an Update Cycle Period", val))		
								
							end	
						
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

	
	private function String _GetUpdateCycleName(Integer num)
						
							String s = ''
						
							switch num
						
								case 1
									s = "Monthly"
								end
								
								case 2
									s = "Semi-Annual"
								end
								
								case 3
									s = "Quarterly"
								end
								
								case 7
									s = "Weekly"
								end
								
								case 12
									s = "Annual"
								end		
								
								case 365
									s = "Daily"
								end	
								
								default
									s = ""
								end
							
							end
												
							return s						
						
						end

end

package DATAMANAGER

public object checkFilePath inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'filePath', -1, 'FilePath', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
							 * 	filepath 		=> String 	=> the filepath we are validating
						     **/
						
							// *** hand off to the API
							Assoc status = $DataManager.FileUtils.CheckPathExists(r.filepath, "file")
							if status.ok
								.fContent = synCore.SuccessResult(true)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "filePath", StringType, "FilePath", FALSE } \
								}
						end

end

package DATAMANAGER

public object VERCREATOR inherits DATAMANAGER::Versions

	override	String	fApiKey3 = 'pOwner'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERCREATOR'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckUser(prgCtx, val)
							
							if status.ok
								val = status.result.ID
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

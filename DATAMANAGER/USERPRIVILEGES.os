package DATAMANAGER

public object USERPRIVILEGES inherits DATAMANAGER::UserGroup

	override	String	fApiKey1 = 'USERPRIVILEGES'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$USERPRIVILEGES'


	override script ValidateVal
	
			
					
							
									
											
													
												
														
															function Assoc ValidateVal(Object prgCtx, \
																						Dynamic val, \
																						String action, \
																						Frame apiDataObj)
																					
																Assoc rtn
																
																switch type(val)
																	case IntegerType
																		// no need to do anything -- should probably validate it though
																		// TODO: Validate this value		
																	end
																	
																	case StringType
																		Assoc privsAssoc = _GetPrivsAssoc(val) 
																		val = $LLIApi.UsersPkg.PrivsAssocToMask( privsAssoc )						
																	end
																end	
																
																rtn.result = val
																rtn.skipNextTime	= false
																rtn.ok = true
																return rtn
																					
															end				
															
															
															function Assoc 	_GetPrivsAssoc(String s)
															
																// *** setup an empty priviliege Assoc
																Assoc	privAssoc
																privAssoc.PrivLogin = false 
																privAssoc.PrivPermWorld = false
																privAssoc.PrivUsers = false
																privAssoc.PrivGroups = false
																privAssoc.PrivAdmin = false
																privAssoc.PrivLivelink = false
															
																// *** $privileges column is string (privname|privname|privname)
																List privList = Str.Elements(s, '|')
																String	priv
																
																for priv in privList
																
																	switch Str.Upper(priv)
																	
																		case "LOGINENABLED"
																			privAssoc.PrivLogin = TRUE
																		end
																		
																		case "PUBLICACCESS"
																			privAssoc.PrivPermWorld = TRUE
																		end
																		
																		case "CREATEUSERS"
																			privAssoc.PrivUsers = TRUE
																		end
																		
																		case "CREATEGROUPS"
																			privAssoc.PrivGroups = TRUE
																		end
																		
																		case "USERADMIN"
																			privAssoc.PrivAdmin = TRUE
																		end
																		
																		case "SYSTEMADMIN"
																			privAssoc.PrivLivelink = TRUE
																		end
																	end
																end
																
																return privAssoc
															
															end			
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

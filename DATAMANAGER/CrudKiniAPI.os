package DATAMANAGER

public object CrudKiniAPI inherits DATAMANAGER::CrudAPI

	
	override function Assoc _DbToUI(Record rec, Integer tzOffset=0)
						
							return rec.INIVALUE
						
						end

	
	override function Assoc _Delete(	Object prgCtx, \
												Dynamic key)
						
														
							Assoc 	rtn = .InitErrObj( "_Delete" )
						
							Dynamic result = CAPI.IniDelete( prgCtx.fDBConnect.fLogin, .fIniSection, key) 
							
							if IsError(result)
								.SetErrObj(rtn, result)
							else
								rtn = .SuccessResult(result)	
							end	
						
							return rtn
						 
						end

	
	override function Assoc _Get(Object prgCtx, \
											Dynamic key, \
											Dynamic dftValue = undefined)
						
							Assoc 	rtn = .InitErrObj( "_Get" )
						
							Dynamic result = CAPI.IniGet( prgCtx.fDBConnect.fLogin, .fIniSection, key, dftValue )
						 
							if IsError(result)
								.SetErrObj(rtn, result)
								return rtn
							end	
						
							return .SuccessResult(result)
						 
						end

	
	override function Assoc _List(Object prgCtx, Assoc filter)
						
							Assoc 	rtn = .InitErrObj( "_List" )
						
							Dynamic result = CAPI.IniList( prgCtx.fDBConnect.fLogin, .fIniSection) 
							
							if IsError(result)
								.SetErrObj(rtn, result)
							else
								rtn = .SuccessResult(result)	
							end	
						
							return rtn
						 
						end

	
	override function Assoc _Put(Object prgCtx, \
											Dynamic key, \
											Dynamic value, \
											Boolean update)
													
							Assoc 	rtn = .InitErrObj( "_Put" )
						
							Dynamic result = CAPI.IniPut( prgCtx.fDBConnect.fLogin, .fIniSection, key, value ) 
							
							if IsError(result)
								.SetErrObj(rtn, result)
							else
								rtn = .SuccessResult(result)	
							end	
						
							return rtn							
						 
						end

end

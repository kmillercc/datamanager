package DATAMANAGER

public object DESCRIPTION inherits DATAMANAGER::Dapi

	override	String	fApiKey3 = 'pComment'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$DESCRIPTION'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj) 
												
							Assoc rtn
							
							if IsDefined(val)
								val = Str.String(val)
								val = Str.ReplaceAll(val, "\r\n", Str.EOL())
							end
							
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

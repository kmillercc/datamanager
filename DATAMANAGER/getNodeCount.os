package DATAMANAGER

public object getNodeCount inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'dataid', 2, 'dataid', TRUE, Undefined }, { 'path', -1, 'path', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
						
							/**
							 * R may contain: 
							 * 	dataid 		=> Integer 	=> the dataid we are trying to retrieve
						 	 * 	path 		=> String 	=> (optional) the path of the node
							 *  limit			=> Integer	=> limit that tells the counter when to stop counting (prevents trying to count very large node trees)
							 *  includeRoot		=> Boolean	=>  Include the root in the count	 
						     **/
						
						
							// *** hand off to the API
							Assoc status = $DataManager.NodeUtils.NodeDescendantCount(.fPrgCtx, r.dataid, r.path, r.limit, r.includeRoot)
							if status.ok
								.fContent = synCore.SuccessResult(status.result)	
							else
								// for debugging .
								synCore.SetErrObj( rtn, status )
							
								.fError = rtn
							end		
							
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "dataid", IntegerType, "dataid", TRUE, undefined }, \
									{ "path", StringType, "path", FALSE } \			
								}
						end

end

package DATAMANAGER

public object ExportPrivilegesAPI inherits DATAMANAGER::PrivilegesAPI

	override	String	fIniSection = 'SyntBulkExporter.ExportPrivileges'


	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg	
							
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_EXPORT_MNG_PRIVS)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end		
						
							return rtn
						
						end

end

package DATAMANAGER

public object PhysObj inherits DATAMANAGER::ColumnValidators

	override	String	fApiKey1 = 'metadata'
	override	String	fApiKey2 = 'poData'
	override	String	fApiKey3
	override	String	fApiKey4


	
	override function Void __Init()
						
							if .fEnabled
								// **** set up a map to this object in the NodeValidateWrapper
								Object wrapper = $DataManager.ValidateWrapper
								if IsUndefined(wrapper._fColValidatorMap)
									wrapper._fColValidatorMap = Assoc.CreateAssoc()
								end
								wrapper._fColValidatorMap.(.fKey) = this	
						
						
								Object apiDataObj = $DataManager.ApiDataObj
								if IsUndefined(apiDataObj._fNodeApiKeysMap)
									apiDataObj._fNodeApiKeysMap = Assoc.CreateAssoc()
								end
								apiDataObj._fNodeApiKeysMap.(.fKey) = Assoc.CreateAssoc()
								
								apiDataObj._fNodeApiKeysMap.(.fKey).("CREATE") = 	{.fApiKey1, .fApiKey2, .fApiKey3, .fApiKey4}
								apiDataObj._fNodeApiKeysMap.(.fKey).("UPDATE") = 	{.fApiKey1, .fApiKey2, .fApiKey3, .fApiKey4}
						
						
							end
						
						end

end

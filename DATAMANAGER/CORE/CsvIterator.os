package DATAMANAGER::CORE

public object CsvIterator inherits DATAMANAGER::LoadIterators

	public		String	_fCurrentCsvFolderPath
	public		List	_fUsedIDList
	public		Integer	fCsvCount
	override	Integer	fIterateTotal
	override	Integer	fMaxID = 2
	override	Integer	fMinID = 1
	public		Integer	fRowIndex
	override	String	fSubKey = 'csv'


	
	public function Assoc CheckHeader(List cols)
														
				return ._fValidateWrapper.CheckHeader(cols)
			
			end

	
	override function Assoc CsvLineCallback(Integer lineNo, Assoc csvData, Assoc dataFlags)
															
				Assoc	rtn
				Assoc	status
				Object 	jobInterface = $DataManager.JobInterface
				Integer id = lineNo
				Object	apiDataObj				
				Integer chunkIndex
		
				// *** KM 7-9-2019 check for an action column
				if IsDefined(csvData.("$ACTION"))
					._SetDynamicAction(csvData, csvData.("$ACTION"), dataFlags)
				end	
		
				// *** KM 12-23-15 allow for an Order column
				if IsDefined(csvData.("$ORDER")) && Type(csvData.("$ORDER")) == IntegerType
					id = ._DetermineID(lineNo, csvData.("$ORDER"))
				end
				
				// *** increment the rowIndex now (used if we have more than 1 CSV file)
				.fRowIndex += 1
						
				// *** increment our total count
				.fIterateTotal +=1
				
				// ***  track the min and max ids
				if id < .fMinId || .fMinId==0
					.fMinId = id
				end
				if id > .fMaxId
					.fMaxId = id
				end		
				
				// *** adjust targetPath for file structure loads
				if Str.Lower(._fOptions.jobSubtype) == "loadfilestructure"
					status = ._AdjustTargetPath(id, csvData)
					if !status.ok
						._SubclassCallback(id, csvData)
						return .SetErrObj(.InitErrObj("CsvLineCallback"), status)
					end			
				end
				
				// *** Call the validate Wrapper now to validate certain fields (some validations must be done with all the csv values available)
				status = ._fValidateWrapper.ValidateLine(._fPrgCtx, ._fAction, lineNo, csvData, dataFlags, ._fOptions, undefined, ._fCurrentCsvFolderPath)
				if !status.ok
					._SubclassCallback(id, csvData)
					return .SetErrObj(.InitErrObj("CsvLineCallback"), status)
				else
					apiDataObj = status.result.apiDataObj
					
					// Get the group that this will belong to
					if ._fOptions.whenOption == "async"
						chunkIndex = ._GetChunkIndexAsync(csvData, apiDataObj)
					else
						chunkIndex = ._GetChunkIndex(csvData, apiDataObj)
					end	
		
					
					// *** how do we cache the data? Cache by id if this is load from csv, by sourcePath if this is load file structure 
					if Str.Lower(._fOptions.jobSubtype) == "loadfilestructure"
						
						String filePath = status.result.filePath
						
						// *** cache the data
						status = ._CacheData({.fFileDataKey, ._HashKey(filePath)}, status.result, chunkIndex)
						if !status.ok
							._SubclassCallback(id, csvData, chunkIndex)
							return .SetErrObj(.InitErrObj("CsvLineCallback"), "Error caching data: " + Str.String(status))
						end	
						
					else
						// *** cache the data
						status = ._CacheData({.fItemDataKey, id}, status.result, chunkIndex)
						if !status.ok
							._SubclassCallback(id, csvData, chunkIndex)
							return .SetErrObj(.InitErrObj("CsvLineCallback"), "Error caching data: " + Str.String(status))
						end		
						
					end	
				end		
				
				// *** update the job comleted count
				if ._fOptions.whenOption == "now" && Str.Lower(._fOptions.jobSubtype) != "loadfilestructure"
					jobInterface.IncrementCompletedCount(._fPrgCtx, ._fJob)	
				end			
				
				// *** let subclass add this to collection (or not)
				._SubclassCallback(id, csvData, chunkIndex)	
				
				rtn.ok = true
				return rtn
			end

	
	override function void InitSubclass(Assoc options, Assoc sourceSettings)
														
							if IsDefined(options.rootTargetPath) && Length(options.rootTargetPath) 
								._fRootTargetPath = options.rootTargetPath
							else
								._fRootTargetPath = ""
							end	
						
							// *** get the ValidateWrapper
							._fValidateWrapper = $DataManager.ValidateWrapper.New(._fPrgCtx, ._fAction, sourceSettings, ._fRootTargetPath, ._fApiProcess)
						
						
						end

	override script Iterate
	
			
					
																														
							// Patched by Pat201510151
							function Assoc Iterate(	Object prgCtx, \
													Dynamic notUsed, \
													String csvPath, \
													Boolean isRoot = true, \
													Assoc result = undefined)
						
								/*
									Iterates csv rows and calls a callback to process them
									
									Java Parser seems to be 3-4 times faster than the Oscript Parser
								*/			
								
								Assoc 	status
								Object  fileUtils = $DataManager.FileUtils
						
								if IsUndefined(result)
									result = Assoc.CreateAssoc()
									result.filesProcessed = {}
									result.dataFlags = Assoc.CreateAssoc()
									result.lineErrorMap = Assoc.CreateAssoc()
									result.numErrors = 0			
								end	
						
								// **** this might have come in as a folder path (async job)
								if File.IsDir(csvPath)
									
									String eachPath
								
									for eachPath in File.FileList(csvPath)
										if eachPath[-10:-1] == "_PROCESSED"
											.GetLogger().INFO(Str.Format("Skipping File '%1' because it is marked '_PROCESSED.'", eachPath))		
						
										elseif $DataManager.Utils.IsPending(eachPath)
											.GetLogger().INFO(Str.Format("Skipping File '%1' because it is marked '_PENDING.'", eachPath))		
				
										// **** KM 9-30-15 -- recurse one level down (BulkLoader dropoff folder style) 
										elseif fileUtils.IsCsv(eachPath) || (File.IsDirSpec(eachPath) && isRoot)
											status = Iterate(prgCtx, notUsed, eachPath, false, result)		
											if !status.ok	
												return .SetErrObj(.InitErrObj("Iterate"), status)
											else
												
												// *** incorporate results into the root results
												if IsDefined(status.result.dataFlags)
													result.dataFlags = _MergeFlags(result.dataFlags, status.result.dataFlags)
												end	
												if IsDefined(status.result.lineErrorMap)
													result.lineErrorMap = Assoc.Merge(result.lineErrorMap, status.result.lineErrorMap)
												end	
												result.numErrors += status.result.numErrors	
												
											end					
										end			
						
									end
								
								else
									if csvPath[-10:-1] == "_PROCESSED"
										
										.GetLogger().INFO(Str.Format("Skipping File '%1' because it is marked '_PROCESSED.'", csvPath))
									
									elseif $DataManager.Utils.IsPending(csvPath)
										
										.GetLogger().INFO(Str.Format("Skipping File '%1' because it is marked '_PENDING.'", csvPath))
				
									elseif fileUtils.IsCsv(csvPath)
									
										._fCurrentCsvFolderPath = fileUtils.GetParentPath(csvPath)
								
										// *** check that the csvPath exists.  Otherwise we will get an ugly error
										status = fileUtils.CheckPathExists(csvPath, "file")
										if !status.ok	
											return .SetErrObj(.InitErrObj("Iterate"), status)
										end	
										
										status = IterateJava(prgCtx, csvPath)
										if !status.ok	
											return .SetErrObj(.InitErrObj("Iterate"), status)
										else
											result.filesProcessed = {@result.filesProcessed, csvPath}					
											result = Assoc.Merge(result, status.result)
											
											if IsFeature(._fOptions, "csvFiles")
												// KM 4-22-19 put this into the options as well 
												._fOptions.csvFiles = List.SetAdd(._fOptions.csvFiles, csvPath)
											end	
											
										end		
									else
										
										return .SetErrObj(.InitErrObj("Iterate"), Str.Format("File path '%1' is not a CSV file", csvPath))
										
									end	
								
								end	
						
								return .SuccessResult(result)	
											
							end		
						
							function _MergeFlags(Assoc a1, Assoc a2)
								
								// *** we don't turn a true into a false -- just false into a true
								String key
								for key in Assoc.Keys(a2)
									if IsUnDefined(a1.(key)) || !(a1.(key))
										a1.(key) = a2.(key)
									end
								end
						
								return a1	
							end
						
						
							function Assoc IterateJava(	Object prgCtx, \
														String csvPath)
						
								/*
									Iterates csv rows and calls a callback to process them
								*/								
														
								Assoc 	rtn
								rtn.ok = true
								Assoc	status
								
								// *** increment our fCsvCount
								.fCsvCount += 1
								
								// *** if we're parsing a second, third, or nth CSV, pass in the .fRowCount
								Integer rowIndex = .fCsvCount > 1 ? .fRowIndex : 0
								
								// *** get the auditpath from the initialized options
								String auditPath = ._fOptions.auditFolderPath
						
								// *** parse the csv, passing in the callback
								status = $DataManager.CsvParser.ParseValidateFile(csvPath, auditPath, this, "CsvLineCallback", "ValidateSetVal", rowIndex) 
						
								// *** parse return										
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else
									rtn.result = status.result	
								end	
								
								return rtn									
							end	
					
				
			
		
	
	endscript

	
	public function Assoc ValidateSetVal(	Integer rowIndex, \
																String col, \
																Dynamic inVal)
						
						
							return ._fValidateWrapper.ValidateSetVal( rowIndex, col, inVal)
						end

	
	private function Assoc _AdjustTargetPath(Integer id, Assoc csvData)
														
							Assoc options = ._fOptions
							String filePath
							
							String sourcePath = csvData.("$SOURCEPATH")
							String fileName = csvData.("$FILENAME")
							
							// *** get the file path for this csv line
						 	Assoc status = ._fApiProcess.GetFilePath(options, fileName, sourcePath, ._fCurrentCsvFolderPath)
						 	if !status.ok
						 		return .SetErrObj(.InitErrObj("_AdjustTargetPath"), status)
						 	else
						 		filePath = status.result	
							end 		
						
							// *** get target path
							String objectName = csvData.("$OBJECTNAME")
							String targetPath = ._GetFileTargetPath(._fRootSourcePath, ._fRootTargetPath, filePath, objectName, options.targetPathType, options.includeRoot)
						
							// *** set the targetPath
							csvData.("$TARGETPATH") = targetPath
							
							// **** finally, set the targetPath using the apiDataObj
							._fValidateWrapper.ValidateSetVal(id, "$TARGETPATH", targetPath)	
						
							Assoc rtn
							rtn.ok = true
							
							return rtn
						
						end

	
	private function Integer _DetermineID(	Integer lineNo, Integer order)
															
			Integer id							
			List usedIdList = ._fUsedIDList
											
		
			if (order in usedIdList == 0)
				id = order
				._fSortChunk = true
			 
			elseif (lineNo in usedIDList == 0)
				id = lineNo
		
			else
				id = List.Max(usedIdList) + 1
				._fSortChunk = true
		
			end
				
			._fUsedIDList = {@._fUsedIDList, id}
			
			//echo(Str.Format("lineNo=%1, order=%2, id=%3", lineNo, order, id))
				
			return id
		end

	
	private function void _SetDynamicAction(Assoc csvData, String action, Assoc dataFlags)
			/*
			List validActions = {"CREATE",\
						"ADDVERSION",\
						"MOVE",\
						"COPY",\
						"PURGE",\
						"DELETE",\
						"UPDATE"}
			*/
		
			// set action
			._fAction = action
			._fValidateWrapper.SetAction(action)
				
				
			// *** get the ApiProcess and set it
			Object factory = $DataManager.ObjectFactory
			Assoc map
			map.("CREATE") = factory.GetObject("APIProcess", "Node.Create")	
			map.("ADDVERSION") = factory.GetObject("APIProcess", "Node.AddVersion")	
			map.("MOVE") = factory.GetObject("APIProcess", "Node.Move")	
			map.("COPY") = factory.GetObject("APIProcess", "Node.Copy")			
			map.("PURGE") = factory.GetObject("APIProcess", "Node.PurgeVersions")
			map.("DELETE") = factory.GetObject("APIProcess", "Node.Delete")		
			map.("UPDATE") = factory.GetObject("APIProcess", "Node.Update")	
			._fApiProcess = map.(action).New(._fOptions)		
				
			._fValidateWrapper.SetApiProcess(._fApiProcess)
				
		end

	
	private function Assoc _SubclassCallback(Integer id, Dynamic data, Integer chunkIndex = 1)
			
				return undefined													
			end

end

package DATAMANAGER::CORE

public object NodeBox inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeBox'
	override	String	fSubKey = 'Node.Box'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node, \
												Assoc thisActionFlags = actionFlags)		// *** allows for sending in flags independent of overall job options (for name collisions specifically)
						
				String auditStr
				Assoc status
				
				Object physObjService = $Service.ServiceRegistry.getItem( "PhysicalObjects" )	
				if IsUnDefined( physObjService )
					return .SetErrObj(.InitErrObj("PerformAction"), "Could not get PhysicalObjects web service")
				end
			
				Object physObjServiceTemp = OS.NewTemp( physObjService )
				physObjServiceTemp.fOutArgs = Assoc.CreateAssoc()
				physObjServiceTemp.fSocket = Undefined
				physObjServiceTemp.SetProgramSession( prgCtx )
			
				// *** remove from box?			
				if IsDefined(actionFlags.poRemoveFromBox)
					List 		ItemIDList = {node.pId}
					
					Integer 	BoxID = actionFlags.poRemoveFromBox
					status = physObjServiceTemp.PhysObjRemoveFromBox( ItemIDList, BoxID )
					if !status.ok
						OS.Delete( physObjServiceTemp )
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					end
					auditStr += "Removed From Box."
				end	
				
				// *** assign to box?
				if IsDefined(actionFlags.poAssignToBox)
					Assoc assignData = Assoc.CreateAssoc()
					assignData.fUpdateRSI = false
					assignData.fUpdateStatus = false
					assignData.fUpdateLocation = false
					assignData.fBoxID = actionFlags.poAssignToBox
					status = physObjServiceTemp.PhysObjAssignToBox( node.pId, assignData )
					if !status.ok
						OS.Delete( physObjServiceTemp )
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					end						
					auditStr += "Assigned To Box."								
				end
				
				
				// *** destroy temp obj
				OS.Delete( physObjServiceTemp )
				
				
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.node = node
				rtn.result.actionPerformed = auditStr
				
				return rtn
			
			end

end

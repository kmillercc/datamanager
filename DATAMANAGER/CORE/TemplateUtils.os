package DATAMANAGER::CORE

public object TemplateUtils inherits DATAMANAGER::Utils

	
	public function Assoc CreateObjectFromTemplate(	Object prgCtx, \
															String name, \
															Assoc metadataVals, \
															Integer subtype, \
															DAPINODE parentNode)
					
					Assoc 		rtn
					Assoc		status
					DAPINODE	node
				
					// *** we put the cacheID in the metadataVals, retrieve it now
					Integer cacheID = metadataVals.cacheID
					
					// *** first get the webNode
					Object webNode = $WebNode.WebNodes.GetItem(subtype)
					
					// *** marshall the parameters we need
					status = ._WizardFinish(prgCtx, \
											cacheID, \
											subtype, \
											parentNode, \	
											webNode)
				
					// *** finally, call the Action-Create2 (this is how the wizard does it)	
					if !status.ok
						return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)
					else
						node = status.result
					end	
						
					rtn.ok = true
					rtn.result = node											
				
					return rtn
				end

	
	public function Assoc HandleTemplateMetadata(Object prgCtx, \
														 Integer templateId, \
														 String name, \
														 Assoc metadataVals, \
														 Integer subtype, \
														 DAPINODE parentNode)
																
				Assoc 		rtn
				Assoc		status
				DAPINODE	node
				Integer		classID, cacheID
				Assoc		categoryFrame
				Assoc		cacheAssoc
				Assoc 		document3Classification
				
				// *** first get the webNode
				Object webNode = $WebNode.WebNodes.GetItem(subtype)
			
				// *** call wizard1 which well set things up for us 
				status = ._TemplateWizard1(prgCtx, webNode, templateID, name, subtype, parentNode)
				if !status.ok
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)
				else
					classID = status.result.classID
					templateID = status.result.TemplateID
					cacheID = status.result.cacheID
					
					// *** important, save the cacheID in the metadataVals assoc so we have it at create time
					metadataVals.cacheID = cacheID		
				end	
				
				// *** post the form from page 1 so we get our categories
				status = ._TemplateWizard1Post(prgCtx, templateID, classID, name, cacheID, subtype, parentNode, webNode)
				if !status.ok
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)
				else
					categoryFrame = status.result.categoryFrame	
				end	
				
				// *** now merge the values from csv into the category frame
				status = $DataManager.CatFrameAttrPrep.PrepareCategoryFrame(prgCtx, metadataVals.attrVals, categoryFrame)
				if !status.ok
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)
				end		
			
				// *** Load the cache data
				status = $LLIApi.CacheUtil.Load( prgCtx, cacheID )
				if !status.OK
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)		
				else
					cacheAssoc = status.ObjectValue
				end
				
				// *** calculate our classifications
				List classNodes = $Doctemplates.Util.CalcClassifications (prgCtx, parentNode.pID, subtype, templateID, classID, metadataVals.classIDs)
				
				// *** post classification data
				status = ._TemplateWizard2Post(prgCtx, cacheID, subtype, parentNode, webNode, classNodes)
				if !status.ok
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)
				else
					document3Classification = status.result
				end		
				
				// *** put modified category frame  and classifications back into the cache
				cacheAssoc.categoryFrame = categoryFrame
				cacheAssoc.Document3Classification = document3Classification
				status = $LLIApi.CacheUtil.Update ( prgCtx, cacheID, cacheAssoc )
				if !status.ok
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)
				end			
			
				rtn.ok = true
				rtn.result = node											
			
				return rtn
			end

	
	private function Assoc _GenerateDummyRequest(	Object prgCtx, \
															Integer subtype, \
															Integer parentID, \
															String objAction, \
															Integer stepID, \
															Object webNode, \
															Assoc additionalArgs = Assoc.CreateAssoc())
													
				Assoc request									
			
				// *** bundle up the stuff we need to call wizard objAction
				request.nextURL=''
				request.objAction=objAction
				request.objId=0
				request.objType=subtype
				request.parentId = Str.String(parentID)
				request.prgCtx = prgCtx
				request.volId=0
				request.webNode = webNode
				request.nextURL = Str.Format("func=ll&objID=%1&objAction=browse", parentID)
				request.SCRIPT_NAME = "livelink.exe"
				
					
				if IsDefined(stepID)
					request.stepID = stepID
				end
				
				// *** merge the additional args
				request = Assoc.Merge(request, additionalArgs)
					
				Assoc	componentParams
				componentParams.noAppearanceCache = TRUE
				componentParams.prgCtx = prgCtx
				request.componentParams = componentParams
			
				request.guiComponents = $WebNode.ComponentUtils.GetComponents( request, Assoc.CreateAssoc(), $WebNode.ComponentUtils.GetKeys( componentParams, prgCtx ), prgCtx )
				request.pageManager = $WebNode.DefaultPageManager._New()		
					
				return request
			end

	
	private function Assoc _GenerateParamsForCreate( Object prgCtx, \
																 DAPINODE parentNode, \
																 String name, \	
																 Integer subtype, \
																 Assoc metadataVals, \
																 Assoc response)
				
				Assoc status, cacheAssoc
				Dynamic request = Assoc.CreateAssoc()
				
				// *** we put the cacheID into the metadataVals
				Integer cacheID = metadataVals.cacheID
													 
				// *** Load the cache data
				status = $LLIApi.CacheUtil.Load( prgCtx, cacheID )
				if !status.OK
					return .SetErrObj(.InitErrObj("HandleTemplateMetadata"), status)		
				else
					cacheAssoc = status.ObjectValue
				end
				
				// *** This mimics the category popup window when doing a normal add
				Object handler = $Spdwizard.WizardSubsystem.GetItem( subtype, 1 )	
				Frame f = handler.GetCategoryFrameFromCache(prgCtx, cacheAssoc)	
				if IsDefined(f)
					Assoc result = f.UpdateCache()
					request.CREATE_CacheID = Str.ValueToString(result.CacheID)
				end	
				
				request.cacheID = cacheID	
				request.parentID = parentNode.pID
				request.name = name
				request.comment = IsFeature(metadataVals.dapiNodeData, "pComment") ? metadataVals.dapiNodeData.pComment : ''
			
				if IsFeature(metadataVals.dapiNodeData, "pCatalog") 
					request.catalogSetting = Str.String(metadataVals.dapiNodeData.pCatalog)
				end
				request.prgCtx = prgCtx
				request = Assoc.ToRecord(request)
				
				response.cacheAssoc = cacheAssoc
				response.data = Assoc.CreateAssoc()
				response.data.inheritNode = parentNode
				response.error = Assoc.CreateAssoc()
				response.error.detail = undefined
				response.error.message = undefined
				response.error.prefix= [WEBNODE_ERRMSG.ErrorAddingNewItem]
				response.nodeAllocOpts = Assoc.CreateAssoc()
				response.nodeAllocOpts.inheritNode = parentNode
				response.parent	= parentNode	
				
				// *** KM 11-12-17
				response.location = undefined
				
				Assoc params
				params.prgCtx = prgCtx
				params.request = request
				params.response = response	
				
				return params								 
			end

	
	private function void _SetClassArgs(Object prgCtx, \
															List classNodes, \
															Assoc args)
								
									// *** loop through our classification nodes and set some stuff for the request
									DAPINODE classNode
									for classNode in classNodes
										args.(Str.Format("hierarchy_%1", classNode.pID)) = 'checked'
										
										DAPINODE	rootnode =  $DocTemplates.Util.GetRootNode (prgCtx, classNode.pid)
										Assoc classVals
										for classVals in $DocTemplates.Util.GetChilds( prgCtx, classNode )
											if ( classVals.selectable ) && classVals.ID == classNode.pID
												args.(Str.Format("selected_%1", classNode.pID)) = Str.Format("{%1,%2,%3}", rootnode.pID, classNode.pID, classVals.ID)	
											end
										end		
									end
									
																			
								end

	
	private function Assoc _TemplateWizard1(Object prgCtx, \
														Object webNode, \
														Integer templateID, \
														String name, \
														Integer subtype, \
														DAPINODE parentNode)
						
							Assoc 	rtn, status
							Object 	webNodeAction
							Integer	classID, cacheID
							
							// *** first get the wizard webNode Action
							webNodeAction = webNode.Action("wizard")
							
							if IsUnDefined(webNodeAction)
								return .SetErrObj(.InitErrObj("_TemplateWizard1"), "Contract Management configuration error: Cannot find appopriate WebNodeAction to create from Template.")
							else
						
								Dynamic request = ._GenerateDummyRequest(prgCtx, subtype, parentNode.pID, "wizard", undefined, webNode)
							
								// *** get a new LL request handler temporary frame
								Object obj = Os.NewTemp( $WebDsp.RequestHandlerSubsystem.GetItem("LL") )
								request.handler = obj
								
								request = Assoc.ToRecord(request)	
								
								// *** call the wizard objAction
								status = webNodeAction.Execute(webNode, request)
								
								if ( IsDefined( status.response.error.message ) )
									return .SetErrObj(.InitErrObj("_TemplateWizard1"), status.response.error.message)
									
								elseif ( IsDefined( status.response.location ) && IsDefined(Str.LocateI(status.response.location, "wizarderror")) )
								
									// *** KM 11-12-17
									String errMsg = Str.Format("Wizard Error: %1", status.response.location)
									
									return .SetErrObj(.InitErrObj("_TemplateWizard1"), errMsg)
									
								else
									// *** default template id to this if we don't have one
									if IsUnDefined(templateID) && IsDefined(status.response.data.selectedTemplateId)
										templateID = status.response.data.selectedTemplateId
									end
								
									// *** default in classID in case we need it
									classID = status.response.data.selectedClassId
								
									// *** get the cacheID
									cacheID = status.request.CacheID		
								end		
							end
							
							Assoc result
							result.templateID = templateID
							result.classID = classID
							result.cacheID = cacheID
							rtn.result = result
							rtn.ok = true
							return rtn
						
						end

	
	private function Assoc _TemplateWizard1Post(Object prgCtx, \
															Integer templateID, \
															Integer classID, \
															String name, \
															Integer cacheID, \
															Integer subtype, \
															DAPINODE parentNode, \	
															Object webNode)
							Assoc 	rtn, status
							Object 	webNodeAction
							Assoc	categoryFrame, document1Create
							
							// *** first get the wizard webNode Action
							webNodeAction = webNode.Action("wizard2")
							
							if IsUnDefined(webNodeAction)
								return .SetErrObj(.InitErrObj("_TemplateWizard1Post"), "Contract Management configuration error: Cannot find appopriate WebNodeAction wizard2 to create from Template.")
							else
								
								Assoc args
								args.cacheID = cacheID
								args.nextStepID=10
								args.name = name
								args.creationType = Str.Format ( 'NewFromTemplate%1_%2', templateID, classId )
								args.templatelist = Str.String(classID)
								args.templateID = templateID
						
								Dynamic request = ._GenerateDummyRequest(prgCtx, subtype, parentNode.pID, "wizard2", 1, webNode, args)
								
								// *** get a new LL request handler temporary frame
								Object obj = Os.NewTemp( $WebDsp.RequestHandlerSubsystem.GetItem("LL") )
								request.handler = obj		
								request = Assoc.ToRecord(request)	
								
								// *** call the wizard objAction
								status = webNodeAction.Execute(webNode, request)
								if ( IsDefined( status.response.error.message ) )
									return .SetErrObj(.InitErrObj("_TemplateWizard1Post"), status.response.error.message)
						
								elseif ( IsDefined( status.response.location ) && IsDefined(Str.LocateI(status.response.location, "wizarderror")) )
								
									// *** KM 11-12-17
									String errMsg = Str.Format("Wizard Error: %1", status.response.location)
									
									return .SetErrObj(.InitErrObj("_TemplateWizard1Post"), errMsg)			
									
								else		
									categoryFrame = status.response.cacheAssoc.categoryFrame
									document1Create = status.response.cacheAssoc.Document1Create	
								end
								
							end
							
							Assoc result
							result.categoryFrame = categoryFrame
							result.document1Create = document1Create
							rtn.result = result
							rtn.ok = true
							return rtn
						
						end

	
	private function Assoc _TemplateWizard2Post(Object prgCtx, \
															Integer cacheID, \
															Integer subtype, \
															DAPINODE parentNode, \	
															Object webNode, \
															List classNodes) 
							Assoc 	rtn, status
							Object 	webNodeAction
							Assoc	classData
							
							// *** first get the wizard webNode Action
							webNodeAction = webNode.Action("wizard2")
							
							if IsUnDefined(webNodeAction)
								return .SetErrObj(.InitErrObj("_TemplateWizard1Post"), "Contract Management configuration error: Cannot find appopriate WebNodeAction wizard2 to create from Template.")
							else
								
								Assoc args
								args.cacheID = cacheID	
								
								._SetClassArgs(prgCtx, classNodes, args)
								
								Dynamic request = ._GenerateDummyRequest(prgCtx, subtype, parentNode.pID, "wizard2", 20, webNode, args)
								
								// *** get a new LL request handler temporary frame
								Object obj = Os.NewTemp( $WebDsp.RequestHandlerSubsystem.GetItem("LL") )
								request.handler = obj		
								request = Assoc.ToRecord(request)	
								
								// *** call the wizard objAction
								status = webNodeAction.Execute(webNode, request)
								if ( IsDefined( status.response.error.message ) )
									return .SetErrObj(.InitErrObj("_TemplateWizard2Post"), status.response.error.message)
									
								elseif ( IsDefined( status.response.location ) && IsDefined(Str.LocateI(status.response.location, "wizarderror")) )
								
									// *** KM 11-12-17
									String errMsg = Str.Format("Wizard Error: %1", status.response.location)
									
									return .SetErrObj(.InitErrObj("_TemplateWizard2Post"), errMsg)				
									
								else		
									classData = status.response.cacheAssoc.Document3Classification
								end
								
							end
							
							rtn.result = classData
							rtn.ok = true
							return rtn
						
						end

	
	private function Assoc _WizardFinish(Object prgCtx, \
													Integer cacheID, \
													Integer subtype, \
													DAPINODE parentNode, \	
													Object webNode)
							Assoc 		rtn, status
							Object 		webNodeAction
							DAPINODE	node
							
							// *** this is a hack -- we need to provide a dummy "ctxOut" for the BrowserBegone that gets called as part of the Wizard2.Finish function 
							String 		tempFilePath = $Kernel.FileUtils.GetTempDir() + Str.String(cacheID) + ".txt"
							File		ctxOut = File.Open(tempFilePath, File.WriteMode)
							
							// *** first get the wizard webNode Action
							webNodeAction = webNode.Action("wizard2")
							
							if IsUnDefined(webNodeAction)
								return .SetErrObj(.InitErrObj("_WizardFinish"), "Contract Management configuration error: Cannot find appopriate WebNodeAction wizard2 to create from Template.")
							else
								
								Assoc args
								args.nextStepID = -1
								args.cacheID = cacheID
								args.ctxOut = ctxOut
								
								Dynamic request = ._GenerateDummyRequest(prgCtx, subtype, parentNode.pID, "wizard2", 20, webNode, args)
								
								// *** get a new LL request handler temporary frame
								Object obj = Os.NewTemp( $WebDsp.RequestHandlerSubsystem.GetItem("LL") )
								obj.fHTMLData = obj
								request.handler = obj	
								request = Assoc.ToRecord(request)	
								
								// *** call the wizard objAction
								status = webNodeAction.Execute(webNode, request)
								if ( IsDefined( status.response.error.message ) )
									File.Close(ctxOut)
									File.Delete(ctxOut)
									return .SetErrObj(.InitErrObj("_WizardFinish"), status.response.error.message)
								
								elseif ( IsDefined( status.response.location ) && IsDefined(Str.LocateI(status.response.location, "wizarderror")) )
							
									// *** KM 11-12-17
									String errMsg = Str.Format("Wizard Error: %1", status.response.location)
								
									return .SetErrObj(.InitErrObj("_WizardFinish"), errMsg)							
									
								else			
									node = status.response.data.node
								end
								
							end
							
							if IsDefined(ctxOut) 
								File.Close(ctxOut)
								File.Delete(ctxOut)
							end
							
							rtn.result = node
							rtn.ok = true
							return rtn
						
						end

end

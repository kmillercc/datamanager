package DATAMANAGER::CORE

public object FileIterator inherits DATAMANAGER::LoadIterators

	override	String	fSubKey = 'files'


	
	override function Assoc FileCallback(Integer id, \
															String filePath, \
															Dynamic data, \
															Boolean isRoot = false)
															
							Assoc	rtn = .InitErrObj("FileIterator.FileCallback")
							Assoc	status	
							Assoc	options = ._fOptions
							Frame	apiDataObj
							Assoc	cacheData
							Boolean	hasMetadata = true
							Assoc	csvData
								
							// *** increment the counter
							.fIterateTotal+=1	
							
							// ***  track the min and max ids
							if id < .fMinId || .fMinId==0
								.fMinId = id
							end
							if id > .fMaxId
								.fMaxId = id
							end		
							
							// *** try to get metadata for the file
							status = .GetCachedData(._fPrgCtx, ._fDataCache, ._HashKey(filePath), .fFileDataKey)
							if status.ok
								cacheData = status.result
								apiDataObj = $DataManager.ApiDataObj.New(._fAction, cacheData.apiVals, cacheData.csvData)
							elseif options.requireMetadata
								._SubclassCallback(id, filePath, data)
								return .SetErrObj(.InitErrObj("FileCallback"), Str.Format("You have the 'Metadata Required' checkbox checked and no metadata was found for the file %1", filePath))
							else
								hasMetadata = false
								apiDataObj = $DataManager.ApiDataObj.New(._fAction)	
							end
							
							// *** if we don't have the metadata, then we have to get ParentNode
							if !hasMetadata	
								// *** set the sourcepath from the filepath that came in
								apiDataObj.SetValueFromCol("$SOURCEPATH", filePath)
								csvData.("$SOURCEPATH") = filePath 	
							
								// *** get target path
								String objectName = apiDataObj.GetValueFromCol("$OBJECTNAME")
								if IsUndefined(objectName)
									objectName = Str.ReplaceAll(File.GetName(filePath), File.Separator(), "")
								end	
								String targetPath = ._GetFileTargetPath(._fRootSourcePath, ._fRootTargetPath, filePath, objectName, options.targetPathType, options.includeRoot)
								apiDataObj.SetValueFromCol("$TARGETPATH", targetPath)
								csvData.("$TARGETPATH") = targetPath 
								
								Assoc dataFlags = ._GetDataFlagTemplate()
								dataFlags.hasFiles = true
								
								// *** Call the validate Wrapper now to validate certain fields (some validations must be done with all the csv values available)
								status = ._fValidateWrapper.ValidateLine(._fPrgCtx, ._fAction, id, csvData, dataFlags, ._fOptions, filePath, undefined, apiDataObj)
								if !status.ok
									._SubclassCallback(id, filePath, data)
									return .SetErrObj(.InitErrObj("FileCallback"), status)
								else
									cacheData = status.result
								end	
							else		
								csvData = apiDataObj.GetCsvData()
							end
							
							// Get the chunk index
							Integer chunkIndex 
							
							if ._fOptions.whenOption == "async"
								chunkIndex = ._GetChunkIndexAsync(csvData, apiDataObj)
							else
								// file loads need to run single threaded
								chunkIndex = 1
							end	
								
							// *** cache data
							cacheData.apiVals = apiDataObj.GetAssocVals()
							status = ._CacheData({.fItemDataKey, id}, cacheData, chunkIndex)
							if !status.ok
								._SubclassCallback(id, filePath, data, chunkIndex)
								.SetErrObj(rtn, "Error cacheing metadata: " + Str.String(status))
								return rtn
							end	
						
							// *** update the job comleted count
							if ._fOptions.whenOption == "now"	
								$DataManager.JobInterface.IncrementCompletedCount(._fPrgCtx, ._fJob)
							end			
							
							// *** let subclass add this object to the collection
							._SubclassCallback(id, filePath, data, chunkIndex)
							
							return rtn
							
						end

	
	override function Assoc GetCachedData(Object prgCtx, Object dataCache, Dynamic key2, Dynamic  key1 = .fItemDataKey, Integer dataType = Assoc.AssocType, Integer listItemType = StringType)
																					
							Assoc 	rtn = .InitErrObj("GetCachedData")
							List	keys = {key1, Str.String(key2)}	
							Dynamic	cacheVals
							
							// *** get from cache
							cacheVals = dataCache.Get(prgCtx, keys, dataType, listItemType)
							if IsUndefined(cacheVals)
								.SetErrObj(rtn, Str.Format("Could not get itemData for keys %1", keys))
								return rtn
							end
							
							rtn.result = cacheVals
							return rtn						
						end

	
	override function void InitSubclass(Assoc options, Assoc sourceSettings)
														
							._fFileMetadataObj = $DataManager.FileMetadataObj.New()
						
						
							// *** get the ValidateWrapper
							._fValidateWrapper = $DataManager.ValidateWrapper.New(._fPrgCtx, ._fAction, sourceSettings, ._fRootTargetPath, ._fApiProcess)
						end

	
	override function Assoc Iterate(Object prgCtx, Dynamic root = undefined, String path = undefined)
							/*
								Iterates Content Server nodes and calls a callback to process them
							*/								
													
							Assoc	status
							Assoc 	options = ._fOptions		
							Integer limit = ._fLimit
						
							List	excludedFileNames = {"auditlog.log", "_audit", "_audit" + File.Separator(), 'thumbs.db', 'Thumbs.db'}
							Assoc 	result
							result.filesProcessed = {}
							result.count=0
						
							// *** first we need to iterate through the CSV files and validate them
							status = ._ValidateFileStructureCsvFiles(prgCtx)
							if !status.ok
								return .SetErrObj(.InitErrObj("Iterate"), status)
							end	
						
							// *** include the root node?
							if options.includeRoot
								result.count+=1
								this.FileCallback(1, options.loadDirPath, undefined)
								result.filesProcessed = {@result.filesProcessed, options.loadDirPath}
							end	
							
							// *** all files/folders in this root folder get added to the files processed
							String eachFile
							for eachFile in File.FileList(options.loadDirPath)
								// *** KM 6/2/16 make sure that _PROCESSED files are not processed
								// *** KM 6/2/16 make sure that _PENDING files are not processed
								String fileName = File.GetName(eachFile) 
								if fileName != "_audit"	&& \
									fileName[-10:-1] != "_PROCESSED" && \
									Str.Lower(fileName) != 'thumbs.db' && \
									!($Datamanager.Utils.IsPending(fileName))
									result.filesProcessed = {@result.filesProcessed, eachFile}
								end	
							end
							
							// *** get a list of files, passing in callback
							status = $DataManager.FileUtils.FileListRecursive(options.loadDirPath, \
																limit, \
																false, \
																false, \
																excludedFileNames, \
																this, \
																"FileCallback", \
																result, \
																true)
																
							if !status.ok
								return .SetErrObj(.InitErrObj("Iterate"), status)
							end	
						
							Assoc rtn
							rtn.result = result
							rtn.ok = true
							
							return rtn						
						end

	
	override function Integer _GetNumCsvFiles()
							// *** default is 1
							return ._fFileMetadataObj.GetNumCsvFiles()
						end

	
	private function void _SubclassCallback(Integer id, String path, Dynamic data, Integer chunkIndex = 1)								
								
						end

	
	private function Assoc _ValidateFileStructureCsvFiles(Object prgCtx)
														
				List	excludedFileNames = {"auditlog.log", "_audit", "_audit" + File.Separator()}										
				
				Assoc status = $DataManager.FileUtils.FileListRecursive( ._fRootSourcePath, \
																		  ._fLimit, \
																		  true, \
																		  true, \
																		  excludedFileNames)
																		  
				if !status.ok
					return .SetErrObj(.InitErrObj("_ValidateFileStructureCsvFiles"), status)
				else	
					List 	files = status.result.files
					String 	csvPath
					Assoc	sourceSettings
					
					// *** ensure dir separator for root source path
					._fRootSourcePath = $DataManager.FileUtils.EnsureDirSeparator(._fRootSourcePath)
					
					
					for csvPath in files
					
						// *** Instantiate the Iterator as a frame now and call it
						Object iterator = $DataManager.CsvIterator.New(prgCtx, \
																		._fJob, \
																		._fAction, \
																		._fLimit, \
																		._fDataCache, \
																		._fOptions, \
																		._fRootSourceId, \
																		._fRootSourcePath, \
																		._fRootTargetId, \
																		._fRootTargetPath, \
																		1, \
																		sourceSettings, \
																		._fApiProcess)
																		
					
						// *** now iterate through the items and process them
						status = iterator.IterateWrapper(prgCtx, undefined, csvPath, false)
						if !status.ok
							return .SetErrObj(.InitErrObj("_ValidateFileStructureCsvFiles"), status)
						
						elseif status.result.numErrors > 0
							
							Integer numErrors = status.result.numErrors
							String msg
							
							if numErrors > 1
								msg = Str.Format("%1 rows in the metadata file '%2' had errors.  Please refer to the log files in the audit folder for more information.", status.result.numErrors, csvPath)
							else
								msg = Str.Format("One row in the metadata file '%1' had errors.  Please refer to the log files in the audit folder for more information.", csvPath)
							end	
							
							return .SetErrObj(.InitErrObj("_ValidateFileStructureCsvFiles"), msg)		
							
						end
						
					end
				end	
																  
				return .SuccessResult()				
			
			end

end

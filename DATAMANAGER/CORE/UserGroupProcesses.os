package DATAMANAGER::CORE

public object UserGroupProcesses inherits DATAMANAGER::LoadAPIProcesses

	public		List	_fRequiredColsGroup
	public		List	_fRequiredColsUser


	
	override function Assoc NewJobPayload(Object prgCtx, Assoc options, String action)
											
			// *** Initialize payload Assoc
			Assoc payload
					
			options.ugLoad = true
					
			// *** ug option
			options.addMembers = (IsFeature(options, "addMembers") && options.addMembers) || (action == "UPDATEUSERGROUP" && IsFeature(options, "updateMemberOption") && options.updateMemberOption != "doNothing")
			options.updateMemberOption = IsFeature(options, "updateMemberOption") ? options.updateMemberOption : "merge"
					
			// *** what are the hidden cols
			options.hiddenCols = Str.Elements(options.hiddenCols, ",")
					
			// *** hardcode the multi value delimiter for now
			options.multiValDelim = "|"
					
			// *** get the load dir path
			String rootFilePath = options.rootSourcePath
			rootFilePath = rootFilePath[1:Str.RChr(rootFilePath, File.Separator())]
			options.loadDirPath = rootFilePath
					
			// *** get the audit log location
			options.auditFolderPath = .GetAuditFolderPath(options)
					
					
			// Archive csv files options - Update 5 for OMERS
			options.archiveCsvFiles = IsFeature(options, "archiveCsvFiles") && options.archiveCsvFiles
			if options.archiveCsvFiles 
				Assoc status = ._GetCsvArchivePath(options)
						
				if status.ok
					options.csvArchivePath = status.result
					options.csvFiles = {}
				else
						.GetLogger().ERROR(Str.Format("Error getting csv archive path: %1. Will not be able to move metadata csv files.", status.errMsg))
					options.archiveCsvFiles = false
				end		
						
		
			end					
						
			// *** put the options in 
			payload.options = options
					
			return payload
				
		end

	/* must implement this signature */
	public function Assoc PerformAction(Object prgCtx, Frame apiDataObj, Assoc options, Assoc actionFlags)
											
				Assoc 	rtn
				rtn.ok = true
				return rtn
			end

	
	override function Assoc ProcessItem(Object prgCtx, \
												String action, \
												Frame apiDataObj, \
												Assoc actionFlags, \
												Assoc options)
																		
				Assoc	status
				Assoc 	ugData
				
				// *** start a transaction
				prgCtx.fDbConnect.StartTrans()
				
					// *** finally, the actual action
					status = .PerformAction(prgCtx, apiDataObj, options, actionFlags)
					if !status.ok
						// *** rollback
						prgCtx.fDbConnect.EndTrans(false)
						return .SetErrObj(.InitErrObj("UserGroupProcesses.ProcessItem"), status)
					end	
				
				// *** commit transaction
				prgCtx.fDbConnect.EndTrans(true)	
			
				Assoc rtn	
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.ugData = ugData
				rtn.result.targetItem = 3	
				rtn.result.actionPerformed = status.result.actionPerformed
			
				return rtn
			end

	
	public function Assoc SubclassValidateItem(Object prgCtx, \
													Assoc actionFlags, \
													Assoc options, \
													Object apiDataObj, \
													Integer typeVal)
		
				Assoc		rtn	
		
		
				rtn.ok = true
		
				return rtn
		
			end

	
	override function Assoc ValidateItem(Object prgCtx, \
												String action, \
												Frame apiDataObj, \
												Assoc dataFlags, \
												Assoc options)
											
				Assoc 	status
				Assoc	ugData
				Frame	currentDataObj = undefined
				Integer	typeVal // user or group type
			
				// *** actionFlags -- either create or update
				Assoc	actionFlags	
				actionFlags.create = false
				actionFlags.update = false
				actionFlags.delete = false
				
				// *** get the type
				status = ._GetSetType(apiDataObj)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				else
					typeVal = status.result
				end	
				
				// *** check required columns
				status = ._CheckRequiredCols(apiDataObj, typeVal)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				end	
				
				// *** let subclasses check for action-specific things
				status = .SubclassValidateItem(prgCtx, actionFlags, options, apiDataObj, typeVal)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				end		
				
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.oldVals = IsDefined(currentDataObj) ? currentDataObj.GetAssocVals() : undefined
				rtn.result.oldCsvData = IsDefined(currentDataObj) ? currentDataObj.GetCsvData() : undefined		
				rtn.result.actionFlags = actionFlags
				rtn.result.ugID = ugData.ID
				
				return rtn	
			
			end

	
	private function Assoc _CheckRequiredCols(Object apiDataObj, Integer typeVal)
												
												String	col
												
												List	requiredCols = typeVal==UAPI.USER ? ._fRequiredColsUser : ._fRequiredColsGroup
												
												for col in requiredCols
													Dynamic testVal = apiDataObj.GetValueFromCol(col)
													
													if IsUndefined(testVal)
														return .SetErrObj(.InitErrObj("_CheckRequiredCols"), Str.Format("The CSV column %1 is required for this action", col)) 
													end
													
												end
											
												Assoc rtn
												rtn.ok = true
												return rtn
											
											end

	
	private function Assoc _FindUserGroup(	Object prgCtx, \
																			Object apiDataObj)
																			
												Integer		id = apiDataObj.GetValueFromCol("$ID")
												String		name = 	apiDataObj.GetValueFromCol("$NAME")
												Assoc		status
											
												if IsDefined(id) 
												
													status = ._UserRecFromID(prgCtx, id, UAPI.USER)
													if status.ok
														apiDataObj.SetValueFromCol("$TYPE", UAPI.USER)
													else
														status = ._UserRecFromID(prgCtx, id, UAPI.GROUP)
														if status.ok
														apiDataObj.SetValueFromCol("$TYPE", UAPI.GROUP)
														else
															return .SetErrObj(.InitErrObj("UserGroupProcesses._FindUserGroup"), Str.Format("Cannot find user/group record with id %1", id))
														end							
													end	
												
												elseif IsDefined(name)
													
													status = ._UserIDFromName(prgCtx, name, UAPI.USER)
													if status.ok
														apiDataObj.SetValueFromCol("$ID", status.result)
														apiDataObj.SetValueFromCol("$TYPE", UAPI.USER)
													else
														status = ._UserIDFromName(prgCtx, name, UAPI.GROUP)
														if status.ok
														apiDataObj.SetValueFromCol("$ID", status.result)
														apiDataObj.SetValueFromCol("$TYPE", UAPI.GROUP)
														else
															return .SetErrObj(.InitErrObj("UserGroupProcesses._FindUserGroup"), Str.Format("Cannot find user/group ID from name'%1.'", name))
														end							
													end	
											
												else
													return .SetErrObj(.InitErrObj("UserGroupProcesses._FindUserGroup"), "Cannot determine the ID for the group or user to update/delete. Expecting $NAME or $ID column.")
												end
											
												Assoc rtn
												rtn.ok = true
												return rtn
											
											end

	script _GetSetType
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Assoc _GetSetType(apiDataObj)
																									
																										// *** figure out if user or group
																										Dynamic typeVal = UAPI.USER	// *** default to user	
																									
																										// *** loop through possible cols to figure out what user wanted
																										List typeCols = {"$TYPE","$OBJECTTYPE"}
																										Dynamic testVal
																										String t
																										for t in typeCols
																											testVal = apiDataObj.GetValueFromCol(t)	
																											
																											if IsDefined(testVal)
																												typeVal =  _DecodeTypeVal(testVal)
																												if IsUndefined(typeVal)
																													return .SetErrObj(.InitErrObj("UserGroupProcesses._DecodeTypeVal"), Str.Format("Cannot determine if type is user or group from $TYPE or $OBJECTTYPE value. Invalid value '%1.'", testVal))
																												end	
																											end		
																										end
																									
																										// *** set the type
																										apiDataObj.SetValueFromCol("$TYPE", typeVal)
																										
																										Assoc rtn
																										rtn.ok = true
																										rtn.result = typeVal
																										return rtn
																									
																									end
																									
																									function Integer _DecodeTypeVal(Dynamic csvVal)
																									
																										if Type(csvVal) == StringType and Str.Lower(csvVal) == "user" 
																											return UAPI.USER
																										elseif Type(csvVal) == StringType and Str.Lower(csvVal) == "group" 	
																											return UAPI.GROUP
																										elseif Type(csvVal) == IntegerType and (csvVal in {UAPI.USER, UAPI.GROUP})
																											return csvVal
																										else
																											return undefined
																										end
																										
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

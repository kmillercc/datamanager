package DATAMANAGER::CORE

public object NodeGenerateLabels inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	String	fActionPerformed = 'Labels Generated.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeGenerateLabels'
	override	String	fSubKey = 'Node.GenerateLabels'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node, \
												Assoc thisActionFlags = actionFlags)		// *** allows for sending in flags independent of overall job options (for name collisions specifically)
						
			
				Object physObjService = $Service.ServiceRegistry.getItem( "PhysicalObjects" )	
				if IsUnDefined( physObjService )
					return .SetErrObj(.InitErrObj("PerformAction"), "Could not get PhysicalObjects web service")
				end
			
				Object physObjServiceTemp = OS.NewTemp( physObjService )
				physObjServiceTemp.fOutArgs = Assoc.CreateAssoc()
				physObjServiceTemp.fSocket = Undefined
				physObjServiceTemp.SetProgramSession( prgCtx )
			
				String	LabelType = apiDataObj.GetValueFromCol("$LabelType")
				Integer NumberOfCopies = apiDataObj.GetValueFromCol("$LabelCopies")
				String  Facility = apiDataObj.GetValueFromCol("$Facility")
				String  Area = apiDataObj.GetValueFromCol("$Area")
				String  BoxLocator = apiDataObj.GetValueFromCol("$Locator")
			
				Boolean	bLocator = actionFlags.doLabelLocator
				
				Assoc status = physObjServiceTemp.PhysObjGenerateLabel( node.pId, LabelType, NumberOfCopies, bLocator, Facility, Area, BoxLocator)		
				if !status.ok
					OS.Delete( physObjServiceTemp )
					return .SetErrObj(.InitErrObj("PerformAction"), status)
				end	
				
				
				// *** destroy temp obj
				OS.Delete( physObjServiceTemp )
				
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.node = node
				rtn.result.actionPerformed = .fActionPerformed
				
				return rtn
			end

end

package DATAMANAGER::CORE

public object NodeRename inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	String	fActionPerformed = 'Node Renamed.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeRename'
	override	String	fSubKey = 'Node.Rename'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node, \
												Assoc thisActionFlags = actionFlags)		// *** allows for sending in flags independent of overall job options (for name collisions specifically)
						
				Assoc rtn = .InitErrObj("PerformAction")
				String actionPerformed
				
				// important to get the name
				String		newName = apiDataObj.getValue("newName")
				
				if IsUndefined(newName)
					.SetErrObj(rtn, "The newName is undefined. Cannot rename the object")
					return rtn
					
				elseif newName!=node.pName
					
					// *** call LLIAPI's NodeRename
					Assoc status = llNode.NodeRename( node, newName )
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					else
						actionPerformed = .fActionPerformed	
					end		
							
				end	
			
				Assoc result
				result.node = node
				result.actionPerformed = actionPerformed
				
				return .SuccessResult(result)
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
														Assoc actionFlags, \									
														Assoc options, \
														Object llNode, \
														DAPINODE node, \
														Object apiDataObj)
							
				Assoc		status
		
				// *** in this case, the parentNode is the node's parent - no need for any fancy targetPath parsing
				DAPINODE	parentNode = DAPI.GetParentNode(node)
				if IsError(parentNode)
					// *** send back an error
					return .SetErrObj(.InitErrObj( "ValidateNameCollision" ), "Could not get parentNode")
				end			
		
				// make sure we have a new name	
				String 	 newName = apiDataObj.GetValueFromCol("$NEWNAME")
				if IsUndefined(newName)
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), "The newName is undefined. Cannot validate new name.")
				end
				
				if newName!=node.pName
					Assoc		collisionOptions = options.nameCollisions
					Boolean 	makeNameUnique = (llNode.fVersioned && collisionOptions.docs.makeUnique) || (!llNode.fVersioned && collisionOptions.containers.makeUnique)
			
					if !$DataManager.NodeAPI.IsUniqueName(prgCtx, parentNode, newName) && !makeNameUnique
						// *** send back an error
						return .SetErrObj(.InitErrObj( "ValidateNameCollision" ), Str.Format("An item with the name '%1' already exists in the desired targetPath.", newName))
					end			
							
					// *** call LLIAPI's CheckRename
					status = llNode.CheckRename( node, newName )
					if !status.ok
						return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
					end		
				end
			
				return Assoc{'ok': true}	
			end

end

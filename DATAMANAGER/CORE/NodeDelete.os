package DATAMANAGER::CORE

public object NodeDelete inherits DATAMANAGER::CORE::NodeProcesses

	override	String	fActionPerformed = 'Deleted.'
	override	Boolean	fAllowPostActionUpdate = FALSE
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeDelete'
	override	String	fSubKey = 'Node.Delete'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \							
												Frame apiDataObj, \								
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE newParentNode, \
												DAPINODE node)
						
				Assoc		status
			
			
				// *** call LLIAPI's NodeMove
				status = llNode.NodeDelete( node )
				if !status.ok
					return .SetErrObj(.InitErrObj("PerformAction"), status)
				end		
				
				Assoc result	
				result.actionPerformed = .fActionPerformed
				result.node = node
				
				return .SuccessResult(result)	
				
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
														Assoc actionFlags, \																	
														Assoc options, \
														Object llNode, \
														DAPINODE node, \
														Object apiDataObj)
																				
				Assoc		status
				
				// *** call LLIAPI's CheckDelete
				status = llNode.CheckDelete( node)
				if !status.ok
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
				end		
				
				return .SuccessResult()	
					
			end

end

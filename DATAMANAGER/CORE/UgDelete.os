package DATAMANAGER::CORE

public object UgDelete inherits DATAMANAGER::CORE::UserGroupProcesses

	override	List	_fRequiredColsGroup = { '$NAME' }
	override	List	_fRequiredColsUser = { '$NAME' }
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'UgDelete'
	override	String	fSubKey = 'UserGroup.Delete'


	/* must implement this signature */
	override function Assoc PerformAction(	Object prgCtx, \
														Frame apiDataObj, \
														Assoc options, \
														Assoc actionFlags)
														
							Assoc		status
							Object		uSession = prgCtx.USession()
							Object		usersPkg = $LLIAPI.UsersPkg
							Integer 	typeVal = apiDataObj.GetValueFromCol("$TYPE")
							Integer 	id = apiDataObj.GetValueFromCol("$ID")
							String		actionPerformed
							
							switch typeVal
							
								case UAPI.USER
									/*	
										function Assoc UserDelete( \
											Object		uapiCtx, \
											Integer		userID, \
											Dynamic		context = UNDEFINED )
									*/
								
									status = usersPkg.UserDelete(uSession, \
																id)
																	
									if !status.ok
										// *** rollback
										prgCtx.fDbConnect.EndTrans(false)						
										return .SetErrObj(.InitErrObj( "PerformAction" ), status)
									else
										actionPerformed = "User Deleted"				
									end											
																	
								end
								
								case UAPI.GROUP
									/* api
										GroupDelete( \
											Object		uapiCtx, \
											Integer		groupID, \
											Assoc		deleteInfo = UNDEFINED )			
									*/
									
									
									// *** call LLIAPI.UsersPkg function
									status = usersPkg.GroupDelete(	uSession, \
																	id)
						
									if !status.ok
										// *** rollback
										prgCtx.fDbConnect.EndTrans(false)			
										return .SetErrObj(.InitErrObj( "PerformAction" ), status)
									else
										actionPerformed = "Group Deleted"								
									end			
						
								end	
							
							end
							
							Assoc 	rtn
							rtn.ok = true
							rtn.result = Assoc.CreateAssoc()
							rtn.result.actionPerformed = actionPerformed
								
							return rtn
						end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
															Assoc actionFlags, \
															Assoc options, \
															Object apiDataObj, \
															Integer typeVal)
							
							Assoc			status
							UAPISESSION		uSession = prgCtx.USession().fSession
						
							// *** does the user/group exist?
							status = ._FindUserGroup(prgCtx, apiDataObj)
							if !status.ok
								return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
							end	
						
							Integer		id = apiDataObj.GetValueFromCol("$ID")
							Record	ugRec
							RecArray ugRecs = UAPI.GetById(uSession, id)
							if Length(ugRecs)
								ugRec = ugRecs[1]
							end	
						
							if IsError(UAPI.CheckAlterPrivs( uSession, ugRec ))
								return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), "You do not have sufficient privileges to delete this user/group")
							end	
						
							actionFlags.delete = true
							
							Assoc rtn
							rtn.ok = true
							return rtn
							
						end

end

package DATAMANAGER::CORE

public object RMPreProcessor inherits DATAMANAGER::LoadMetadataObjects

	
	public function Assoc ProcessRMDataForCreate(Object prgCtx, Frame apiDataObj, Boolean inheritParent)					
					
					Assoc status
					
					// *** handle filenumber						
					status = ._HandleFileNumber(prgCtx, apiDataObj, inheritParent, true)
					if !status.ok
						return .SetErrObj(.InitErrObj("ProcessData"), status)
					end
						
					Assoc rtn
					rtn.ok = true	
					return rtn
				
				end

	
	public function Assoc ProcessRMDataForUpdate(Object prgCtx, Frame apiDataObj)
					
					Assoc status
					
					// *** handle filenumber						
					status = ._HandleFileNumber(prgCtx, apiDataObj, false, true)
					if !status.ok
						return .SetErrObj(.InitErrObj("ProcessData"), status)
					end
						
					Assoc rtn
					rtn.ok = true	
					return rtn
				
				end

	script _HandleFileNumber
	
		// Patched by Pat201909040
		function Assoc _HandleFileNumber(	Object prgCtx, \
											Frame apiDataObj, \
											Boolean inheritParent, \
											Boolean classIDRequired)
		
			String 	fileNumberStr = apiDataObj.GetValueFromCol("$FileNumber")
			Integer classId = apiDataObj.GetValueFromCol("$RDClassification")	
			Assoc 	status		
		
			// *** do we have a File Number but no RMClassification from the CSV?  Can we get it from File Number (only if we're not supposed to inherit from parent)?	
			if IsUndefined(classId) 
				if !inheritParent && IsDefined(fileNumberStr)
					status = _NodeFromFileNumber(prgCtx, fileNumberStr)
					if !status.ok
						return .SetErrObj(.InitErrObj("_HandleFileNumber"), status)
					else
			
						DAPINODE classNode = status.result
						apiDataObj.SetValueFromCol("$RDClassification", classNode.pId)
					end
				elseif classIDRequired
					// don't we have to return an error here? There is no $RDCLASSIFICATION in the CSV and it will be required when we create
					// TODO would be nice to catch this on validation
					return .SetErrObj(.InitErrObj("_HandleFileNumber"), 'Cannot set RM Classification. You need to supply a $FILENUMBER OR $RDCLASSIFICATION value.')
				end	
			end	
			
			Assoc rtn
			rtn.ok = true
			return rtn
			
		end	
		
			function Assoc _NodeFromFileNumber(Object prgCtx, String fileNumberStr)
		
				DAPINODE	rootNode, classNode
				Integer		parentClassId = 0	
				Integer		partIdx=1
				String		partStr
					
				// *** get the delim
				String		delim = CAPI.INIGet(prgCtx.fDBConnect.fLogin, 'RecMan', 'fileSeparator', '-')	
		
				// *** break up file number into parts
				List	fnParts = Str.Elements(fileNumberStr, delim)
				
				if Length(fnParts)
					String rootFNumber = fnParts[1]
					
					// *** get possible roots
					List rootNodes = _GetClassificatioNodesFromFileNumber(prgCtx, rootfNumber)
		
					if Length(fnParts) > 1
						// *** loop through the roots and find the correct paths below
						for rootNode in rootNodes
							parentClassId = rootNode.pId
							
							for partIdx = 2 to Length(fnParts)
								partStr = delim + fnParts[partIdx]
								classNode = _GetClassificatioNodeFromFileNumberAndParentId(prgCtx, partStr, parentClassId)
								
								if IsUndefined(classNode)
									return .SetErrObj(.InitErrObj("_NodeFromFileNumber"), Str.Format("Cannot set RM Classification from File Number. Filenumber '%1' is invalid", fileNumberStr))
								else
									parentClassId = classNode.pId	
								end
							end		
						end		
		
					elseif Length(rootNodes) > 1
						return .SetErrObj(.InitErrObj("_NodeFromFileNumber"), Str.Format("Cannot set RM Classification from File Number. More than 1 classification has this file Number.", fileNumberStr))
										
					elseif Length(rootNodes) == 1
						classNode = rootNodes[1]
						
					end
				end
				
				// **** KM 9-4-19 changed code to handle this error and void sending back rtn.ok = true when no classification is found
				if IsUndefined(classNode)
					return .SetErrObj(.InitErrObj("_NodeFromFileNumber"), Str.Format("Cannot set RM Classification from File Number. Cannot find Classification with FileNumber '%1'", fileNumberStr))			
				end
		
				Assoc rtn 
				rtn.ok = true
				rtn.result = classNode
				return rtn
				
			end
		
			function List _GetClassificatioNodesFromFileNumber(Object prgCtx, String fileNumberStr)
		
				Record		rec
				DAPINODE	classNode
				List		classNodes
				String		stmt = 'select NodeID, FileNumber from RM_CLASSIFICATION where FileNumber=:A1'
				RecArray 	recs = .ExecSql(prgCtx, stmt, {fileNumberStr})
		
				if IsNotError(recs)
					for rec in recs
						// *** get the DAPINODE
						classNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.NODEID)
						if IsNotError(classNode)
							classNodes = {@classNodes, classNode}
						end
					end
				end
				
				return classNodes				
		
			end
		
			function DAPINODE _GetClassificatioNodeFromFileNumberAndParentId(Object prgCtx, String fileNumberStr, Integer parentId)
		
				Record rec		
				DAPINODE classNode
		
				String	stmt = 'select NodeID, FileNumber from RM_CLASSIFICATION where FileNumber=:A1'
				RecArray recs = .ExecSql(prgCtx, stmt, {fileNumberStr})
				
				if IsNotError(recs)		
					for rec in recs
						// *** get the DAPINODE
						classNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, rec.NODEID)
						if IsNotError(classNode) && classNode.pParentId == parentID
							return classNode
						end
					end
				end
				
				return undefined
		
			end
	
	endscript

	
	private function void _MergeValsForUpdate(Frame currentDataObj, Frame apiDataObj)
							
					// *** the llNode.NodeUpdate function (with objAction=info2) assumes we have all values from the request
					// *** so we have to pull all the current vals and merge new vals in with them
				
					Assoc currentApiVals = currentDataObj._fApiVals
					Assoc newApiVals = apiDataObj._fApiVals
				
					// *** get the rm details page data
					// *** copy the current vals so we don't overwrite them
					Assoc currentRMData = Assoc.Copy(currentApiVals.metadata.rmUpdateInfo)
					Assoc newRMData = newApiVals.metadata.rmUpdateInfo	
					
				
					Assoc mergedVals = Assoc.Merge(currentRMData, newRMData)
					
					// *** now set back into apiDataObj
					newApiVals.metadata.rmUpdateInfo = mergedVals
				
					
					// *** ok, now we need the classification id
					if IsUndefined(newApiVals.metadata.rmClassId) && IsDefined(currentApiVals.metadata.rmClassId)
						newApiVals.metadata.rmClassId = currentApiVals.metadata.rmClassId
					end
					
				
				end

end

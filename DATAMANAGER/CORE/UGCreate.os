package DATAMANAGER::CORE

public object UGCreate inherits DATAMANAGER::CORE::UserGroupProcesses

	override	List	_fRequiredColsGroup = { '$NAME', '$TYPE' }
	override	List	_fRequiredColsUser = { '$NAME', '$TYPE' }
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'UGCreate'
	override	String	fSubKey = 'UserGroup.Create'


	/* must implement this signature */
	override function Assoc PerformAction(	Object prgCtx, \
											Frame apiDataObj, \
											Assoc options, \
											Assoc actionFlags)
											
				// *** if we determined there was a name conflict, then update
				if actionFlags.update
					return $DataManager.UgUpdate.PerformAction(prgCtx, apiDataObj, options, actionFlags)
				end							
				
				Assoc		status
				Object		uSession = prgCtx.USession()
				Object		usersPkg = $LLIAPI.UsersPkg
				Assoc		ugData
				Integer 	typeVal = apiDataObj.GetValueFromCol("$TYPE")
				String		actionPerformed
				
				switch typeVal
				
					case UAPI.USER
				
						ugData = ._SetUserData(prgCtx, options, apiDataObj)
					
						/* api
						//		uapiCtx			The UapiSession object.
						//		userName		The name of the new user.
						//		userPWD			The user password.
						//		defGroup		The name of the base group this user belonged to. (deprecated)
						//		privs			The user privileges mask.
						//		data			The user data for the UserData column.
						//		exAttrs			The extended user attributes
						//		context			The generic request handler object
						//		expireDateAssoc	The password expiration date.
						//		groupID			The ID of the default group.
						//		spaceID			The ID of the space
						//		userType		The type of user to create
						//											
						*/
					
					
						status = usersPkg.UserNew(uSession, \
													ugData.NAME, \
													ugData.USERPWD, \
													ugData.DEPARTMENT, \
													ugData.USERPRIVILEGES, \
													ugData.USERDATA, \
													ugData.exAttrs, \
													undefined, \
													ugData.expireDateAssoc, \
													ugData.GROUPID, \
													ugData.SPACEID)
														
						if !status.ok
							return .SetErrObj(.InitErrObj( "PerformAction" ), status)
						else
							actionPerformed = "User Created"
							apiDataObj.SetValueFromCol("$ID", status.userID)	
						end											
														
					end
					
					case UAPI.GROUP
					
						// set up the data we need
						ugData = ._SetGroupData(prgCtx, options, apiDataObj)
					
						// make a call to usersPkg.GroupNew
						status = usersPkg.GroupNew(	uSession, \
													ugData.NAME, \
													ugData.LEADERID, \
													ugData.USERDATA, \
													ugData.SPACEID)
															
															
						if !status.ok
							return .SetErrObj(.InitErrObj( "PerformAction" ), status)
						else
							apiDataObj.SetValueFromCol("$ID", status.GroupID)
							actionPerformed = "Group Created"
						end	
						
						// *** add members?
						if options.addMembers && Length(ugData.addUsers) 
							
							status = $DataManager.UgUpdate.PerformAction(prgCtx, apiDataObj, options, actionFlags)
							
							if !status.ok
								return .SetErrObj(.InitErrObj( "PerformAction" ), status)
							else
								actionPerformed = "Group Created and Members Added"	
							end
						end				
					end
			
				end
				
				Assoc 	rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.actionPerformed = actionPerformed
				return rtn
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
												Assoc actionFlags, \
												Assoc options, \
												Object apiDataObj, \
												Integer typeVal)
				
				String 	ugTypeStr = typeVal == UAPI.USER ? "User" : "Group"
				
				// *** check for specific action types here
				UAPISESSION	uSession = prgCtx.USession().fSession
				
				// *** get name and groupid
				String 	name = apiDataObj.GetValueFromCol("$NAME")	
				Integer	groupId = apiDataObj.GetValueFromCol("$GROUPID")	
				Dynamic	privs = apiDataObj.GetValueFromCol("$USERPRIVILEGES")
				
				// *** does the user have privileges to do this
				if UAPI.CheckAddPrivs( uSession, typeVal ) != 0
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), Str.Format("You do not have sufficient privileges create a %1", ugTypeStr))
				end	
				
				// *** make sure create user has a groupid
				if typeVal == UAPI.User && IsUndefined(groupId)
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), "User creation requires a $Group or $DefaultGroup value")
				end
				
				// *** make sure create user has userprivileges column
				if typeVal == UAPI.User && IsUndefined(privs)
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), "User creation requires a $USERPRIVILEGES column")
				end										
				
				// *** is there a name conflict error?
				RecArray recs = .ExecSql(prgCtx, "select ID, Name from KUAF where Type=:A1 and Deleted=0 and Name=:A2", {typeVal, name})
				if Length(recs)	
					if Str.Upper(options.nameCollisions)=="UPDATE"
					
						apiDataObj.SetValueFromCol("$ID", recs[1].ID)
						actionFlags.update = true
						
						// *** make sure we have everything needed to update
						return $DataManager.UgUpdate.ValidateItem(prgCtx, "UPDATE", apiDataObj, actionFlags, options)
						
					else	
						return.SetErrObj(.InitErrObj( "SubclassValidateItem" ), Str.Format("A %1 with the name %2 already exists. Consider changing parameters in the name collision section to avoid these errors.", ugTypeStr, name))
					end	
				else
				
					actionFlags.create = true
				end
				
				Assoc rtn
				rtn.ok = true
				return rtn
				
			end

	
	private function Assoc _SetGroupData(Object prgCtx, \
										Assoc options, \
										Object apiDataObj)
				
			
				Assoc	ugData
				
				// *** build an assoc we will use for api functions
				List 	groupFields = {"NAME","LEADERID","USERDATA","SPACEID"}
				String	fName
				for fName in groupFields
					Dynamic val = apiDataObj.GetValueFromCol("$" + fName)
				
					switch fname
						case "SPACEID"
							ugData.(fName) = IsDefined(val) && Type(val) == IntegerType ? val : 0 
						end
			
						default
							ugData.(fName) = IsDefined(val) && Length(val) ? val : undefined
						end
					end
				end
				
				// *** fields used for group memberhip
				ugData.newUsers = undefined
				ugData.addUsers = undefined
				ugData.delUsers = undefined
				
				if options.updateMemberOption == "merge"
					ugData.addUsers = apiDataObj.GetValueFromCol("$MEMBERS")
				elseif options.updateMemberOption == "clear"
					ugData.newUsers = apiDataObj.GetValueFromCol("$MEMBERS")
				end	
			
			
				return ugData						
			end

	
	private function Assoc _SetUserData(Object prgCtx, \
										Assoc options, \
										Object apiDataObj)
				
				Assoc 	ugData
				
				// *** fields that the API asks for
				ugData.Department	= undefined  		// setting group by name is deprecated in UsersPkg
				ugData.ExAttrs	= Assoc.CreateAssoc()
			
			
				// *** build an assoc we will use for api functions
				List 	userFields = {"NAME","USERPWD","USERPRIVILEGES","USERDATA","GROUPID","SPACEID","LASTNAME","MIDDLENAME","FIRSTNAME","CONTACT","FIRSTNAME","LASTNAME","MAILADDRESS","TITLE","OFFICELOCATION"}
				String	fName
				for fName in userFields
					Dynamic val = apiDataObj.GetValueFromCol("$" + fName)
				
					switch fname
						case "SPACEID"
							ugData.(fName) = IsDefined(val) && Type(val) == IntegerType ? val : 0 
						end
				
						case "CONTACT", "FAX", "FIRSTNAME", "LASTNAME", "MIDDLENAME", "MAILADDRESS", "TITLE", "TIMEZONE", "OFFICELOCATION"
							ugData.ExAttrs.(Str.Lower(fName)) = IsDefined(val) ? val : ''
						end
			
						default
							ugData.(fName) = IsDefined(val) && Length(val) ? val : undefined
						end
					end
				end
			
				
				if IsDefined(ugData.("$PwdExpireDate")) && Type(ugData.("$PwdExpireDate")) == DateType
					ugData.ExpireDateAssoc	= Assoc.CreateAssoc()
					ugData.expireDateAssoc.Date = ugData.("$PwdExpireDate")
				else
					ugData.ExpireDateAssoc = undefined	
				end	
				
				
				return ugData						
			end

end

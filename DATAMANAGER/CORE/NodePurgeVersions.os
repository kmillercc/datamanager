package DATAMANAGER::CORE

public object NodePurgeVersions inherits DATAMANAGER::CORE::NodeProcesses

	override	String	fActionPerformed = 'Versions Purged.'
	override	Boolean	fAllowPostActionUpdate = FALSE
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodePurgeVersions'
	override	String	fSubKey = 'Node.PurgeVersions'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \							
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE newParentNode, \
												DAPINODE node)
						
				Assoc		status
			
				// *** Integer versions to keep
				Integer versionsToKeep = apiDataObj.getValue("versionsToKeep")
			
				// *** call LLIAPI's Node Purge Versions
				status = llNode.NodePurgeVersions( node, versionsToKeep )
				if !status.ok
					return .SetErrObj(.InitErrObj("PerformAction"), status)
				end		
				
				Assoc result	
				result.actionPerformed = .fActionPerformed
				result.node = node
				
				return .SuccessResult(result)
				
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
														Assoc actionFlags, \																	
														Assoc options, \
														Object llNOde, \
														DAPINODE node, \
														Object apiDataObj)
							
				Object		dbConnect = prgCtx.fDbConnect
				
				// *** make sure this is a versioned object
				if !llNode.fVersioned
					return .SetErrObj( .InitErrObj( "SubclassValidateItem" ), "Non-Versioned Object. Purge Versions is an invalid action.")	
				end
			
				// *** ensure we have a value for versions to keep
				if IsUndefined(apiDataObj.getValue("versionsToKeep"))
					apiDataObj.SetValue("versionsToKeep", 1)	
				end
			
				// *** is version delete disabled in this system?	
				if $LLIApi.VersionDeleteDisabled && !( dbConnect.HasByPassPerm() || dbConnect.HasSysAdminPerm() )
					return .SetErrObj( .InitErrObj( "SubclassValidateItem" ), "Deleting Versions is disabled in this system (opentext.ini setting).")
				end		
				
				// *** does user have required permissions?
				if !($LLIApi.NodeUtil.CheckPermissions( node, { $PDeleteVersions } ) )
					return .SetErrObj( .InitErrObj( "SubclassValidateItem" ), [DOCMANSERVICE_ERRMSG.InsufficientPermissionForOperation])
				end		
				
				// *** is this a released node?
				if llNode.IsReleasedNode( prgCtx.DSession(), node )
					return .SetErrObj( .InitErrObj( "SubclassValidateItem" ), "The node is a released node. You cannot delete any of its versions")
				end
					
				return .SuccessResult()	
											
			end

end

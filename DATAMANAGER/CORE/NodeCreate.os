package DATAMANAGER::CORE

public object NodeCreate inherits DATAMANAGER::CORE::NodeProcesses

	override	String	fActionPerformed = 'Created.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeCreate'
	override	String	fSubKey = 'Node.Create'


	
	public function CreateObject(Object prgCtx, \
													  String name, \
													  Frame apiDataObj, \
													  Assoc options, \
													  Assoc actionFlags, \
													  String filePath, \
													  Object llNode, \
													  DAPINODE parentNode, \
													  Boolean makeNameUnique)
										
							Assoc 		rtn
							Assoc		status
							Assoc		createInfo
							Assoc		metadataVals = apiDataObj.GetValue("metadata")
							DAPINODE 	node
											
							// *** call LLIAPI's CheckNewItem
							Object llparent = $LLIAPI.LLNodeSubsystem.GetItem( parentNode.pSubtype )
							status = llparent.CheckNewItem( parentNode, name )
							if !status.ok
								if makeNameUnique && status.errMsg == Str.Format( [LLAPI_LLNodeErr.NameNotUnique], name )
									// do nothing
								else
									rtn = .SetErrObj(.InitErrObj("CreateObject"), status)
									rtn.errMsg = rtn.errMsg + " (code 001)"
									return rtn
								end
							end				
								
							// *** are we creating from a template? -- yes if we have a templateID or if it's a binder or contract file
							Integer templateID = apiDataObj.GetValue("templateId")
							if IsDefined(templateID) || llNode.fSubtype in {31066,31067} > 0
								status = $DataManager.TemplateUtils.CreateObjectFromTemplate( prgCtx, name, metadataVals, llNode.fSubtype, parentNode)
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("CreateObject"), status)
									rtn.errMsg = rtn.errMsg + " (code 001.5)"
									return rtn	
								else
									node = status.result
								end					
								
							else	
							
								// *** now get the initial assoc for the createInfo
								status = ._GetSetCreateInfo(prgCtx, metadataVals, apiDataObj, options, actionFlags, filePath, parentNode, llNode)
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("CreateObject"), status)
									rtn.errMsg = rtn.errMsg + " (code 002)"
									return rtn
								else
									createInfo = status.result
								end		
								
								if IsDefined(templateID) && llNode.fSubtype==848
									createInfo.templateNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, templateID)
									if IsError(createInfo.templateNode)
										rtn = .SetErrObj(.InitErrObj("CreateObject"), Str.Format("Error getting template node from templateID %1", templateID))
										rtn.errMsg = rtn.errMsg + " (code 016)"
										return rtn		
									end
									Assoc result = $OTSAPWKSP.Utils.GetBusinessReference( prgCtx, createInfo.templateNode )
									if result.ok
										createInfo.ReferenceTypeID = result.reference.ID_REFERENCE_TYPE
										createInfo.id_bo = result.reference.ID_BO
										createInfo.id_bo_type = result.reference.ID_BO_TYPE	
										createInfo.IsEarlyScenario = result.isEarly
										createInfo.IsDocTemplate = true
									else
										return .SetErrObj(.InitErrObj("CreateObject"), result)
									end							
								end	
					
								// *** update owner or group ?
								if actionFlags.doOwnerData
									metadataVals.dapiNodeData = Assoc.Merge(metadataVals.dapiNodeData, metadataVals.ownerData)
								end			
						
								// *** KM 11/1/17 add email folder to this conditional that will do GetCreateNode instead of just Create
								if (options.autoCreateFolders && llNode.fSubtype in {$TypeFolder, 751} > 0)
									// *** if we are auto creating folders, we have to allow for possibility that the item already exists
									// *** and since we are doing this load multi-threaded, we need to allow that the node might get created in the split-seconds
									// *** between checking for it's existence and creating
							
									// *** hand off to our node creation API
									Integer numRetries = 10
							
									status = $DataManager.NodeAPI.GetCreateNode(prgCtx, \
																		   parentNode, \
																		   llNode.fSubtype, \
																		   name, \
																		   numRetries, \
																		   createInfo, \ 	
																		   metadataVals.dapiNodeData, \
																		   options.inheritParent,\
																		   makeNameUnique, \
																		   false,\
																		   metadataVals.systemAtts)
																	   
									if !status.ok
										rtn = .SetErrObj(.InitErrObj("PerformAction"), status)
										rtn.errMsg = rtn.errMsg + " (code 003)"
										return rtn
									else
										node = status.result.node		
									end													   
																	   
								else
																	   
									status = $DataManager.NodeAPI.CreateNode(prgCtx, \
																		parentNode, \
																		llNode.fSubtype, \
																		name, \
																		createInfo, \
																		metadataVals.dapiNodeData, \
																		options.inheritParent,\
																		makeNameUnique, \
																		false,\
																		metadataVals.systemAtts)	
																	
									if !status.ok
										rtn = .SetErrObj(.InitErrObj("CreateObject"), status)
										rtn.errMsg = rtn.errMsg + " (code 004)"
										return rtn
									else
										node = status.result		
									end														
																
								end
							end
							
							// *** KM 11-15-18 this is no longer needed
							/*
							if IsDefined($ADN)	
								Object cb
								Object dapiCtx = prgCtx.DSession()
								
								for cb in llNode.GetCallbacks('CBCreate')
									if OS.Name(cb) == "AutoGenNodeCallbacks"
										// *** attrCacheID !=0 required for the callback
										createInfo.AttrCacheID=1
										
										status = cb.CBCreate( dapiCtx, node, createInfo, undefined )
											
										if !status.ok
											rtn = .SetErrObj(.InitErrObj("CreateObject"), status)
											rtn.errMsg = rtn.errMsg + " (code 005)"
											return rtn
										end		
									end	
								end
							end	
							*/	
							
						
							rtn.ok = true
							rtn.actionPerformed = .fActionPerformed
							rtn.node = node	
							
							return rtn
						end

	
	public function Assoc HandleNameCollision(Object prgCtx, \
											  DAPINODE node, \
											  String name, \
											  Frame apiDataObj,\
											  Assoc options, \
											  Assoc actionFlags, \
											  String filePath, \
											  Object llNode, \
											  DAPINODE parentNode)
			
				String		actionPerformed = ""
				Assoc		collisionOptions = options.nameCollisions
			
				// *** if there'sw a subtype mismatch, we are in trouble	
				if llNode.fSubType != node.pSubtype
					
					return .SetErrObj(.InitErrObj("HandleNameCollision"), Str.Format("There is already an object with the name %1, but the subtype is different than the object you specified. Possible $TARGETPATH error. Aborting any update operations.", node.pName, node.pId))
														
				elseif llNode.fVersioned && (collisionOptions.docs.addVersion || collisionOptions.docs.addVersionIfNewer || collisionOptions.docs.updateMetadata || collisionOptions.docs.rename)
			
					return ._HandleDocNameCollision(prgCtx, node, name, apiDataObj, options, actionFlags, filePath, llNode, parentNode)
					
				elseif !llNode.fVersioned && collisionOptions.containers.updateMetadata
				
					return ._HandleContainerNameCollision(prgCtx, node, name, apiDataObj, options, actionFlags, filePath, llNode, parentNode)
				
				elseif (llNode.fVersioned && collisionOptions.docs.doNothing) || collisionOptions.containers.doNothing
				
					Assoc rtn
					rtn.ok = true
					rtn.actionPerformed = actionPerformed
					return rtn		
					
				else
					// *** send back an error
					return .SetErrObj(.InitErrObj("PerformAction"), Str.Format("An item with the name '%1' already exists in the desired targetPath. Check name collision and target path settings.", name))
			
				end	
		
			
			end

	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, Frame apiDataObj, Assoc options, Assoc actionFlags, Object llNode, DAPINODE parentNode, DAPINODE node)
									
							Assoc 		rtn
							Assoc		status
							String		actionPerformed
							Assoc		collisionOptions = options.nameCollisions
							
							// important to get the name
							String		name = apiDataObj.getValue("name")
					
							// *** once last chance to get the parentNode if it was undefined (only needed for create and move)
							// *** TODO:  Need to add support for Compound Documents (.CD extension)
							if IsUndefined(parentNode)		
								status = .GetCreateParentNode(prgCtx, options, apiDataObj, node, name, options.autoCreateFolders)
								if !status.ok
									.SetErrObj(rtn, status)
									rtn.errMsg = "Error Code 011 " + rtn.errMsg
									return rtn
								else
									parentNode = status.result
								end	
							end	
						
							// *** are we using a template to create this object?
							Integer	subType = apiDataObj.getValue("subType")
							Integer templateID = apiDataObj.GetValue("templateId")
							Boolean isCreateFromTemplate = IsDefined(templateID) || subtype in {31066,31067} > 0
							Assoc 	metadataVals = apiDataObj.getValue("metadata")	
							String 	filePath = apiDataObj.GetValue("filePath")		
							List 	attrGroups
							
							if isCreateFromTemplate
					
								status =  $DataManager.TemplateUtils.HandleTemplateMetadata(prgCtx, templateId, name, metadataVals, subtype, parentNode)
								if !status.ok
									.SetErrObj(rtn, status)
									rtn.errMsg = "Error Code 013 " + rtn.errMsg
									return rtn
								end				
							
							elseif (actionFlags.doAttrData || options.inheritParent)
								List catIds =  metadataVals.catIds
						
								// *** build the attr groups to pass to action functions (in some cases, we need to take the values from metadataVals and massage into a format for WebService API calls)
								status = $DataManager.AttrGroupPrep.PrepareCatAttrsForCreate(prgCtx, parentNode, catIDs, metadataVals.attrVals, options.inheritParent, actionFlags)
								if !status.ok
									.SetErrObj(rtn, status)
									rtn.errMsg = "Error Code 012 " + rtn.errMsg
									return rtn
								else
									attrGroups = status.result.attrGroups
			
									if Length(status.result.inheritedCatDefs)
										String key		
										for key in Assoc.Keys(status.result.inheritedCatDefs)
											apiDataObj.AddCatDefByKey(key, status.result.inheritedCatDefs.(key))
										end
									end
									
									if status.categoriesUpgraded
										actionPerformed = "Categories upgraded."
									end
								end
							
							end
							
							// records management
							if actionFlags.doRecManData && IsDefined($RecMan) && $DataManager.NodeUtils.IsRecManagedSubtype(prgCtx, subType) 
								status = $DataManager.AttrGroupPrep.PrepareRecManAttrsForCreate(prgCtx, parentNode, options.inheritParent, subtype, metadataVals, attrGroups, actionFlags)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else	
									attrGroups = status.result
								end	
							end
							
							// set the attrGroups into our metadata Assoc
							metadataVals.attrGroups = attrGroups		
					
							// *** pass in the makeName unique flag if it's in the options
							Boolean makeNameUnique = (llNode.fVersioned && collisionOptions.docs.makeUnique) || (!llNode.fVersioned && collisionOptions.containers.makeUnique)
							
							// *** can we create it (ensure it does not already exist)
							if makeNameUnique || $DataManager.NodeAPI.IsUniqueName(prgCtx, parentNode, name)
							
								// *** pass off to sub-function to handle this logic (trying to avoid overly large functions)
								status = .CreateObject(prgCtx, \
											  		   name, \
											  		   apiDataObj, \
											   		   options, \
											  		   actionFlags, \
											  		   filePath, \
											  		   llNode, \
											  		   parentNode, \
											  		   makeNameUnique)
								
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("PerformAction"), status)
									return rtn	
								else
									node = status.node
									actionPerformed = status.actionPerformed
								end			
								
								// *** delete files if flag is present
								if llNode.fVersioned && IsDefined(filePath) 
								
									if options.archiveImportedFiles
										.ArchiveFile(filePath, options.fileArchivePath, options.rootSourcePath)	
				
									elseif options.removeImportedFiles
										.RemoveFile(filePath, options.fileRemovalPath)	
				
									end
			
								end	
								
								// *** avoid updating cats, class, and other stuff later because we already did it
								actionFlags.doAttrData = false
								actionFlags.doClassification = false
								actionFlags.doDapiNodeData = false
								actionFlags.doOwnerData = false
								actionFlags.doSystemAttrs = false
								actionFlags.doRecManData = false
								actionFlags.doPhysObjData = false	
													 
							else
							
								// *** we should be able to get the node now
								if IsUndefined(node)
									node = .DAPIGetNode(prgCtx, parentNode, name)
									if IsError(node)
										rtn = .SetErrObj(.InitErrObj("PerformAction"), Str.Format("NodeCreate: Could not get node from parentNode '%1' and name '%2'", parentNode.pId, name))
										rtn.errMsg = rtn.errMsg + " (code 005)"
										return rtn
									end
								end
								
								// *** pass off to sub-function to handle this logic (trying to avoid overly large functions)
								status = .HandleNameCollision(	prgCtx, \
														   		node, \
														  		name, \
														  		apiDataObj,\
														  		options, \
														  		actionFlags, \
														  		filePath, \
														  		llNode, \
														  		parentNode)
								
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("PerformAction"), status)
									return rtn	
								else
									actionPerformed = status.result
								end				
							
							end
							
						
							Assoc result
							result.node = node
							result.actionPerformed = actionPerformed
						
							return .SuccessResult(result)
						
						end

	
	override function Assoc PreProcessData(Object prgCtx, Assoc options, Object	apiDataObj, Object currentDataObj, Assoc actionFlags)
						
							// Get Current Data (if doing physical object or records management data)
							if ( actionFlags.doRecManData )
								Assoc status = $DataManager.RMPreProcessor.ProcessRMDataForCreate( prgCtx, apiDataObj, options.inheritParent )
								if !status.ok
									return .SetErrObj( .InitErrObj( "PreProcessData" ), status )
								end
							end		
			
							Assoc rtn
							rtn.ok = true
							rtn.result = undefined
							return rtn
					
							return rtn
					
						end

	
	override function Assoc ValidateItem(Object prgCtx, String action, Frame apiDataObj, Assoc dataFlags, Assoc options)
															
				Assoc 		status
			
				// *** grab some pieces of data from the apiDataObj
				Integer subType = apiDataObj.GetValueFromCol("$OBJECTTYPE")
				String	sourcePath = apiDataObj.GetValueFromCol("$SOURCEPATH")
				String 	fileName = apiDataObj.GetValueFromCol("$FILENAME")		
				String 	rootFilePath = apiDataObj.GetValue("rootFilePath")
				String 	filePath = apiDataObj.GetValue("filePath")
				
				// *** need to get subtype if we don't already have it from CSV (needto rule out needing a $SourcePath column)
				if IsUndefined(subtype)	
					// *** guess based on the file path							
					if IsUnDefined(filePath)
						status = .GetFilePath(options, fileName, sourcePath, rootFilePath)
						if !status.ok
							// KM 11-27-18 changed this to set error to whatever GetFilePath returned so we try to keep error messages unique
							return .SetErrObj(.InitErrObj("ValidateItem"), status)			
						else
							filePath = status.result
						end				
					end	
				
					if File.IsDir(filePath)
						subtype = IsDefined(options.containerType) ? options.containerType : $TypeFolder
					elseif File.Exists(filePath)
						subtype = $TypeDocument
					else
						return .SetErrObj(.InitErrObj("ValidateItem"), "Object type assumed to be Document, but no file could be found. You may need an $OBJECTTYPE column in your CSV to specify the subtype (e.g. 144, 0) of the object you want to create.")			
					end		
				end									
				
				// grab the llNode Object
				Object 	llNode = $LLIAPI.LLNodeSubsystem.GetItem(subtype)	
				
				// *** are we supposed to use this file's modify date for our create/modify dates?
				if llNode.fVersioned 
					// make sure we have a filepath							
					if IsUnDefined(filePath)
						// KM 12-19-18 must pass in false because we don't want a folder path
						status = .GetFilePath(options, fileName, sourcePath, rootFilePath, false)
						if !status.ok
							return .SetErrObj(.InitErrObj("ValidateItem"), status)			
						else
							filePath = status.result
						end				
					end				
						
					if (options.useFileModifyDate || options.useFileCreateDate)
						Assoc fileStats = File.Stat(filePath)
						if IsUndefined(fileStats)
							return .SetErrObj(.InitErrObj("ValidateItem"), Str.Format("Could not get file information to set create/modify dates for filePath %1", filePath))
						end
						
						if options.useFileCreateDate
							apiDataObj.SetValueFromCol("$CREATEDATE", fileStats.(File.StatLastChangeTime))
							apiDataObj.SetValueFromCol("$MODIFYDATE", fileStats.(File.StatLastChangeTime))
							dataFlags.hasDapiNodeData = true
						
						elseif options.useFileModifyDate
							apiDataObj.SetValueFromCol("$CREATEDATE", fileStats.(File.StatLastModifyTime))
							apiDataObj.SetValueFromCol("$MODIFYDATE", fileStats.(File.StatLastModifyTime))				
							dataFlags.hasDapiNodeData = true
											
						end
					end	
				end			
				
				// *** check for Selected Media column
				if subtype in {411, 412, 424}
					if IsUndefined(apiDataObj.GetValueFromCol("$SELECTEDMEDIA"))
						return .SetErrObj(.InitErrObj("ValidateItem"), "You need a $SELECTEDMEDIA column and value to indicate what Physical Item Type to create (e.g. 'Box').")
					end	
				end
				
				// set some values in the apiDataObj		
				apiDataObj.setValue("subType", subType)
				apiDataObj.setValue("filePath", filePath)	
				
				// *** action flags
				Assoc actionFlags = ._GetActionFlags(prgCtx, action, options, subtype, dataFlags)				
				
				// *** now we need to do standard validation for new object
				status = $DataManager.NewObjectInterface.ValidateCreate(prgCtx, subtype, options, apiDataObj, llNode)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				end	
				
				// *** Shortcut, check for 
				if llNode.fSubtype == 1
				
					Dynamic targetPath = apiDataObj.GetValueFromCol("$SHORTCUTTARGET")
					
					if IsDefined(targetPath)
					
						// *** KM 11/29/16 pass in a false for getVolume
						status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, targetPath, undefined, false)
						if status.ok
							apiDataObj.setValue("shortcutTargetID", status.result.pID)
						else
							// *** KM 9-21-17 fix for error message here
							return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), Str.Format("The path '%1' for the shortcut is invalid or you do not have permission to access the node.", targetPath))
						end	
					
					else
						return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), "You need the column $ShortcutTarget in order to create a shortcut.")	
					end
				
				end
				
				// Call any pre-processors
				status = .PreProcessData(prgCtx, options, apiDataObj, undefined, actionFlags)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				end				
				
																	
				Assoc 		rtn		
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.oldVals = undefined
				rtn.result.oldCsvData = undefined	
				rtn.result.nodeId = undefined
				rtn.result.actionFlags = actionFlags
				rtn.result.filePath = filePath												
				
				return rtn
				
												
			end

	
	private function Assoc _CheckNameCollisionError(Object llNode, \
																		Assoc options, \
																		String errMsg)
									
							Boolean isDoc = llNode.fVersioned
									
							if isDoc && options.nameCollisions.docs.reportErrors
								return .SetErrObj(.InitErrObj("_CheckNameCollisionErrors"), errMsg)
							elseif !isDoc && options.nameCollisions.containers.reportErrors 
								return .SetErrObj(.InitErrObj("_CheckNameCollisionErrors"), errMsg)
							end
						
							Assoc	rtn
							rtn.ok = true	
							return rtn
																
						end

	
	private function Boolean _FileIsNewer(	String filePath, DAPINODE node)
									
							Boolean isNewer = false
						
							Assoc fileStats = File.Stat(filePath)
							if IsUndefined(fileStats)
								// *** set error for logging
								.SetErrObj(.InitErrObj("_FileIsNewer"), Str.Format("Could not get file information to determine if file is newer %1", filePath))
								return false
							else
							
								Date fileDate = fileStats.(File.StatLastModifyTime)
							
								DAPIVERSION currentVersion = DAPI.GetVersion(node, DAPI.VERSION_GET_CURRENT)
								
								if IsError(currentVersion)
									// *** set error for logging
									.SetErrObj(.InitErrObj("_FileIsNewer"), Str.Format("Could not get current version to determine if file is newer %1", filePath))
									return false		
								else
									Date currentVDate = currentVersion.pCreateDate
									
									isNewer = fileDate > currentVDate			
								
								end	
							end
						
							return isNewer
															
						end

	
	private function Assoc _GetSetCreateInfo(	Object prgCtx, \
																	Assoc metadataVals, \	
																	Object apiDataObj, \						
																	Assoc options, \
																	Assoc actionFlags, \
																	String filePath, \
																	DAPINODE parentNode, \
																	Object llNode)
																		
					Assoc 		rtn = .InitErrObj( "_GetSetCreateInfo" )
					Assoc		status
					Object 		dapiCtx = prgCtx.DSession()
					
					// **** start with base Assoc	
					Assoc createInfo = Assoc.CreateAssoc()
					
					// *** versionInfo?
					if llNode.fVersioned
						String verFileName = apiDataObj.GetValueFromCol("$VERFILENAME")
						._GetFileInfo(options, createInfo, filePath, verFileName)
						
						// *** Use advanced versioning if the $VerMajor value is greater than 0 or true
						Boolean isAdvVersioning = apiDataObj.GetValueFromCol("$ADVVERSIONING")
						Boolean majorVal = apiDataObj.GetValueFromCol("$VERMAJOR")
					
						// *** Use advanced versioning 
						if isAdvVersioning || majorVal
							createInfo.AdvVersionControl = TRUE	
						end		
						
						// *** what about some of the version values?
						createInfo.fileSize = apiDataObj.GetValueFromCol("$VERFILESIZE")
						
						// *** KM 8-24-16 Need to make sure mime type is not already set
						String csvMimeType = apiDataObj.GetValueFromCol("$VERMIMETYPE")
						if IsDefined(csvMimeType)
							createInfo.mimeType = csvMimeType
						end
						
						// *** set any of the following if we have them		
						Assoc versionInfo
						versionInfo.pCreateDate  = apiDataObj.GetValueFromCol("$VERCREATEDATE")
						versionInfo.pOwner = apiDataObj.GetValueFromCol("$VERCREATOR")
						versionInfo.pModifyDate = apiDataObj.GetValueFromCol("$VERMODIFIEDDATE")
						versionInfo.pFileName = createInfo.fileName
						createInfo.versionInfo = versionInfo	
						
					end		
							
					// *** Shortcut?
					if llNode.fSubType==1
						Integer targetID = apiDataObj.GetValue("shortcutTargetID")
						
						DAPINODE targetNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, targetID)
						if IsError(targetNode)
							.SetErrObj(rtn, Str.Format("Could not get shortcut target from ID %1", targetID))
							return rtn
						else
							createInfo.sourceNode = targetNode
						end
					end
						
					// *** hardcode these flags for now
					Boolean	splitTran = false
					Boolean parentUpdateAfterCreate = true
					
					// *** set up creatInfo constants we want 
					createInfo.parent = parentNode
					createInfo.dapiCtx = dapiCtx
					createInfo.splitTran = splitTran
					createInfo.isCreate = TRUE
					createInfo.createOptions = 0
					createInfo.parentUpdateAfterCreate = false
					createInfo.createOptions = !parentUpdateAfterCreate ? DAPI.CREATE_SKIP_PARENT_UPDATE : 0
					createInfo.noParentUpdate = !parentUpdateAfterCreate
					createInfo.request = Assoc.CreateAssoc()
					createInfo.request.Comment = undefined	
						
					// *** KM 10-28-16 required for ADN
					createInfo.request.prgCtx = prgCtx
					
					// *** prepare attributes for create with attrData
					if actionFlags.doAttrData || actionFlags.doRecManData || IsDefined($ADN)
					
						if IsDefined($ADN)
							status = $DataManager.AutoDocumentUtils.HandleADN(prgCtx, apiDataObj, llNode, parentNode, metadataVals.attrGroups, createInfo)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							else
								metadataVals.attrGroups = status.result	
							end					
						end
					
						// *** Make use of the DocMan WebService again to prepare the values for create.  It will set the values into createInfo
						status = $DocManService.AttributeHandlerRegistry.PrepareAttributesForCreate( prgCtx, llNode, parentNode, metadataVals.attrGroups, createInfo )
						if !status.ok
							.SetErrObj(rtn, status)
							return rtn
						end	
									
					end	
						
					// *** classifications
					if actionFlags.doClassification
						createInfo.request.('CLASS_ID_List') = Str.ValueToString(metadataVals.classIds)
					end
					
					if actionFlags.doPhysObjData
						createInfo.request = Assoc.Merge(createInfo.request, metadataVals.poData.request)
					
						// *** KM Fix for 8/3/16 patch
						if IsFeature(apiDataObj, "_fApiVals") && IsFeature(apiDataObj._fApiVals, "poCreateData") && IsFeature(apiDataObj._fApiVals.poCreateData, "request")
							createInfo.request = Assoc.Merge(createInfo.request, apiDataObj._fApiVals.poCreateData.request)		
						end
				
						// *** also do we have boxing data we can use in create?
						if IsDefined(metadataVals.poData.boxData) && IsDefined(metadataVals.poData.boxData.boxID)
							// sigh, Physical Objects module expects it in string form
							createInfo.request.boxID = Str.String(metadataVals.poData.boxData.boxID)
						end
				
					end
					
					// *** email data	
					if actionFlags.doEmailData && llNode.fSubType == 749
						._SetEmailCreateInfo(prgCtx, metadataVals, options, filePath, createInfo)
					end	
					
					// URL?
					if llNode.fSubType==140
						createInfo.request.TargetUrl = apiDataObj.GetValue("targetURL")
					end
						
					return .SuccessResult(createInfo)
						
				end

	
	private function Assoc _HandleContainerNameCollision (Object prgCtx, \
																			  DAPINODE node, \
																			  String name, \
																			  Frame apiDataObj,\
																			  Assoc options, \
																			  Assoc actionFlags, \
																			  String filePath, \
																			  Object llNode, \
																			  DAPINODE parentNode)
							Assoc 		rtn
							Assoc		status
							String		actionPerformed = ""
							Assoc		collisionOptions = options.nameCollisions
						
							
							// *** set action flags according to what user specified
							actionFlags.doMetadataUpdate = collisionOptions.containers.updateMetadata
							actionFlags.doRename = collisionOptions.containers.rename	
							
							// *** make sure we set the correct values in apiDataObj
							apiDataObj.setValue("subType", node.pSubType)
							apiDataObj.setValue("newName", apiDataObj.getValue("name"))
							apiDataObj.setValue("parentId", node.pParentID)
							apiDataObj.setValue("volumeId", node.pVolumeID)
							apiDataObj.setValue("nodeId", node.pID)
									
							// *** make sure to turn inherit attributes off - since this is now an attribute update
							options.inheritParent = false				
					
							// *** Process Item					
							status = $DataManager.NodeUpdate.ProcessItem(prgCtx, "UPDATE", apiDataObj, actionFlags, options)
							if !status.ok
								rtn = .SetErrObj(.InitErrObj("_HandleContainerNameCollision"), status)
								rtn.errMsg = rtn.errMsg + " (code 006)"
								return rtn
							else
								actionPerformed = status.result.actionPerformed
							end			
					
							// *** now need to turn off updating so that it doesn't get done post action
							actionFlags.doMetadataUpdate = false
							actionFlags.doRename = false	
					
							// *** the only post action that we would do is udpate permissions
							actionFlags.doUpdate = actionFlags.doPermUpdate
							
							rtn.ok = true
							rtn.result = actionPerformed
						
							return rtn
						
						end

	
	private function Assoc _HandleDocNameCollision(Object prgCtx, \
																	  DAPINODE node, \
																	  String name, \
																	  Frame apiDataObj,\
																	  Assoc options, \
																	  Assoc actionFlags, \
																	  String filePath, \
																	  Object llNode, \
																	  DAPINODE parentNode)
							Assoc 		rtn
							Assoc		status
							String		actionPerformed = ""
							Assoc		collisionOptions = options.nameCollisions
							
							// save this for later
							Boolean doAttrData = actionFlags.doAttrData
						
							if collisionOptions.docs.addVersion || (collisionOptions.docs.addVersionIfNewer && ._FileIsNewer(filePath, node))
						
								// *** call AddVersion action, but first turn off the doAttrData flag
								actionFlags.doAttrData = false
								
								// *** Now all Perform Action				
								status =  $DataManager.NodeAddVersion.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("PerformAction"), status)
									rtn.errMsg = rtn.errMsg + " (code 007)"
									return rtn
								else
									actionPerformed = status.result.actionPerformed				
								end
						
							end	
							
							if collisionOptions.docs.updateMetadata || collisionOptions.docs.rename
							
								// *** Reset the doAttrData flag
								actionFlags.doAttrData = doAttrData
						
								// *** set action flags according to what user specified
								actionFlags.doMetadataUpdate = collisionOptions.docs.updateMetadata
								
								// setting this turns on metadata update
								options.actionMetadata = actionFlags.doMetadataUpdate
								
								// rename too?
								actionFlags.doRename = collisionOptions.docs.rename
								
								// *** make sure we set the correct values in apiDataObj
								apiDataObj.setValue("subType", node.pSubType)
								apiDataObj.setValue("newName", apiDataObj.getValue("name"))
								apiDataObj.setValue("parentId", node.pParentID)
								apiDataObj.setValue("volumeId", node.pVolumeID)
								apiDataObj.setValue("nodeId", node.pID)
								
								// *** make sure to turn inherit attributes off - since this is now an attribute update
								options.inheritParent = false
								
								// *** validate and then process
								Object updateProcess = $DataManager.NodeUpdate
								
								// we need to re-validate in order to get the correct data for updating now (instead of creating)
								status = updateProcess.ValidateItem(prgCtx, "UPDATE", apiDataObj, options.dataFlags, options)
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("HandleNameCollision"), status)
									rtn.errMsg = rtn.errMsg + " (code 019)"
									return rtn
								else
									actionFlags = status.result.actionFlags
								end	
								
								// now put through the Process Item					
								status = updateProcess.ProcessItem(prgCtx, "UPDATE", apiDataObj, actionFlags, options)
								if !status.ok
									rtn = .SetErrObj(.InitErrObj("HandleNameCollision"), status)
									rtn.errMsg = rtn.errMsg + " (code 006)"
									return rtn
								else
									// *** KM 8-16-17 concatenate here so we don't lose the fact that we may have added version above
									actionPerformed += status.result.actionPerformed
								end	
								
								// *** now need to turn off updating so that it doesn't get done post action
								actionFlags.doMetadataUpdate = false
								actionFlags.doRename = false
						
								// *** the only post action that we would do is udpate permissions
								actionFlags.doUpdate = actionFlags.doPermUpdate	
							end
					
							rtn.ok = true
							rtn.result = actionPerformed
						
							return rtn
						
						end

	script _SetEmailCreateInfo
	
			
					
							
									
											
												function Assoc _SetEmailCreateInfo(	Object prgCtx, \
																					Assoc metadataVals, \							
																					Assoc options, \
																					String filePath, \
																					Assoc createInfo)
												
													Assoc	status	
													Assoc	emailData = metadataVals.emailData
												
													// *** get the body	from the msg file
													status = _GetBody(filePath)
													if !status.ok
														return .SetErrObj(.InitErrObj("_SetEmailCreateInfo"), status)
													else
														emailData.Body = status.result
													end	
													
													// *** get the particpants from msg file if we don't have them
													if IsUndefined(emailData.Participants) || Length(emailData.Participants)==0
														status = _GetParticipants(filePath)
														if !status.ok
															return .SetErrObj(.InitErrObj("_SetEmailCreateInfo"), status)
														else
															emailData.Participants = status.result
														end			
													end
												
													// *** default in some other values
													if !IsFeature(emailData, "Version")
														emailData.Version = 4
													end	
													if !IsFeature(emailData.Properties, "BodyFormat")	
														emailData.Properties.BodyFormat = 1
													end
													if !IsFeature(emailData.Properties, "HasAttachments")		
														emailData.Properties.HasAttachments	= 0
													end	
												
													// *** set the email properties	
													createInfo.Request.EmailProperties = emailData
													
													Assoc rtn
													rtn.ok = true
													
													return rtn
																					
												end			
												
												function Assoc _GetParticipants(String filePath)
												
													// *** call the GetHeader Method
													Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.EmailMsgParser", "GetMsgParticipants", {filePath} )
													
													// *** parse the Java Error and return
													if IsError(result)
														return ._ReturnJavaError(.InitErrObj("_GetParticipants"), result)
													
													else
														// *** loop through result and set the participant types
														Assoc p
														for p in result
															if (p.ParticipantType == "to") 
																				
																p.ParticipantType = $OTEmailType_To
																
															elseif ( p.ParticipantType == "cc" ) 
																
																p.ParticipantType = $OTEmailType_CC
															
															elseif ( p.ParticipantType == "bcc" ) 
																
																p.ParticipantType = $OTEmailType_BCC
																
															elseif ( p.ParticipantType == "forward" ) 
																
																p.ParticipantType = $OTEmailType_MailboxForward
																
															elseif ( p.ParticipantType == "from" ) 
																
																p.ParticipantType = $OTEmailType_From
															
															elseif ( p.ParticipantType == "onbehalfof" ) 
																
																p.ParticipantType = $OTEmailType_OnBehalfOf
																
															end				
														end
													end
													
													// *** return success
													Assoc rtn
													rtn.ok = true
													rtn.result = result	
													
													return rtn	
												
												
												end
												
												function Assoc _GetBody(String filePath)
												
													// *** call the GetMsgBody Method
													Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.EmailMsgParser", "GetMsgBody", {filePath} )
													
													// *** parse the Java Error and return
													if IsError(result)
														return ._ReturnJavaError(.InitErrObj("_GetBody"), result)
													end
													
													// *** return success
													Assoc rtn
													rtn.ok = true
													rtn.result = result	
													
													return rtn	
												
												
												end
												
												
																				
											
										
									
								
							
						
					
				
			
		
	
	endscript

end

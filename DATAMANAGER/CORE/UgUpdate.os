package DATAMANAGER::CORE

public object UgUpdate inherits DATAMANAGER::CORE::UserGroupProcesses

	override	List	_fRequiredColsGroup = { '$ID' }
	override	List	_fRequiredColsUser = { '$ID' }
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'UgUpdate'
	override	String	fSubKey = 'UserGroup.Update'


	/* must implement this signature */
	override function Assoc PerformAction(	Object prgCtx, \
											Frame apiDataObj, \
											Assoc options, \
											Assoc actionFlags)
			
				Assoc		status
				Object		uSession = prgCtx.USession()
				Object		usersPkg = $LLIAPI.UsersPkg
				Integer 	typeVal = apiDataObj.GetValueFromCol("$TYPE")
				Assoc		ugData
				String		actionPerformed
				
				switch typeVal
				
					case UAPI.USER
					
						status = ._SetUserData(prgCtx, options, apiDataObj)
						if !status.ok
							return .SetErrObj(.InitErrObj( "PerformAction" ), status)
						elseif !status.result.changed
							actionPerformed = "No Action Performed. There was no change in the user data from the CSV values."
						else
							ugData = status.result.ugData
												
							/*	
							function Assoc UserUpdate( \
								Object		uapiCtx, \										The UapiSession object.
								Integer		userID, \										The ID of the user.
								String		newUserName = Undefined, \						The new login name of the user.
								String		newGroupName = Undefined, \						The new group name of the user. (deprecated)
								Integer		newPrivs = Undefined, \							The new privileges of the user.
								Assoc		newExAttrs = Undefined, \						The lastName, the middleName, the firstName and the mailAddress of the user.
								String		oldPassword = Undefined, \						The old password of the user.
								String		newPassword = Undefined, \						The new password of the user.
								Dynamic		context = Undefined, \							The generic request handler object
								Dynamic		newUserData = Undefined, \						The userdata of the user.
								Assoc		expireDateAssoc = Undefined, \					expireDateAssoc		The password expiration date
								Integer		newGroupID = Undefined, \  groupID				The ID of the new default group.
								Boolean		allowPwdExpiration = Undefined )				Whether a users password can expire or not.  If undefined, then don't change. f false, then user's password is not subject to expiration.
							*/
						
							status = usersPkg.UserUpdate(uSession, \
														ugData.ID, \
														ugData.NAME, \
														ugData.Department, \
														ugData.USERPRIVILEGES, \
														ugData.ExAttrs, \
														undefined, \
														ugDATA.USERPWD, \
														undefined, \
														ugData.USERDATA, \
														ugData.expireDataAssoc, \
														ugData.GROUPID)
															
							if !status.ok
								// *** rollback
								prgCtx.fDbConnect.EndTrans(false)						
								return .SetErrObj(.InitErrObj( "PerformAction" ), status)
							else
								actionPerformed = "User Updated"	
							end	
						end	
														
					end
					
					case UAPI.GROUP
					
						status = ._SetGroupData(prgCtx, options, apiDataObj)
						if !status.ok
							return .SetErrObj(.InitErrObj( "PerformAction" ), status)
						elseif !status.result.changed
							actionPerformed = "No Action Performed. There was no change in the group data from the CSV values."
						else
							ugData = status.result.ugData
							
							/* api
								//		uapiCtx			The UapiSession object.
								//		groupID			The id of the group.
								//		newGroupName	The new name of the group.
								//		newUsers		The list of users or groups for this group (ignored if addUsers or delUsers is defined).
								//		addUsers		The list of users to be added.
								//		delUsers		The list of users to be deleted.
								//		newLeaderID		The id of the new leader.
								//		newUserData		The userdata of the group.			
							*/
							
							
							// *** call LLIAPI.UsersPkg function
							status = usersPkg.GroupUpdate(	uSession, \
															ugData.ID, \
															ugData.NAME, \
															ugData.newUsers, \
															ugData.addUsers,\
															ugData.delUsers, \
															ugData.LEADERID, \
															ugData.USERDATA)
				
							if !status.ok
								// *** rollback
								prgCtx.fDbConnect.EndTrans(false)			
								return .SetErrObj(.InitErrObj( "PerformAction" ), status)
							else
								actionPerformed = "Group Updated"				
							end					
							
						end
			
					end	
				
				end
			
				Assoc 	rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.actionPerformed = actionPerformed	
				return rtn
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
														Assoc actionFlags, \
														Assoc options, \
														Object apiDataObj, \
														Integer typeVal)
				Assoc			status
				UAPISESSION		uSession = prgCtx.USession().fSession
				
				// *** does the user/group exist?
				status = ._FindUserGroup(prgCtx, apiDataObj)
				if !status.ok
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
				end		
			
				// *** get user record
				Integer		id = apiDataObj.GetValueFromCol("$ID")
				Record	ugRec
				RecArray ugRecs = UAPI.GetById(uSession, id)
				if Length(ugRecs)
					ugRec = ugRecs[1]
				end	
			
				// *** check privileges
				if IsError(UAPI.CheckAlterPrivs( uSession, ugRec ))
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), "You do not have sufficient privileges to update this user/group")
				end	
			
				actionFlags.update = true
				
				Assoc rtn
				rtn.ok = true
				return rtn
			
			end

	
	private function Assoc _SetGroupData(Object prgCtx, \
													Assoc options, \
													Object apiDataObj)
							
						
				Assoc 	status
				Assoc 	ugData
				Boolean	changed = false
				
				// *** get the userId
				Integer groupID = apiDataObj.GetValueFromCol("$ID")
				
				// *** start out with current vals pre-populated
				status = ._UserRecFromID(prgCtx, groupID, UAPI.GROUP)	
				if !status.ok
					return .SetErrObj(.InitErrObj("_SetGroupData"), status)
				else
					ugData = ._UserRecToUgData(status.result)
				end		
				
				// *** build an assoc we will use for api functions
				List 	groupFields = {"NAME","LEADERID","USERDATA","SPACEID"}
				String	fName
				for fName in groupFields
					Dynamic val = apiDataObj.GetValueFromCol("$" + fName)
				
					if IsDefined(val) && val != ugData.(fName)
						changed = true
						ugData.(fName) = val
					end	
				end
				
				// *** fields used for group memberhip
				ugData.newUsers = undefined
				ugData.addUsers = undefined
				ugData.delUsers = undefined
				
				if options.updateMemberOption == "merge"
					ugData.addUsers = apiDataObj.GetValueFromCol("$MEMBERS")
					// *** need to set changed flag
					changed = true
				elseif options.updateMemberOption == "clear"
					// *** need to set changed flag
					changed = true
					ugData.newUsers = apiDataObj.GetValueFromCol("$MEMBERS")
				end	
			
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.ugData = ugData	
				rtn.result.changed = changed
				
				return rtn
								
			end

	
	private function Assoc _SetUserData(Object prgCtx, \
												Assoc options, \
												Object apiDataObj)
							
				Assoc	status
				Assoc 	ugData
				Boolean	changed = false
			
				// *** get the userId
				Integer userId = apiDataObj.GetValueFromCol("$ID")
				
				// *** start out with current vals pre-populated
				status = ._UserRecFromID(prgCtx, userId, UAPI.USER)	
				if !status.ok
					return .SetErrObj(.InitErrObj("_SetUserData"), status)
				else
					ugData = ._UserRecToUgData(status.result)
				end	
				
				// *** set department to undefined (deprecated parameter)
				ugData.Department	= undefined  			
			
				// *** build an assoc we will use for api functions
				List 	userFields = {"NAME","USERPWD","USERPRIVILEGES","USERDATA","GROUPID","SPACEID","LASTNAME","MIDDLENAME","FIRSTNAME","CONTACT","FIRSTNAME","LASTNAME","MAILADDRESS","TITLE","OFFICELOCATION"}
				String	fName
				for fName in userFields
					Dynamic val = apiDataObj.GetValueFromCol("$" + fName)
				
					if IsDefined(val)
						
						switch fname
							case "CONTACT", "FAX", "FIRSTNAME", "LASTNAME", "MIDDLENAME", "MAILADDRESS", "TITLE", "TIMEZONE", "OFFICELOCATION"
								if val != ugData.ExAttrs.(Str.Lower(fName))
									changed = true
									ugData.ExAttrs.(Str.Lower(fName)) = val
								end	
							end
				
							default
								if val != ugData.(fName)
									changed = true
									ugData.(fName) = val
								end	
							end
						end
					end	
				end
			
				
				if IsDefined(ugData.("$PwdExpireDate")) && Type(ugData.("$PwdExpireDate")) == DateType
					ugData.ExpireDateAssoc	= Assoc.CreateAssoc()
					ugData.expireDateAssoc.Date = ugData.("$PwdExpireDate")
				end	
				
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.ugData = ugData	
				rtn.result.changed = changed
				
				return rtn
					
								
			end

	
	private function Assoc _UserRecToUgData(Record ugRec)
						
				Assoc ugData
			
				ugData.ID	= ugRec.ID
				ugData.GROUPID = ugRec.OWNERID
				ugData.SPACEID = ugRec.SPACEID
				ugData.NAME = ugRec.NAME
				ugData.USERDATA = ugRec.USERDATA
				ugData.LEADERID = ugRec.LEADERID
				ugData.DELETED = ugRec.DELETED
				ugData.GROUPID = ugRec.GROUPID
				ugData.USERPRIVILEGES = ugRec.USERPRIVILEGES
				
				
				Assoc ExAttrs
				ExAttrs.LASTNAME = ugRec.LASTNAME
				ExAttrs.MIDDLENAME = ugRec.MIDDLENAME
				ExAttrs.FIRSTNAME = ugRec.FIRSTNAME
				ExAttrs.MAILADDRESS = ugRec.MAILADDRESS
				ExAttrs.CONTACT = ugRec.CONTACT
				ExAttrs.TITLE = ugRec.TITLE
				ExAttrs.SETTINGSNUM = ugRec.SETTINGSNUM
				ExAttrs.FAX = ugRec.FAX
				ExAttrs.OFFICELOCATION = ugRec.OFFICELOCATION
				ExAttrs.TIMEZONE = ugRec.TIMEZONE
				ExAttrs.PHOTOID = ugRec.PHOTOID
				ExAttrs.GENDER = ugRec.GENDER
				ExAttrs.BIRTHDAY = ugRec.BIRTHDAY
				ExAttrs.PERSONALEMAIL = ugRec.PERSONALEMAIL
				ExAttrs.HOMEADDRESS1 = ugRec.HOMEADDRESS1
				ExAttrs.HOMEADDRESS2 = ugRec.HOMEADDRESS2
				ExAttrs.HOMEPHONE = ugRec.HOMEPHONE
				ExAttrs.HOMEFAX = ugRec.HOMEFAX
				ExAttrs.CELLULARPHONE = ugRec.CELLULARPHONE
				ExAttrs.PAGER = ugRec.PAGER
				ExAttrs.HOMEPAGE = ugRec.HOMEPAGE
				ExAttrs.FAVORITES1 = ugRec.FAVORITES1
				ExAttrs.FAVORITES2 = ugRec.FAVORITES2
				ExAttrs.FAVORITES3 = ugRec.FAVORITES3
				ExAttrs.INTERESTS = ugRec.INTERESTS
				ugData.ExAttrs = ExAttrs
			
				if IsDefined(ugRec.PWDEXPIREDATE)
					ugData.ExpireDateAssoc	= Assoc.CreateAssoc()
					ugData.expireDateAssoc.Date = ugRec.PWDEXPIREDATE
				else
					ugData.ExpireDateAssoc = undefined
				end	
					
				return ugData
			
			end

end

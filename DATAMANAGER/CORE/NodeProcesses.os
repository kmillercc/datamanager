package DATAMANAGER::CORE

public object NodeProcesses inherits DATAMANAGER::LoadAPIProcesses

	public		String	fActionPerformed
	public		Boolean	fAllowPostActionUpdate = TRUE


	
	public function void ArchiveFile(String filePath, String archivePath, String rootSourcePath)
					
				Assoc status			
				Object logger = .GetLogger()
			
				String dest = Str.ReplaceAll(filePath, rootSourcePath, archivePath)
				if dest == filePath
					// means that the sourcePath is CSV
					rootSourcePath = $DataManager.FileUtils.GetParentPath(rootSourcePath)
					dest = Str.ReplaceAll(filePath, rootSourcePath, archivePath)
					if dest == filePath
						logger.ERROR(Str.Tab() + Str.Tab() + "File Archive ERROR: Could node determine archive path.")
					end	
				end	
			
			
				// make sure the folder path exists
				String destParentPath = $DataManager.FileUtils.GetParentPath(dest)	
				status = $DataManager.FileUtils.EnsurePathExists(destParentPath)
			
				if status.ok	
					status =  $Kernel.FileUtils.Move( filePath, dest )
				
					if status.ok
						logger.INFO(Str.Format(Str.Tab() + Str.Tab() + "File Archive SUCCESS: Moved file from '%1' to '%2'", filePath, dest))
					else
						logger.ERROR(Str.Format(Str.Tab() + Str.Tab() + "File Archive ERROR: Could node move file from '%1' to '%2'. Error: %3", filePath, dest, status.errMsg))
					end			
				else
					logger.ERROR(Str.Format(Str.Tab() + Str.Tab() + "File Archive ERROR: Could node move file from '%1' to '%2'. Error: %3", filePath, dest, status.errMsg))
				end	
									
			end

	
	override function Assoc Finalize(Assoc options)
				Assoc rtn
				rtn.ok = true
				if options.archiveCsvFiles
					String csvFilePath
					for csvFilePath in options.csvFiles
						.ArchiveFile(csvFilePath, options.csvArchivePath, options.rootSourcePath)	
					end
				end
				return rtn
			end

	
	public function Assoc GetCreateParentNode(Object prgCtx, Assoc options, Frame apiDataObj, DAPINODE node, String objectName, Boolean autoCreate)
				
					DAPINODE	parentNode
					Assoc		status
					
					// *** do we have a parentId in the apiDataObj assoc from previous validation run
					Integer parentId = apiDataObj.GetValue("parentID")
					Integer volumeId = apiDataObj.GetValue("volumeID")
					String	parentPath = apiDataObj.GetValue("parentPath")
							
					if IsDefined(parentId)
						parentNode = DAPI.GetNodeById(prgCtx.DapiSess(), volumeId, parentId)
						if IsError(parentNode)
							return .SetErrObj(.InitErrObj("GetCreateParentNode"), Str.Format("Could not get parentNode from parentId %1", parentId))
						end	
			
					// try targetpath		
					elseif IsDefined(parentPath)
				
						// // determine which method to use based on auto create settings
						if autoCreate
							status = $DataManager.NodeUtils.GetCreateNodeFromPath(prgCtx, parentPath, 10, options.containerType)
						else
							status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, parentPath)					
						end		
				
						if !status.ok
							return .SetErrObj(.InitErrObj("GetCreateParentNode"), status)
						else
							parentNode = status.result
						end
					else 
						return .SetErrObj(.InitErrObj("GetCreateParentNode"), 'You need either a $TargetPath column or $ParentId column in your CSV for this action.')
					end
					
					// *** check and make sure we don't need to get a volume (i.e. for projects)
					Object llNode = $LLIAPI.llNodeSubsystem.GetItem(parentNode.pSubtype)
					if llNode.fAssociatedVolume != 0
						// *** get the actual node
						DAPINODE volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(parentNode.pId))
						if IsNotError(volNode)
							parentNode = volNode			
						elseif !llNode.fAssociatedVolumeOptional
							return .SetErrObj(.InitErrObj("GetParentNode"), Str.Format("Could not get volume node for node %1 [dataid %2]", parentNode.pId, parentNode.pName))				
						end
					end		
					
					Assoc rtn
					rtn.ok = true
					rtn.result = parentNode
					return rtn
					
				end

	
	public function Assoc GetCurrentData(Dynamic prgCtx, DAPINODE node, Dynamic	apiDataObj, Dynamic	dataFlags, Object llNode)
					
					Assoc rtn
					rtn.ok = true
					rtn.result = undefined
					return rtn
					
				end

	
	public function Assoc GetFilePath(Assoc options, String fileName, String sourcePath, String rootFilePath, Boolean allowFolderPath = true)		/* KM 5-2-16 added this parameter as part of patch */
							
				List	possiblePaths = {}
				String	filePath = undefined
				
				// *** make sure we got something (i.e. this is a create or addversion operation on a versioned object)
				if IsUnDefined(fileName) && IsUndefined(sourcePath)
					return .SetErrObj(.InitErrObj("_GetFilePath"), "You need either a $SourcePath or $FileName column in your CSV for this operation.")
				end	
				
				// *** sourcePath + fileName
				if IsDefined(sourcePath) && IsDefined(fileName)
					if options.rootFileLoc == "absolute"
						possiblePaths = List.SetAdd(possiblePaths, $DataManager.FileUtils.EnsureDirSeparator(sourcePath) + fileName)
						possiblePaths = List.SetAdd(possiblePaths, rootFilePath + $DataManager.FileUtils.EnsureDirSeparator(sourcePath) + fileName)
					else
						possiblePaths = List.SetAdd(possiblePaths, rootFilePath + $DataManager.FileUtils.EnsureDirSeparator(sourcePath) + fileName)
						possiblePaths = List.SetAdd(possiblePaths, $DataManager.FileUtils.EnsureDirSeparator(sourcePath) + fileName)		
					end	
				end	
				
				// *** sourcePath alone
				if IsDefined(sourcePath) && (allowFolderPath || !(File.IsDirSpec(sourcePath))) /* KM 5-2-16 added this parameter as part of patch */
					if options.rootFileLoc == "absolute"
						possiblePaths = List.SetAdd(possiblePaths, sourcePath)
						possiblePaths = List.SetAdd(possiblePaths, rootFilePath + sourcePath)	
					else
						possiblePaths = List.SetAdd(possiblePaths, rootFilePath + sourcePath)				
						possiblePaths = List.SetAdd(possiblePaths, sourcePath)		
					end	
				end	
				
				// *** fileName alone
				if IsDefined(fileName) && !(File.IsDirSpec(fileName))
					if options.rootFileLoc == "absolute"
						possiblePaths = List.SetAdd(possiblePaths, fileName)
						possiblePaths = List.SetAdd(possiblePaths, rootFilePath + fileName)		
					else
						possiblePaths = List.SetAdd(possiblePaths, rootFilePath + fileName)				
						possiblePaths = List.SetAdd(possiblePaths, fileName)			
					end	
				end		
				
				// *** Loop through possible Paths
				String path
			
				// *** KM 1-3-19 adding some logging to figure out which paths work
				Object logger = .GetLogger()
				
				for path in possiblePaths
					if File.Exists(path)
						logger.Debug(Str.Format(Str.Tab() + "FilePath '%1' exists.", path))
						
						/* KM 5-2-16 added this parameter as part of patch */
						Boolean isFolderPath = File.IsDirSpec(path)
			
						if allowFolderPath || !isFolderPath
							logger.Trace(Str.Format(Str.Tab() + "allowFolderPath=%1, isFolderPath=%2", allowFolderPath, isFolderPath))
							filePath = path
							break
						end	
					else
						logger.Debug(Str.Format(Str.Tab() + "FilePath '%1' does not exist.", path))
					end
				end
			
				if IsUndefined(filePath)
					if IsDefined(sourcePath) && IsDefined(fileName)
						return .SetErrObj(.InitErrObj("ValidateItem"), Str.Format("$SourcePath + $FileName %1 does not exist in Server File Location.", sourcePath + fileName))
					elseif IsDefined(sourcePath)
						return .SetErrObj(.InitErrObj("ValidateItem"), Str.Format("$SourcePath %1 does not exist in Server File Location.", sourcePath))
					elseif IsDefined(fileName)
						return .SetErrObj(.InitErrObj("ValidateItem"), Str.Format("$FileName %1 does not exist in Server File Location.", fileName))		
					else		
						// this condition is not possible because of conditional at the top of the page 	
					end
				end
				
				Assoc rtn
				rtn.result = filePath
				rtn.ok = true
				return rtn
				
			end

	
	override function Assoc NewJobPayload(Object prgCtx, Assoc options, String action)
														
					// *** Initialize payload Assoc 
					Assoc payload
					
					// *** Setup a new node cache that we will use
					// *** KM 12-10-15 need to avoid overwriting the root node
					$DataManager.NodeCache = $DataManager.NodeCacheRoot.New()
					
					// *** name collisions
					._SetupNameCollisionOptions(options)
					
					// *** for copy
					options.copyCurrentV = IsFeature(options, "actionCopyCurrentV") && options.actionCopyCurrentV
					
					// *** default in some options if they aren't there
					if !IsFeature(options, "targetPathType")
						options.targetPathType = "object"
					end	
					
					// *** put in some flags for checkboxes that may not have come with form
					options.autoCreateFolders = IsFeature(options, "autoCreateFolders") && options.autoCreateFolders
					options.autoCreateCD = IsFeature(options, "autoCreateCD") && options.autoCreateCD
					options.inheritParent = IsFeature(options, "inheritParent") && options.inheritParent
					options.useFileModifyDate = IsFeature(options, "useFileModifyDate") && options.useFileModifyDate
					options.useFileCreateDate = IsFeature(options, "useFileCreateDate") && options.useFileCreateDate
					
					// *** include Root
					options.includeRoot = IsFeature(options, "includeRoot") && options.includeRoot
					
					// 4-10-19 checkRootPath for OMERS
					options.checkRootPath = IsFeature(options, "checkRootPath") && options.checkRootPath
					
					// *** KM 4-1-16 File Removal Settings
					options.removeImportedFiles = IsFeature(options, "removeImportedFiles") && options.removeImportedFiles
					if options.removeImportedFiles 
						Assoc status = ._GetFileRemovalPath(options)
						
						if status.ok
							options.fileRemovalPath = status.result
						else
							.GetLogger().ERROR(Str.Format("Error getting file removal path: %1. Will not be able to remove files.", status.errMsg))
							options.removeImportedFiles = false
						end		
					end
			
					
					// Archive files options - Update 4 for OMERS
					options.archiveImportedFiles = IsFeature(options, "archiveImportedFiles") && options.archiveImportedFiles
					if options.archiveImportedFiles 
						Assoc status = ._GetFileArchivePath(options)
						
						if status.ok
							options.fileArchivePath = status.result
						else
							.GetLogger().ERROR(Str.Format("Error getting file archive path: %1. Will not be able to move files.", status.errMsg))
							options.archiveImportedFiles = false
						end		
					end		
					
					// *** Physical Objects Settings
					options.poSyncFromToDates = false
					options.poSyncTransferDates	= false
					if options.doPo
						// *** PO SyncDate Settings
						Integer		settingsMask = CAPI.IniGet(prgCtx.fDBConnect.fLogin, 'PhysicalObjects', 'settingsMask', 0)
						options.poSyncFromToDates = settingsMask & $PO_SETTING_SYNCCONTAINERDATES
						options.poSyncTransferDates = settingsMask & $PO_SETTING_SYNCTRANSFDATES
					end			
					
					// *** what are the hidden cols
					options.hiddenCols = Str.Elements(options.hiddenCols, ",")
					
					// *** hardcode the multi value delimiter for now
					options.multiValDelim = "|"
					
					// *** get the rootFilePath
					options.rootFilePath = ._GetRootFilePath(options)
					
					// *** get the audit log location
					options.auditFolderPath = .GetAuditFolderPath(options)
			
			
					// Archive csv files options - Update 5 for OMERS
					options.archiveCsvFiles = IsFeature(options, "archiveCsvFiles") && options.archiveCsvFiles
					if options.archiveCsvFiles 
						Assoc status = ._GetCsvArchivePath(options)
						
						if status.ok
							options.csvArchivePath = status.result
							options.csvFiles = {}
						else
							.GetLogger().ERROR(Str.Format("Error getting csv archive path: %1. Will not be able to move metadata csv files.", status.errMsg))
							options.archiveCsvFiles = false
						end		
					end	
					
					// *** put the options in 
					payload.options = options
					
				
					return payload
				
				end

	/* must implement this signature */
	public function Assoc PerformAction(Object prgCtx, Frame apiDataObj, Assoc options, Assoc actionFlags, Object llNode, DAPINODE parentNode, DAPINODE node)									
			
					Assoc rtn
					rtn.ok = true
					return rtn
			
				end

	
	public function Assoc PreProcessData(Object prgCtx, Assoc options, Object apiDataObj, Object currentDataObj, Assoc actionFlags)
						
					// Abstract. Override in subclasses
					Assoc rtn
					rtn.ok = true
					rtn.result = undefined
					return rtn		
				
				end

	
	override function Assoc ProcessItem(Object prgCtx, String action, Frame apiDataObj, Assoc actionFlags, Assoc options)
																					
					Assoc 	rtn = .InitErrObj("NodeProcesses.ProcessItem")
					Assoc 	status
					Assoc 	result
					String 	actionPerformed
					
					// *** initialize the result with an undefined nodeid
					result.nodeid = undefined
					rtn.result = result
					
					// *** retrieve vals that should be in data (ValidateAPI put them there)
					Integer		subType = apiDataObj.getValue("subType")
					Integer		parentId = apiDataObj.getValue("parentId")
					Integer		volumeId = apiDataObj.getValue("volumeId")
					Integer		nodeId = apiDataObj.getValue("nodeId")
					
					// KM 10/26/18 default volumeID here to avoid trace file
					if IsUndefined(volumeId)
						volumeId = DAPI.BY_DATAID
					end
					// *** get llNode from subtype
					Object	llNode = $LLIAPI.llNodeSubsystem.GetItem(subType)
					
					// *** get parentNode (may be undefined)
					DAPINODE 	parentNode = IsDefined(parentId) ? DAPI.GetNodeById(prgCtx.DapiSess(), volumeId, parentId) : undefined
					if IsError(parentNode)
						.SetErrObj(rtn, Str.Format("Could not get parentNode from parentId %1 and volumeId %2", parentId, volumeId))
						rtn.errMsg = "Error Code 010 " + rtn.errMsg
						return rtn
					end	
				
					// *** get node (may be undefined at this point)
					DAPINODE	node 
					if IsDefined(nodeId)
						node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, nodeId)
						if IsError(node)
							.SetErrObj(rtn, Str.Format("Could not get node from nodeId %1", nodeId))
							return rtn
						end
					end	
					
					// *** start a transaction
					prgCtx.fDbConnect.StartTrans()
					
						// *** finally, the actual action
						status = .PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
						if !status.ok
							// *** rollback
							prgCtx.fDbConnect.EndTrans(false)
							.SetErrObj(rtn, status)
							return rtn
						else
							node = status.result.node
							actionPerformed += status.result.actionPerformed
						end	
						
						// *** post-action updates?
						if actionFlags.doUpdate && .fAllowPostActionUpdate
						
							// *** get the node again in case the data we have for it is stale (some dynamic naming may have occured -- e.g. in contract management)
							node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, node.pId) 
							
							if IsError(node)
								.SetErrObj(rtn, "Can not get newly created node for update")
								rtn.errMsg = "Error Code 014 " + rtn.errMsg
								return rtn				
							end
							status = $DataManager.NodeUpdate.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
							if !status.ok
								// *** rollback
								prgCtx.fDbConnect.EndTrans(false)	
								.SetErrObj(rtn, status)
								rtn.errMsg = "Error Code 014 " + rtn.errMsg		
								return rtn
							else
								actionPerformed += status.result.actionPerformed
							end	
						end	
						
					// *** commit transaction
					prgCtx.fDbConnect.EndTrans(true)
				
					// *** tidy up the action Performed
					actionPerformed = Str.Trim(Str.ReplaceAll(actionPerformed, ".", ". "))
						
					result.node = node
					result.targetItem = 2
					result.actionPerformed = actionPerformed
					result.nodeid = node.pId
				
					return .SuccessResult(result)						
				end

	
	public function void RemoveFile(String filePath, String removalPath)
								
					Object logger = .GetLogger()
				
					// *** Changed for Mannai 5/2/16 to allow full removal of file
					
					/*
						// *** set the dest
						String dest = removalPath + File.GetName(filePath)
				
						Assoc status =  $Kernel.FileUtils.Move( filePath, dest )
					
						if status.ok
							logger.DEBUG(Str.Format(Str.Tab() + Str.Tab() + "File Removal SUCCESS: Moved file from '%1' to '%2'", filePath, dest))
						else
							logger.ERROR(Str.Format(Str.Tab() + Str.Tab() + "File Removal ERROR: Could node move file from '%1' to '%2'. Error: %3", filePath, dest, status.errMsg))
						end			
					*/
					
					
					if File.Delete( filePath )							
						logger.DEBUG(Str.Format(Str.Tab() + Str.Tab() + "File Delete SUCCESS: Deleted file '%1'", filePath))
					else
						logger.ERROR(Str.Format(Str.Tab() + Str.Tab() + "File Delete ERROR: Could not delete file '%1'", filePath))
					end								
				end

	
	public function Assoc SubclassValidateItem(Object prgCtx, Assoc actionFlags, Assoc options, Object llNode, DAPINODE node, Object apiDataObj)				
					
					Assoc rtn
					rtn.ok = true
					return rtn
				
				end

	
	override function Assoc ValidateItem(Object prgCtx, String action, Frame apiDataObj, Assoc dataFlags, Assoc options)
						
					Assoc 		status
					Frame		currentDataObj = undefined
					Assoc 		rtn
					Integer 	subtype
					DAPINODE 	node		
					Object		llNode
			
					// *** metadata required?
					if options.requireMetadata && !apiDataObj.GetValue("hasMetadata")
						return .SetErrObj(.InitErrObj("ValidateItem"), "You have the 'Metadata Required' checkbox checked and no metadata was found for this item.")
					end		
			
					// Get the node we're operating on		
					status = ._GetNode(prgCtx, options, apiDataObj, action)
					if !status.ok
						return .SetErrObj(.InitErrObj("ValidateItem"), status)
					else
						node = status.result
						subType = node.pSubType
						llNode = $LLIAPI.LLNodeSubsystem.GetItem(subtype)
					end		
			
					// set some values in the apiDataObj		
					apiDataObj.setValue("subType", subType)
					apiDataObj.setValue("nodeId", node.pId)
					
					// *** action flags
					Assoc actionFlags = ._GetActionFlags(prgCtx, action, options, subtype, dataFlags)			
			
					// *** let subclasses check for action-specific things
					status = .SubclassValidateItem(prgCtx, actionFlags, options, llNode, node, apiDataObj)
					if !status.ok
						return .SetErrObj(.InitErrObj("ValidateItem"), status)
					end			
			
					// Get Current Data (if doing physical object or records management data)
					status = .GetCurrentData(prgCtx, node, apiDataObj, dataFlags, llNode)
					if !status.ok
						return .SetErrObj(.InitErrObj("ValidateItem"), status)
					else
						currentDataObj = status.result	
					end	
			
					// Call any pre-processors
					status = .PreProcessData(prgCtx, options, apiDataObj, currentDataObj, actionFlags)
					if !status.ok
						return .SetErrObj(.InitErrObj("ValidateItem"), status)
					end		
			
					rtn.ok = true
					rtn.result = Assoc.CreateAssoc()
					rtn.result.oldVals = IsDefined(currentDataObj) ? currentDataObj.GetAssocVals() : undefined
					rtn.result.oldCsvData = IsDefined(currentDataObj) ? currentDataObj.GetCsvData() : undefined	
					rtn.result.nodeId = IsDefined(node) ? node.pId : undefined
					rtn.result.actionFlags = actionFlags
					rtn.result.filePath = undefined
					
					return rtn
			
				end

	
	private function _GetActionFlags(Object prgCtx, String action, Assoc options, Integer subtype, Assoc dataFlags)
												 				 
					// *** look at the job options and the kind of metadata we have and the subtype to determine what actions we will perform
				
					Assoc a
					
					// *** metadata flags
					a.doRecManData = dataFlags.hasRecManData && options.mdRm && $DataManager.NodeUtils.IsRecManagedSubtype(prgCtx, subType)
					
					if action == "CREATE"
						a.doPhysObjData = subtype in {411,412,424} > 0 && IsDefined($PhysicalObjects)
					else
						a.doPhysObjData = dataFlags.hasPhysObjData && options.mdPo && IsDefined($PhysicalObjects) && (subtype in {411,412,424} > 0)
					end	
					
					a.doPhysObjCirc = dataFlags.hasPhysObjCirc && options.actionPoCirc && IsDefined($PhysicalObjects) && (subtype in {411,412,424} > 0)
				
					a.doPhysObjBox = dataFlags.hasPhysObjBox && options.actionPoBox && IsDefined($PhysicalObjects) && (subtype in {411,412} > 0)
				
					a.doPhysObjLocator = dataFlags.hasPhysObjLocator && options.actionPoLocator && IsDefined($PhysicalObjects) && (subtype in {411,412,424} > 0)
				
					a.doPhysObjXfer = dataFlags.hasPhysObjXfer && options.actionPoXFer && IsDefined($PhysicalObjects) && (subtype in {411,412,424} > 0)
					
					a.doPhysObjLabel = dataFlags.hasPhysObjLabel && options.actionPoLabel && IsDefined($PhysicalObjects) && (subtype in {411,412,424} > 0)
					
					a.doEmailData = dataFlags.hasEmailData && subType == 749 && IsDefined($OTEmail)
					
					a.doClassification = dataFlags.hasClassification && options.mdClass
					
					a.doAttrData = options.mdCats && dataFlags.hasAttrData
					
					a.doDapiNodeData = dataFlags.hasDapiNodeData && options.mdDapi
					
					a.doOwnerData = dataFlags.hasOwnerData && options.mdOwner
					
					a.doNickName = dataFlags.hasNickName && options.mdDapi
					
					a.doSystemAttrs = options.mdSysAttrs && dataFlags.hasSystemAttrs
					
					a.doContractFileData = dataFlags.hasContractFileData && subtype==31067	
				
					// *** general actions
					a.doRename = options.actionRename && dataFlags.hasNewName
					
					// *** special handling for metadata update flag
					if action=="CREATE"
						if a.doContractFileData
							a.doMetadataUpdate = true
							// *** avoid updating cats, class, and other stuff because we already did it
							a.doAttrData = false
							a.doClassification = false
							a.doDapiNodeData = false
							a.doOwnerData = false
							a.doNickName = false
							a.doSystemAttrs = false
				
							a.doRecManData = dataFlags.hasRecManData && $DataManager.NodeUtils.IsRecManagedSubtype(prgCtx, subType)
						
							// *** avoid renaming -- very important!
							a.doRename = false
						else
							// *** KM 9/15/16 Added this clause to handle updating Nickname from CSV	
				
							// *** avoid renaming -- very important!
							a.doRename = false
							
							// *** we can update metadata if we have a nickname in the csv
							a.doMetadataUpdate = a.doNickName
				
						end	
					else
						a.doMetadataUpdate = options.actionMetadata && dataFlags.hasMetadata
				
					end
						
					a.doPermUpdate = options.actionPerms && dataFlags.hasPermData
					
					// *** we do update if it's a permission update, metadata update, rename, or creation of a contract file (in which case we have to update these things after creation)
					a.doUpdate = a.doRename || a.doMetadataUpdate || a.doPermUpdate
					
					// *** po metadata
					a.poChargeIn = false
					a.poChargeOut = false
					a.poRemoveFromBox = undefined
					a.poAssignToBox = undefined
					a.poSyncFromToDates = options.poSyncFromToDates
					a.poSyncTransferDates = options.poSyncTransferDates
					a.doLabelLocator = false
					
					return a
					
				end

	script _GetFileInfo
	
			
					
											
								// Patched by Pat201609270
								function void _GetFileInfo(	Assoc options, \
															Assoc createInfo, \
															String filePath, \
															String verfileName = undefined)
								
									String	fileName = File.GetName(filePath)
									String	fileExt = _GetFileNameExt(options, fileName)
									
									// *** set the source for the add version call
									createInfo.VersionFile = filePath
									
									// *** set filename, mimetype,filetype	
									createInfo.filename = IsDefined(verfileName) ? verfileName : fileName
									createInfo.filetype = fileExt
									createInfo.mimetype = IsDefined(fileExt) && Length(fileExt) > 1  ? $WebDsp.MimeTypePkg.GetFileExtMimeType(fileExt) : options.defaultFileExtension
									
								end	
								
								function String _GetFileNameExt(Assoc options, String fName )
								
									// return the extension of a filename, if any
									String	ext = options.defaultFileExtension
									
									Integer	i = Str.RChr(fname, '.' )
									if i > 0 AND i < Length(fname)
										ext = fname[i+1:]
									end	
									
									return ext
								
								end
						
						
					
				
			
		
	
	endscript

	
	private function Assoc _GetFileRemovalPath(Assoc options)
														
					String	path
					Object 	fileUtils = $Datamanager.FileUtils
					String 	rootPath = IsDefined(options.fileRemovalPath) && Length(options.fileRemovalPath) ? fileUtils.EnsureDirSeparator(options.fileRemovalPath) : fileUtils.GetFileRemovalPath()
					
					Assoc status = fileUtils.EnsurePathExists(rootPath)
					
					if status.ok 
					
						status = fileUtils.CreateTimeSpecificDir(rootPath)
					
						if status.ok
							path = status.result
						else
							return .SetErrObj(.InitErrObj("_GetFileRemovalPath"), status)
						end	
					
					else
						
						return .SetErrObj(.InitErrObj("_GetFileRemovalPath"), status)
						
					end
					
					Assoc rtn
					rtn.ok = true
					rtn.result = path
				
					return rtn
				
				end

	
	private function Assoc _GetNode(Object prgCtx, Assoc options, Frame apiDataObj, String action)
					
					Assoc		status
					DAPINODE	node
					
					Integer		nodeId = apiDataObj.GetValueFromCol("$OBJECTID")
					String		targetPath = apiDataObj.GetValueFromCol("$TARGETPATH")
					String		objectName = apiDataObj.GetValueFromCol("$OBJECTNAME")
					
					// *** we might have the nodeId in the apiDataObj
					// *** Create operations should ignore objectID -- makes it easier to reuse CSV files that were exported
					if IsDefined(nodeId)
					
						node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, nodeId)
						if IsError(node)
							return .SetErrObj(.InitErrObj("_GetNode"), Str.Format("Could not get node from id %1", nodeId))
						end	
					
					elseif IsDefined(targetPath)
					
						// *** check that node path exists
						if options.targetPathType == "parent"
						
							// *** KM 10-18-16 Add logic here for ADN
							if IsUndefined(objectName) 
								if IsDefined($ADN)
									status = ._NewUniqueID(prgCtx)
									if !status.ok
										return .SetErrObj(.InitErrObj("_GetName"), status)
									end
									objectName = Str.String(status.result)
								else
									return .SetErrObj(.InitErrObj("_GetNode"), "You have indicated that $TargetPath refers to the parent's location, but you are missing a required $ObjectName")	
								end
							end
			
							// ok, now concatenate target and object names to get full path						
							targetPath = $DataManager.NodeUtils.EnsurePathSeparator(targetPath) + objectName
					
							// ok, now try to get the node
							status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, targetPath)
							if !status.ok
								return .SetErrObj(.InitErrObj("_GetNode"), status)
							else
								node = status.result					
							end							
						
						// *** parse the node path out of the full path and check that it exists
						elseif options.targetPathType == "object"
			
							status = $DataManager.NodeUtils.GetNodeFromPath(prgCtx, targetPath)
							if !status.ok
								return .SetErrObj(.InitErrObj("_GetNode"), status)
							else
								node = status.result
							end
						
						end
					else
						return .SetErrObj(.InitErrObj("_GetNode"), "Could not determine the object.  You need either an $ObjectId or $TargetPath column.")
					end	
					
					Assoc rtn
					rtn.result = node
					rtn.ok = true
			
					return rtn
			
				end

	
	private function String _GetRootFilePath(Assoc options)
															
					String rootFilePath
				
					// *** figure out what the root path is
					if options.rootFileLoc == "csvpath"
						rootFilePath = options.rootSourcePath
						rootFilePath = rootFilePath[1:Str.RChr(rootFilePath, File.Separator())]
						options.loadDirPath = rootFilePath
					
					elseif options.rootFileLoc == "absolute"
						rootFilePath = ""
						
						if IsDefined(options.rootSourcePath) && Length(options.rootSourcePath)
							String path = options.rootSourcePath
							path = path[1:Str.RChr(path, File.Separator())]
							options.loadDirPath = path
						end
						
					elseif IsDefined(options.rootSourcePath) && Length(options.rootSourcePath)
					
						rootFilePath = $DataManager.FileUtils.EnsureDirSeparator(options.rootSourcePath)
						options.loadDirPath = rootFilePath
					end
				
					return rootFilePath
				
				end

	
	private function Assoc _GetfileArchivePath(Assoc options)
													
				String	path
				Object 	fileUtils = $Datamanager.FileUtils
				String 	rootPath = IsDefined(options.fileArchivePath) && Length(options.fileArchivePath) ? fileUtils.EnsureDirSeparator(options.fileArchivePath) : fileUtils.GetfileArchivePath()
				
				Assoc status = fileUtils.EnsurePathExists(rootPath)
				
				if status.ok 
				
					status = fileUtils.CreateTimeSpecificDir(rootPath)
				
					if status.ok
						path = status.result
					else
						return .SetErrObj(.InitErrObj("_GetfileArchivePath"), status)
					end	
				
				else
					
					return .SetErrObj(.InitErrObj("_GetfileArchivePath"), status)
					
				end
				
				Assoc rtn
				rtn.ok = true
				rtn.result = path
			
				return rtn
			
			end

	
	private function void _SetupNameCollisionOptions(Assoc options)
														
					// *** build an assoc
					Assoc a
					a.docs = Assoc.CreateAssoc()
					a.containers = Assoc.CreateAssoc() 
				
					// *** DOCUMENTS	
				    if IsFeature(options, "docOption")
				    	switch Str.Upper(options.docOption)
				    		case "UPDATE"
								a.docs.reportErrors = false
					    		a.docs.makeUnique = false      		
				    		
				    			a.docs.AddVersion = IsFeature(options, "docOptionAddVersion") && options.docOptionAddVersion
				    			a.docs.AddVersionIfNewer = IsFeature(options, "docOptionAddVersionIfNewer") && options.docOptionAddVersionIfNewer
				    			a.docs.updateMetadata = IsFeature(options, "docOptionUpdateMd") && options.docOptionUpdateMd
								a.docs.rename = IsFeature(options, "docOptionRename") && options.docOptionRename
				    		end
				    		
				    		case "MAKEUNIQUE"
								a.docs.reportErrors = false
					    		a.docs.makeUnique = true   
					    		
				    			a.docs.AddVersion = false
				    			a.docs.AddVersionIfNewer = false
				    			a.docs.updateMetadata = false
								a.docs.rename = false
				    		end
				    	
				    		case "REPORTERRORS"
					    		a.docs.reportErrors = true
					    		a.docs.makeUnique = false
				
				    			a.docs.AddVersion = false
				    			a.docs.AddVersionIfNewer = false
				    			a.docs.updateMetadata = false
								a.docs.rename = false
				    		end
				    	end
				    else
				    	a.docs.reportErrors = true
				    end	
				
					// *** CONTAINERS
				    if IsFeature(options, "containerOption")
				    	switch Str.Upper(options.containerOption)
				    		case "UPDATE"
				    			a.containers.updateMetadata = IsFeature(options, "containerOptionUpdateMd") && options.containerOptionUpdateMd
								a.containers.rename = IsFeature(options, "containerOptionRename") && options.containerOptionRename
								
								a.containers.reportErrors = false
					    		a.containers.makeUnique = false
				    		end
				    		
				    		case "MAKEUNIQUE"
								a.containers.reportErrors = false
					    		a.containers.makeUnique = true   
				
				    			a.containers.updateMetadata = false
								a.containers.rename = false
				    		end
				    	
				    		case "REPORTERRORS"
					    		a.containers.reportErrors = true
					    		a.containers.makeUnique = false
				
				    			a.containers.updateMetadata = false
								a.containers.rename = false
				    		end
				    	end
				    else
				    	a.containers.reportErrors = true
				    end
				                   
					options.nameCollisions = a                    
				                    
				end

end

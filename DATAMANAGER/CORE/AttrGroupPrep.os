package DATAMANAGER::CORE

public object AttrGroupPrep inherits DATAMANAGER::LoadMetadataObjects

	public		Dynamic	_fRimsDbToCreateInfoMap


	
	public function Assoc GetRimsDbToCreateInfoMap()
							return ._fRimsDbToCreateInfoMap
						end

	
	public function Assoc PrepareCatAttrsForCreate(Object prgCtx, DAPINODE parentNode, List catIDs, Assoc attrVals, Boolean inheritParent, Assoc actionFlags, Boolean didUpgrade=FALSE)
							
				Assoc 	rtn = .InitErrObj("PrepareCatAttrsForCreate")
				Assoc 	status	
				List	attrGroups
				Assoc 	inheritedCatDefs
			
				if inheritParent
					status = $DocManService.AttributeHandlerRegistry.GetDefaultAttributes( prgCtx, parentNode.pSubType, parentNode )
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					else
						attrGroups = status.attributeGroups
						if Length(attrGroups) > 0
							actionFlags.doAttrData = true
		
							// add to our list of catDefs from categories inherited from parent in case category vals for any of these categories are not in the csv							
							Frame attrGroup
							for attrGroup in attrGroups
								if Str.Upper(attrGroup.fType) == "CATEGORY"
									List keys = Str.Elements(attrGroup.fKey, ".")
									if IsDefined(keys) && Length(keys) > 0
										Integer catID = Str.StringToInteger(keys[1])
										
										if !(catID in catIDs)
											// set this as an inherited catDef so we can pass back to caller
											// KM 12-19-18 changed this to call our GetCategoryDefinition Function to avoid trace file
											status = $DataManager.CategoryAPI.GetCategoryDefinition(prgCtx, catID)
										
											if status.ok
												inheritedCatDefs.(attrGroup.fKey) = status.result
											end	
										end
										
									end
									
								end
							end 
							
						end	
					end
				end	
				
				// *** now add in whatever wasn't there	
				status = ._AddAttrsFromCsv(prgCtx, parentNode, attrGroups, catIDs, attrVals, didUpgrade)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				elseif status.categoryUpgraded
		
					if didUpgrade
						.SetErrObj(rtn, Str.Format("Could not automatically upgrade category on parent %1 [%2].", parentNode.pName, parentNode.pID))	
					else
						// call the same function again, but this time the category will be upgraded so we can proceed
						return PrepareCatAttrsForCreate(prgCtx, parentNode, catIDs, attrVals, inheritParent, actionFlags, true)
					end
				
				else
					attrGroups = status.attributeGroups
				end	
				
				rtn.categoriesUpgraded = didUpgrade
				rtn.result = Assoc{'attrGroups': attrGroups, 'inheritedCatDefs': inheritedCatDefs}
				return rtn
			end

	
	public function Assoc PrepareCatAttrsForUpdate(Object prgCtx, DAPINODE node, List catIDs, List catIdsToRemove, Assoc attrVals, Boolean didUpgrade = FALSE)
								
				Assoc 	rtn = .InitErrObj("PrepareCatAttrsForUpdate")
				Assoc 	status	
				List	attrGroups
			
				// *** get current attributes for this node
				status = $DocManService.AttributeHandlerRegistry.GetAttributes( prgCtx, node)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				else
					attrGroups = status.attributeGroups
				end	
				
				// *** do we have categories to remove?
				if Length(catIdsToRemove) > 0
					Integer catID
					Object attrGroupToRemove
					List newAttrGroups = attrGroups
					for catID in catIdsToRemove
						status = $DocManService.AttributeHandlerRegistry.GetAttributeGroupTemplate( prgCtx, "Category", Str.Format( "%1.0", catID ))
						if !status.ok
							.SetErrObj(rtn, status)
							return rtn
						else
							attrGroupToRemove = status.attributeGroup	
						end					
						Object 	attrGroup
						for attrGroup in attrGroups
							if attrGroup.fKey == attrGroupToRemove.fKey
								newAttrGroups = List.SetRemove(newAttrGroups, attrGroup)
								break
							end
						end
					end
					attrGroups = newAttrGroups
				end	
				
				// *** now add in whatever wasn't there	
				status = ._AddAttrsFromCsv(prgCtx, node, attrGroups, catIDs, attrVals, didUpgrade)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				elseif status.categoryUpgraded
					// call the same function again, but this time the category will be upgraded so we can proceed
		
					if didUpgrade
						.SetErrObj(rtn, Str.Format("Could not automatically upgrade category on node %1 [%2].", node.pName, node.pID))
					else
						return PrepareCatAttrsForUpdate(prgCtx, node, catIDs, catIdsToRemove, attrVals, true)
					end				
				else
					attrGroups = status.attributeGroups
				end		
				
				rtn.categoriesUpgraded = didUpgrade
				rtn.result = attrGroups
				return rtn
			end

	
	public function Assoc PrepareRecManAttrsForCreate(Object prgCtx, DAPINODE parentNode, Boolean inheritParent, Integer subtype, Assoc metadataVals, List attrGroups, Assoc actionFlags)
						
							Assoc 	rtn = .InitErrObj("PrepareRecManAttrs")
							Assoc	status
							String	attrKey
							Assoc 	rmCreateInfo = metadataVals.rmCreateInfo
							
							// *** ok we have said we want to inherit. If we're not setting any classId from the CSV, then see if the parent has an RM classification that we ought to inherit
							// *** But first make sure that we are creating an RM managed object and that the parent is in the inheritance list for RecMan settings	(?func=RecMan.ManagedObjects page)	
							if inheritParent && IsUndefined(metadataVals.rmClassId)
								
								// *** check if either CREATE action or set to inherit from MOVE/COPY
								if parentNode.pSubtype in $RecMan.RMSettings.GetCachedInheritList( prgCtx )
								
									status = $DataManager.NodeUtils.GetRecManData(prgCtx, parentNode, undefined)
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn
									elseif IsDefined(status.result)
										Assoc map = ._fRimsDbToCreateInfoMap
										Assoc dbVals = status.result
										String key
										
										// *** set the rmClassId
										metadataVals.rmClassId = dbVals.CID
										
										// *** set the flag that we have the data
										actionFlags.doRecManData = true
												
										for key in Assoc.Keys(dbVals)
											rmCreateInfo.(map.(key)) = {dbVals.(key)}
										end
									end
								end	
							end			
							
							// ** get attr group
							status = ._GetRecManAttrGroup(prgCtx, subType, {metadataVals.rmClassId})
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							else
								status = ._SetAdd(attrGroups, status.result)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else
									attrGroups = status.result
								end
							end
							
							// *** put the rm data into attrGroups for WebService
							for attrKey in Assoc.Keys(rmCreateInfo)
								List attrKeys = ._ExpandKeys(attrKey)
								status = ._SetAttrValue(attrGroups, attrKeys, rmCreateInfo.(attrKey), false, false)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end
							end
							
							// *** we need to get the classId in there
							// *** TODO what about multiple classifications?
							rmCreateInfo.('CLASS_ID_List') = {metadataVals.rmClassId}
							rmCreateInfo.('primaryRMClass') = {metadataVals.rmClassId}
						
							rtn.result = attrGroups
							return rtn
						
						end

	
	private function Assoc _AddAttrsFromCsv(Object prgCtx, DAPINODE node, List attrGroups, List catIDs, Assoc attrVals, Boolean alreadyUpgraded)
					
							Assoc 	rtn = .InitErrObj("_AddAttrsFromCsv")
							rtn.categoryUpgraded = false
							
							Integer catID
							Assoc 	status, upgradeStatus
							String 	attrKey
									
							// Loop through the categories (ids specifically) that are represented in the CSV
							for catID in catIds
								status = $DocManService.AttributeHandlerRegistry.GetAttributeGroupTemplate( prgCtx, "Category", Str.Format( "%1.0", catID ))
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else
									status = ._SetAdd(attrGroups, status.attributeGroup)
									if !status.ok
										
										// do we need to upgrade? Check to make sure we didn't just try
										// to upgrade
										if status.upgradeCatID > 0 && !alreadyUpgraded
											upgradeStatus = ._UpgradeCategory(prgCtx, node, status.upgradeCatID)
											if !upgradeStatus.ok
												.SetErrObj(rtn, upgradeStatus)
												return rtn
											else
												// return a result that shows category was upgraded
												rtn.categoryUpgraded = true
												return rtn
											end			
										
										else
											.SetErrObj(rtn, status)
											return rtn
										end	
									else
										attrGroups = status.result
									end		
								end
							end
							
							// *** put the attribute data into attrGroups for WebService
							for attrKey in Assoc.Keys(attrVals)
								List attrKeys = ._ExpandKeys(attrKey)
								Boolean clearSetRows = false
								
								// *** allow for clearing of attribute set rows
								if Length(attrKeys) == 3 && attrKeys[1] == 	"0_attrSet"
									attrKeys = {attrKeys[2], attrKeys[3]}
									
									if attrVals.(attrKey) == {}
										clearSetRows = true
									end
									
								end		
								
								// *** set the value
								status = ._SetAttrValue(attrGroups, attrKeys, attrVals.(attrKey), true, clearSetRows)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end
							end	
					
							rtn.attributeGroups = attrGroups
							return rtn
						end

	
	private function List _CheckForADNID(Object prgCtx, List attrGroups, DAPINODE parentNode, DAPINODE node)
									
							// *** check to see if we need ADN ID generated
							Object		llNode = $LLIAPI.LLNodeSubsystem.GetItem( parentNode.pSubtype )
							
							Assoc status = llNode.NodeCategoriesGet( parentNode )
							if status.ok
								List rootKey
								for rootKey in Assoc.Keys(status.AttrData.fDefinitions)
									Assoc rootDef = status.AttrData.fDefinitions.(rootKey)
									Assoc attr
									for attr in rootDef.Children
										if attr.Type == 10131 && attr.Required 	// *** do we have a required ADN ID attribute?
										
											._GatherAttrValuesForADN(rootKey, attrGroups, rootDef)
										
											Assoc result = $ADN.Utils.GenerateIDNumber(prgCtx, rootDef, attr, '_1_1_2_1', 1)
											if status.ok
												Assoc value = Assoc.CreateAssoc()
												value.Number = result.Number
												value.ADNID = result.ADNID
												value.Prefix = Str.ValueToString( result.PrefixList )
												value.Suffix = Str.ValueToString( result.SuffixList )
												value.Quantity = 1
												value.UserID = prgCtx.USession().fUserID
												value.UserName = prgCtx.fDbConnect.fUserName
												value.SaveToDB = True
												value.SeqID = result.SeqID
												value.Sheet = result.Sheet
												
												Assoc	refTypeConst = $Adn.Constants.REFERENCE_KEY_TYPE
												Integer	nodeID = IsDefined(node) ? node.pID : 0	
												value.RefKeys = { refTypeConst.Node, nodeID, 1, Undefined }
												value.AddSheet  = false
												
												String valueStr = Str.ValueToString(value)
						
												Frame attrGroup
												for attrGroup in attrGroups
													String groupKey = attrGroup.fkey
													
													Frame f
													for f in attrGroup.fValues
														if f.fKey == groupKey + "." + Str.String(attr.ID)
															if IsUndefined(f.fValues[1])
																Object llAttr = $LLIApi.LLAttributeSubsystem.GetItem(attr.Type)
																Assoc dataAssoc
																dataAssoc.ID = attr.ID
																dataAssoc.Values = {undefined}
																llattr.ValueSet(prgCtx, attr, dataAssoc, valueStr, 1)
																
																f.fValues[1] = result.Number
																
															end
														end
													end
													
												end								
									
											end	
										end
									end
								end
							
							end
							
							return attrGroups
						end

	// Patched by Pat202003240
	private function Frame _CopyFrame(Frame src)
	                                                                                                
	    Frame newFrame = Frame.New()
	    String slotName
	    
	    for slotName in Frame.SlotNames(src)
	                    if slotName == "fValues"
	                                    
	                                    if Type(src.(slotName)) == ListType && Length(src.(slotName)) && Type(src.(slotName)[1]) == Frame.FrameType
	                                                    Frame.AddSlot(newFrame, slotName, {})
	                                    
	                                                    Frame eachFrame
	                                                    for eachFrame in src.(slotName)
	                                                                    newFrame.(slotName) = {@newFrame.(slotName), _CopyFrame(eachFrame)}
	                                                    end
	                                    else
	                                                    Frame.AddSlot(newFrame, slotName, {})
	                                    end        
	                    elseif Type(src.(slotName)) == Frame.FrameType
	                                    Frame.AddSlot(newFrame, slotName, _CopyFrame(src.(slotName)))
	                    else
	                                    Frame.AddSlot(newFrame, slotName, src.(slotName))     
	                    end
	                                    
	    end
	    
	    // KM 3-23-20 need this flag for CS 16.2.10 for new documentservice code
	    Frame.AddSlot(newFrame, 'isAbstract', false)
	
	    return newFrame
	end


	
	private function List _ExpandKeys(Dynamic elements)
									
							// *** allow either string separated by periods or regular list
							if Type(elements) == StringType
								elements = Str.Elements(elements, ".")
							end
										
							List keys
						
							switch Length(elements)
								case 3
									String  key1 = Str.Format("%1.%2", elements[1], elements[2])
									String 	key2 = Str.Format("%1.%2.%3", elements[1], elements[2], elements[3])
									keys = {key1, key2}		
								end
								
								case 4		
									if elements[1] == "0_attrSet"
										// **** attribute set --maybe with with "<clear>" directive to remove all rows
										String  key1 = Str.Format("%1", elements[1])
										String  key2 = Str.Format("%1.%2", elements[2], elements[3])
										String 	key3 = Str.Format("%1.%2.%3", elements[2], elements[3], elements[4])		
										keys =  {key1, key2, key3}		
									else
									
										// *** TODO:  I think this section would never get used
										String  key1 = Str.Format("%1.%2", elements[1], elements[2])
										String 	key2 = Str.Format("%1.%2.%3", elements[1], elements[2], elements[3])		
										String  key3 = Str.Format("%1.%2.%3.%4", elements[1], elements[2], elements[3], elements[4])
										keys =  {key1, key2, key3}		
									end					
								
								end
								case 5
									String  key1 = Str.Format("%1.%2", elements[1], elements[2])
									String 	key2 = Str.Format("%1.%2.%3", elements[1], elements[2], elements[3])
									keys = {key1, key2, Str.StringToInteger(elements[4]), Str.StringToInteger(elements[5])}	
								end
							end
						
							return keys
						end

	script _GatherAttrValuesForADN
	
			
					
							
									
											
												// Patched by Pat201611030
												function void _GatherAttrValuesForADN(List catIDList, List attrGroups, Assoc rootDef)
												
													Frame attrGroup, attr
													Dynamic id
													Integer idx
													Dynamic	val
												
													// *** get the category id as a string
													String rootKey = Str.Catenate(catIDList, ".")[1:-2]
												
													for attrGroup in attrGroups
														
														if attrGroup.fType == "Category" && attrGroup.fKey == rootKey								
															for attr in attrGroup.fValues
																if Length(attr.fValues) && IsDefined(attr.fValues[1])
																	
																	// *** this is the attribute value
																	val = attr.fValues[1]
																	
																	// *** get the integer ID number so we can put into rootDef
																	idx = Str.RChr(attr.fKey, ".")
																	id = attr.fKey[idx+1:]
																	id = Str.StringToInteger(id)
																	
																	if IsDefined(id)
																		_PutValInRootDef(id, val, rootDef)
																	end
												
																	
																end	
															end 
														end
													end 
													
												end
												
												function void _PutValInRootDef(	Integer id, \
																				Dynamic val, \
																				Assoc rootDef)
														
													Assoc a
																		
													if Length(rootDef.ValueTemplate.Values)
														Assoc vals = rootDef.ValueTemplate.Values[1]
														for a in vals
															if a.ID == id
																a.Values = {val}
															end
														end		
													end						
												
												
												end	
										
									
								
							
						
					
				
			
		
	
	endscript

	script _GetRecManAttrGroup
	
			
					
							
									
											
												function Assoc _GetRecManAttrGroup( Object prgCtx, Integer subType, List rmClassIds)
												
													Assoc 	rtn = .InitErrObj("_GetRecManAttrGroup")
													
													// *** get attrHandler	
													
													// *** get the definition (use our own script - hate to do this but the regular one filters out anything that's not included in the add item page 
													Assoc status = ._GetRecManAttrGroupDefinition(prgCtx, Str.String(subtype))	
													if !status.ok
														.SetErrObj(rtn, status)
														return rtn
													end
													
													// *** set the fValues
													Object 	attrGroup = status.attributeGroupDef
													List	values = _CreateValues( attrGroup.fAttributes, rmClassIds)
													Frame.AddSlot(attrGroup, "fValues", values)
												   	Frame.RemoveSlot(attrGroup, 'attributes')
												   	
												   	attrGroup.fKey = 'RMCreateInfo'
												   										
													return .SuccessResult(attrGroup)										
												end		
												
												// Create Value objects for attrDefs using given data, with given root key
												function List _CreateValues( List attrDefs, List rmClassIds)
												
													Object		attrDef
													List		values
													Object		value
													
												
													values = {}
													
													for attrDef in attrDefs
														
														value = _GetValueSDO( attrDef )
												
														value.fKey = attrDef.fDisplayName
														value.fDescription = attrDef.fDisplayName
														
														// If this is a Set, TableValue and RowValue SDOs are used
														if ( value._SDOName == "Core.TableValue" )
															/*
															// Each set value will have a rowValue. Since clients must create
															// and destroy row values when updating the set values, each row does
															// not have a unique key. Row is determined by position.
												
															Assoc	setValue
																		
															value.fValues = {}
												
															for setValue in data.( attrDef.ID ).Values
																Object rowValue = $Service.ServiceRegistry.GetSDO( "Core.RowValue" )
												
																rowValue.fValues = _CreateValues( attrDef.Children, setValue, value.fKey )
																
																value.fValues = { @value.fValues, rowValue }
															end
															*/			
														else
															value.fValues = {}
														end
														
														values = { @values, value }
													end
													
													// *** add a CLASS_ID_List attrubute to the def
													value = $Service.ServiceRegistry.GetSDO( 'Core.IntegerValue' )
													value.fKey = 'CLASS_ID_List'
													value.fDescription = 'CLASS_ID_List'	
													value.fValues = rmClassIds
													values = { @values, value }	
													
													// *** add primaryClass
													value = $Service.ServiceRegistry.GetSDO( 'Core.IntegerValue' )
													value.fKey = 'primaryRMClass'
													value.fDescription = 'primaryRMClass'	
													value.fValues = rmClassIds	
													values = { @values, value }
													
													return values
												end
												
												/**
												 * Return the Core Value SDO corresponding to the specified attribute type.
												 */
												function Object _GetValueSDO( Object attributeDef )
												
													String	name
													
												
													switch Str.Lower(( attributeDef.fType ))
												
														case 'string'
															name = "Core.StringValue"
														end
												
														case 'integer'
															name = "Core.IntegerValue"
														end
														
														case 'date'
															name = "Core.DateValue"
														end
												
														case 'boolean'
															name = "Core.BooleanValue"
														end
												
														case 'real'
															name = "Core.RealValue"
														end
												
														case Assoc.AssocType	// Set: its values are rows of values
															name = "Core.TableValue"
														end
												
														default
															name = "Core.StringValue"
														end
													end
												
													return $Service.ServiceRegistry.GetSDO( name )
												end
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function Assoc _GetRecManAttrGroupDefinition( Object prgCtx, String key )
								
							Assoc		retVal
							Dynamic		apiError
							String		errMsg
						
							Boolean		ok = TRUE
							Dynamic		item
							Object		attributeGroupDef = Undefined
							Object		attribute =  Undefined
							Object		docManService = $Service.ServiceRegistry.getItem( "DocumentManagement" )
							List		attributeList={}
							
							Assoc		data = Assoc.CreateAssoc()
							Assoc		moreInfo = Assoc.CreateAssoc()
							
							if IsDefined( key ) 
								// we just need to know if we retrieve settings for 144 or 411 to add record type
								// the key can be: '144','411','Document','Physical Item'
								if (key == [PHYSOBJ_LABEL.PhysicalItemSubtypeName] ) || key =='411'
									moreInfo.objType = 411
								elseif (key == [LLAPI_LLNODEMISC.Document] || key == '144')
									moreInfo.objType =144	
								end
						
							end
							
							// use RMGetCodes to populate drop downs
							moreInfo.selectCodes = FALSE
							
							data = $RecMan.RMSettings.GetRMDataSettings( prgCtx, moreInfo )
						
							attributeGroupDef = $Service.ServiceRegistry.GetSDO( "DocMan.AttributeGroupDefinition" )
							
							if !IsError( attributeGroupDef )
							
								attributeGroupDef.fID = 0
								attributeGroupDef.fKey = key
								attributeGroupDef.fDisplayName = "RMCreateInfo"
								attributeGroupDef.fType = "RMCreateInfo"
								
								for item in data.fieldsToShow
									
									// *** allow them all to get added	
									//if item.Add
									
										attribute = docManService._GetSDO('StringAttribute')
										
										attribute.fDisplayLength = 32
										attribute.fDisplayName = item.Name
										attribute.fID = 1
										attribute.fKey = key
										attribute.fMaxLength = 32
										attribute.fMaxValues = 32
										attribute.fMinValues = 32
										attribute.fRequired = item.AddReq
										attribute.fSearchable = TRUE
										attribute.fType = 'String'
										attribute.fValues = {}
										
										attributeList = { @attributeList, attribute }
										
									//end
								end
								attributeGroupDef.fAttributes = attributeList
							end	
							
							if ( IsUndefined( retVal.ok ) )
								retVal.ok = ok
								retVal.errMsg = errMsg
								retVal.apiError = apiError
								
								retVal.attributeGroupDef = attributeGroupDef
							end
							
							return retVal
						end

	
	private function Assoc _SetAdd(	List attrGroups, Object attrGroup)
									
							Assoc 	rtn = .InitErrObj("_SetAdd")
							rtn.upgradeCatID = 0
							Boolean found = false
							Object 	a
							
							for a in attrGroups
								List 	newKeys = Str.Elements(attrGroup.fKey, ".")
								List 	thisKeys = Str.Elements(a.fKey, ".")
							
								if a.fKey == attrGroup.fKey
									found = true
								elseif newKeys[1] == thisKeys[1] 
									rtn.upgradeCatID = Str.StringToInteger(thisKeys[1])
									.SetErrObj(rtn, Str.Format("Category '%1' must be upgraded first.", a.fDisplayName))
									return rtn	
								end
							end
						
							if !found
								attrGroups = {@attrGroups, attrGroup}	
							end
						
							rtn.result = attrGroups
						
							return rtn
						end

	script _SetAttrValue
	
			
					
						function Assoc _SetAttrValue(	List attrGroups, List attrKeys, Dynamic vals, Boolean returnError, Boolean clearSetRows)
						
							Assoc	rtn = .InitErrObj("_SetAttrValue")
							Dynamic	attrGroup
							String 	attKey	
							Integer cardinality
							Integer attId
							
							// *** did the vals come in as a list of assoces? If so, it's a set.
							if Type(vals) == ListType && Length(vals) && Type(vals[1]) == Assoc.AssocType
								// *** means we have an attribute set	
								Assoc setVals
								cardinality = 0
								for setVals in vals
									cardinality+=1
									attrGroup = _GetAttrGroupFromKeyList(attrGroups, attrKeys, cardinality)
									if IsUnDefined(attrGroup) && returnError
										.SetErrObj(rtn, Str.Format("Could not find the attribute specified by attrKeys %1", attrKeys))
										return rtn			
									end					
									
									for attId in Assoc.Keys(setVals)
										// *** example structure
										// *** attrGroup.fValues[ setId ].fValues[ setCardinality ].fValues[ 1 ].fValues
										attKey = attrKeys[2] + "." + Str.String(attId)
										vals = setVals.(attId)
										Assoc status = _SetAttrValue(attrGroup.fValues, {attKey}, vals, returnError, clearSetRows)
										if !status.ok
											.SetErrObj(rtn, status)
											return rtn
										end	
									end
								end
						
							// *** do we have attrKeys that are 4 elements long -- that means a set too
							elseif Length(attrKeys) == 4
								cardinality = attrKeys[3]
								attId = attrKeys[4]
								
								attrGroup = _GetAttrGroupFromKeyList(attrGroups, {attrKeys[1], attrKeys[2]}, cardinality)
								if IsUnDefined(attrGroup) && returnError
									.SetErrObj(rtn, Str.Format("Could not find the attribute specified by attrKeys %1", attrKeys))
									return rtn			
								end	
								
								attKey = attrKeys[2] + "." + Str.String(attId)		
								Assoc status = _SetAttrValue(attrGroup.fValues, {attKey}, vals, returnError, clearSetRows)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end			
								
							// *** regular attribute or attribute set with "<clear>" directive	
							else
								// *** vals is expected to be a list
								vals = Type(vals) != ListType ? {vals} : vals
								
								// **** loop through the keys until we get the bottom level attr
								attrGroup = _GetAttrGroupFromKeyList(attrGroups, attrKeys)
								
								if IsDefined(attrGroup)
								
									if vals=={} && clearSetRows
										_ClearSetRows(attrGroup)
									
									elseif vals[1] == "<append>" || vals[1] == "<appendunique>"
				
										Boolean doUnique = vals[1] == "<appendunique>"	
		
										// *** KM 8-5-19 support for appending attribute values	
										if Length(vals) > 1
											if Length(attrGroup.fValues) == 1 && IsUndefined(attrGroup.fValues[1])
												attrGroup.fValues = {}		
											end	
											Integer i
											for i = 2 to Length(vals)
												if doUnique
													attrGroup.fValues = List.SetAdd(attrGroup.fValues, vals[i])
												else	
													attrGroup.fValues = { @attrGroup.fValues, vals[i] }
											    end
											end		
										end	
				
									else
										// **** now set the value in the frame that we got
										attrGroup.fValues = vals
									end	
								
								elseif returnError
									.SetErrObj(rtn, Str.Format("Could not find the attribute specified by attrKeys %1", attrKeys))
									return rtn			
								end			
								
							end	
								
							return .SuccessResult(attrGroups)
						
						end
						
						function void _ClearSetRows(Frame attrGroup)
						
							List values = attrGroup.fValues
							Frame attr
						
							// *** trim to a length of 1
							if Length(values) > 1
								values = {values[1]}
							end
						
							// *** set all attrs in row 1 to undefined
							for attr in values[1].fValues
								attr.fValues = {undefined}
							end
							
							// *** save the 1 row of undefined values back into the frame
							attrGroup.fValues = values
						
						end
						
						function Frame _GetAttrGroupFromKeyList(List attrGroups, List attrKeys, Integer cardinality = undefined)
						
							Frame 	attrGroup
							Integer i
						
							for i=1 to Length(attrKeys)
								String key = attrKeys[i]
								attrGroup = _GetAttrGroupByKey(attrGroups, key)
								if IsDefined(attrGroup)
									attrGroups = attrGroup.fValues
								end	
							end
							
							// *** is this a set?
							Integer count	
							if IsDefined(cardinality)
								if Length(attrGroup.fValues) >= cardinality
									for attrGroup in attrGroup.fValues
										count+=1		
										if count==cardinality
											return attrGroup
										end
									end	
								elseif Length(attrGroup.fValues)
									// *** we need to add a new row. All sets will have at least 1 row already. Take advantage of this and copy the row.
									Frame template = ._CopyFrame(attrGroup.fValues[1])
									attrGroup.fValues =  {@attrGroup.fValues, template}
									
									// *** KM 12-18-15 set undefineds in the .fValues to avoid the empty text box
									Frame attr
									for attr in template.fValues
										attr.fValues = {undefined}
									end
									return template
								else
									.GetLogger().Error("Could not get template to add new row to attribute set")	
								end	
							end
						
							return attrGroup			
							
						end	
						
						function Frame _GetAttrGroupByKey(List attrGroups, \
														  String key)
							Frame attrGroup
						
							for attrGroup in attrGroups
								if attrGroup.fkey == key
									return attrGroup
								end
							end
							
							return undefined
						end	
										
				
			
		
	
	endscript

	
	private function Assoc _UpgradeCategory(Object prgCtx, DAPINODE node, Integer catID)
								
							Frame attrData = $LLIApi.AttrData.New( prgCtx, node.pID, node.pVersionNum )
							// pass in no arguments to DBGet to make compatible with Update 9
							Assoc status = attrData.DBGet()
							if !status.ok
								return .SetErrObj(.InitErrObj("_UpgradeCategory"), status)
							else
								status = attrData.Upgrade(catID)	
								if !status.ok
									return .SetErrObj(.InitErrObj("_UpgradeCategory"), status)
								else
									status = attrData.DbPut()
									if !status.ok
										return .SetErrObj(.InitErrObj("_UpgradeCategory"), status)
									end				
								end		
							end
								
							Assoc rtn
							rtn.ok = true
							
							return rtn
						end

	
	public function Void __Init()
								
							Object tempObj = OS.NewTemp(this)
									
							$DataManager.AttrGroupPrep = tempObj
							
							/* map rimsNodeClassification table columns to createInfo fields (used for inheritance)*/
							Assoc dbToCreateInfoMap
							
							dbToCreateInfoMap.('rimsSubject') =	'RMCreateInfo.rimssubject'
							dbToCreateInfoMap.('rimsOfficial') =	'RMCreateInfo.official'
							dbToCreateInfoMap.('rimsDocDate') =	'RMCreateInfo.docDate'
							dbToCreateInfoMap.('rimsStatus') =	'RMCreateInfo.fileStatus'
							dbToCreateInfoMap.('rimsStatusDate') =	'RMCreateInfo.statusDate'
							dbToCreateInfoMap.('rimsEssential') =	'RMCreateInfo.essential'
							dbToCreateInfoMap.('rimsReceivedDate') =	'RMCreateInfo.rimsReceivedDate'
							dbToCreateInfoMap.('rimsStorage') =	'RMCreateInfo.storage'
							dbToCreateInfoMap.('rimsAccession') =	'RMCreateInfo.rimsAccession'
							dbToCreateInfoMap.('rimsAddressee') =	'RMCreateInfo.rimsAddressee'
							dbToCreateInfoMap.('rimsCyclePeriod') =	'RMCreateInfo.vitalRecordCycle'
							dbToCreateInfoMap.('rimsNextReviewDate') =	'RMCreateInfo.nextReviewDate'
							dbToCreateInfoMap.('rimsLastReviewDate') =	'RMCreateInfo.rimsLastReviewDate'
							dbToCreateInfoMap.('rimsRSI') =	'RMCreateInfo.rsi'
							dbToCreateInfoMap.('rimsOriginator') =	'RMCreateInfo.rimsOriginator'
							dbToCreateInfoMap.('rimsSentTo') =	'RMCreateInfo.rimsSentTo'
							dbToCreateInfoMap.('rimsEstablishment') =	'RMCreateInfo.rimsEstablishment'	
									
							tempObj._fRimsDbToCreateInfoMap = dbToCreateInfoMap
						
						end

end

package DATAMANAGER::CORE

public object CatFrameAttrPrep inherits DATAMANAGER::CORE::AttrGroupPrep

	
	public function Assoc PrepareCategoryFrame(Object prgCtx, \
															Assoc attrVals,\
															Assoc categoryFrame)
						
							Assoc 	rtn = .InitErrObj("PrepareCategoryFrame")
							Assoc 	status	
							String 	attrKey
							
							// *** put the attribute data into attrGroups for WebService
							for attrKey in Assoc.Keys(attrVals)
								List attrKeys = ._ExpandKeysCatFrame(attrKey)
								
								// *** allow for clearing of attribute set rows
								if Length(attrKeys)
								
									// *** set the value
									status = ._SetAttrValueCatFrame(categoryFrame, attrKeys, attrVals.(attrKey), true)
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn
									end
									
								end	
							end	
							
							return rtn
								
						end

	
	private function Assoc _CopyCatFrame(Assoc categoryFrame, \
												  Dynamic attrGroup)
						
							Assoc srcVals = attrGroup.Values[1]
							Assoc newAssoc = Assoc.Copy(srcVals)
							Integer key
							
							for key in Assoc.Keys(newAssoc)
								newAssoc.(key) = Assoc.Copy(srcVals.(key))
								newAssoc.(key).Values = {undefined}
							end
						
							return newAssoc
						end

	
	private function List _ExpandKeysCatFrame(Dynamic elements)
							
							// *** allow either string separated by periods or regular list
							if Type(elements) == StringType
								elements = Str.Elements(elements, ".")
								Integer i
								for i = 1 to Length(elements)
									elements[i] = Str.StringToInteger(elements[i])	
								end
								
							end
							
							List keys
							
							switch(Length(elements))
								
								case 3
									keys = {{elements[1], elements[2]}, elements[3]}		
								end
								
								case 5
									keys = {{elements[1], elements[2]}, elements[3], elements[4], elements[5]}	
								end	
							
							end
							
						
							return keys
						end

	
	private function Assoc _GetAttrGroupByKey( Assoc attrGroup, \
														   Integer key)
						
						
							if IsFeature(attrGroup, "Values")
								Assoc catGroup = attrGroup.Values[ 1 ]
								return catGroup.(key)
							else
								return attrGroup.(key)	
							end	
							 
							return undefined
						end

	
	private function Assoc _GetAttrGroupFromKeyList(Assoc categoryFrame, \
																List attrKeys, \
																Integer cardinality = undefined)
						
							Assoc rtn, status
							Dynamic attrGroup = categoryFrame
						
							if Type(attrKeys[1]) == ListType
								// *** get the category group
								List catKeys = attrKeys[1]
								status = ._GetCatGroupByKey(categoryFrame, catKeys)
								if !status.ok
									return .SetErrObj(.InitErrObj("._GetAttrGroupFromKeyList"), status)
								else
									attrGroup = status.result
								end
								if IsUndefined(attrGroup)
									return .SetErrObj(.InitErrObj("._GetAttrGroupFromKeyList"), Str.Format("Could not find category ID %1 in the template for this item. Please check that the template contains all the categories specified in your CSV", attrKeys[1]))
								else
									attrGroup = ._GetAttrGroupByKey(attrGroup, attrKeys[2])
									if IsUndefined(attrGroup)
										return .SetErrObj(.InitErrObj("._GetAttrGroupFromKeyList"), Str.Format("Could not find attribute ID %1 in the template for this item.", attrKeys[2]))
									end
								end		
							else
								attrGroup = ._GetAttrGroupByKey(attrGroup, attrKeys[1])
								if IsUndefined(attrGroup)
									return .SetErrObj(.InitErrObj("._GetAttrGroupFromKeyList"), Str.Format("Could not find attribute ID %1 in the template for this item.", attrKeys[1]))
								end					
							end	
						
								
							// *** is this a set?
							Integer count	
							if IsDefined(cardinality)
								if Length(attrGroup.Values) >= cardinality
									for attrGroup in attrGroup.Values
										count+=1		
										if count==cardinality
											break
										end
									end	
								elseif Length(attrGroup.Values)
									// *** we need to add a new row. All sets will have at least 1 row already. Take advantage of this and copy the row.
									Assoc template = ._CopyCatFrame(categoryFrame, attrGroup)
									attrGroup.Values =  {@attrGroup.Values, template}
									attrGroup = template
								else
									return .SetErrObj(.InitErrObj("._GetAttrGroupFromKeyList"), "Could not get template to add new row to attribute set")
								end	
							end
							
							rtn.ok = true
							rtn.result = attrGroup
							return rtn			
							
						end

	
	private function Assoc _GetCatGroupByKey(Assoc categoryFrame, \
														  Dynamic inputKey)
							List frameKey
							Assoc rtn
							rtn.ok = true
							Assoc group
						
							for frameKey in Assoc.Keys(categoryFrame.fData)
								if frameKey == inputKey
									group = categoryFrame.fData.(frameKey)
								
								elseif frameKey[1] == inputKey[1]
								
									// *** this means the category needs to be upgraded
									return .SetErrObj(.InitErrObj("_GetAttrGroupByKey"), Str.Format("Category DataID %1 needs to be upgraded on either your template or on the parent container.", inputKey[1]))
									
											
								end
							end
							
							rtn.result = group
							
							return rtn
						end

	
	private function Assoc _SetAttrValueCatFrame(	Assoc categoryFrame, \
																List attrKeys, \
																Dynamic vals, \
																Boolean returnError)
						
							Assoc	rtn = .InitErrObj("_SetAttrValue")
							Dynamic	attrGroup
							List 	attKey	
							Integer cardinality
							Integer attId
							Assoc	status
							
							// *** did the vals come in as a list of assoces? If so, it's a set.
							if Type(vals) == ListType && Length(vals) && Type(vals[1]) == Assoc.AssocType
								// *** means we have an attribute set	
								Assoc setVals
								cardinality = 0
								for setVals in vals
									cardinality+=1
									status = ._GetAttrGroupFromKeyList(categoryFrame, attrKeys, cardinality)
									if !status.ok && returnError
										.SetErrObj(rtn, status)
										return rtn
									else
										attrGroup = status.result	
									end								
									
									for attId in Assoc.Keys(setVals)
										// *** example structure
										// *** attrGroup.Values[ setId ].Values[ setCardinality ].Values[ 1 ].Values
										attKey = {attId}
										vals = setVals.(attId)
										status = _SetAttrValueCatFrame(attrGroup, attKey, vals, returnError)
										if !status.ok
											.SetErrObj(rtn, status)
											return rtn
										end	
									end
								end
						
							// *** do we have attrKeys that are 4 elements long -- that means a set too
							elseif Length(attrKeys) == 4
								cardinality = attrKeys[3]
								attId = attrKeys[4]
								
								status = ._GetAttrGroupFromKeyList(categoryFrame, {attrKeys[1], attrKeys[2]}, cardinality)
								if !status.ok && returnError
									.SetErrObj(rtn, status)
									return rtn
								else
									attrGroup = status.result	
								end			
								
								attKey = {attId}	
								status = _SetAttrValueCatFrame(attrGroup, attKey, vals, returnError)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end			
								
							// *** regular attribute or attribute set with "<clear>" directive	
							else
								// *** vals is expected to be a list
								vals = Type(vals) != ListType ? {vals} : vals
								
								// **** loop through the keys until we get the bottom level attr
								status = ._GetAttrGroupFromKeyList(categoryFrame, attrKeys)
								if !status.ok && returnError
									.SetErrObj(rtn, status)
									return rtn
								else
									attrGroup = status.result	
								end			
								
						
								// **** now set the value in the frame that we got
								attrGroup.Values = vals	
								
							end	
								
							return .SuccessResult()
						
						end

end

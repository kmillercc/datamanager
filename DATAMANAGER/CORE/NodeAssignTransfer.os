package DATAMANAGER::CORE

public object NodeAssignTransfer inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	String	fActionPerformed = 'Transfer Assigned.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeAssignTransfer'
	override	String	fSubKey = 'Node.AssignTransfer'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node, \
												Assoc thisActionFlags = actionFlags)		// *** allows for sending in flags independent of overall job options (for name collisions specifically)
						
			
				Object physObjService = $Service.ServiceRegistry.getItem( "PhysicalObjects" )	
				if IsUnDefined( physObjService )
					return .SetErrObj(.InitErrObj("PerformAction"), "Could not get PhysicalObjects web service")
				end
			
				Object physObjServiceTemp = OS.NewTemp( physObjService )
				physObjServiceTemp.fOutArgs = Assoc.CreateAssoc()
				physObjServiceTemp.fSocket = Undefined
				physObjServiceTemp.SetProgramSession( prgCtx )
			
				
				// *** assign to transfer?
				String		TransferID = apiDataObj.getValueFromCol("$TransferID")
				Boolean		bSynchDates = actionFlags.SyncTransferDates
			
				Assoc status = physObjServiceTemp.PhysObjAssignToTransfer( TransferID, node.pId, bSynchDates )
				if !status.ok
					OS.Delete( physObjServiceTemp )
					return .SetErrObj(.InitErrObj("PerformAction"), status)
				end
				
				// *** destroy temp obj
				OS.Delete( physObjServiceTemp )
				
				
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.node = node
				rtn.result.actionPerformed = .fActionPerformed
				
				return rtn
			end

end

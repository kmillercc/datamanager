package DATAMANAGER::CORE

public object NodeCirculate inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeCirculate'
	override	String	fSubKey = 'Node.Circulate'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node, \
												Assoc thisActionFlags = actionFlags)		// *** allows for sending in flags independent of overall job options (for name collisions specifically)
						
				String auditStr
				Assoc status
			
				Object physObjService = $Service.ServiceRegistry.getItem( "PhysicalObjects" )	
				if IsUnDefined( physObjService )
					return .SetErrObj(.InitErrObj("PerformAction"), "Could not get PhysicalObjects web service")
				end
			
				Object physObjServiceTemp = OS.NewTemp( physObjService )
				physObjServiceTemp.fOutArgs = Assoc.CreateAssoc()
				physObjServiceTemp.fSocket = Undefined
				physObjServiceTemp.SetProgramSession( prgCtx )
			
				// *** charge in? ***
				if actionFlags.poChargeIn
					Integer		Action = 2 		// 2 = Cancel borrow, 3= Return *** assumed to be a cancel
					Assoc returnInfo
					ReturnInfo.fReturnedDate = Date.Now()
					ReturnInfo.fLocation = apiDataObj.GetValueFromCol("$CurrentLocation")
			
					status = physObjServiceTemp.PhysObjChargeIn( node.pId, Action, ReturnInfo )
					if !status.ok
						OS.Delete( physObjServiceTemp )
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					end
					auditStr += "Charged In."
				end
			
				// *** charge out? ***
				if actionFlags.poChargeOut		
			
					Assoc borrowInfo
					
					Integer userId = apiDataObj.GetValueFromCol("$BorrowedByID")
					Integer obtainedById = apiDataObj.GetValueFromCol("$ObtainedByID")
					
					borrowInfo.fUserName = apiDataObj.GetValueFromCol("$BorrowedByName")
					borrowInfo.fUserID = IsDefined(userId) ? userId : 0
					borrowInfo.fBorrowDate = apiDataObj.GetValueFromCol("$BorrowedDate")
					borrowInfo.fReturnByDate = apiDataObj.GetValueFromCol("$ReturnDate")
					borrowInfo.fLocation = apiDataObj.GetValueFromCol("$CurrentLocation")
					borrowInfo.fSecurityOverride = true
					
					borrowInfo.fObtainedByID = IsDefined(obtainedById) ? obtainedById : 0
					borrowInfo.fObtainedBy = apiDataObj.GetValueFromCol("$ObtainedByName")
					borrowInfo.fBorrowComments = apiDataObj.GetValueFromCol("$BorrowComments")
					
					if IsUndefined(physObjServiceTemp)
						physObjService = $Service.ServiceRegistry.getItem( "PhysicalObjects" )	
						if IsUnDefined( physObjService )
							return .SetErrObj(.InitErrObj("PerformAction"), "Could not get PhysicalObjects web service")
						end
						physObjServiceTemp = OS.NewTemp( physObjService )
						physObjServiceTemp.fOutArgs = Assoc.CreateAssoc()
						physObjServiceTemp.fSocket = Undefined
						physObjServiceTemp.SetProgramSession( prgCtx )			
					end
					
					status = physObjServiceTemp.PhysObjChargeOut( node.pId, borrowInfo )		
					if !status.ok
						OS.Delete( physObjServiceTemp )
						status.errMsg = "Error attempting to charge out: " + status.errMsg
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					end	
			
							
					auditStr += "Charged Out."			
				end
				
				
				// *** destroy temp obj
				OS.Delete( physObjServiceTemp )
				
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.node = node
				rtn.result.actionPerformed = auditStr
				
				return rtn
			end

end

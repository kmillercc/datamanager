package DATAMANAGER::CORE

public object NodeUpdate inherits DATAMANAGER::CORE::NodeProcesses

	override	String	fActionPerformed = 'Updated.'
	override	Boolean	fAllowPostActionUpdate = FALSE
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeUpdate'
	override	String	fSubKey = 'Node.Update'


	
	override function Assoc GetCurrentData(Dynamic prgCtx, DAPINODE node, Dynamic apiDataObj, Dynamic dataFlags, Object llNode)
		
				Object	currentDataObj = $DataManager.ApiDataObj.New("UPDATE")
							
				// *** its important that we don't pass in the colNames.  we want to get all the phys object data for merging values--not just the col names that this CSV used
				Assoc status = currentDataObj.PopulateFromNode(prgCtx, node, dataFlags, {}, true, llNode)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				end			
		
				return Assoc{'ok': true, 'result': currentDataObj}
		
			end

	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node, \
												Assoc thisActionFlags = actionFlags)		// *** allows for sending in flags independent of overall job options (for name collisions specifically)
						
				Assoc		status
				String		auditStr = ""
				
				// *** permission update
				if thisActionFlags.doPermUpdate
					status = $DataManager.NodeUpdatePerms.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
				
				// *** rename
				if thisActionFlags.doRename
					status = $DataManager.NodeRename.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
				
				// *** metadata update
				if thisActionFlags.doMetadataUpdate
					status = $DataManager.NodeUpdateMetadata.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end			
				
				// *** circulation
				if thisActionFlags.doPhysObjCirc
					status = $DataManager.NodeCirculate.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
				
				// *** box
				if thisActionFlags.doPhysObjBox
					status = $DataManager.NodeBox.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
				
				// *** locator
				if thisActionFlags.doPhysObjLocator
					status = $DataManager.NodeAssignLocator.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
			
				// *** transfer
				if thisActionFlags.doPhysObjXfer
					status = $DataManager.NodeAssignTransfer.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
				
				// *** labels
				if thisActionFlags.doPhysObjLabel
					status = $DataManager.NodeGenerateLabels.PerformAction(prgCtx, apiDataObj, options, actionFlags, llNode, parentNode, node)
					if !status.ok
						return .SetErrObj(.InitErrObj("PerformAction"), status)
					else
						auditStr += status.result.actionPerformed		
					end	
				end		
				
				Assoc result
				result = Assoc.CreateAssoc()
				result.node = node
				result.actionPerformed = Length(Str.Trim(auditStr)) ? auditStr : "Nothing to update. No action performed."
				
				return .SuccessResult(result)
			 
			end

	
	override function Assoc PreProcessData(Object prgCtx, Assoc options, Object apiDataObj, Object currentDataObj, Assoc actionFlags)
		
				Assoc status
			
				// *** is this a physical object? if so, we need to get media type and process the media type fields and set some action flags if possible
				if actionFlags.doPhysObjData || actionFlags.doPhysObjData || actionFlags.doPhysObjCirc || actionFlags.doPhysObjBox || actionFlags.doPhysObjLocator || actionFlags.doPhysObjXfer || actionFlags.doPhysObjLabel || actionFlags.doRecManData
		
					// *** call the Phys Obj Preprocessor to handle setting these flags
					status = $DataManager.PhysObjPreProcessor.ProcessDataForUpdate(prgCtx, options, apiDataObj, currentDataObj, actionFlags)
					if !status.ok
						return .SetErrObj(.InitErrObj("PreProcessData"), status)
					end					
				end	
		
				// *** recman pre-processing - we may need to do some data wrangling there too
				if actionFlags.doRecManData
				
					// *** check Official value -- we might need to set a flag
					Dynamic val = apiDataObj.GetValueFromCol("$RDOFFICIAL")
					if IsDefined(val) && !val
						apiDataObj._fApiVals.metadata.rmUpdateInfo.fOfficialRemove = true
					end
						
					// Call RM Preprocessor
					status = $DataManager.RMPreProcessor.ProcessRMDataForUpdate(prgCtx, apiDataObj)
					if !status.ok
						return .SetErrObj(.InitErrObj("PreProcessData"), status)
					end		
				end
			
				return Assoc{'ok': true}
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
														Assoc actionFlags, \																		
														Assoc options, \
														Object llNode, \
														DAPINODE node, \
														Object apiDataObj)
															
				Assoc		status
				
				/* *** action flags
				actionFlags.doRename
				actionFlags.doMetadataUpdate
				actionFlags.doPermUpdate
				actionFlags.doUpdate
				*/
				
				// *** call LLIAPI's CheckUpdate
				status = llNode.CheckUpdate( node )
				if !status.ok
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
				end		
				
				// *** metadata update
				if actionFlags.doMetadataUpdate
					status = $DataManager.NodeUpdateMetadata.SubclassValidateItem(prgCtx, actionFlags, options, llNode, node, apiDataObj)
					if !status.ok
						return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
					end		
				end			
				
				// *** permission update
				if actionFlags.doPermUpdate
					status = $DataManager.NodeUpdatePerms.SubclassValidateItem(prgCtx, actionFlags, options, llNode, node, apiDataObj)
					if !status.ok
						return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
					end		
				end		
				
				// *** rename
				if actionFlags.doRename
					status = $DataManager.NodeRename.SubclassValidateItem(prgCtx, actionFlags, options, llNode, node, apiDataObj)
					if !status.ok
						return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
					end		
				end		
				
				return .SuccessResult()	
			end

end

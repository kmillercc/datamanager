package DATAMANAGER::CORE

public object NodeUpdatePerms inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	String	fActionPerformed = 'Perms Updated.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeUpdatePerms'
	override	String	fSubKey = 'Node.UpdatePerms'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \	
												Frame apiDataObj, \
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE parentNode, \
												DAPINODE node)
						
				Assoc		status
				
				// *** get the permData
				Assoc permData = apiDataObj.getValue("permData")
				
				// *** just call NodeRightsUpdate
				status  = llNode.NodeRightsUpdate(node, permData.changeList, permData.options)
				if !status.ok
					return .SetErrObj(.InitErrObj("PerformAction"), status)
				end
				
				Assoc result
				result.node = node
				result.actionPerformed = .fActionPerformed
				
				return .SuccessResult(result)
				
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \		
														Assoc actionFlags, \																
														Assoc options, \
														Object llNOde, \
														DAPINODE node, \
														Object apiDataObj)
															
				// *** get the changeList
				Assoc 	permData = apiDataObj.getValue("permData")	
				
				// *** use LLNODE CheckRightsUpdate
				Assoc status = llNode.CheckRightsUpdate(node, permData.changeList, permData.options)
				if !status.ok
					return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
				end	
					
				return .SuccessResult()	
				
			end

end

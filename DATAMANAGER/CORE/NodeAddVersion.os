package DATAMANAGER::CORE

public object NodeAddVersion inherits DATAMANAGER::CORE::NodeProcesses

	override	String	fActionPerformed = 'Version Added.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeAddVersion'
	override	String	fSubKey = 'Node.AddVersion'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
															Frame apiDataObj, \							
															Assoc options, \
															Assoc actionFlags, \
															Object llNode, \
															DAPINODE parentNode, \
															DAPINODE node)
																	
						Assoc 		rtn 
						Assoc		status
						Assoc		addVerInfo, renditionData
						Assoc		metadataVals = apiDataObj.GetValue("metadata")
						
						DAPIVERSION version
						
						// *** set the result in the rtn
						Assoc result
						result.node = node
						result.actionPerformed = .fActionPerformed	
						rtn.result = result
						rtn.ok = true
				
						// get some values from apiDataObj		
						String filePath = apiDataObj.GetValue("filePath")
						String 	renditionType = apiDataObj.GetValueFromCol("$RENDITIONTYPE")	
					
						// *** regular version	
						if IsUndefined(renditionType)
					
							// *** now get the initial assoc for the addVerInfo
							status = ._GetSetAddVerInfo(prgCtx, node, metadataVals, apiDataObj, options, actionFlags, filePath, parentNode, llNode)
							if !status.ok
								return .SetErrObj(.InitErrObj("PerformAction"), status)
							else
								addVerInfo = status.result
							end
							
							// *** hand off to LLIAPI
							status = llNode.NodeAddVersion(node, addVerInfo) 		
							if !status.ok
								return .SetErrObj(.InitErrObj("PerformAction"), status)
							else
								version = status.Version
							
								// *** set any of the following if we have them
								Date verCreateDate = apiDataObj.GetValueFromCol("$VERCREATEDATE")
								if IsDefined(verCreateDate)
									version.pCreateDate = verCreateDate
								end
								
								Integer verCreator = apiDataObj.GetValueFromCol("$VERCREATOR")
								if IsDefined(verCreator)
									version.pOwner = verCreator
								end		
								
								Date verModDate = apiDataObj.GetValueFromCol("$VERMODIFIEDDATE")
								if IsDefined(verModDate)
									version.pModifyDate = verModDate
								end
								
								Dynamic res = DAPI.UpdateVersion(version)
								if IsError(res)
									return .SetErrObj(.InitErrObj("PerformAction"), res)
								end
								
							end			
					
						// *** Rendition		
						elseif IsDefined($Rendition)		
						
							Integer verNum = apiDataObj.GetValueFromCol("$VERSIONNUM")	
							
							if IsUndefined(verNum)
								verNum = DAPI.VERSION_GET_CURRENT
							end
							
							version = DAPI.GetVersion(node, verNum)
							
							if IsError(version)
								return .SetErrObj(.InitErrObj("PerformAction"), Str.Format("Failed to get version %1 for nodeid %2", verNum, Node.pId))
							end
							
							// *** now get the initial assoc for the addVerInfo
							status = ._GetSetRenditionInfo(prgCtx, renditionType, options, filePath, llNode)
							if !status.ok
								return .SetErrObj(.InitErrObj("PerformAction"), status)
							else
								renditionData = status.result
							end
							
							// *** delete any existing rendition of this type for this version
							if Length(DAPI.ListRenditions(version))
								status = llNode.RenditionDelete(version, renditionType)
								if !status.ok
									// KM 8-16-17 this error may be fine. Log it and see if we can add the rendition
									.SetErrObj(.InitErrObj("PerformAction"), status)
								end
							end
							
							// *** Now add rendition
							status = llNode.RenditionAdd(version, renditionData) 		
							if !status.ok
								return .SetErrObj(.InitErrObj("PerformAction"), status)
							else
								// *** KM 9/13/16 Ensure that we set these values for rendition too
								DAPIVERSION rendition = status.Version
								
								// *** set any of the following if we have them
								Date verCreateDate = apiDataObj.GetValueFromCol("$VERCREATEDATE")
								if IsDefined(verCreateDate)
									rendition.pCreateDate = verCreateDate
								end
								
								Integer verCreator = apiDataObj.GetValueFromCol("$VERCREATOR")
								if IsDefined(verCreator)
									rendition.pOwner = verCreator
								end		
								
								Date verModDate = apiDataObj.GetValueFromCol("$VERMODIFIEDDATE")
								if IsDefined(verModDate)
									rendition.pModifyDate = verModDate
								end
								
								Dynamic res = DAPI.UpdateVersion(rendition)
								if IsError(res)
									return .SetErrObj(.InitErrObj("PerformAction"), res)
								end	
							end
							
							result.actionPerformed = "Rendition Added."
					
						end			
						
						// *** delete files if flag is present
						if options.archiveImportedFiles
							.ArchiveFile(filePath, options.fileArchivePath, options.rootSourcePath)	
						elseif options.removeImportedFiles
							.RemoveFile(filePath, options.fileRemovalPath)	
						end
					
						return rtn
					end

	
	override function Assoc ValidateItem(Object prgCtx, \
														String action, \
														Frame apiDataObj, \
														Assoc dataFlags, \
														Assoc options)
														
						Assoc 		status
						DAPINODE	node										
						Integer 	subtype
				
						// *** we will need the filePath
						// *** Note: All file structure loads pass in the filePath. This is only for CSV loads 
						String 	filePath = apiDataObj.GetValue("filePath")
						String 	rootFilePath = apiDataObj.GetValue("rootFilePath")
						String	sourcePath = apiDataObj.GetValueFromCol("$SOURCEPATH")
						String 	fileName = apiDataObj.GetValueFromCol("$FILENAME")
						
						// definitely need a filePath here
						if IsUndefined(filePath)
							
							// KM 11-28-18 added false flag to disallow folder path
							status = .GetFilePath(options, fileName, sourcePath, rootFilePath, false)
							if !status.ok
								return .SetErrObj(.InitErrObj("ValidateItem"), status)
							else
								filePath = status.result
							end			
						end		
						
						// Get the node we're operating on		
						status = ._GetNode(prgCtx, options, apiDataObj, action)
						if !status.ok
							return .SetErrObj(.InitErrObj("ValidateItem"), status)
						else
							node = status.result
							subType = node.pSubType
						end				
						
						// grab the llNode Object
						Object 	llNode = $LLIAPI.LLNodeSubsystem.GetItem(subtype)
						
						// *** are we supposed to use this file's modify date for our create/modify dates?
						if options.useFileModifyDate || options.useFileCreateDate
							Assoc fileStats = File.Stat(filePath)
							if IsUndefined(fileStats)
								return .SetErrObj(.InitErrObj("ValidateItem"), Str.Format("Could not get file information to set create/modify dates for filePath %1", filePath))
							end
							
							if options.useFileCreateDate
								apiDataObj.SetValueFromCol("$CREATEDATE", fileStats.(File.StatLastChangeTime))
								apiDataObj.SetValueFromCol("$MODIFYDATE", fileStats.(File.StatLastChangeTime))
								dataFlags.hasDapiNodeData = true
							
							elseif options.useFileModifyDate
								apiDataObj.SetValueFromCol("$CREATEDATE", fileStats.(File.StatLastModifyTime))
								apiDataObj.SetValueFromCol("$MODIFYDATE", fileStats.(File.StatLastModifyTime))				
								dataFlags.hasDapiNodeData = true
												
							end
						end	
				
						// set some values in the apiDataObj		
						apiDataObj.setValue("subType", subType)
						apiDataObj.setValue("nodeId", node.pId)
						apiDataObj.setValue("filePath", filePath)
						
						// *** action flags
						Assoc actionFlags = ._GetActionFlags(prgCtx, action, options, subtype, dataFlags)				
								
						// *** call LLIAPI's CheckAddVersion
						status = llNode.CheckAddVersion( node )
						if !status.ok
							return .SetErrObj(.InitErrObj( "ValidateItem" ), status)
						end		
						
						// *** if it's a major/minor node, we need to have some indication of which type user wants to add
						Object	dapiCtx = prgCtx.DSession()
						if llNode.IsMajorMinorNode( dapiCtx, node )
							Boolean majorVal = apiDataObj.GetValueFromCol("$VERMAJOR")
							Boolean minorVal = apiDataObj.GetValueFromCol("$VERMINOR")
							
							if IsUndefined(majorVal) && IsUndefined(minorVal)
								return .SetErrObj(.InitErrObj( "ValidateItem" ), "This document has major/minor versioning. You need a value in the $VERMAJOR and/or $VERMINOR column to indicate what type of version this is")
							end
						end
				
						Assoc 		rtn		
						rtn.ok = true
						rtn.result = Assoc.CreateAssoc()
						rtn.result.oldVals = undefined
						rtn.result.oldCsvData = undefined	
						rtn.result.nodeId = undefined
						rtn.result.actionFlags = actionFlags
						rtn.result.filePath = filePath												
						
						return rtn
														
					end

	
	private function _GetSetAddVerInfo(	Object prgCtx, \
															DAPINODE node, \	
															Assoc metadataVals, \
															Object apiDataObj, \							
															Assoc options, \
															Assoc actionFlags, \
															String filePath, \
															DAPINODE parentNode, \
															Object llNode)
																
						Assoc 		rtn = .InitErrObj( "_GetSetaddVerInfo" )
						Assoc		status
						Object 		dapiCtx = prgCtx.DSession()
						
						// **** start with base Assoc	
						Assoc addVerInfo = Assoc.CreateAssoc()
						
						// *** versionInfo?
						if llNode.fVersioned
							String verFileName = apiDataObj.GetValueFromCol("$VERFILENAME")
							._GetFileInfo(options, addVerInfo, filePath, verFileName)
						end			
					
						// *** hardcode these flags for now
						
						// *** set up creatInfo constants we want 
						addVerInfo.dapiCtx = dapiCtx
						addVerInfo.splitTran = false
					
						// *** prepare attributes for create with attrData
						if actionFlags.doAttrData
						
							// *** Make use of the DocMan WebService again to prepare the values for create.  It will set the values into addVerInfo
							status = $DocManService.AttributeHandlerRegistry.PrepareAttributesForAddVersion( prgCtx, node, metadataVals.attrGroups, addVerInfo )
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn
							end				
						end	
						
						// *** check for major minor
						Boolean majorVal = apiDataObj.GetValueFromCol("$VERMAJOR")
						Boolean minorVal = apiDataObj.GetValueFromCol("$VERMINOR")
						
						// *** It is a major val if the major value is true and minor value is undefined or false
						if llNode.IsMajorMinorNode( dapiCtx, node )
							// *** set the major/minor vals
							if majorVal 
								if IsUndefined(minorVal) || !minorVal
									addVerInfo.IsMajorVersion = TRUE
								else
									// *** get the current version
									DAPIVERSION version = DAPI.GetVersion(node, DAPI.VERSION_GET_CURRENT)
								
									if IsError(version)
										return .SetErrObj(rtn, "Could not add minor version. Could not get current version")
									else
										AddVerInfo.VerMajor = version.pVerMajor
										AddVerInfo.VerMinor	= version.pVerMinor + 1
									end	
								end
							else
								// *** get the current version
								DAPIVERSION version = DAPI.GetVersion(node, DAPI.VERSION_GET_CURRENT)
								
								if IsError(version)
									return .SetErrObj(rtn, "Could not add minor version. Could not get current version")
								else
									AddVerInfo.VerMajor = version.pVerMajor
									AddVerInfo.VerMinor	= version.pVerMinor + 1
								end		
							end		
						end
						
						// *** set the other version values that we can set
						// *** {'$VERSIONNUM','$VERDESCRIPTION','$VERMAJOR','$VERMINOR','$VERFILENAME','$VERFILESIZE','$VERCREATEDATE','$VERMODIFIEDDATE','$VERMIMETYPE','$VERCREATOR'}
						
						// *** these are the values we can actually set at create time. Other values we'll need to set afterwards ($VERCREATEDATE, $VERMODIFIEDDATE, $VERCREATOR)
						AddVerInfo.comment = apiDataObj.GetValueFromCol("$VERDESCRIPTION") 
						AddVerInfo.fileSize = apiDataObj.GetValueFromCol("$VERFILESIZE")
						
						// *** KM 8-24-16 Need to make sure mime type is not already set
						String csvMimeType = apiDataObj.GetValueFromCol("$VERMIMETYPE")
						if IsDefined(csvMimeType)
							AddVerInfo.mimeType = csvMimeType
						end		
						
						
						return .SuccessResult(addVerInfo)
					
					end

	
	private function _GetSetRenditionInfo(	Object prgCtx, \
																String renditionType, \							
																Assoc options, \
																String filePath, \
																Object llNode)
																					
						// **** start with base Assoc	
						Assoc verData = Assoc.CreateAssoc()
						
						// *** add the rendition type 
						verData.VerType = renditionType
						
						// *** versionInfo?
						if llNode.fVersioned
							._GetFileInfo(options, verData, filePath)
						end			
						
						/*
						// *** modify the filename to make it unique
						String ext = "." + verData.filetype
						String oldName = verData.filename
						String newName = Str.Replace(oldName, ext, "")
						newName = Str.Format("%1_%2.%3", newName, Date.Tick(), verData.filetype)
						
						// *** rename the file on file system
						String oldPath = verData.VersionFile
						String newPath = Str.Replace(oldPath, verData.filename, newName)
						
						if $Kernel.FileUtils.Copy(verData.VersionFile, newPath)
						
							verData.VersionFile = newPath
							verData.filename = newName
							verData.copiedFile = newPath
							
						else
							return .SetErrObj(rtn, Str.Format("Failed to copy file from path %1 to path %2", oldPath, newPath))
						end	
						*/	
						
						return .SuccessResult(verData)
					
					end

end

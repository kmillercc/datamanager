package DATAMANAGER::CORE

public object PhysObjPreProcessor inherits DATAMANAGER::LoadMetadataObjects

	
	public function Assoc ProcessDataForUpdate(Object prgCtx, \
													Assoc options, \
													Frame apiDataObj, \
													Frame currentDataObj, \
													Assoc actionFlags)
							
				._SetActionFlagsUpdate(prgCtx, apiDataObj, currentDataObj, actionFlags)
				._MergeValsForUpdate(currentDataObj, apiDataObj)	
										
				Assoc rtn
				rtn.ok = true	
				return rtn
			
			end

	
	private function void _MergeValsForUpdate(Frame currentDataObj, Frame apiDataObj)
						
				// *** the llNode.NodeUpdate function (with objAction=info2) assumes we have all values from the request
				// *** so we have to pull all the current vals and merge new vals in with them
			
				Assoc currentApiVals = currentDataObj.GetAssocVals()
				Assoc newApiVals = apiDataObj.GetAssocVals()
			
				// *** get the phys obj data
				// *** copy the current vals so we don't overwrite the current Data
				Assoc currentPoData = Assoc.Copy(currentApiVals.metadata.poData)
				Assoc newPoData = newApiVals.metadata.poData	
				
			
				Assoc mergedVals = Assoc.Merge(currentPoData, newPoData)
				
				// *** now set back into apiDataObj
				apiDataObj._fApiVals.metadata.poData = mergedVals
			
			end

	
	private function void _SetActionFlagsUpdate(Object prgCtx, Frame apiDataObj, Frame currentDataObj, Assoc actionFlags)
			
				/* action flag defaults						
					a.poChargeIn = false
					a.poChargeOut = false
					a.poChangeCurrentLocation = false
					a.poRemoveFromBox = undefined
					a.poAssignToBox = undefined
					a.poSyncFromToDates = options.poSyncFromToDates
					a.poSyncTransferDates = options.poSyncTransferDates
					a.doLabelLocator = false
					
					a.doPhysObjCirc
					a.doPhysObjBox
					a.doPhysObjLocator
					a.doPhysObjXfer
					a.doPhysObjLabel
												
				*/					
				
				String facility, area, locator
			
				// *** Boxing information
				if actionFlags.doPhysObjBox
					Integer boxId = apiDataObj.GetValueFromCol("$BoxID")
					Integer currentBoxId = currentDataObj.GetValueFromCol("$BoxID")
					if IsDefined(boxId) && boxId != currentBoxId
						actionFlags.poAssignToBox = boxId
						
						if IsDefined(currentBoxId)
							actionFlags.poRemoveFromBox = currentBoxId
						end
					end
				end	
				
			
				// *** generate label?  Are we supposed to pass in locator too?	
				if actionFlags.doPhysObjLabel
					
					// *** Assign Locator for the label if we have the data
					facility = apiDataObj.GetValueFromCol("$Facility")
					locator = apiDataObj.GetValueFromCol("$Locator")
					area = apiDataObj.GetValueFromCol("$Area") 
					if IsDefined(facility) && IsDefined(locator) && IsDefined(area) && \
						(facility != currentDataObj.GetValueFromCol("$Facility") || locator != currentDataObj.GetValueFromCol("$Locator") || area != currentDataObj.GetValueFromCol("$Area"))
			
						actionFlags.doLabelLocator = true
					end			
				end
			
				// *** circulation - figure out whether to charge in or charge out or both
				if actionFlags.doPhysObjCirc			
			
					// *** charge out?
					String borrowedBy = apiDataObj.GetValueFromCol("$BorrowedByName")
					Dynamic borrowedById = apiDataObj.GetValueFromCol("$BorrowedById")
					String currentBorrowedBy = currentDataObj.GetValueFromCol('$BorrowedByName')
					Integer currentBorrowedById = currentDataObj.GetValueFromCol('$BorrowedById')
					
					if Type(borrowedBy) == StringType && Str.Lower(borrowedBy) in {"<clear>", "[clear]"}
						
						if IsDefined(currentBorrowedById) || IsDefined(currentBorrowedBy)
					
							actionFlags.poChargeIn = true
						end	
						
					elseif Type(borrowedById) == StringType && Str.Lower(borrowedById) in {"h", "[clear]"}
						
						if IsDefined(currentBorrowedById) || IsDefined(currentBorrowedBy)
					
							actionFlags.poChargeIn = true
						end				
				
					elseif (IsDefined(borrowedBy) && currentBorrowedBy != borrowedBy) || (IsDefined(borrowedById) && currentBorrowedById != borrowedById)
					
						// *** is it already checked out?  In that case, charge in first
						if IsDefined(currentBorrowedBy) || IsDefined(currentBorrowedById)
							actionFlags.poChargeIn = true
						end	
						
						actionFlags.poChargeOut = true	
							
					end	
					
					// *** did we determine that we needed to do anything?
					actionFlags.doPhysObjCirc = actionFlags.poChargeOut || actionFlags.poChargeIn
					
				end
				
				if actionFlags.doPhysObjLocator
				
					facility = apiDataObj.GetValueFromCol("$Facility")
					area = apiDataObj.GetValueFromCol("$Area")
					locator = apiDataObj.GetValueFromCol("$Locator")
					
					if IsUndefined(facility) || IsUndefined(locator) || IsUndefined(area)
						actionFlags.doPhysObjLocator = false
					end
				
				end
			
			end

end

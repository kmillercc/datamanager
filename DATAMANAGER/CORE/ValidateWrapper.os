package DATAMANAGER::CORE

public object ValidateWrapper inherits DATAMANAGER::LoadObjects

	public		String	_fAction
	public		Dynamic	_fApiProcess
	public		Dynamic	_fApiVals
	public		Dynamic	_fApiValsTemplate
	public		Dynamic	_fAttrKeysMap
	public		Dynamic	_fCache
	public		Dynamic	_fCatAttrValidator
	public		Dynamic	_fColTypeMap
	public		Dynamic	_fColValidatorMap
	public		Dynamic	_fFindReplaceVals
	public		List	_fHiddenCols
	public		Dynamic	_fPrgCtx
	public		Dynamic	_fPushDownVals
	public		String	_fRootTargetPath
	public		Dynamic	_fSkipCols
	public		Dynamic	_fSysAttrValidator
	public		Dynamic	_fUGColValidatorMap
	override	Boolean	fEnabled = TRUE


	
	public function Assoc CheckHeader(List cols)
								
						Assoc 	rtn = .InitErrObj("CheckHeader")
						Assoc	status
						String	col
						
						for col in cols
							col = Str.Trim(col)	
				
							if (col[1] == "#" || col in ._fIgnoredColumns > 0)
							
								// ignore this column
				
				
							// *** regular $ columns
							elseif col[1] == "$"
								
								if IsUndefined(._fColValidatorMap.(col)) && IsUndefined(._fUGColValidatorMap.(col))
									// *** TODO should we return error here?
									return .SetErrObj(rtn, Str.Format("Unknown column %1", col))
								end	
								
								
							// *** column that tells us that a category is attached to the node.
							elseif IsDefined(Str.LocateI(col, "IsAttachedCat"))
					
								// *** save this as an attached category for this line
								String catPath = col[1:Str.LocateI(col, "IsAttachedCat")-2]
								status = ._fCatAttrValidator.GetCatId(._fPrgCtx, col, catPath)
								if !status.ok
									return .SetErrObj(rtn, status)
								end
	
							// *** column that tells us that a category isto be removed from the node.
							elseif IsDefined(Str.LocateI(col, "RemoveCat"))
					
								// *** save this as an attached category for this line
								String catPath = col[1:Str.LocateI(col, "RemoveCat")-2]
								status = ._fCatAttrValidator.GetCatId(._fPrgCtx, col, catPath)
								if !status.ok
									return .SetErrObj(rtn, status)
								end								
								
							// *** category attribute values
							elseif IsDefined(Str.Locate(col, ":"))
							
								status = ._fCatAttrValidator.ValidateAttrPath(._fPrgCtx, col)
								if !status.ok
									return .SetErrObj(rtn, status)
								else
									// *** register this key with the apiDataObj so we will know where to put it
									._RegisterAttrKey(col, status.result.attrKey)		
								end
							
							// *** system attributes
							else
							
								status = ._fSysAttrValidator.ValidateAttrName(._fPrgCtx, col)
								if !status.ok
									return .SetErrObj(rtn, status)
								else	
									// *** KM 1-28-2019 register this key with the apiDataObj so we will know where to put it
									._RegisterSysAttribute(col)
								end			
								
							end	
								
						end			
					
						// *** initialize the caches
						._InitCaches(cols)
					
					
						return rtn
					end

	
	public function Assoc GetAllApiVals()
								
			return ._fApiVals
					
		end

	
	public function Assoc GetApiVals(Integer itemNo)
								
						return ._fApiVals.(itemNo).GetAssocVals()
					
					end

	
	public function Frame GetFromCsvData(Assoc csvData)
								
						// *** check header first
						Assoc status = .CheckHeader(Assoc.Keys(csvData))	
						if !status.ok
							// for debugging purposes
							.SetErrObj(.InitErrObj("GetFromCsvData"), status)
						end	
					
						String	col
					
						for col in Assoc.Keys(csvData)
							if IsDefined(csvData.(col))
								.ValidateSetVal(1, col, csvData.(col))
							end	
						end
						
						return ._fApiVals.(1)	
					
					
					end

	
	public function Object New(Object prgCtx, String action, Assoc sourceSettings, String rootTargetPath, Object apiProcess = undefined)
								
						Frame f = ._NewFrame()
					
						f._fPrgCtx = prgCtx	
						f._fSkipCols = Assoc.CreateAssoc()
						f._fCache = Assoc.CreateAssoc()
						f._fApiVals = Assoc.CreateAssoc()
						f._fAction = action
						f._fCatAttrValidator = $DataManager.CatAttrValidator.New()
						f._fSysAttrValidator = $DataManager.SysAttrValidator.New()
						f._fRootTargetPath = rootTargetPath
						f._fApiProcess = apiProcess
						f._fColTypeMap = Assoc.CreateAssoc()
						f._fAttrKeysMap = Assoc.CreateAssoc()
						
						// *** set up the hidden cols and find/replace vals
						sourceSettings = IsDefined(sourceSettings) ? sourceSettings : Assoc.CreateAssoc()
						f._fHiddenCols = IsFeature(sourceSettings, "hiddenCols") ? sourceSettings.hiddenCols : {}
						f._fPushDownVals = IsFeature(sourceSettings, "pushDownVals") ? sourceSettings.pushDownVals : Assoc.CreateAssoc()
						f._fFindReplaceVals = IsFeature(sourceSettings, "findReplaceVals") ? sourceSettings.findReplaceVals : Assoc.CreateAssoc()	
						
						return f
						
					end

	
	public function void SetAction(String action)
	
			._fAction = action	
				
		end

	
	public function void SetApiProcess(Object apiProcess)
								
			._fApiProcess = apiProcess
					
		end

	
	public function void SetApiVals(Assoc apiVals)
								
			._fApiVals = apiVals
					
		end

	// Patched by Pat201911010
	public function Assoc ValidateLine(Object prgCtx, \
									String action, \
									Integer lineNo, \
									Assoc csvData, \
									Assoc dataFlags, \
									Assoc options, \
									String filePath, \
									String rootFilePath, \							
									Frame	apiDataObj = ._fApiVals.(lineNo))
												
			// *** log out for debug purposes
			Object logger = .GetLogger()
			logger.Debug(Str.Format("Validating Line %1: %2", lineNo, csvData))
	
			// KM 11/1/19 catch problem with Dynamic job without a $ACTION value in the CSV
			if Str.Upper(action) == "DYNAMIC" 
				if !IsFeature(csvData, "$ACTION")
					return .SetErrObj(.InitErrObj("ValidateLine"), "The CSV file is missing an $ACTION column. This is required for a Dynamic job.")
				elseif IsUndefined(csvData.("$ACTION"))
					return .SetErrObj(.InitErrObj("ValidateLine"), "Undefined value in $ACTION column.")
				end
			end	
	
			// put the file path information in the apiDataObj
			// KM 11-27-18 ONLY DO THIS IF FILEPATH IS DEFINED (from folder structure load). Otherwise it will null out the previously set value
			if IsDefined(filePath)
				apiDataObj.SetValue("filePath", filePath)
			end	
			apiDataObj.SetValue("rootFilePath", rootFilePath)
			
			// *** Call to the api process to validate the entire line of data
			Assoc status = ._fApiProcess.ValidateItem(prgCtx, action, apiDataObj, dataFlags, options)
			
			if !status.ok											
				return .SetErrObj(.InitErrObj("ValidateLine"), status)
			else
				Assoc rtn
				rtn.ok = true
				rtn.result = Assoc.CreateAssoc()
				rtn.result.apiDataObj = apiDataObj
				rtn.result.apiVals = apiDataObj.GetAssocVals()
				rtn.result.csvData = csvData
				rtn.result.actionFlags = status.result.actionFlags
				rtn.result.oldVals = status.result.oldVals
				rtn.result.oldCsvData = status.result.oldCsvData
				rtn.result.ugID = status.result.ugID
				rtn.result.nodeId = status.result.nodeId
				rtn.result.dataFlags = dataFlags
				rtn.result.filePath = IsFeature(status.result, "filePath") ? status.result.filePath : undefined 
				
				//logger.Debug(Str.Format("Returning success from ValidateLine: %1", rtn))			
						
				return rtn
			end	
		end

	
	public function Assoc ValidateSetVal(Integer rowNum, String col, Dynamic inVal)
																																												
		// *** is this a category attribute column? We need to know because we have special handling for how we store this value
		Boolean isCatAttr  = false
		Dynamic val	
		Assoc	status
		Assoc 	rtn
		rtn.ok = true	
			
		String action	= ._fAction
			
		// default is to cache the result of this validation, if possible
		Boolean cacheResult = true
		// *** trim out space in col
		col = Str.Trim(col)
			
		// *** strip out leading and trailing whitespace in value
		if Type(inVal) == StringType
			inVal = Str.Trim(inVal)
		end	
			
		// *** skip doing anything if the col is hidden
		if col[1] != "#" && (col in ._fHiddenCols == 0) && (col in ._fIgnoredColumns == 0)
		
			// *** get apiDataObj
			Frame apiDataObj = ._fApiVals.(rowNum)
			if IsUndefined(apiDataObj)
				apiDataObj = ._fApiVals.(rowNum) = $DataManager.ApiDataObj.New(action, undefined, undefined, ._fAttrKeysMap)
			end
		
			// *** make sure we set up this column
			if IsUndefined(._fCache.(col))
				._InitCaches({col})
			end	
		
			// *** special processing for clear
			Boolean useCache = true
		
			if (Type(inVal)==StringType && Str.LocateI(inVal, "<clear>"))
				useCache = false
				
			elseif Str.Upper(col) == "$MEDIATYPEFIELDS"
				useCache = false
		
			end			
		
			// *** did we already cache the values for this value on this column?	
			status = ._fCache.(col).(inVal)
				
			// *** KM 1-12-17 make sure we don't skip over setting <clear> for classifications
			if (useCache && IsDefined(status))
				isCatAttr = status.isCatAttr
				val = status.result
		
				// KM 4-22-19 Make sure we add category info because ADN relies on it
				if isCatAttr && status.ok
					apiDataObj.SetAddValueFromCol("$CATIDS", status.catId, false)
					apiDataObj.AddCatDef(status.catId, status.catVersionNum, status.catDef)
				end
				
			else
				
				// *** Process FindReplace and PushDown changes from ui
				if IsDefined(._fFindReplaceVals.(col)) || IsDefined(._fPushDownVals.(col))
					val = ._DoPushDownFindReplace(col, inVal) 
				else
					val = inVal
				end	
					
				// *** special parsing of the $TargetPath -- lack of a leading colon means we use the root target path
				if Str.CmpI(col, "$TargetPath") == 0 && Type(val) == StringType && val[1] != ":"
					val = ._fRootTargetPath + ":" + val
				end				
				
				// *** is this a column that we don't need to validate?
				if ._fSkipCols.(col)
					apiDataObj.SetValueFromCol(col, val, false)
					return rtn						
				end		
				
				// *** convert clear to undefined and set it (avoid validation) -- except for mediatypefields
				if Type(val) == StringType && (Str.Lower(val) in {"<clear>","[clear]"}) && (col in {"$MEDIATYPEFIELDS", "$CLASSIFICATION"} == 0)
					
					if (._fColTypeMap.(col) == 3) // means category attribute 
						// *** KM 11-27-15 if category, make sure we add to catIDS	
						status = ._fCatAttrValidator.ValidateVal(._fPrgCtx, col, undefined, action)
						if status.ok
							apiDataObj.SetAddValueFromCol("$CATIDS", status.result.catId, false)
							apiDataObj.SetValueFromCol(col, undefined, true)
								
							// KM 11-15-18 Add the category definition to the apiDataObj
							apiDataObj.AddCatDef(status.result.catId, status.result.catVersionNum, status.result.catDef)
								
							return .SuccessResult(undefined)							
						end				
						
					elseif col in {"$RDACCESSION","$RDSTORAGEMEDIUM","$RDSUBJECT","$RDADDRESSEE","$RDORIGINATOR","$RDORGANIZATION"} > 0
						
						// *** RMUpdateDetails expects an empty string in this case to clear out value
						apiDataObj.SetValueFromCol(col, "", isCatAttr)
						return rtn							
						
					elseif col in {"$RDRECEIVEDDATE","$RDUPDATECYCLEPERIOD","$RDNEXTREVIEWDATE","$RDLASTREVIEWDATE"} > 0 
						
						// *** RMUpdateDetails expects a zero in this case to clear out value
						apiDataObj.SetValueFromCol(col, 0, isCatAttr)
						return rtn									
						
					else
						// set undefined and hope it works to clear out the value
						apiDataObj.SetValueFromCol(col, undefined, isCatAttr)
						return rtn
							
					end	
					
				// *** is this a column that we don't need to validate?
				elseif ._fSkipCols.(col) 
		
					apiDataObj.SetValueFromCol(col, val, false)
					return rtn						
		
				end							
		
				switch ._fColTypeMap.(col)
		
					// *** regular $ columns
					case 1
						Object colValidator
							
						// *** KM 11/9/16 distinguish between users and groups and regular node actions
						if action in {"UPDATEUSERGROUP","CREATEUSERGROUP","DELETEUSERGROUP"}
							colValidator = ._fUGColValidatorMap.(col)
						else
							colValidator = ._fColValidatorMap.(col)
						end	
							
						if IsUndefined(colValidator)
							// *** TODO should we flag this as an error or let it go?
							status = .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("Cannot find validator for column named %1", col))
							return rtn
						else
							
							// *** now call the actual validator
							status = colValidator.ValidateVal(._fPrgCtx, val, action, apiDataObj)
			
							if status.ok 
								
								// *** special handling for the phys object pseudo columns BOXCLIENT, CLIENT, OBTAINEDBY, BORROWEDBY
								// *** these columns need to be resolved to ID or NAME columns --
								if IsFeature(status, "useColName")
									return ValidateSetVal(rowNum, status.useColName, val)
								end
								
								// *** mark this for skipping if there's was no need to validate
								// KM 4/2/18 only skip if there is an explicit flag!
								if IsFeature(status, 'skipNextTime') && status.skipNextTime
									._fSkipCols.(col) = true
								end						
								val = status.result	
							end							
						end	
				
					end	
							
					// *** column that tells us that a category is attached to the node.
					case 2
				
						// *** save this as an attached category for this line
						String catPath = col[1:Str.LocateI(col, "IsAttachedCat")-2]
							
						if val == '1' || val == 1  // 1 means it's attached
							status = ._fCatAttrValidator.GetCatId(._fPrgCtx, col, catPath)
							if status.ok
								apiDataObj.SetAddValueFromCol("$CATIDS", status.result, false)
								return .SuccessResult(undefined)					
							end	
						else
							// *** just break out
							return .SuccessResult(undefined)	
						end	
						
					end	
	
					// *** column that tells us that a category is to be removed from a node
					case 5
				
						// *** save this as an attached category for this line
						String catPath = col[1:Str.LocateI(col, "RemoveCat")-2]
							
						if val == '1' || val == 1  // 1 means it's to be removed
							status = ._fCatAttrValidator.GetCatId(._fPrgCtx, col, catPath)
							if status.ok
								apiDataObj.SetAddValueFromKeys("metadata", "catIdsToRemove", undefined, status.result)
								return .SuccessResult(undefined)
							end	
						else
							// *** just break out
							return .SuccessResult(undefined)	
						end	
						
					end						
									
							
					// *** category attribute values
					case 3
						
						isCatAttr = true
						
						// see if we can get a dataid for this object
						Integer nodeID = apiDataObj.GetValue("nodeID")	
	
						// *** means it's a category attribute
						status = ._fCatAttrValidator.ValidateVal(._fPrgCtx, col, val, action, nodeID)
						cacheResult = status.cacheResult
						if status.ok
							val = status.result.val
							apiDataObj.SetAddValueFromCol("$CATIDS", status.result.catId, false)
								
							// KM 11-15-18 Add the category definition to the apiDataObj
							apiDataObj.AddCatDef(status.result.catId, status.result.catVersionNum, status.result.catDef)
						end	
					end
							
						
					// *** system attributes
					case 4
						
						// *** means it's a system attribute
						status = ._fSysAttrValidator.ValidateVal(._fPrgCtx, col, val, action)
							
						if status.ok
							val = status.result	
						end
							
					end 
					
				end
					
				// *** make sure we have the col in the cache here
				if IsUndefined(._fCache.(col))
					._fCache.(col) = Assoc.CreateAssoc()
				end
					
				// *** cache the result (good or bad)
				Assoc cachedStatus
				cachedStatus.ok = status.ok
				cachedStatus.result = val
				cachedStatus.isCatAttr = isCatAttr
		
				if status.ok
						
					// KM 4-22-19 cache some info for category attribute
					if isCatAttr
						cachedStatus.catID = status.result.catId
						cachedStatus.catVersionNum = status.result.catVersionNum
						cachedStatus.catDef = status.result.catDef
					end						
						
				else
							
					cachedStatus.errMsg = status.errMsg
					cachedStatus.context 	= status.context
					cachedStatus.methodName	= status.methodName
	
				end	
	
				if cacheResult
					._fCache.(col).(inVal) = cachedStatus			
				end				
					
			end
				 
			// *** get results from the status return from colValidator
			if !status.ok
				return .SetErrObj(.InitErrObj("ValidateSetVal"), status)
						
			// *** store the value in the ApiDataObj (except for special case mediatypefields)		
			elseif col != "$MEDIATYPEFIELDS"
			
				// *** make sure we have a value (it came in defined so it should not get nulled out)
				// *** special case with create action -- we null out the $ObjectID if it comes in
				if IsUndefined(val) && !(col=="$OBJECTID" && action=="CREATE")
					return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("Lost value for column %1", col))	
				end
					
				// *** store the value in the api vals
				apiDataObj.SetValueFromCol(col, val, isCatAttr)
		
			end	
		end
			
		return rtn
									
	end

	
	private function Dynamic _DoPushDownFindReplace(String col, Dynamic val)
									
						Object formatPkg = $DataManager.FormatPkg									
					
						// *** Any find/replace set for this column?										
						Assoc a = ._fFindReplaceVals.(col)
						
						if IsDefined(a)									
							String	find = a.find
							String  replace = a.replace
					
							if Type(val) != StringType
								// *** special handling for dates
								String origStr = formatPkg.ConvertValToString(val)
									
								if IsDefined(origStr)
									String newStr = Str.ReplaceAll(origStr, find, replace)
									Dynamic newVal = formatPkg.ConvertStringToVal(newStr, Type(val), false, false)
									if IsDefined(newVal)
										val = newVal
									end
								end	
									
							else
								val = Str.ReplaceAll(val, find, replace)
							end
						end
						
						// *** push down vals
						String newVal = ._fPushDownVals.(col)
						
						if IsDefined(newVal)
							if Type(val) != StringType
								val = formatPkg.ConvertStringToVal(newVal, Type(val), false, false)
									
							else
								val = newVal
							end		
						end	
						
					
						return val
					
					end

	
	private function void _InitCaches(List cols)
								
									String col
								
									for col in cols
										._fCache.(col) = Assoc.CreateAssoc()
										._fSkipCols.(col) = false
										
										// *** regular $ columns
										if col[1] == "$"
											._fColTypeMap.(col) = 1
											
										// *** column that tells us that a category is attached to the node.
										elseif IsDefined(Str.LocateI(col, "IsAttachedCat"))
											._fColTypeMap.(col) = 2
	
										// *** column that tells us that a category is attached to the node.
										elseif IsDefined(Str.LocateI(col, "RemoveCat"))
											._fColTypeMap.(col) = 5									
								
										// *** category attribute values
										elseif IsDefined(Str.Locate(col, ":"))
											._fColTypeMap.(col) = 3		
											
										// *** system attributes
										else
											._fColTypeMap.(col) = 4		
										
										end		
										
									end	
								
								end

	
	private function void _RegisterAttrKey(	String col, \
											String attrKey)
				
				if !(IsFeature(._fAttrKeysMap, col))
					._fAttrKeysMap.(col) = {"metadata", "attrVals", attrKey}
				end
					
											
			end

	
	private function void _RegisterSysAttribute( String col )
		
			// Patched by Pat201901280
			
			echo("_RegisterSysAttribute")
		
			if !(IsFeature(._fAttrKeysMap, col))
				._fAttrKeysMap.(col) = {"metadata", "systemAtts", col}
			end
									
		end

	
	override function void _SubclassDestructor()
								
									._fPrgCtx = undefined
									._fApiVals = undefined
									._fCache = undefined
									._fCatAttrValidator = undefined
									._fSysAttrValidator = undefined	
									._fAttrKeysMap = undefined
								
								end

end

package DATAMANAGER::CORE

public object NodeDynamic inherits DATAMANAGER::CORE::NodeProcesses

	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeDynamic'
	override	String	fSubKey = 'Node.Dynamic'

end

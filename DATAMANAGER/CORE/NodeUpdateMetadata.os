package DATAMANAGER::CORE

public object NodeUpdateMetadata inherits DATAMANAGER::CORE::NodeUpdateProcesses

	override	String	fActionPerformed = 'Metadata Updated.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeUpdateMetadata'
	override	String	fSubKey = 'Node.UpdateMetadata'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
													Frame apiDataObj, \							
													Assoc options, \
													Assoc actionFlags, \
													Object llNode, \
													DAPINODE parentNode,\
													DAPINODE node)	
					Assoc		status
					String		auditStr = ""
					Assoc		metadataVals = apiDataObj.getValue("metadata")
					
					/* the action flags
						actionFlags.doRecManData = false
						actionFlags.doPhysObjData = false
						actionFlags.doClassification = false
						actionFlags.doAttrData = false	
						actionFlags.doDapiNodeData = false
						actionFlags.doOwnerData = false
						actionFlags.doNickName = false
						actionFlags.doContractFileData = false
					*/
					
					// *** update owner or group
					if actionFlags.doOwnerData
						status = ._UpdateOwnerData(prgCtx, node, llNode, metadataVals.ownerData)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += status.result.auditStr
						end			
					end
					
					// *** NickName
					if actionFlags.doNickName
						status = $NickName.NickNameUtils.UpdateNickname(prgCtx, node.pId, metadataVals.NickName)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += "Nickname updated."			
						end			
					end	
					
					// *** update attributes?
					if actionFlags.doAttrData
						List 	catIds =  metadataVals.catIDs
						List 	catIdsToRemove =  metadataVals.catIDsToRemove
						List	attrGroups
			
						// *** build the attr groups to pass to action functions (in some cases, we need to take the values from metadataVals and massage into a format for WebService API calls)
						status = $DataManager.AttrGroupPrep.PrepareCatAttrsForUpdate(prgCtx, node, catIDs, catIdsToRemove, metadataVals.attrVals)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							attrGroups = status.result
							
							if status.categoriesUpgraded
								auditStr = "Categories upgraded."
							end	
						end					
					
						// *** let the docman service handle updating attributes
						status = $DocManService.AttributeHandlerRegistry.UpdateAttributes( prgCtx, node, attrGroups )
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += "Category attributes updated."
						end
						
					end
					
					// *** set rec man data?
					if actionFlags.doRecManData
						status = ._UpdateRecManData(prgCtx, node, metadataVals)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += "Record management data updated."
						end
					end		
					
					// *** set rec man data?
					if actionFlags.doPhysObjData
						status = ._UpdatePhysObjData(prgCtx, node, metadataVals, actionFlags)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += status.result
						end
					end			
					
					// *** set a classification?  only if it's a regular one.  RM classification was already set above
					if actionFlags.doClassification
						status = ._ApplyClassifications(prgCtx, node, metadataVals.doClearClassifications, metadataVals.classIds)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += status.actionPerformed
						end
					end
					
					// *** Basic Node Data?
					if actionFlags.doDapiNodeData
						status = $DataManager.NodeAPI.ApplyNodeData(prgCtx, node, metadataVals.dapiNodeData)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)	
						else
							auditStr += "Basic node data updated."
						end			
					end	
					
					// *** System Attributes?
					if actionFlags.doSystemAttrs
						status = $DataManager.NodeAPI.ApplySystemAtts(prgCtx, node, metadataVals.systemAtts, true)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += "System Attributes Updated."
						end			
					end	
					
					// *** do we have Contract file data?
					if node.pSubtype == 31067 && actionFlags.doContractFileData
						status = ._UpdateContractFileData(prgCtx, node, metadataVals.contractFile)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						else
							auditStr += status.result.auditStr
						end	
					end
					
					Assoc result
					result.node = node
					result.actionPerformed = auditStr
					
					return .SuccessResult(result)
				 
				end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
															Assoc actionFlags, \
															Assoc options, \
															Object llNOde, \
															DAPINODE node, \
															Object apiDataObj)
																
					Assoc		metadata = apiDataObj.getValue("metadata")	
					Assoc		status								
													
					// *** call _UpdateRecManData but pass in flag telling function not to actually perform the change
					if actionFlags.doRecManData
						status = ._CheckRecManData(prgCtx, node, metadata)
						if !status.ok
							return .SetErrObj(.InitErrObj("PerformAction"), status)
						end
					end									
													
					
					Assoc rtn
					rtn.ok = true
					return rtn
				end

	
	private function Assoc _ApplyClassifications(Object prgCtx, \
																DAPINODE node, \
																Boolean doClear, \
																List classIds = {})									
					Assoc	status
					List 	classSubTypes = { 199, 196 } 
					String	actionPerformed
				
					// *** remove any existing classifications
					if doClear
						status = $CLASSIFICATION.ClassificationUtils.RemoveClassifications( prgCtx, node.pId, true, {}, classSubTypes)
						if !status.ok
							return .SetErrObj(.InitErrObj("_ApplyClassifications"), status)
						else
							actionPerformed = "Classification Removed."	
						end		
					end	
				
					// *** apply the classifications specified	
					if Length(classIds)	
						status = $CLASSIFICATION.ClassificationUtils.ApplyClassifications( prgCtx, node.pId, classIds, classSubTypes)
						if !status.ok
							return .SetErrObj(.InitErrObj("_ApplyClassifications"), status)
						else
							actionPerformed += "Classifications Applied."				
						end
					end
				
					Assoc	rtn	
					rtn.ok = true
					rtn.actionPerformed = actionPerformed
					return rtn								
				end

	
	private function Assoc _CheckRecManData(Object prgCtx, \
															DAPINODE node, \
															Assoc metadata)
														
							
				/*
					if IsDefined(AdditionalData.rimsStatus) && Length(AdditionalData.rimsStatus) > 0
							
						dbResults = CAPI.Exec(LLConnect,"SELECT COUNT(*) As nCount FROM FILE_STATUS WHERE FILE_STATUS=:A1", AdditionalData.rimsStatus)
						if IsError( dbResults ) || dbResults[1].nCount == 0
							ok = false
							errMsg = "Incorrect Status specified."
				
						end
					end
					if ok && IsDefined(AdditionalData.rimsEssential) && Length( AdditionalData.rimsEssential ) > 0
							
						dbResults = CAPI.Exec(LLConnect, "SELECT COUNT(*) As nCount FROM ESS_RECORDS  WHERE ESS_RECORDS =:A1",AdditionalData.rimsEssential)
						if IsError( dbResults ) || dbResults[1].nCount == 0
							ok = false
							errMsg = "Incorrect Essential specified."
						
						end
						if ok
							if (AdditionalData.rimsEssential in vitalRecordCodes )
								bVital = true
							end
						end	
					end
					
					if ok 
						if IsDefined(AdditionalData.rimsCyclePeriod) && AdditionalData.rimsCyclePeriod != 0
							if !(AdditionalData.rimsCyclePeriod in {1,2,3,12})
								ok = false
								errMsg = "Incorrect Cycle Period specified."
							end
						elseif bVital
							ok = false
							errMsg = "Please specify Cycle Period for vital record."
								
						end
					end
						
					if ok && IsDefined(AdditionalData.rimsRSI) && Length(AdditionalData.rimsRSI) > 0
						dbResults = CAPI.Exec(LLConnect, "SELECT Count(*) As nCount FROM RSI WHERE RSI=:A1",AdditionalData.rimsRSI)
						if IsError( dbResults ) || dbResults[1].nCount == 0
							ok = false
							errMsg = "Incorrect RSI specified."
								
						end 
					end 
						
					if ok && IsDefined(AdditionalData.rimsStorage) && Length(AdditionalData.rimsStorage) > 0
						dbResults = CAPI.Exec(LLConnect,"SELECT Count(*) As nCount FROM STORAGE WHERE STORAGE=:A1", AdditionalData.rimsStorage ) 
						if IsError( dbResults ) || dbResults[1].nCount == 0
							ok = false
							errMsg = "Incorrect Storage specified."
							
						end 
					end
					
					
					if ok && IsDefined(AdditionalData.rimsAccession) && Length(AdditionalData.rimsAccession) > 0
						dbResults = CAPI.Exec(LLConnect,"SELECT Count(*) As nCount FROM ACCESSION WHERE ACCESSION=:A1", AdditionalData.rimsAccession ) 
						if IsError( dbResults ) || dbResults[1].nCount == 0
							ok = false
							errMsg = "Incorrect Accession specified."
							
						end 
					end
				*/	
				
				
				
					Assoc rtn
					rtn.ok = true
					
					return rtn
				
				end

	
	private function Assoc _UpdateContractFileData(Object prgCtx, \
																DAPINODE node, \
																Assoc newCfData)
								
								Assoc 	rtn = .InitErrObj("_UpdateContractFileData")
								Assoc	status
								Assoc 	cfData
							
							    
							    //@param	{Assoc}		cfData		The duration cfData. Expects to have 
								//									<Date>effective
								//									<Date>currentend
								//									<Date>earlytermination
								//									<Boolean>terminated
								//									<Assoc>periods, this may not exist if there is no period has been defined. See schema definition for more detail													
								//									
							
								// *** first, get the existing duration info			
								status = $DataManager.ContractFileUtils.GetDuration(prgCtx, node)
								if !status.ok
									return .SetErrObj(rtn, status)
								else
									cfData = status.duration.data
									cfData = Assoc.Merge(cfData, newCfData)
								end
							
								// *** try to update the duration info
								status = $ContractManagement.DurationUtils.SetContractFileDuration(prgCtx, node, cfData )
								if !status.ok
									return .SetErrObj(rtn, status)
								end
								
								Assoc result
								result.auditStr = "Duration Info Updated"
							
								return .SuccessResult(result)
								
							end

	
	private function Assoc _UpdateOwnerData(Object prgCtx, \
															DAPINODE node, \
															Object llNode, \
															Assoc ownerData)
								
								Assoc 	rtn = .InitErrObj("_UpdateOwnerData")
								String 	auditStr
								Assoc	status
								
								if IsDefined(ownerData.pUserId) && ownerData.pUserId != node.pUserId
									// *** -3 signals clear
									if ownerData.pUserId == -3
										status = llnode.NodeRightRemove( node, DAPI.PERMTYPE_USER )
									else
										status = llnode.NodeRightUpdate( node, DAPI.PERMTYPE_USER, ownerData.pUserId, node.pUserPerm )			
									end	
								
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn
									else
										auditStr += "Owner changed."
									end
								end
							
								if IsDefined(ownerData.pGroupId) && ownerData.pGroupId != node.pGroupId
							
									// *** -3 signals clear
									if ownerData.pGroupId == -3
										status = llnode.NodeRightRemove( node, DAPI.PERMTYPE_GROUP )
									else
										status = llnode.NodeRightUpdate( node, DAPI.PERMTYPE_GROUP, ownerData.pGroupId, node.pGroupPerm )			
									end	
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn
									else
										auditStr += "Owner group changed."
									end
								end
							
								Assoc result
								result.auditStr = auditStr
							
								return .SuccessResult(result)
								
							end

	
	private function Assoc _UpdatePhysObjData(Object prgCtx, \
															DAPINODE node, \
															Assoc metadataVals, \
															Assoc actionFlags)
					Assoc	status
					Object 	llNode = $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType)
					String	auditStr = ''
					
					// *** Update Specific Tab Properties
					Assoc updateInfo
					updateInfo.request = metadataVals.poData.request
			
					// *** important to set objAction=info2. Otherwise, nothing happens in LLNode
					updateInfo.request.objAction = "info2"
				
					// **** call llNode Update	
					status = llNode.NodeUpdate(node, updateInfo)
					if !status.ok
						return .SetErrObj(.InitErrObj("_UpdatePhysObjData"), status)
					else
						auditStr += "Physical Object Properties Updated."
					end
				
					Assoc rtn
					rtn.ok = true	
					rtn.result = auditStr
				
					return rtn		
					
				end

	
	private function Assoc _UpdateRecManData(Object prgCtx, \
															DAPINODE node, \
															Assoc metadata)
															
					Object	recmanService
					Object	recmanServiceTemp
					Assoc	status
				
				
					// ** get rm service and make temp object
					recmanService = $Service.ServiceRegistry.getItem( "RecordsManagement" )
					if IsDefined( recmanService )
						recmanServiceTemp = OS.NewTemp( recmanService )
					end
				
					// *** set prgCtx
					recmanServiceTemp.SetProgramSession( prgCtx )
				
					// *** KM 10-22-15 classId actually optional
					Integer		classId
					if IsDefined(metadata.rmClassId)
						classId = metadata.rmClassId
		
						// *** KM 8-5-2019 need to set bNewClassification to true if classID is passsed in
		  				metadata.rmUpdateInfo.bNewClassification = TRUE
		
					else
						// *** KM 10-22-15 classId actually optional
						//return .SetErrObj(.InitErrObj("_UpdateRecManData"), "Classification is required for setting/updating records management data")
					end	
					
					// *** KM 12/7/18 set flag that allows us to clear out values
					metadata.rmUpdateInfo.bFromRESTAPI = TRUE
					
					// *** make the call to update
					status = recmanServiceTemp.RMUpdateDetails( node.pId, ClassID, metadata.rmUpdateInfo)
					// *** destroy temp object
					if IsDefined( recmanServiceTemp )
						OS.Delete( recmanServiceTemp )
					end	
					if !status.ok
						return .SetErrObj(.InitErrObj("_UpdateRecManData"), status)
					end
				
					Assoc rtn
					rtn.ok = true	
					return rtn
					
				end

end

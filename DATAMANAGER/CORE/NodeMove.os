package DATAMANAGER::CORE

public object NodeMove inherits DATAMANAGER::CORE::NodeProcesses

	override	String	fActionPerformed = 'Moved.'
	override	Boolean	fEnabled = TRUE
	override	String	fName = 'NodeMove'
	override	String	fSubKey = 'Node.Move'


	/* must implement this signature */
	override function Assoc PerformAction(Object prgCtx, \
												Frame apiDataObj, \								
												Assoc options, \
												Assoc actionFlags, \
												Object llNode, \
												DAPINODE newParentNode, \
												DAPINODE node)
						
				Assoc 	rtn = .InitErrObj("PerformAction")
				Assoc	status
				Assoc	collisionOptions = options.nameCollisions
				
				// important to get the name
				String		name = apiDataObj.getValue("name")
				
				// *** once last chance to get the parentNode if it was undefined (only needed for create and move)
				// *** TODO:  Need to add support for Compound Documents (.CD extension)
				if IsUndefined(newParentNode)		
					status = .GetCreateParentNode(prgCtx, options, apiDataObj, node, name, options.autoCreateFolders)
					if !status.ok
						.SetErrObj(rtn, status)
						rtn.errMsg = "Error Code 011 " + rtn.errMsg
						return rtn
					else
						newParentNode = status.result
					end	
				end	
					
				// *** inherit, source, or merged attributes?
				Assoc moveInfo
				moveInfo.AttrSourceType = IsDefined(options.attrSourceType) ? options.attrSourceType : 1
			
				// *** RM Class inherit?
			    moveInfo.RMClassInherit = IsDefined(options.rmClassInherit) && options.rmClassInherit
			
				// *** permissions inherit?
				moveInfo.ForcePermInherit = IsDefined(options.forcePermInherit) && options.forcePermInherit
				
				// *** pass in the makeName unique flag if it's in the options
				Boolean makeNameUnique = (llNode.fVersioned && collisionOptions.docs.makeUnique) || (!llNode.fVersioned && collisionOptions.containers.makeUnique)
				
				// *** get a unique name if we need to
				Boolean isUnique = $DataManager.NodeAPI.IsUniqueName(prgCtx, newParentNode, name)		
				if makeNameUnique && !isUnique
					name = $DataManager.NodeAPI.GetUniqueName(prgCtx, newParentNode, name)
				elseif !isUnique && !makeNameUnique
					// *** send back an error
					.SetErrObj(rtn, Str.Format("An item with the name '%1' already exists in the desired targetPath.", name))
					return rtn	
				end
			
				// *** call LLIAPI's NodeMove
				status = llNode.NodeMove( node, newParentNode, name, moveInfo)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				end		
				
				Assoc result
				result.node = node
				result.actionPerformed = .fActionPerformed
				
				return .SuccessResult(result)
				
			end

	
	override function Assoc SubclassValidateItem(Object prgCtx, \
														Assoc actionFlags, \																	
														Assoc options, \
														Object llNode, \
														DAPINODE node, \
														Object apiDataObj)
															
				Assoc		status
				DAPINODE	parentNode
				String		name	
				
				// *** now we need to do standard validation for new object
				status = $DataManager.NewObjectInterface.ValidateCopyMove(prgCtx, options, node, apiDataObj, llNode)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				else
					parentNode = status.parentNode
					name = status.name	
				end			
				
				if IsDefined(parentNode)
					// *** call LLIAPI's CheckMove
					status = llNode.CheckMove( node, parentNode, name )
					if !status.ok
						return .SetErrObj(.InitErrObj( "SubclassValidateItem" ), status)
					end
				end	
				
				return .SuccessResult()	
											
			end

end

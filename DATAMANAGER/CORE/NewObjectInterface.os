package DATAMANAGER::CORE

public object NewObjectInterface inherits DATAMANAGER::SynCoreMain

	
	public function Assoc ValidateCopyMove(Object prgCtx, Assoc options, DAPINODE node, Object apiDataObj, Object llNode)
					
							DAPINODE parentNode
					
							// values from CSV
							String	name = apiDataObj.GetValueFromCol("$OBJECTNAME")
							String	newName = apiDataObj.GetValueFromCol("$NEWNAME")
							String	targetPath = apiDataObj.GetValueFromCol("$TARGETPATH")
							Integer parentID = apiDataObj.GetValueFromCol("$PARENTID")
							Integer volumeID = apiDataObj.GetValueFromCol("$VOLUMEID") || DAPI.BY_DATAID
					
							// do we have a name from the CSV
							if IsUndefined(name) 
								if IsDefined(newName)
									name = newName
								else	
									name = node.pNAME
								end
							end	
							
							// make sure we set the name
							apiDataObj.setValue("name", name)
					
							// *** validate the parent
							Assoc status = ._ValidateParent(prgCtx, parentID, volumeID, targetPath, name, options, apiDataObj)
							if !status.ok
								return .SetErrObj(.InitErrObj("ValidateItem"), status)
							else
								parentNode = status.result	
							end
					
							// *** check for name collisions
							if IsDefined(parentNode)
								status = ._ValidateNameCollision(prgCtx, parentNode, name, options, llNode)
								if !status.ok
									return .SetErrObj(.InitErrObj("ValidateItem"), status)
								end
							end	
		
							Assoc rtn
							rtn.ok = true
							rtn.parentNode = parentNode
							rtn.name = name
							return rtn					
		
						end

	
	public function Assoc ValidateCreate(Object prgCtx, Integer subtype, Assoc options, Object apiDataObj, Object llNode)
					
			DAPINODE parentNode
		
			// values from CSV
			String	name
			String	targetPath = apiDataObj.GetValueFromCol("$TARGETPATH")
			Integer parentID = apiDataObj.GetValueFromCol("$PARENTID")
			Integer volumeID = apiDataObj.GetValueFromCol("$VOLUMEID") || DAPI.BY_DATAID
		
			// *** get name
			Assoc status = ._GetName(prgCtx, apiDataObj, targetPath, options)
			if !status.ok
				return .SetErrObj(.InitErrObj("ValidateItem"), status)
			else
				name = status.result
				apiDataObj.setValue("name", name)
			end	
		
			// *** validate the parent
			status = ._ValidateParent(prgCtx, parentID, volumeID, targetPath, name, options, apiDataObj)
			if !status.ok
				return .SetErrObj(.InitErrObj("ValidateItem"), status)
			else
				parentNode = status.result			
			end
			
			// *** check for name collisions
			if IsDefined(parentNode)
				status = ._ValidateNameCollision(prgCtx, parentNode, name, options, llNode)
				if !status.ok
					return .SetErrObj(.InitErrObj("ValidateItem"), status)
				end
			end			
		
			Assoc rtn
			rtn.ok = true
			return rtn
		end

	
	private function Assoc _CheckRootPath(Object prgCtx, String parentPath, Assoc options)
		
			String rootPath = ._GetRootPath(parentPath)
		
			// *** try to get this node so we can cache the ObjectID
			Assoc status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, rootPath)
			if !status.ok
				return .SetErrObj(.InitErrObj("_CheckRootPath"), Str.Format("Root path '%1' does not exist", rootPath))
			end
		
			Assoc rtn
			rtn.ok = true
			return rtn
			
		end

	
	private function Assoc _GetName(Object prgCtx, Object apiDataObj, String targetPath, Assoc options)
		
			String 	name
			String 	objectName =  apiDataObj.GetValueFromCol("$OBJECTNAME")
		
			// *** if the target path type is the object itself, then the name is the last element of the path
			if options.targetPathType == "object"
				if IsDefined(targetPath)
					String nameFromPath
					List elements = Str.Elements(targetPath, ":")
					if Length(elements)==0
						return .SetErrObj(.InitErrObj("_GetName"), Str.Format("Invalid format for target path %1. Path elements must be separated by colons.", targetPath))
					else
						nameFromPath = elements[-1]			
						
						if Str.Trim(nameFromPath) == ""
							return .SetErrObj(.InitErrObj("_GetName"), Str.Format("Invalid target path: trailing colon. Name cannot be empty."))
						end
					
					end
					
					// what if we have an objectname and the objectname from path is different? Might mean trouble.
					if IsDefined(objectName) && nameFromPath != objectName
						return .SetErrObj(.InitErrObj("_GetName"), Str.Format("The $OBJECTNAME in the CSV does not match the last element in the $TARGETPATH. Maybe the target path indicates the parent object? Remove $OBJECTNAME to use the setting '$TargetPath refers to the object's location.'", targetPath))
					else
						name = nameFromPath
					end
						
				else
					return .SetErrObj(.InitErrObj("_GetName"), "Could not get object name. Missing $TARGETPATH column.")
				end	
			
			elseif IsDefined(objectName)
				// targetPathType is parent. Is the name defined? If so, use it. 	
				
				name = objectName
		
			// *** KM 10-28-16 Make allowance for ADN. look for name if it came in undefined
			// *** KM 1-7-19 check if it's a supported node type first
			elseif IsDefined($ADN)
		
				// *** KM 1-7-19 check if it's a supported node type first
				Integer	subtype = apiDataObj.GetValueFromCol("$OBJECTTYPE")
		
				if subtype in $ADN.Utils.SupportedObjectTypes()
					Assoc status = ._NewUniqueID(prgCtx)
					if !status.ok
						return .SetErrObj(.InitErrObj("_GetName"), status)
					else
						name = Str.String(status.result)	
					end
				else
					return .SetErrObj(.InitErrObj("_GetName"), "Could not get object name. Missing $OBJECTNAME column.")
				end
		
			else
				return .SetErrObj(.InitErrObj("_GetName"), "Could not get object name. Missing $OBJECTNAME column.")
			end	
		
			// *** trim leading and trailing separators
			name = Str.ReplaceAll(name, File.Separator(), "")
		
			Assoc 	rtn
			rtn.result = name
			rtn.ok = true
		
			return rtn
		
		end

	
	private function Assoc _GetParentFromPath(Object prgCtx, String parentPath, Assoc options)
						
				DAPINODE	parentNode
				Assoc		status
				Assoc		rtn
		
				// *** try to get this node so we can cache the ObjectID
				status = $DataManager.NodeCache.GetNodeFromPath(prgCtx, parentPath)
				if !status.ok
					return .SetErrObj(.InitErrObj("_GetParentFromPath"), status)
				else
					parentNode = status.result					
				end	
		
				// *** check and make sure we don't need to get a volume (i.e. for projects)
				Object llNode = $LLIAPI.llNodeSubsystem.GetItem(parentNode.pSubtype)
				if llNode.fAssociatedVolume != 0
					// *** get the actual node
					DAPINODE volNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, -(parentNode.pId))
					if IsNotError(volNode)
						parentNode = volNode			
					elseif !llNode.fAssociatedVolumeOptional
						return .SetErrObj(.InitErrObj("GetParentNode"), Str.Format("Could not get volume node for node %1 [dataid %2]", parentNode.pId, parentNode.pName))				
					end
				end		
				
				rtn.ok = true
				rtn.result= parentNode
				return rtn
				
			end

	
	private function String _GetParentPath(	Assoc options, String targetPath, String objectName)
					
				// removing leading colon
				String parentPath = ._FormatNodePath(targetPath)	
					
				// *** parse the parent path out of the full path to object
				if options.targetPathType == "object"
		
					List 	pathParts = Str.Elements(parentPath, ":")
					
					// *** if the last path element == the object name, then assume 
					// that the parent is everything but the last element
					// *** otherwise, the parent path is the path specified
					if Length(pathParts) && objectName == pathParts[-1]
						Integer lastSepLoc = Str.RChr(parentPath, ":")
						
						if IsDefined(lastSeploc)
							parentPath = parentPath[1:lastSepLoc-1]
						end					
					end
					
				end
					
				return parentPath
			
			end

	
	private function String _GetRootPath(String parentPath)
		
			// *** start from a uniform format  - format the path to remove leading and trailing colons
			parentPath = ._FormatNodePath(parentPath)
							
			// the rootpath should be the first n elements of the path
			List 		pathParts = Str.Elements(parentPath, ":")
		
			// the rootpath should be the first 2 elements of the path
			String rootPath
			Integer rootLength = $DataManager.ModulePrefs.GetPref("RootElementLevels", 4)	
		
			if Length(pathParts) >= rootLength 
				rootPath = Str.Catenate(pathParts[1:rootLength], ":")[1:-2]	
				
			else
				rootPath = parentPath
			
			end	
			
			return rootPath
			
		end

	
	private function Assoc _ValidateNameCollision(Object prgCtx, DAPINODE parentNode, String name, Assoc options, Object llNode)
						
				Assoc	collisionOptions = options.nameCollisions
				String	errMsg = Str.Format("An item with the name '%1' already exists in the desired targetPath. Check name collision and $TARGETPATH settings.", name)
				Boolean makeNameUnique = (llNode.fVersioned && collisionOptions.docs.makeUnique) || (!llNode.fVersioned && collisionOptions.containers.makeUnique)
		
				if !$DataManager.NodeAPI.IsUniqueName(prgCtx, parentNode, name) && !makeNameUnique
					if llNode.fVersioned && !(collisionOptions.docs.makeUnique || collisionOptions.docs.addVersion || collisionOptions.docs.addVersionIfNewer || collisionOptions.docs.updateMetadata || collisionOptions.docs.rename)
						// *** send back an error
						return .SetErrObj(.InitErrObj( "ValidateNameCollision" ), errMsg)
					elseif !llNode.fVersioned && !(collisionOptions.containers.updateMetadata || collisionOptions.containers.makeUnique)
					
						// *** send back an error
						return .SetErrObj(.InitErrObj("ValidateNameCollision"), errMsg)
					end							
				end
				
				Assoc rtn
				rtn.ok = true
				return rtn
			end

	
	private function Assoc _ValidateParent(Object prgCtx, Integer parentID, Integer volumeID, String targetPath, String objectName, Assoc options, Object apiDataObj)
						
		DAPINODE parentNode
		Assoc	status
	
		// we either have a path or parentID
		if IsDefined(parentID)
			parentNode = DAPI.GetNodeById(prgCtx.DapiSess(), volumeId, parentId)
			if IsError(parentNode)
				return .SetErrObj(.InitErrObj("_ValidateParent"), Str.Format("Could not get parentNode from parentId %1", parentId))
			else
				parentNode = status.result
			end
	
		elseif IsDefined(targetPath)
	
			// *** check the format of target path	
			if Length(Str.Elements(targetPath, ":"))==0
				return .SetErrObj(.InitErrObj("_GetParentFromPath"), Str.Format("Invalid format for target path %1. Path elements must be separated by colons.", targetPath))
			end
			
			// extra check here for misunderstanding of $TargetPath + $ObjectName combination
			if options.targetPathType == "object"
			
				// guard against repeated loads to the same object -- probably a $TargetPathe error
				String key = ._HashKey(targetPath)
		
				// KM 4-21-2020 this validation is best avoided for Ovintiv
				// loads in which many repeated versions will be loaded to the same document				
				//options.targetPathCache = IsDefined(options.targetPathCache) ? options.targetPathCache : Assoc.CreateAssoc()
				//options.targetPathCache.(key) = IsFeature(options.targetPathCache, key) ? options.targetPathCache.(key) + 1 : 1 
			
				// KM 2-25-2020 also set a hash of the objectpath that we can use to determine chunking
				apiDataObj.setValue("hashedObjectPath", key)
		
				// KM 4-21-2020 this validation is best avoided for Ovintiv
				// loads in which many repeated versions will be loaded to the same document	
				//if options.targetPathCache.(key) >= 3
					//Assoc rtn = .SetErrObj(.InitErrObj("_ValidateParent"), Str.Format("$TargetPath is set to indicate the object (not parent), but the CSV $TargetPath points repeatedly to the same object. Change either the '$TargetPath Meaning' setting or the $TargetPath in the CSV.", targetPath))
					//rtn.fatal = true
					//return rtn	
				//end
			end			
			
			// *** parse out the parent path
			String parentPath = ._GetParentPath(options, targetPath, objectName)
			
			// set the parentPath into the apiDataObj (Important!)
			apiDataObj.setValue("parentPath", parentPath)
	
			if options.targetPathType != "object"
	
				String objectPath = parentPath + ":" + objectName	
	
				String key = ._HashKey(objectPath)
				
				// KM 2-25-2020 also set a hash of the objectpath that we can use to determine chunking
				apiDataObj.setValue("hashedObjectPath", key)
	
			end		
	
			// 4-10-19 check the rootPath if flag is passed in
			if options.checkRootPath
				status = ._CheckRootPath(prgCtx, parentPath, options)
				if !status.ok
					return .SetErrObj(.InitErrObj("_ValidateParent"), status)
				end						
			end			
	
			// now try to get the node			
			status = ._GetParentFromPath(prgCtx, parentPath, options)
			if status.ok
				parentNode = status.result
	
				// *** is the object a container?
				Object parentLLNode = $LLIAPI.LLNodeSubsystem.GetItem(parentNode.pSubtype)
				if !parentLLNode.IsContainer()
					return .SetErrObj(.InitErrObj("_ValidateParent"), "Specified target is not a container. Check $TargetPath setting to make sure loader understands your target correctly.")															
				end	
				
			elseif ._IsRegexExpression(targetPath)
	
				// *** allow this -- the parent will hopefully get created before this item
				// *** TODO -- ideally we should evaluate the regex to check if it's at least partially valid
				// *** better yet -- evaluate it to see if there are other items higher up in the CSV that would make the path valid
	
			elseif !options.autoCreateFolders
				
				return .SetErrObj(.InitErrObj("_ValidateParent"), "Could not find parent from $TARGETPATH. Consider enabling the auto create folders option.")
			
			end				
	
		else
			return .SetErrObj(.InitErrObj("_ValidateParent"), 'You need either a $TargetPath column or $ParentId column in your CSV for this action.')
		end	
	
		Assoc rtn
		rtn.ok = true
		return rtn
	
	end

end

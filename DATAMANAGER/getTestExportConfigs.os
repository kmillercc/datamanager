package DATAMANAGER

public object getTestExportConfigs inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
						     **/
						
							Object tester = $DataManager.Tester
							
							// *** generate the change and clear test files
							tester.GenerateChangeClearCsvFiles()
							
							List	config1 = tester.GetTestCsvExports(prgCtx)
							List	config2 = tester.GetTestFileExports(prgCtx)
							
							List 	allConfigs = {@config1, @config2}
							
							.fContent = synCore.SuccessResult(allConfigs)
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
								}
						end

end

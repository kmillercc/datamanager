package DATAMANAGER

public object Utils inherits DATAMANAGER::SynCoreMain

	
	public function Boolean IsPending(String path)
				
				String s	
				
				if path[-1] == File.Separator()
					path = path[1:-2]
				end
			
				List 	pendingSuffixes = { "PENDING" }
				 	
				for s in pendingSuffixes
					Integer lengthSuffix = Length(s)
					
					if Str.Upper(path[-lengthSuffix:]) == s
						return true
					end	 
				end
				
				return false
			end

	
	public function Boolean RemovePending(String path)
				
				String s	
				
				if path[-1] == File.Separator()
					path = path[1:-2]
				end
				
				Integer pathLength = Length(path)
				List 	pendingSuffixes = {"._PENDING", ".PENDING", "_PENDING"}
				
				for s in pendingSuffixes
					Integer lengthSuffix = Length(s)
					Integer endPoint = pathLength - lengthSuffix
					
					if Str.Upper(path[-lengthSuffix:]) == s
						String newPath = path[1:endPoint]
						
						if File.Rename(path, newPath)
							return true
						end
					end	 
				end
				
				return false
			end

end

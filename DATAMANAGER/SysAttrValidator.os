package DATAMANAGER

public object SysAttrValidator inherits DATAMANAGER::LoadObjects

	public		Dynamic	_fCache
	public		String	fApiKey1 = 'systemAtts'
	override	Boolean	fEnabled = TRUE


	
	public function Object New()
									
										Frame f = ._NewFrame()
									
										f._fCache = Assoc.CreateAssoc()
									
										return f
										
									end

	
	public function Assoc ValidateAttrName(Object prgCtx, \
																			String col)
												
												// *** try to get what is presumed to be a system attribute
												Dynamic attr = $DataManager.SystemAttributes.GetSystemAttrRec(prgCtx, col)
																
												if IsUndefined(attr) 
													return .SetErrObj(.InitErrObj( "ValidateAttrName" ), Str.Format("Could not find attribute record for system attribute '%1.'", col))
												else
													._fCache.(col) = attr
												end
											
												Assoc rtn
												rtn.ok = true	
												return rtn
																	
											end

	
	public function Assoc ValidateVal(Object prgCtx, \
																		String col, \
																		Dynamic val, \
																		String action)
																	
												Dynamic attr = ._fCache.(col)
												Dynamic outVal
																
												if IsUndefined(attr) 
													return .SetErrObj(.InitErrObj( "ValidateVal" ), Str.Format("Could not find attribute record for system attribute '%1.'", col))
											
												else
												
													if IsDefined(val)
												
														Integer maxLen = attr.ATTRLEN
												
														/*
														 o  ATTR_DATE
														Specifies an attribute of type date.
														 o  ATTR_FCHAR
														Specifies an attribute of type fixed length text.
														 o  ATTR_INT
														Specifies an attribute of type Integer.
														 o  ATTR_LCHAR
														Specifies an attribute of type long length text.
														 o  ATTR_REAL
														Specifies an attribute of type Real.
														 o  ATTR_VCHAR
														Specifies an attribute of type variable length text
														*/
													
														switch attr.ATTRTYPE
												
															case DAPI.ATTR_DATE
																if Type(val) == StringType 
																	outVal = $DataManager.FormatPkg.ConvertStringToVal(val, DateType)
																elseif Type(val) != DateType
																	return .SetErrObj(.InitErrObj( "ValidateSetVal" ), Str.Format("%1 is an invalid value for system attribute '%2.' Expecting date type.", val, attr.ATTRNAME))
																end	
															end
															
															case DAPI.ATTR_INT
																if Type(val) == StringType 
																	outVal = $DataManager.FormatPkg.ConvertStringToVal(val, IntegerType)
																elseif Type(val) != IntegerType
																	return .SetErrObj(.InitErrObj( "ValidateSetVal" ), Str.Format("%1 is an invalid value for system attribute '%2.' Expecting integer type.", val, attr.ATTRNAME))
																end				
															end
															
															case DAPI.ATTR_REAL
																if Type(val) == StringType 
																	outVal = $DataManager.FormatPkg.ConvertStringToVal(val, RealType)
																elseif Type(val) != RealType
																	return .SetErrObj(.InitErrObj( "ValidateSetVal" ), Str.Format("%1 is an invalid value for system attribute '%2.' Expecting real type.", val, attr.ATTRNAME))
																end				
															end
												
															case DAPI.ATTR_FCHAR, DAPI.ATTR_LCHAR, DAPI.ATTR_VCHAR
																outVal = Str.String(val)
																if IsDefined(maxLen) && Length(val) > maxLen
																	return .SetErrObj(.InitErrObj( "ValidateSetVal" ), Str.Format("%1 for system attribute '%2' is too long. Maximum length is %3.", val, attr.ATTRNAME, maxLen))
																end
															end
												
														end
														
														if IsUndefined(outVal)
															return .SetErrObj(.InitErrObj( "ValidateSetVal" ), Str.Format("Cannot convert %1 to the appropriate data type for system attribute '%2'", val, attr.ATTRNAME))
														end
													
													else
													
														// TODO: check to see if it's required	
														outVal = val	
													
													end	
											
												end			
												
												Assoc rtn
												rtn.result = outVal
												rtn.ok = true
												return rtn
																	
											end

end

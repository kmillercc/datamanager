package DATAMANAGER

public object LicenseObj inherits DATAMANAGER::SynCoreMain

	public		Boolean	_fAllowBulkExporter = FALSE
	public		Boolean	_fAllowBulkLoader = FALSE
	public		Date	_fDefaultExpireDate = 3000-12-31T00:00:00
	public		Integer	_fEvalLimit
	public		Date	_fExpireDate = 3000-12-31T00:00:00
	public		Boolean	_fHasLicense = FALSE
	public		String	_fLicenseKey
	public		String	_fLicenseMode
	public		Boolean	fEnabled = TRUE
	public		String	fLICENSE_MODE_ENTERPRISE = 'Enterprise'
	public		String	fLICENSE_MODE_EVAL = 'Evaluation'
	public		String	fLICENSE_MODE_INVALID = 'Invalid'
	public		String	fLICENSE_MODE_SINGLE = 'Single Content Server DB'
	public		String	fLicenseKiniKey = 'LicenseKey'
	public		Boolean	fLicenseLoaded = FALSE


	script AllowBulkExporter
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																									
																								
																										
																											function Boolean AllowBulkLoader()
																											
																												return ._fAllowBulkExporter
																												
																											end
																										
																									
																								
																										
																									
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function Boolean AllowBulkLoader()
												
													return ._fAllowBulkLoader
													
												end

	
	public function Integer GetEvalLimit()
												
													return ._fEvalLimit
												
												end

	
	public function String GetEvalStatus()
												
													return Str.Format("Expires on %1.  Exports and Loads up to %1 objects allowed.", ._fExpireDate, ._fEvalLimit)
												
												end

	
	public function Dynamic GetExpireDate()
												
													if ._fExpireDate == ._fDefaultExpireDate
														return ''
													else
														return ._fExpireDate	
													end	
												
												end

	
	public function void GetLicenseInfo(Object prgCtx = .GetAdminCtx().prgCtx)
												
													if !.fLicenseLoaded
													
														// *** default an expire date way in the future
														._fExpireDate = Date.StringToDate('12/31/3000','%m/%d/%Y')
													
														Object modPrefs = $DataManager.ModulePrefs
														
														String key = modPrefs.GetPref("key", undefined, prgCtx)
														
														if IsDefined(key)
														
															// *** this is the default license key that allows for loads/exports up to 250 items and expires 15 days from first use
															String licenseKey = modPrefs.GetPrefString(.fLicenseKiniKey,\
																				'gb0VMk2Ai1np97m7qyeAXedrfRcNJDIcaAUSboUhKxyizziKiVb6IxmEYT/Y4d/RxUi82exaj6khZJh1HizDTknQqavUro6gUclghOf1i71/tCmVljqqy3gy/SH3NRe84QKICzK6aE1fhUuIsZ2lmK80L5N6rrSgB1Hddq4ViaM=',\
																				 prgCtx,\
																				 true)
																				 
																				 
															String decodedStr = $Kernel.Cipher.Decrypt(licenseKey, Str.Decode(key))
															Assoc	a = Str.StringToValue(decodedStr)
															
															// *** a date value of integer indicates that this is a new eval license that we sent the customer (used to extend eval period)
															if a.fLicenseMode == .fLICENSE_MODE_EVAL && Type(a.fExpireDate) == IntegerType
																a.fExpireDate = Date.Now() + (86400 * a.fExpireDate)
																String s = Str.ValueToString(a)
																licenseKey = $Kernel.Cipher.Encrypt(s, Str.Decode(key))
																modPrefs.PutPrefString(prgCtx, .fLicenseKiniKey, licenseKey)
																this._fHasLicense = false
																
															elseif a.fLicenseMode == .fLICENSE_MODE_SINGLE || a.fLicenseMode == .fLICENSE_MODE_ENTERPRISE
																// TODO - need to come up with a way to enforce these types of licenses
																a.fHasLicense = true
																
																if IsFeature(a, 'fExpireDate')
																	if Type(a.fExpireDate) == IntegerType
																		a.fExpireDate = Date.Now() + (86400 * a.fExpireDate)
																		._fExpireDate = a.fExpireDate
																		String s = Str.ValueToString(a)
																		licenseKey = $Kernel.Cipher.Encrypt(s, Str.Decode(key))
																		modPrefs.PutPrefString(prgCtx, .fLicenseKiniKey, licenseKey)					
																	
																	elseif Type(a.fExpireDate) == DateType
																		._fExpireDate = a.fExpireDate
																	end
																end	
															end	
															
															for key in Assoc.Keys(a)
																if IsFeature(this, "_" + key)
																	this.("_" + key) = a.(key)
																end	
															end		
															
															this._fLicenseKey = licenseKey	
														
														else
															
															._fLicenseMode = .fLICENSE_MODE_INVALID
														end
														
														.fLicenseLoaded = true
													
													end
													
												end

	
	public function String GetLicenseKey()
													return ._fLicenseKey
												end

	
	public function String GetLicenseMode()
													return ._fLicenseMode
												end

	
	public function Boolean HasLicense()
													
													return ._fLicenseMode == .fLICENSE_MODE_ENTERPRISE || ._fLicenseMode == .fLICENSE_MODE_SINGLE
													
												end

	
	public function Boolean IsEval()
													return ._fLicenseMode == .fLICENSE_MODE_EVAL
												end

	
	public function Boolean IsEvalExpired()
												
													return Date.Now() > ._fExpireDate
												
												end

	
	public function Boolean IsExpired()
												
													return Date.Now() > ._fExpireDate
												
												end

	override script Scratch
	
			
					
							
						
								function Scratch()
									DecodeLicense()
								end
								
								
								function void EncodeLicenseEval()
								
									String licenseKey = "A<1,?,'fAllowBulkExporter'=true,'fAllowBulkLoader'=true,'fLicenseMode'='" + .fLICENSE_MODE_SINGLE + "'>"
								
									String encodedStr = $Kernel.Cipher.Encrypt(licenseKey, "")
								
									echo("eval License=" + encodedStr)
								
								end
										
								function void DecodeLicense()
							
									String licenceKey = "gb0VMk2Ai1np97m7qyeAXedrfRcNJDIcaAUSboUhKxyizziKiVb6IxmEYT/Y4d/RxUi82exaj6khZJh1HizDTknQqavUro6gUclghOf1i71/tCmVljqqy3gy/SH3NRe84QKICzK6aE1fhUuIsZ2lmK80L5N6rrSgB1Hddq4ViaM="
									
									String key = $DataManager.ModulePrefs.GetPref("key", "")
									
									String decodedStr = $Kernel.Cipher.Decrypt(licenceKey, Str.Decode(key))
							
									
									echo("License=" + decodedStr)
									
								end	
						
					
				
			
		
	
	endscript

	
	public function void __Init()
													
					if OS.IsTemp($DataManager.License)
						echo("Ospace Unlocked!")	
						
						.SendMessage({"kmiller@syntergy.com"}, \
									{"kmiller@syntergy.com"}, \
									"Syntergy Data Manager Unlocked",\
									"The Syntergy Data Manager Code has been unlocked")	
				
					else	
				
						// create a license object for us to set the license info
						$DataManager.License = $Datamanager.License = OS.NewTemp(this)		
						
					end
				
				end

end

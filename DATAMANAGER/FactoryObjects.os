package DATAMANAGER

public object FactoryObjects inherits DATAMANAGER::InstanceObjects

	public		Dynamic	fKey
	public		Dynamic	fSubKey


	/*
		* Unique identifier
		* As a good practice, in the key string, include the name of the ospace as a component of the key  
		* The idea being that each ospace developer will keep track of their own key names within the ospace
		* the inclusion of the ospace name will prevent collisions across ospaces
		*
		* @return {String}
		*/
	public function String Key()
											
			String key = Undefined
			
			
			if ( IsDefined( .fKey ) && Length( .fKey ) > 0 )
			
				key = .fKey
				
			elseif ( IsDefined( .OSName ) && Length( .OSName ) > 0 )
				
				key = Str.Collapse(.OSName)
			
			elseif ( .fEnabled ) 
				
				echo( Str.Format( ".fKey is Undefined for %1", this ) )
				
			end
			
			return key
			
		end

	
	public function Dynamic SubKey()
											
			if IsDefined(.fSubKey)
				return .fSubKey
			else
				return undefined
			end	
		
		end

	
	override function Frame _NewFrame(Dynamic subkey = undefined)
											
			Object o
		
			if IsDefined(subkey)
				Object factory = $(._fOspaceName).ObjectFactory
				o = factory.GetObject(this.Key(), subkey)
			else
				o = this
			end
		
			Frame	 f
		
			f = Frame.New( {o} )
			
			return f
		end

	
	override function Void __Init()
											
			if .fEnabled
				Object ospaceGlobal = $(._fOspaceName)
			
				if IsDefined(ospaceGlobal)
					Object factory = ospaceGlobal.ObjectFactory
					factory.RegisterItem(this, this.Key(), this.SubKey())
				else
					echo("Could not get " + ._fOspaceName + " globals.")	
				end
			end
		
		end

end

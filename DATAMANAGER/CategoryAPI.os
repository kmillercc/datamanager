package DATAMANAGER

public object CategoryAPI inherits DATAMANAGER::Utils

	
	public function Assoc AddOrSetCategoryToNode(	Object prgCtx, \
																					DAPINODE node, \
																					String catName, \
																					Assoc vals, \
																					Boolean nonInherit = false)
												
												Assoc	rtn = .InitErrObj("_AddOrSetCategoryToNode")	
															
												Frame 	attrData 
												Assoc	status
												Integer	catID
												String	attName
												
												// *** get the categoryID
												status = ._GetCategoryId(prgCtx, catName)
												
												if status.ok
													catId = status.ID
												else
													.SetErrObj(rtn, status)
													return rtn
												end	
												
												// *** get the category frame for 
												attrData = $LLIAPI.AttrData.New( prgCtx, node.pID, 0 )
											
												// *** get the attributes
											   	status = attrData.DBGet()
											 	if !status.ok
											 		.SetErrObj(rtn, status)
											 		return rtn
											 	end   	
											    
											  	// **** If we need to, add the category to the folder
											  	if !._IsCategoryAlreadyOnNode(attrData, catName)
												 	status = attrData.AttrGroupAdd( catId )
												 	
												 	if !status.ok
												 		.SetErrObj(rtn, status)
												 		return rtn
												 	end
												 	
												end	
												
												// *** Do we need to flag this category as non-inherit?
												if nonInherit
													._SetCategoryNonInherit(attrData, catName)
												end
											 	
											 	// *** Now loop through the vals and save them
											 	for attName in Assoc.Keys(vals)
											 		status = ._SetAttributeValue(attrData, catName, attName, vals.(attName))
											 		
											 		if !status.ok
											 			.SetErrObj(rtn, status)
											 			return rtn
											 		end
											 		
											 	end	
											 	
											  	// **** Update database
												status = .SaveAttributeData(prgCtx, node, attrData)
													
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn	
												end		
												
												rtn.attrData = attrData			
															
												return rtn								
											end

	
	public function Assoc CopyAttributes(	Object prgCtx, \
																			DAPINODE srcNode, \
																			DAPINODE node)
												
												Assoc	rtn = .InitErrObj("CopyAttributes")
												Assoc	status
												Dynamic	attrData
														
												// *** Retrieve the attribute data.
												status = $LLIApi.AttrUtil.GetAttrDataWithSourceOption( \
																							prgCtx, \
																							0, \
																							srcNode.pId, \
																							srcNode.pVersionNum, \
																							TRUE, \
																							$LLIApi.AttrSourceOriginal, \
																							0, \
																							0, \
																							FALSE, \
																							TRUE )
											
												if status.OK
													attrData = status.AttrData
												else
													.SetErrObj(rtn, status)
													return rtn
												end
																
												// *** Now save these atts
												attrData.fID = node.pID
												attrData.fVerNum = node.pVersionNum
												status = attrData.DbPut( false )
												if !status.OK
													.SetErrObj(rtn, status)
													return rtn
												end	
											
												return rtn
											
											end

	
	public function List GetAvailableCatNames(Object prgCtx, \
																				Boolean toUpper = false)
											
												
												List		names
												RecArray	catRecs
												Record		rec
												
												// *** get all the categories in the cat volume
												String stmt = 'select DATAID,NAME from dtree where subtype=:A1 and ownerid=:A2 order by name'
												catRecs = .ExecSql(prgCtx, stmt, {$TypeCategory, -2004})
												
												if IsNotError(catRecs) 
													for rec in catRecs
														if toUpper
															names = {@names, Str.Upper(rec.NAME)}
														else
															names = {@names, rec.NAME}
														end	
													end
												else
													// *** for debugging
													.SetErrObj(.InitErrObj("GetAvailableCatNames"), "Could not get categories from category volume: " + Str.String(catRecs))
												end
												
												return names
											
											end

	
	public function Assoc GetAvailableCategories(Object prgCtx)
											
												Assoc		rtn = .InitErrObj("GetAvailableCategories")
												List		options
												RecArray	catRecs
												Record		rec
												DAPINODE	volNode
												
												// *** get the cat volume
												Assoc 	status = prgCtx.DSession().GetNodeCategories()
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												else
													volNode = status.CategoriesVol
												end	
																
												// *** get all the categories in the cat volume
												String stmt = "select DataID,Name from DTree where SubType=:A1 and OwnerID=:A2 order by Name"	
												catRecs = .ExecSql(prgCtx, stmt, {$TypeCategory, volNode.pVolumeID})
												
												if IsNotError(catRecs) 
													for rec in catRecs
														Assoc a = Assoc.CreateAssoc()
														a.id = rec.DATAID
														a.name = rec.NAME
														options = {@options, Assoc.Copy(a)}
													end
												else
													.SetErrObj(rtn, "Could not get categories from category volume: " + Str.String(catRecs))
													return rtn	
												end
												
												rtn.result = options
												
												return rtn
											
											end

	
	public function Assoc GetCategoryDefinition( 	Object prgCtx, \
																					Integer objID, \
																					Integer verNum = 0 )
											
												Assoc			rtn = .InitErrObj("_GetCategoryDefinition")
												Assoc			status
												DAPISESSION 	dSession = prgCtx.DSession().fSession
												DAPINODE		node
												DAPIVERSION		version
												Dynamic			result
												
												// *** Try to get the dapinode
												node = DAPI.GetNodeByID( dSession, DAPI.BY_DATAID, objID )
												if IsNotError( node )
											
													// *** Get the latest version for the current attribute definition
													version = DAPI.GetVersion( node, verNum )
													if IsNotError( version )
											
														// *** get the definition version
														status = $LLIApi.LLNodeSubsystem.GetItem( node.pSubType ).NodeFetchVersion( node, version )
														if status.ok
															result = status.Content
														else
															.SetErrObj(rtn, result)
															return rtn			
														end
													else
														.SetErrObj(rtn, Str.Format([ATTRTABLE_ERR.CouldNotGetNodeVersionFromId], objID))
														return rtn
													end
												else
													.SetErrObj(rtn, Str.Format([ATTRTABLE_ERR.CouldNotGetNodeFromId], objID))
													return rtn	
												end					
											
												return .SuccessResult(result)
												
											end

	
	public function Assoc GetNodeAttrDataFrame (Object prgCtx, \
																				Dynamic node, \ 			// *** either DAPINODE or Integer dataid
																				DAPIVERSION version = undefined)
											
												Assoc	rtn = .InitErrObj("GetNodeAttrDataFrame")
												Dynamic attrData
											
												// **** what is the DAPINODE
												if Type(node) == IntegerType
													node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, node)
													if IsError(node)
														.SetErrObj(rtn, node)
														return rtn			
													end
												elseif Type(node) != DAPI.DapiNodeType
													.SetErrObj(rtn, "Invalid datatype for node.")
													return rtn
												end
												
												Integer versionNum = IsDefined(version) ? version.pNumber : node.pVersionNum
												Integer id = node.pId
												Boolean removeInherit = FALSE
												Boolean	srcRemoveInherit = FALSE
												Integer secSourceID = 0
												Integer secSourceVNum = 0
												Integer sourceType = $lliApi.AttrSourceOriginal
														
												Assoc	status = $LLIApi.AttrUtil.GetAttrDataWithSourceOption( \
																								prgCtx, \
																								0, \
																								id, \
																								versionNum, \
																								removeInherit, \
																								sourceType, \
																								secSourceID, \
																								secSourceVNum, \
																								srcRemoveInherit )
																								
											
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												else
													attrData = status.attrData
												end
																								
												return .SuccessResult(attrData)
											end

	
	public function Assoc GetNodeCategoryVals(	Object prgCtx, \
																				DAPINODE node, \
																				DAPIVERSION version = undefined, \
																				Boolean convertUserIdtoName = false, \
																				List filteredCatIds = {})
																						
												Assoc	rtn = .InitErrObj("GetNodeCategoryVals")
												Assoc	status
												Frame	attrData
												Assoc	attrVals
												Integer	i
												String	catName
												List	attDefs
												Assoc	attDef, attDef2
												List	vals
												List	cats, children
												Assoc	cat
												List	setVals
												
												// *** first get the attribute data
												status = .GetNodeAttrDataFrame(prgCtx, node, version)
												
												if status.ok
													attrData = status.result
													
													cats = ._GetCategoriesOnNode(prgCtx, attrData, filteredCatIds)	
											
													for cat in cats
														catName = cat.DisplayName
														
														// *** set up the Assoc that will be our structure
														attrVals.(cat.Path) = Assoc.CreateAssoc()
													
													
														attDefs = ._GetAllAttrDefs(attrData, catName)
														
														for attDef in attDefs
															Dynamic catKey = attDef.catID
															Integer attKey = attDef.ID	
														
															// *** the valList is where the raw value is stored
															List rawValList = attrData.fData.(catKey).Values[1].(attKey).Values
														
															// *** retrieve these vals
															vals = ._GetAttributeValsFromRawList(prgCtx, rawValList, attDef, convertUserIdtoName)
															attrVals.(cat.Path).(attDef.name)  = Assoc.CreateAssoc()
															attrVals.(cat.Path).(attDef.name).vals = vals
															attrVals.(cat.Path).(attDef.name).Type = attDef.Type
															attrVals.(cat.Path).(attDef.name).Name = attDef.Name
															
															// *** drill into attribute sets
															if attDef.Type == $AttrTypeSet
																// *** null out the vals so that we avoid confusing ourselves --set values go in the children list
																attrVals.(cat.Path).(attDef.name).vals = {}
																children = {}	
															
																// *** example of what this looks like
																// ***{A<1,?,4=A<1,?,'ID'=4,'Values'={D/2012/2/25:0:0:0}>,5=A<1,?,'ID'=5,'Values'={'Field1'}>>,A<1,?,4=A<1,?,'ID'=4,'Values'={D/2012/3/25:0:0:0}>,5=A<1,?,'ID'=5,'Values'={'Field2'}>>,A<1,?,4=A<1,?,'ID'=4,'Values'={D/2012/4/25:0:0:0}>,5=A<1,?,'ID'=5,'Values'={'Field3','Field4'}>>}				
															
																for i = 1 to Length(vals)
																	setVals = {}
																	for attDef2 in attDef.children
																		Assoc a = Assoc.CreateAssoc()
																		a.Name = attDef2.Name
																		a.Type = attDef2.Type
																		a.vals = ._GetAttributeValsFromRawList(prgCtx, vals[i].(attDef2.ID).Values, attDef2, convertUserIdtoName)
																		setVals	= {@setVals, Assoc.Copy(a)}		 
																	end					
																	children = {@children, setVals}		
																end	
															end
															
															attrVals.(cat.Path).(attDef.name).children = children
														end	
													end	
													
												else
													.SetErrObj(rtn, status)
													return rtn
												end
												
												return .SuccessResult(attrVals)
																						
											end

	script PreCreateAddCatToNode
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Assoc PreCreateSetupCatsAtts(	Object prgCtx, \
																																			List categoryNodes, \
																																			Assoc vals, \
																																			Boolean nonInherit = false)
																										
																										Assoc		rtn = .InitErrObj("_AddOrSetCategoryToNode")	
																										Frame 		attrData 
																										Assoc		status
																										DAPINODE	catNode
																										String		attName, catName
																										
																										// *** get a blank category attribute frame 
																										attrData = $LLIAPI.AttrData.New( prgCtx, 0, 0 )
																									
																										// *** get the attributes
																									   	status = attrData.DBGet()
																									 	if !status.ok
																									 		.SetErrObj(rtn, status)
																									 		return rtn
																									 	end   	
																									    
																									    // *** Loop through the category ids
																									    for catNode in categoryNodes
																										    catName = catNode.pName
																										    
																										  	// **** Add the category to the attrData frame
																										 	status = attrData.AttrGroupAdd( catNode.pId )
																										 	if !status.ok
																										 		.SetErrObj(rtn, status)
																									 			return rtn
																									 		end	
																											
																											// *** Do we need to flag this category as non-inherit?
																											if nonInherit
																												._SetCategoryNonInherit(attrData, catNode.pName)
																											end
																										 	
																										 	// *** Now loop through the vals and save them
																										 	for attName in Assoc.Keys(vals)
																										 		status = ._SetAttributeValue(attrData, catName, attName, vals.(catName).(attName))
																										 		if !status.ok
																										 			.SetErrObj(rtn, status)
																										 			return rtn
																										 		end
																										 	end	
																										end	
																										 
																										return .SuccessResult(attrData)
																									 	
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	/*
												description:	
														
												parameters:		
												
												returns:		
												
												author:			Kit Miller
												
												date created:	7/11/03
													
												======================================================
												07/11/03		KM		Initial revision.
											*/
	public function Assoc SaveAttributeData(	Object prgCtx, \
																				Dynamic node, \
																				Frame attrData)
												
												Assoc	rtn = .InitErrObj("SaveAttributeData")
												
												// **** what is the DAPINODE
												if Type(node) == IntegerType
													node = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, node)
													if IsError(node)
														.SetErrObj(rtn, node)
														return rtn			
													end
												elseif Type(node) != DAPI.DapiNodeType
													.SetErrObj(rtn, "Invalid datatype for node.")
													return rtn
												end	
												
												Object 	llnode = $LLIApi.LLNodeSubsystem.GetItem( node.pSubType )
												Assoc 	result = llnode.NodeCategoriesUpdate( node, attrData )
												
												if !result.OK
													.SetErrObj(rtn, result)
													return rtn	
												end
												
												return rtn
												
											end

	script _GetAllAttrDefs
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									//
																									//
																									// Syntergy
																									// Author: 			Kit Miller  
																									// Date: 			March 20, 2008
																									// Description:		Utility method to find the integer keys of a particular attribute in the category definition frame.
																									// Modifications:
																									// Date           Name         Description
																									//
																									//////////////////////////////////////////////////////
																									
																									function List _GetAllAttrDefs(Frame attrData, \
																																String categoryName)
																									
																										List	defs
																										List	catKey
																										Assoc	catDef
																										Assoc	attDef1
																										Assoc 	definitions = attrData.fDefinitions	
																										
																										// *** example:  definitions.{81922,2}.Children[ 2 ].Children
																									
																										for catKey in Assoc.Keys(definitions)
																											catDef = definitions.(catKey)
																											if Str.Lower(catDef.DisplayName) == Str.Lower(categoryName)
																												for attDef1 in catDef.Children
																													defs = {@defs, _GetSmallDefFromAttrDef(attDef1, catKey)}
																												end		
																											end		
																										end	
																									
																										return defs
																										
																									end
																									
																									function Assoc _GetSmallDefFromAttrDef(Assoc attrDef, List catKey)
																									
																										Assoc 	a = Assoc.CreateAssoc()				
																										List	children = {}
																										Assoc	attrDef2
																										
																										a.ID = attrDef.ID
																										a.CatID = catKey
																										a.Name = attrDef.DisplayName	
																										a.Type = attrDef.Type			
																									
																										if IsFeature(attrDef, 'Children')
																											for attrDef2 in attrDef.Children
																												children = {@children, _GetSmallDefFromAttrDef(attrDef2, catKey)}								
																											end
																										end
																									
																										a.Children = children
																										
																										return a
																									
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	private function List _GetAllAttrNames(	Frame attrData, \
																			String categoryName)
											
												Assoc 	definitions = attrData.fDefinitions	
												List	key
												Assoc	catDef
												Assoc	attDef
												List 	attNames
											
												for key in Assoc.Keys(definitions)
													catDef = definitions.(key)
													if Str.Lower(catDef.DisplayName) == Str.Lower(categoryName)
														for attDef in catDef.Children
															attNames = {@attNames, attDef.DisplayName}					
														end	
													end		
												end	
												
												return attNames
											end

	
	private function Assoc _GetAttrKeysFromDefinition(Assoc catDef, \
																					String attributeName, \
																					String attrSetName = undefined)
											
												Assoc 	rtn = .InitErrObj("_GetAttrKeysFromDefinition")
												Assoc	attDef, attSetDef
												Assoc	result
											
												if IsDefined(attrSetName)	// *** then look one level more
													for attSetDef in catDef.Children
														if attSetDef.DisplayName == attrSetName
															for attDef in attSetDef.Children
																if attDef.DisplayName == attributeName
																	result.attKey = attDef.ID
																	result.attSetKey = attSetDef.ID
																	return .SuccessResult(result)						
																end
															end							
														end	
													end
												else
													for attDef in catDef.Children
														if attDef.DisplayName == attributeName
															result.attKey = attDef.ID
															return .SuccessResult(result)				
														end
													end		
												end
												
												// ** if we made it here, it means we never found it
												.SetErrObj(rtn, Str.Format("Could not find attribute named %1.", attributeName))
												return rtn
											
												return rtn
												
											end

	
	private function Assoc _GetAttrKeysFromDefinitions(	Assoc definitions, \
																						String categoryName,\
																						String attributeName, \
																						String attrSetName = undefined)
											
												Assoc 	rtn = .InitErrObj("_GetAttrKeysFromDefinitions")
												List	key
												List	catKey = undefined
												Assoc	catDef
												Assoc	status
												Assoc	result
											
												for key in Assoc.Keys(definitions)
													catDef = definitions.(key)
													if Str.Lower(catDef.DisplayName) == Str.Lower(categoryName)
														catKey = key
														status = ._GetAttrKeysFromDefinition(catDef, attributeName, attrSetName)
														if status.ok
															result = status.result
															result.catKey = catKey
															return .SuccessResult(result)				
														else
															.SetErrObj(rtn, Str.Format("Could not find attribute named %1 in category %2.", attributeName, categoryName))
															return rtn
														end
													end	
												end
											
												// ** if we made it here, it means we never found it
												.SetErrObj(rtn, Str.Format("Could not find attribute named %1 in category %2.", attributeName, categoryName))
												return rtn
												
											end

	
	private function List _GetAttributeValsFromRawList(	Object prgCtx, \
																						List rawValList, \
																						Assoc attDef, \
																						Boolean convertUserIdtoName = false)
											
												List	finalValList
												Dynamic	eachVal
												
												// *** loop through each value
												for eachVal in rawValList
													if attDef.type == $AttrTypeUser && convertUserIdToName
														// ***  convert userid to name if desired
														eachVal = ._UserNameFromID(prgCtx, eachVal)
													end	
														
													finalValList = {@finalValList, eachVal}
												end
											
												return finalValList
																					
											end

	
	private function Assoc _GetAttributeValueFromNames(	Frame attrData, \
																						String categoryName, \
																						String attributeName, \
																						String attrSetName = undefined)
											
												/* Note this script does not get multi attribute values.  It is a utility for getting a simple value given a category and attribute name (+ attribute set name if it's in a set) */
												/* See _GetAttributeValsFromDef for a complete script to get all values from an attrData frame given the attrDef */
																				
												Assoc	data = attrData.fData
												Assoc	definitions = attrData.fDefinitions	
												Assoc	rtn = .InitErrObj("_GetAttributeValue")
												Dynamic val
												
												// *** Locate the attributes								
												Assoc	result = ._GetAttrKeysFromDefinitions(definitions, categoryName, attributeName, attrSetName)	
																			
											
												// *** If we got the attribute, then return it
												if IsDefined(result.catKey) && IsDefined(result.attSetKey) && IsDefined(result.attKey)
													// **** example of what the data looks like 
													// data.{21316,12}.Values[ 1 ].23.Values[ 1 ].13					
													val = data.(result.catKey).Values[1].(result.attSetKey).Values[1].(result.attKey).Values[1]
												elseif 	IsDefined(result.catKey) && IsDefined(result.attKey)
													val = data.(result.catKey).Values[1].(result.attKey).Values[1]
												else
													.SetErrObj(rtn, Str.Format("Could not find attribute named %1 in category %2.", attributeName, categoryName))
													return rtn	
												end
												
												
												
												rtn.val = val
											
												return rtn
																					
											end

	
	private function List _GetCategoriesOnNode(	Object prgCtx, \
																				Frame attrData, \
																				List filteredCatIds)
											
												Assoc 	definitions = attrData.fDefinitions	
												List	catDefs
												List	key
												Assoc	catDef	
												
												// *** Now gather all the cat definitions that match
												for key in Assoc.Keys(definitions)
													if IsUndefined(filteredCatIds) || Length(filteredCatIds)==0 || key[1] in filteredCatIds
														catDef = definitions.(key)
														
														catDefs = {@catDefs, catDef}	
														
														// *** add the path
														catDef.Path = ._NodeIdToPath(prgCtx, key[1])
													end		
												end
												
												return catDefs
											end

	
	private function Assoc _GetCategoryId(	Object prgCtx, \
																			String catName)
																			
												Assoc 	rtn = .InitErrObj("_GetCategoryId")
												Dynamic	result
												
												// *** get the category from the categories volume
												String stmt = "select DataID from DTree where SubType=:A1 and UPPER(Name)=:A2"	
												
												result = .ExecSQL(prgCtx, stmt, {$TypeCategory, Str.Upper(catName)})
												
												if IsNotError(result) 
													if Length(result)
														
														rtn.ID = result[1].DATAID
														
													else
														.SetErrObj(rtn, Str.Format("Could not find category %1.", catName))
														return rtn	
													end	
												else
													.SetErrObj(rtn, result)
													return rtn		
												end
												
												
												return rtn
																			
											end

	
	private function Boolean _IsCategoryAlreadyOnNode(Frame attrData, \
																					  String categoryName)
											
											
												Assoc	definitions = attrData.fDefinitions	
												List	key
												Assoc	catDef
											
												for key in Assoc.Keys(definitions)
													catDef = definitions.(key)
													if Str.Lower(catDef.DisplayName) == Str.Lower(categoryName)
														return true
													end	
												end
											
												return false
												
											end

	
	private function Assoc _SaveAttributeValue(	Frame attrData, \
																				String categoryName, \
																				String attributeName, \
																				Dynamic val, \
																				String attrSetName = undefined)
																				
												Assoc	rtn = .InitErrObj("_SaveAttributeValue")
												Assoc	data = attrData.fData
												Assoc	definitions = attrData.fDefinitions	
												Assoc	status
												Assoc	result
												
												// *** Locate the attributes								
												status = ._GetAttrKeysFromDefinitions(definitions, categoryName, attributeName, attrSetName)								
												if status.ok
													result = status.result
												else
													.SetErrObj(rtn, status)
													return rtn	
												end
											
												// *** If we got the attribute, then return it
												if IsFeature(result, "attSetKey")
													// **** example of what the data looks like 
													// data.{21316,12}.Values[ 1 ].23.Values[ 1 ].13					
													data.(result.catKey).Values[1].(result.attSetKey).Values[1].(result.attKey).Values[1] = val
												else
													data.(result.catKey).Values[1].(result.attKey).Values[1] = val
												end
											
												return rtn
																					
											end

	
	private function Assoc _SetAttributeValue(	Frame attrData, \
																				String categoryName, \
																				String attributeName, \
																				Dynamic val, \
																				String attrSetName = undefined)
																				
												Assoc	data = attrData.fData
												Assoc	definitions = attrData.fDefinitions	
												Assoc	rtn = .InitErrObj("_SetAttributeValue")
												
												// *** Locate the attributes								
												Assoc	result = ._GetAttrKeysFromDefinitions(definitions, categoryName, attributeName, attrSetName)								
											
												// *** If we got the attribute, then return it
												if IsDefined(result.catKey) && IsDefined(result.attSetKey) && IsDefined(result.attKey)
													// **** example of what the data looks like 
													// data.{21316,12}.Values[ 1 ].23.Values[ 1 ].13					
													data.(result.catKey).Values[1].(result.attSetKey).Values[1].(result.attKey).Values[1] = val
												elseif 	IsDefined(result.catKey) && IsDefined(result.attKey)
													data.(result.catKey).Values[1].(result.attKey).Values[1] = val
												else
													.SetErrObj(rtn, Str.Format("Could not find attribute named %1 in category %2.", attributeName, categoryName))
													return rtn	
												end
											
												return rtn
																					
											end

	
	private function Void _SetCategoryNonInherit(	Frame attrData, \
																					String categoryName)
											
											
												Assoc	definitions = attrData.fDefinitions	
												List	key
												Assoc	catDef
											
												for key in Assoc.Keys(definitions)
													catDef = definitions.(key)
													if Str.Lower(catDef.DisplayName) == Str.Lower(categoryName)
														catDef.NonInherit = TRUE
													end	
												end
											
											end

end

package DATAMANAGER

public object LoadIterators inherits DATAMANAGER::AbstractIterator

	public		Boolean	_fCachedColumns = FALSE
	public		Dynamic	_fItemValidator
	public		Dynamic	_fValidateWrapper
	public		String	fFileDataKey = 'fileData'


	script CsvLineCallback
	
			
					
							
									
																													
												function Assoc Callback(Integer id, \
																		String path, \
																		Dynamic data, \
																		Boolean isRoot = false)
													
													// abstract.  All Callback methods must implement this signature.  Optional arguments can be added at the end if need be but must be defaulted.
													return undefined
												end			
										
									
								
							
						
					
				
			
		
	
	endscript

	script FileCallback
	
			
					
							
									
																						
												function Assoc Callback(Integer id, \
																		String path, \
																		Dynamic data, \
																		Boolean isRoot = false)
													
													// abstract.  All Callback methods must implement this signature.  Optional arguments can be added at the end if need be but must be defaulted.
													return undefined
												end			
												
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function Assoc GetCachedData(Object prgCtx,  Object dataCache, Dynamic key2, Dynamic  key1 = .fItemDataKey, Integer dataType = Assoc.AssocType, Integer listItemType = StringType)
																		
								Assoc 	rtn = .InitErrObj("GetCachedData")
								List	keys = {.fItemDataKey, Str.String(key2)}	
								Dynamic	cacheVals
								
								// *** get from cache
								cacheVals = dataCache.Get(prgCtx, keys, dataType, listItemType)
								if IsUndefined(cacheVals)
									.SetErrObj(rtn, Str.Format("Could not get itemData for keys %1", keys))
									return rtn
								end
								
								rtn.result = cacheVals
								return rtn						
							end

	
	override function void Scratch()
											
												Integer i
												Integer callCount = 250000
												$SyntergyCore.Timer.Start( "JavaSpeedTest" )
											
												
												for i = 1 to callCount	
													// *** call the IterateCSVFile Method
													Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvParser", "DoNothing", {} )
													
													
													if IsError(result)
														echo("Error: " + result)
														echo("Error: " + JavaObject.GetError() )
												        List errStack = JavaObject.GetErrorStack()
												        String s
												        for s in errStack
												            echo( s )
												        end		
												        return
													end
												end
													
												Real elapsed = $SyntergyCore.Timer.Stop( "JavaSpeedTest" )
												Echo( "CSV: ", elapsed * 1000.0, "ms, ", Math.Round( callCount / elapsed ), " rows per second" )								
											
											end

	
	private function String _GetFileTargetPath(String rootFilePath, \
																		  		String rootTargetPath, \
																				String filePath, \
																				String objectName, \
																				String targetPathType, \
																				Boolean includeRoot = false)
											
												if includeRoot
													rootFilePath = $DataManager.FileUtils.GetParentPath(rootFilePath)
												end	
																		  
												String path = rootTargetPath + ":" + Str.ReplaceAll(filePath, rootFilePath, "")
											    path = Str.ReplaceAll(path, File.Separator(), ":")
											    if path[-1] == ":"
											    	path = path[1:-2]
											    end	
												if path[1] == ":"
											    	path = path[2:]
											    end	    
											    
											    if IsDefined(objectName)
											    	List elements = Str.Elements(path, ":")
													
													if targetPathType == "parent"
												    	elements = {@elements, objectName}
												    else
												    	// make sure the last element is the $ObjectName value (not the file name value)
												    	elements[Length(elements)] = objectName
												    end	
											    	
											    	path = ''
											    	String el
											    	for el in elements
											    		path += ":" + el
											    	end
											    end
											    
											    return path
											
											end

	
	override function void _SubclassDestructor()
												
												._fPrgCtx = undefined
												._fJob = undefined
												._fDataCache = undefined
												._fValidateWrapper = undefined
											
											end

end

package DATAMANAGER

public object RDOTHERADDRESSEE inherits DATAMANAGER::RecMan

	override	String	fCreateApiKey3 = 'RMCreateInfo.rimsSentTo'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$RDOTHERADDRESSEE'
	override	String	fUpdateApiKey3 = 'fSentTo'

end

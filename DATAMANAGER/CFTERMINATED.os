package DATAMANAGER

public object CFTERMINATED inherits DATAMANAGER::ContractFile

	override	String	fApiKey3 = 'terminated'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$CFTERMINATED'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
												
							
							Assoc status = ._CheckBoolean(val)
							
							if status.ok
								val = status.result
							else
								return status
							end		
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
												
						end

end

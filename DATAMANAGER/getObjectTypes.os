package DATAMANAGER

public object getObjectTypes inherits DATAMANAGER::ExportRequestHandlers

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Object		prgCtx = .fPrgCtx
								
							/**
							 * R may contain: 
						     **/		
								
							// *** hand off to the API
							Assoc result = $DataManager.ObjectTypes.GetAllObjectTypes(prgCtx)
							.fContent = synCore.SuccessResult(result.listOfAssoces)	
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{ \
								}
						end

end

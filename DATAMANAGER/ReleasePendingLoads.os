package DATAMANAGER

public object ReleasePendingLoads inherits DATAMANAGER::#'DataManager LLRequestHandlers'#

	override	Boolean	fEnabled = TRUE
	override	String	fHTMLFile = 'ReleasePendingLoads.html'
	public		List	fJobs


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
				
				Object		prgCtx = .PrgSession()
		
				// load the jobs to display on page
				Assoc filter = Assoc{'jobtype': 'load'}
				Assoc status = $DataManager.LoadJobsAPI.GetFilteredItems(prgCtx, undefined, filter, true)
						
				Assoc job
				
				for job in status.result
					String	rootPath = job.rootSourcePath
					String	subPath
					
					if File.Exists(rootPath) && File.IsDir(rootPath)
						job.subPaths = {}	
					
						For subPath in File.FileList(rootPath)
							if ._IsPending(subPath)
								job.subPaths = {@job.subPaths, subPath}	
							end
						end						
						
						if (Length(job.SubPaths)> 0)
							.fJobs = {@.fJobs, job}
						end	
					end
				end
							
				return undefined		
				
			end

	
	public function String ReleaseUrl(String path)
			
				return Str.Format('%1?func=datamanager.ReleasePendingFolder&dirPath=%2', .URL(), Web.Escape(path))
					
			end

	
	private function Boolean _IsPending(String path)
				
				return $DataManager.Utils.IsPending(path)
				
			end

	
	private function Void _TestIsPending()
				
				echo(._IsPending("C:\ImportExport\EPC\WR - White Rose_PENDING\"))
				echo(._IsPending("C:\ImportExport\Offgas & Petchem\PROJECT DEVELOPMENT PROJECTS.pending\"))
				echo(._IsPending("C:\ImportExport\Offgas & Petchem\PROJECT DEVELOPMENT PROJECTS.pending\"))
				echo(._IsPending("C:\ImportExport\03 Contract Agreements\M-N.Pending\"))
				
						
			end

end

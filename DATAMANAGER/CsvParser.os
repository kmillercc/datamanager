package DATAMANAGER

public object CsvParser inherits DATAMANAGER::SynCoreMain

	
	public function Assoc CheckCsvSize(String csvPath, Integer limit)
							
				// *** call the ParseCsvFile Method
				Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvParser", "CheckCsvSize", {csvPath, limit} )
				
				// *** parse the Java Error and return
				if IsError(result)
					return ._ReturnJavaError(.InitErrObj("CheckCsvSize"), result)
				end
				
				// *** return success
				Assoc rtn
				rtn.ok = true
				rtn.result = result	// *** integer number of lines in the CSV
				
				return rtn	
			
			end

	
	public function Assoc GetHeader(String csvPath)
							
				// *** call the GetHeader Method
				Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvParser", "GetHeader", {csvPath} )
				
				// *** parse the Java Error and return
				if IsError(result)
					return ._ReturnJavaError(.InitErrObj("GetHeader"), result)					
				end
				
				// *** return success
				Assoc rtn
				rtn.ok = true
				rtn.result = result	
				
				return rtn	
			
			
			end

	
	public function Assoc ParseDataIdFile(String filePath, \
												Object  lineCallbackObj = undefined, \
												String  lineCallbackFunc = "CsvLineCallback", \
												String  auditPath = undefined)
												
				// *** get the dateFormats
				
				// *** set a limit for how many lines we want to actually parse
				Integer endLineNo = 1000000
					
				// *** call the ParseCsvFile Method
				Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvParser", "ParseDataIdFile", {filePath, 1, endLineNo, lineCallbackObj, lineCallbackFunc, auditPath} )
				
				// *** parse the Java Error and return
				if IsError(result)
					return ._ReturnJavaError(.InitErrObj("ParseDataIdFile"), result)
				end
				
				// *** return success
				Assoc rtn
				rtn.ok = true
				rtn.result = result	
				
				return rtn						
			
			end

	
	public function Assoc ParseFile(String csvPath, \
												Integer startLineNo = 1, \				// start with line #2 if you already have columns and need the data
												Integer endLineNo = undefined, \				// start with line 1 and end with line 1 if you need only the headers	
												Integer limit = 1000000, \
												Object  lineCallbackObj = undefined, \
												String  lineCallbackFunc = "CsvLineCallback")
												
				// *** get the dateFormats
				
				// *** set a limit for how many lines we want to actually parse
				endLineNo = IsDefined(endLineNo) ? endLineNo : limit
					
				// *** call the ParseCsvFile Method
				Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvParser", "ParseCsvFile", {csvPath, startLineNo, endLineNo, lineCallbackObj, lineCallbackFunc} )
			
				
				// *** parse the Java Error and return
				if IsError(result)
					return ._ReturnJavaError(.InitErrObj("ParseFile"), result)
				end
				
				// *** return success
				Assoc rtn
				rtn.ok = true
				rtn.result = result	
				
				return rtn						
			
			end

	
	public function Assoc ParseValidateFile(String csvPath, \
														 String auditPath, \
														 Object  callbackObj, \
														 String  lineCallbackFunc, \
														 String  colCallbackFunc, \
														 Integer rowIndex)
														
			
				// *** get the dateFormats
				List dateFormats = ._GetJavaDateFormats()
					
				// *** call the ValidateParseCsvFile Method
				Dynamic result = JavaObject.InvokeStaticMethod( "com.syntergy.utils.CsvParser", "ValidateParseCsvFile", {csvPath, auditPath, dateFormats, callbackObj, lineCallbackFunc, colCallbackFunc, rowIndex} )	
				
				// *** parse the Java Error and return
				if IsError(result)
					return ._ReturnJavaError(.InitErrObj("ParseValidateFile"), result, "Error in CSV file. ")
				end
				
				// *** return success
				Assoc rtn
				rtn.ok = true
				rtn.result = result	
				
				return rtn
											
			end

	
	private function _GetJavaDateFormats()
							
				Object glob = $DataManager
			
				if !(glob.fDateFormatsInitialized)
					// *** defaults will be like this
					// just date: 02/17/2015
					// date with time: 02/17/2015 04:37 PM
					
					
					String	dateFormat = $Kernel.SystemPreferences.GetPrefLocale( "InputDateFormat", "%m/%d/%Y"  )
					String 	timeFormat = $Kernel.SystemPreferences.GetPrefLocale( "InputTimeFormat", "%I:%M %p" )
					
					/* Java Formats:
				
						Letter	Date or Time Component	Presentation	Examples
						y		Year					Year			1996; 96
						M		Month in year			Month			July; Jul; 07
						d		Day in month			Number			10
						E		Day in week				Text			Tuesday; Tue
						a		Am/pm marker			Text			PM
						H		Hour in day (0-23)		Number			0
						k		Hour in day (1-24)		Number			24
						K		Hour in am/pm (0-11)	Number			0
						h		Hour in am/pm (1-12)	Number			12
						m		Minute in hour			Number			30
						s		Second in minute		Number			55
					*/
					
					// *** map oscript tokens to Java
					Assoc dateMap
					dateMap.("%b") = "MMM"	 	/*The three-character abbreviated month name (e.g., Jan, Feb, etc.)*/
					dateMap.("%m") = "MM"	 	/*The two-digit month (e.g., 01-12)*/
				
					dateMap.("%d") = "dd"	 	/*The two-digit day of the month, from 01 to 31 (e.g., 01-31)*/
				
					dateMap.("%Y") = "yyyy"	 	/*The year, including the century (e.g., 1993)*/
				
					Assoc timeMap
					timeMap.("%H") = "hh"	 	/*The two-digit hour on a 24-hour clock, from 00 to 23*/
					timeMap.("%I") = "h"	 	/*The two-digit hour, from 01 through 12*/
					timeMap.("%M") = "mm"	 	/*The minutes past the hour, from 00 to 59*/
					timeMap.("%S") = "ss"	 	/*The seconds past the minute, from 00 to 59*/		
					timeMap.("%p") = "a"	 	/*AM or PM*/
					
					String key
					
					for key in Assoc.Keys(dateMap)
						dateFormat = Str.ReplaceAll(dateFormat, key, dateMap.(key))
					end
					
					for key in Assoc.Keys(timeMap)
						timeFormat = Str.ReplaceAll(timeFormat, key, timeMap.(key))
					end
					
					glob.fJavaDateFormat = dateFormat
					glob.fJavaDateTimeFormat = dateFormat + " " + timeFormat
			
					glob.fDateFormatsInitialized = true
				end
				
				return {glob.fJavaISO8601DateFormat, glob.fJavaDateTimeFormat, glob.fJavaDateFormat}
				
					
			end

end

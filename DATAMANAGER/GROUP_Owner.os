package DATAMANAGER

public object GROUP_Owner inherits DATAMANAGER::OwnerGroup

	override	String	fApiKey3 = 'pGroupId'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$GROUP'


	
	override function Assoc ValidateVal(Object prgCtx, \
																		Dynamic val, \
																		String action, \
																		Frame apiDataObj)
																	
												
												Assoc status = ._CheckGroup(prgCtx, val)
												
												if status.ok
													val = status.result
												else
													return status
												end		
												
												Assoc rtn
												rtn.result = val
												rtn.ok = true
												return rtn
																	
											end

end

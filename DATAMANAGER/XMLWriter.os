package DATAMANAGER

public object XMLWriter inherits DATAMANAGER::FileWriter

	override	String	fSubKey = 'xml'


	
	override function Assoc EndFile(String closingElementStr = "")
											
												Assoc rtn
												rtn.ok = true
											
												// *** Write out the end of the livelink element
												File.Write(._fFile, Str.Format("</%1>", closingElementStr))
											
												// *** Try to close the file
												Dynamic result = File.Close(._fFile)
											
												if IsError(result)
													rtn = .SetErrObj(.InitErrObj("EndFile"), result)
													rtn.errMsg += " Error closing file."
													return rtn
												end
											
												return rtn
											
											end

	
	public function Assoc StartFile(String rootElementStr = "", String encoding="")
												
												
												Assoc status = .OpenFile()
												if !status.ok
													return .SetErrObj(.InitErrObj("StartFile"), status)
												end
												
												// **** out the opening xml stuff
												File.Write(._fFile, Str.Format('<?xml version="1.0" encoding="%1"?>', encoding))
												File.Write(._fFile, rootElementStr)		
											
												Assoc rtn
												rtn.ok = true		
												return rtn
											
											end

end

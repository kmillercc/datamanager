package DATAMANAGER

public object FileUtils inherits DATAMANAGER::Utils

	
	public function Assoc CheckPathExists(String path, String typeObj = undefined /* "file" or "folder"*/)
														
				if !File.Exists(path)
					return .SetErrObj( .InitErrObj( "CheckPathExists" ), Str.Format('File path "%1" does not exist', path) )
				end	
			
				if IsDefined(typeObj)
					// check to make sure that its a file or folder
					if typeObj=='folder' && !File.IsDir(path)
						return .SetErrObj( .InitErrObj( "CheckPathExists" ), Str.Format('File path "%1" is not a folder.', path) )
					elseif typeObj=='file' && 	File.IsDir(path)
						return .SetErrObj( .InitErrObj( "CheckPathExists" ), Str.Format('File path "%1" is not a file.', path) )
					end
				end	
			
				Assoc rtn
				rtn.ok = true
				return rtn
			end

	
	public function Assoc CreateFolderRetry(String path, Integer numRetries = 10)
				
				Integer i
				for i = 1 to numRetries
					if !File.Exists(path)
					 	// *** ok, we can just create it
						if File.Create(path)
							break	
						end
					else
						break		
					end	
				end	
				if !File.Exists(path)
					return .SetErrObj(.InitErrObj("CreateFolderRetry"), Str.Format("Could not create folder path '%1'", path))
				end
			
				Assoc rtn
				rtn.ok = true	
				return rtn	
			
			end

	
	public function Assoc CreateTimeSpecificDir(	String rootDir, String formatStr = '%Y-%m-%dT%H-%M-%S')
			
								
				Assoc	retVal = .InitErrObj("CreateTimeSpecificDir")								
			
				String fName = Date.DateToString(Date.Now(), formatStr)
				fName = .StripIllegalFileChars(fName)
			
				String dir = rootDir + fName + File.Separator()	
				
				if !File.Exists(dir)
					Dynamic result = File.Create(dir)
					
					if IsError(result)
						.SetErrObj(retVal, Str.Format("Could not create folder path %1", dir))
						return retVal
					end	
				end	
				
				return .SuccessResult(dir)
				
			end

	
	public function String EnsureDirSeparator(String path)
														
				// *** ensure that it's not undefined
				path = IsUndefined(path) ? "" : path
			
				// ** make sure Import path has a trailing separator
				path = Str.Trim(path)
				if !File.IsDirSpec(path)
					path += File.Separator()
				end	
				
				return path
				
			end

	
	public function Assoc EnsurePathExists(String wholePath, Boolean doOnlyLastElement = FALSE)
					
				Assoc	rtn = .InitErrObj("EnsurePathExists")
				Object	log = .GetLogger()
				Assoc	status
				
				// *** convert all "/" and "\" separators to system specific
				wholePath = Str.ReplaceAll(wholePath, "/", File.Separator())
				wholePath = Str.ReplaceAll(wholePath, "\", File.Separator())	
			
				// *** now split into parts
				List 	elements = $SyntergyCore.FileUtils.SplitPath(wholePath)
				
				// *** make sure we have at least 2 elements
				if Length(elements) < 2
					.SetErrObj(rtn, Str.Format("EnsurePathExists: invalid path '%1' specified", wholePath))
					return rtn		
				end
				
				// *** fix for UNIX root path
				if wholePath[1] == File.Separator() && Length(elements)
					elements[1] = File.Separator() + File.Separator() + elements[1]
				end	
				
				// *** do we know that the parent/grandparent/... paths already exist?  if so, we can just skip to the last element in the path
				if doOnlyLastElement
				
					status = .CreateFolderRetry(wholePath)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn				
					else			
						log.DEBUG(Str.Format("Created folder at path: '%1.'", wholePath))
					end	
				
				else
				
					// *** loop through the parts until we find the root path
					// *** should be 1st element e.g. "C:\" or "\\anotherPC" -- this should always exist
					Integer i
					String path = ''
					for i = 1 to Length(elements)
						if Length(elements[i])
							// *** grab the root
							path += elements[i] + File.Separator()
							
							if i>=2
								if File.Exists(path)
									log.TRACE(Str.Format("path '%1' exists", path))
								else
									log.TRACE(Str.Format("path '%1' DOES NOT exist", path))
									
									status = .CreateFolderRetry(path)
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn				
									else			
										log.DEBUG(Str.Format("Created folder at path: '%1.'", path))
									end	
								end				
							end	
						end	
					end
				end	
				
				return rtn
			end

	
	public function Integer FileCountRecursive( String path, Integer limit, Assoc result = undefined, List excludedFileNames = {}, Boolean isRoot = true)
														
					if IsUndefined(result)
						result = Assoc.CreateAssoc()
						result.count = 0
					end	
														
					String	f
															
					if isRoot
						$SyntergyCore.Timer.Start( "FileCountRecursive" )		
					end
																
					if ( File.IsDirSpec( path ) && File.Exists( path ) )
														
						for f in File.FileList( path )
																	
							if (File.GetName(f) in excludedFileNames == 0)
																
								result.count+=1
																	
								if result.count>limit
									return result.count
								end
																		
								if File.IsDirSpec( f )
									FileCountRecursive( f, limit, result, excludedFileNames, false )
								end
							end	
						end
														
					else
															
						return 0
															
					end
														
					if IsRoot
						echo("FileCountRecursive Total Count = ", result.count, " files and folders.")
						Real elapsed = $SyntergyCore.Timer.Stop( "FileCountRecursive" )
						echo("FileCountRecursive ", elapsed, " seconds")
																
					end
														
					return result.count
														
			end

	
	public function Assoc FileListRecursive( String path, \
													Integer limit, \
													Boolean returnFiles = true, \
													Boolean onlyMetadataFiles = false, \
													List excludedFileNames = {}, \
													Object callbackObj = undefined, \
													String callbackFunc = "FileCallback", \
													Assoc result = Assoc.CreateAssoc(), \
													Boolean excludeMetadataFiles = false, \
													Boolean isRoot = true)
														
				Assoc 	rtn = .InitErrObj("FileListRecursive")
				String	f
				Assoc	status
				Object  logger = .GetLogger()
														
				// *** default the result
				if Length(result)==0
					result = Assoc.CreateAssoc()
					result.count=0
					result.files={}
				end
													 
				if isRoot
					$SyntergyCore.Timer.Start( "FileListRecursive" )		
				end
															
				if ( File.IsDirSpec( path ) && File.Exists( path ) )
													
					for f in File.FileList( path )
						String fileName = File.GetName(f)
						
						if fileName[-1] == File.Separator()
							fileName = fileName[1:-2]
						end
																
						if fileName[-10:-1] == "_PROCESSED"
																
							logger.INFO(Str.Format("Skipping File '%1' because it is marked '_PROCESSED.'", f))	
		
						elseif $DataManager.Utils.IsPending(fileName) 
																
							logger.INFO(Str.Format("Skipping File '%1' because it is marked '_PENDING.'", f))	
		
																
						elseif (fileName in excludedFileNames == 0) && !(excludeMetadataFiles && .IsMetadataCsv(f))
													
							if onlyMetadataFiles 
								if !File.IsDirSpec( f ) && .IsMetadataCsv(f) && returnFiles
									result.files = {@result.files, f}
								end									
							elseif returnFiles
								result.files = {@result.files, f}							
							end	
																	
							result.count+=1
																	
							if IsDefined(callbackObj) && IsFeature(callbackObj, callbackFunc)
								status = callbackObj.(callbackFunc)(result.count, f, undefined)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end
							end						
													
							if result.count>limit
								return .SuccessResult(result)
							end
																	
							if File.IsDirSpec( f )
								status = FileListRecursive( f, limit, returnFiles, onlyMetadataFiles, excludedFileNames, callbackObj, callbackFunc, result, excludeMetadataFiles, false )
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								end						
							end
						end	
					end
													
				else
					// KM *** 11/1/17 Ignore this error
					//.SetErrObj(rtn, Str.Format("FileListRecursive called on a non-folder: %1.", path))
					//return rtn
														
				end
													
				if IsRoot
					echo("FileListRecursive Total Count = ", result.count, " files and folders.")
					Real elapsed = $SyntergyCore.Timer.Stop( "FileListRecursive" )
					echo("FileListRecursive ", elapsed, "total time")
															
				end
													
				return .SuccessResult(result)
													
			end

	
	public function String GetFileArchivePath()
					String defaultDir = $Kernel.FileUtils.GetTempDir()
					if defaultDir == ""
						$Kernel.FileUtils.CreateTempDir()
						defaultDir = $Kernel.FileUtils.GetTempDir()
					end
					String path = $DataManager.ModulePrefs.GetPref("FILE_ARCHIVE_PATH", defaultDir)
					path = Str.ReplaceAll(.EnsureDirSeparator(path), '\', '\\')
					return path
				end

	
	public function String GetFileRemovalPath()
														
				String defaultDir = $Kernel.FileUtils.GetTempDir()
				
				if defaultDir == ""
					$Kernel.FileUtils.CreateTempDir()
					defaultDir = $Kernel.FileUtils.GetTempDir()
				end
				
				String path = $DataManager.ModulePrefs.GetPref("FILE_REMOVAL_PATH", defaultDir)
				
				path = Str.ReplaceAll(.EnsureDirSeparator(path), '\', '\\')
				
				return path
			
			end

	
	public function String GetParentPath(String pathIn, Integer goUp = 1)
				
				String path = pathIn
				
				Integer i = 1
				for i = 1 to goUp
					path = path[-1] == File.Separator() ? path[1:-2] : path
				
					if IsDefined(Str.Locate(path, File.Separator()))
						path = path[1:Str.RChr(path, File.Separator())]
					end		
				end
				
				path = .EnsureDirSeparator(path)
				
				return path
			end

	
	public function String GetUniqueFileName(	String dir, String fileName, String fExt, Boolean overwrite = false)
				
				Integer i = 0
				String	wholePath = Str.Format('%1%2.%3', dir, fileName, fExt)
				
				if overwrite
					if File.Exists(wholePath)
						File.Delete(wholePath)
					end
				else
					while File.Exists(wholePath)
						i += 1
						wholePath = Str.Format('%1%2_%3.%4', dir, fileName, i, fExt)
					end		
				end
				
				return wholePath
												
			end

	
	public function Boolean IsCsv(String filePath)
															
				String fName = File.GetName(filePath)
			
				return Str.Upper(fName[-3:-1]) == "CSV" && fName[1:2] != "._"
				
			end

	
	public function Boolean IsMetadataCsv(String filePath)
															
				if !File.IsDirSpec(filePath) 
					String fileName = File.GetName(filePath)
				
					if Str.Upper(fileName[-3:-1]) == "CSV" && Str.LocateI(fileName, "metadata")
						return true
					end
				end	
				
				return false
			end

	
	public function String StripIllegalFileChars(String fileName, Assoc options = Assoc.CreateAssoc())
														
				String	strPattern = "<[/@?<>\:@*|?^'""#~]+>"
				List	illegalFirstLastChars = {"."," "}
				String	strReplace = "_"
				
				// *** Strip out any illegal windows file chars
				Assoc		result = $LLIAPI.FormatPkg.StrChangeAll(fileName, strPattern, strReplace)
				if result.Count > 0 && IsDefined(result.value)
					fileName = result.value
				end		
				
				// *** ensure that the last char is not a period or space
				String	appendStr = ''	
				String lastChar = fileName[-1]
				while lastChar in illegalFirstLastChars
					fileName = fileName[1:Length(fileName)-1]
					lastChar = fileName[-1]
					appendStr += '_'
					breakif Length(fileName)==0
				end
				fileName += appendStr
				
				// *** ensure that the first char is not a period or space
				String prependStr = ''
				String firstChar = fileName[1]
				while firstChar in illegalFirstLastChars
					fileName = fileName[2:Length(fileName)]
					firstChar = fileName[1]
					prependStr += '_'
					breakif Length(fileName) == 0
				end
				fileName = prependStr + fileName
			
				return fileName
			end

	
	public function String TrimLeadingSeparator(String path)
														
				// *** ensure that it's not undefined
				path = IsUndefined(path) ? "" : path
			
				// ** make sure Import path has no leading separator
				path = Str.Trim(path)
				path = path[1] == File.Separator() ? path[2:] : path
				
				return path
				
			end

end

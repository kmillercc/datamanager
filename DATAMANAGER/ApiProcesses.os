package DATAMANAGER

public object ApiProcesses inherits DATAMANAGER::FactoryObjects

	override	String	fKey = 'APIProcess'
	public		String	fName
	public		String	fType


	
	public function Assoc Finalize(Assoc options)
		
			Assoc rtn
			rtn.ok = true
			return rtn
		end

	
	public function String GetAuditFolderPath(Assoc options)
											
												String auditPath
											
												if IsDefined(options.auditFolderPath) && Length(options.auditFolderPath)	
													String path = $DataManager.FileUtils.EnsureDirSeparator(options.auditFolderPath)
													Assoc status = $DataManager.FileUtils.EnsurePathExists(path)
													if status.ok
														auditPath = path
													else
														auditPath = ._GetDefaultAuditPath(options)
													end
												else
													auditPath = ._GetDefaultAuditPath(options)
												end	
											
												return auditPath		
											
											end

	script GetSourceRoot
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Assoc _GetSourceRoot(Object prgCtx, Assoc options)
																									
																										Dynamic root
																										String 	path
																										Integer id
																									
																										
																										Assoc result
																										result.root = root
																										result.path = path
																										result.id = id	
																										
																										return .SuccessResult(result)
																									end	
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function GetTargetRoot(Object prgCtx, Assoc options)
																			
												Integer		id
												String		path
												
												
												Assoc result
												result.path = path
												result.id = id	
												
												return .SuccessResult(result)	
												
											end

	
	public function Assoc Initialize(Object prgCtx, \
																		Object dataCache, \
																		Object job, \
																		Assoc options, \
																		Object iterator, \
																		Integer limit, \
																		Assoc sourceSettings)
																		
												Assoc	rtn = .InitErrObj("Initialize")
											
												return rtn
													
											end

	/*
											* Get the name of the task.
											*
											* @return {String}
											*/
	public function String Name()
											
												String		name
												
												if ( IsDefined( .fName ) && Length( .fName ) > 0 )
												
													name = .fName
												
												else
												
													name = .Key()
												
												end
												
												
												return name
											
											end

	
	public function Object New( Assoc options, \
																Dynamic subtype = undefined)
											
										Frame f = ._NewFrame(subtype)
									
									
										return f
										
									end

	
	public function Assoc NewJobPayload(Object prgCtx, Assoc options, String action)
											
							// *** implemented in subclasses
							return undefined	
						end

	
	public function  Assoc ProcessItem(	Object prgCtx, \
																		String action, \
																		Frame apiDataObj, \
																		Assoc actionFlags, \
																		Assoc options)
											
												Assoc rtn
												rtn.ok = true
												return rtn						
											end

	
	public function Assoc ValidateItem(Object prgCtx, \
																		String action, \
																		Frame apiDataObj, \
																		Assoc dataFlags, \
																		Assoc options, \
																		String filePath = undefined, \		
																		String rootFilePath = undefined)	
																		
												Assoc 	rtn = .InitErrObj( "ValidateItem" )
												
												return rtn
											end

	
	public function Assoc ValidateJob(Object prgCtx, \
																		List colNames, \
																		String action, \
																		Assoc options, \
																		String rootSourcePath, \
																		String rootTargetPath, \
																		Integer limit, \
																		Assoc sourceSettings)
												
												Assoc rtn
												rtn.ok = true			
																			
												return rtn
											
											end

	
	private function String _GetDefaultAuditPath(Assoc options)
											
												// **** need to do something here.  For now, log to the opentext logs folder
												String path = File.StartupDir() + "logs" + File.Separator() + "datamanager" + File.Separator() 
													
												return path			
											end

end

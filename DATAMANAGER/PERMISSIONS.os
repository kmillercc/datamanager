package DATAMANAGER

public object PERMISSIONS inherits DATAMANAGER::Perms

	override	String	fApiKey1 = 'permData'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$PERMISSIONS'


	override script ValidateVal
	
		
				// Patched by Pat201601220
				function Assoc ValidateVal(Object prgCtx, \
											Dynamic val, \
											String action, \
											Frame apiDataObj)
										
					/* we want to build a changeList that we can pass into the llNode.NodeRightsUpdate  Here's what it needs to look like:
						* The changeList assocs include the following fields:
						*
						*	Type			Type of update:
						*
						*				 	0	($LLIApi.updateRightNone) None.  Do nothing.
						*					1	($LLIApi.updateRightReplace) Replace.  Replace for node permissions
						*						and ACEs.
						*					2	($LLIApi.updateRightAdd) Add.  Add node permissions or ACE.
						*						Not applicable for system permissions.
						*					3	($LLIApi.updateRightAddReplace) Add & Replace.  Replace for node
						*						permissions and ACE.
						*					4	($LLIApi.updateRightRemove) Remove.  Not applicable for system
						*						permissions.
						*
						*	permType		Permissions type: ACL (DAPI.PERMTYPE_ACL), user (DAPI.PERMTYPE_USER),
						*					group (DAPI.PERMTYPE_GROUP), or world (DAPI.PERMTYPE_WORLD).
						*	RightID			The user or group id for ACL permissions Type.
						*					New user or group id for user owner or group owner.
						*					Ignored for world.
						*	Permissions		The new permissions mask.
						*
						* The options assoc includes:
						*
						*	ClearACLs				TRUE to clear Existing ACL.
						*	CheckGrantGroupsOnly	TRUE to check grant access based on the restrict to groups only option.
					*/	
					
					List	changeList	
					Assoc	options
				
					// *** do we have a "clear" directive at beginning of string?
					if val[1:7]=='<clear>' || val[1:7]=='[clear]'
						options.ClearACLs = TRUE
						val = val[8:]
					end		
				
					// *** pushdown no longer supported -- need to update documentation	
					if val[1:10]=='<pushdown>' || val[1:17]=='<pushdownupdate>'
						// *** we no longer support pushdown operations.  This should be done in Content Server UI
						return .SetErrObj(.InitErrObj("ValidateSetVal"), "The DataManager is currently not supporting 'pushdown' $Permissions operations. To update multiple nodes, copy $Permissions values to multiple rows in your CSV or use the Content Server UI.")
					end	
				
					// *** skip over empty strings
					// *** KM 11-18-15 adjusted this logic -- a colon with no perms in front can signify no perms at all
					if val[1] in {"|", ._fMultiValDelim}
						val=val[2:]
					end
					
					// *** parse the string
					List	permList = Str.Elements(val, ._fMultiValDelim)
					
					// *** loop through the result
					String s
					for s in permList
						Assoc changeVals
						
						// *** by default set this to $LLIApi.updateRightAddReplace
						changeVals.Type = $LLIApi.updateRightAddReplace	
						
						List 	p = Str.Elements(s, ":")
						
						if Length(p) < 2
							return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("Invalid format for $Permissions column. Expecting at least two elements separated by colon in this section. Instead encountered '%1.' Consult the user guide.", s))
						end
					
						Integer i
						for i=2 to Length(p)
							// *** KM 1-22-16 allow <remove> flag to signal that user/group is to be removed from ACL
							if Str.Upper(p[1]) in {"[REMOVE]","<REMOVE>"}
								changeVals.Type = $LLIAPI.UpdateRightRemove
								changeVals.Permissions = undefined			
							else
								// *** perm mask	
								changeVals.Permissions = _PermStringToMask(p[1])		
							end
				
							
							// *** KM 11-18-15 need to set this to undefined to avoid inadvertently updating the owner or group to a different ID (if the Owner or Group comes after the 
							changeVals.RightID = undefined
				
							// *** the user or group
							String  userGroup = p[i]
						
							switch Str.Upper(userGroup)
								
								case "$OWNER"
									changeVals.permType = DAPI.PERMTYPE_USER
								end
								
								case "$GROUP"
									changeVals.permType = DAPI.PERMTYPE_GROUP
								end
								
								case "$PUBLIC"
									changeVals.permType = DAPI.PERMTYPE_WORLD
								end
								
								default // *** it's a user or group name
									changeVals.permType = DAPI.PERMTYPE_ACL
							
									Integer typeObj
									if userGroup[1] == "G"
										typeObj = UAPI.GROUP
									elseif userGroup[1] == "U"
										typeObj = UAPI.USER
									else
										return .SetErrObj(.InitErrObj("ValidateSetVal"), "Invalid entry '" + userGroup + "' in permissions column. Expected a 'U' or 'G' prefix to denote user or group.")
									end
									
									// *** get from kuaf
									RecArray results = .ExecSql(prgCtx, "select ID from KUAF where UPPER(Name)=:A1 and Type=:A2 and Deleted=0", {Str.Upper(userGroup[2:]), typeObj})
									if IsNotError(results) && Length(results)
										changeVals.RightID = results[1][1]
									else
										return .SetErrObj(.InitErrObj("ValidateSetVal"), Str.Format("$Permissions column error: Could not find user or group named %1.", userGroup))
									end
								end
							
							end
					
							changeList = {@changeList, Assoc.Copy(changeVals)}
						end	
						
					end
					
					Assoc rtn
					rtn.result = Assoc.CreateAssoc()
					rtn.result.changeList = changeList
					rtn.result.options = options		
					rtn.ok = true
					return rtn							
												
				end
				
				function Integer _PermStringToMask(String permString)
				
					Integer mask = $PRemoveNode
					String	s
					
					for s in permString
						
						switch s
						
							case 'S'
								mask |= $PSee 			
							end
							
							case 'C'
								mask |= $PSeeContents
							end
							
							case 'M'
								mask |= $PModify			
							end
							
							case 'A'
								mask |= $PCreateNode			
							end
							
							case 'E'
								mask |= $PEditAtts			
							end
							
							case 'V'
								mask |= $PDeleteVersions			
							end
							
							case 'D'
								mask |= $PDelete			
							end
							
							case 'R'
								mask |= $PCheckOut			
							end
							
							case 'P'
								mask |= $PEditPerms
							end
		
							case 'J'
								mask |= $PAddMajorVersion
							end
						
						end
						
					end
					
					return mask
				
				end						
		
		
	
	endscript

end

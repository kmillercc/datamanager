package DATAMANAGER

public object DataCache inherits DATAMANAGER::InstanceObjects

	public		Dynamic	_fData
	public		List	_fDbKeys
	public		Integer	_fJobId
	public		String	_fJobType
	public		Boolean	_fNeedDb = TRUE
	public		Dynamic	_fPattern
	override	Boolean	fEnabled = TRUE


	
	public function Assoc CleanupType( Object prgCtx, String jobType, Integer minAgeSeconds )
												
												Date 	dbDate = CAPI.Now( prgCtx.fDBConnect.fConnection )
											
												prgCtx.fDBConnect.StartTrans()
												
													String stmt
													if $Syntergycore.SQLUtils.IsOracle()
														stmt = "delete from DATAMANAGER_DATACACHE dc WHERE dc.EXPIRATION_DATE<:A1 AND dc.JOB_TYPE =:A2 and not exists ( SELECT 1 FROM SYN_JOB sj WHERE sj.JOB_ID=dc.JOB_ID)"
													else
														stmt = "delete dc from DATAMANAGER_DATACACHE AS dc where dc.EXPIRATION_DATE<:A1 AND dc.JOB_TYPE =:A2 and not exists ( SELECT 1 FROM SYN_JOB sj WHERE sj.JOB_ID=dc.JOB_ID)"
													end	
												
													Integer result = .ExecSQL( prgCtx, stmt, { dbDate - minAgeSeconds, jobType } )
													
													if IsError(result)
														prgCtx.fDBConnect.EndTrans( false )
														return .SetErrObj( .InitErrObj( 'CleanupType' ), result, prgCtx )
													end
													
												prgCtx.fDBConnect.EndTrans( true )
															
											
												Assoc rtn
												rtn.result = result	
												rtn.ok = true
												return rtn
											End

	script Get
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Dynamic Get( Object prgCtx, \
																														  Dynamic keys, \
																														  Integer dataType, \
																														  Integer listItemType = dataType, \
																														  Dynamic defaultVal = undefined, \						  
																														  Boolean isLong = false,\
																														  Boolean orderByInt = false)
																														  
																										Dynamic result		// *** return value
																										Integer i			  
																									
																										if dataType == ListType && listItemType == ListType
																											.GetLogger().Warn("DataCache.Get Caller passed in List dataType without specifying the type of item in the List. A List of lists is not allowed.  List item type assumed to be string.")
																											listItemType = StringType 
																										end
																									
																										if IsDefined(keys) && Type(keys) != ListType
																											keys = {keys}
																										end
																										
																										if Type(keys) != ListType
																											keys = {keys}
																										end
																										
																										// **** which data col to put in
																										String dataCol = ._GetDataCol(dataType==ListType ? listItemType : dataType, undefined, isLong)
																									
																										// *** Now build the statement
																										String stmt
																										List subs
																										
																										// *** do a distinct?
																										if dataType==ListType && dataCol != "VAL_LONG" && !orderByInt
																											
																											// *** SQL server does not allow DISTINCT on NTEXT columns
																											if IsDefined(Str.Locate(dataCol, "VAL_STRING_MAX")) && $DataManager.SynCoreInterface.IsMSSQL()
																												// do nothing
																											else
																												dataCol = "DISTINCT " + dataCol
																											end	
																											
																										end
																										
																										switch Length(keys)
																											case 1
																												stmt = Str.Format("select %1 from DATAMANAGER_DATACACHE where JOB_ID=:A1 and JOB_TYPE=:A2 and KEY1=:A3", dataCol)
																												subs = {._fJobId, ._fJobType, keys[1]}
																											end
																											
																											case 2
																												stmt = Str.Format("select %1 from DATAMANAGER_DATACACHE where JOB_ID=:A1 and JOB_TYPE=:A2 and KEY1=:A3 and KEY2=:A4", dataCol)
																												subs = {._fJobId, ._fJobType, keys[1], keys[2]}
																											end
																										end	
																										
																										
																										// *** order by int?
																										if orderByInt
																											stmt += ' order by VAL_INT'
																										end
																											
																										
																										RecArray recs = .ExecSql(prgCtx, stmt, subs)
																										if IsNotError(recs) && Length(recs)
																												
																											if dataType != ListType
																									
																												if dataType == StringType && Length(RecArray.FieldNames(recs)) > 1
																													result = IsDefined(recs[1][1]) ? recs[1][1] : recs[1][2]
																													
																												else
																													result = recs[1][1]
																												end
																												
																												result = _ConvertVal(result, dataType)
																									
																											elseif dataType == ListType
																										
																												// *** did we select a string?  which col is it, the max or the 255 column?
																												if listItemType == StringType && Length(RecArray.FieldNames(recs)) > 1
																													List valList1 = RecArray.ColumnToList(recs, 1)
																													List valList2 = RecArray.ColumnToList(recs, 2)			
																													result = {}
																												
																													Dynamic val
																													for i = 1 to Length(valList1)
																														val = IsDefined(valList1[i]) ? valList1[i] : valList2[i]
																														
																														val = Type(val) != listItemType ? _ConvertVal(val, listItemType) : val
																														
																														if orderByInt
																															result = {@result, val}
																														else
																															result = List.SetAdd(result, val)
																														end	
																													end			
																												else
																													List valList = RecArray.ColumnToList(recs, 1)
																													result = {}
																												
																													Dynamic val
																													for val in valList
																														result = List.SetAdd(result, _ConvertVal(val, listItemType))
																													end	
																												end
																										
																												if Length(result) == 1 && IsUndefined(result[1])	
																													result = {}
																												end
																											end			
																										
																										elseif IsNotError(recs) && Length(recs)==0 && dataType == ListType
																										
																											result = {}	
																									
																										end
																									
																										return result
																									end
																									
																									function _ConvertVal(val, dataType)
																									
																										if dataType==ListType && Type(val) != ListType
																											val = {val}
																										elseif dataType== Assoc.AssocType && Type(val) == StringType
																												val = Str.StringToValue(val)
																										elseif dataType== IntegerType && Type(val) == StringType
																											val = Str.StringToInteger(val)
																										elseif dataType== RealType && Type(val) == StringType
																											val = Str.StringToReal(val)
																										end
																										
																										return val
																									end
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function Assoc IncrementInt(Object prgCtx, \
																		Dynamic keys  /*** either can be list or other */, \
																		Integer increment, \
																		Integer timeout = 8*60*60)  /* timeout in seconds default to 8 hours */
											
												
												String 		stmt
												Dynamic 	subs
												Assoc		rtn = .InitErrObj("Put")
											
												if Type(keys) != ListType
													keys = {keys}
												end
												
												Integer i
												for i=1 to Length(keys)
													// *** Do we need to hash the key (i.e. for unwieldy file paths and such)?
													keys[i] = ._FormatKey(keys[i])	
												end	
												
												// *** which data Col to put in?
												String dataCol = "VAL_INT"
												
												switch Length(keys)
													case 1
														stmt = Str.Format('update DATAMANAGER_DATACACHE set %1=%1+%2 where JOB_ID =:A1 and JOB_TYPE=:A2 and KEY1=:A3', dataCol, increment)
														subs = {._fJobId, ._fJobType, keys[1]}
													end
													
													case 2
														stmt = Str.Format('update DATAMANAGER_DATACACHE set %1=%1+%2 where JOB_ID =:A1 and JOB_TYPE=:A2 and KEY1=:A3 and KEY2=:A4', dataCol, increment)
														subs = {._fJobId, ._fJobType, keys[1], keys[2]}
													end
												end
												
												// *** actual insert
												prgCtx.fDbConnect.StartTrans()
													Dynamic result = .ExecSql(prgCtx, stmt, subs)
													if IsError(result)
														prgCtx.fDbConnect.EndTrans(false)
														.SetErrObj(rtn, result)
														return rtn
													end
												
												prgCtx.fDbConnect.EndTrans(true)		
												
												return rtn
											end

	
	public function Frame New( Object prgCtx, Integer jobId, String jobType)
									
										Frame f = ._NewFrame()
									
										f._fJobId = jobId	
										f._fJobType = jobType
										f._fPattern = Pattern.CompileFind( "[!a-zA-Z0-9_]" )	
											
										return f
										
									end

	
	public function Assoc Put(	Object prgCtx, \
																Dynamic keys  /*** either can be list or other */, \
																Dynamic newData, \ /* the data we are caching */
																Boolean update = false, \
																Boolean isLong = false, \
																Integer intOrder = undefined, \
																Integer timeout = 24*60*60)  /* timeout in seconds default to 72 hours */
											
												
												String 		stmt
												Dynamic 	subs
												Date 		dbDate = CAPI.Now( prgCtx.fDBConnect.fConnection )
												Date 		expiration_date = dbDate + timeOut
											
												if Type(keys) != ListType
													keys = {keys}
												end
												
												// *** which data Col to put in?
												String dataCol = ._GetDataCol(Type(newData), newData, isLong)
												
												// *** are we using the VAL_INT column to keep things in order?
												Boolean		orderByInt = IsDefined(intOrder) && dataCol != "VAL_INT" && !update && Type(newData) != ListType
												
												// *** convert new data if assoc
												if Type(newData) == Assoc.AssocType
													newData = Str.ValueToString(newData)
												end	
												
												switch Length(keys)
													case 1
														if update
															stmt = Str.Format("update DATAMANAGER_DATACACHE set %1=:A1 where JOB_ID =:A2 and JOB_TYPE=:A3 and KEY1=:A4", dataCol)
															subs = {newData, ._fJobId, ._fJobType, keys[1]}
														else
															if orderByInt
																stmt = Str.Format("insert into DATAMANAGER_DATACACHE (JOB_ID, JOB_TYPE, KEY1, %1, EXPIRATION_DATE, VAL_INT) values (:A1, :A2, :A3, :A4, :A5, :A6)", dataCol)
															else
																stmt = Str.Format("insert into DATAMANAGER_DATACACHE (JOB_ID, JOB_TYPE, KEY1, %1, EXPIRATION_DATE) values (:A1, :A2, :A3, :A4, :A5)", dataCol)
															end					
															
																			
															switch Type(newData)
																case ListType
																	subs = RecArray.Create( {"1","2","3","4","5"} )
																	Dynamic val
																	for val in newData
																		RecArray.AddRecord(subs, {._fJobId, ._fJobType, keys[1], val, expiration_date})
																	end
																end
																default
																	if orderByInt
																		subs = {._fJobId, ._fJobType, keys[1], newData, expiration_date, intOrder}
																	else
																		subs = {._fJobId, ._fJobType, keys[1], newData, expiration_date}
																	end								
																end
															end	
														end					
													end
													
													case 2
														if update
															stmt = Str.Format("update DATAMANAGER_DATACACHE set %1=:A1 where JOB_ID =:A2 and JOB_TYPE=:A3 and KEY1=:A4 and KEY2=:A5", dataCol)
															subs = {newData, ._fJobId, ._fJobType, keys[1], keys[2]}
														else
															if orderByInt			
																stmt = Str.Format("insert into DATAMANAGER_DATACACHE (JOB_ID, JOB_TYPE, KEY1, KEY2, %1, EXPIRATION_DATE, VAL_INT) values (:A1, :A2, :A3, :A4, :A5, :A6, :A7)", dataCol)
															else
																stmt = Str.Format("insert into DATAMANAGER_DATACACHE (JOB_ID, JOB_TYPE, KEY1, KEY2, %1, EXPIRATION_DATE) values (:A1, :A2, :A3, :A4, :A5, :A6)", dataCol)
															end									
															
															switch Type(newData)
																case ListType
																	subs = RecArray.Create( {"1","2","3","4","5","6"} )
																	Dynamic val
																	for val in newData
																		RecArray.AddRecord(subs, {._fJobId, ._fJobType, keys[1], keys[2], val, expiration_date})
																	end
																end
																default
																	if orderByInt
																		subs = {._fJobId, ._fJobType, keys[1], keys[2], newData, expiration_date, intOrder}
																	else
																		subs = {._fJobId, ._fJobType, keys[1], keys[2], newData, expiration_date}
																	end							
																end
															end	
														end	
													end
												end
												
												// *** actual insert
												prgCtx.fDbConnect.StartTrans()
													Dynamic result = .ExecSql(prgCtx, stmt, subs)
													if IsError(result)
														prgCtx.fDbConnect.EndTrans(false)
														return .SetErrObj(.InitErrObj("Put"), result)
													end
												
												prgCtx.fDbConnect.EndTrans(true)		
												
												Assoc rtn
												rtn.ok = true
												return rtn
											end

	
	private function _FormatKey(Dynamic key)
											
												Integer LENGTH_LIMIT = 24
											
												if Type(key) == StringType && (Length(key) > LENGTH_LIMIT || Pattern.Find( key, ._fPattern ))
													key = ._HashKey(key)
												end
											
												return key
											
											end

	
	private function String _GetDataCol(Integer dataType, Dynamic newData, Boolean isLong)
											
												if isLong
													return "VAL_LONG"
												end
											
											
												// **** which data col to put in
												String dataCol = "VAL_STRING_255"  // default val
												
												switch dataType
													case Assoc.AssocType
														dataCol = "VAL_LONG"
													end
													
													case IntegerType
														dataCol = "VAL_INT"		
													end
													
													case BooleanType
														dataCol = "VAL_INT"	
													end
													
													case StringType
														if IsDefined(newData)
															if Length(newData) > 255
																dataCol = "VAL_STRING_MAX"	
															else
																dataCol = "VAL_STRING_255"
															end
														else
															dataCol = "VAL_STRING_MAX,VAL_STRING_255"
														end	
													end
													
													case RealType
														dataCol = "VAL_REAL"		
													end
												
													case DateType
														dataCol = "VAL_DATE"		
													end
												end
											
												return dataCol
											
											end

end

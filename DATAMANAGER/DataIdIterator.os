package DATAMANAGER

public object DataIdIterator inherits DATAMANAGER::NodeIterator

	public		Integer	fNumErrors
	override	String	fSubKey = 'dataids'
	
	override function Assoc Iterate(Object prgCtx, \
									Dynamic notUsed, \
									String filePath, \
									Boolean isRoot = true, \
									String rootFilePath = ._fRootTargetPath)
																	
		/*
			Iterates through a file and tries to get a node from a dataid on each line
		*/								
							
		Assoc	status
		String 	auditPath = ._fOptions.auditFolderPath
		
		// *** load the csv
		status = $DataManager.CsvParser.ParseDataIdFile(filePath, this, "LineCallback", auditPath)
		if !status.ok
			return .SetErrObj(.InitErrObj("Iterate"), status)
		elseif Length(status.result.data) == 0
			return .SetErrObj(.InitErrObj("Iterate"), "Error parsing the dataID file. Please check the audit logs.")
		end						  
		
		Assoc result = Assoc{'numErrors': .fNumErrors}

		return Assoc{'ok': true, 'result': result}
		
	end

	
	public function Assoc LineCallback(Integer id, \
								Assoc data, \
								Boolean isRoot = false, \
								Assoc origVals = Assoc.CreateAssoc())					
		
		Assoc	rtn = .InitErrObj("CsvIterator.CsvCallback")
		Object 	prgCtx = ._fPrgCtx
		Assoc 	options = ._fOptions		
		Boolean	recurse = options.recurse
		Integer limit = ._fLimit
		String	parentPath		
	
		// *** did we get a node id from this node?
		if IsFeature(data, "NodeID")
		
			Integer nodeId
		
			// *** get the nodeId from the data
			if Type(data.NodeID) == IntegerType
				nodeId = data.NodeID		
			
			elseif Type(data.NodeID) == StringType
				nodeId = Str.StringToInteger(data.NodeID)
				if IsUndefined(nodeId)
					.fNumErrors += 1
					String errMsg = Str.Format("Could not convert value '%1' from line '%2' to an integer dataid.", data.NodeID, id)
					.GetLogger().Error(errMsg)
					.SetErrObj(rtn, errMsg)
					return rtn 
				end
			end	
	
			// *** try to get a dapinode from the nodeid
			DAPINODE node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, nodeId)
			if IsError(node)
				.fNumErrors += 1
				.SetErrObj(rtn, Str.Format("Could not find object with dataid of '%1' from line '%2'.", data.NodeID, id))
				return rtn 			
			end		
		
			// *** is this a folder node?
			Boolean		isFolderNode = ._IsFolderNode(node, $LLIAPI.LLNodeSubsystem.GetItem(node.pSubType))				
			
			if ._fHierarchyStructure
				// *** ok, determine what the path would be
				String fullFilePath = ._fRootTargetPath + ._GetFileNameFromNode(._fRootTargetPath, node, isFolderNode)
				parentPath = $DataManager.FileUtils.GetParentPath(fullFilePath)
			end			
			
			// *** now call the callback as if we were iterating through nodes like NodeIterator
			String 	nodePath = ._NodeIDToPath(prgCtx, node.pId)
			String 	nodeFilePath = ._fRootTargetPath
			Assoc status  = .Callback(node.pId, nodeFilePath, node, false, parentPath)
			if !status.ok
				.fNumErrors += 1
				.SetErrObj(rtn, status)
				return rtn
			end
			
			if .fIterateTotal > limit
				.SetErrObj(rtn, "Limit of " + Str.String(limit) + " reached.")
				return rtn				
			end
			
			// *** recurse
			if recurse && isFolderNode
				// *** last parameter is false because we don't want to recurse unlimited
				// *** KM 2-18-16 removed the hardcoded false for Pfizer -- who wanted to export Contract Files from the Binder Level
				status = ._IterateNode(prgCtx, node, nodePath, false, nodeFilePath)
				if !status.ok
					.fNumErrors += 1
					.SetErrObj(rtn, status)
					return rtn
				end				
			end		
			
		end
		
		return rtn
		
	end

	
	override function void _0SetSubkey()
												.fSubkey = 'dataids'
											end

	
	private function Assoc _IterateNode(Object prgCtx, \
										DAPINODE node, \
										String nodePath, \
										Boolean isRoot = true, \
										String filePath = ._fRootTargetPath, \
										Boolean	recurse = ._fOptions.recurse)
		/*
			Iterates Content Server nodes and calls a callback to process them
		*/								
							
		Assoc 	rtn = .InitErrObj("IterateNode")
		Assoc	status
		Assoc 	options = ._fOptions		
		Integer limit = ._fLimit
		
		// *** include the root node?
		if options.includeRoot && IsRoot
			status = this.Callback(node.pId, nodePath, node, true, filePath)
			
			if !status.ok
				.fNumErrors += 1
				// *** KM 12-20-18 Log Failure to csv
				._LogFailure(node, nodePath, filePath, status)	
			end
			
		end		
	
		// *** get a list of files, passing in callback
		Object childGetter = $DataManager.NodeChildGetter.New(node.pSubType)
		status = childGetter.GetSubNodes(prgCtx, node)	
		if !status.ok
			.SetErrObj(rtn, status)
			return rtn
		else
			DAPINODE childNode
			String	newFilePath
			
			if isRoot 
				if ._fIncludeRoot
					newFilePath = filePath + node.pName + ._fSeparator
				else
					newFilePath = filePath
				end
			else
				newFilePath = filePath + node.pName + ._fSeparator	
			end	
	
			for childNode in status.result
				String 	childNodePath = nodePath + ":" + childNode.pName
				
				// *** call the callback
				status = this.Callback(childNode.pId, childNodePath, childNode, false, newFilePath)
				if !status.ok
					.fNumErrors += 1
				
					// *** KM 12-20-18 Log Failure to csv
					._LogFailure(childNode, childNodePath, newFilePath, status)	
				end
				
				if .fIterateTotal > limit
					.SetErrObj(rtn, "Limit of " + Str.String(limit) + " reached.")
					return rtn				
				end
				
				// *** recurse
				if recurse && $DataManager.NodeUtils.IsFolderNode(childNode)
					status = _IterateNode(prgCtx, childNode, childNodePath, false, newFilePath)
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end				
				end
				
			end
		end					
		
		return rtn	
		
	end

end

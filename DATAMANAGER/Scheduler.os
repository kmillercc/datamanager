package DATAMANAGER

public object Scheduler inherits DATAMANAGER::SynCoreMain

	
	public function Date GetNextDateTime(Assoc dbVals, Date startPoint )
	
			// *** default in some values to avoid having to check for undefineds
			//Date startDate = IsDefined(dbVals.start_date) ? Date.Date(dbVals.start_date) : Date.Date(CAPI.Now( prgCtx.fDBConnect.fConnection ))
			//Integer startTime = IsDefined(dbVals.start_time) ? dbVals.start_time : 0
			//Date endDate = IsDefined(dbVals.end_date) ? dbVals.end_date : Date.StringToDate("12/31/3000", "%m/%d/%Y")
			//Integer endTime = IsDefined(dbVals.end_time) ? dbVals.end_time : 86399
			
			// *** return value
			Date 	nextDateTime	
			Date 	today = Date.Date(startPoint)
			Boolean tryForToday = true
			
			switch dbVals.schedule_type
			
				case 1	// one time only
					nextDateTime = dbVals.one_time_datetime
				end
				
				case 2	// recurring
					
					Date 	nextDay = ._GetNextScheduledDay(today, tryForToday, dbVals.day_pattern, dbVals.day_recurrence, dbVals.days_of_week)
					
					switch dbVals.daily_frequency
		
						case 1 		// certain times during the day
							nextDateTime = ._GetNextDailyAtTime(dbVals.daily_at_times, nextDay)
							
							if IsUndefined(nextDateTime)
								// means we tried to schedule for today but could not fit it in
								tryForToday = false
								nextDay = ._GetNextScheduledDay(today, tryForToday, dbVals.day_pattern, dbVals.day_recurrence, dbVals.days_of_week)
								nextDateTime = ._GetNextDailyAtTime(dbVals.daily_at_times, nextDay)
							end
							
						end
						
						case 2 		// regular intervals during the day
							Integer add = dbVals.daily_every
							
							if tryForToday
								nextDateTime = Date.Now() + 60
							else	
								switch dbVals.daily_every_unit
									case 1  //(Hours) 
										nextDateTime = startPoint + (add*3600)	
									end
									
									case 2  //(Minutes)	
										nextDateTime = startPoint + (add * 60)																
									end
								end		
							end	
						end
					end
				end
			end
			
			// *** fit within start and end dates
			// KM commented out 10-15-19 there is no time window functionality right now
			// nextDateTime = ._FitWithinWindow(dbVals, nextDateTime, startTime, endDate, endTime)
			
			return nextDateTime
		
		end

	
	override function void scratch()
			
			Date rightNow = Date.Now()
			Date nextDateTime
			
			echo("RightNow=", rightNow)
			
			nextDateTime = rightNow + (1*3600)	
			echo("adding 1 hour=", nextDateTime)
	
			nextDateTime = rightNow + (6*3600)	
			echo("adding 6 hours=", nextDateTime)
					
			nextDateTime = rightNow + (10*60)	
			echo("adding 10 minutes=", nextDateTime)
	
			nextDateTime = rightNow + (60*60)	
			echo("adding 60 minutes=", nextDateTime)
					
		end

	
	private function Integer _DateToSeconds(Date d)
												
			Integer	h = Date.Hour(d)
			Integer	m = Date.Minute(d)
			Integer	s = Date.Second(d)
			
			// *** calculate seconds
			s += (m*60) + (h*3600)
		
			return s
		end

	
	private function Date _FitWithinWindow(	Assoc dbVals, \
											Date nextDateTime, \
											Integer startTime, \
											Date endDate, \
											Integer endTime, \
											Integer tries = 1)
			
			// *** not confident that infinite loop is impossible.  short circuit it here
			if tries > 100
				// for debugging
				.SetErrObj(.InitErrObj("_FitWithinWindow"), Str.Format("Could not get NextDateTime after %1 tries.", tries))
				return undefined
			end
	
			// *** get the next time in seconds
			Integer nextTimeSeconds = ._DateToSeconds(nextDateTime)
	
			// *** so, is the datetime within the end_date ?	
			if nextDateTime > endDate
			
				// for debugging
				.SetErrObj(.InitErrObj("_FitWithinWindow"), Str.Format("NextDateTime falls after endDate of %1", endDate))
				nextDateTime = undefined
			
			elseif nextTimeSeconds > endTime
			
				// *** get 24 hours later
				Date nextDate = nextDateTime + 86400
				nextDate = Date.Date(nextDate)
			
				nextDateTime = _FitWithinWindow(dbVals, nextDate, startTime, endDate, endTime, tries + 1)
				
			elseif nextTimeSeconds < startTime	
	
				nextDateTime = nextDateTime + startTime
			
			end	
	
			return nextDateTime
	
		end

	
	private function Date _GetNextDailyAtTime(String daily_at_times, Date nextDay)
		
			List	timesInSeconds
			
			if IsDefined(daily_at_times)
				
				List 	secondStrings = Str.Elements(daily_at_times, "|")
				String  secondStr
				Integer i
				
				for secondStr in secondStrings
					i+=1 	
					timesInSeconds = {@timesInSeconds, Str.StringToInteger(secondStr)}
				end
			
			end	
			
			// *** KM 3-8-16 make sure that we have a time here
			if Length(timesInSeconds)==0
				timesInSeconds = {0}
			end
		
			// *** sort the list
			timesInSeconds = List.Sort(timesInSeconds)
			
			// *** get the next seconds
			Boolean isToday = Date.Date(Date.Now()) == nextDay
			if isToday
				Integer seconds = ._GetNextSeconds(timesInSeconds)
				
				if IsUndefined(seconds)
					return undefined
				else
					return nextDay + seconds	
				end
			else
				return nextDay + timesInSeconds[1]	
			end	
			
		end

	
	private function Date _GetNextScheduledDay(Date today, Boolean tryForToday, Integer day_pattern, Integer day_recurrence, String days_of_week)
									
			// ** return value
			Date nextDay = today
						
			switch day_pattern
			
				case 1		// every x days			
					nextDay = ._GetNextScheduledDayIncremental(today, day_recurrence)
				end
				
				case 2		// on certain days of week
					nextDay = ._GetNextScheduledDayOfWeek(today, tryForToday, days_of_week)
	
				end
			
			end
		
			return nextDay
		
		end

	
	private function Date _GetNextScheduledDayIncremental(Date today, Integer day_recurrence)
			
			Integer numDays = day_recurrence		
			
			return today + (numDays * 86400)	
			
		end

	
	private function Date _GetNextScheduledDayOfWeek(Date today, Boolean tryForToday, String days_of_week)
			
			// *** start with the day of week passed in
			Integer 	thisDayOfWeek = Date.DayOfWeek(today)
			
			if tryForToday && days_of_week[thisDayOfWeek] == "1"
				return today
			else
				Integer i = 1
				
				while i <= 7
					Date testDate = today + (i*86400)
					
					Integer testDayOfWeek = Date.DayOfWeek(testDate)
					
					if  days_of_week[testDayOfWeek] == "1"
						return testDate
					end
					
					i+=1
				end
			end	
			
			return today
			
		end

	
	private function Integer _GetNextSeconds(List timesInSeconds)
		
			// *** - we don't want a next time that's earlier than now
			Integer secondsNow = ._DateToSeconds(Date.Now())
			Integer eachSeconds
			
			for eachSeconds in  timesInSeconds
				if eachSeconds > secondsNow
					return eachSeconds
				end
			end
			
			return undefined
			
		end

	
	private function void _oTest()
	
			/*
			Date today = Date.Date(Date.Now())
			
			echo("")
			echo("")
	
			echo(._GetNextScheduledDayIncremental(today, 1), " should be ", "Thursday")
			echo(._GetNextScheduledDayIncremental(today, 2), " should be ", "Friday")
			echo(._GetNextScheduledDayIncremental(today, 3), " should be ", "Saturday")
			echo(._GetNextScheduledDayIncremental(today, 4), " should be ", "Sunday")
			echo(._GetNextScheduledDayIncremental(today, 5), " should be ", "Monday")
			echo(._GetNextScheduledDayIncremental(today, 6), " should be ", "Tuesday")
	
			echo("")
			
			echo(._GetNextScheduledDayOfWeek(today, false, '0111111'), " should be ", "Thursday")
			echo(._GetNextScheduledDayOfWeek(today, true, '0001111'), " should be ", "Wednesday")
			echo(._GetNextScheduledDayOfWeek(today, true, '0000111'), " should be ", "Thursday")
			
			echo(._GetNextScheduledDayOfWeek(today, false, '1110011'), " should be ", "Friday")
			echo(._GetNextScheduledDayOfWeek(today, false, '1110001'), " should be ", "Saturday")
			echo(._GetNextScheduledDayOfWeek(today, false, '1110000'), " should be ", "Sunday")
			echo(._GetNextScheduledDayOfWeek(today, false, '0110000'), " should be ", "Monday")
			
			
			echo(._GetNextScheduledDayOfWeek(today, true, '1110000'), " should be ", "Sunday")
			*/
			
			Object prgCtx = $PrgSessions[1]
			
			String	stmt = 'select * from DATAMANAGER_JOB_SCHEDULES where SCHEDULEID=:A1'
			RecArray result = .ExecSql(prgCtx, stmt, {72})
			
			Assoc dbVals = Assoc.FromRecord(result[1])
			
			echo(.GetNextDateTime(dbVals, Date.Now()))
			
			
			
		end

end

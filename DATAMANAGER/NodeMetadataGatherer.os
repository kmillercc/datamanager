package DATAMANAGER

public object NodeMetadataGatherer inherits DATAMANAGER::ExportObjects

	public		List	_fAlwaysColNames = { '$OBJECTNAME', '$TARGETPATH', '$OBJECTID', '$OBJECTTYPE' }
	public		List	_fFilterCatIds
	public		String	_fMVDelimiter = '|'
	public		Boolean	_fMetadataOnly = FALSE
	public		List	_fSystemAttrColNames
	public		Boolean	_fWantAllVersions = TRUE
	public		Boolean	_fWantBasicData = TRUE
	public		Boolean	_fWantCategoryData = TRUE
	public		Boolean	_fWantClassifications = TRUE
	public		Boolean	_fWantContractData = FALSE
	public		Boolean	_fWantEmails = TRUE
	public		List	_fWantNodeIdCols
	public		Boolean	_fWantOwner = TRUE
	public		Boolean	_fWantPerms = TRUE
	public		Boolean	_fWantPhysObj = TRUE
	public		Boolean	_fWantPhysObjCirc = FALSE
	public		Boolean	_fWantPhysObjOther = FALSE
	public		Boolean	_fWantRMData = TRUE
	public		Boolean	_fWantRenditions = FALSE
	public		Boolean	_fWantSystemAtts = FALSE
	public		Boolean	_fWantVersionData = FALSE


	
	public function Assoc GatherMetadata(	Object prgCtx, \
							DAPINODE node, \
							DAPIVERSION version, \
							String sourcePath, \
							String fileName = "") /* KM 6-13-16 removed ability to pass in object name. Now must always equal the node.pName */
											
			Assoc 		status
				
			// *** initialize the csvVals assoc
			Assoc csvVals = Assoc.Copy(._fEmptyVals)
		
			// *** columns that identify a node	
			if ._fWantNodeIdCols
		
				if IsFeature(._fEmptyVals, "$ObjectName")		
					csvVals.('$ObjectName') = node.pName	/* KM 6-13-16 removed ability to pass in object name. Now must always equal the node.pName */
				end
				
				if IsFeature(._fEmptyVals, "$TargetPath")			
					csvVals.('$TargetPath')	= ":" + ._NodeToPath(prgCtx, node)
				end
				
				if IsFeature(._fEmptyVals, "$ObjectID")					
					csvVals.('$ObjectID') = Str.String(node.pID)
				end
				
				if IsFeature(._fEmptyVals, "$ObjectType")					
					csvVals.('$ObjectType') = Str.String(node.pSubType)
				end	
			end
			
			if !._fMetadataOnly	
				csvVals.('$SOURCEPATH') = sourcePath
			end	
			
			// *** Basic node data
			if ._fWantBasicData
				status = ._GetNodeVals(prgCtx, node, version, csvVals)
				if !status.ok
					return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
				end	
			end
			
			// *** Permissions
			if ._fWantPerms
				
				if IsFeature(._fEmptyVals, "$Permissions")		
					csvVals.('$Permissions') = ._GetPermissionString(prgCtx, node, csvVals)
				end	
		
		
			end
			
			// *** Owner
			if ._fWantOwner
				if IsFeature(._fEmptyVals, "$Owner")
					csvVals.('$Owner') = ._UserNameFromID(prgCtx, node.pUserID)
				end	
				if IsFeature(._fEmptyVals, "$Group")
					csvVals.('$Group') = ._UserNameFromID(prgCtx, node.pGroupID)
				end	
			end	
			
			// *** Classification
			if ._fWantClassifications
				if IsFeature(._fEmptyVals, "$Classification")
					csvVals.('$Classification')	= ._GetClassificationPathStr(prgCtx, node)
				end	
			end
		
			// *** add category keys into colNames
			if ._fWantCategoryData || Length(._fFilterCatIds)
				// *** get the category csvVals so we know the columns we need
				status = ._GetCategoryVals(prgCtx, node, version, csvVals)
				if !status.ok
					return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
				end
			end	
			
			// *** version type metadata
			if IsDefined(version)
				// *** all versions?
				if ._fWantAllVersions || ._fWantVersionData
	
					csvVals.('$EXPORTED_FILENAME') = fileName	
	
					status = ._GetVersionVals(prgCtx, node, version, csvVals)
					if !status.ok
						return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
					end		
				end	
			
				// *** all versions?
				if ._fWantRenditions && IsDefined($Rendition)
	
					csvVals.('$EXPORTED_FILENAME') = fileName
	
					status = ._GetRenditionVals(prgCtx, node, version, csvVals)
					if !status.ok
						return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
					end		
				end	
			end
			
			// *** get email properties if it's an email
			if ._fWantEmails && IsDefined($OTEmail) && 	node.pSubtype == 749// *** type email
				status = ._GetEmailVals(prgCtx, node, csvVals)
				if !status.ok
					return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
				end	
			end		
			
			// *** get Rec Man attributes if user asked for any
			if ._fWantRmData && IsDefined($RecMan) && $DataManager.NodeUtils.IsRecManagedSubtype(prgCtx, node.pSubtype)
				status = ._GetRecManVals(prgCtx, node, csvVals)
				if !status.ok
					return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
				end	
			end	
		
			// *** get Physical Object data if user asked for any
			if (._fWantPhysObj || ._fWantPhysObjCirc) && IsDefined($PhysicalObjects) && node.pSubtype in {411,412,424}
				status = ._GetPhysObjVals(prgCtx, node, csvVals)	
				if !status.ok
					return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
				end	
			end	
			
			// *** What about system attributes
			if ._fWantSystemAtts && Length(._fSystemAttrColNames)
				String aName
				for aName in ._fSystemAttrColNames
					Dynamic aVal = DAPI.GetNodeUserAttr(node, aName)
					if IsNotError(aVal)
						csvVals.(aName) = ._FormatCatVal(aVal)
					else
						csvVals.(aName) = undefined
					end	
				end
				
			end
			
			// *** Contract Metadata
			if ._fWantContractData
				// *** Binder metadata
				if node.pSubtype == 31066
					status = ._GetBinderData(prgCtx, node, csvVals)
					if !status.ok
						return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
					end		
		
			
				// *** Contract metadata
				elseif node.pSubtype == 31067
					status = ._GetContractFileData(prgCtx, node, csvVals)
					if !status.ok
						return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
					end		
		
			
				// *** Case Type metadata
				elseif node.pSubtype in $DataManager.ObjectTypes.GetCaseTypes(prgCtx)
					status = ._GetBasicCaseData(prgCtx, node, csvVals)
					if !status.ok
						return .SetErrObj(.InitErrObj( "GatherMetadata" ), status)
					end			
				end	
			end	
		
			Assoc 		rtn
			rtn.ok = true
			rtn.result = csvVals
			return rtn
		
		end

	
	public function Dynamic New(	Object prgCtx, \
										Assoc options, \
										String filePath = undefined, \
										List colNames = {},\		// pre-initialized colNames
										Boolean convertToString = true, \
										List filterCatIds = {}, \	// filtered categoryIds 
										String outputType = 'csv') 	// csv or xml or json (only csv supported right now)
			
			Frame 	f = ._NewFrame()
		
			f._fOutputType = outputType
			f._fFilePath = filePath
		
			// *** ConstantVals is empty (holdover from old BE)
			f._fConstantVals = options.constantVals
		
			// *** convert the values to Strings?	
			f._fConvertToString = convertToString
			
			// *** flags to tell us what data to export
			f._fMetadataOnly = options.metadataOnly
			f._fWantNodeIdCols = options.wantNodeId
			f._fWantBasicData = options.wantBasicData
			f._fWantVersionData = options.wantVersionData
			f._fWantClassifications = options.wantClassifications
			f._fWantRMData = options.wantRMData
			f._fWantCategoryData= options.wantCategoryData
			f._fWantSystemAtts = options.wantSystemAtts
			f._fWantEmails = options.wantEmails
			f._fWantPhysObj = options.wantPhysObj
			f._fWantAllVersions = options.wantAllVersions
			f._fWantPerms = options.wantPerms
			f._fWantOwner = options.wantOwner
			f._fWantPhysObjCirc = options.wantPhysObjCirc
			f._fWantPhysObjOther = options.wantPhysObjOther
			f._fWantContractData = options.wantContractData
			f._fWantRenditions = options.wantRenditions
			
			// *** allow for category id filter
			f._fFilterCatIds = filterCatIds
			
			// *** system attribute colNames
			f._fSystemAttrColNames = $DataManager.SystemAttributes.GetAllAttrNames(prgCtx)
		
			// *** initialize the columnNames
			f._InitColNames(colNames)	
		
			// *** initialize empty vals
			f._InitEmptyVals()
				
			return f
			
		end

	
	public function Assoc WriteMetadata(Object prgCtx, \
											DAPINODE node, \
											DAPIVERSION version, \
											String sourcePath, \
											String fileName = "")		/* KM 6-13-16 removed ability to pass in object name. Now must always equal the node.pName */
											
			Assoc 	status
			Boolean isStart = false
			Assoc	csvVals
			Assoc	rtn = .InitErrObj("WriteMetadata")
		
			if IsUndefined(._fFileWriter)
				._fFileWriter = $DataManager.FileWriter.New("json", ._fFilePath)
				
				// *** start the file
				status = ._fFileWriter.StartFile()
				if !status.ok
					return .SetErrObj(.InitErrObj("WriteMetadata"), status)
				end
				isStart	 = true	
			end	
		
		
			// *** get the metadata
			status = .GatherMetadata(prgCtx, node, version, sourcePath, fileName)
			if !status.ok
				return .SetErrObj(.InitErrObj("WriteMetadata"), status)
			else
				csvVals = status.result	
			end
			
			// *** a note of explanation: the Java library we are using to transform the json to csv gathers the column
			// *** names from the first row of json data. So, we need to have all column names in the first row 
			if isStart
				
				._fFirstRowData = csvVals
		
			else 	
		     	String jsonStr = ._fFirstRowWritten ? "," : ""    
		      	jsonStr += $DataManager.SynCoreInterface.toJSON(csvVals) 
		      	._fFirstRowWritten = true 
		      	._fFileWriter.Write(jsonStr) 			
			end
			
			return rtn
		
		end

	
	private function String _FormatCatVal(Dynamic val, Integer attType = undefined)
											
			if Type(val) == ListType
				String listStr
				Dynamic v
				for v in val
					listStr += ._FormatCatVal(v, attType) + ._fMVDelimiter
				end		
		
				listStr = listStr[:-2]
				
				return listStr
		
			elseif Type(val) == DateType
				return ._DateToString(val)
				
			elseif Type(val) == StringType
		
				// *** replace any carriage returns and escape any double quotes -- only necessary for multiline attribute type	
				if attType == 11
					val = Str.ReplaceAll(Str.ReplaceAll(val, Str.EOL(), '\r\n'), '"', '\"')
				end	
				
				return val
				
			else	
				return $DataManager.FormatPkg.ConvertValToString(val)
			end	
		
		end

	
	private function Assoc _GetBasicCaseData(Object prgCtx, \
									  DAPINODE node, \
									  Assoc csvVals)
		
			Assoc extData = node.pExtendedData
			
			// *** RefNR
			if IsDefined(extData) && IsDefined(extData.name) && IsFeature(extData.name, "refnr")
				csvVals.("$CMREFNR") = extData.name.refnr
			end		
		
			// **** Case type	
			if IsDefined($CMBase)
				Assoc 		result = $CMBase.Utils.GetCaseConfig( node.pID, node )
				if result.ok && IsDefined(result.content)
					Assoc 		caseConfig = result.content
					Assoc 		caseType = $CMBase.NodeUtils.GetCMType( prgCtx, node, caseConfig )	
					csvVals.("$CMCONTRACTTYPE") = caseType.contracttype
				end	
			end
			
			Assoc rtn
			rtn.ok = true
		
			return rtn
		
		end

	
	private function Assoc _GetBinderData(Object prgCtx, \
											  DAPINODE node, \
											  Assoc csvVals)
		
			return ._GetBasicCaseData(prgCtx, node, csvVals)
			
		end

	
	private function Assoc _GetCategoryVals(Object prgCtx, \
											  DAPINODE node, \
											  DAPIVERSION version, \
											  Assoc csvVals)
		
			Assoc	vals
			Assoc	status
			Assoc	att, childAtt
			List	eachSet
			String	key
			Assoc	rtn
			
			// *** if we want all category data, then pass in undefined for the filter so we won't waste time checking
			List filteredCatIds = ._fWantCategoryData ? undefined : ._fFilterCatIds
			
			// *** get the category values
			Boolean convertUserIdToName = true
			status = $DataManager.CategoryAPI.GetNodeCategoryVals(prgCtx, node, version, convertUserIdToName, filteredCatIds)
			if status.ok
				vals = status.result
				
				// *** data looks like this:
				// *** vals.:Livelink Categories:BulkLoadDemo3.A Test Set.children[ 1 ]
				String catPath
				
				for catPath in Assoc.Keys(vals)
					key = catPath + ':IsAttachedCat'
					csvVals.(key) = '1'
					
					// *** also add in a blank if necessary into the first row vals for attached Cat
					if IsDefined(._fFirstRowData) && !IsFeature(._fFirstRowData, key)
						._fFirstRowData.(key) = ""
					end			
					
					for att in vals.(catPath)
						if att.type == $AttrTypeSet
							Integer i = 1
							for eachSet in att.Children
								for childAtt in eachSet
									key = Str.Format("%1:%2[%3]:%4", catPath, att.Name, i, childAtt.Name)
									csvVals.(key) = ._FormatCatVal(childAtt.vals, childAtt.Type)
									
									// *** also add in a blank if necessary into the first row vals
									if IsDefined(._fFirstRowData) && !IsFeature(._fFirstRowData, key)
										._fFirstRowData.(key) = ""
									end
									
								end	
								i+=1
							end	
						else
							key = Str.Format("%1:%2", catPath, att.Name)
							csvVals.(key) = ._FormatCatVal(att.vals, att.Type)
							
							// *** also add in a blank if necessary into the first row vals
							if IsDefined(._fFirstRowData) && !IsFeature(._fFirstRowData, key)
								._fFirstRowData.(key) = ""
							end					
							
						end
					end
				end
				
				
			else
				rtn = .InitErrObj("_GetCategoryVals")
				.SetErrObj(rtn, status)
				return rtn
			end	
			
			rtn.ok = true
			return rtn
			
		end

	
	private function String _GetClassificationPathStr(Object prgCtx, DAPINODE node)
											
			String	s = ''
			
			// *** KM 6-2-16 fix case for case senstive SQL Server
			if IsDefined($Classification)
				String 	stmt = "select llc.*, d.Subtype from LLClassify llc, DTree d where llc.ID=:A1 and d.DataID=llc.CID and d.Subtype=:A2 and llc.Status=:A3"	
				Dynamic result = .ExecSql(prgCtx, stmt, {node.pID, $TypeClassification, "accepted"})
				Record rec
				
				if IsNotError(result) && Length(result)
					for rec in result
						s += ._NodeIdToPath(prgCtx, rec.CID) + "|" 
					end	
					s = s[1:-2]
				end	
			end	
			
			return s
		end

	
	private function Assoc _GetContractFileData(Object prgCtx, DAPINODE node,  Assoc csvVals)
											
			// *** get basic data
			Assoc status = ._GetBasicCaseData(prgCtx, node, csvVals)
			if !status.ok
				// *** return .SetErrObj(.InitErrObj( "_GetContractFileData" ), status)
			end		
			
			// *** get duration info
			status = $ContractManagement.DurationUtils.GetDurationByDataID( prgCtx, node.pID )
							
			if status.ok
							
				Assoc durationAssoc = status.duration
				  
				
				if IsFeature(._fEmptyVals, "$CFEFFECTIVE") && IsDefined(durationAssoc.effective)	
					csvVals.('$CFEFFECTIVE') = ._fConvertToString ? ._DateToString(durationAssoc.effective) : durationAssoc.effective
				end
				
				if IsFeature(._fEmptyVals, "$CFEARLYTERMINATION") && IsDefined(durationAssoc.earlytermination)	
					csvVals.('$CFEARLYTERMINATION') = ._fConvertToString ? ._DateToString(durationAssoc.earlytermination) : durationAssoc.earlytermination
				end	
				
				if IsFeature(._fEmptyVals, "$CFCURRENTEND") && IsDefined(durationAssoc.currentend)	
					csvVals.('$CFCURRENTEND') = ._fConvertToString ? ._DateToString(durationAssoc.currentend) : durationAssoc.currentend
				end	
				
				if IsFeature(._fEmptyVals, "$CFTERMINATED") && IsDefined(durationAssoc.terminated)	
					csvVals.('$CFTERMINATED') = ._fConvertToString ?  Str.String(durationAssoc.terminated) : durationAssoc.terminated
				end							
			
						
			end
		
			// *** get master agreement
			Object webNode = $WebNode.WebNodes.GetItem(node.pSubtype)
			if IsFeature(webNode, "_BrowseGetMasterAgreement")
				status = webNode._BrowseGetMasterAgreement(prgCtx, node)
				if status.ok && IsDefined(status.masterAgreement)
					csvVals.("$CFMASTERAGREEMENT") = status.masterAgreement.NAME	
				end	
			end
				
			
			Assoc rtn
			rtn.ok = true
			
			return rtn
				
		
		end

	
	private function Assoc _GetEmailVals(Object prgCtx, DAPINODE node, Assoc csvVals)
											
			// *** call the OTEMAIL utility to get the properties
			CAPICONNECT	capiConn	= prgCtx.fDBConnect.fConnection	// capi connection variable
			Assoc status = $OTEMAIL.OTEmailUtils.doGetProperties2(prgCtx, capiConn, node.pId)
			if !status.ok
				return .SetErrObj(.InitErrObj("_GetEmailVals"), status)
			else
		
				// *** stuff that's stored in the top Assoc		
				Assoc 	emailData = status.ReturnProperties
				csvVals.('$EmailHasAttachments') = ._fConvertToString ? Str.String(emailData.ExtendedData.OTEmailHasAttachments) : emailData.ExtendedData.OTEmailHasAttachments
				csvVals.('$EmailVersion') = ._fConvertToString ? Str.String(emailData.Version) : emailData.Version
				csvVals.('$EmailParticipants') = ._fConvertToString ? Str.String(emailData.Participants) : emailData.Participants
				
				if IsUnDefined(emailData.BodyFormat) 
					emailData.BodyFormat = 1
				end	
				csvVals.('$EmailBodyFormat') = ._fConvertToString ? Str.String(emailData.BodyFormat) : emailData.BodyFormat
				
				// *** sub-assoc "Properties" properties
				Assoc	props = emailData.Properties
				csvVals.('$EmailSubject') = IsDefined(props.OTEmailSubject) ? props.OTEmailSubject : ''
				csvVals.('$EmailTo') = IsDefined(props.OTEmailTo) ? props.OTEmailTo : ''
				csvVals.('$EmailFrom') = IsDefined(props.OTEmailFrom) ? props.OTEmailFrom : ''
				csvVals.('$EmailOnBehalfOf') = IsDefined(props.OTEmailOnBehalfOf) ? props.OTEmailOnBehalfOf : ''
				csvVals.('$EmailCC') = IsDefined(props.OTEmailCC) ? props.OTEmailCC : ''
				csvVals.('$EmailBCC') = IsDefined(props.OTEmailBCC) ? props.OTEmailBCC : ''
				csvVals.('$EmailReceivedDate') = ._fConvertToString ? ._DateToString(props.OTEmailReceivedDate) : props.OTEmailReceivedDate
				csvVals.('$EmailSentDate') = ._fConvertToString ? ._DateToString(props.OTEmailSentDate) : props.OTEmailSentDate
				
			end	
		
		
			Assoc rtn
			rtn.ok = true
			return rtn	
		
		end

	script _GetNodeVals
	
																								
				function Assoc _GetNodeVals(Object prgCtx, \
											DAPINODE node, \
											DAPIVERSION version, \
											Assoc csvVals)
				
					
					// **** Basic Data fields
					if IsFeature(._fEmptyVals, "$CreateDate")
						csvVals.('$CreateDate') = ._fConvertToString ? ._DateToString(node.pCreateDate) : node.pCreateDate
					end	
					
					if IsFeature(._fEmptyVals, "$ModifyDate")	
						csvVals.('$ModifyDate') = ._fConvertToString ? ._DateToString(node.pModifyDate) : node.pModifyDate
					end
					
					if IsFeature(._fEmptyVals, "$Creator")		
						csvVals.('$Creator') = ._fConvertToString ? ._UserNameFromID(prgCtx, node.pCreatedBy) : node.pCreatedBy
					end	
				
					if IsFeature(._fEmptyVals, "$Description")	
						// *** replace any double quotes with single quotes and end of lines with space
						csvVals.('$Description') = IsDefined(node.pComment) ? Str.ReplaceAll(Str.ReplaceAll(node.pComment, Str.EOL(), '\r\n'), '"', "'") : ''
					end	
					
					if IsFeature(._fEmptyVals, "$NickName")	
						csvVals.('$NickName') = _GetNickName(prgCtx, node)
					end
					
					if IsFeature(._fEmptyVals, "$Catalog")		
						csvVals.('$Catalog') = ._fConvertToString ? Str.String(node.pCatalog) : node.pCatalog
					end	
				
					Assoc rtn
					rtn.ok	= true
					return rtn 
				
				end		
				
				function String _GetNickName( Object prgCtx, DAPINode node )
				
					String nickname
						
					if $NickName
						Assoc result = $NickName.NickNameUtils.FindNickName(prgCtx, node.pID)
						nickname = result.ok && IsDefined(result.NickName) ? result.nickname : ''
					end
						
					return nickname
					
				
				end
		
		
	
	endscript

	
	private function String _GetPermissionString(Object prgCtx, \
													DAPINODE node, \
													Assoc csvVals)
											
			String perms
			perms = ._PermMaskToString(node.pUserPerm) + ':$Owner' +._fMVDelimiter
			perms += ._PermMaskToString(node.pGroupPerm) + ':$Group' + ._fMVDelimiter
			perms += ._PermMaskToString(node.pWorldPerm) + ':$Public' + ._fMVDelimiter 
			
			Record right 
			RecArray rights = DAPI.GetNodeRights(node)
			if !IsError(rights) && length(rights)
				for right in rights
					Boolean includeUGPrefaceChar = true
					String name = ._UserNameFromID(prgCtx, right.rightID, includeUGPrefaceChar)
					if name
						perms += ._PermMaskToString(right.Permissions) +':' + name + ._fMVDelimiter 
					end		
				end	
			end
			
			// drop the ending MVdelimiter
			perms = perms[1:-2]		
		
			return perms
				
		end

	
	private function Assoc _GetPhysObjVals(Object prgCtx, DAPINODE node, Assoc csvVals)
									  
			Assoc 	status
			String	stmt
			Dynamic	result	
		
			// *** From PhysItemData
			stmt = "select * from PhysItemData where NodeID=:A1"
			result = .ExecSQL(prgCtx, stmt, {node.PId})
			if IsError(result)
				return .SetErrObj(.InitErrObj("GetPhysObjVals"), result)
			end
			
			/**** these are the CSV columns we need to populate
				'$Area'		-- string
				'$BoxId'	-- dataid convert to path PHYSITEMCO table
				'$ClientID' -- conver to username
				'$ClientName'	-- regular string field
				'$Facility'	-- string
				'$FromDate'		-- date, but in string format
				'$Keywords'		-- string
				'$Location'		-- string
				'$Locator'	-- string
				'$LocatorType'	-- string
				'$NumCopies'	-- integer PHYSITEMCO table		
				'$OffsiteStorID' -- string
				'$ReferenceRate' -- string
				'$PHYSITEMTYPE' -- dataid convert to name
				'$TemporaryID'	-- string
				'$ToDate'		-- date, but in string format
				'$UniqueID'		-- string	PHYSITEMCO table
			*/
			if Length(result)
				if IsFeature(._fEmptyVals, "$AREA") && IsDefined(result[1].AREA)
					csvVals.('$Area') =  result[1].AREA
				end
				if IsFeature(._fEmptyVals, "$Client")
					if IsDefined(result[1].CLIENTID)	
						csvVals.('$Client') =  ._fConvertToString ? ._UserNameFromID(prgCtx, result[1].CLIENTID) : result[1].CLIENTID
					elseif IsDefined(result[1].CLIENT)
						csvVals.('$Client') =  result[1].CLIENT
					end	
				end
				if IsFeature(._fEmptyVals, "$Facility") && IsDefined(result[1].FACILITY)
					csvVals.('$Facility') = result[1].FACILITY
				end
				if IsFeature(._fEmptyVals, "$FromDate") && IsDefined(result[1].FROMDATE)	
					csvVals.('$FromDate') = ._fConvertToString ? ._DateToString(result[1].FROMDATE) : result[1].FROMDATE
				end
				if IsFeature(._fEmptyVals, "$Keywords") && IsDefined(result[1].PHYSITEMKEYWORDS)
					csvVals.('$Keywords') = result[1].PHYSITEMKEYWORDS
				end
				if IsFeature(._fEmptyVals, "$HomeLocation") && IsDefined(result[1].DEFAULTLOCATION) 
					csvVals.('$HomeLocation') = result[1].DEFAULTLOCATION
				end
				if IsFeature(._fEmptyVals, "$Locator") && IsDefined(result[1].BOXLOCATOR)	
					csvVals.('$Locator') = result[1].BOXLOCATOR
				end
				if IsFeature(._fEmptyVals, "$LocatorType") && IsDefined(result[1].LOCATORTYPE)	
					csvVals.('$LocatorType') = result[1].LOCATORTYPE
				end
				if IsFeature(._fEmptyVals, "$OffsiteStorID") && IsDefined(result[1].OFFSITESTORID) 
					csvVals.('$OffsiteStorID') = result[1].OFFSITESTORID
				end
				if IsFeature(._fEmptyVals, "$ReferenceRate") && IsDefined(result[1].REFRATE)
					csvVals.('$ReferenceRate') = result[1].REFRATE
				end	
				
				if IsFeature(._fEmptyVals, "$PHYSITEMTYPE") && IsDefined(result[1].MEDIATYPEID)
					if ._fConvertToString
						// *** special handling for media type
						status = $DataManager.PoMediaTypes.GetMediaTypeNameFromId(prgCtx, result[1].MEDIATYPEID)
						if !status.ok
							return .SetErrObj(.InitErrObj("GetPhysObjVals"), status)
						else
							csvVals.('$PHYSITEMTYPE') = status.result
						end
					else
						csvVals.('$PHYSITEMTYPE') = result[1].MEDIATYPEID
					end	
				end	
					
				if IsFeature(._fEmptyVals, "$ToDate") && IsDefined(result[1].TODATE)
					csvVals.('$ToDate') = ._fConvertToString ? ._DateToString(result[1].TODATE) : result[1].TODATE
				end
				if IsFeature(._fEmptyVals, "$TemporaryID") && IsDefined(result[1].TEMPORARYID)
					csvVals.('$TemporaryID') = result[1].TEMPORARYID
				end	
				
				if IsFeature(._fEmptyVals, "$TransferID") && IsDefined(result[1].TRANSFERID)
					csvVals.('$TransferID') = result[1].TRANSFERID
				end			
			end
		
			// **** From PhysItemCo
			// **** KM 3-2-18 Fix for case-sensitive Sql Server
			stmt = "select * from PhysItemCO where NodeID=:A1"
			result = .ExecSQL(prgCtx, stmt, {node.PId})
			if IsError(result)
				return .SetErrObj(.InitErrObj("GetPhysObjVals"), result)
			end	
			if Length(result)
			
				if IsFeature(._fEmptyVals, "$NumCopies") && IsDefined(result[1].COPYNUMBER)
					csvVals.('$NumCopies') = ._fConvertToString ? Str.String(result[1].COPYNUMBER) : result[1].COPYNUMBER
				end
				if IsFeature(._fEmptyVals, "$UniqueID") && IsDefined(result[1].UNIQUEID)	
					csvVals.('$UniqueID') = result[1].UNIQUEID
				end
				if IsFeature(._fEmptyVals, "$BoxID") && IsDefined(result[1].BOXID)	
					csvVals.('$BoxID') = result[1].BOXID
				end	
				if IsFeature(._fEmptyVals, "$Box") && IsDefined(result[1].BOXID)	
					csvVals.('$Box') = ._NodeIDToPath(prgCtx, result[1].BOXID)
				end	
		
				// *** circulation info
				if ._fWantPhysObjCirc	
			
					if IsFeature(._fEmptyVals, "$BorrowedDate") && IsDefined(result[1].CHARGEDATE)			
						csvVals.('$BorrowedDate') = ._fConvertToString ? ._DateToString(result[1].CHARGEDATE) : result[1].CHARGEDATE
					end	
			
					if IsFeature(._fEmptyVals, "$BorrowedBy") && IsDefined(result[1].USERID)	
						csvVals.('$BorrowedBy') = ._fConvertToString ? ._UserNameFromID(prgCtx, result[1].USERID) : result[1].USERID
		
					elseif IsFeature(._fEmptyVals, "$BorrowedBy") && IsDefined(result[1].CHARGEDTO)	
						csvVals.('$BorrowedBy') = result[1].CHARGEDTO					
					end
						
					if IsFeature(._fEmptyVals, "$ObtainedBy") && IsDefined(result[1].OBTAINEDBYID)				
						csvVals.('$ObtainedBy') = ._fConvertToString ? ._UserNameFromID(prgCtx, result[1].OBTAINEDBYID) : result[1].OBTAINEDBYID
		
					elseif IsFeature(._fEmptyVals, "$ObtainedBy") && IsDefined(result[1].OBTAINEDBY)				
						csvVals.('$ObtainedBy') = result[1].OBTAINEDBY			
		
					end
			
					if IsFeature(._fEmptyVals, "$BorrowComment") && IsDefined(result[1].BORROWCOMMENT)
						csvVals.('$BorrowComment') = result[1].BORROWCOMMENT			
					end
					
					if IsFeature(._fEmptyVals, "$ReturnDate") && IsDefined(result[1].RETURNBY)			
						csvVals.('$ReturnDate') = ._fConvertToString ? ._DateToString(result[1].RETURNBY) : result[1].RETURNBY
					end
			
					if IsFeature(._fEmptyVals, "$CurrentLocation") && IsDefined(result[1].LOCATION)				
						csvVals.('$CurrentLocation') = result[1].LOCATION
					end
					
				end
			end
		
			if IsFeature(._fEmptyVals, "$MediaTypeFields")
				// **** From PhysItemExt	
				Object formatPkg = $LLIAPI.FormatPkg
				String valStr  = ''
				stmt = "select * from PhysItemExt where NodeID=:A1"
				result = .ExecSQL(prgCtx, stmt, {node.pId})
				if !IsError(result) && Length(result)
					Record rec
					for rec in result
						valStr += rec.FIELDNAME + "=" + formatPkg.ValToString(rec.FIELDVALUE) + "|"
					end
					csvVals.('$MediaTypeFields') = IsDefined(valStr) && Length(valStr) ? valStr[1:-2] : valStr	
				end	
			end
		
			Assoc rtn
			rtn.ok = true
			return rtn
		
		end

	script _GetRecManVals
	
																								
				function Assoc _GetRecManVals(Object prgCtx, \
											  DAPINODE node, \
											  Assoc csvVals)
				
					
					// *** retrieve this from the db cache
					Assoc status = $DataManager.NodeUtils.GetRecManData(prgCtx, node)
					if !status.ok	
						return .SetErrObj(.InitErrObj("_GetRecManVals"), status)
					end
					
					// *** parse the result into the csvVals Assoc
					Assoc result = status.result
					if IsUndefined(result) || Length(Assoc.Keys(result))==0
						// *** there is just no rec man data there -- return success
						Assoc rtn
						rtn.ok = true
						return rtn		
					else
						if IsFeature(._fEmptyVals, "$RDAccession") && IsDefined(result.RIMSACCESSION)
							csvVals.('$RDAccession') = result.RIMSACCESSION
						end
						
						if IsFeature(._fEmptyVals, "$RDAddressee") && IsDefined(result.RIMSADDRESSEE)
							csvVals.('$RDAddressee') = result.RIMSADDRESSEE
						end	
						
						if IsFeature(._fEmptyVals, "$RDAuthor") && IsDefined(result.RIMSORIGINATOR)
							csvVals.('$RDAuthor') = result.RIMSORIGINATOR
						end	
						
						if IsFeature(._fEmptyVals, "$RDEssential") && IsDefined(result.RIMSESSENTIAL)
							csvVals.('$RDEssential') = result.RIMSESSENTIAL
						end	
						
						if IsFeature(._fEmptyVals, "$RDLastReviewDate") && IsDefined(result.RIMSLASTREVIEWDATE)
							csvVals.('$RDLastReviewDate') = ._fConvertToString ? ._DateToString(result.RIMSLASTREVIEWDATE) : Date.StringToDate(Str.String(result.RIMSLASTREVIEWDATE), '%Y%m%d')
						end
							
						if IsFeature(._fEmptyVals, "$RDNextReviewDate") && IsDefined(result.RIMSNEXTREVIEWDATE)
							csvVals.('$RDNextReviewDate')  = ._fConvertToString ? ._DateToString(result.RIMSNEXTREVIEWDATE) : Date.StringToDate(Str.String(result.RIMSNEXTREVIEWDATE), '%Y%m%d')
						end	
						
						if IsFeature(._fEmptyVals, "$RDOfficial") && IsDefined(result.RIMSOFFICIAL)
							csvVals.('$RDOfficial') = ._fConvertToString ? Str.String(result.RIMSOFFICIAL) : result.RIMSOFFICIAL
						end	
						
						if IsFeature(._fEmptyVals, "$RDOrganization") && IsDefined(result.RIMSESTABLISHMENT)
							csvVals.('$RDOrganization') = result.RIMSESTABLISHMENT
						end
							
						if IsFeature(._fEmptyVals, "$RDOtherAddressee") && IsDefined(result.RIMSSENTTO)
							csvVals.('$RDOtherAddressee') = result.RIMSSENTTO
						end
							
						if IsFeature(._fEmptyVals, "$RDReceivedDate") && IsDefined(result.RIMSRECEIVEDDATE)
							csvVals.('$RDReceivedDate') = ._fConvertToString ? ._DateToString(result.RIMSRECEIVEDDATE) : Date.StringToDate(Str.String(result.RIMSRECEIVEDDATE), '%Y%m%d')
						end	
						
						if IsFeature(._fEmptyVals, "$RDRecordDate") && IsDefined(result.RIMSDOCDATE)
							csvVals.('$RDRecordDate') = ._fConvertToString ? ._DateToString(result.RIMSDOCDATE) : Date.StringToDate(Str.String(result.RIMSDOCDATE), '%Y%m%d')
						end	
						
						if IsFeature(._fEmptyVals, "$RDRSI") && IsDefined(result.RIMSRSI)
							csvVals.('$RDRSI') = result.RIMSRSI
						end									
																	
						if IsFeature(._fEmptyVals, "$RDStatus") && IsDefined(result.RIMSSTATUS)
							csvVals.('$RDStatus') = result.RIMSSTATUS
						end	
						
						if IsFeature(._fEmptyVals, "$RDStatusDate") && IsDefined(result.RIMSSTATUSDATE)
							csvVals.('$RDStatusDate') = ._fConvertToString ? ._DateToString(result.RIMSSTATUSDATE) : Date.StringToDate(Str.String(result.RIMSSTATUSDATE), '%Y%m%d') 
						end	
						
						if IsFeature(._fEmptyVals, "$RDStorageMedium") && IsDefined(result.RIMSSTORAGE)
							csvVals.('$RDStorageMedium') = result.RIMSSTORAGE
						end	
						
						if IsFeature(._fEmptyVals, "$RDSubject") && IsDefined(result.RIMSSUBJECT)
							csvVals.('$RDSubject') = result.RIMSSUBJECT
						end	
						
						if IsFeature(._fEmptyVals, "$RDUpdateCyclePeriod") && IsDefined(result.RIMSCYCLEPERIOD)
							csvVals.('$RDUpdateCyclePeriod') = ._fConvertToString ? _GetUpdateCycleName(result.RIMSCYCLEPERIOD) : result.RIMSCYCLEPERIOD
						end	
						
						if IsFeature(._fEmptyVals, "$RDClassification") && IsDefined(result.CID)
							// *** convert classid to path
							csvVals.('$RDClassification') = ._fConvertToString ? ._NodeIdToPath(prgCtx, result.CID) : result.CID
						end	
						
						// *** get the file number	
						if IsFeature(._fEmptyVals, "$FileNumber") && IsDefined(result.CID)
							csvVals.('$FileNumber') = _GetFileNumber(prgCtx, result.CID)
						end	
					end
				
					Assoc rtn
					rtn.ok = true
					return rtn
				
				end
				
				Function String _GetFileNumber(Object prgCtx, Integer classId, String fileNumberStr = '')
				
				
				
					// *** get file number for this class
					String	stmt = 'select FileNumber from RM_CLASSIFICATION where NodeID=:A1'
					Dynamic result = .ExecSQL(prgCtx, stmt, {classId})
					if IsNotError(result) && Length(result)
						fileNumberStr = result[1].FILENUMBER + fileNumberStr		
					else
						return fileNumberStr	
					end
				
					DAPINODE	classNode = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, classId)
					if IsNotError(classNode)
						fileNumberStr = _GetFileNumber(prgCtx, classNode.pParentId, fileNumberStr)				
					end
				
					return fileNumberStr
				
				end
				
				function String _GetUpdateCycleName(Integer num)
				
					String s = ''
				
					switch num
				
						case 1
							s = "Monthly"
						end
						
						case 2
							s = "Semi-Annual"
						end
						
						case 3
							s = "Quarterly"
						end
						
						case 7
							s = "Weekly"
						end
						
						case 12
							s = "Annual"
						end		
						
						case 365
							s = "Daily"
						end	
						
						default
							s = "None"
						end
					
					end
										
					return s						
				
				end
			
		
	
	endscript

	
	private function Assoc _GetRenditionVals(Object prgCtx, \
												DAPINODE node, \
												DAPIVERSION version, \
												Assoc csvVals)
		
		
			if IsFeature(._fEmptyVals, "$RENDITIONTYPE") && IsDefined($Rendition)			
				csvVals.('$RENDITIONTYPE') = IsDefined(version.pType) ? version.pType : ''
			end	
		
			Assoc rtn
			rtn.ok	= true
			return rtn 
				
		end

	
	private function Assoc _GetVersionVals(Object prgCtx, \
												DAPINODE node, \
												DAPIVERSION version, \
												Assoc csvVals)
				
			if IsFeature(._fEmptyVals, "$VERSIONNUM")	
				if IsDefined(version)
					csvVals.('$VERSIONNUM') = Str.String(version.pNumber)
			
				elseif node.pSubType == $ApplTypeDocument
					csvVals.('$VERSIONNUM') = Str.String(node.pVersionNum)
				end	
			end	
	
			if IsFeature(._fEmptyVals, "$VERSIONID") 
				csvVals.('$VERSIONID') = Str.String(version.pID)
			end													
									
			if IsFeature(._fEmptyVals, "$VERFILESIZE")
				csvVals.('$VERFILESIZE') = ._fConvertToString ? Str.String(version.pFileDataSize) : version.pFileDataSize
			end
			
			if IsFeature(._fEmptyVals, "$VERCREATEDATE")			
				csvVals.('$VERCREATEDATE') = ._fConvertToString ? ._DateToString(version.pCreateDate) : version.pCreateDate
			end
			
			if IsFeature(._fEmptyVals, "$VERMODIFIEDDATE")			
				csvVals.('$VERMODIFIEDDATE') = ._fConvertToString ? ._DateToString(version.pModifyDate) : version.pModifyDate
			end
			
			if IsFeature(._fEmptyVals, "$VERDESCRIPTION")			
				csvVals.('$VERDESCRIPTION') = IsDefined(version.pComment) ? version.pComment : ''
			end
			
			if IsFeature(._fEmptyVals, "$VERFILENAME")			
				csvVals.('$VERFILENAME') = IsDefined(version.pFileName) ? version.pFileName : ''
			end
			
			if IsFeature(._fEmptyVals, "$VERMIMETYPE")			
				csvVals.('$VERMIMETYPE') = IsDefined(version.pMimeType) ? version.pMimeType : ''
			end	
		
			if IsFeature(._fEmptyVals, "$VERMAJOR")			
				csvVals.('$VERMAJOR') = IsDefined(version.pVerMajor) ? version.pVerMajor : ''
			end	
			
			if IsFeature(._fEmptyVals, "$VERMINOR")			
				csvVals.('$VERMINOR') = IsDefined(version.pVerMinor) ? version.pVerMinor : ''
			end	
		
			if IsFeature(._fEmptyVals, "$RENDITIONTYPE") && IsDefined($Rendition)			
				csvVals.('$RENDITIONTYPE') = IsDefined(version.pType) ? version.pType : ''
			end	
			
			if IsFeature(._fEmptyVals, "$VERCREATOR")		
				csvVals.('$VERCREATOR') = ._fConvertToString ? ._UserNameFromID(prgCtx, version.pOwner) : version.pOwner
			end	
			
			// *** always put in MajorMinor flag
			Object	dapiCtx = prgCtx.DSession()
			Boolean	isAdvVersioning =  $LLIAPI.LLNodeSubsystem.GetItem(node.pSubtype).IsMajorMinorNode( dapiCtx, node )
			csvVals.('$ADVVERSIONING') = isAdvVersioning
			
			// *** for versions, we will add by default the $ALLOWBREAKBEFORE directive (as false) to ensure that versions get added in order
			csvVals.('$ALLOWBREAKBEFORE') = FALSE
		
			Assoc rtn
			rtn.ok	= true
			return rtn 
				
		end

	
	private function void _InitColNames(List preSelectedColNames = {})
												
			if Length(preSelectedColNames)
												
				._fColNames = preSelectedColNames
													
				// *** versions	
				if ._fWantAllVersions || ._fWantVersionData
					._fColNames = {@._fColNames, @._fVersionColNames}
					._fColNames = {@._fColNames, @._fRenditionColNames}	
					._fColNames = List.SetAdd(._fColNames, '$ADVVERSIONING')
					._fColNames = List.SetAdd(._fColNames, '$ALLOWBREAKBEFORE')
	
					if !._fMetadataOnly
						._fColNames = List.SetAdd(._fColNames, '$EXPORTED_FILENAME')
					end
				end			
														
													
			else
													
				// *** initialize only the most basic names
				if ._fWantNodeIdCols
					._fColNames = ._fAlwaysColNames
				end	
													
				// ***  basic data?
				if ._fWantBasicData
					._fColNames = {@._fColNames, @._fBasicColNames}
				end	
													
				// *** What about system attributes
				if ._fWantSystemAtts && Length(._fSystemAttrColNames)
					._fColNames = {@._fColNames, @._fSystemAttrColNames}		
				end		
													
				// *** Source Path?
				if !._fMetadataOnly
					._fColNames = {@._fColNames, '$SOURCEPATH'}
				end		
													
				// *** Classification
				if ._fWantClassifications
					._fColNames = {@._fColNames, "$CLASSIFICATION"}
				end	
													
				// *** Perms
				if ._fWantPerms
					._fColNames = {@._fColNames, @._fPermColNames}
				end
													
				// *** Owner
				if ._fWantOwner
					._fColNames = {@._fColNames, @._fOwnerColNames}
				end	
												
				// *** versions	
				if ._fWantAllVersions || ._fWantVersionData
					._fColNames = {@._fColNames, @._fVersionColNames}
					._fColNames = {@._fColNames, @._fRenditionColNames}
					._fColNames = List.SetAdd(._fColNames, '$ADVVERSIONING')
					._fColNames = List.SetAdd(._fColNames, '$ALLOWBREAKBEFORE')
					if !._fMetadataOnly
						._fColNames = List.SetAdd(._fColNames, '$EXPORTED_FILENAME')
					end													
				end	
													
				// *** renditions	
				if ._fWantRenditions
					._fColNames = {@._fColNames, @._fRenditionColNames}
					if !._fMetadataOnly
						._fColNames = List.SetAdd(._fColNames, '$EXPORTED_FILENAME')
					end													
				end			
												
				// *** get Rec Man attributes if user asked for any
				if IsDefined($RecMan) && ._fWantRmData
					._fColNames = {@._fColNames, @._fRecManColNames}
				end	
												
				// *** get email properties if it's an email
				if IsDefined($OTEmail) && ._fWantEmails
					._fColNames = {@._fColNames, @._fEmailColNames}
				end	
												
				// *** get Physical Object data if user asked for any
				if IsDefined($PhysicalObjects) && ._fWantPhysObj
					._fColNames = {@._fColNames, @._fExportedPOSpecTabColNames}
					._fColNames = {@._fColNames, "$PHYSITEMTYPE"}
				end	
												
				// *** get Physical Object Circ data if user asked for any
				if IsDefined($PhysicalObjects) && ._fWantPhysObjCirc
					._fColNames = {@._fColNames, @._fExportedPOCircColNames}
				end	
												
				// *** get Physical Object Circ data if user asked for any
				if IsDefined($PhysicalObjects) && ._fWantPhysObjOther
					._fColNames = {@._fColNames, @._fExportedPOOtherColNames}
				end	
													
				// *** set contract file columns
				if IsDefined($ContractManagement) && ._fWantContractData
					._fColNames = {@._fColNames, @._fContractColNames}
				end
												
			end
												
		end

	
	private function String _PermMaskToString( Integer mask )
		
			String s 
			
			if mask & $PSee 
				s += 'S'
			end
				
			if mask & $PSeeContents
				s += 'C'
			end
				
			if mask & $PModify
				s += 'M'
									
			end
			
			if mask & $PCreateNode
				s += 'A'
			end
					
			if mask & $PEditAtts
				s += 'E'
			end	
					
							
			if mask & $PDeleteVersions
				s += 'V'
			end
							
			if mask & $PDelete
				s += 'D'
			end
							
			if mask & $PCheckOut
				s += 'R'
			end					
				
			if mask & $PEditPerms
				s += 'P'
			end	
								
			return s				
							
		end

	
	private function Dynamic _Round(Real n, Integer numDecPlaces)
											
			Integer r	
											
			switch numDecPlaces
												
				case 0
					return Math.Round(n)
				end
												
				case 1
					n = n * 10.0 
					r = Math.Round(n)
					n = (r*1.0)/10.0
				end
													
				case 2
					n = n * 100.0 		
					r = Math.Round(n)
					n = (r*1.0)/100.0			
				end
													
				case 3
					n = n * 1000.0 		
					r = Math.Round(n)
					n = (r*1.0)/1000.0			
				end
													
				case 4
					n = n * 10000.0 		
					r = Math.Round(n)
					n = (r*1.0)/10000.0			
				end
													
			end
											
			return n
											
		end

end

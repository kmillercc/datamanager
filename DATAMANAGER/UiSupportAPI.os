package DATAMANAGER

public object UiSupportAPI inherits DATAMANAGER::Utils

	
	public function Real GetAvgTime(Object prgCtx, String typeStr, String jobSubtype)
				
				String key = typeStr + "_" + jobSubtype + "_avgTime"
				
				Dynamic result = CAPI.IniGet( prgCtx.fDBConnect.fLogin, ._fOspaceName, key, 0.05)
				
				return result
													
			end

	
	public function Assoc GetCsvColumns(String csvPath)
												
				Assoc 	rtn = .InitErrObj( "GetCsvColumns" )
				Assoc	status
				List	colNames
				
				// *** load the csv
				status = $DataManager.CsvParser.GetHeader(csvPath)
				if status.ok
					
					colNames = status.result.colNames
					
					
				else
					.SetErrObj(rtn, status)
					return rtn
				end
			
				return .SuccessResult(colNames)
			
			end

	
	public function Assoc GetEnterprise(Object prgCtx)
													
				Assoc 	status = prgCtx.DSession().GetNodeLibrary()
				Assoc	retVal
						
				if status.ok
					DAPINODE entNode = status.Library
					retVal.name=entNode.pName
					retVal.ID=entNode.pID
				else
					retVal.name="Enterprise"
					retVal.ID=2000
				end
				
				return retVal
			end

	
	public function GetEnterpriseName(Object prgCtx)
												
				DAPINODE entNode = DAPI.GetNodeByID(prgCtx.DapiSess(), DAPI.BY_DATAID, 2000)
				
				if IsNotError(entNode)
					return entNode.pName	
				else
					return ""
				end	
			
			end

	
	public function Assoc GetFileTree(	String path, \
												Boolean	showFiles, \
												String filterExtension, \
												Integer limit, \
												Boolean doRecursive, \
												Boolean getMetadata, \
												String rootPath = path, \
												Assoc rootFileInfo = Assoc.CreateAssoc())							
												
				Assoc 	rtn = .InitErrObj( "GetFileTree" )
				Assoc	status
			
				// *** is this a metadata file or a directory?
				path = $DataManager.FileUtils.EnsureDirSeparator(path)
				
				// *** get a list of folders and optionally files
				status = ._GetFileList(path, limit, showFiles, filterExtension, doRecursive, getMetadata, rootPath, rootFileInfo)
				if status.ok
					return .SuccessResult(status.result)
				else
					.SetErrObj(rtn, status)
					return rtn
				end	
			
			end

	
	public function Assoc GetGroupTree(	Object prgCtx, \
												Integer groupId, \
												Integer limit, \
												String path, \
												Boolean	showUsers, \
												Boolean doRecursive = false)
												
				Assoc 		rtn = .InitErrObj( "GetGroupTree" )
				Assoc		status
				RecArray	recs
				Boolean		initialCall = groupId==-1
			
				if IsUndefined(groupId) || groupID == 0 || groupId == -1
					
					recs = UAPI.GetByID( prgCtx.USession().fSession, 1001 )
					if IsError(recs)
						.SetErrObj(rtn, "Unable to retrieve root virtual group.")
						return rtn
					end
					recs[1].NAME = "System Root"
					recs[1].ID = 0
					path = "System Root"
				else	
			
					recs = UAPI.GetByID( prgCtx.USession().fSession, groupId )
					if IsError(recs)
						.SetErrObj(rtn, Str.Format("Unable to retrieve group id %1", groupId))
						return rtn
					end	
				end
			
			
				// *** get a list of groups
				status = ._GetGroupList(prgCtx, recs[1], limit, path, showUsers, doRecursive, initialCall)
				if status.ok
					Assoc	groupInfo = status.result
				
					return .SuccessResult(groupInfo)
				else
					.SetErrObj(rtn, status)
					return rtn
				end	
			
			
			end

	
	public function Assoc GetJobStatusHtml(Object prgCtx, Integer scheduleId)	
												
				String	s
				Object	schedApi = $DataManager.SchedJobsAPI
				Dynamic	nextRunTime
				Dynamic	lastRunTime	
			
				// *** get item from JobsAPI
				Assoc status = schedApi.GetItem(prgCtx, scheduleId)
				if status.ok
					Assoc schedData = status.result
					nextRunTime  = schedData.NEXT_RUN_DATETIME
					lastRunTime  = schedData.LAST_RUN_DATETIME		
			
					if schedData.JOBSTATUS == schedApi.fSTATUS_RUNNING
						Object	job
						
						String jobType = "DataManager" + "_" + Str.Capitalize(schedData.action)
					
						status = $DataManager.JobInterface.GetActiveJobByExtId(prgCtx, jobType, scheduleId)
						if !status.ok
							// *** ok, looks like it was set to running, and never updated.  Must mean a stack trace
							s = schedApi.fSTATUS_UNKNOWN
							schedAPI.UpdateJobStatus(prgCtx, schedAPI.fSTATUS_UNKNOWN, scheduleId)
						else
							
							job = status.result
							Assoc payload = job.GetPayload()
							
							// *** KM 3/3/17 let's be a little more generous and set error if the last update occurred more than 15 minutes ago
							if job.GetLastUpdated() < (date.Now() - 900)
								s = schedApi.fSTATUS_UNKNOWN
								schedAPI.UpdateJobStatus(prgCtx, schedAPI.fSTATUS_UNKNOWN, scheduleId)
							else
							
								Integer total = Math.Max(1, payload.totalCount)
								Integer	completed = payload.completedCount	
					
								Integer percent = Math.Round(((completed * 1.0)/(total * 1.0)) * 100.0)
								String 	percentStr = Str.String(percent) + "%"
								
								if percent > 0
									s = Str.Format('<div class="progress"><div id="mybar" class="bar" style="width:%1;color:#ffffff;font-size:x-small">%1 Complete</div></div>', percentStr)
								else
									s = schedData.JOBSTATUS
								end
							end	
						end		
					else
						s = schedData.JOBSTATUS
					end	
			
				else
					s = 'Could not find job.'
				end
			
				Assoc result
				result.statusHtml = s
				result.nextRunTime = nextRunTime
				result.lastRunTime = lastRunTime		
				
				return .SuccessResult(result)
				
			end

	
	public function Assoc GetNodeTree(Object prgCtx, \
											Integer dataid, \
											Integer limit, \
											String path, \
											Boolean	showFiles, \
											Boolean doRecursive = false)
												
				Assoc 		rtn = .InitErrObj( "GetNodeTree" )
				DAPINODE 	node
				Assoc		status
				
				if dataid==1 || dataid==-1 || path == "System" || path=="System Root"
				
					Assoc nodeInfo
					nodeInfo.title = "System Root"
					nodeInfo.isFolder = true
					nodeInfo.dataid  = -1
					nodeInfo.children = {}
					nodeInfo.path = "System Root"
					nodeInfo.expand = true
				
				
					status = ._GetRootVolumeList(prgCtx)
					if status.ok
						Record childRec
						
						for childRec in status.result
							Assoc childInfo = Assoc.CreateAssoc()
							childInfo.isLazy = true
							childInfo.title = childRec.NAME
							childInfo.isFolder = true
							// only Enterprise and My Workspace are selectable
							childInfo.unselectable = childRec.SUBTYPE != 141 && childRec.SUBTYPE != 142 		
							childInfo.dataid  = childRec.DATAID
							childInfo.path  = nodeInfo.path + ":" + childRec.NAME
							nodeInfo.children = {@nodeInfo.children, childInfo}		
						end
				
						return .SuccessResult(nodeInfo)
					else
						.SetErrObj(rtn, status)
						return rtn
					end			
					
				// *** KM 3/8/17
				elseif dataid == $TypeFavoritesVolsVirtual
				
					// *** get a list of folders and optionally files
					status = ._GetMyFavoritesList(prgCtx, limit, path, showFiles, doRecursive)
					if status.ok
						return .SuccessResult(status.result)
					else
						.SetErrObj(rtn, status)
						return rtn
					end	
				
					
			
				else
				
					if IsUndefined(dataId)
					
						status = $DataManager.NodeUtils.GetNodeFromPath(prgCtx, path)
						if !status.ok
							.SetErrObj(rtn, status)
							return rtn
						else
							node = status.result	
						end
				
					else	
				
						// *** get the node
						node = DAPI.GetNodeById(prgCtx.DapiSess(), DAPI.BY_DATAID, dataId)	
						if IsError(node)
							.SetErrObj( rtn, Str.Format( "Node %1 must be a valid container", dataid) )	
							return rtn
						end	
				
					end	
			
					// *** get a list of folders and optionally files
					status = ._GetNodeList(prgCtx, node, limit, path, showFiles, doRecursive)
					if status.ok
						return .SuccessResult(status.result)
					else
						.SetErrObj(rtn, status)
						return rtn
					end	
				end			
			
			end

	
	public function Assoc GetRootNodeTree(Object prgCtx, Integer limit, Boolean showFiles)
			
			return .GetNodeTree(prgCtx, 1, limit, 'System Root', showFiles, false)
			
		end

	
	public function void SaveAvgTime(Object prgCtx, \
											String jobType, \
											String jobSubtype, \
											Real average)	
								
				String key = jobType + "_" + jobSubtype + "_avgTime"
				
				CAPI.IniPut( prgCtx.fDBConnect.fLogin, ._fOspaceName, key, average ) 
				
												
			end

	
	private function Assoc _GetFavoritesContents(Object		prgCtx )
												
			RecArray	contents
					
			// get the list of favorite tabs
			
			Assoc status = $LLIAPI.FavoriteUtil.ListFavorites( prgCtx )	
			
			if ( status.ok && IsFeature( status, 'data' ) && IsDefined( status.data ) )	
				/*
				List ids = RecArray.ColumnToList(status.data, 'DATAID')
			
				status = $LLIAPI.NodeUtil.ListNodesByIDs(\
									prgCtx,\
									ids,\
									'DataID',\
									UNDEFINED,\
									{},\
									'WebNodes' )
									
				if status.OK
				end
				 */
				contents = RecArray.Sort(status.data, "NAME")
			
			end	
		
			return Assoc{'ok': true, 'contents': contents}
					
		end

	script _GetFileList
	
			
						Function Assoc _GetFileList( String path, \
													Integer limit, \
													Boolean showFiles, \
													String	filterExtension, \
													Boolean doRecursive, \
													Boolean	getMetadata, \
													String rootPath, \
													Assoc rootFileInfo, \
													Object fileMetadataObj = $DataManager.FileMetadataObj.New())
						
							/* example of the output we need:
							{title: "Item 1"},
						    {title: "Folder 2", isFolder: true,
						            children: [
							                   {title: "Sub-item 2.1"},
						                       {title: "Sub-item 2.2"}
						                      ]
						    },
						    {title: "Item 3"}
							*/  
							
							Assoc 	rtn = .InitErrObj( "_GetFileList" )
							String	child
							Assoc	fileInfo
							Assoc	status
							Boolean	isRoot = path==rootPath
							
							
							Assoc	emptyMetadata = Assoc.CreateAssoc()
							emptyMetadata.data = {Assoc.CreateAssoc()}
							emptyMetadata.colNames = {}
							
							List	excludedFileNames = {"auditlog.log"}
							List	exludedFolderNames = {"_audit", "_audit" + File.Separator()}
							
							
							if (File.Exists( path ) && File.IsDirSpec( path )	)
								// *** get the title and path
								fileInfo.title = File.GetName( path )[1:-2]
								fileInfo.isFolder = true
								fileInfo.path  = path
								fileInfo.children = {}
									
								if getMetadata && !isRoot
									// *** look for metadata for this file
									status = fileMetadataObj.GetMetadataForFile(path, limit, rootPath)
									if status.ok
										fileInfo.metadata = status.result
										fileInfo.hasMetadata = true
									else
										// *** could not get metadata
										fileInfo.metadata = emptyMetadata
										fileInfo.hasMetadata = false
									end	
								else
									//fileInfo.metadata = emptyMetadata
								end
								
								// *** save root values so we can get the total num files from the UI
								if isRoot
									rootFileInfo = fileInfo
									rootFileInfo.total = 0
									rootFileInfo.isLazy = !doRecursive
									rootFileInfo.expand = true
								else
									fileInfo.itemNo = rootFileInfo.total
								end
								
								// *** add the num of children to the file list
								List	childList = File.FileList( path )
								childList = List.Sort(childList)
								
								// *** add the children
								for child in childList
									String fileName = File.GetName(child)
									
									if !(_IsExcluded(child, fileName, excludedFileNames, exludedFolderNames))
										if File.IsDirSpec(child) || showFiles
											
											// *** make sure we did not reach our limit
											if rootFileInfo.total == limit
												.SetErrObj( rtn, Str.Format( "Number of files exceeded the maximum limit of %1.", limit ) )	
												return rtn						
											end
											
											// *** add this file/folder to the total num of items
											rootFileInfo.total += 1
											
											if File.IsDirSpec(child) && doRecursive
												status = _GetFileList(child, limit, showFiles, filterExtension, doRecursive, getMetadata, rootPath, rootFileInfo, fileMetadataObj)
												if !status.ok
													.SetErrObj(rtn, status)
													return rtn
												else
													fileInfo.children = {@fileInfo.children, status.result}
												end	
											else
												Boolean isDirectory = File.IsDirSpec(child)
												
												if isDirectory || (IsUndefined(filterExtension) || Length(filterExtension) == 0 || Str.Upper(child[-4:-1]) == Str.Upper("." + filterExtension))
													Assoc childInfo = Assoc.CreateAssoc()
													childInfo.isLazy = isDirectory
													childInfo.title = isDirectory ? fileName[1:-2] : fileName
													childInfo.isFolder = isDirectory
													childInfo.unselectable = !isDirectory
													childInfo.path  = child
													childInfo.itemNo = rootFileInfo.total
													
													if getMetadata
														// *** look for metadata for this file
														status = fileMetadataObj.GetMetadataForFile(child, limit, rootPath)
														if status.ok
															childInfo.metadata = status.result
															childInfo.hasMetadata = true
														else
															// *** could not get metadata
															childInfo.metadata = emptyMetadata
														end
													else
														//childInfo.metadata = emptyMetadata
													end
													
													fileInfo.children = {@fileInfo.children, childInfo}							
												end	
											end	
										end	
									end					
								end
						
							else  // *** caller is asking for file list of a non-directory path.  return an error
							
								.SetErrObj( rtn, Str.Format( "Path must be a valid directory: %1", path ) )	
								return rtn
							
							end
						
							return .SuccessResult(fileInfo)
							
						End
						
						function Boolean _IsExcluded(String path, String fileName, List excludedFileNames, List exludedFolderNames)
						
							if File.IsDirSpec(path)
								return fileName in exludedFolderNames > 0
						
							elseif fileName in excludedFileNames
								return true
						
							elseif Str.Upper(fileName[-4:-1]) == ".LOG"
								return true
								
							else
							
								return false
							end	
						
						end
				
			
		
	
	endscript

	
	private function Assoc _GetGroupList( Object prgCtx, \
												Record groupRec, \
												Integer limit, \
												String path, \
												Boolean showUsers, \
												Boolean doRecursive = false, \
												Boolean initialCall = false, \
												Record rootRec = groupRec, \
												Assoc rootGroupInfo = Assoc.CreateAssoc())
												
				/* example of the output we need:
				{title: "Item 1"},
			    {title: "Folder 2", isGroup: true,
			            children: [
				                   {title: "Sub-item 2.1"},
			                       {title: "Sub-item 2.2"}
			                      ]
			    },
			    {title: "Item 3"}
				*/  
				
				Assoc 		rtn = .InitErrObj( "_GetGroupList" )
				Assoc		ugInfo
				Assoc		status
				Boolean		isRoot =rootRec.ID==groupRec.ID
				Record		childRec
				String		imgPath = $Kernel.ModuleSubsystem.GetItem( "datamanager" ).SupportPrefix() + "images/"
				String		groupIconPath = imgPath + "2-guys.gif"
				String		userIconPath = imgPath + "guy.gif"
				
				if groupRec.TYPE == UAPI.GROUP
				
					// *** get the title and path
					ugInfo.title = groupRec.NAME
					ugInfo.isGroup = true
					ugInfo.id  = groupRec.ID
					ugInfo.icon = groupIconPath
					ugInfo.children = {}
					ugInfo.path = path
					
					// *** save root values so we can get the total num groups from the UI
					if isRoot
						rootGroupInfo = ugInfo
						rootGroupInfo.total = 1
						rootGroupInfo.expand = true
			
						
						// *** if this is the initial call into this function, we just show the System Root node and
						// *** allow for the child groups to get lazy if the user elects to expands the list 
						// *** (helps avoid long wait and any issues with displaying characters in JSON)
						if initialCall
							rootGroupInfo.isLazy = true
							rootGroupInfo.expand = false
							rtn.result = rootGroupInfo
							return rtn
						end	
					end		
			
					// *** get the children
					status = $DataManager.UserUtils.GetGroupMembers(prgCtx, groupRec.ID, showUsers ? {UAPI.GROUP, UAPI.USER} : {UAPI.GROUP})
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					end
					
					// *** loop through the children	
					for childRec in status.result
						Boolean isGroup = childRec.TYPE == UAPI.GROUP
						if isGroup || showUsers
			
							// *** make sure we did not reach our limit
							if rootGroupInfo.total == limit
								.SetErrObj( rtn, Str.Format( "Number of nodes exceeded the maximum limit of %1.", limit ) )	
								return rtn						
							end			
						
						
							// *** add to the total num of items
							rootGroupInfo.total += 1
						
							if isGroup && doRecursive
								status = _GetGroupList(prgCtx, childRec, limit, ugInfo.path + ":" + childRec.NAME, showUsers, doRecursive, false, rootRec, rootGroupInfo)
								if !status.ok
									.SetErrObj(rtn, status)
									return rtn
								else
									ugInfo.children = {@ugInfo.children, status.result}	
								end	
							else
								Assoc childInfo = Assoc.CreateAssoc()
								childInfo.isLazy = isGroup
								childInfo.title = childRec.NAME
								childInfo.isGroup = isGroup
								childInfo.icon = isGroup ? groupIconPath : userIconPath
								childInfo.unselectable = !isGroup
								childInfo.id  = childRec.ID
								childInfo.path  = ugInfo.path + ":" + childRec.NAME
								ugInfo.children = {@ugInfo.children, childInfo}							
							end	
						end	
					end
						
				elseif !isRoot
				
					// *** get the title and path
					ugInfo.title = groupRec.NAME
					ugInfo.isGroup = false
					ugInfo.id  = groupRec.NAME
					ugInfo.children = {}
					ugInfo.path = path		
			
				else  // *** initial call is asking for file list of a non-directory path.  return an error
				
					.SetErrObj( rtn, Str.Format( "Group %1 must be a valid group", groupRec.ID ) )	
					return rtn
				
				end
				
				rtn.result = ugInfo
			
				return rtn
				
			End

	
	private function Assoc _GetMyFavoritesList(Object prgCtx, \
														Integer limit, \
														String path, \
														Boolean showFiles, \
														Boolean doRecursive = false, \
														Assoc rootNodeInfo = Assoc.CreateAssoc())
												
				/* example of the output we need:
				{title: "Item 1"},
			    {title: "Folder 2", isFolder: true,
			            children: [
				                   {title: "Sub-item 2.1"},
			                       {title: "Sub-item 2.2"}
			                      ]
			    },
			    {title: "Item 3"}
				*/  
				
				Assoc 		rtn = .InitErrObj( "_GetMyFavoritesList" )
				Record		childRec
				Assoc		nodeInfo
				Assoc		status
				Object		nodeUtils = $Datamanager.NodeUtils
					
				// *** allow passing of DAPINODE or WebNodes View Row
				Integer		nodeId = $TypeFavoritesVolsVirtual
				String		nodeName  = "My Favorites"
				Boolean		isRoot = true
			
				// *** get the title and path
				nodeInfo.title = nodeName
				nodeInfo.isFolder = true
				nodeInfo.dataid  = nodeID
				nodeInfo.children = {}
				nodeInfo.path = path
				
				// *** save root values so we can get the total num files from the UI
				if isRoot
					rootNodeInfo = nodeInfo
					rootNodeInfo.total = 1
					rootNodeInfo.expand = true
				end		
			
				// *** get the my favorites nodes	
				status = ._GetFavoritesContents(prgCtx)
				if !status.ok
					.SetErrObj(rtn, status)
					return rtn
				elseif IsDefined(status.Contents)
					for childRec in status.Contents
						Boolean isContainer = $DataManager.NodeUtils.IsFolderNode(childRec)
						if isContainer || showFiles
			
							// *** make sure we did not reach our limit
							if rootNodeInfo.total == limit
								.SetErrObj( rtn, Str.Format( "Number of nodes exceeded the maximum limit of %1.", limit ) )	
								return rtn						
							end			
						
							// *** add to the total num of items
							rootNodeInfo.total += 1
						
							Assoc childInfo = Assoc.CreateAssoc()
							childInfo.isLazy = isContainer
							childInfo.title = childRec.NAME
							childInfo.isFolder = isContainer
							childInfo.unselectable = !isContainer
							childInfo.dataid  = childRec.DATAID
							childInfo.path  = nodeUtils.NodeIdToPath(prgCtx, childRec.DATAID)
							nodeInfo.children = {@nodeInfo.children, childInfo}								
						end	
					end
				end	
							
				rtn.result = nodeInfo
			
				return rtn
				
			End

	
	private function Assoc _GetNodeList(Object prgCtx, \
												Dynamic node, \
												Integer limit, \
												String path, \
												Boolean showFiles, \
												Boolean doRecursive = false, \
												Dynamic rootNode = node, \
												Assoc rootNodeInfo = Assoc.CreateAssoc())
												
				/* example of the output we need:
				{title: "Item 1"},
			    {title: "Folder 2", isFolder: true,
			            children: [
				                   {title: "Sub-item 2.1"},
			                       {title: "Sub-item 2.2"}
			                      ]
			    },
			    {title: "Item 3"}
				*/  
				
				Assoc 		rtn = .InitErrObj( "_GetNodeList" )
				Record		childRec
				Assoc		nodeInfo
				Assoc		status
					
				// *** allow passing of DAPINODE or WebNodes View Row
				Integer		subtype = Type(node) == DAPI.DapiNodeType ? node.pSubtype : node.SUBTYPE
				Integer		nodeId = Type(node) == DAPI.DapiNodeType ? node.pID : node.DATAID
				Integer		rootId = Type(rootNode) == DAPI.DapiNodeType ? rootNode.pID : rootNODE.DATAID
				String		nodeName  = Type(node) == DAPI.DapiNodeType ? node.pNAME : node.NAME
			
				Boolean		isRoot = rootId==nodeID
			
			
				if $DataManager.NodeUtils.IsFolderNode(node)
				
					// *** get the title and path
					nodeInfo.title = nodeName
					nodeInfo.isFolder = true
					nodeInfo.dataid  = nodeID
					nodeInfo.children = {}
					nodeInfo.path = path
					
					// *** save root values so we can get the total num files from the UI
					if isRoot
						rootNodeInfo = nodeInfo
						rootNodeInfo.total = 1
						rootNodeInfo.expand = true
					end		
			
					// *** add the children
					Object childGetter = $DataManager.NodeChildGetter.New(subType)
					
					status = childGetter.GetSortedContents(prgCtx, node, "NAME")	
					if !status.ok
						.SetErrObj(rtn, status)
						return rtn
					else
						for childRec in status.result
							Boolean isContainer = $DataManager.NodeUtils.IsFolderNode(childRec)
							if isContainer || showFiles
				
								// *** make sure we did not reach our limit
								if rootNodeInfo.total == limit
									.SetErrObj( rtn, Str.Format( "Number of nodes exceeded the maximum limit of %1.", limit ) )	
									return rtn						
								end			
							
							
								// *** add to the total num of items
								rootNodeInfo.total += 1
							
								if isContainer && doRecursive
									status = _GetNodeList(prgCtx, childRec, limit, nodeInfo.path + ":" + childRec.NAME, showFiles, doRecursive, rootNode, rootNodeInfo)
									if !status.ok
										.SetErrObj(rtn, status)
										return rtn
									else
										nodeInfo.children = {@nodeInfo.children, status.result}	
									end	
								else
									Assoc childInfo = Assoc.CreateAssoc()
									childInfo.isLazy = isContainer
									childInfo.title = childRec.NAME
									childInfo.isFolder = isContainer
									childInfo.unselectable = !isContainer
									childInfo.dataid  = childRec.DATAID
									childInfo.path  = nodeInfo.path + ":" + childRec.NAME
									nodeInfo.children = {@nodeInfo.children, childInfo}							
								end	
							end	
						end
					end	
						
				elseif !isRoot
				
					// *** get the title and path
					nodeInfo.title = nodeName
					nodeInfo.isFolder = false
					nodeInfo.dataid  = nodeID
					nodeInfo.children = {}
					nodeInfo.path = path		
			
				else  // *** initial call is asking for file list of a non-directory path.  return an error
				
					.SetErrObj( rtn, Str.Format( "Node %1 must be a valid container", nodeID ) )	
					return rtn
				
				end
				
				rtn.result = nodeInfo
			
				return rtn
				
			End

	
	private function Assoc _GetRootVolumeList(Object prgCtx)
																							
				String 		s
				Dynamic 	status
				RecArray 	contents
				
			
				// find all browseable volumes subtypes
				/*
				for obj in $WebNode.WebNodes.GetItems()
					if obj.BrowseableVolume( prgCtx ) 
					
						if $ApplTypeUserWorkbin in obj.ApplTypes()
							workbinSubTypes.( obj.fSubType ) = TRUE
							
						elseif obj.fSubType == $TypeVolDomainWorkspace 
						
							if ( prgCtx.fDbConnect.UserInfo().SpaceID != 0 )
								subTypes.( obj.fSubType ) = TRUE
							end
						else
							subTypes.( obj.fSubType ) = TRUE
						end
						
					end
				end
				*/
				
				// create the statement
				
				if prgCtx.fDbConnect.HasByPassPerm()
				
					if IsDefined($TypeVolRecycleBin)
						s = Str.Format('(SubType in (%1,%2,%3) or (SubType=%4 and UserID=:A1))',\
								$TypeVolLibrary, \
								$TypeVolRecycleBin, \
								$TypeVolDeletedItems, \		
								$TypeVolWorkbin )
					else
						s = Str.Format('(SubType in (%1,%2) or (SubType=%3 and UserID=:A1))',\
								$TypeVolLibrary, \
								$TypeVolDeletedItems, \		
								$TypeVolWorkbin )
					end			
							
				else
					s = Str.Format('(SubType=%1 or (SubType=%2 and UserID=:A1))',\
							$TypeVolLibrary, \
							$TypeVolWorkbin )
				end			
										
				
				// create the statement (prescreen for see access)
				
				status = $LLIAPI.NodeUtil.ListNodes( prgctx, s, { prgCtx.DSession().fUserID } )
				
				// now locate the nodes
				
				if !status.OK
					return .SetErrObj(.InitErrObj("_GetRootVolumeList"), status)
				
				else
					contents = status.contents
					
					
					// *** also plop in a virtual container for My Favorites
					Record rec, workbinRec
					
					for rec in contents
						if rec.SUBTYPE == $TypeVolWorkbin
							workbinRec = $LLIAPI.RecArrayPkg.CopyRec(rec)
							break
						end
					end
					
					if IsDefined(workbinRec)
						workbinRec.DATAID = $TypeFavoritesVolsVirtual
						workbinRec.SUBTYPE = $TypeFavoritesVolsVirtual
						workbinRec.NAME = "My Favorites"
						
						RecArray.InsertRecordAt(contents, workbinRec, Length(contents)+1)
						
					end
					
					contents = RecArray.Sort(contents, "NAME")
					
					
				end		
			
				Assoc rtn
				rtn.ok = true
				rtn.result = contents
				return rtn
			
			end

end

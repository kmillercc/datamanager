package DATAMANAGER

public object Export inherits DATAMANAGER::ExportLoadCmds

	override	Integer	fEvent = 6
	override	List	fNodetypes = { 0, 298, 202, 201 }
	override	Integer	fPerms = 36865


	override script #'0 Setup'#
	
			
					
							
									
											
													
												
														
															// Patched by Pat201610180
															//	Set required permissions mask.
															.fPerms = $PSeeContents
															
															.fNodetypes = {$TypeFolder, $TypeCollection, $TypeProject, $TypeProjectVol}
															
															.fEvent = $DataManager.AllowPkg.fEVENT_EXPORT_SYNC
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function String Confirm(\
							Record	args,\						//	RequestHandler.fArgs.
							Dynamic	node )						//	WebNodes view row.
							
							
							return "Do you want to setup a bulk export of this container?"
						end

end

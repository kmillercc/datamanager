package DATAMANAGER

public object SynCoreInterface inherits DATAMANAGER::SynCoreMain

	
	public function void ClearLogIndent()
						
							$SynLog.Indent.Clear()
						
						end

	
	public function void ClearNDC()
						
							$SyntergyCore.LogPkg.NDC.Clear()
						
						end

	
	public function void DumpErrorAssoc(Assoc err, \
													Object log)
														
							$SyntergyCore.LogUtils.DumpErrorAssoc(err, log)
						end

	
	public function GetObjectName(Dynamic context = this)
						
							return ._GetObjectName(context)
							
						end

	
	public function Boolean IsMSSQL()
							return $Syntergycore.SQLUtils.IsMSSQL()
						End

	
	public function Boolean IsOracle()
							return $Syntergycore.SQLUtils.IsOracle()
						End

	
	public function Void LogEntry( Object log, \
												Integer level, \
												String methodName, \
												Object context, \
												List params)
							
							if log.IsLevelEnabled(level)
								$SynLog.LogEntry(log, level, methodName, context, params)
							end	
							
						end

	
	public function LogIndentPop()
						
							$SynLog.Indent.Pop()	
						
						end

	
	public function LogIndentPush()
						
							$SynLog.Indent.Push()	
						
						end

	
	public function void PopNDC()
						
							$SyntergyCore.LogPkg.NDC.Pop()
						
						end

	
	public function void PushNDC(String name)
						
							$SyntergyCore.LogPkg.NDC.Push(name)
						
						end

	
	public function void RecordTime(String timerName)
							$SyntergyCore.Stats.Stop( "DataManager", timerName ) 	
						end

	
	public function void StartTimer(String timerName)
						
							$SyntergyCore.Timer.Start( timerName )
						
						end

	
	public function Real StopTimer(String timerName)
							return $SyntergyCore.Timer.Stop( timerName )
						end

	
	public function Real TimeElapsed(String timerName)
							return $SyntergyCore.Timer.Elapsed( timerName )
						end

	
	public function String ToJSON( Dynamic x, Boolean useBuiltinIfAvailable=true )
							return $SyntergyCore.JSON.toJSON( x, useBuiltinIfAvailable )
						End

end

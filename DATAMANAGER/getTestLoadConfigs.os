package DATAMANAGER

public object getTestLoadConfigs inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
						
							/**
							 * R may contain: 
						     **/
						
							
							List	config1 = $DataManager.Tester.GetTestCsvLoads()
							List	config2 = $DataManager.Tester.GetTestFileLoads()
							
							List 	allConfigs = {@config1, @config2}
							
							.fContent = synCore.SuccessResult(allConfigs)
							
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
								}
						end

end

package DATAMANAGER

public object OBJECTTYPE inherits DATAMANAGER::Common

	override	String	fApiKey1 = 'subtype'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$OBJECTTYPE'


	
	override function Assoc ValidateVal(Object prgCtx, \
													Dynamic val, \
													String action, \
													Frame apiDataObj)
							
							if Type(val) == StringType
								Integer intType = Str.StringToInteger(val)
								if IsDefined(intType)
									val = intType
								else
									val = Str.Upper(val)
									
									switch val
									
										case "FOLDER"
											val = $TypeFolder
										end
										
										case "DOCUMENT", "DOC"
											val = $TypeDocument
										end
										
										case "COMPOUNDDOC", "CD"
											val = $TypeCompoundDoc	
										end
										
										default
											return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("Could not determine subtype of object. Unknown value '%1'", val))
										end
									
									end
								end
							end	
							
							// **** do we havethis subtype installed	
							if !$LLIAPI.LLNodeSubsystem.GetItem(val).fEnabled
								// *** KM 11-30-15 - Add check to see if it's one of the case types
							
								if IsDefined($CaseBasic) && (val in $Datamanager.ObjectTypes.GetCaseTypes(prgCtx))
									// *** we're ok -- it's a case type	
								
								else
									return .SetErrObj(.InitErrObj("ValidateVal"), Str.Format("The $ObjectType %1 is not installed in this system", val))
								end				
							end	
							
							Assoc rtn
							rtn.result = val
							rtn.ok = true
							return rtn
										
												
						end

end

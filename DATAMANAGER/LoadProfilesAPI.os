package DATAMANAGER

public object LoadProfilesAPI inherits DATAMANAGER::ProfilesAPI

	override	String	fIniSection = 'SyntBulkLoader.LoadProfiles'


	
	override function Assoc _IsAllowed(Object prgCtx)
						
							Assoc 	rtn = .InitErrObj( "_IsAllowed" )
							Object	allowPkg = $DataManager.AllowPkg	
							
							// **** check if we are ok to do this
							Assoc status = allowPkg.CheckAllowed(prgCtx, allowPkg.fEVENT_LOAD_MNG_PROF)
							if !status.ok
								.SetErrObj(rtn, status)
								return rtn	
							end		
						
							return rtn
						
						end

end

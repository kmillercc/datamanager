package DATAMANAGER

public object AboutDataManager inherits DATAMANAGER::DatamanagerRoot

	public		String	fReleaseDateStr = 'Mar 26, 2020'
	public		Integer	fSchemaVersion = 104
	public		Integer	fUpdateVersion = 3

	public function String GetFooterString()
												
			List levels = $DataManager.WebModule.fVersion 
			String s = Str.Format("Data Manager %1.%2.%3.%4 ", levels[1], levels[2], levels[4], .fUpdateVersion)
			
			
			return s + .fReleaseDateStr
			
		end

	
	public function String GetHeaderString()
												
			List levels = $DataManager.WebModule.fVersion 
			String s = Str.Format("Data Manager %1.%2.%3.%4 ", levels[1], levels[2], levels[4], .fUpdateVersion)
			
			
			return s
		
		end

	
	public function String GetLastCommit()
												
			return ''
		
		end

	
	public function String GetReleaseString()
												
			List levels = $DataManager.WebModule.fVersion 
			String s = Str.Format("Data Manager %1.%2.%3", levels[1], levels[2], levels[4])
			
			if .fUpdateVersion > 0
			
				s += Str.Format(" - Update %1", Str.String(.fUpdateVersion))
				
			end
			
			return s
		
		end

	
	public function String GetSchemaVersion()
		
			return Str.String(.fSchemaVersion)
		
		end

end

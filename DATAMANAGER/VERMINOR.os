package DATAMANAGER

public object VERMINOR inherits DATAMANAGER::MajorMinorBooleanInt

	override	String	fApiKey3 = 'pVerMinor'
	override	Boolean	fEnabled = TRUE
	override	String	fKey = '$VERMINOR'

end

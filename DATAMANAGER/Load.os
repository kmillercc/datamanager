package DATAMANAGER

public object Load inherits DATAMANAGER::ExportLoadCmds

	override	Integer	fEvent = 13
	override	List	fNodetypes = { 0, 202 }
	override	Integer	fPerms = 4


	override script #'0 Setup'#
	
			
					
							
									
											
													
												
														
															//	Set required permissions mask.
															.fPerms = $PCreateNode
															
															.fNodetypes = {$TypeFolder, $TypeProject}
															
															.fEvent = $DataManager.AllowPkg.fEVENT_LOAD_SYNC
														
													
												
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	override function String Confirm(\
							Record	args,\						//	RequestHandler.fArgs.
							Dynamic	node )						//	WebNodes view row.
							
							
							return "Do you want to setup a bulk export of this container?"
						end

end

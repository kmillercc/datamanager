package DATAMANAGER

public object getPathFromId inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'nodeId', 2, 'nodeId', FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
							
							Object		synCore = $DataManager.SynCoreInterface
							Object		prgCtx = .fPrgCtx
						
							/**
							 * R may contain: 
							 *  nodeId			=> Integer	=> jobType
						     **/
						
							// *** hand off to the API
							String path = $DataManager.NodeUtils.NodeIdToPath(prgCtx, r.nodeId)
							
							.fContent = synCore.SuccessResult(path)
						
							return undefined	
						End

	
	override function void SetPrototype()
							
							.fPrototype =\
								{\
									{ "nodeId", IntegerType, "nodeId", FALSE } \
								}
						end

end

package DATAMANAGER

public object getFileTree inherits DATAMANAGER::JSONEnabled

	override	Boolean	fCheckReferer = TRUE
	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'filepath', -1, 'FilePath', FALSE }, { 'doRecursive', 5, 'DoRecursive', TRUE, FALSE }, { 'showFiles', 5, 'ShowFiles', TRUE, TRUE }, { 'filterExtension', -1, 'ShowFiles', FALSE, Undefined }, { 'returnDirect', 5, 'ReturnDirect', TRUE, FALSE }, { 'getMetadata', 5, 'GetMetadata', TRUE, FALSE } }


	
	override function Dynamic DoExec( Dynamic ctxIn, Dynamic ctxOut, Record r )
												
												Object		synCore = $DataManager.SynCoreInterface
												Assoc		rtn = synCore.InitErrObj( "Validate.DoExec", this )
												Assoc		status
												/**
												 * R may contain: 
												 * 	filepath 		=> String 	=> the filepath we are validating
											 	 *  doRecursive 	=> Boolean	=> recurse into directory and get all files
											 	 *  showFiles 		=> Boolean	=> showFiles
											  	 *  filterExtension => String	=> filter file extension
											 	 *  returnDirect	=> Boolean	=> return the data directly with no standard OK assoc (i.e. for lazy loading)
											 	 *  getMetadata		=> Boolean get metadata for each file
											  	 *	needCount		=> Boolean	=> do you need the total count of objects?
											     **/
											
												// **** make sure file path does not come in empty! very important as this will cause LL to start looking in the root
												if Str.Trim(r.filepath) == ''
													// ** for debugging
													synCore.SetErrObj( rtn, "Filepath must not be empty")
													.fError = rtn	
													return undefined				
												end
											
												// *** check that file exists
												if !r.createPath
													status = $DataManager.FileUtils.CheckPathExists(r.filepath, 'folder')
													if !status.ok
														// ** for debugging
														synCore.SetErrObj( rtn, status )
														.fError = rtn
														return undefined
													end
												end
												
												Integer limit = $DataManager.ModulePrefs.GetPref("LOAD_EXPORT_SYNC_LIMIT", 10000)	
													
												// *** hand off to the API
												status = $DataManager.UiSupportAPI.GetFileTree(r.filepath, r.showFiles, r.filterExtension, limit, r.doRecursive, r.getMetadata)
												if status.ok
													Assoc fileInfo = status.result
													if r.returnDirect
														.fContent = fileInfo.children
													else
														.fContent = synCore.SuccessResult(fileInfo)	
													end	
												else
													// for debugging .
													synCore.SetErrObj( rtn, status )
													.fError = rtn
												end		
												
												return undefined	
											End

	
	override function void SetPrototype()
												
												.fPrototype =\
													{\
														{ "filepath", StringType, "FilePath", FALSE }, \
														{ "doRecursive", BooleanType, "DoRecursive", TRUE, FALSE }, \			
														{ "showFiles", BooleanType, "ShowFiles", TRUE, TRUE }, \			
														{ "filterExtension", StringType, "ShowFiles", FALSE, undefined }, \			
														{ "returnDirect", BooleanType, "ReturnDirect", TRUE, FALSE }, \						
														{ "getMetadata", BooleanType, "GetMetadata", TRUE, FALSE } \
													}
											end

end

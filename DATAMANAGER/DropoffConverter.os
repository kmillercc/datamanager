package DATAMANAGER

public object DropoffConverter inherits DATAMANAGER::Utils

	script ConvertDropoffJob
	
			
					
							
									
											
													
															
																	
																			
																					
																							
																						
																								
																									function Assoc ConvertDropoffJob(Object prgCtx, \
																																	Integer id, \
																																	String jobType, \
																																	String action)
																									
																										
																										String	stmt = "select * from BULKLOADER_WATCHED_FOLDERS where ID=:A1"
																									
																										RecArray result = .ExecSql(prgCtx, stmt, {id})
																										if IsError(result)
																											return .SetErrObj(.InitErrObj("ConvertDropoffJob"), result)
																										elseif Length(result)==0
																											return .SetErrObj(.InitErrObj("ConvertDropoffJob"), Str.Format("Could not get job with id %1", id))	
																										else	
																											Record rec = result[1]
																											
																											Assoc options = IsDefined(rec.OPTIONS) ? Str.StringToValue(rec.OPTIONS) : Assoc.CreateAssoc()
																											
																											/* values we need to populate */
																											Assoc newVals
																											newVals.("jobType")				= "load"
																											newVals.("jobSubtype")			= jobType
																											newVals.("action")				= action
																											newVals.("enabled") 			= true	
																											newVals.("markAfterLoading") 	= true
																									        newVals.("rootSourcePath")		= rec.DIRPATH
																									        newVals.("rootTargetPath") 		= _ParseLoadPath(rec.LOAD_PATH)
																											newVals.("jobname")				= rec.FRIENDLY_NAME	
																											newVals.("jobdesc")				= rec.FRIENDLY_NAME
																											newVals.("notify_emails")		= ._GetNotifyEmails(prgCtx, options)
																											newVals.("schedule_type")		= 2				//1 (1 time only) or 2 (recurring)
																											newVals.("one_time_datetime")	= undefined
																											newVals.("day_pattern")			= 2				//1 (Every X Days) or 2 (On certain days of week)
																											newVals.("day_recurrence")		= undefined
																									
																											// *** get days of week
																											._SetDaysOfWeek(rec.DMASK, newVals)		
																											
																											// *** set the hour and minute times/frequencies
																											._SetHoursAndMinutes(rec.HMASK, rec.MMASK, newVals)
																											
																											// *** set other options
																											._SetOptions(options, newVals)
																											
																											// *** now call the LoadJobsAPI to add the job						
																											Assoc status = $DataManager.LoadJobsAPI.AddEditItem(prgCtx, newVals)
																											if !status.ok
																												return .SetErrObj(.InitErrObj("ConvertDropoffJob"), status)
																											end				
																											
																										end	
																									
																									
																										Assoc rtn
																										rtn.ok = true	
																										
																										return rtn
																									
																									end
																									
																									function _ParseLoadPath(String path)
																										
																										if path[1] == ":"
																											path=path[2:]
																										end
																										
																										if Str.Upper(path[1:15]) == "CONTENT SERVER:"
																											path = path[16:]
																										elseif Str.Upper(path[1:15]) == "LIVELINK:"
																											path = path[10:]
																										end
																									
																										return path
																									end		
																											
																										
																								
																							
																						
																								
																							
																						
																					
																				
																			
																		
																	
																
															
														
													
												
											
										
									
								
							
						
					
				
			
		
	
	endscript

	
	public function Boolean HasDropoffs(Object prgCtx)
											
												RecArray result = .ExecSql(prgCtx, "select * from BULKLOADER_WATCHED_FOLDERS")
												
												if IsNotError(result) && Length(result)
													return true
												else
													return false
												end	
											end

	
	public function Assoc UpgradeBulkToolsJobs(Object prgCtx = .GetAdminCtx().prgCtx)
											
												Object loadJobsAPI = $DataManager.LoadJobsAPI
											
												// *** get the bulk tools jobs
												RecArray recs = .ExecSql(prgCtx, "select * from SYNTBT_JOB_SCHEDULES")
												
											
												// *** iterate through and save into new table	
												if IsNotError(recs)
													Record rec
													Assoc 	dbVals
													
													for rec in recs
														dbVals = Assoc.FromRecord(rec)
											
														// *** delete the schedule id
														Assoc.Delete(dbVals, "SCHEDULEID")
														
														// *** daily at times
														dbVals.("DAILY_AT_TIMES") = IsDefined(dbVals.DAILY_AT_TIME) ? Str.String(dbVals.DAILY_AT_TIME) : undefined
														Assoc.Delete(dbVals, "DAILY_AT_TIME")
														
														// *** folderstructure changed to filestructure
														if dbVals.JOBSUBTYPE=="loadfolderstructure"
															dbVals.JOBSUBTYPE="loadfilestructure"
															
															Assoc options = Str.StringToValue(dbVals.options)
															options.jobSubtype = "loadfilestructure"
															dbVals.options = Str.ValueToString(options)
														end
														
														// *** start transaction
														prgCtx.fDbConnect.StartTrans()
												
															Assoc status = loadJobsAPI.PutDbVals(prgCtx, dbVals)
															if !status.ok
																return .SetErrObj(.InitErrObj("UpgradeBulkToolsJobs"), status)
															end					
											
														// *** end transaction
														prgCtx.fDbConnect.EndTrans(status.ok)
														
													end
											
												else
													return .SetErrObj(.InitErrObj("UpgradeBulkToolsJobs"), recs)
												end
												
												Assoc rtn
												rtn.ok = true
												return rtn
											end

	
	private function String _GetNotifyEmails(Object prgCtx, Assoc options)
											
												List 	usersList = IsDefined(options.NotifyUsers) ? options.NotifyUsers : {}
												List 	userList
												String	emailStr
												
												for userList in usersList
														
													Dynamic userId = Type(userList[1]) == StringType ? Str.StringToInteger(userList[1]) : userList[1]
													
													Assoc status = ._UserRecFromID(prgCtx, userId)
													if status.ok 
														Record userRec = status.result
														
														if IsDefined(userRec.MAILADDRESS)
															emailStr += Length(emailStr) ? "," : ""
															emailStr += userRec.MAILADDRESS
														end
														
													
													end
											
												end
												
												return emailStr
											
											end

	
	private function Assoc _MaskToAssoc( Integer nMask, Integer mask )
											
												Integer		n
												
												Assoc		outMask = Assoc.CreateAssoc( '' )
												
												for n = 0 to ( nMask - 1 )
													if ( mask & 1 ) != 0
														outMask.( n ) = true
													end
											
													mask = mask >> 1
												end
											
												return outMask
											end

	
	private function Void _SetDaysOfWeek(Integer dMask, Assoc newVals)
											
												Assoc dayAssoc = ._MaskToAssoc(7, dMask)
											
											
												if (dayAssoc.(0))
													newVals.("sunday") = true
												end
											
												if (dayAssoc.(1))
													newVals.("monday") = true
												end
												
												if (dayAssoc.(2))
													newVals.("tuesday") = true
												end
												
												if (dayAssoc.(3))
													newVals.("wednesday") = true
												end
												
												if (dayAssoc.(4))
													newVals.("thursday") = true
												end
												
												if (dayAssoc.(5))
													newVals.("friday") = true
												end
												
												if (dayAssoc.(6))
													newVals.("saturday") = true
												end					
											
											end

	
	private function Void _SetHoursAndMinutes(	Integer hMask, \
																				Integer mMask, \
																				Assoc newVals)
												
												Assoc hours = ._MaskToAssoc(24, hMask)
												Assoc minutes = ._MaskToAssoc(12, hMask)
												
												Integer numHours = Length(Assoc.Keys(hours))
												Integer numMinutes = Length(Assoc.Keys(minutes)) 
												Integer timesPerDay = numHours * numMinutes
												Integer everyXMinutes = 1440/timesPerDay				// *** 1440 minutes in a day
												
												// *** the number of hour and minute times we have will determine whether or not we just set this to an interval
												if timesPerDay > 10
													
													newVals.("daily_frequency")	= 2		//1 (Once during the day) or 2 (At regular intervals during the day)
											
													if everyXMinutes < 60
														newVals.("daily_every_unit")	= 2			//1 (Hours) 2 (Minutes)	
														newVals.("daily_every")			= Math.Max(10, everyXMinutes)			// no greater than every 10 mins
											
													else
														
														newVals.("daily_every_unit")	= 1			//1 (Hours) 2 (Minutes)	
														
														Integer everyXHours = everyXMinutes/60
														newVals.("daily_every")			= everyXHours
													
													
													end	
													
												else	
													
												end	
																				
																				
											end

	
	private function void _SetOptions(Assoc options, Assoc newVals)
											
											/*
												Old Options we are not accounting for. Not sure what these are
												ENABLECUSTOMVALIDATE=false,
												PERMGROUP_ID=-1,
												PERMGROUP_NAME=
												NewStyleCerlRevision = false
											*/
											
												// *** make sure we set async
												newVals.("whenOption") = "async"
												
												// *** undefined audit path will put the audit logs in the load folder
												newVals.("auditFolderPath") = undefined
											
												// *** hard code some values based on the behavior of Bulk Loader
												newVals.("sourcePathType") = "parent"
												newVals.("targetPathType") = "parent"	
											
												// *** select all metadata
												newVals.("mdCats") 	=true
												newVals.("mdClass")	=true
												newVals.("mdDapi")	=true
												newVals.("mdPo")	=true
												newVals.("mdRm")	=true
												newVals.("mdEmail")	=true
												newVals.("mdSysAttrs")	=true
												newVals.("mdOwner")	=true
												
												// *** option flags we might have from old job
												newVals.("autoCreateFolders") = IsDefined(options.DOFOLDERS) ? options.DOFOLDERS : true
												newVals.("autoCreateCD") = true
												newVals.("docOptionAddVersion") = IsDefined(options.DOVERSIONS) ? options.DOVERSIONS : true
												newVals.("docOption") = newVals.("docOptionAddVersion") ? "update" : "reporterrors"
												newVals.("docOptionUpdateMd") = newVals.("docOptionAddVersion")
												newVals.("containerOption") = newVals.("docOptionAddVersion") ? "update" : "reporterrors"
												newVals.("containerOptionUpdateMd") = newVals.("docOptionAddVersion")
												newVals.("inheritParent") = IsDefined(options.INHERITATTRIBUTES) ? options.INHERITATTRIBUTES  : true
												newVals.("requireMetadata") = IsDefined(options.SKIPNOMD) ? options.SKIPNOMD  : true
												newVals.("defaultFileExtension") = IsDefined(options.DEFAULTFILETYPE) ? options.DEFAULTFILETYPE  : "pdf"	
												newVals.("useFileCreateDate")  = IsDefined(options.CREATEWITHFILEDATE) ? options.CREATEWITHFILEDATE : false
												newVals.("docOptionAddVersionIfNewer") = IsDefined(options.DOVERSIONSIFFILENEWER) ? options.DOVERSIONSIFFILENEWER : false
												
												// *** new security flags
												newVals.("runAsUserName") = IsDefined(options.PERMUSER_NAME) && Length(options.PERMUSER_NAME) ? options.PERMUSER_NAME : "Admin"			// *** default run as admin	
												newVals.("allowAnonymousUsers") = IsDefined(options.AllowAnonymousUsers) ? options.AllowAnonymousUsers : true
												newVals.("allowPOUpdates") = IsDefined(options.AllowPOUpdates) ? options.AllowPOUpdates : true
												newVals.("allowRDUpdates") = IsDefined(options.AllowRDUpdates) ? options.AllowRDUpdates : true		
											
												
											end

end
